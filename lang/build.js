var fs = require('fs'); 
var path = require('path');
var parse = require('csv-parse');

let source = process.argv[2] || 'lang';
let dest = process.argv[3] || 'public/locales';

function findFilesInDir(startPath,filter){
  var results = [];

  if (!fs.existsSync(startPath)){
      console.log("no dir ",startPath);
      return;
  }

  var files=fs.readdirSync(startPath);
  for(var i=0;i<files.length;i++){
      var filename=path.join(startPath,files[i]);
      var stat = fs.lstatSync(filename);
      if (stat.isDirectory()){
          results = results.concat(findFilesInDir(filename,filter)); //recurse
      }
      else if (filename.indexOf(filter)>=0) {
          // console.log('-- found: ',filename);
          results.push(filename);
      }
  }
  return results;
}

let langData = {};

function processLangFile(filename) {
  console.log(`Processing ${filename}`);
  let parts = filename.split('/');
  let basename = parts[1].split('.')[0];
  let n = basename.split('-');
  let namespace = n[0];
  let locale = n[1];

  let fd = fs.createReadStream(filename).pipe(parse.parse({columns: ['component', 'key', 'value']}, (err, records) => {
    //let langData = {};
    if(langData[locale] == undefined) {
      langData[locale] = {};
    }
    if(langData[locale][namespace] == undefined) {
      langData[locale][namespace] = {};
    }
    records.forEach(({component, key, value}) => {
      if(component == '') {
        langData[locale][namespace][key] = value;
      } else {
        if(langData[locale][namespace][component] == undefined) {
          langData[locale][namespace][component] = {};
        }
        langData[locale][namespace][component][key] = value;
      }
    });
  }));
  let e = new Promise((resolve, reject) => {
    fd.on('end', () => {resolve(true);});
    fd.on('error', reject)
  })
  return e;
}

function combineSection(ne, en) {
  return {...en, ...ne};
}

function combineNamespace(ne, en) {
  if(ne === undefined) {
    return en;
  }
  if(typeof ne === 'string') {
    return ne;
  }
  let sections = Object.keys(ne).forEach(s => {
    if(typeof ne[s] === 'string') {
      return;
    }
    ne[s] = combineSection(ne[s], en[s]);
  })
  return ne;
}

function combineData(ne, en) {
  Object.keys(en).forEach((namespace) => {
    ne[namespace] = combineNamespace(ne[namespace], en[namespace]);
 })
 return ne;
}

function writeOutputs() {
  let langs = Object.keys(langData);
  langs.forEach((lang) => {
    
    if(lang != 'en') {
      langData[lang] = combineData(langData[lang], langData['en']);
    }

    let namespaces = Object.keys(langData[lang]);
    namespaces.forEach((ns) => {
      let destName = `${dest}/${lang}/${ns}.json`;
      console.log(`Saving to ${destName}`);
      let data = JSON.stringify(langData[lang][ns], undefined, 2);
      fs.mkdirSync(`${dest}/${lang}`, {recursive: true});
      fs.writeFileSync(destName, data)
      })
  })
}

let r = findFilesInDir(source, ".csv");
let promises = r.map(f => processLangFile(f) );

Promise.all(promises).then(x => {
  writeOutputs();
});
