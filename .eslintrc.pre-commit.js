/*
This ESLint config is used when linting files as part of the pre-commit hook.
The aim of this config is to enforce working files to not be ignored by ESLint
while still allowing existing code to have lint issues.
 */
module.exports = {
  extends: ['./.eslintrc.js'],
  plugins: ['unicorn'],
  rules: {
    'unicorn/no-abusive-eslint-disable': 'error',
  },
};
