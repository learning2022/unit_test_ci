## New Environment

```sh
# from root directory of git repo
yarn install

export LOREE_ROOT_DIR=$(pwd)
for d in amplify/backend/function/* ; do
  cd "$d/src"
  npm install
  echo "$d"
  cd $LOREE_ROOT_DIR
done

## Temporarily need to add some libraries but don't check it in
yarn add --dev aws-sdk
yarn add --dev he
yarn add --dev node-html-parser

export AWS_PROFILE=cd-sandbox-AdministratorAccess && aws sso login && . ~/.aws/set-aws-sso-credentials.sh

export LOREE_ENV_NAME=nickddev
export MASTEDLY_TESTING_ACCESS_TOKEN=____PLEASETAKEMEFROMYOURCANVASPROFILE____
export LOREE_USERNAME=
export LOREE_PASSWORD=

amplify env add $LOREE_ENV_NAME
# pick AWS Profile
# pick profile: cd-sandbox-AdministratorAccess

amplify push --yes

### IMPORTANT - get latest aws-exports.js file
amplify env checkout $LOREE_ENV_NAME

# setup integration
# the '2> setup_stack_err.log' part writes out detailed errors if they occur
node deploy/setup/setupStack.mjs 2> setup_stack_err.log
```

```sh
yarn start

# then open the generated external tool URL from script above

# in another tab/window
# tail the lti while you are checking login to canvas works
aws logs tail /aws/lambda/loreeltifn-${LOREE_ENV_NAME} --since 10m --follow
```

### Other useful commands

```sh
# see log groups
aws logs describe-log-groups | jq '.logGroups[] | .logGroupName' | grep $LOREE_ENV_NAME
```

### References

https://crystaldelta.atlassian.net/wiki/spaces/PBUG/pages/776503328/Client+on-board+process+in+Loree-v2
https://crystaldelta.atlassian.net/wiki/spaces/PBUG/pages/1396313699/Loree+2.0+Installation+Guide+for+UTS+Dev+Environment
https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/861110404/Loree+2.0+Installation+Guide+Complete
