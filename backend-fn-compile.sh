#!/bin/sh

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

function do_compile() {
  TARGET="${1}"
  TPATH_SRC="${SCRIPT_DIR}/${TARGET}/src"

  if [ ! -d "${TPATH_SRC}" ]; then
    echo "ERR Function SRC path does not exist: ${TPATH_SRC}";
    exit 1;
  fi

  # Only compile tsc if tsconfig exists...
  TPATH_TSCONFIG="${SCRIPT_DIR}/${TARGET}/real-src/tsconfig.json"
  if [ -f "${TPATH_TSCONFIG}" ]; then
    cd ${TPATH_SRC}
    echo "Compiling ${TARGET}..."
    yarn compile
    echo "Compiling ${TARGET} DONE"
    cd ${SCRIPT_DIR}
  fi
}

for dirname in amplify/backend/function/* ; do
  do_compile "$dirname"
done

