/*
 Copyright (c) 2014-2018, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or https://ckeditor.com/license
*/
CKEDITOR.plugins.a11ychecker.quickFixes.get({
  name: 'ElementReplace',
  callback: function (ElementReplace) {
    function VideoTitleReplace(issue) {
      ElementReplace.call(this, issue);
    }

    VideoTitleReplace.prototype = new ElementReplace();
    VideoTitleReplace.prototype.constructor = VideoTitleReplace;
    VideoTitleReplace.prototype.display = function (a) {
      console.log(this.issue.element);
      a.setInputs({
        title: {
          type: 'text',
          label: 'Suggested Title',
          value: this.issue.element.getAttribute('title') || 'Loree Video Title',
        },
      });
    };
    VideoTitleReplace.prototype.fix = function (a, b) {
      console.log(a.title);
      this.issue.element.setAttribute('title', a.title);
      b && b(this);
    };
    VideoTitleReplace.prototype.getTargetName = function (formAttributes) {
      return 'em';
    };

    CKEDITOR.plugins.a11ychecker.quickFixes.add('en/VideoTitleReplace', VideoTitleReplace);
  },
});
