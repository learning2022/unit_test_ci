let foregroundColorPicker = null;
let textBackgroundColorPicker = null;
let borderColorPicker = null;
let backgroundColorPicker = null;
let rowColumnBorderColorPicker = null;

let foregroundDefaultColor = null;
let textBackgroundDefaultColor = null;
let backgroundDefaultColor = null;
let rowColumnBorderDefaultColor = null;
let textBorderDefaultColor = null;

const validHosts = [
  'https://master.d1uzdukkodhm61.amplifyapp.com',
  'https://stg-loree-v2.crystaldelta.net',
  'https://dev-loree-v2.crystaldelta.net',
  'http://localhost:3000',
  'https://loree-int-dev.crystaldelta.net',
  'https://stg.loree-interactive.crystaldelta.net',
  'https://loree-int.crystaldelta.net',
  'https://loree-h5p-dev.crystaldelta.net',
  'https://loree-h5p.crystaldelta.net',
  'https://master.dsw63snfssb15.amplifyapp.com',
  'https://new-develop.d19viipu2480ae.amplifyapp.com',
  'https://master.d273ugfkhsuatz.amplifyapp.com', // Loree US Region
];

function isValidHost(host) {
  return validHosts.includes(host);
}

function initiateForegroundColorPickers() {
  foregroundColorPicker = window.Pickr.create({
    el: '#loree-text-options-foreground-color-picker',
    container: '#loree-text-options-foreground-color-picker-wrapper',
    theme: 'nano',
    default: '#000000',
    inline: true,
    showAlways: true,
    useAsButton: true,
    lockOpacity: true,
    defaultRepresentation: 'HEXA',
    components: {
      preview: true,
      opacity: false,
      hue: true,
      interaction: {
        hex: true,
        rgba: false,
        input: false,
        clear: true,
        save: true,
        cancel: true,
      },
    },
    i18n: {
      'btn:save': 'Apply',
      'btn:cancel': 'Cancel',
      'btn:clear': 'Clear',
    },
  });

  foregroundColorPicker.on('change', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'foregroundColor',
      color: color.toHEXA().toString(),
    };
    parent.postMessage(data, window.location.origin);
  });

  foregroundColorPicker.on('save', (color) => {
    if (color) {
      const data = {
        type: 'colorPickerSave',
        key: 'foregroundColor',
        color: color.toHEXA().toString(),
      };
      parent.postMessage(data, window.location.origin);
      parent.postMessage(
        {
          type: 'closeColorPicker',
          key: 'foregroundColor',
        },
        window.location.origin,
      );
    }
  });

  foregroundColorPicker.on('clear', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'foregroundColor',
      color: '',
    };
    parent.postMessage(data, window.location.origin);
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'foregroundColor',
      },
      window.location.origin,
    );
  });

  foregroundColorPicker.on('cancel', () => {
    if (foregroundDefaultColor) {
      const data = {
        type: 'colorPickerChange',
        key: 'foregroundColor',
        color: foregroundDefaultColor,
      };
      parent.postMessage(data, window.location.origin);
    } else {
      const data = {
        type: 'colorPickerChange',
        key: 'foregroundColor',
        color: foregroundColorPicker.setColor(''),
      };
      parent.postMessage(data, window.location.origin);
    }
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'foregroundColor',
      },
      window.location.origin,
    );
  });
}

function initiateTextBackgroundColorPickers() {
  textBackgroundColorPicker = window.Pickr.create({
    el: '#loree-text-options-bg-color-picker',
    container: '#loree-text-options-bg-color-picker-wrapper',
    theme: 'nano',
    default: textBackgroundDefaultColor || '#ffffff',
    inline: true,
    showAlways: true,
    useAsButton: true,
    lockOpacity: true,
    defaultRepresentation: 'HEXA',
    components: {
      preview: true,
      opacity: false,
      hue: true,
      interaction: {
        hex: true,
        rgba: false,
        input: false,
        clear: true,
        save: true,
        cancel: true,
      },
    },
    i18n: {
      'btn:save': 'Apply',
      'btn:cancel': 'Cancel',
      'btn:clear': 'Clear',
    },
  });

  textBackgroundColorPicker.on('change', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'textBackgroundColor',
      color: color.toHEXA().toString(),
    };
    parent.postMessage(data, window.location.origin);
  });

  textBackgroundColorPicker.on('save', (color) => {
    if (color) {
      const data = {
        type: 'colorPickerSave',
        key: 'textBackgroundColor',
        color: color.toHEXA().toString(),
      };
      parent.postMessage(data, window.location.origin);
      parent.postMessage(
        {
          type: 'closeColorPicker',
          key: 'textBackgroundColor',
        },
        window.location.origin,
      );
    }
  });

  textBackgroundColorPicker.on('clear', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'textBackgroundColor',
      color: '',
    };
    parent.postMessage(data, window.location.origin);
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'textBackgroundColor',
      },
      window.location.origin,
    );
  });
  textBackgroundColorPicker.on('cancel', () => {
    if (textBackgroundDefaultColor) {
      const data = {
        type: 'colorPickerChange',
        key: 'textBackgroundColor',
        color: textBackgroundDefaultColor,
      };
      parent.postMessage(data, window.location.origin);
    } else {
      const data = {
        type: 'colorPickerChange',
        key: 'textBackgroundColor',
        color: textBackgroundColorPicker.setColor(''),
      };
      parent.postMessage(data, window.location.origin);
    }
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'textBackgroundColor',
      },
      window.location.origin,
    );
  });
}

function initiateBorderColorPickers() {
  borderColorPicker = window.Pickr.create({
    el: '#loree-text-options-border-color-picker',
    container: '#loree-text-options-border-color-picker-wrapper',
    theme: 'nano',
    default: textBorderDefaultColor || '#ffffff',
    defaultRepresentation: 'HEXA',
    inline: true,
    showAlways: true,
    useAsButton: true,
    lockOpacity: true,
    components: {
      preview: true,
      opacity: false,
      hue: true,
      interaction: {
        hex: true,
        rgba: false,
        input: false,
        clear: true,
        save: true,
        cancel: true,
      },
    },
    i18n: {
      'btn:save': 'Apply',
      'btn:cancel': 'Cancel',
      'btn:clear': 'Clear',
    },
  });

  borderColorPicker.on('change', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'borderColor',
      color: color.toHEXA().toString(),
    };
    parent.postMessage(data, window.location.origin);
  });

  borderColorPicker.on('save', (color) => {
    if (color) {
      const data = {
        type: 'colorPickerSave',
        key: 'borderColor',
        color: color.toHEXA().toString(),
      };
      parent.postMessage(data, window.location.origin);
      parent.postMessage(
        {
          type: 'closeColorPicker',
          key: 'borderColor',
        },
        window.location.origin,
      );
    }
  });

  borderColorPicker.on('clear', () => {
    const data = {
      type: 'colorPickerChange',
      key: 'borderColor',
      color: '#FFFFFF',
    };
    parent.postMessage(data, window.location.origin);
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'borderColor',
      },
      window.location.origin,
    );
  });

  borderColorPicker.on('cancel', () => {
    if (textBorderDefaultColor) {
      const data = {
        type: 'colorPickerChange',
        key: 'borderColor',
        color: textBorderDefaultColor,
      };
      parent.postMessage(data, window.location.origin);
    } else {
      const data = {
        type: 'colorPickerChange',
        key: 'borderColor',
        color: borderColorPicker.setColor(textBorderDefaultColor),
      };
      parent.postMessage(data, window.location.origin);
    }
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'borderColor',
      },
      window.location.origin,
    );
  });
}

function initiateBackgroundColorPickers() {
  backgroundColorPicker = window.Pickr.create({
    el: '#loree-row-column-background-color-picker',
    container: '#loree-row-column-background-color-picker-wrapper',
    theme: 'nano',
    default: backgroundDefaultColor || '#ffffff',
    defaultRepresentation: 'HEXA',
    inline: true,
    showAlways: true,
    useAsButton: true,
    lockOpacity: true,
    components: {
      preview: true,
      opacity: false,
      hue: true,
      interaction: {
        hex: true,
        rgba: false,
        input: true,
        clear: true,
        save: true,
        cancel: true,
      },
    },
    i18n: {
      'btn:save': 'Apply',
      'btn:cancel': 'Cancel',
      'btn:clear': 'Clear',
    },
  });

  backgroundColorPicker.on('change', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'backgroundColor',
      color: color.toHEXA().toString(),
    };
    parent.postMessage(data, window.location.origin);
  });

  backgroundColorPicker.on('save', (color) => {
    if (color) {
      const data = {
        type: 'colorPickerChange',
        key: 'backgroundColor',
        color: color.toHEXA().toString(),
      };
      parent.postMessage(data, window.location.origin);
      parent.postMessage(
        {
          type: 'closeColorPicker',
          key: 'backgroundColor',
        },
        window.location.origin,
      );
    }
  });

  backgroundColorPicker.on('clear', () => {
    const data = {
      type: 'colorPickerChange',
      key: 'backgroundColor',
      color: backgroundColorPicker.setColor(''),
    };
    parent.postMessage(data, window.location.origin);

    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'backgroundColor',
      },
      window.location.origin,
    );
  });

  backgroundColorPicker.on('cancel', () => {
    if (backgroundDefaultColor) {
      const data = {
        type: 'colorPickerChange',
        key: 'backgroundColor',
        color: backgroundDefaultColor,
      };
      parent.postMessage(data, window.location.origin);
    } else {
      const data = {
        type: 'colorPickerChange',
        key: 'backgroundColor',
        color: backgroundColorPicker.setColor(''),
      };

      parent.postMessage(data, window.location.origin);
    }
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'backgroundColor',
      },
      window.location.origin,
    );
  });
}

function initiateRowColumnBorderColorPickers() {
  rowColumnBorderColorPicker = window.Pickr.create({
    el: '#loree-row-column-border-color-picker',
    container: '#loree-row-column-border-color-picker-wrapper',
    theme: 'nano',
    default: rowColumnBorderDefaultColor || '#ffffff',
    defaultRepresentation: 'HEXA',
    inline: true,
    showAlways: true,
    useAsButton: true,
    lockOpacity: true,
    components: {
      preview: true,
      opacity: false,
      hue: true,
      interaction: {
        hex: true,
        rgba: false,
        input: true,
        clear: true,
        save: true,
        cancel: true,
      },
    },
    i18n: {
      'btn:save': 'Apply',
      'btn:cancel': 'Cancel',
      'btn:clear': 'Clear',
    },
  });

  rowColumnBorderColorPicker.on('change', (color) => {
    const data = {
      type: 'colorPickerChange',
      key: 'rowColumnBorderColor',
      color: color.toHEXA().toString(),
    };
    parent.postMessage(data, window.location.origin);
  });

  rowColumnBorderColorPicker.on('save', (color) => {
    if (color) {
      const data = {
        type: 'colorPickerChange',
        key: 'rowColumnBorderColor',
        color: color.toHEXA().toString(),
      };
      parent.postMessage(data, window.location.origin);
      parent.postMessage(
        {
          type: 'closeColorPicker',
          key: 'rowColumnBorderColor',
        },
        window.location.origin,
      );
    }
  });

  rowColumnBorderColorPicker.on('clear', () => {
    const data = {
      type: 'colorPickerChange',
      key: 'rowColumnBorderColor',
      color: rowColumnBorderColorPicker.setColor('#FFFFFF'),
    };
    parent.postMessage(data, window.location.origin);

    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'rowColumnBorderColor',
      },
      window.location.origin,
    );
  });

  rowColumnBorderColorPicker.on('cancel', () => {
    if (rowColumnBorderDefaultColor) {
      const data = {
        type: 'colorPickerChange',
        key: 'rowColumnBorderColor',
        color: rowColumnBorderDefaultColor,
      };

      parent.postMessage(data, window.location.origin);
    } else {
      const data = {
        type: 'colorPickerChange',
        key: 'rowColumnBorderColor',
        color: rowColumnBorderColorPicker.setColor('#FFFFFF'),
      };
      parent.postMessage(data, window.location.origin);
    }
    parent.postMessage(
      {
        type: 'closeColorPicker',
        key: 'rowColumnBorderColor',
      },
      window.location.origin,
    );
  });
}

function appendSwatchColors(colorPicker, swatchColor) {
  const colorPickerContainer = colorPicker.options.container;
  const swatchContainer = document.querySelectorAll(`#${colorPickerContainer.id} .pcr-swatches`);
  swatchContainer[0].innerHTML = '';
  if (swatchColor.length > 0) {
    for (let i = 0; i < swatchColor.length; i++) {
      colorPicker.addSwatch(swatchColor[i]);
    }
  }
}

window.addEventListener('message', (event) => {
  if (!isValidHost(event.origin)) {
    return;
  }
  switch (event.data.key) {
    case 'setForeGroundColor':
      appendSwatchColors(foregroundColorPicker, event.data.swatches);
      foregroundDefaultColor = event.data.value;
      if (foregroundColorPicker.isOpen()) {
        foregroundColorPicker.setColor(event.data.value, true);
      } else {
        foregroundColorPicker.on('show', () => {
          foregroundColorPicker.setColor(event.data.value, true);
        });
      }
      break;
    case 'setBackgroundColor':
      appendSwatchColors(textBackgroundColorPicker, event.data.swatches);
      textBackgroundDefaultColor = event.data.value;
      textBackgroundColorPicker.setColor('#ffffff', true);
      if (textBackgroundColorPicker.isOpen()) {
        textBackgroundColorPicker.setColor(event.data.value, true);
      } else {
        textBackgroundColorPicker.on('show', () => {
          textBackgroundColorPicker.setColor(event.data.value, true);
        });
      }
      break;
    case 'setBorderColor':
      appendSwatchColors(borderColorPicker, event.data.swatches);
      textBorderDefaultColor = event.data.value;
      if (borderColorPicker.isOpen()) {
        borderColorPicker.setColor(textBorderDefaultColor, true);
      } else {
        borderColorPicker.on('show', () => {
          borderColorPicker.setColor(textBorderDefaultColor, true);
        });
      }
      break;
    case 'setRowColumnBackgroundColor':
      appendSwatchColors(backgroundColorPicker, event.data.swatches);
      backgroundDefaultColor = event.data.value;
      if (backgroundColorPicker.isOpen()) {
        backgroundColorPicker.setColor(event.data.value, true);
      } else {
        backgroundColorPicker.on('show', () => {
          backgroundColorPicker.setColor(event.data.value, true);
        });
      }
      break;
    case 'setRowColumnBorderColor':
      appendSwatchColors(rowColumnBorderColorPicker, event.data.swatches);
      rowColumnBorderDefaultColor = event.data.value === '' ? '#FFFFFF' : event.data.value;
      if (rowColumnBorderColorPicker.isOpen()) {
        rowColumnBorderColorPicker.setColor(rowColumnBorderDefaultColor, true);
      } else {
        rowColumnBorderColorPicker.on('show', () => {
          rowColumnBorderColorPicker.setColor(rowColumnBorderDefaultColor, true);
        });
      }
      break;
  }
});

function fixColorPickerBehaviour() {
  const pcrSelections = document.querySelectorAll('.pcr-selection');
  const pcrResult = document.querySelectorAll('.pcr-result');
  pcrSelections.forEach((selection) => {
    selection.addEventListener('click', () => {
      pcrResult.forEach((input) => {
        input.blur();
      });
    });
  });
}

(function () {
  initiateForegroundColorPickers();
  initiateTextBackgroundColorPickers();
  initiateBorderColorPickers();
  initiateBackgroundColorPickers();
  initiateRowColumnBorderColorPickers();
  fixColorPickerBehaviour();
})();

window.$('[data-toggle="tooltip"]').tooltip();
window.$('.editor-tooltip').on('click', function () {
  window.$(this).tooltip('hide');
});

// Iframe height auto fit based on the content code...
window.addEventListener(
  'message',
  (event) => isValidHost(event.origin) && receiveMessage(event),
  false,
);
window.addEventListener(
  'message',
  (event) => isValidHost(event.origin) && h5pIframeHeight(event),
  false,
);

window.addEventListener(
  'message',
  (event) => isValidH5PHost(event.origin) && receiveH5PMessage(event),
  false,
);
const validh5pHosts = ['h5p.com'];

function isValidH5PHost(host) {
  return host.includes(validh5pHosts);
}

function receiveH5PMessage(event) {
  const data = JSON.parse(event.data);
  if (!event.data) return;
  const frames = document.getElementsByTagName('iframe');
  Array.from(frames).forEach((frame) => {
    if (!frame.src || frame.contentWindow !== event.source) return;
    frame.style.height = data.height + 'px';
  });
}

function receiveMessage(event) {
  if (
    event.data &&
    event.data.type &&
    event.data.type === 'LOREE_WINDOW_SIZE' &&
    event.data.height
  ) {
    const frames = document.getElementsByTagName('iframe');
    for (const frame of frames) {
      if (
        frame.src &&
        frame.contentWindow === event.source &&
        frame.src.startsWith(event.data.url)
      ) {
        frame.style.height = event.data.height.toString() + 'px';
        if (frame.id.includes('InteractiveCards') || frame.id.includes('flipcard')) {
          frame.scrolling = 'yes';
        } else {
          frame.scrolling = 'no';
        }
      }
    }
  }
}

function h5pIframeHeight(event) {
  const h5pScript = document.createElement('script');
  h5pScript.setAttribute('charset', 'UTF-8');
  h5pScript.setAttribute(
    'src',
    'https://loree-h5p.crystaldelta.net/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js',
  );
  document.body.appendChild(h5pScript);
}
