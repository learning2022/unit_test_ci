# Accessibility Guidelines

There is no magic tool that will be able to check your code and ensure it meets all accessibility standards, as well as providing a good user experience to users with special needs. In the same way there are no automated tools to assess good visual design, it's up to you as the developer to ensure what you are creating will be accessible. When developing any kind of UI work, you should always be thinking about how the UI will be accessible. The easiest way to do this, is to think to yourself: "Can I use this without a mouse, with my eyes closed." This is exactly how assistive technology will process the page.

## General Advice

### Try to use native elements when possible

Try and use the most appropriate [semantic HTML](https://developer.mozilla.org/en-US/docs/Glossary/Semantics#semantics_in_html) for the UI you are developing. There are many built in HTML elements to provide interactivity. Avoid manually adding extraneous `aria` attributes if there is already an existing semantic HTML element that achieves the same purpose. There are far too many to outline in this document but here are some common examples:

#### Buttons/Links

When it comes to clickable items, you should be using a `<button>` or `<a>` element. If it's a link, use `<a>`. If it has an `onClick` handler then use a `<button>`. These two elements are automatically keyboard accessible with the `click` event. Mouse users can click the element, while keyboard/screen reader users can virtually "click" the element by highlighting it and pressing "space bar".

There should always be screen-readable text inside the button. You can do this by using the `loree-visually-hidden` class.

```html
<button>
  <!-- This span will be hidden from screen readers -->
  <span class='some-icon' aria-hidden='true' />

  <!-- This span will only be visible to screen readers -->
  <span class='loree-visually-hidden'>Do something important</span>
</button
```

#### Form Elements

Every single form element should have a label. The easiest way to do this, is to nest the `<input>` element inside a `<label>`. This is particularly useful for checkboxes at it will allow users to click anywhere on the label to check/uncheck the checkbox.

```html
<label>
  <input type='checkbox' />
  <span>Enable the thing</span>
</label
```

See [here](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label#accessibility_concerns) for more examples.

_Side Note:_ Never use the `placeholder` html attribute as a label. Screen readers will often ignore it.

#### Complex UI

Complex UI such as alerts, accordions, dialogs, and tabs require very special considerations around accessibility. In many cases, you can use [Reach UI](https://reach.tech/) to take care of these special cases. It already has built in considerations for accessibility. For UI that is not covered by Reach UI, it will require your own research. You can try finding examples of the UI pattern others have created by performing a web search for "<Name of UI> accessibility html". eg: "tooltip accessibility html". You can also look at the examples in the [Deque Code Library](https://dequeuniversity.com/library/) and use your browser devtools to inspect their provided demos.

## Testing

As you are developing your feature, you should be testing the accessibility. There are a number of tools that you can use.

### Browser Plugins

#### Chrome

- [Axe DevTools Extension](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd?hl=en-US)
- [Lighthouse](https://developers.google.com/web/tools/lighthouse#devtools)
- [Emulate Vision Deficiencies](https://developer.chrome.com/blog/new-in-devtools-83/#vision-deficiencies)

#### Firefox

- [Accessibility Inspector](https://developer.mozilla.org/en-US/docs/Tools/Accessibility_inspector)

### Unit Tests

All unit tests should also include an accessibility check. This is no substitute for real testing in a browser but will provide us with a way to catch some accessibility issues early.

_Side Note:_ Axe will not be able to detect visual issues in unit tests. This is because styles are not applied in the JSDOM environment that Jest uses.

```tsx
import React from 'react';
import { render } from '@testing-library/react';

// Import from this helper to ensure we have all our custom rules
import axe from './src/axeHelper';

import MyComponent from './app';

it('ensures something', async () => {
  const { container } = render(<MyComponent />);
  const axeResults = await axe(container);
  expect(axeResults).toHaveNoViolations();
});
```

### Screen Readers

- [NVDA](https://www.nvaccess.org/about-nvda/) - Windows - Free, open source. Similar controls to JAWS.
- [JAWS](https://www.freedomscientific.com/products/software/jaws/) - Windows - Seems to be favored by western educational institutions that can provide licenses to students.
- [Apple VoiceOver](https://www.apple.com/accessibility/vision/) - MacOS and iOS - Differs in usability from JAWS and NVDA. Only works in Safari.
- [TalkBack](https://support.google.com/accessibility/android/answer/6283677?hl=en) - Android - Most ubiquitous option on Android - built into OS.

## General Development Procedure

1. Assess the feature you are working on and decide if there are any complex user interactions that would require special accessibility consideration.
2. If there are complex interactions, see if you can use any existing components from [Reach UI](https://reach.tech/)
3. Develop the feature.
4. Test in your browser using the [browser plugins](#browser-plugins).
5. Test using a screen reader ([NVDA](https://www.nvaccess.org/about-nvda/) on Windows, [Apple VoiceOver](https://www.apple.com/accessibility/vision/) on MacOS)
6. Ensure your [unit tests](#unit-tests) contain accessibility checks.
