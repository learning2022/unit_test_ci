# What does this MR do?

A few sentences describing the overall goals of the merge request's commits.

# Unit tests?

Unit test details

# How Has This Been Tested?

Please describe the tests that you ran to verify your changes.
Provide instructions so we can reproduce.
Please also list any relevant details for your test configuration.

- [ ] Test A
- [ ] Test B

# JIRA Cards

Closes LOREE-24

# Checklist

- [ ] I have performed a self-review of my own code
- [ ] Ensure code is formatted correctly
- [ ] I have added unit test for my changes (including accessibility)
- [ ] My changes generate no new warnings (including ES-lint)
- [ ] New and existing unit tests pass locally with my changes

# dev-checklist

- [ ] I have ensured my code produces accessible output
- [ ] I have Refactored new code as well as dependent code.
- [ ] I have made corresponding changes to the documentation
- [ ] I have ensured my code produces accessible output
- [ ] I have fixed all ES-lint warnings ([Phase 1](https://crystaldelta.atlassian.net/wiki/spaces/PBUG/pages/edit-v2/2110914756?draftShareId=d9957a5e-a718-42e0-a431-a4756eb30555)).
- [ ] My changes are not breaking existing feature of application.
- [ ] I have wrote separate code for new feature and fixes.
- [ ] Console.log should not be include in the code.
- [ ] My code is following [DRY (Don't Repeat Yourself)](https://www.drycode.io/) concept.
- [ ] DOM action should be keep in separate file.
- [ ] My code is managed as per [loree-rules](https://crystaldelta.atlassian.net/wiki/spaces/PBUG/pages/edit-v2/2110914756?draftShareId=d9957a5e-a718-42e0-a431-a4756eb30555).

# Screenshots

Attach before and after screenshots for UI changes

# Reviews

Please identify two developers to review this change

- [ ] @personA
- [ ] @personB
