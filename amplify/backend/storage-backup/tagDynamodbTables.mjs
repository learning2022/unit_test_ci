import AWS from 'aws-sdk';

const backupStatus = process.argv.slice(2)[0] ? process.argv.slice(2)[0] : 'daily';

const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });

const tables = await dynamodb.listTables({}).promise();
//[tables.TableNames[0]].forEach(async (table) => { // for testing against first table
tables.TableNames.forEach(async table => {
  const tags = {
    ResourceArn: `arn:aws:dynamodb:${process.env.AWS_REGION}:${process.env.AWS_ACCOUNT_ID}:table/${table}`,
  };
  try {
    if (backupStatus === 'untag') {
      tags.TagKeys = ['dynamodb-backup'];
      console.log(tags);
      await dynamodb.untagResource(tags).promise();
      console.log(`Successfully untagged ${table}`);
    } else {
      tags.Tags = [{ Key: 'dynamodb-backup', Value: backupStatus }];
      console.log(tags);
      await dynamodb.tagResource(tags).promise();
      console.log(`Successfully tagged ${table}`);
    }
  } catch (e) {
    console.log(e);
  }
});
