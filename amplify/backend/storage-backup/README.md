## Deploy

```
aws cloudformation deploy --stack-name loree-backup --template-file ./backup.yaml --capabilities CAPABILITY_IAM
# if you want to see progress in detail
./cfwatch --stack-name loree-backup --watch
```

## Update DynamoDB

Two methods

1. Tag everything in Amplify with the dynamodb-backup tag - AppSync doesnt seem to be able to add tags to DynamoDB tables only
2. Use a script to tag what we want to backup on top of Amplify

```sh
# Option 2 approach...

# requires nodejs 14
export AWS_PROFILE=cd-aws-edtech-nonprod
#export AWS_PROFILE=cd-aws-edtech-prod
aws sso login
source set-aws-sso-credentials.sh
#node tagDynamodbTables.mjs disable # for testing in nonprod
node tagDynamodbTables.mjs daily
```
