#!/usr/bin/env bash

# Set strict mode if inside a script.
if [ -n "${BASH_SOURCE[0]:-}" ]; then
	set -euo pipefail
fi

export AWS_REGION="${AWS_REGION:-ap-southeast-2}"

echo "Loading temporary access credentials for AWS profile ${AWS_PROFILE:-default}..."

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

# Figure out temporary credentials.
SSO_ROLE=$(aws sts get-caller-identity --query=Arn | cut -d'_' -f 2)
SSO_ACCOUNT=$(aws sts get-caller-identity --query=Account --output text)
SESSION_FILE=$(find ~/.aws/sso/cache -type f -regex ".*/cache/[a-z0-9]*.json" | head -n 1)
SSO_ACCESS_TOKEN=$(jq -r '.accessToken' "$SESSION_FILE")
CREDENTIALS=$(aws sso get-role-credentials --role-name="$SSO_ROLE" --account-id="$SSO_ACCOUNT" --access-token="$SSO_ACCESS_TOKEN")

# Export temporary credentials
export AWS_ACCOUNT_ID="$(aws sts get-caller-identity --query Account --output text)"
export AWS_ACCESS_KEY_ID=$(echo "$CREDENTIALS" | jq -r '.roleCredentials.accessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo "$CREDENTIALS" | jq -r '.roleCredentials.secretAccessKey')
export AWS_SESSION_TOKEN=$(echo "$CREDENTIALS" | jq -r '.roleCredentials.sessionToken')

echo "AWS_ACCOUNT_ID: ${AWS_ACCOUNT_ID}"
echo "AWS_REGION: ${AWS_REGION}"
echo "AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY: ${AWS_SECRET_ACCESS_KEY:0:10}..."
echo "AWS_SESSION_TOKEN: ${AWS_SESSION_TOKEN:0:20}..."