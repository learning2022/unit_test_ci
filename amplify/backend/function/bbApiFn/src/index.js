/* Amplify Params - DO NOT EDIT
	API_LOREEV2API_GRAPHQLAPIIDOUTPUT
	API_LOREEV2API_LTIACCESSTOKENTABLE_ARN
	API_LOREEV2API_LTIACCESSTOKENTABLE_NAME
	API_LOREEV2API_LTIAPIKEYTABLE_ARN
	API_LOREEV2API_LTIAPIKEYTABLE_NAME
	API_LOREEV2API_LTIPLATFORMTABLE_ARN
	API_LOREEV2API_LTIPLATFORMTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const Contents = require('./Api/Contents');
const RolesManagement = require('./Api/AdminDashboard');
const BbImages = require('./Api/Images');
const BBFilesUpload = require('./Api/fileUploads');

const resolvers = {
  Query: {
    viewBbCourses: ctx => {
      return Contents.viewCourses(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    viewBbContents: ctx => {
      return Contents.viewContents(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    viewBbContentsById: ctx => {
      return Contents.viewContentsById(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.contentId);
    },
    viewBbPageContentsById: ctx => {
      return Contents.viewPageContentsById(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.contentId);
    },
    listBbCourseImages: ctx => {
      return BbImages.bbCourseImages(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    listChildCourseImages: ctx => {
      return BbImages.bbCourseFolderImages(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.resourceId);
    },
    listCourseImagesById: ctx => {
      return BbImages.bbCourseImagesById(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.resourceId);
    },
    fetchInstitutionRoles: ctx => {
      return RolesManagement.institutionRole(ctx.identity.claims["email"]);
    },
    courseEnrollment: ctx => {
      return RolesManagement.getCourseRole(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
  },
  Mutation: {
    updateBbContents: ctx => {
      return Contents.updateContents(
        ctx.identity.claims["email"],
        ctx.arguments.courseId,
        ctx.arguments.contentId,
        ctx.arguments.updateContentData,
      );
    },
    updateUltraContent: ctx => {
      return Contents.updateBBUltaContents(
        ctx.identity.claims["email"],
        ctx.arguments.courseId,
        ctx.arguments.contentId,
        ctx.arguments.updateContentData,
      );
    },
    createBbRootContent: ctx => {
      return Contents.createRootContent(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.contentData);
    },
    createBbChildContent: ctx => {
      return Contents.createChildContent(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.contentId, ctx.arguments.contentData);
    },
    createBbContent: ctx => {
      return Contents.createContent(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.contentData);
    },
    bbCourseImageUpload: ctx => {
      return BbImages.bbImageUpload(
        ctx.identity.claims["email"],
        ctx.arguments.courseId,
        ctx.arguments.file,
        ctx.arguments.fileName,
        ctx.arguments.fileType,
        ctx.arguments.parentId,
      );
    },
    uploadBBUltraFileToS3: ctx => {
      return BBFilesUpload.uploadBBFiles(ctx.arguments.platformId, ctx.arguments.file, ctx.arguments.title, ctx.arguments.newTitle);
    },
  },
};

exports.handler = async event => {
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      return await resolver(event);
    }
  }
  throw new Error('Resolver not found.');
};
