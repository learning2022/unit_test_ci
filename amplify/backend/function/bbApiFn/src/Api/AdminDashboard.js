const Request = require('./Request');
const Database = require('../Database');

class RolesManagement {
  async institutionRole(loreeUserEmail) {
    const endPoint = 'learn/api/public/v1/courseRoles';
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }
  async getCourseRole(loreeUserEmail, courseId) {
    const userDetails = await Database.queryLtiAccessTokenByEmail(loreeUserEmail);
    const endPoint = `learn/api/public/v1/courses/${courseId}/users/${userDetails.user}`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }
}

module.exports = new RolesManagement();
