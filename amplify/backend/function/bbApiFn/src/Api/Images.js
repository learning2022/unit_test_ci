const Request = require('./Request');

class BbImages {
  async bbCourseImages(loreeUserEmail, courseId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/resources`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async bbCourseFolderImages(loreeUserEmail, courseId, resourceId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/resources/${resourceId}/children`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }
  async bbCourseImagesById(loreeUserEmail, courseId, resourceId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/resources/${resourceId}`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }
  async bbImageUpload(loreeUserEmail, courseId, file, fileName, fileType, parentId) {
    const endPoint = `learn/api/v1/files?expand=mimeType,isMedia&courseId=${courseId}`;
    const base64Data = Buffer.from(file.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    const imageData = await Request.axiosImageRequest(endPoint, 'post', base64Data, fileName, loreeUserEmail, fileType);
    const uploadedResponse = await this.uploadBbFile(loreeUserEmail, courseId, imageData, parentId, fileName);
    await this.deleteBbFile(loreeUserEmail, courseId, uploadedResponse.body.id);
    return uploadedResponse;
  }

  async uploadBbFile(loreeUserEmail, courseId, data, parentId, fileName) {
    const endPoint = `/learn/api/v1/courses/${courseId}/contents`;
    const reqData = {
      courseId: courseId,
      parentId: parentId,
      isAvailable: false,
      contentHandler: 'resource/x-bb-file',
      title: fileName,
      contentDetail: { 'resource/x-bb-file': { file: JSON.parse(data) } },
    };
    return await Request.axiosRequest(endPoint, 'post', reqData, loreeUserEmail);
  }

  async deleteBbFile(loreeUserEmail, courseId, contentId) {
    const endPoint = `/learn/api/v1/courses/${courseId}/contents/${contentId}`;
    return await Request.axiosRequest(endPoint, 'delete', {}, loreeUserEmail);
  }
}

module.exports = new BbImages();
