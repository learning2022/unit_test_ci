const aws = require('aws-sdk');
const s3 = new aws.S3();

class BBFilesUpload {
  //Temporary function for BB file upload handling(For demo purpose),
  // will change it as BB file  API's,once we get clear API from BB.
  s3Upload = async (param, platformId, title) => {
    try {
      await s3.putObject(param).promise();
      const imageUrl = `${process.env.IMAGE_S3_URL}/${platformId}/bb-ultra-content/${title}`;
      return imageUrl;
    } catch (err) {
      console.log('Error in image upload', err);
      return '';
    }
  };

  checkFileExist = async key => {
    try {
      const checkImage = await s3.headObject(key).promise();
      if (checkImage) {
        return true;
      }
    } catch (err) {
      console.log('Image not exits', err);
      return false;
    }
  };

  uploadBBFiles = async (platformId, file, title, newTitle) => {
    try {
      let fileKey = {
        Bucket: process.env.BUCKET_NAME,
        Key: `${platformId}/bb-ultra-content/${title}`,
      };
      let param = {
        ...fileKey,
        Body: file,
        ContentType: 'text/html ',
        ACL: 'public-read',
      };
      let imageNameCheck = await this.checkFileExist(fileKey);
      if (imageNameCheck) {
        param.Key = `${platformId}/bb-ultra-content/${newTitle}`;
        return this.s3Upload(param, platformId, newTitle);
      } else {
        return this.s3Upload(param, platformId, title);
      }
    } catch (e) {
      console.log('Error in uploading image to S3', e);
    }
  };
}

module.exports = new BBFilesUpload();
