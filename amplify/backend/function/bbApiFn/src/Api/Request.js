const axios = require('axios');
const request = require('request-promise');
const Database = require('../Database');
const qs = require('qs');

class Request {
  async getAccessToken(loreeUserEmail) {
    const accessToken = await Database.queryLtiAccessTokenByEmail(loreeUserEmail);
    return accessToken;
  }

  async CheckTokenValidity(userAccessToken) {
    let refresh_token = userAccessToken.refreshToken;
    let currentTime = new Date().toISOString();
    const getApiKeyData = await Database.getApiKeyData(userAccessToken.ltiApiKeyID);
    let generateNewtoken = await this.refreshDBToken(refresh_token, getApiKeyData);
    console.log('Token refreshed', generateNewtoken);
    if (generateNewtoken) {
      await Database.updateLtiAccessToken(
        userAccessToken.id,
        generateNewtoken.data.access_token,
        currentTime,
        generateNewtoken.data.refresh_token,
      );
      return generateNewtoken.data.access_token;
    } else {
      return false;
    }
  }

  refreshDBToken = async (refresh_token, apiKeyData) => {
    //get refreshed access token
    const formData = qs.stringify({
      grant_type: 'refresh_token',
      refresh_token: refresh_token,
    });
    const basicAuthToken = Buffer.from(apiKeyData.apiClientId + ':' + apiKeyData.apiSecretKey).toString('base64');

    const refreshParams = {
      method: 'post',
      url: apiKeyData.oauthTokenUrl,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${basicAuthToken}`,
      },
      data: formData,
    };

    let tokenResponse = await axios(refreshParams);
    return tokenResponse;
  };

  async axiosRequest(url, method, postData, loreeUserEmail) {
    console.log(`TOKKEN ID--> axios ${loreeUserEmail}- ${postData} - ${method} - ${url}`);
    const canvasData = await this.getAccessToken(loreeUserEmail);
    const options = {
      method: method,
      url: `${canvasData.lmsApiUrl}/${url}`,
      headers: {
        Authorization: `Bearer ${canvasData.accessToken}`,
        'Content-Type': 'application/json',
      },
      data: postData,
    };
    console.log('Axios Reqeust Options', options);
    try {
      const response = await axios.request(options);
      console.log('Axios Response', response);
      const responseData = {
        statusCode: response.status,
        body: response.data,
      };
      return responseData;
    } catch (err) {
      console.log('axios error', err);
      if (err.response.status === 401) {
        return this.axiosRefreshRequest(err, canvasData);
      } else {
        const responseData = {
          statusCode: err.response.status,
          body: err.response.data,
        };
        return responseData;
      }
    }
  }

  async axiosBBUltraRequest(url, method, postData, tokenId, pageID) {
    console.log(`TOKKEN ID--> axios ${tokenId}- ${postData} - ${method} - ${url} - ${pageID}`);
    const canvasData = await this.getAccessToken(tokenId);

    var data = {
      body: {
        rawText: `<!-- {"bbMLEditorVersion":1} --><a data-bbid="bbml-editor-id_${postData.uuid}" data-bbfile="{&quot;linkName&quot;:&quot;${postData.fileName}&quot;}" data-bbtype="embedded-unsafe-html" href=${postData.fileUrl}></a>`,
        displayText: '',
        webLocation: `https://crystaldelta-eval.blackboard.com/courses/1/L02/content/${pageID}/embedded/`,
      },
    };

    const options = {
      method: method,
      url: `${canvasData.lmsApiUrl}/${url}`,
      headers: {
        Authorization: `Bearer ${canvasData.accessToken}`,
        'Content-Type': 'application/json',
      },
      data: data,
    };

    console.log('Axios Reqeust Options', options);
    try {
      const response = await axios(options);
      console.log('Axios Response', response);
      const responseData = {
        statusCode: response.status,
        body: response.data,
      };
      return responseData;
    } catch (err) {
      console.log('axios error', err);
      if (err.response.status === 401) {
        return this.axiosRefreshRequest(err, canvasData);
      } else {
        const responseData = {
          statusCode: err.response.status,
          body: err.response.data,
        };
        return responseData;
      }
    }
  }

  async axiosImageRequest(url, method, file, fileName, tokenId, fileType) {
    console.log(`TOKKEN ID--> axios ${tokenId}- ${file}- ${fileName} - ${method} - ${url}`);
    const canvasData = await this.getAccessToken(tokenId);
    const fileDetail = {
      filename: fileName,
      contentType: fileType,
    };
    const options = {
      method: method,
      url: `${canvasData.lmsApiUrl}/${url}`,
      headers: {
        Authorization: `Bearer ${canvasData.accessToken}`,
      },
      formData: {
        firstFile: {
          value: file,
          options: fileDetail,
        },
      },
    };
    console.log('Reqeust Options', options);
    try {
      return request(options, function (error, response) {
        if (error) console.log(error);
        console.log(response.body);
      });
    } catch (err) {
      console.log('error', err);
      if (err.response.status === 401) {
        return this.axiosRefreshRequest(err, canvasData);
      } else {
        const responseData = {
          statusCode: err.response.status,
          body: err.response.data,
        };
        return responseData;
      }
    }
  }
  async axiosRefreshRequest(err, token) {
    const refreshToken = await this.CheckTokenValidity(token);
    if (refreshToken) {
      console.log('Inside refresh token', refreshToken);
      err.response.config.headers['Authorization'] = 'Bearer ' + refreshToken;
      try {
        console.log('Error response API', err.response.config);
        const apiResponse = await axios(err.response.config);
        const responseData = {
          statusCode: apiResponse.status,
          body: apiResponse.data,
        };
        return responseData;
      } catch (error) {
        console.log('Error response API failed', error);
        const responseData = {
          statusCode: error.response.status,
          body: error.response.data,
        };
        return responseData;
      }
    } else {
      const responseData = {
        statusCode: 401,
        body: 'Unauthorized',
      };
      return responseData;
    }
  }
}

module.exports = new Request();
