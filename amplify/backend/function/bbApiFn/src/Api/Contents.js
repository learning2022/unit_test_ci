const Request = require('./Request');

class Contents {
  async viewCourses(loreeUserEmail, courseId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async viewContents(loreeUserEmail, courseId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async viewContentsById(loreeUserEmail, courseId, contentId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents/${contentId}/children`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async viewPageContentsById(loreeUserEmail, courseId, contentId) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents/${contentId}`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async updateContents(loreeUserEmail, courseId, contentId, updateContentData) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents/${contentId}`;
    const data = updateContentData;
    return await Request.axiosRequest(endPoint, 'patch', data, loreeUserEmail);
  }

  async updateBBUltaContents(loreeUserEmail, courseId, contentId, updateContentData) {
    const endPoint = `learn/api/v1/courses/${courseId}/contents/${contentId}`;
    const data = updateContentData;
    return await Request.axiosBBUltraRequest(endPoint, 'patch', data, loreeUserEmail, contentId);
  }

  async createRootContent(loreeUserEmail, courseId, contentData) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents`;
    console.log(contentData);
    const data = contentData;
    return await Request.axiosRequest(endPoint, 'post', data, loreeUserEmail);
  }

  async createContent(loreeUserEmail, courseId, contentId, contentData) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents/${contentId}`;
    const data = contentData;
    return await Request.axiosRequest(endPoint, 'post', data, loreeUserEmail);
  }

  async createChildContent(loreeUserEmail, courseId, contentId, contentData) {
    const endPoint = `learn/api/public/v1/courses/${courseId}/contents/${contentId}/children`;
    const data = contentData;
    return await Request.axiosRequest(endPoint, 'post', data, loreeUserEmail);
  }
}

module.exports = new Contents();
