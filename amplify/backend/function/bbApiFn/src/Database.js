const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

class Database {
  async queryLtiAccessToken(id) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      Key: {
        id: id,
      },
    };

    try {
      const data = await docClient.get(params).promise();
      console.log('Query LtiAccessToken. Data.items :', data, data.Item);
      return data.Item;
    } catch (err) {
      console.error('Error in query LtiAccessToken. Error:', err);
      return err;
    }
  }

  async queryLtiAccessTokenByEmail(email) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      IndexName: 'byEmail',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': email },
      ExpressionAttributeNames: { '#name': 'loreeUsername' },
    };
    try {
      const data = await docClient.query(params).promise();
      return data.Items[0];
    } catch (err) {
      return err;
    }
    
  }

  async getApiKeyData(ltiApiKeyID) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIAPIKEYTABLE_NAME,
      Key: {
        id: ltiApiKeyID,
      },
    };

    try {
      const data = await docClient.get(params).promise();
      console.log('Query api key. Data.items :', data.Item);
      return data.Item;
    } catch (err) {
      console.error('Error in query lit api key. Error:', err);
      return err;
    }
  }

  async updateLtiAccessToken(id, token, dateGenerated, refresh_token) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      Key: {
        id: id,
      },
      UpdateExpression: 'set #access_token = :tokenValue, #dateGen = :dateGenerated, #refresh_token = :refresh_token',
      ExpressionAttributeNames: {
        '#access_token': 'accessToken',
        '#dateGen': 'generatedAt',
        '#refresh_token': 'refreshToken',
      },
      ExpressionAttributeValues: {
        ':tokenValue': token,
        ':dateGenerated': dateGenerated,
        ':refresh_token': refresh_token,
      },
    };
    try {
      await docClient.update(params).promise();
    } catch (err) {
      console.error('Error in updating accessToken. Error:', err);
      return err;
    }
  }

  async queryLtiPlatform(id) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMTABLE_NAME,
      Key: {
        id: id,
      },
    };
    return this.docRequest(params);
  }

  async queryLtiApiKey(platformId) {
    var params = {
      TableName: process.env.API_LOREEV2API_LTIAPIKEYTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };
    const data = await docClient.query(params).promise();
    console.log('Query queryLtiApiKey Data.items :', data.Items);
    return data.Items[0];
  }

  async queryLtiPlatformKey(platformId) {
    var params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };

    try {
      const data = await docClient.query(params).promise();
      console.log('Query LtiApiKey. Data.items :', data.Items);
      return data.Items[0];
    } catch (err) {
      console.error('Error in query LtiApiKey. Error:', err);
      return err;
    }
  }

  async updateLtiPlatformKey(keyid, kid, privatekey, publicKey) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME,
      Key: {
        id: keyid,
      },
      UpdateExpression: 'set #kid = :kid, #privatekey = :privatekey, #publicKey = :publicKey',
      ExpressionAttributeNames: {
        '#kid': 'kid',
        '#privatekey': 'privatekey',
        '#publicKey': 'publicKey',
      },
      ExpressionAttributeValues: {
        ':kid': kid,
        ':privatekey': privatekey,
        ':publicKey': publicKey,
      },
    };
    try {
      console.log('updated LtiPlatformKey. updateLtiPlatformKey:');
      return await docClient.update(params).promise();
    } catch (err) {
      console.error('Error in updateLtiPlatformKey. Error:', err);
      return err;
    }
  }

  async docRequest(params) {
    try {
      const data = await docClient.get(params).promise();
      console.log('Query LtiAccessToken. Data.items :', data, data.Item);
      return data.Item;
    } catch (err) {
      console.error('Error in query LtiAccessToken. Error:', err);
      return err;
    }
  }
}

module.exports = new Database();
