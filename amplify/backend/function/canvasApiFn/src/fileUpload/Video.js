/* eslint-disable */
const kaltura = require('kaltura-client');
const Database = require('../Database');
//Kaltura configuration
const config = new kaltura.Configuration();
config.serviceUrl = 'https://www.kaltura.com';
const client = new kaltura.Client(config);

class Video {
  async getKalturaConfig(id) {
    const configDetails = await Database.queryKalturaConfigDetails(id);
    return configDetails;
  }

  list = async (userId, platformId) => {
    try {
      const type = kaltura.enums.SessionType.ADMIN;
      const expiry = '';
      const privileges = null;
      let kalturaConfig = await this.getKalturaConfig(platformId);
      let collection = await this.fetchKalturaMedia(
        kalturaConfig.tokenID,
        kalturaConfig.appToken,
        type,
        kalturaConfig.partnerID,
        userId,
        expiry,
        privileges,
      );
      console.log('Kaltura Response', collection);
      let perspective = await this.processMedia(collection);
      console.log('Kaltura Response', perspective);
      return perspective;
    } catch (err) {
      console.log('Kaltura Response failed', err);
      return [];
    }
  };

  get = async (userId, entryId, platformId) => {
    try {
      const type = kaltura.enums.SessionType.ADMIN;
      const expiry = '';
      const privileges = null;
      let kalturaConfig = await this.getKalturaConfig(platformId);
      let collection = await this.fetchKalturaVideo(
        kalturaConfig.tokenID,
        kalturaConfig.appToken,
        type,
        kalturaConfig.partnerID,
        userId,
        entryId,
        expiry,
        privileges,
      );
      console.log(collection);
      return collection;
    } catch (err) {
      console.log('Kaltura Response failed', err);
      return [];
    }
  };

  upload = async (userId, path, name, platformId) => {
    try {
      const type = kaltura.enums.SessionType.ADMIN;
      const userID = userId;
      const expiry = '';
      const privileges = null;
      let kalturaConfig = await this.getKalturaConfig(platformId);
      let result = await this.uploadMedia(
        path,
        name,
        kalturaConfig.tokenID,
        kalturaConfig.appToken,
        userID,
        type,
        kalturaConfig.partnerID,
        expiry,
        privileges,
      );
      console.log('Canvas Response', result);
      return result;
    } catch (err) {
      console.log('Canvas Response failed', err);
      return err;
    }
  };

  //initate kaltura media fetch
  fetchKalturaVideo = async (tokenID, appToken, type, partnerID, userID, entryId, expiry, privileges) => {
    try {
      const startSessionId = await this.startWidgetSession(partnerID);
      const tokenHash = await this.appToken_hashValue(appToken, startSessionId);
      await this.session_start(tokenID, tokenHash, userID, type, expiry, privileges);
      const listmediaData = await this.fetchVideo(entryId);
      return listmediaData;
    } catch (e) {
      console.error('Kaltura fetch error');
    }
  };

  // Fetch Video
  fetchVideo = async entryId => {
    console.log('START FETCH MEDIA');
    let version = -1;
    let resObj = '';
    const videoResponse = await kaltura.services.media.get(entryId, version).execute(client);
    console.log(videoResponse);
    resObj = {
      id: videoResponse.id,
      title: videoResponse.name,
      dataUrl: videoResponse.dataUrl,
      thumbnailUrl: videoResponse.thumbnailUrl,
      createdAt: videoResponse.createdAt,
      status: videoResponse.status.toString(),
    };
    return resObj;
  };

  //initate kaltura media fetch
  fetchKalturaMedia = async (tokenID, appToken, type, partnerID, userID, expiry, privileges) => {
    try {
      const startSessionId = await this.startWidgetSession(partnerID);
      const tokenHash = await this.appToken_hashValue(appToken, startSessionId);
      await this.session_start(tokenID, tokenHash, userID, type, expiry, privileges);
      const listmediaData = await this.fetchMedia(userID);
      return listmediaData;
    } catch (e) {
      console.error('Kaltura fetch error');
    }
  };

  // Fetch Media
  fetchMedia = async userID => {
    console.log('START FETCH MEDIA');
    const filter = new kaltura.objects.MediaEntryFilter();
    filter.userIdEqual = userID;
    const pager = new kaltura.objects.FilterPager();
    const resultFiles = await kaltura.services.media.listAction(filter, pager).execute(client);
    return resultFiles;
  };

  // Inserting new video in to the Collection
  processMedia = async collection => {
    const mediaList = collection.objects;
    if (mediaList) {
      let perspectivesArray = [];
      await mediaList.map(function (item) {
        perspectivesArray.push({
          id: item.id,
          title: item.name,
          dataUrl: item.dataUrl,
          thumbnailUrl: item.thumbnailUrl,
          createdAt: item.createdAt,
          status: item.status,
        });
      });
      return perspectivesArray;
    } else {
      return [];
    }
  };

  //Intializing Upload
  uploadMedia = async (filePath, fileName, tokenID, appToken, userID, type, partnerID, expiry, privileges) => {
    try {
      const startSessionId = await this.startWidgetSession(partnerID);
      console.log('KALTURA WIDGET SESSION', startSessionId);
      const tokenHash = await this.appToken_hashValue(appToken, startSessionId);
      console.log('KALTURA TOKEN HASH', tokenHash);
      await this.session_start(tokenID, tokenHash, userID, type, expiry, privileges);
      const mediaData = await this.media_add(filePath, fileName, userID);
      console.log('KALTURA TOKEN HASH', mediaData);
      return mediaData;
    } catch (e) {
      console.error('Kaltura upload error');
    }
  };

  // Widget Session Start
  startWidgetSession = async partnerID => {
    console.log('START WIDGET SESSION');
    const widgetId = '_' + partnerID;
    const expiry = 86400;
    const widgetSession = await kaltura.services.session.startWidgetSession(widgetId, expiry).execute(client);
    console.log('WIDGET SESSION', widgetSession.ks);
    client.setKs(widgetSession.ks);
    return widgetSession.ks;
  };

  // creating hash value
  appToken_hashValue = async (appToken, widgetSession) => {
    console.log('START appToken_hashValue');
    var crypto = require('crypto');
    var shasum = await crypto.createHash('sha256');
    shasum.update(widgetSession + appToken);
    var hash = shasum.digest('hex');
    console.log('appToken_hashValue', hash);
    return hash;
  };

  // App Token Session Start
  session_start = (tokenID, tokenHash, userId, type, expiry, sessionPrivileges) => {
    console.log('START KALTURA SESSION');
    return new Promise((resolve, reject) => {
      kaltura.services.appToken
        .startSession(tokenID, tokenHash, userId, type, expiry, sessionPrivileges)
        .completion((success, ks) => {
          if (!success) {
            reject(ks.message);
            return;
          }
          client.setKs(ks.ks);
          console.log('KALTURA SESSION', ks.ks);
          resolve();
        })
        .execute(client);
    });
  };

  // Upload method
  media_add = async (filePath, fileName, userID) => {
    let resObj = '';

    let uploadToken = new kaltura.objects.UploadToken();

    const token = await kaltura.services.uploadToken.add(uploadToken).execute(client);

    let uploadTokenId = token.id;
    // let fileData = filePath;
    // let resume = false;
    // let finalChunk = true;
    // let resumeAt = -1;

    // await kaltura.services.uploadToken.upload(uploadTokenId, fileData, resume, finalChunk, resumeAt, userID).execute(client);
    let mediaEntry = new kaltura.objects.MediaEntry();
    mediaEntry.name = fileName;
    mediaEntry.description = `Video file - ${fileName}`;
    mediaEntry.mediaType = kaltura.enums.MediaType.VIDEO;
    mediaEntry.creatorId = userID;
    mediaEntry.userId = userID;

    const entry = await kaltura.services.media.add(mediaEntry).execute(client);
    let entryId = entry.id;
    let resource = new kaltura.objects.UrlResource();
    resource.url = filePath;
    resource.token = uploadTokenId;

    const finalResult = await kaltura.services.media.addContent(entryId, resource).execute(client);
    resObj = {
      id: finalResult.id,
      title: finalResult.name,
      dataUrl: finalResult.dataUrl,
      thumbnailUrl: finalResult.thumbnailUrl,
      createdAt: finalResult.createdAt,
      status: finalResult.status.toString(),
    };
    return resObj;
  };
}

module.exports = new Video();
