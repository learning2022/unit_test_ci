/* eslint-disable */
const request = require('request-promise');
const AWSXRay = require('aws-xray-sdk');
const Database = require('../Database')
const aws = AWSXRay.captureAWS(require('aws-sdk'));
const s3 = new aws.S3();
const { getS3Key, getSignedUrl, getBufferedContent, isMemoryRanOutOfHeap } = require('../helpers/utils');
const detectLimit = require('async/detectLimit');
const logger = require('../service/logger').logger;
const axios = require('axios');

const addImagesToCanvas = async uploadData => {
  try{
    let imageType = '';
    logger.info("Upload image file extension: " + uploadData.name.split('.')[1]);
    uploadData.type === 'image' ? (imageType = 'image/' + uploadData.name.split('.')[1]) : (imageType = uploadData.type);
    logger.info("Fetching Image type extension: " + imageType);
    const file = {
      type: `${imageType}`,
      name: `${decodeURI(uploadData.name)}`,
      path: `${uploadData.path}`,
    };
    const canvasData = await Database.queryLtiAccessTokenByEmail(uploadData.loreeUserEmail);
    logger.info('image upload =>' + canvasData);
    if (file.type.includes('image')) {

      const searchFileInCanvasOptions = {
        fileName: file.name,
        fileType: file.type,
        s3ImageUrl: file.path,
        courseId: uploadData.courseId,
        canvasData: canvasData
      }
      const searchCanvasResult = await searchFileinCanvasStorage(searchFileInCanvasOptions);
      logger.info("Search result: " + searchCanvasResult);

      if(!searchCanvasResult) {
        const fileUploadresponse = await uploadFileToCanvas(file.name, file.type, file.path, uploadData.courseId, canvasData);
        logger.info('uploadFiles response' + fileUploadresponse);
        if (fileUploadresponse.statusCode === 200) {
          let fileUploadDetailsResponse = await getUploadFileDetails(fileUploadresponse.body, file.path, file.type);
          logger.info('getUploadDetails response' + fileUploadDetailsResponse.body);
          if (fileUploadDetailsResponse.statusCode >= 300 && fileUploadDetailsResponse.statusCode < 400) {
            const uploadFileResponse = await uploadIntoFiles(fileUploadDetailsResponse);
            return uploadFileResponse;
          } else {
            fileUploadDetailsResponse = JSON.parse(fileUploadDetailsResponse.body);
            return fileUploadDetailsResponse;
          }
        } else {
          return false;
        }
      } else {
        const response = {
          url: searchCanvasResult
        };
        return response;
      }

    } else {
      return false;
    }
  }
  catch(e){
    logger.error("Error in adding to canvas: " + e);
    throw new Error("Error in uploading to canvas");
  }
};

const searchFileinCanvasStorage = async searchFileInCanvasOptions => {
  try {
    let canvasUrl = '';
    const BUCKETNAME = process.env.STORAGE_LOREEV2S35B8B11F1_BUCKETNAME;
    const {fileName, fileType, courseId, s3ImageUrl, canvasData} = searchFileInCanvasOptions;

    const imageKey = getS3Key(s3ImageUrl);
    const s3ImageFilePath = await getSignedUrl(decodeURI(imageKey), BUCKETNAME);
    logger.info("S3 Image file path: " + s3ImageFilePath);

    const s3ImageBufferedData = await getBufferedContent(s3ImageFilePath);
    if(isMemoryRanOutOfHeap()) {
      return '';
    }

    let fileDataList = await getFileDataFromCanvas(fileName, fileType, courseId, canvasData);
    fileDataList = fileDataList.filter(fileData => decodeURI(fileData.filename.replace(/\+/g, ' ')) === fileName);
    await detectLimit(fileDataList, 10, async function(fileData, callback){
      const canvasS3Url = await getCanvasGeneratedS3Url(fileData.id, canvasData);
      const canvasFileBufferedData = await getBufferedContent(canvasS3Url.public_url);
      if(isMemoryRanOutOfHeap()) {
        return '';
      }  
      if(Buffer.compare(canvasFileBufferedData, s3ImageBufferedData) === 0){
        canvasUrl = fileData.url;
        return true;
      }
    }).then(result => {
      canvasUrl = result ? result.url : '';
    }).catch(err => {
      logger.error('File comparison cannot be processed: ' + err);
      throw new Error('unable to perform file comparison');
    });
    return canvasUrl;
  } catch (e) {
    logger.error('File checking cannot be processed: ' + e);
    throw new Error('unable to perform file checking');
  }
};

const getFileDataFromCanvas = async(fileName, fileType, courseId, canvasData) => {
  try{
    const searchImageInCanvasOption = {
      method: 'GET',
      url: `${canvasData.lmsApiUrl}/api/v1/courses/${courseId}/files`,
      data: {
        search_term: `${fileName.split('.')[0]}`,
        content_types: `${fileType}`,
      },
      headers: {
        authorization: `Bearer ${canvasData.accessToken}`,
      },
      simple: false,
      resolveWithFullResponse: true,
    };
    const canvasFileData = await axios(searchImageInCanvasOption);
    return canvasFileData.data;
  } catch(err) {
    logger.error('getFileDataFromCanvas is not processing: ' + e);
    throw new Error('unable to perform getFileDataFromCanvas function');
  }
}

const getCanvasGeneratedS3Url = async(fileId, canvasData) => {
  try {
    const getCanvasS3UrlOption = {
      method: 'GET',
      url: `${canvasData.lmsApiUrl}/api/v1/files/${fileId}/public_url/`,
      headers: {
        authorization: `Bearer ${canvasData.accessToken}`,
      },
      simple: false,
      resolveWithFullResponse: true,
    };
    const getCanvasPublicUrl = await axios(getCanvasS3UrlOption);
    return getCanvasPublicUrl.data;
  } catch (e) {
    logger.error('Error in generating canvasS3 url ' + e);
    throw new Error('unable to generate canvas S3 url');
  }
};

const uploadFileToCanvas = async (name, type, path, courseId, canvasData) => {
  try {
    const uploadFileRequestOptions = {
      method: 'POST',
      url: `${canvasData.lmsApiUrl}/api/v1/courses/${courseId}/files`,
      formData: {
        name: `${name}`,
        size: '10000000',
        content_type: `${type}`,
        parent_folder_path: 'Images',
        url: path,
        on_duplicate: 'rename',
      },
      headers: {
        authorization: `Bearer ${canvasData.accessToken}`,
      },
      simple: false,
      resolveWithFullResponse: true,
    };
    return request(uploadFileRequestOptions);
  } catch (e) {
    logger.error('Files cannot be uploaded ' + e);
    throw new Error('unable to upload');
  }
};

const getUploadFileDetails = async (response, path, fileType) => {
  try {
    response = JSON.parse(response);
    response.upload_url.toString();
    const data = {
      filename: response.upload_params.filename,
      content_type: `${fileType}`,
      target_url: response.upload_params.target_url,
    };
    const getUploadFileRequestOptions = {
      method: 'POST',
      url: response.upload_url,
      headers: {
        'content-type': `${fileType}`,
      },
      formData: data,
      resolveWithFullResponse: true,
      simple: false,
      followRedirect: false,
    };
    return request(getUploadFileRequestOptions);
  } catch (e) {
    logger.error('Files details not found ' + e);
    throw new Error('unable to get image details');
  }
};

const uploadIntoFiles = async response => {
  const uploadUrl = response.caseless.dict.location;
  const options = {
    method: 'POST',
    url: uploadUrl,
  };
  return request(options)
    .then(function (response) {
      return JSON.parse(response);
    })
    .catch(function (err) {
      logger.error(`error upload into files: ` + err);
      throw new Error('unable to upload files');
    });
};

/** Save to templates workflow */
const uploadToS3 = async (platformId, path, title, type) => {
  logger.info('PlatformId:' + platformId);
  logger.info('Image URL:' + path);
  logger.info('Title:' + title);
  const options = {
    uri: path,
    encoding: null,
  };

  const body = await request(options);
  const uploadResult = await s3
    .upload({
      Bucket: process.env.STORAGE_LOREEV2S35B8B11F1_BUCKETNAME,
      Key: `public/${platformId}/${type}/${title}`,
      Body: body,
    })
    .promise();
  logger.info("Uploaded S3:" + uploadResult);
  return `${platformId}/${type}/${title}`;
};

module.exports = {
  addImagesToCanvas,
  searchFileinCanvasStorage,
  uploadToS3,
  uploadIntoFiles
};