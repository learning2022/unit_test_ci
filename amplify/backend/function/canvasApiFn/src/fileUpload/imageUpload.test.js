const Request = require('../Api/Request');
const nock = require('nock');
const { addImagesToS3BucketAndUpdateHtml } = require('../service/imageHandling');
const { uploadToS3 } = require('./ImageUpload');
const Database = require('../Database');
const { globalHtmlContentWithCourseAndFileId, globalHtmlContentWithFileId, globalHtmlContentWithMixedImagesInSingleColumn, saveAsCustomBlocksWithMixedContentInSingleColumn } = require('./imageHandlingMockData');

jest.mock('../fileUpload/ImageUpload', () => ({
  uploadToS3: jest.fn(),
}));

describe('Save to blocks process', () => {
  const saveAsTemplatesResultMockData = '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><h3 class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: Lato; font-size: 46px;">Hal Jordan GL<br></h3></div></div><div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 loree-iframe-content-column" style="padding:10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="Green lantern" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/dummyFileName.jpg" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/875/files/373522" data-api-returntype="File"></div></div><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/dummyFileName.jpg" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/875/files/373523" data-api-returntype="File"></div></div></div><div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper "></div></div></div>';
  const saveAsTemplatesResultMixedContentMockData = '<div class="loree-iframe-content-row row loree-style-0c3e1c"> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-407746"> <div class="loree-iframe-content-image-wrapper loree-style-a539ce"><img alt="" class="loree-iframe-content-image loree-style-862c96" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/Global-Elements/-1641645535907?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Credential=ASIATHBL7ES3DXNJHXZJ%2F20220412%2Fap-southeast-2%2Fs3%2Faws4_request&amp;X-Amz-Date=20220412T112146Z&amp;X-Amz-Expires=900&amp;X-Amz-Security-Token=IQoJb3JpZ2luX2VjEJv%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDmFwLXNvdXRoZWFzdC0yIkcwRQIhALhfFmyJbOlB8Peshu9qrszxQasQddZq18gfrmp9HJuoAiA5fJGGM1Z9ysczGaX3ir%2Bjl9DXD6HMDy2v2fwPu6dlHCriBAhEEAMaDDIyMTI4MzAzNDI5NCIM5%2BLU2%2BKgAQXfgjeyKr8EV5Bq80p6NfkxrlQem4nSo5uIQlpyNwR0jI8EwgdsCnzmSAx6Te78QHMXQmXQNXXaXeyeOxPB1LdIaLBbBzUi8QF6GvJxgtErtYVTiBYBSKAX55%2FM%2Fid7ZUTHVTNXTwlC6GdSfjzjO8%2FIftFSWfE0t8Kcnqf6zo%2Fyxiu06swbrmUY%2FSc60tP278Er0CtBYTik0Iy9qykU%2Fcw15oP%2B4nb0qaQH%2F7Fp1y%2Bc5XyYikN69gNXIGcn9tKP%2FWGinCaPkEd%2F1oGthmwVpznOCHxSXcVLts880ySxJKqOUnS8TGr8vbakm2eX%2ByF051L0aheui%2Fr3vfevyMlANrR9Oy62g5NiGFdK6Hy3QbPgqA8%2Fcsx3%2B0S9feJ9Ocmb3Big8a3Z2KF1RzLuuTupy1pTa0s9JWMQ4iMnDzHQC1pIbYgb%2BEwVEQ7B9L6Ot9L%2BDGlnTqVALd4mxnq4fbhnbSufTBNyYi45B6AgaEv%2Fp65IT0BqVJpbZUl7RPzv9p2Op74D7KnG6XdUOWGXEuFLVdAF5Ez%2FQT%2FewYKAklkvSuHzr%2FYVB5xIxLvSswhQmkg4%2BRyikoZdSIjB%2FoU4Ct2ksOq9hWVmhmK%2FakxTqvYvV1HzdC%2FQjuOeJ2yUUlkvtra8Z9MF9i5mekf3vY9smleNRcx0uOUDYJK6EV1WraWE0OAzDE28jx%2Bn3ia%2B6cjixfaLzyaIGDCgVN%2BvSFK93dYnnSCxKlPK3IIF%2FdR%2F4%2FcP7ADWzhIyRhIz3IMlPydWl%2FCOXTje1LWK6SMwj8HVkgY6hQIIR1y6bbzr9HgaeKJfvcZd%2BT7UX62sYxZQhfD3psp6O606SpFac9FySfZ9XnZQX72vHTPmt81GBLBhP%2BMAoyEnHrHEYijOtYGtKgyGIkOAMseqTyRmI%2FcN52OraRObDWtOWLmli01xKVRxku1Nx7qm1cQyOpoQ5hGZ76IUIBiYQZcj%2BZEClg32dptziE5LIs41sGitjzQgz3Rwm3tKj8FyfYlVEleclrYxl85%2F6QlIe9ynkz99BlIi%2FBSk13c49sqh5AiI7EjRaREzMvQzmJENKyHQlF20ICSvNYLMmOrajgFMLYGVDVY%2FuWIBUvHxhFIrQmpcIS%2B95wc27xZj9X38lkl%2BDBs%3D&amp;X-Amz-Signature=489004698b3104f3f406cf2c7234df3fc20453c70d8431152a7a4d69469e39bf&amp;X-Amz-SignedHeaders=host&amp;x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Ubuntu%3B%20Linux%20x86_64%3B%20rv%3A99.0%29%20Gecko%2F20100101%20Firefox%2F99.0%20aws-amplify%2F3.8.1%20js&amp;x-id=GetObject"></div></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-36e99a"> <div class="loree-iframe-content-image-wrapper loree-style-1a0138"><img alt="" class="loree-iframe-content-image loree-style-cd018b" src="https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/dummyFileName.jpg"></div></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-cea5de"></div></div>';
  beforeEach(() => {

    uploadToS3.mockImplementation(() => 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/dummyFileName.jpg');

    const accessToken = { lmsApiUrl: "https://crystaldelta.instructure.com" };
    Request.getAccessToken = jest.fn().mockImplementation(() => accessToken);
    nock("https://crystaldelta.instructure.com/api/v1/courses/875/files/373522").persist().get(/.*/)
    .reply(200, `{"display_name": "ThisIsFromAPIDisplayName.jpg"}`);
    nock("https://crystaldelta.instructure.com/api/v1/files/373522").persist().get(/.*/)
    .reply(200, `{"display_name": "ThisIsFromAPIDisplayName.jpg"}`);
    const queryByEmailTokenReturnValue = {lmsApiUrl: "https://crystaldelta.instructure.com", accessToken: "ahts625-13434-2323-1212-12"};
    Database.queryLtiAccessTokenByEmail = jest.fn().mockImplementation(() => queryByEmailTokenReturnValue);
  });
  test.each([
    { globalHtmlContent: globalHtmlContentWithCourseAndFileId },
    { globalHtmlContent: globalHtmlContentWithFileId },
  ])('Save to templates complete process', async({ globalHtmlContent }) => {
    const data = {
      globalHtmlContent: globalHtmlContent,
      type: 'dbcdefgh-12354gts-617393-13121',
      platform: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      idToken: 'ee104332-596d-4353-9f75-fabdccd35984'
    };
    await expect(addImagesToS3BucketAndUpdateHtml(data)).resolves.toBe(saveAsTemplatesResultMockData);
  });
  test('canvas and loree images mixed for save as custom blocks', async() => {
    const globalHtmlContent = `<div class="loree-iframe-content-row row loree-style-0c3e1c"> <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-407746"> <div class="loree-iframe-content-image-wrapper loree-style-a539ce"><img alt="" class="loree-iframe-content-image loree-style-862c96" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/Global-Elements/-1641645535907?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Credential=ASIATHBL7ES3DXNJHXZJ%2F20220412%2Fap-southeast-2%2Fs3%2Faws4_request&amp;X-Amz-Date=20220412T112146Z&amp;X-Amz-Expires=900&amp;X-Amz-Security-Token=IQoJb3JpZ2luX2VjEJv%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDmFwLXNvdXRoZWFzdC0yIkcwRQIhALhfFmyJbOlB8Peshu9qrszxQasQddZq18gfrmp9HJuoAiA5fJGGM1Z9ysczGaX3ir%2Bjl9DXD6HMDy2v2fwPu6dlHCriBAhEEAMaDDIyMTI4MzAzNDI5NCIM5%2BLU2%2BKgAQXfgjeyKr8EV5Bq80p6NfkxrlQem4nSo5uIQlpyNwR0jI8EwgdsCnzmSAx6Te78QHMXQmXQNXXaXeyeOxPB1LdIaLBbBzUi8QF6GvJxgtErtYVTiBYBSKAX55%2FM%2Fid7ZUTHVTNXTwlC6GdSfjzjO8%2FIftFSWfE0t8Kcnqf6zo%2Fyxiu06swbrmUY%2FSc60tP278Er0CtBYTik0Iy9qykU%2Fcw15oP%2B4nb0qaQH%2F7Fp1y%2Bc5XyYikN69gNXIGcn9tKP%2FWGinCaPkEd%2F1oGthmwVpznOCHxSXcVLts880ySxJKqOUnS8TGr8vbakm2eX%2ByF051L0aheui%2Fr3vfevyMlANrR9Oy62g5NiGFdK6Hy3QbPgqA8%2Fcsx3%2B0S9feJ9Ocmb3Big8a3Z2KF1RzLuuTupy1pTa0s9JWMQ4iMnDzHQC1pIbYgb%2BEwVEQ7B9L6Ot9L%2BDGlnTqVALd4mxnq4fbhnbSufTBNyYi45B6AgaEv%2Fp65IT0BqVJpbZUl7RPzv9p2Op74D7KnG6XdUOWGXEuFLVdAF5Ez%2FQT%2FewYKAklkvSuHzr%2FYVB5xIxLvSswhQmkg4%2BRyikoZdSIjB%2FoU4Ct2ksOq9hWVmhmK%2FakxTqvYvV1HzdC%2FQjuOeJ2yUUlkvtra8Z9MF9i5mekf3vY9smleNRcx0uOUDYJK6EV1WraWE0OAzDE28jx%2Bn3ia%2B6cjixfaLzyaIGDCgVN%2BvSFK93dYnnSCxKlPK3IIF%2FdR%2F4%2FcP7ADWzhIyRhIz3IMlPydWl%2FCOXTje1LWK6SMwj8HVkgY6hQIIR1y6bbzr9HgaeKJfvcZd%2BT7UX62sYxZQhfD3psp6O606SpFac9FySfZ9XnZQX72vHTPmt81GBLBhP%2BMAoyEnHrHEYijOtYGtKgyGIkOAMseqTyRmI%2FcN52OraRObDWtOWLmli01xKVRxku1Nx7qm1cQyOpoQ5hGZ76IUIBiYQZcj%2BZEClg32dptziE5LIs41sGitjzQgz3Rwm3tKj8FyfYlVEleclrYxl85%2F6QlIe9ynkz99BlIi%2FBSk13c49sqh5AiI7EjRaREzMvQzmJENKyHQlF20ICSvNYLMmOrajgFMLYGVDVY%2FuWIBUvHxhFIrQmpcIS%2B95wc27xZj9X38lkl%2BDBs%3D&amp;X-Amz-Signature=489004698b3104f3f406cf2c7234df3fc20453c70d8431152a7a4d69469e39bf&amp;X-Amz-SignedHeaders=host&amp;x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Ubuntu%3B%20Linux%20x86_64%3B%20rv%3A99.0%29%20Gecko%2F20100101%20Firefox%2F99.0%20aws-amplify%2F3.8.1%20js&amp;x-id=GetObject"></div></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-36e99a"> <div class="loree-iframe-content-image-wrapper loree-style-1a0138"><img alt="" class="loree-iframe-content-image loree-style-cd018b" src="https://crystaldelta.instructure.com/files/386518/download?download_frd=1&amp;verifier=mT2WrdhPZkTDLtHZ1hknZ8YoNvzPT34VX2C4Zm8L"></div></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-cea5de"></div></div>`;
    const data = {
      globalHtmlContent: globalHtmlContent,
      type: 'dbcdefgh-12354gts-617393-13121',
      platform: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      idToken: 'ee104332-596d-4353-9f75-fabdccd35984'
    };
    await expect(addImagesToS3BucketAndUpdateHtml(data)).resolves.toBe(saveAsTemplatesResultMixedContentMockData);
  });
  test('Mixed images in the same column element', async() => {
    const data = {
      globalHtmlContent: globalHtmlContentWithMixedImagesInSingleColumn,
      type: 'dbcdefgh-12354gts-617393-13121',
      platform: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      idToken: 'ee104332-596d-4353-9f75-fabdccd35984'
    };
    await expect(addImagesToS3BucketAndUpdateHtml(data)).resolves.toBe(saveAsCustomBlocksWithMixedContentInSingleColumn);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
});