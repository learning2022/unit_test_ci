const nock = require('nock');
const {
  searchFileinCanvasStorage
} = require('./ImageUpload');
const {
  canvasUrl,
  canvasGetFileApiResult,
  s3DummyImage
} = require('./imageuploadDuplicationMockData');

describe('#Image Duplication protocol', () => {
  const queryByEmailTokenReturnValue = {lmsApiUrl: "https://crystaldelta.instructure.com", accessToken: "ahts625-13434-2323-1212-12"};
  process.env.STORAGE_LOREEV2S35B8B11F1_BUCKETNAME = 'Our-S3-Bucket';
  beforeEach(() => {
    nock("https://s3.amazonaws.com/Our-S3-Bucket/public/").persist().get(/.*/).reply(200, ``);
    nock("https://crystaldelta.instructure.com/api/v1/courses/875/files").get(/.*/).once().reply(200, canvasGetFileApiResult);
    nock("https://crystaldelta.instructure.com/api/v1/385841/public_url").get(/.*/).once().reply(200, {"public_url":"https://s3.amazonaws.com/Our-S3-Bucket/public/"});
  });
  test('Image present in the canvas', async() => {
    const searchFileInCanvasOptions = {
      fileName: 'FlyingParrot-1',
      fileType: 'image/jpg',
      courseId: 875,
      s3ImageUrl: s3DummyImage,
      canvasData: queryByEmailTokenReturnValue
    };
    expect(await searchFileinCanvasStorage(searchFileInCanvasOptions)).toBe(canvasUrl);
  });
  test('Image name not present in the canvas', async() => {
    const searchFileInCanvasOptions = {
      fileName: 'Not-a-FlyingParrot-1',
      fileType: 'image/jpg',
      courseId: 875,
      s3ImageUrl: s3DummyImage,
      canvasData: queryByEmailTokenReturnValue
    };
    const searchCanvasResult = await searchFileinCanvasStorage(searchFileInCanvasOptions);
    expect(searchCanvasResult).toBeFalsy();
    expect(searchCanvasResult).toBe('');
  });
});