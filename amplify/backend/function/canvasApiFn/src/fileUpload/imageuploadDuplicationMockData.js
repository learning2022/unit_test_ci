const canvasUrl = 'https://crystaldelta.instructure.com/files/385841/download?download_frd=1&verifier=Dw551C22sgtvfrGmEG4lroepdqEk2JDUh2MmtQXV';

const canvasGetFileApiResult = `[
  {
    "id": 385841,
    "uuid": "Dw551C22sgtvfrGmEG4lroepdqEk2JDUh2MmtQXV",
    "folder_id": 13215,
    "display_name": "Hal-ea663c9f-8f81-43fd-bf19-647132cee6d4",
    "filename": "FlyingParrot-1",
    "upload_status": "success",
    "content-type": "image/jpg",
    "url": "${canvasUrl}",
    "size": 82972,
    "created_at": "2022-04-07T13:52:20Z",
    "updated_at": "2022-04-07T13:52:20Z",
    "unlock_at": null,
    "locked": false,
    "hidden": false,
    "lock_at": null,
    "hidden_for_user": false,
    "thumbnail_url": null,
    "modified_at": "2022-04-07T13:52:20Z",
    "mime_class": "file",
    "media_entry_id": null,
    "locked_for_user": false
  },
  {
    "id": 385841,
    "uuid": "Dw551C22sgtvfrGmEG4lroepdqEk2JDUh2MmtQXV",
    "folder_id": 13215,
    "display_name": "Hal-ea663c9f-8f81-43fd-bf19-647132cee6d4",
    "filename": "SomethingElse",
    "upload_status": "success",
    "content-type": "image/jpg",
    "url": "https://crystaldelta.instructure.com/files/385841/download?download_frd=1&verifier=Dw551C22sgtvfrGmEG4lroepdqEk2JDUh2MmtQXV",
    "size": 82972,
    "created_at": "2022-04-07T13:52:20Z",
    "updated_at": "2022-04-07T13:52:20Z",
    "unlock_at": null,
    "locked": false,
    "hidden": false,
    "lock_at": null,
    "hidden_for_user": false,
    "thumbnail_url": null,
    "modified_at": "2022-04-07T13:52:20Z",
    "mime_class": "file",
    "media_entry_id": null,
    "locked_for_user": false
  }
]`;

const s3DummyImage = 'https://loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3OCM6SQ3C%2F20210828%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210828T060035Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEE4aDmFwLXNvdXRoZWFzdC0yIkcwRQIhAM6ewX043PHKLw%2FmD%2FDF7DKsaMfJrh1kfOh9yPcynj3PAiAgWdkDO%2BC&x-id=GetObject';

const globalHTMLCustomBlock = `<div class="loree-iframe-content-row row loree-style-9ed8f4"><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-a4848f"> <div class="loree-iframe-content-image-wrapper loree-style-4b78c7"><img alt="" class="loree-iframe-content-image loree-style-9cf029" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-5ff4cf"> <div class="loree-iframe-content-image-wrapper loree-style-44244a"><img alt="" class="loree-iframe-content-image loree-style-f11b1f" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-1d5e48"> <div class="loree-iframe-content-image-wrapper loree-style-38bed3"><img alt="" class="loree-iframe-content-image loree-style-ea693b" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-8a3d7d"> <div class="loree-iframe-content-image-wrapper loree-style-389eaa"><img alt="" class="loree-iframe-content-image loree-style-e8bbb0" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-664bf5"> <div class="loree-iframe-content-image-wrapper loree-style-30ff6e"><img alt="" class="loree-iframe-content-image loree-style-b9fd85" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-46e079"> <div class="loree-iframe-content-image-wrapper loree-style-b7141a"><img alt="" class="loree-iframe-content-image loree-style-7ed7c8" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div></div><div class="loree-iframe-content-row row loree-style-9ed8f4"><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-a4848f"> <div class="loree-iframe-content-image-wrapper loree-style-4b78c7"><img alt="" class="loree-iframe-content-image loree-style-9cf029" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-5ff4cf"> <div class="loree-iframe-content-image-wrapper loree-style-44244a"><img alt="" class="loree-iframe-content-image loree-style-f11b1f" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-1d5e48"> <div class="loree-iframe-content-image-wrapper loree-style-38bed3"><img alt="" class="loree-iframe-content-image loree-style-ea693b" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-8a3d7d"> <div class="loree-iframe-content-image-wrapper loree-style-389eaa"><img alt="" class="loree-iframe-content-image loree-style-e8bbb0" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-664bf5"> <div class="loree-iframe-content-image-wrapper loree-style-30ff6e"><img alt="" class="loree-iframe-content-image loree-style-b9fd85" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 loree-iframe-content-column loree-style-46e079"> <div class="loree-iframe-content-image-wrapper loree-style-b7141a"><img alt="" class="loree-iframe-content-image loree-style-7ed7c8" src="https://loreev2storage24cd6295054c4df4b5661676008c768e105225-local.s3.ap-southeast-2.amazonaws.com/public/d66b1309-ac2b-48e5-b96f-c0098e8baf0a/d0f5eb54-d676-4d7c-a647-e0fe8298c471/1652869767728-stock-photo-test"></div></div></div>`;

module.exports = { canvasUrl, canvasGetFileApiResult, s3DummyImage, globalHTMLCustomBlock };
