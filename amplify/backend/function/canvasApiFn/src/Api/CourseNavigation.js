/* eslint-disable */
const Request = require('./Request');

class CourseNavigation {
  // fetch Quizzes
  async list(loreeUserEmail, courseId, pageIndex, pageItems) {
    const url = `api/v1/courses/${courseId}/tabs?order_by=recent_activity&sort=id&page=${pageIndex}&per_page=${pageItems}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }
}

module.exports = new CourseNavigation();
