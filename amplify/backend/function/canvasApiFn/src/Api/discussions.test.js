const discussionInstance = require('./Discussions');
const nock = require('nock');
const Request = require('./Request');

describe('Pages API tests', () => {
  process.env.ENABLE_EXTERNAL_LTI_TOOL = 'true';
  nock("https://crystaldelta.instructure.com/api/v1/courses/875/pages/10").persist().get(/.*/)
  test('view page content with empty content', async () => {
    const returnWithEmptyStatement = { body: {body: null}  };
    Request.axiosRequest = jest.fn().mockImplementationOnce(() => returnWithEmptyStatement);
    const htmlDiscussionContent = await discussionInstance.view('dummyEmail@mail.com', 875, 10)
    expect(htmlDiscussionContent.body.body).toBe(null);
  });
  test('view page content with body content', async () => {
    const returnWithStatement = { body: {body: '<div>Design Content</div>'}  };
    Request.axiosRequest = jest.fn().mockImplementationOnce(() => returnWithStatement);
    const htmlDiscussionContent = await discussionInstance.view('dummyEmail@mail.com', 875, 10)
    expect(htmlDiscussionContent.body.body).not.toBe(null);
  });
});
