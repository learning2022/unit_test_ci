/* eslint-disable camelcase */
const Canvas = require('@kth/canvas-api');
const axios = require('axios');
const Database = require('../Database');
const logger =  require('../service/logger').logger;

class Request {
  async getAccessToken(loreeUserEmail) {
    const accessToken = await Database.queryLtiAccessTokenByEmail(loreeUserEmail);
    return accessToken;
  }

  async CheckTokenValidity(userAccessToken) {
    logger.info('Access token', userAccessToken);
    const refreshToken = userAccessToken.refreshToken;

    const currentTime = new Date().toISOString();
    const getApiKeyData = await Database.getApiKeyData(userAccessToken.ltiApiKeyID);
    const generateNewtoken = await this.refreshDBToken(refreshToken, getApiKeyData);
    logger.info('Token refreshed', generateNewtoken);
    if (generateNewtoken) {
      await Database.updateLtiAccessToken(userAccessToken.id, generateNewtoken.data.access_token, currentTime);
      return generateNewtoken.data.access_token;
    } else {
      return userAccessToken.accessToken;
    }
  }

  async refreshDBToken (refreshToken, data) {
    // get refreshed access token
    const formData = {
      grant_type: 'refresh_token',
      client_id: data.apiClientId,
      client_secret: data.apiSecretKey,
      refresh_token: `${refreshToken}`,
    };
    try {
      const response = await axios.post(data.oauthTokenUrl, formData);
      return response;
    } catch (err) {
      logger.error('Refresh token failed', err);
      return false;
    }
  }

  async axiosRequest(url, method, postData, loreeUserEmail) {
    const canvasData = await this.getAccessToken(loreeUserEmail);
    const options = {
      method: method,
      url: `${canvasData.lmsApiUrl}/${url}`,
      headers: {
        Authorization: `Bearer ${canvasData.accessToken}`,
      },
      data: postData,
    };
    logger.info('Axios Request Options', options);
    try {
      const response = await axios.request(options);
      logger.info('Axios Response', response);
      const responseData = {
        statusCode: response.status,
        body: response.data,
      };
      return responseData;
    } catch (err) {
      logger.error('axios error', err);
      if (err.response.status === 401) {
        return this.axiosRefreshRequest(err, canvasData);
      } else {
        const responseData = {
          statusCode: err.response.status,
          body: err.response.data,
        };
        return responseData;
      }
    }
  }

  async axiosRefreshRequest(err, token) {
    const refreshToken = await this.CheckTokenValidity(token);
    if (refreshToken) {
      logger.info('Inside refresh token', refreshToken);
      try {
        logger.info('Error response API', err.response.config);
        const refreshOptions = {
          method: err.response.config.method,
          url: err.response.config.url,
          headers: {
            Authorization: `Bearer ${refreshToken}`,
          },
          data: {},
        };
        logger.info('refreshOptions params', refreshOptions);        
        const apiResponse = await axios(refreshOptions);
        const responseData = {
          statusCode: apiResponse.status,
          body: apiResponse.data,
        };
        return responseData;
      } catch (error) {
        logger.error('Error response API failed', error);
        const responseData = {
          statusCode: error.response.status,
          body: error.response.data,
        };
        return responseData;
      }
    } else {
      const responseData = {
        statusCode: 401,
        body: 'Unauthorized',
      };
      return responseData;
    }
  }

  // Request which used kth package
  async kthRequest(url, data, loreeUserEmail) {
    const canvasData = await this.getAccessToken(loreeUserEmail);
    try {
      // TODO: fetch from accessToken table
      const canvas = Canvas(`${canvasData.lmsApiUrl}/api/v1`, canvasData.accessToken);
      const response = await canvas.list(`${url}`).toArray();
      logger.info('response', response);
      return {
        statusCode: 200,
        body: response,
      };
    } catch (error) {
      logger.info('Error in fetching data from canvas', error);
      if (error.statusCode === 401) {
        logger.error('Inside Refresh Token API');
        return this.kthRefreshRequest(error, canvasData, url);
      } else {
        const responseData = {
          statusCode: 500,
          body: error,
        };
        return responseData;
      }
    }
  }

  async kthRefreshRequest(error, token, url) {
    const refreshToken = await this.CheckTokenValidity(token);
    logger.info('Refresh token', refreshToken);
    if (refreshToken) {
      try {
        const canvas = Canvas(`${token.lmsApiUrl}/api/v1`, refreshToken);
        const response = await canvas.list(`${url}`).toArray();
        logger.info('response', response);
        return {
          statusCode: 200,
          body: response,
        };
      } catch (err) {
        logger.error('Error in fetching data from canvas', err);
        const responseData = {
          statusCode: error.statusCode,
          body: err,
        };
        return responseData;
      }
    } else {
      const responseData = {
        statusCode: 401,
        body: 'Unauthorized',
      };
      return responseData;
    }
  }
}

module.exports = new Request();
