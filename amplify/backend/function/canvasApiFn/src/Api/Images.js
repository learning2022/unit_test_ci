/* eslint-disable */
const Request = require('./Request');

class Images {
  // fetch Discussions
  async list(loreeUserEmail, courseId) {
    const url = `/courses/${courseId}/files?per_page=100&content_types[]=image&sort=updated_at&order=asc`;
    const data = {};
    return await Request.kthRequest(url, data, loreeUserEmail);
  }
}

module.exports = new Images();
