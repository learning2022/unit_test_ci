/* eslint-disable */
const Request = require('./Request');
const { addImagesToCanvasAndUpdateHtml } = require('../service/imageHandling');
const { replaceIframeUrls, getHtmlContentFromResponse } = require('../service/iframeHandling');

class Assignments {
  // fetch Assignments
  async list(loreeUserEmail, courseId) {
    const url = `/courses/${courseId}/assignment_groups?include[]=assignments&per_page=100`;
    const data = {};
    return await Request.kthRequest(url, data, loreeUserEmail);
  }
  async view(loreeUserEmail, courseId, assignmentId) {
    const url = `api/v1/courses/${courseId}/assignments/${assignmentId}`;
    const data = {};
    let response = await Request.axiosRequest(url, 'get', data, loreeUserEmail);
    if(process.env.ENABLE_EXTERNAL_LTI_TOOL === 'true' && response.body.body !== null){
      response = getHtmlContentFromResponse(response, 'ASSIGNMENT');
    }
    return response;
  }
  async update(updateData) {
    const url = `api/v1/courses/${updateData.courseId}/assignments/${updateData.assignmentId}`;
    const data = `title=${updateData.assignmentName}`;
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }
  async save(updateData) {
    let editorContent = updateData.editorContent;
    if(updateData.uploadAndConvertFromBackendFeatureToggle){
      editorContent = await addImagesToCanvasAndUpdateHtml(
        updateData.editorContent,
        updateData.platformId,
        updateData.loreeUserEmail,
        updateData.courseId);
    }
    if(process.env.ENABLE_EXTERNAL_LTI_TOOL === 'true'){
      editorContent = await replaceIframeUrls(
        editorContent,
        updateData.platformId,
        updateData.loreeUserEmail,
        updateData.courseId);
    }
    const url = `api/v1/courses/${updateData.courseId}/assignments/${updateData.assignmentID}`;
    const data = {
      assignment: {
        description: editorContent,
      },
    };
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }
}

module.exports = new Assignments();
