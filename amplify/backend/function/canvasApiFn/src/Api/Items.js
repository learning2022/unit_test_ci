/* eslint-disable */
const Request = require('./Request');

class Items {
  // Create new Page for the selected module
  async createPageToModule(loreeUserEmail, courseId, moduleId, pageUrl) {
    const url = `api/v1/courses/${courseId}/modules/${moduleId}/items?module_item[type]=Page&module_item[page_url]=${pageUrl}`;
    return await Request.axiosRequest(url, 'post', {}, loreeUserEmail);
  }

  // Update page name for the selected module
  async updateModuleItem(updateData) {
    const url = `api/v1/courses/${updateData.courseId}/modules/${updateData.moduleId}/items/${updateData.itemId}`;
    const updateModuleItemTitle = `module_item[title]=${updateData.title}`;
    return await Request.axiosRequest(url, 'put', updateModuleItemTitle, updateData.loreeUserEmail);
  }

  // Delete page for the selected module
  async deleteModuleItem(loreeUserEmail, courseId, moduleId, itemId) {
    const url = `api/v1/courses/${courseId}/modules/${moduleId}/items/${itemId}`;
    return await Request.axiosRequest(url, 'delete', {}, loreeUserEmail);
  }

  async duplicatePageFromModule(loreeUserEmail, courseId, moduleId, pageUrl) {
    const url = `api/v1/courses/${courseId}/pages/${pageUrl}/duplicate`;
    let response = await Request.axiosRequest(url, 'post', {}, loreeUserEmail);
    if (response.statusCode === 200) {
      const item_url = `api/v1/courses/${courseId}/modules/${moduleId}/items`;
      const item_data = {
        module_item: {
          type: 'Page',
          page_url: `${response.body.url}`,
        },
      };
      return await Request.axiosRequest(item_url, 'post', item_data, loreeUserEmail);
    } else {
      return {
        statusCode: 500,
        body: 'Error in duplicating the page',
      };
    }
  }
}

module.exports = new Items();
