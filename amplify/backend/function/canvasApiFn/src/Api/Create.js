/* eslint-disable */
const Request = require('./Request');

class Create {
  requestOptions = updateData => {
    let url;

    switch (updateData.type) {
      case 'ITEM':
        return `api/v1/courses/${updateData.courseId}/modules/${updateData.moduleId}/items?module_item[type]=Page&module_item[page_url]=${updateData.Name}`;

      case 'MODULE':
        return `api/v1/courses/${updateData.courseId}/modules?module[name]=${updateData.Name}`;

      case 'PAGE':
        return `api/v1/courses/${updateData.courseId}/pages?wiki_page[title]=${updateData.Name}`;

      default:
        return url;
    }
  };

  async moduleOrPage(updateData) {
    const url = this.requestOptions(updateData);
    return await Request.axiosRequest(url, 'post', '', updateData.loreeUserEmail);
  }
}

module.exports = new Create();
