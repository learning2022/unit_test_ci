/* eslint-disable */
const Request = require('./Request');

class Modules {
  // fetch modules
  async list(loreeUserEmail, courseId, pageIndex, pageItems) {
    const url = `api/v1/courses/${courseId}/modules?order=asc&sort=id&page=${pageIndex}&per_page=${pageItems}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }

  // fetch module Items
  async listItems(loreeUserEmail, courseId, moduleId) {
    const url = `/courses/${courseId}/modules/${moduleId}/items?per_page=100`;
    const data = {};
    return await Request.kthRequest(url, data, loreeUserEmail);
  }


  // Update module name for the selected module
  async update(updateData) {
    const url = `api/v1/courses/${updateData.courseId}/modules/${updateData.moduleId}`;
    const data = `module[name]=${updateData.moduleName}`;
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }

  // Delete module
  async delete(loreeUserEmail, courseId, moduleId) {
    const url = `api/v1/courses/${courseId}/modules/${moduleId}`;
    const data = {};
    return await Request.axiosRequest(url, 'delete', data, loreeUserEmail);
  }
}

module.exports = new Modules();
