/* eslint-disable */
const Request = require('./Request');

class Announcement {
  // fetch Quizzes
  async list(loreeUserEmail, courseId, pageIndex, pageItems) {
    const url = `api/v1/announcements?context_codes[]=course_${courseId}&page=${pageIndex}&per_page=${pageItems}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }
}

module.exports = new Announcement();
