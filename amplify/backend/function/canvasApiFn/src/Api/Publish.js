/* eslint-disable */
const Request = require('./Request');

class Publish {
  requestOptions = updateData => {
    let data = {};

    switch (updateData.type) {
      case 'ITEM':
        data['url'] = `api/v1/courses/${updateData.courseId}/modules/${updateData.moduleId}/items/${updateData.itemId}`;
        data['body'] = `module_item[published]=${updateData.status}`;
        return data;

      case 'MODULE':
        data['url'] = `api/v1/courses/${updateData.courseId}/modules/${updateData.moduleId}`;
        data['body'] = `module[published]=${updateData.status}`;
        return data;

      case 'PAGE':
        data['url'] = `api/v1/courses/${updateData.courseId}/pages/${updateData.pageId}`;
        data['body'] = `wiki_page[published]=${updateData.status}`;
        return data;

      case 'ASSIGNMENT':
        data['url'] = `api/v1/courses/${updateData.courseId}/assignments/${updateData.assignmentId}`;
        data['body'] = `assignment[published]=${updateData.status}`;
        return data;

      case 'DISCUSSION':
        data['url'] = `api/v1/courses/${updateData.courseId}/discussion_topics/${updateData.discussionId}`;
        data['body'] = `published=${updateData.status}`;
        return data;

      case 'QUIZ':
        data['url'] = `api/v1/courses/${updateData.courseId}/quizzes/${updateData.quizId}`;
        data['body'] = `quiz[published]=${updateData.status}`;
        return data;

      default:
        return data;
    }
  };

  async publish(updateData) {
    console.log(updateData);
    const options = this.requestOptions(updateData);
    console.log(options);
    return await Request.axiosRequest(options.url, 'put', options.body, updateData.loreeUserEmail);
  }
}

module.exports = new Publish();
