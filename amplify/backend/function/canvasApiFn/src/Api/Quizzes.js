/* eslint-disable */
const Request = require('./Request');

class Quizzes {
  // fetch Quizzes
  async list(loreeUserEmail, courseId, pageIndex, pageItems) {
    const url = `api/v1/courses/${courseId}/quizzes?order_by=recent_activity&sort=id&page=${pageIndex}&per_page=${pageItems}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }
}

module.exports = new Quizzes();
