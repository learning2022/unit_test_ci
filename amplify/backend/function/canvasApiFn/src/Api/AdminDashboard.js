/* eslint-disable */
const Request = require('./Request');
const Database = require('../Database');
const crypto = require('crypto');

class AdminDashboard {
  // fetch accounts
  async accounts(loreeUserEmail) {
    const url = `/accounts`;
    const data = {};
    return await Request.kthRequest(url, data, loreeUserEmail);
  }
  // fetch subaccounts
  async subAccounts(loreeUserEmail, accountId) {
    const url = `/accounts/${accountId}/sub_accounts`;
    const data = {};
    return await Request.kthRequest(url, data, loreeUserEmail);
  }
  // fetch roles
  async roles(loreeUserEmail, accountId) {
    const url = `/accounts/${accountId}/roles`;
    const data = {};
    return await Request.kthRequest(url, data, loreeUserEmail);
  }

  async courses(loreeUserEmail, courseId) {
    const url = `api/v1/courses/${courseId}?include[]=account`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }

  async accountById(loreeUserEmail, accountId) {
    const url = `api/v1/accounts/${accountId}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }

  async dashboardStatistics(platformId) {
    return await Database.queryDashboardStatistics(platformId);
  }

  async client(platformId) {
    try {
      const platform = await Database.queryLtiPlatform(platformId);
      console.log('Platform Details', platform);
      const apiKey = await Database.queryLtiApiKey(platformId);
      console.log('API Details', apiKey);
      const platformResponse = {
        platformId: platform.id,
        clientId: platform.clientId,
        clientSecret: platform.clientSecret,
        platformUrl: platform.platformUrl,
        apiClientId: apiKey.apiClientId,
        apiSecretKey: apiKey.apiSecretKey,
      };
      return {
        statusCode: 200,
        body: platformResponse,
      };
    } catch (err) {
      console.log('Error', err);
      return {
        statusCode: 500,
        body: err,
      };
    }
  }

  async generatekeys(platformId) {
    console.log('Platform keys', platformId);
    const key = await Database.queryLtiPlatformKey(platformId);
    let kid = crypto.randomBytes(16).toString('hex');
    console.log('Platform keys ID', kid);
    const keys = crypto.generateKeyPairSync('rsa', {
      modulusLength: 4096,
      publicKeyEncoding: {
        type: 'spki',
        format: 'pem',
      },
      privateKeyEncoding: {
        type: 'pkcs1',
        format: 'pem',
      },
    });
    const { publicKey, privateKey } = keys;
    console.log('Platform keys KEYS', publicKey);
    console.log('Platform keys KEYS', privateKey);
    return await Database.updateLtiPlatformKey(key.id, kid, privateKey, publicKey);
  }
}

module.exports = new AdminDashboard();
