/* eslint-disable */
const Request = require('./Request');
const { addImagesToCanvasAndUpdateHtml } = require('../service/imageHandling');
const { replaceIframeUrls, getHtmlContentFromResponse } = require('../service/iframeHandling');

class Discussions {
  // fetch Discussions
  async list(loreeUserEmail, courseId, pageIndex, pageItems) {
    const url = `api/v1/courses/${courseId}/discussion_topics?order=asc&sort=id&page=${pageIndex}&per_page=${pageItems}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }
  async view(loreeUserEmail, courseId, discussionId) {
    const url = `api/v1/courses/${courseId}/discussion_topics/${discussionId}`;
    const data = {};
    let response = await Request.axiosRequest(url, 'get', data, loreeUserEmail);
    if(process.env.ENABLE_EXTERNAL_LTI_TOOL === 'true' && response.body.body !== null){
      response = getHtmlContentFromResponse(response, 'DISCUSSIONS');
    }
    return response;
  }
  async update(updateData) {
    const url = `api/v1/courses/${updateData.courseId}/discussion_topics/${updateData.discussionId}`;
    const data = `title=${updateData.discussionName}`;
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }
  async save(updateData) {
    let editorContent = updateData.editorContent;
    if(updateData.uploadAndConvertFromBackendFeatureToggle){
      editorContent = await addImagesToCanvasAndUpdateHtml(
        updateData.editorContent,
        updateData.platformId,
        updateData.loreeUserEmail,
        updateData.courseId);
    }
    if(process.env.ENABLE_EXTERNAL_LTI_TOOL === 'true'){
      editorContent = await replaceIframeUrls(
        editorContent,
        updateData.platformId,
        updateData.loreeUserEmail,
        updateData.courseId);
    }
    const url = `api/v1/courses/${updateData.courseId}/discussion_topics/${updateData.discussionID}`;
    const data = {
      message: editorContent,
    };
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }
}

module.exports = new Discussions();
