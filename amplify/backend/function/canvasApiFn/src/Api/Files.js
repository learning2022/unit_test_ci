/* eslint-disable */
const Request = require('./Request');

class Files {
  // fetch Quizzes
  async list(loreeUserEmail, courseId, pageIndex, pageItems) {
    const url = `api/v1/courses/${courseId}/files?order_by=recent_activity&page=${pageIndex}&per_page=${pageItems}`;
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }
}

module.exports = new Files();
