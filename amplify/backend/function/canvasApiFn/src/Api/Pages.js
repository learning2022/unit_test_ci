/* eslint-disable */
const Request = require('./Request');
const { addImagesToCanvasAndUpdateHtml } = require('../service/imageHandling');
const { replaceIframeUrls, getHtmlContentFromResponse } = require('../service/iframeHandling');
const logger =  require('../service/logger').logger;

class Pages {
  // fetch pages
  async list(pageListOptions) {
    const { loreeUserEmail, courseId, pageIndex, pageItems, orderType, sortingValue } = pageListOptions;
    const url = `api/v1/courses/${courseId}/pages?order=${orderType}&sort=${sortingValue}&page=${pageIndex}&per_page=${pageItems}`; // Ordering should be passed
    const data = {};
    return await Request.axiosRequest(url, 'get', data, loreeUserEmail);
  }

  // view page
  async view(loreeUserEmail, courseId, pageId) {
    const url = `api/v1/courses/${courseId}/pages/${pageId}`;
    const data = {};
    let response = await Request.axiosRequest(url, 'get', data, loreeUserEmail);
    if(process.env.ENABLE_EXTERNAL_LTI_TOOL === 'true' && response.body.body !== null){
      response = getHtmlContentFromResponse(response, 'PAGES');
    }
    return response
  }

  // Create new Page for the selected course
  async create(loreeUserEmail, courseId, pageName) {
    const url = `api/v1/courses/${courseId}/pages?wiki_page[title]=${pageName}`;
    const data = {};
    return await Request.axiosRequest(url, 'post', data, loreeUserEmail);
  }

  // Update page name for the selected page
  async update(updateData) {
    const url = `api/v1/courses/${updateData.courseId}/pages/${updateData.pageUrl}`;
    const data = `wiki_page[title]=${updateData.pageName}`;
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }

  // Delete page
  async delete(loreeUserEmail, courseId, pageUrl) {
    const url = `api/v1/courses/${courseId}/pages/${pageUrl}`;
    return await Request.axiosRequest(url, 'delete', {}, loreeUserEmail);
  }

  // Duplicate page
  async duplicate(loreeUserEmail, courseId, pageUrl) {
    const url = `api/v1/courses/${courseId}/pages/${pageUrl}/duplicate`;
    return await Request.axiosRequest(url, 'post', {}, loreeUserEmail);
  }

  // save page
  async save(updateData) {
    let editorContent = updateData.editorContent;
    if(updateData.uploadAndConvertFromBackendFeatureToggle){
      editorContent = await addImagesToCanvasAndUpdateHtml(
        updateData.editorContent,
        updateData.platformId,
        updateData.loreeUserEmail,
        updateData.courseId);
    }
    if(process.env.ENABLE_EXTERNAL_LTI_TOOL === 'true'){
      editorContent = await replaceIframeUrls(
        editorContent,
        updateData.platformId,
        updateData.loreeUserEmail,
        updateData.courseId);
    }
    const url = `api/v1/courses/${updateData.courseId}/pages/${updateData.pageID}`;
    const data = {
      wiki_page: {
        body: editorContent,
      },
    };
    return await Request.axiosRequest(url, 'put', data, updateData.loreeUserEmail);
  }
}

module.exports = new Pages();
