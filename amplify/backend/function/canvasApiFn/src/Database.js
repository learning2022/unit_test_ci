/* eslint-disable */
const AWSXRay = require('aws-xray-sdk');
const AWS = AWSXRay.captureAWS(require('aws-sdk'));
const logger =  require('./service/logger').logger;

const docClient = new AWS.DynamoDB.DocumentClient();

class Database {
  async queryLtiAccessToken(id) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      Key: {
        id: id,
      },
    };
    return await this.docRequest(params, 'queryLtiAccessToken');
  }
  
  // this function will be removed once we changed id token as server side for all function and this log is for testing purposes.
  async queryLtiAccessTokenByEmail(email) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      IndexName: 'byEmail',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': email },
      ExpressionAttributeNames: { '#name': 'loreeUsername' },
    };
    logger.debug("🚀 ~ file: Database.js ~ line 24 ~ Database ~ queryLtiAccessToken1 ~ params"+ params)
    try {
      const data = await docClient.query(params).promise();
      logger.debug('Query access token. Data.items :'+ data, data.Items);
      return data.Items[0];
    } catch (err) {
      logger.error('Error in query access token config. Error:'+ err);
      return err;
    }
    
  }

  async queryKalturaConfigDetails(platformId) {
    const params = {
      TableName: process.env.API_LOREEV2API_KALTURACONFIGTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };
    try {
      const data = await docClient.query(params).promise();
      console.log('Query kaltura config. Data.items :', data, data.Items);
      return data.Items[0];
    } catch (err) {
      console.error('Error in query kaltura config. Error:', err);
      return err;
    }
  }

  async getApiKeyData(ltiApiKeyID) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIAPIKEYTABLE_NAME,
      Key: {
        id: ltiApiKeyID,
      },
    };
    return await this.docRequest(params, 'getApiKeyData');
  }

  async updateLtiAccessToken(id, token, dateGenerated) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      Key: {
        id: id,
      },
      UpdateExpression: 'set #access_token = :tokenValue, #dateGen = :dateGenerated',
      ExpressionAttributeNames: {
        '#access_token': 'accessToken',
        '#dateGen': 'generatedAt',
      },
      ExpressionAttributeValues: {
        ':tokenValue': token,
        ':dateGenerated': dateGenerated,
      },
    };
    try {
      await docClient.update(params).promise();
      console.log('updated accessToken:');
    } catch (err) {
      console.error('Error in updating accessToken. Error:', err);
      return err;
    }
  }

  async queryLtiPlatform(id) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMTABLE_NAME,
      Key: {
        id: id,
      },
    };
    return this.docRequest(params, 'queryLtiPlatform');
  }

  async queryLtiApiKey(platformId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIAPIKEYTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };
    const data = await docClient.query(params).promise();
    console.log('Query queryLtiApiKey Data.items :', data.Items);
    return data.Items[0];
  }

  async queryDashboardStatistics(platformId) {
    const customTemplateParams = {
      TableName: process.env.API_LOREEV2API_CUSTOMTEMPLATETABLE_NAME,
      IndexName: 'byLtiPlatform',
      Select: 'COUNT',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };
    const customBlockParams = {
      TableName: process.env.API_LOREEV2API_CUSTOMBLOCKTABLE_NAME,
      IndexName: 'byLtiPlatform',
      Select: 'COUNT',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };
    const userParams = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      IndexName: 'byLtiPlatform',
      Select: 'SPECIFIC_ATTRIBUTES',
      ProjectionExpression: 'id, createdAt',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };
    try {
      const runQueries = async () => {
        return Promise.all([
          docClient.query(customTemplateParams).promise(),
          docClient.query(customBlockParams).promise(),
          docClient.query(userParams).promise()
        ]);
      };

      const [ templatesCount, blocksCount, usersCount ] = await runQueries();

      const data = {
        UserData: usersCount,
        UserCount: usersCount.Count,
        TemplatesCount: templatesCount.Count,
        BlocksCount: blocksCount.Count,
      };
      return {
        statusCode: 200,
        body: data,
      };
    } catch (err) {
      console.error('Error in query Custom templates. Error:', err);
      return {
        statusCode: 500,
        body: err,
      };
    }
  }

  async queryLtiPlatformKey(platformId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };

    try {
      const data = await docClient.query(params).promise();
      console.log('Query LtiApiKey. Data.items :', data.Items);
      return data.Items[0];
    } catch (err) {
      console.error('Error in query LtiApiKey. Error:', err);
      return err;
    }
  }

  async updateLtiPlatformKey(keyid, kid, privatekey, publicKey) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME,
      Key: {
        id: keyid,
      },
      UpdateExpression: 'set #kid = :kid, #privatekey = :privatekey, #publicKey = :publicKey',
      ExpressionAttributeNames: {
        '#kid': 'kid',
        '#privatekey': 'privatekey',
        '#publicKey': 'publicKey',
      },
      ExpressionAttributeValues: {
        ':kid': kid,
        ':privatekey': privatekey,
        ':publicKey': publicKey,
      },
    };
    try {
      console.log('updated LtiPlatformKey. updateLtiPlatformKey:');
      return await docClient.update(params).promise();
    } catch (err) {
      console.error('Error in updateLtiPlatformKey. Error:', err);
      return err;
    }
  }

  async docRequest(params, requestType="docRequest") {
    try {
      const data = await docClient.get(params).promise();
      console.log(`Query ${requestType}, Data.items :`, data, data.Item);
      return data.Item;
    } catch (err) {
      console.error(`Error in query ${requestType}, Error:`, err);
      return err;
    }
  }
}

module.exports = new Database();