module.exports = {
  root: true,
  extends: [
    'plugin:jest/recommended',
    'prettier',
    "eslint:recommended",
    "plugin:prettier/recommended"
  ],
  plugins: [
    'prettier',
    'spellcheck',

    // not used in this config but necessary for overrides
    'unicorn',
  ],
  parser: '', // Specifies the ESLint parser
  env: {
    commonjs: true,
    browser: true,
    node: true,
    es6: true,
    jest: true,
  },
  parserOptions: {
    project: './jsConfig.json',
    ecmaVersion: "latest", // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      arrowFunctions: true,
    },
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js'],
        paths: ['./src'],
      },
    },
  },
  rules: {
    // General
    'one-var': ['error', 'never'],

    // Prettier
    'prettier/prettier': 'error',

    // Fixes
    'no-void': 'off', // fix for conflict with @typescript-eslint/no-floating-promises

    'camelcase': [2, {"properties": "always"}],
    'no-var': 2,
    //'no-ternary': 2,
    'no-nested-ternary': 2,
    'no-whitespace-before-property': 2,
    'one-var-declaration-per-line': [2, "always"],
    'array-bracket-spacing': [2, "always"],
    'semi': 2,
    'no-extra-semi': 2,

    // Spelling
    'spellcheck/spell-checker': [
      'warn',
      {
        comments: false,
        strings: true,
        identifiers: false,
        lang: 'en_US',
        skipWords: [
          'dict',
          'aff',
          'hunspellchecker',
          'hunspell',
          'utils',
          'aws',
          'loree',
          'iframe',
        ],
        skipIfMatch: [
          'http://[^s]*',
          '^[-\\w]+/[-\\w\\.]+$', // For MIME Types
        ],
        skipWordIfMatch: [
          // '^foobar.*$', // words that begin with foobar will not be checked
        ],
        minLength: 3,
      },
    ],

    'unicorn/no-abusive-eslint-disable': 'off',
  },
  overrides: [
    {
      files: ['**/*.+(js)'],
      rules: {

      },
    },
    {
      // Lower severity in tests for particularly annoying rules
      files: ['**/__tests__/*.+(js)', '**/*.+(test|spec).+(js)'],
      rules: {
      },
    },
  ],
  ignorePatterns: [
    ".eslintrc.js",
    "npm_modules",
    "build",
    "setUpTests.js",
    "test.test.js"
  ],
};
