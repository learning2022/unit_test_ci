/* eslint-disable */
const AWSXRay = require('aws-xray-sdk');
const AWS = AWSXRay.captureAWS(require('aws-sdk'));

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2016-04-18',
});
const USERPOOLID = process.env.AUTH_LOREEV275F92347_USERPOOLID;
class Migrate {
  async getUserForMigration(email) {
    try {
      const getUserParams = {
        UserPoolId: USERPOOLID,
        Username: email,
      };
      const user = await cognitoidentityserviceprovider.adminGetUser(getUserParams).promise();
      return user.UserAttributes;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = new Migrate();
