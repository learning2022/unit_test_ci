const AWSXRay = require('aws-xray-sdk');
const { JSDOM } = require("jsdom");
const logger =  require('../service/logger').logger;

const aws = AWSXRay.captureAWS(require('aws-sdk'));
const s3 = new aws.S3();
const request = require('request-promise');
const axios = require('axios');
const limitedMemory = 5020; // In MB

const getS3Key = (imageUrl) => {
  try {
    const imagePathname = new URL(imageUrl).pathname;
    const imageUrlParams = imagePathname.split('?');
    const imageS3KeyArray = imageUrlParams[0].split('/');
    const keyValue = imageS3KeyArray ? `${imageS3KeyArray[1]}/${imageS3KeyArray[2]}/${imageS3KeyArray[3]}` : '';
    if(keyValue && imageS3KeyArray[2] !== 'tmp') {
      return keyValue + `/${imageS3KeyArray[4]}`;
    }
    return keyValue;
  } catch (e) {
    logger.error('error on URL during obtaining S3 Key: ' + e);
    throw new Error('Error in getting S3 url');
  }
};

const getSignedUrl = async(key, bucketName) => {
  const signedUrlParams = {
    Bucket: bucketName,
    Key: key,
  };

  return s3.getSignedUrlPromise('getObject', signedUrlParams);
};

const updateImageUrlsInHtmlContent = (htmlString, currentImageUrls, replacingImageUrls, includedStrings) => {
  const element = new JSDOM(htmlString).window.document.body;
  Object.values(element.getElementsByTagName('img')).filter(imageElement => imageElement.hasAttribute("src"))
  .filter(imageElement => includedStrings.some(includeString => imageElement.getAttribute("src").includes(includeString)))
  .forEach((imageElement, index) => {
    const imageSrc = imageElement.getAttribute('src').replace(/&amp;/g, '&');
    const currentImageUrl =  currentImageUrls[index].imageUrl.replace(/&amp;/g, '&');
    if (imageSrc.split('?')[0] === currentImageUrl.split('?')[0] || currentImageUrl.includes(imageSrc)) {
      imageElement.setAttribute('src', replacingImageUrls[index]);
    }
  });
  return element.innerHTML;
};

const updateImageUrlsInHtmlContentCustomBlock = (htmlString, currentImageUrls, replacingImageUrls) => {
  const htmlContent = new JSDOM(htmlString).window.document.body;
  Object.values(htmlContent.getElementsByTagName('img')).filter(imageElement => imageElement.hasAttribute("src"))
  .forEach((imageElement, index) => {
    if(replacingImageUrls[index] !== ''){
      imageElement.setAttribute('src', replacingImageUrls[index]);
    }
  });
  return htmlContent.innerHTML;
};

const getbase64Content = async( s3Url ) => {
  let base64Data = '';
  const { buffer } = await request({ url: s3Url, encoding: null });
  const bufferData = Buffer.from(new Uint8Array(buffer));
  base64Data = bufferData.toString('base64');
  logger.info("Base 64 file data: " + base64Data);
  return base64Data;
};

const getBufferedContent = async( s3Url ) => {
  const response = await axios.get(s3Url);
  const bufferedData = Buffer.from(response.data);
  logger.info('Buffered data: ' + bufferedData);
  return bufferedData;
};

const isMemoryRanOutOfHeap = () => {
  const memoryUsage = process.memoryUsage();
  const usedMemoryMb = (memoryUsage.heapUsed / 1048576);
  logger.info('Memory Usage: ' + usedMemoryMb + ' MB');
  if(usedMemoryMb > limitedMemory) {
    logger.error('Nearly ran out of memory: ' + limitedMemory + ' MB');
    return true;
  }
  return false;
};

module.exports = {
  getS3Key,
  getSignedUrl,
  updateImageUrlsInHtmlContent,
  updateImageUrlsInHtmlContentCustomBlock,
  getbase64Content,
  getBufferedContent,
  isMemoryRanOutOfHeap
};
