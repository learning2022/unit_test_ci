const imageCorrectUrl =
  'https://loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3OCM6SQ3C%2F20210828%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210828T060035Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEE4aDmFwLXNvdXRoZWFzdC0yIkcwRQIhAM6ewX043PHKLw%2FmD%2FDF7DKsaMfJrh1kfOh9yPcynj3PAiAgWdkDO%2BC&x-id=GetObject';

const correctUrlResponse =
  'public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg';

const imageWrongUrl =
  'public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg';

module.exports = { imageCorrectUrl, correctUrlResponse, imageWrongUrl };
