const nock = require('nock');
const fs = require('fs');
const {
  getS3Key,
  getSignedUrl,
  updateImageUrlsInHtmlContent,
  updateImageUrlsInHtmlContentCustomBlock,
  getbase64Content,
  getBufferedContent,
  isMemoryRanOutOfHeap
 } = require('./utils');

describe('get s3 key from image url', () => {
  const imageCorrectUrl = 'https://loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3OCM6SQ3C%2F20210828%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210828T060035Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEE4aDmFwLXNvdXRoZWFzdC0yIkcwRQIhAM6ewX043PHKLw%2FmD%2FDF7DKsaMfJrh1kfOh9yPcynj3PAiAgWdkDO%2BC&x-id=GetObject';
  const correctUrlS3KeyResult = 'public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg';

  const imageUrlForTemporaryS3Bucket = 'https://loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox.s3.ap-southeast-2.amazonaws.com/public/tmp/screenshot-crystaldelta.instructure.com-2022.05.04-20_15_02.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3OCM6SQ3C%2F20210828%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210828T060035Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEE4aDmFwLXNvdXRoZWFzdC0yIkcwRQIhAM6ewX043PHKLw%2FmD%2FDF7DKsaMfJrh1kfOh9yPcynj3PAiAgWdkDO%2BC&x-id=GetObject';
  const correctUrlS3KeyResultForTemporaryS3Bucket = 'public/tmp/screenshot-crystaldelta.instructure.com-2022.05.04-20_15_02.png';
  
  const imageWrongUrl = 'public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg';

  it('passing correct url', async() => {
    expect(getS3Key(imageCorrectUrl)).toEqual(correctUrlS3KeyResult);
  });
  it('passing correct url for temporary S3 bucket', async() => {
    expect(getS3Key(imageUrlForTemporaryS3Bucket)).toEqual(correctUrlS3KeyResultForTemporaryS3Bucket);
  });
  it('passing wrong url', () => {
    expect(() => getS3Key(imageWrongUrl)).toThrow();
  });
});

describe('getting s3 signed url', () => {
  const keyValues = 'public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/2a19c92be7c0d74f0ef47ef372553bdf%283%29-1623747186735.jpg';
  test('get s3 signed URL with empty key values', async() => {
    await expect(getSignedUrl('', 'loreeV2Storage')).rejects.toThrow();
  });
  test('get s3 signed URL with empty bucketname values', async() => {
    await expect(getSignedUrl(keyValues, '')).rejects.toThrow();
  });
  test('get s3 signed URL with params values', async() => {
    expect(await getSignedUrl(keyValues, 'loreeV2Storage')).not.toBeNull();
  });
});

describe('Replacing S3 url to native Url', () => {
  
  const platformId = [ '37212122-2c37-462f-8a61-d68632463bd7' ];
  
  test('parse html content', () => {
    const htmlContentWithSingleS3Image =
    '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div> ';
    const s3ImageSource = [
      {"index": 0, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
    ];
    const replaceS3Images = [
      'http://crystaldelta.instructure.com/files/123/new_url',
    ];
    expect(updateImageUrlsInHtmlContent(htmlContentWithSingleS3Image, s3ImageSource, replaceS3Images, platformId)).toEqual(
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="http://crystaldelta.instructure.com/files/123/new_url"></div></div></div> ',
    );
  });
  test('no <img> element in the html content', () => {
    const htmlContentWithoutS3Images =
    '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><h1>No Image here</h1></div></div></div> ';
    expect(updateImageUrlsInHtmlContent(htmlContentWithoutS3Images, [], [], platformId)).toEqual(htmlContentWithoutS3Images);
  });
  test('multiple <img> element in the html content', () => {
    const htmlContentWithMultipleS3Images =
    '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div> ';
    const imageSourceForMultipleImages = [
      {"index": 0, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
      {"index": 1, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
    ];
    const ReplaceimageUrlForMultipleImages = [
      'http://crystaldelta.instructure.com/files/123/new_url',
      'http://crystaldelta.instructure.com/files/123/new_url',

    ];
    expect(updateImageUrlsInHtmlContent(htmlContentWithMultipleS3Images, imageSourceForMultipleImages, ReplaceimageUrlForMultipleImages, platformId)).toEqual(
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="http://crystaldelta.instructure.com/files/123/new_url"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="http://crystaldelta.instructure.com/files/123/new_url"></div></div></div> ',
    );
  });
  test('mixed <img> element with S3 and native LMS url in the html content', () => {
    const htmlContentWithMixedS3Images =
    '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="Green lantern" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="https://crystaldelta.instructure.com/courses/875/files/373522/download?verifier=xc75cuPkBzxnKofG8z07emlMWjphMRDTYJL8hT2T" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/875/files/373522" data-api-returntype="File"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div> ';
    const s3ImageSource = [
      {"index": 0, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
    ];
    const replaceS3Images = [
      'http://crystaldelta.instructure.com/files/123/new_url',
    ];
    expect(updateImageUrlsInHtmlContent(htmlContentWithMixedS3Images, s3ImageSource, replaceS3Images, platformId)).toEqual(
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="Green lantern" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="https://crystaldelta.instructure.com/courses/875/files/373522/download?verifier=xc75cuPkBzxnKofG8z07emlMWjphMRDTYJL8hT2T" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/875/files/373522" data-api-returntype="File"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="http://crystaldelta.instructure.com/files/123/new_url"></div></div></div> ',
    );
  });
  test('some random s3 bucket we have no control over(expect the same HTML request as response)', () => {
    const htmlContentWithSingleS3Image =
    '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div> ';
    const s3ImageSource = [
      {"index": 0, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
    ];
    const replaceS3Images = [
      'http://crystaldelta.instructure.com/files/123/new_url',
    ];
    const platformId = [ '37212122-2c37-462f-8a61-d68632463bd8' ];

    expect(updateImageUrlsInHtmlContent(htmlContentWithSingleS3Image, s3ImageSource, replaceS3Images, platformId)).toEqual(htmlContentWithSingleS3Image);
  });
  test('parse html content with 2 replace url for 3 images', () => {
    const htmlContentWithSingleS3Image =
    '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/dummy-client-id/Global-Templates/images-1624442408092.png"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div> ';
    const s3ImageSource = [
      {"index": 0, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
      {"index": 2, "imageUrl": 'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'},
    ];
    const replaceS3Images = [
      'http://crystaldelta.instructure.com/files/123/new_url',
      '',
      'http://crystaldelta.instructure.com/files/123/new_url',
    ];
    expect(updateImageUrlsInHtmlContentCustomBlock(htmlContentWithSingleS3Image, s3ImageSource, replaceS3Images)).toEqual(
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="http://crystaldelta.instructure.com/files/123/new_url"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/dummy-client-id/Global-Templates/images-1624442408092.png"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="http://crystaldelta.instructure.com/files/123/new_url"></div></div></div> ',
    );
  });
});

describe('#getbase64Content', () => {
  const filePath = 'public/stack-abuse-logo-out.png';
  nock("https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/").persist().get(/.*/).reply(200, `{}`);
  test('get base 64 for the live url', async() => {
    const s3ImageDummyUrl = `https://uploads.sitepoint.com/wp-content/uploads/2016/10/1475698586jest2-01.png`;
    const imageBase64 = await getbase64Content(s3ImageDummyUrl);
    const buffBase64 = Buffer.from(imageBase64, 'base64');
    fs.writeFileSync(filePath, buffBase64);
    expect(fs.existsSync(filePath)).toBeTruthy();
  });
  test('get buffered data for the live url', async() => {
    const s3ImageDummyUrl = `https://uploads.sitepoint.com/wp-content/uploads/2016/10/1475698586jest2-01.png`;
    const imageBase64 = await getBufferedContent(s3ImageDummyUrl);
    expect(Buffer.isBuffer(imageBase64)).toBeTruthy();
  });
  test('#isMemoryRanOutOfHeap ran out of heap', () => {
    const heapMockResult = {heapUsed: 5200*1048576};
    process.memoryUsage = jest.fn().mockImplementationOnce(() => heapMockResult);
    expect(isMemoryRanOutOfHeap()).toBeTruthy();
  });
  test('#isMemoryRanOutOfHeap within the heap', () => {
    const heapMockResult = {heapUsed: 500*1048576};
    process.memoryUsage = jest.fn().mockImplementationOnce(() => heapMockResult);
    expect(isMemoryRanOutOfHeap()).toBeFalsy();
  });
});