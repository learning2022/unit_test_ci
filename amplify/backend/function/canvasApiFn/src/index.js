/* eslint-disable */
/* Amplify Params - DO NOT EDIT
	API_LOREEV2API_CUSTOMBLOCKTABLE_ARN
	API_LOREEV2API_CUSTOMBLOCKTABLE_NAME
	API_LOREEV2API_CUSTOMTEMPLATETABLE_ARN
	API_LOREEV2API_CUSTOMTEMPLATETABLE_NAME
	API_LOREEV2API_GRAPHQLAPIIDOUTPUT
	API_LOREEV2API_KALTURACONFIGTABLE_ARN
	API_LOREEV2API_KALTURACONFIGTABLE_NAME
	API_LOREEV2API_LTIACCESSTOKENTABLE_ARN
	API_LOREEV2API_LTIACCESSTOKENTABLE_NAME
	API_LOREEV2API_LTIAPIKEYTABLE_ARN
	API_LOREEV2API_LTIAPIKEYTABLE_NAME
	API_LOREEV2API_LTIPLATFORMKEYTABLE_ARN
	API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME
	API_LOREEV2API_LTIPLATFORMTABLE_ARN
	API_LOREEV2API_LTIPLATFORMTABLE_NAME
	AUTH_LOREEV275F92347_USERPOOLID
	ENV
	REGION
	STORAGE_LOREEV2S35B8B11F1_BUCKETNAME
Amplify Params - DO NOT EDIT */
const AWSXRay = require('aws-xray-sdk');
AWSXRay.captureHTTPsGlobal(require('http'));
AWSXRay.captureHTTPsGlobal(require('https'));
AWSXRay.capturePromise();

const Modules = require('./Api/Modules');
const Pages = require('./Api/Pages');
const Items = require('./Api/Items');
const Assignments = require('./Api/Assignments');
const Discussions = require('./Api/Discussions');
const Quizzes = require('./Api/Quizzes');
const CourseNavigation = require('./Api/CourseNavigation');
const Announcements = require('./Api/Announcements');
const Files = require('./Api/Files');
const Images = require('./Api/Images');
const Publish = require('./Api/Publish');
const Create = require('./Api/Create');
const { addImagesToCanvas, uploadToS3 } = require('./fileUpload/ImageUpload');
const { addImagesToS3BucketAndUpdateHtml } = require('./service/imageHandling');
const Video = require('./fileUpload/Video');
const AdminDashboard = require('./Api/AdminDashboard');
const Migrate = require('./migrate/index');
const COGNITO_PLATFORM_KEY = 'custom:platform';

const resolvers = {
  Query: {
    canvasModules: ctx => {
      return Modules.list(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageIndex, ctx.arguments.pageItems);
    },
    canvasModuleItems: ctx => {
      return Modules.listItems(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.moduleId);
    },
    canvasPages: ctx => {
      const pageListOptions = {
      loreeUserEmail: ctx.identity.claims["email"],
      courseId: ctx.arguments.courseId,
      pageIndex: ctx.arguments.pageIndex,
      pageItems: ctx.arguments.pageItems,
      orderType: ctx.arguments.orderType,
      sortingValue: ctx.arguments.sortingValue
      };
      return Pages.list(pageListOptions);
    },
    canvasAssignments: ctx => {
      return Assignments.list(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    canvasDiscussions: ctx => {
      return Discussions.list(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageIndex, ctx.arguments.pageItems);
    },
    canvasQuizzes: ctx => {
      return Quizzes.list(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageIndex, ctx.arguments.pageItems);
    },
    canvasFiles: ctx => {
      return Files.list(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageIndex, ctx.arguments.pageItems);
    },

    canvasAnnouncements: ctx => {
      return Announcements.list(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageIndex, ctx.arguments.pageItems);
    },
    canvasCourseNavigation: ctx => {
      return CourseNavigation.list(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageIndex, ctx.arguments.pageItems);
    },
    canvasImages: ctx => {
      return Images.list(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    kalturaVideos: ctx => {
      return Video.list(ctx.arguments.courseId, ctx.arguments.platformId);
    },
    getKalturaVideo: ctx => {
      return Video.get(ctx.arguments.userId, ctx.arguments.entryId, ctx.arguments.platformId);
    },
    adminAccounts: ctx => {
      return AdminDashboard.accounts(ctx.identity.claims["email"]);
    },
    adminSubAccounts: ctx => {
      return AdminDashboard.subAccounts(ctx.identity.claims["email"], ctx.arguments.accountId);
    },
    adminRoles: ctx => {
      return AdminDashboard.roles(ctx.arguments.loreeUserEmail, ctx.arguments.accountId);
    },
    courseDetails: ctx => {
      return AdminDashboard.courses(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    accountByCourse: ctx => {
      return AdminDashboard.accountById(ctx.identity.claims["email"], ctx.arguments.accountId);
    },
    dashboardStatistics: ctx => {
      return AdminDashboard.dashboardStatistics(ctx.identity.claims[COGNITO_PLATFORM_KEY]);
    },
    canvasClient: ctx => {
      return AdminDashboard.client(ctx.arguments.platformId);
    },
    refreshPlatformKeys: ctx => {
      return AdminDashboard.generatekeys(ctx.arguments.platformId);
    },
    migrateUser: ctx => {
      return Migrate.getUserForMigration(ctx.arguments.email);
    },
  },
  Mutation: {
    createToCanvas: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        Name: ctx.arguments.Name,
        type: ctx.arguments.type,
        moduleId: ctx.arguments.moduleId,
      };
      return Create.moduleOrPage(data);
    },
    updateCanvasModule: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        moduleId: ctx.arguments.moduleId,
        moduleName: ctx.arguments.moduleName,
      };
      return Modules.update(data);
    },

    deleteCanvasModule: ctx => {
      return Modules.delete(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.moduleId);
    },
    viewCanvasPage: ctx => {
      return Pages.view(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageId);
    },
    viewCanvasAssignments: ctx => {
      return Assignments.view(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.assignmentId);
    },
    updateCanvasPage: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        pageUrl: ctx.arguments.pageUrl,
        pageName: ctx.arguments.pageName,
      };
      return Pages.update(data);
    },

    deleteCanvasPage: ctx => {
      return Pages.delete(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageUrl);
    },
    duplicateCanvasPage: ctx => {
      return Pages.duplicate(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.pageUrl);
    },
    saveCanvasPage: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        pageID: ctx.arguments.pageID,
        editorContent: ctx.arguments.editorContent,
        platformId: ctx.identity.claims[COGNITO_PLATFORM_KEY],
        uploadAndConvertFromBackendFeatureToggle: ctx.arguments.uploadAndConvertFromBackendFeatureToggle,
      };
      return Pages.save(data);
    },

    viewCanvasDiscussion: ctx => {
      return Discussions.view(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.discussionId);
    },
    saveCanvasDiscussion: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        discussionID: ctx.arguments.discussionID,
        editorContent: ctx.arguments.editorContent,
        platformId: ctx.identity.claims[COGNITO_PLATFORM_KEY],
        uploadAndConvertFromBackendFeatureToggle: ctx.arguments.uploadAndConvertFromBackendFeatureToggle,
      };
      return Discussions.save(data);
    },
    updateCanvasDiscussion: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        discussionId: ctx.arguments.discussionId,
        discussionName: ctx.arguments.discussionName,
      };
      return Discussions.update(data);
    },
    viewCanvasAssignment: ctx => {
      return Assignments.view(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.assignmentId);
    },
    updateCanvasAssignment: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        assignmentId: ctx.arguments.assignmentId,
        assignmentName: ctx.arguments.assignmentName,
      };
      return Assignments.update(data);
    },
    saveCanvasAssignment: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        assignmentID: ctx.arguments.assignmentID,
        editorContent: ctx.arguments.editorContent,
        platformId: ctx.identity.claims[COGNITO_PLATFORM_KEY],
        uploadAndConvertFromBackendFeatureToggle: ctx.arguments.uploadAndConvertFromBackendFeatureToggle,
      };
      return Assignments.save(data);
    },

    updateModuleItem: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        itemId: ctx.arguments.itemId,
        title: ctx.arguments.title,
        moduleId: ctx.arguments.moduleId,
      };
      return Items.updateModuleItem(data);
    },
    deleteModuleItem: ctx => {
      return Items.deleteModuleItem(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.moduleId, ctx.arguments.itemId);
    },
    duplicatePageFromModule: ctx => {
      return Items.duplicatePageFromModule(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.moduleId, ctx.arguments.pageUrl);
    },

    publishToCanvas: ctx => {
      const data = {
        loreeUserEmail: ctx.identity.claims["email"],
        courseId: ctx.arguments.courseId,
        moduleId: ctx.arguments.moduleId,
        itemId: ctx.arguments.itemId,
        pageId: ctx.arguments.pageId,
        assignmentId: ctx.arguments.assignmentId,
        discussionId: ctx.arguments.discussionId,
        quizId: ctx.arguments.quizId,
        status: ctx.arguments.status,
        type: ctx.arguments.type,
      };
      return Publish.publish(data);
    },
    imageUpload: ctx => {
      const data = {
        courseId: ctx.arguments.courseId,
        name: ctx.arguments.name,
        type: ctx.arguments.type,
        path: ctx.arguments.path,
        loreeUserEmail: ctx.identity.claims["email"],
      };
      return addImagesToCanvas(data);
    },
    imageS3Upload: ctx => {
      return uploadToS3(ctx.arguments.platformId, ctx.arguments.path, ctx.arguments.title, ctx.arguments.type);
    },
    uploadImagesToS3: ctx => {
      const data = {
        globalHtmlContent: ctx.arguments.globalHtmlContent,
        type: ctx.arguments.type,
        loreeUserEmail: ctx.identity.claims["email"],
        platform: ctx.identity.claims[COGNITO_PLATFORM_KEY],
      };
      return addImagesToS3BucketAndUpdateHtml(data);
    },
    videoUpload: ctx => {
      return Video.upload(ctx.arguments.courseId, ctx.arguments.path, ctx.arguments.name, ctx.arguments.platformId);
    },
  },
};

exports.handler = async event => {
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      return await resolver(event);
    }
  }
  throw new Error('Resolver not found.');
};
