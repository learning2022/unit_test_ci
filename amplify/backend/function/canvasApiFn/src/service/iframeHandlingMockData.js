const canvasSuccessOneIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const updatedCanvasSuccessOneIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class=\"loree-iframe-content-row row\" style=\"padding: 10px; position:relative; margin: 0px\"><div class=\"col-12 loree-iframe-content-column\" style=\"padding:10px;\"><div style=\"padding:10px; margin-bottom: 20px;\" class=\"loree-iframe-content-external-tool-wrapper \"><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe></div></div></div><script src=\"https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js\"></script>'
  }
}

const canvasSuccessMultipleIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe style=\"width: 779px; height: 600px;\" title=\"Elements of the National Quality Framework\" src=\"https://crystaldelta.instructure.com/courses/896/external_tools/retrieve?display=borderless&amp;resource_link_lookup_uuid=ea662f10-9926-42e9-8e86-6fdecc9beffe&amp;url=https%3A%2F%2Flimon.h5p.com%2Fcontent%2F1291511087806338699\" allowfullscreen=\"allowfullscreen\" allow=\"geolocation *; microphone *; camera *; midi *; encrypted-media *; autoplay *; clipboard-write *; display-capture *\"></iframe></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const updatadCanvasSuccessMultipleIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class=\"loree-iframe-content-row row\" style=\"padding: 10px; position:relative; margin: 0px\"><div class=\"col-12 loree-iframe-content-column\" style=\"padding:10px;\"><div style=\"padding:10px; margin-bottom: 20px;\" class=\"loree-iframe-content-external-tool-wrapper \"><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe style=\"width: 779px; height: 600px;\" title=\"Elements of the National Quality Framework\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291511087806338699\" allowfullscreen=\"allowfullscreen\" allow=\"geolocation *; microphone *; camera *; midi *; encrypted-media *; autoplay *; clipboard-write *; display-capture *\" data-tool-url=\"https://crystaldelta.instructure.com/courses/896/external_tools/retrieve?display=borderless&amp;resource_link_lookup_uuid=ea662f10-9926-42e9-8e86-6fdecc9beffe&amp;url=https%3A%2F%2Flimon.h5p.com%2Fcontent%2F1291511087806338699\"></iframe></div></div></div><script src=\"https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js\"></script>'
  }
}

const canvasSuccessMixedIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://test.instructure.com/courses?url=https://limon.test.com/content/1291487526232336489"></iframe><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src=\"https://stg.loree-interactive.crystaldelta.net/Button/6cf9b241751c1def19898d4fb7501ac6\"></iframe></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const updatedCanvasSuccessMixedIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class=\"loree-iframe-content-row row\" style=\"padding: 10px; position:relative; margin: 0px\"><div class=\"col-12 loree-iframe-content-column\" style=\"padding:10px;\"><div style=\"padding:10px; margin-bottom: 20px;\" class=\"loree-iframe-content-external-tool-wrapper \"><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://test.instructure.com/courses?url=https://limon.test.com/content/1291487526232336489"></iframe><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://stg.loree-interactive.crystaldelta.net/Button/6cf9b241751c1def19898d4fb7501ac6\"></iframe></div></div></div><script src=\"https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js\"></script>'
  }
}

const canvasSuccessWithoutIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const updatedCanvasSuccessWithoutIframe = {
  statusCode: 200,
  body: {
    url: 'test',
    title: 'test',
    created_at: '2022-02-10T03:13:59Z',
    editing_roles: 'teachers',
    page_id: 79561,
    last_edited_by: {
      id: 1052,
      anonymous_id: 't8',
      display_name: 'Arvind Kumar',
      avatar_image_url: 'https://crystaldelta.instructure.com/images/messages/avatar-50.png',
      html_url: 'https://crystaldelta.instructure.com/courses/777/users/1052',
      pronouns: null
    },
    published: true,
    hide_from_students: false,
    front_page: false,
    html_url: 'https://crystaldelta.instructure.com/courses/777/pages/test',
    todo_date: null,
    updated_at: '2022-04-05T14:42:33Z',
    locked_for_user: false,
    body: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const canvasSuccessDiscussionResponse = {
  statusCode: 200,
  body: { 
    url: 'test',
    title: 'test',
    message: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const updatedCanvasSuccessDiscussionResponse = {
  statusCode: 200,
  body: { 
    url: 'test',
    title: 'test',
    message: '<div class=\"loree-iframe-content-row row\" style=\"padding: 10px; position:relative; margin: 0px\"><div class=\"col-12 loree-iframe-content-column\" style=\"padding:10px;\"><div style=\"padding:10px; margin-bottom: 20px;\" class=\"loree-iframe-content-external-tool-wrapper \"><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe></div></div></div><script src=\"https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js\"></script>'
  }
}

const canvasSuccessAssignmentResponse = {
  statusCode: 200,
  body: { 
    url: 'test',
    title: 'test',
    description: '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="padding:10px; margin-bottom: 20px;" class="loree-iframe-content-external-tool-wrapper "><iframe title="external-tool" id="external-tool-id" class="loree-iframe-content-external-tool-element" frameborder="0" width="100%" scrolling="yes" height="400" src="https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe></div></div></div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js"></script>'
  }
}

const updatedCanvasSuccessAssignmentResponse = {
  statusCode: 200,
  body: { 
    url: 'test',
    title: 'test',
    description: '<div class=\"loree-iframe-content-row row\" style=\"padding: 10px; position:relative; margin: 0px\"><div class=\"col-12 loree-iframe-content-column\" style=\"padding:10px;\"><div style=\"padding:10px; margin-bottom: 20px;\" class=\"loree-iframe-content-external-tool-wrapper \"><iframe title=\"external-tool\" id=\"external-tool-id\" class=\"loree-iframe-content-external-tool-element\" frameborder=\"0\" width=\"100%\" scrolling=\"yes\" height=\"400\" src=\"https://loree-api.com//lti/tool?content_url=https://limon.h5p.com/content/1291487526232336489\" data-tool-url=\"https://crystaldelta.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe></div></div></div><script src=\"https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/364203/canvas.js\"></script>'
  }
}

module.exports = { 
  canvasSuccessOneIframe, 
  updatedCanvasSuccessOneIframe, 
  canvasSuccessMultipleIframe, 
  updatadCanvasSuccessMultipleIframe, 
  canvasSuccessMixedIframe, 
  updatedCanvasSuccessMixedIframe,
  canvasSuccessWithoutIframe,
  updatedCanvasSuccessWithoutIframe,
  canvasSuccessDiscussionResponse,
  updatedCanvasSuccessDiscussionResponse,
  canvasSuccessAssignmentResponse,
  updatedCanvasSuccessAssignmentResponse
};