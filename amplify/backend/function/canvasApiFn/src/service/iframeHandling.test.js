const { getElementsByTagAndUpdateHTML, getContentLink, getHtmlContentFromResponse } = require('./iframeHandling');
const {   
  canvasSuccessOneIframe, 
  updatedCanvasSuccessOneIframe, 
  canvasSuccessMultipleIframe, 
  updatadCanvasSuccessMultipleIframe, 
  canvasSuccessMixedIframe, 
  updatedCanvasSuccessMixedIframe,
  canvasSuccessWithoutIframe,
  updatedCanvasSuccessWithoutIframe,
  canvasSuccessDiscussionResponse,
  updatedCanvasSuccessDiscussionResponse,
  canvasSuccessAssignmentResponse,
  updatedCanvasSuccessAssignmentResponse }  = require('./iframeHandlingMockData');

describe('Replace loree url to canvas url', () => {
  const htmlContentInTheDocumentWithLoreeLtiURL = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" data-tool-url="https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe></div>`;

  const htmlContentInTheDocumentWithoutDataAtrrLoreeLtiURL = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe></div>`;

  const htmlContentInTheDocumentWithCanvasLtiURL = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" data-tool-url="https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489" src="https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe></div>`;

  const htmlContentInTheDocumentWithoutDataAttrCanvasLtiURL = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489"></iframe></div>`;

  const htmlContentInTheDocumentWithIframeURL = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://loree.instructure.com/courses/777/external_tools/retrieve"></iframe></div>`;

  const htmlContentInTheDocumentWithMultipleIframes = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" data-tool-url="https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe class="loree-iframe-content-interactive-element loree-style-d546ac" title="Test Button" id="Button_6cf9b241751c1def19898d4fb7501ac6" frameborder="0" width="100%" height="200" scrolling="no" src="https://stg.loree-interactive.crystaldelta.net/Button/6cf9b241751c1def19898d4fb7501ac6"></div>`;

  const updaetdHtmlContentInTheDocumentWithMultipleIframes = `<div><iframe style=\"width: 779px; height: 600px; max-width: 100%;\" title=\"Courses\" data-tool-url=\"https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\" src=\"https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe class=\"loree-iframe-content-interactive-element loree-style-d546ac\" title=\"Test Button\" id=\"Button_6cf9b241751c1def19898d4fb7501ac6\" frameborder=\"0\" width=\"100%\" height=\"200\" scrolling=\"no\" src=\"https://stg.loree-interactive.crystaldelta.net/Button/6cf9b241751c1def19898d4fb7501ac6\"></div></iframe></div>`;

  const htmlContentInTheDocumentWithoutDataAtrrMultipleLoreeLtiURL = `<div><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe><iframe style="width: 779px; height: 600px; max-width: 100%;" title="Courses" src="https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;content_url=https://limon.h5p.com/content/1291487526232336489"></iframe></div>`;
  
  const updatedHtmlContentInTheDocumentWithoutDataAtrrMultipleLoreeLtiURL = `<div><iframe style=\"width: 779px; height: 600px; max-width: 100%;\" title=\"Courses\" src=\"https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe style=\"width: 779px; height: 600px; max-width: 100%;\" title=\"Courses\" src=\"https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe style=\"width: 779px; height: 600px; max-width: 100%;\" title=\"Courses\" src=\"https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe><iframe style=\"width: 779px; height: 600px; max-width: 100%;\" title=\"Courses\" src=\"https://canvas.instructure.com/courses/777/external_tools/retrieve?display=borderless&amp;url=https://limon.h5p.com/content/1291487526232336489\"></iframe></div>`;

  test('updates canvas url when iframe has custom data attribute', () => {
    const dataParams = {
      globalHtmlContent: htmlContentInTheDocumentWithLoreeLtiURL,
      lmsUrl: 'https://canvas.instructure.com',
      searchAttr: 'src',
      searchValue: ['h5p.com/content'],
      searchTag: 'iframe',
      courseId: 777,
      searchParam: 'content_url',
      isLoreeToCanvas: true
    }
    expect(getElementsByTagAndUpdateHTML(dataParams)).toEqual(htmlContentInTheDocumentWithCanvasLtiURL);
  });
  test('return same html when iframe url not matches with external tool', async() => {
    const dataParams = {
      globalHtmlContent: htmlContentInTheDocumentWithIframeURL,
      lmsUrl: 'https://canvas.instructure.com',
      searchAttr: 'src',
      searchValue: ['h5p.com/content'],
      searchTag: 'iframe',
      courseId: 777,
      searchParam: 'content_url',
      isLoreeToCanvas: true
    }
    expect(getElementsByTagAndUpdateHTML(dataParams)).toEqual(htmlContentInTheDocumentWithIframeURL);
  });
  test('return same html when no iframe elements are found', async() => {
    const dataParams = {
      globalHtmlContent: `<div><p>hello</p><img src="https://crystaldelta.instructure.com/courses/875/files/373522/download"></div>`,
      lmsUrl: 'https://canvas.instructure.com',
      searchAttr: 'src',
      searchValue: ['h5p.com/content'],
      searchTag: 'iframe',
      courseId: 777,
      searchParam: 'content_url',
      isLoreeToCanvas: true
    }
    expect(getElementsByTagAndUpdateHTML(dataParams)).toEqual(`<div><p>hello</p><img src="https://crystaldelta.instructure.com/courses/875/files/373522/download"></div>`);
  });
  test('updates only lti tools url', async() => {
    const dataParams = {
      globalHtmlContent: htmlContentInTheDocumentWithMultipleIframes,
      lmsUrl: 'https://canvas.instructure.com',
      searchAttr: 'src',
      searchValue: ['h5p.com/content'],
      searchTag: 'iframe',
      courseId: 777,
      searchParam: 'content_url',
      isLoreeToCanvas: true
    }
    expect(getElementsByTagAndUpdateHTML(dataParams)).toEqual(updaetdHtmlContentInTheDocumentWithMultipleIframes);
  });
  test('updates all external tool loree urls', async() => {
    const dataParams = {
      globalHtmlContent: htmlContentInTheDocumentWithoutDataAtrrMultipleLoreeLtiURL,
      lmsUrl: 'https://canvas.instructure.com',
      searchAttr: 'src',
      searchValue: ['h5p.com/content'],
      searchTag: 'iframe',
      courseId: 777,
      searchParam: 'content_url',
      isLoreeToCanvas: true
    }
    expect(getElementsByTagAndUpdateHTML(dataParams)).toEqual(updatedHtmlContentInTheDocumentWithoutDataAtrrMultipleLoreeLtiURL);
  });
  test('updates canvas url when iframe url matches with external tool', () => {
    const dataParams = {
      globalHtmlContent: htmlContentInTheDocumentWithoutDataAtrrLoreeLtiURL,
      lmsUrl: 'https://canvas.instructure.com',
      searchAttr: 'src',
      searchValue: ['h5p.com/content'],
      searchTag: 'iframe',
      courseId: 777,
      searchParam: 'content_url',
      isLoreeToCanvas: true
    }
    expect(getElementsByTagAndUpdateHTML(dataParams)).toEqual(htmlContentInTheDocumentWithoutDataAttrCanvasLtiURL);
  });
});

describe('get lti content url', () => {
  test('returns selected lti content url', async () => {
    expect(getContentLink('https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless&content_url=https://limon.h5p.com/content/1291487526232336489', 'content_url', 'h5p.com/content')).toEqual(
      'https://limon.h5p.com/content/1291487526232336489',
    );
  });
  test('returns selected lti content url', async () => {
    expect(getContentLink('https://loree.instructure.com/courses/777/external_tools/retrieve?display=borderless', 'content_url', 'h5p.com/content')).toEqual(
      null
    );
  });
});

describe('Replace canvas url to loree url for pages', () => {
  process.env.LOREE_API_URL = 'https://loree-api.com/'
  test('updates canvas url to loree url', () => {
    expect(getHtmlContentFromResponse(canvasSuccessOneIframe, 'PAGES')).toEqual(updatedCanvasSuccessOneIframe);
  });
  test('updates all canvas url to loree urls', () => {
    expect(getHtmlContentFromResponse(canvasSuccessMultipleIframe, 'PAGES')).toEqual(updatadCanvasSuccessMultipleIframe);
  });
  test('updates only canvas external url to loree extrnal urls', () => {
    expect(getHtmlContentFromResponse(canvasSuccessMixedIframe, 'PAGES')).toEqual(updatedCanvasSuccessMixedIframe);
  });
  test('returns same content when no external tool urls found', () => {
    expect(getHtmlContentFromResponse(canvasSuccessWithoutIframe, 'PAGES')).toEqual(canvasSuccessWithoutIframe);
  });
  test('returns same response when the api is failed', () => {
    const canvasFailedResponse = {
      statusCode: 500,
      body: {
        error: 'error'
      }
    }
    expect(getHtmlContentFromResponse(canvasFailedResponse, 'PAGES')).toEqual(canvasFailedResponse);
  });
})

describe('Replace canvas url to loree url for assignments', () => {
  process.env.LOREE_API_URL = 'https://loree-api.com/'
  test('updates canvas url to loree url', () => {
    expect(getHtmlContentFromResponse(canvasSuccessAssignmentResponse, 'ASSIGNMENT')).toEqual(updatedCanvasSuccessAssignmentResponse);
  });
  test('returns same response when the api is failed', () => {
    const canvasFailedResponse = {
      statusCode: 500,
      body: {
        error: 'error'
      }
    }
    expect(getHtmlContentFromResponse(canvasFailedResponse, 'ASSIGNMENT')).toEqual(canvasFailedResponse);
  });
})

describe('Replace canvas url to loree url for discussions', () => {
  process.env.LOREE_API_URL = 'https://loree-api.com/'
  test('updates canvas url to loree url', () => {
    expect(getHtmlContentFromResponse(canvasSuccessDiscussionResponse, 'DISCUSSIONS')).toEqual(updatedCanvasSuccessDiscussionResponse);
  });
  test('returns same response when the api is failed', () => {
    const canvasFailedResponse = {
      statusCode: 500,
      body: {
        error: 'error'
      }
    }
    expect(getHtmlContentFromResponse(canvasFailedResponse, 'DISCUSSIONS')).toEqual(canvasFailedResponse);
  });
})