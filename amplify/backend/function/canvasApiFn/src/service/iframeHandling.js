const logger =  require('./logger').logger;
const { JSDOM } = require("jsdom");
const Request = require('../Api/Request');
const Database = require('../Database')

const replaceIframeUrls = async(globalHtmlContent, ltiPlatformId, loreeUserEmail, courseId) => {
  const accessToken = await Database.queryLtiAccessTokenByEmail(loreeUserEmail);;
  const dataParams = {
    globalHtmlContent: globalHtmlContent,
    lmsUrl: accessToken.lmsApiUrl,
    searchAttr: 'src',
    searchValue: [ 'h5p.com/content' ],
    searchTag: 'iframe',
    courseId: courseId,
    searchParam: 'content_url',
    isLoreeToCanvas: true
  };
  return getElementsByTagAndUpdateHTML(dataParams);
};

const getElementsByTagAndUpdateHTML = (params) => {
  const element = new JSDOM(params.globalHtmlContent).window.document.body;
  params.searchValue.forEach((searchValue) => {
    Object.values(element.getElementsByTagName(params.searchTag)).filter(elementByTag => elementByTag.hasAttribute(params.searchAttr))
    .filter(elementByTag => decodeURIComponent(elementByTag.getAttribute(params.searchAttr)).includes(searchValue))
    .forEach((elementByTag) =>{
      const link = setContentLink(elementByTag, params, searchValue);
      if(!params.isLoreeToCanvas){
        elementByTag.setAttribute('data-tool-url', elementByTag.getAttribute(params.searchAttr));
      }
      if(link){
        elementByTag.setAttribute('src', link);
      }
    });
  });
  return element.innerHTML;
};

const getContentLink = (iframeUrl, searchParam, searchValue) => {
  const url = new URL(iframeUrl);
  if (url.searchParams.get(searchParam) &&  url.searchParams.get(searchParam).includes(searchValue)) {
    const urlvalue = url.searchParams.get(searchParam);
    const contentUrl = new URL(urlvalue);
    return contentUrl.href;
  }
  logger.info('content link is not matched for the search filter');
  return null;
};

const setContentLink = (elementByTag, params, searchValue) => {
  const toolUrl = elementByTag.getAttribute('data-tool-url');
  if(params.isLoreeToCanvas && toolUrl){
    logger.info('data-tool-url attribute ' + toolUrl);
    return toolUrl;
  }
  const iframeUrl = decodeURIComponent(elementByTag.getAttribute('src'));
  const contentLink = getContentLink(iframeUrl, params.searchParam, searchValue);
  if(contentLink){
    if(params.isLoreeToCanvas) {
      return `${params.lmsUrl}/courses/${params.courseId}/external_tools/retrieve?display=borderless&url=${contentLink}`;
    }
    return `${params.lmsUrl}/lti/tool?content_url=${contentLink}`;
  }
  return null;
};

const getHtmlContentFromResponse = (response, type) => {
  if(response.statusCode !== 200){
    return response;
  }
  logger.info('response' + response.body.body);
  const dataParams = {
    globalHtmlContent: getCanvasAPIMethod(response, type),
    lmsUrl: process.env.LOREE_API_URL,
    searchAttr: 'src',
    searchValue: [ 'h5p.com/content' ],
    searchTag: 'iframe',
    searchParam: 'url',
    isLoreeToCanvas: false
  };
  const updatedResponse = getElementsByTagAndUpdateHTML(dataParams);
  logger.info('updatedResponse' + updatedResponse);
  if(type === 'PAGES'){
    response.body.body = updatedResponse;
  } else if(type === 'DISCUSSIONS') {
    response.body.message = updatedResponse;
  } else {
    response.body.description = updatedResponse;
  }
  return response;
};

const getCanvasAPIMethod = (response, type) => {
  if(type === 'PAGES'){
    return response.body.body;
  } else if(type === 'DISCUSSIONS') {
    return response.body.message;
  } else {
    return response.body.description;
  }
};

module.exports = {
  replaceIframeUrls,
  getElementsByTagAndUpdateHTML,
  getContentLink,
  getHtmlContentFromResponse
};