/* eslint-disable */
const request = require('request-promise');
const {addImagesToCanvas, uploadToS3} = require('../fileUpload/ImageUpload');
const logger =  require('./logger').logger;
const {getS3Key, getSignedUrl, updateImageUrlsInHtmlContent, updateImageUrlsInHtmlContentCustomBlock} = require('../helpers/utils');
const mapLimit = require('async/mapLimit');
const { JSDOM } = require("jsdom");
const Database = require('../Database');

const addImagesToCanvasAndUpdateHtml = async(globalHtmlContent, ltiPlatformId, loreeUserEmail, courseId) => {

  const BUCKETNAME = process.env.STORAGE_LOREEV2S35B8B11F1_BUCKETNAME;
  
  const s3ImageUrls = getImageElementsToUpload(globalHtmlContent, ltiPlatformId, 'src');
  logger.info('S3 Image URLs: ' + s3ImageUrls);

  if (s3ImageUrls.length === 0) {
    return globalHtmlContent;
  }

  const lmsUrlArray = Array(s3ImageUrls.length).fill("");

  return mapLimit(s3ImageUrls, 10, async function(image, callback){ // 500 units (700 units maximum)
      const imageUrlParams = image.imageUrl.split('?');
      const imageS3FileName = imageUrlParams[0].split('/').pop();
      const imageNameElements = imageS3FileName.split('-');

      let separateName = imageS3FileName;

      if(imageNameElements[0].length === 13 && new Date(parseInt(imageNameElements[0])).getTime() > 0) {
        imageNameElements.shift();
        separateName = imageNameElements.join('-');
      }
      let newImageUrl = image.imageUrl;
      if (newImageUrl.includes('https://loreev2storage')) {
        const imageKey = getS3Key(newImageUrl);
        newImageUrl = await getSignedUrl(decodeURI(imageKey), BUCKETNAME);
      }
      logger.info('<addImagesToCanvasAndUpdateHtml()> New S3 Signed Url: ' + newImageUrl);

      const requestParam = {
        loreeUserEmail: loreeUserEmail,
        courseId: courseId,
        name: separateName,
        type: 'image',
        path: newImageUrl,
      };
      const response = await addImagesToCanvas(requestParam);

      lmsUrlArray[image.index] = response.url ? response.url : "";

      
    }).then( (response) => {
      logger.info("image conversion response: " + response);
      logger.info('All files have been saved successfully ' + lmsUrlArray);

      const updateEditorContent = updateImageUrlsInHtmlContent(globalHtmlContent, s3ImageUrls, lmsUrlArray, [ ltiPlatformId ]);

      logger.info('Conversion result: ' + updateEditorContent);

      return updateEditorContent;
    }).catch( err => {
      logger.error(err);
      throw new Error('Error in parallel image upload');
    });
};

const getImageElementsToUpload = (globalHtmlContent, ltiPlatformId, attribute) => {
  const element = new JSDOM(globalHtmlContent).window.document.body;
  const filteredImageElementsToUpload = [];
  
    Object.values(element.getElementsByTagName('img')).filter(imageElement => imageElement.hasAttribute("src"))
    .filter(imageElement => imageElement.getAttribute('src').includes(ltiPlatformId))
    .map((imageElement, index) => filteredImageElementsToUpload.push({"index": index, "imageUrl": imageElement.getAttribute(attribute)}));

  return filteredImageElementsToUpload;
};

/** Save as Templates workflow */

const addImagesToS3BucketAndUpdateHtml = async uploadData => {
  const { globalHtmlContent, type, platform, loreeUserEmail }  = uploadData;
  const accessToken = await Database.queryLtiAccessTokenByEmail(loreeUserEmail);
  const imageSrcUrls = await getImageSourceUrls(globalHtmlContent, type, platform, accessToken.lmsApiUrl);

  if (imageSrcUrls.length === 0) {
    return globalHtmlContent;
  }

  let imageTitles = [];
  if (globalHtmlContent.includes('wysiwyg')) {
    imageTitles = getMigratedTemplateImageTitle(globalHtmlContent);
  }
  const element = new JSDOM(globalHtmlContent).window.document.body;
  let imageDisplayName;
  const s3UrlArray = Array(element.getElementsByTagName('img').length).fill("");

  return mapLimit(imageSrcUrls, 10, async function(imageSrcUrl, callback){
    const imageSourceCollection = element.getElementsByTagName("img");
    const imageAlt = imageSourceCollection[imageSrcUrl.index].hasAttribute('alt') ? imageSourceCollection[imageSrcUrl.index].getAttribute('alt'): '';
    if(isLmsCourseImage(imageSrcUrl.imageUrl, accessToken.lmsApiUrl) && imageTitles.length === 0) {
      imageDisplayName = await getCanvasFileDisplayName(imageSrcUrl.imageUrl, accessToken);
    } else if (type.includes('Global-') && !imageAlt.includes(type) && imageTitles.length === 0){
      imageDisplayName = imageAlt; 
    } else if(imageTitles.length > 0) {
      imageDisplayName = imageTitles[imageSrcUrl.index];
    }

    validImageNametoBeUploaded = getUniqueImageNameForS3Url(imageDisplayName);
    logger.info("Vaild file name before saving checkpoint 1.2: " + validImageNametoBeUploaded);
    
    const list = await uploadToS3(platform, imageSrcUrl.imageUrl, validImageNametoBeUploaded, type);

    s3UrlArray[imageSrcUrl.index] = list ? list.toString() : "";
  }).then( (response) => {
    logger.info("image conversion response: " + response);
    logger.info('All files have been saved successfully ' + s3UrlArray);

    const lmsIdentifier = [ accessToken.lmsApiUrl ];
    if (type.includes('Global-')) {
      lmsIdentifier.push(platform);
    }

    const updateEditorContent = updateImageUrlsInHtmlContentCustomBlock(
      globalHtmlContent,
      imageSrcUrls,
      s3UrlArray,
    );
    logger.info('Conversion result: ' + updateEditorContent);

    return updateEditorContent;
  }).catch( err => {
    logger.error(err);
    throw new Error('Error in parallel image upload');
  });
};

const getImageSourceUrls = async( htmlContent, type, platformId, lmsApiUrl ) => {
  let imageSrcUrls = [];
  const element = new JSDOM(htmlContent).window.document.body;

  const imageSourceCollection = element.getElementsByTagName("img");
  await Promise.all(
    Object.values(imageSourceCollection).map(async( imageElement, index ) => {
      const imageSource = imageElement.getAttribute('src');
      const updateImage = imageSource.replace(/&amp;/g, '&');
      if(isLmsCourseImage(imageSource, lmsApiUrl) ||
        imageSource.includes('wysiwyg')) {
        imageSrcUrls.push({"index": index, "imageUrl": updateImage});
      } else if (type.includes('Global-')) {
        const getImageSrcFromV1 =  await getImageSrcBasedOnUrl(imageSource, updateImage, type, platformId);
        imageSrcUrls.push({"index": index, "imageUrl": getImageSrcFromV1});
      }
    })
  );
  return imageSrcUrls;
};

const isLmsCourseImage = (imageUrl, lmsApiUrl) => {
  const imageUrlPath = imageUrl.split('/');
  const imageBaseUrl = imageUrlPath[0] + '//' + imageUrlPath[2];
  logger.info('imageBaseUrl', imageBaseUrl, 'lmsApiUrl', lmsApiUrl )
  if( imageBaseUrl === lmsApiUrl ) {
    return true;
  } else {
    return false;
  }
}

const getCanvasFileDisplayName = async ( imageSrcUrl, accessToken ) => {
  let fileDetails = '';
  let fileId = '';
  const imageSrcSplitUrl = imageSrcUrl.split('/');
  if(imageSrcSplitUrl[3] == 'courses') {
    const courseId = imageSrcSplitUrl[4];
    fileId = imageSrcSplitUrl[6];
    logger.info("This Url is a course based file url: " + courseId + " - " + fileId);
    fileDetails = await getFileDetailsFromCanvasCourse(fileId, courseId, accessToken);        
  } else {
    fileId = imageSrcSplitUrl[4];
    logger.info("This Url is a pure file url: " + fileId);
    fileDetails = await getFileDetailsfromCanvasFiles(fileId, accessToken);
  }
  const fileDetailsResponse = JSON.parse(fileDetails);
  logger.info('File display name with extension(display_name): ' + fileDetailsResponse.display_name);
  return fileDetailsResponse.display_name;
}

const getFileDetailsFromCanvasCourse = async(fileId, courseId, accessToken) => {
  try{
    const getFileInfoOptions = {
      method: 'GET',
      url: `${accessToken.lmsApiUrl}/api/v1/courses/${courseId}/files/${fileId}`,
      headers: {
        authorization: `Bearer ${accessToken.accessToken}`,
      }
    };
    logger.info(getFileInfoOptions);
    return request(getFileInfoOptions);
  } catch(e){
    logger.error('File info cannot be retrieved ' + e);
    throw new Error('Unable to get file info');
  }
};

const getFileDetailsfromCanvasFiles = async(fileId, accessToken) => {
  try{
    const getFileInfoOptions = {
      method: 'GET',
      url: `${accessToken.lmsApiUrl}/api/v1/files/${fileId}`,
      headers: {
        authorization: `Bearer ${accessToken.accessToken}`,
      }
    };
    logger.info(getFileInfoOptions);
    return request(getFileInfoOptions);
  } catch(e){
    logger.error('File info cannot be retrieved ' + e);
    throw new Error('Unable to get file info');
  }
};

const getImageSrcBasedOnUrl = async (image, updateImage, type, platformId) => {
  const BUCKETNAME = process.env.STORAGE_LOREEV2S35B8B11F1_BUCKETNAME;
  if (image.includes('https://loreev2storage') && !image[1].includes(type)) {
    const imageKey = getS3Key(updateImage);
    return getSignedUrl(decodeURI(imageKey), BUCKETNAME);
  } else if (!image.includes('https://loreev2storage') && image.includes(platformId)) {
    return getSignedUrl(updateImage, BUCKETNAME);
  }
  throw new Error("Image source url with platformId not identified");
};

const getUniqueImageNameForS3Url = ( imageDisplayName ) => {
  const timestamp = Date.now();
  const imageNameContents = imageDisplayName.split('-');
  if(imageNameContents[0].length === 13 && new Date(parseInt(imageNameContents[0])).getTime() > 0) {
    imageNameContents[0] = timestamp;
    return imageNameContents.join('-');
  } else {
    return timestamp + '-' + imageDisplayName;
  }
};

const getMigratedTemplateImageTitle = ( htmlContent ) => {
  const element = new JSDOM(htmlContent).window.document.body;
  return Object.values(element.getElementsByTagName('img'))
  .filter(imageElement => imageElement.hasAttribute("src"))
  .map(imageElement => imageElement.getAttribute("src").split('/').pop());
};

module.exports = {
  addImagesToCanvasAndUpdateHtml,
  getImageElementsToUpload,
  addImagesToS3BucketAndUpdateHtml,
  getImageSourceUrls,
  getCanvasFileDisplayName,
  getImageSrcBasedOnUrl,
  getUniqueImageNameForS3Url,
  getMigratedTemplateImageTitle,
  isLmsCourseImage,
};