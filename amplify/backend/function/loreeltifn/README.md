## loree lti function

#### Typescript

Combining typescript with amplify is a bit fiddly, due to amplify requiring specific paths for its build and deploy process.

To satisfy amplify, we keep javascript & typescript in `real-src`, and the compiled code goes into `src` for amplify to deploy.

Additionally:
 - `loreeltifn/src/package.json` must be in src to create node_modules
 - `loreeltifn/src/node_modules` must be in src to be deployed by amplify
 - `loreeltifn/src/tsconfig` overrides `*` to `src/node_modules` to satisfy dev/build time import paths.
 - `loreeltifn/.gitignore` excludes `src/**` but manually includes things like `package.json` and `yarn.lock`
