const AWSXRay = require('aws-xray-sdk');
const AWS = AWSXRay.captureAWS(require('aws-sdk'));
const crypto = require('crypto');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18' });
const USERPOOLID = process.env.AUTH_LOREEV275F92347_USERPOOLID;
class CognitoAuth {
  async getUser(email, name, platformId, lmsEmail) {
    const _USERNAME = email;
    const _NAME = name;
    const _PLATFORM_ID = platformId;
    const _EMAIL = lmsEmail;
    var _PASSWORD = await this.randomPassword();
    try {
      var getUserParams = {
        UserPoolId: USERPOOLID,
        Username: _USERNAME,
      };
      const user = await cognitoidentityserviceprovider.adminGetUser(getUserParams).promise();
      console.log('GET cognito User', user);
      await this.setPassword(_USERNAME, _PASSWORD);
      return _PASSWORD;
    } catch (err) {
      console.log('GET user Error', err);
      if (err.code === 'UserNotFoundException') {
        let createUser = await this.createUser(_USERNAME, _NAME, _PLATFORM_ID, _PASSWORD, _EMAIL);
        console.log('Created User', createUser);
        await this.setPassword(_USERNAME, _PASSWORD);
        return _PASSWORD;
      }
    }
  }
  async updateUser(_USERNAME, _EMAIL) {
    console.log('User password', _EMAIL, _USERNAME);
    var updateUserParams = {
      UserPoolId: USERPOOLID,
      Username: _USERNAME,
      UserAttributes: [
        {
          Name: 'custom:lms_email',
          Value: _EMAIL,
        },
      ],
    };
    try {
      return await cognitoidentityserviceprovider.adminUpdateUserAttributes(updateUserParams).promise();
    } catch (err) {
      console.log('Error in adminUpdateUser. err:', err);
    }
  }

  async createUser(_USERNAME, _NAME, _PLATFORM_ID, _PASSWORD, _EMAIL) {
    console.log('**********Create User ***********', _USERNAME, _NAME, _PLATFORM_ID, _EMAIL);
    var createUserParams = {
      UserPoolId: USERPOOLID,
      MessageAction: 'SUPPRESS',
      Username: _USERNAME,
      TemporaryPassword: _PASSWORD,
      UserAttributes: [
        {
          Name: 'name',
          Value: _NAME,
        },
        {
          Name: 'custom:platform',
          Value: _PLATFORM_ID,
        },
        {
          Name: 'custom:lms_email',
          Value: _EMAIL,
        },
      ],
    };
    try {
      const createCognitoUser = await cognitoidentityserviceprovider.adminCreateUser(createUserParams).promise();
      console.log('******Enter into try block******', createCognitoUser);
      return createCognitoUser;
    } catch (err) {
      console.log('Error in adminCreateUser. err:', err);
    }
  }

  async setPassword(_USERNAME, _PASSWORD) {
    console.log('User password', _PASSWORD);
    var setPasswordParams = {
      UserPoolId: USERPOOLID,
      Username: _USERNAME,
      Password: _PASSWORD,
      Permanent: true,
    };
    try {
      await cognitoidentityserviceprovider.adminSetUserPassword(setPasswordParams).promise();
    } catch (err) {
      console.log('Error in adminSetUserPassword. err:', err);
    }
  }

  async randomPassword() {
    var maxLength = 12;
    var minLength = 8;
    var randomLength = (await Math.floor((crypto.randomBytes(4).readUInt32LE() / 4294967295) * (maxLength - minLength))) + minLength;
    const random_letters = await this.generateRandomChar(
      '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#%&',
      randomLength,
    );
    const random_alphabets = await this.generateRandomChar('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', randomLength);
    const random_numbers = await this.generateRandomChar('0123456789', 3);
    const random_symbols = await this.generateRandomChar('!@#%&', 2);
    const pwd = random_letters + random_alphabets + random_numbers + random_symbols;
    console.log('Random password', pwd);
    return pwd;
  }

  async generateRandomChar(characters, maxLength) {
    return await Array(maxLength)
      .fill(characters)
      .map(function (x) {
        return x[Math.floor((crypto.randomBytes(4).readUInt32LE() / 4294967295) * x.length)];
      })
      .join('');
  }
}

module.exports = new CognitoAuth();
