const axios = require('axios');
const Jwk = require('rasha');
const jwt = require('jsonwebtoken');
const Database = require('./Database');
// import axios from "axios";
// import Jwk from "rasha";
// import jwt from "jsonwebtoken"
// import Database from "./Database";
import ExecTimer from "./ExecTimer";

type ValidationParameters = {
  alg: string;
  maxAge: number;
  iss: string;
  browser: string;

}

enum AuthMethod {
  JWK_SET = "JWK_SET",
  JWK_KEY = "JWK_KEY",
  RSA_KEY = "RSA_KEY",
}

type AuthConfig = {
  method: AuthMethod,
  key: string
}

type Platform = {
  authEndpoint: string,
  __typename: 'LtiPlatform',
  accesstokenEndpoint: string,
  updatedAt: string,
  createdAt: string,
  platformName: string,
  platformUrl: string,
  id: string,
  clientId: string,
  authConfig: AuthConfig,
  googleAnalyticsTrackingId: string // does this exist?!
}

type CachedTokenKeys = {
  [AuthMethod.JWK_SET]: KeySet | null
  [AuthMethod.JWK_KEY]: string | null
  [AuthMethod.RSA_KEY]: string | null
}

type KeySetItem = {
  kid: string;
}

type KeySet = [KeySetItem]|undefined

type JwtToken = {
  'https://purl.imsglobal.org/spec/lti/claim/message_type': 'LtiResourceLinkRequest',
  'https://purl.imsglobal.org/spec/lti/claim/version': '1.3.0',
  'https://purl.imsglobal.org/spec/lti/claim/resource_link': {
    id: string|null,
    description: string|null,
    title: string|null,
    validation_context: string|null,
    errors: [any]
  },
  aud: string,
  azp: string,
  'https://purl.imsglobal.org/spec/lti/claim/deployment_id': string,
  exp: number,
  iat: number,
  iss: 'https://canvas.instructure.com',
  nonce: string,
  sub: string,
  'https://purl.imsglobal.org/spec/lti/claim/target_link_uri': 'https://k6ep2vsy5i.execute-api.ap-southeast-2.amazonaws.com/camdev/lti/auth',
  picture: 'https://canvas.instructure.com/images/messages/avatar-50.png',
  email: string,
  name: string,
  given_name: string,
  family_name: string,
  'https://purl.imsglobal.org/spec/lti/claim/lis': {
    person_sourcedid: string,
    course_offering_sourcedid: string|null,
    validation_context: string|null,
    errors: [Object]
  },
  'https://purl.imsglobal.org/spec/lti/claim/context': {
    id: '3f32f0d64e8931e2402568b1ebc01362ffba98a3',
    label: 'Having a blast',
    title: 'Canvas Sandbox',
    type: [any],
    validation_context: null,
    errors: [any]
  },
  'https://purl.imsglobal.org/spec/lti/claim/tool_platform': {
    guid: 'Xe2I0bpLXro2py69QEfbbmvUbau5VAzBQYUq1bUh:canvas-lms',
    name: 'Crystal Delta',
    version: 'cloud',
    product_family_code: 'canvas',
    validation_context: null,
    errors: [any]
  },
  'https://purl.imsglobal.org/spec/lti/claim/launch_presentation': {
    document_target: 'iframe',
    height: number,
    width: number,
    return_url: 'https://crystaldelta.instructure.com/courses/803/external_content/success/external_tool_redirect',
    locale: 'en-GB',
    validation_context: null,
    errors: [any]
  },
  locale: 'en-GB',
  'https://purl.imsglobal.org/spec/lti/claim/roles': [
    'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator',
    'http://purl.imsglobal.org/vocab/lis/v2/membership#ContentDeveloper',
    'http://purl.imsglobal.org/vocab/lis/v2/system/person#User'
  ],
  'https://purl.imsglobal.org/spec/lti/claim/custom': {
    roles: string,
    user_id: number,
    course_id: number,
    is_root_account_admin: true
  },
  'https://purl.imsglobal.org/spec/lti/claim/lti11_legacy_user_id': '2c8da6cc7db0854f803aff70fb4cb488ff4e7146',
  'https://purl.imsglobal.org/spec/lti/claim/lti1p1': {
    user_id: string,
    validation_context: null,
    errors: [any]
  },
  errors: { errors: any },
  'https://www.instructure.com/placement': string
}

type JwtData = {
  header: {
    typ: 'JWT',
    alg: string, // 'RS256',
    kid: string,
  },
  payload: JwtToken,
  signature: string
}

// Authentication class manages RSA keys and validation of tokens.
class Auth {

  cachedTokenKeys:CachedTokenKeys = {
   [AuthMethod.JWK_SET]: null,
   [AuthMethod.JWK_KEY]: null,
   [AuthMethod.RSA_KEY]: null,
  }

  // Resolves a promisse if the token is valid following LTI 1.3 standards.
  async validateToken(token:string, validationParameters:ValidationParameters) {
    const decoded = jwt.decode(token, { complete: true }) as JwtData;
    if (!decoded) {
      throw new Error('INVALID_JWT_RECEIVED');
    }

    console.log('Decoded data: ', decoded);

    const kid = decoded.header.kid;
    validationParameters.alg = decoded.header.alg;
    validationParameters.maxAge = 200;

    console.log('Attempting to validate iss claim');
    console.log('Request Iss claim: ' + validationParameters.iss);
    console.log('Response Iss claim: ' + decoded.payload.iss);

    // Due to default enable of prevent cross-site tracking in Safari,
    // it is not allowing the cross-site cookie by default,
    // so skipping cookie validation for safari
    if (validationParameters.browser != 'safari' && !validationParameters.iss && validationParameters.iss !== decoded.payload.iss) {
      throw new Error('ISS_CLAIM_DOES_NOT_MATCH');
    }

    console.log('Attempting to retrieve registered platform');
    console.log('decoded.payload.aud:', decoded.payload.aud);
    let platform;
    if (!Array.isArray(decoded.payload.aud)) {
      platform = await Database.getPlatform(decoded.payload.iss, decoded.payload.aud) as Platform;
      console.log('Platform received:', platform);
    } else {
      for (const aud of decoded.payload.aud) {
        platform = await Database.getPlatform(decoded.payload.iss, aud) as Platform;
        if (platform) break;
      }
    }
    console.log('Platform:', platform);
    if (!platform) throw new Error('UNREGISTERED_PLATFORM');

    const authConfig = platform.authConfig;

    switch (authConfig.method) {
      case AuthMethod.JWK_SET: {        
        return await this.fetchAndVerifyJwkKeySet(authConfig, token, validationParameters, platform, kid)
    }
      case AuthMethod.JWK_KEY: {
        return this.verifyForJwkKey(token, validationParameters, platform, authConfig);
      }
      case AuthMethod.RSA_KEY: {
        return this.verifyForRsaKey(token, validationParameters, platform, authConfig);
      }
      default: {
        console.log('No auth configuration found for platform');
        throw new Error('AUTHCONFIG_NOT_FOUND');
      }
    }
  }

  async fetchAndVerifyJwkKeySet(authConfig:AuthConfig, token:string, validationParameters:ValidationParameters, platform:Platform, kid:string) {
    console.log('Retrieving key from jwk_set');
    if (!kid) {
      throw new Error('KID_NOT_FOUND');
    }
    
    const useCaching = true;
    ExecTimer.start(`fetchAndVerifyJwkKeySet-cache-${useCaching}`);
    if(useCaching) {
      try {
        const cachedKeySet = this.cachedTokenKeys[authConfig.method] as KeySet;
        if(cachedKeySet) {
          const result = await this.verifyForJwkKeySet(token, validationParameters, platform, cachedKeySet, kid);
          console.log("Cached KeySet - cache hit")
          ExecTimer.end();
          return result;
        }
        console.log("Cached KeySet - cache miss")
      } catch(e) {
        console.log("Cached KeySet - key invalid")
        // first time, or cached key has expired, must retrieve from endpoint
      }
    }
    const keySet = await this.retrieveFromKeySetEndpoint(authConfig);
    const result = await this.verifyForJwkKeySet(token, validationParameters, platform, keySet, kid);
    if(useCaching) {
      this.cachedTokenKeys[authConfig.method] = keySet;
    }
    ExecTimer.end();
    return result;
  }

  async retrieveFromKeySetEndpoint(authConfig:AuthConfig) {
    const keysEndpoint = authConfig.key;
    const res = await axios.get(keysEndpoint);
    console.log(`Received keys: ${res.data.keys} from ${keysEndpoint}`);
    return res.data.keys;
  }

  async verifyForJwkKeySet(token:string, validationParameters:ValidationParameters, platform:Platform, keySet:KeySet, kid:string) {
    if (!keySet) {
      throw new Error('KEYSET_NOT_FOUND');
    }
    const jwk = keySet?.find(key => key.kid === kid);
    if (!jwk) {
      throw new Error('KEY_NOT_FOUND');
    }
    console.log('Converting JWK key to PEM key');
    const key = await Jwk.export({ jwk: jwk });
    const verified = await this.verifyToken(token, key, validationParameters, platform);
    return verified;
  }

  // FIXME CamS is this ever called? Couldn't find calls in dev logs.
  // It uses `authConfig.key` as key, whereas retrieveFromKeySetEndpoint() uses authConfig.key as an endpoint:
  // EG: let keySet = get(authConfig.key).data.keys
  async verifyForJwkKey(token:string, validationParameters:ValidationParameters, platform:Platform, authConfig:AuthConfig) {
    console.log('Retrieving key from jwk_key');
    if (!authConfig.key) {
      throw new Error('KEY_NOT_FOUND');
    }
  
    const key = Jwk.jwk2pem(authConfig.key);
  
    const verified = await this.verifyToken(token, key, validationParameters, platform);
    return verified;
  }

  // FIXME CamS is this ever called? Couldn't find calls in dev logs.
  // It uses `authConfig.key` as key, whereas retrieveFromKeySetEndpoint() uses authConfig.key as an endpoint:
  // EG: let keySet = get(authConfig.key).data.keys
  async verifyForRsaKey(token:string, validationParameters:ValidationParameters, platform:Platform, authConfig:AuthConfig) {
    console.log('Retrieving key from rsa_key');
    const key = authConfig.key;
    if (!key) {
      throw new Error('KEY_NOT_FOUND');
    }

    const verified = await this.verifyToken(token, key, validationParameters, platform);
    return verified;
  }

  // Verifies a token.
  // token, key, validationParameters, platform
  async verifyToken(token:string, key:string, validationParameters:ValidationParameters, platform:Platform) {
    console.log('Attempting to verify JWT with the given key');
    const verified = jwt.verify(token, key, { algorithms: [validationParameters.alg] });
    await this.oidcValidation(verified, platform, validationParameters);
    await this.claimValidation(verified);
    verified.clientId = await platform.clientId;
    verified.platformId = await platform.id;
    verified.trackingId = await platform.googleAnalyticsTrackingId;
    return verified;
  }

  // Validates de token based on the OIDC specifications.
  async oidcValidation(token:JwtToken, platform:Platform, validationParameters:ValidationParameters) {
    console.log('Token signature verified');
    console.log('Initiating OIDC aditional validation steps');

    const aud = this.validateAud(token, platform);
    const alg = this.validateAlg(validationParameters.alg);
    const maxAge = this.validateMaxAge(token, validationParameters.maxAge);
    const nonce = this.validateNonce(token); // TODO: Update Nonce

    return Promise.all([aud, alg, maxAge, nonce]);
  }

  // Validates Aud.
  async validateAud(token:JwtToken, platform:Platform) {
    console.log("Validating if aud (Audience) claim matches the value of the tool's clientId given by the platform");
    console.log('Aud claim: ' + token.aud);
    console.log("Tool's clientId: " + (await platform.clientId));
    if (Array.isArray(token.aud)) {
      console.log('More than one aud listed, searching for azp claim');
      if (token.azp && token.azp !== (await platform.clientId)) throw new Error('AZP_DOES_NOT_MATCH_CLIENTID');
    }
    return true;
  }

  // Validates Aug.
  async validateAlg(alg:string) {
    console.log('Checking alg claim. Alg: ' + alg);
    if (alg !== 'RS256') throw new Error('ALG_NOT_RS256');
    return true;
  }

  // Validates token max age.
  async validateMaxAge(token:JwtToken, maxAge:number) {
    console.log('Max age parameter: ', maxAge);
    if (!maxAge) return true;
    console.log('Checking iat claim to prevent old tokens from being passed.');
    console.log('Iat claim: ' + token.iat);
    console.log('Exp claim: ' + token.exp);
    const curTime = Date.now() / 1000;
    console.log('Current_time: ' + curTime);
    const timePassed = curTime - token.iat;
    console.log('Time passed: ' + timePassed);
    if (timePassed > maxAge) throw new Error('TOKEN_TOO_OLD');
    return true;
  }

  // Validates Nonce.
  async validateNonce(token:JwtToken) {
    console.log('Validating nonce');
    console.log('Nonce: ' + token.nonce);

    if (await Database.getNonce(token.nonce)) throw new Error('NONCE_ALREADY_RECEIVED');
    console.log('Storing nonce');
    await Database.insertNonce(token.nonce);

    return true;
  }

  // Validates de token based on the LTI 1.3 core claims specifications.
  async claimValidation(token:JwtToken) {
    console.log('Initiating LTI 1.3 core claims validation');

    console.log('Checking Message type claim');
    if (
      token['https://purl.imsglobal.org/spec/lti/claim/message_type'] !== 'LtiResourceLinkRequest' &&
      token['https://purl.imsglobal.org/spec/lti/claim/message_type'] !== 'LtiDeepLinkingRequest'
    )
      throw new Error('NO_MESSAGE_TYPE_CLAIM');

    if (token['https://purl.imsglobal.org/spec/lti/claim/message_type'] === 'LtiResourceLinkRequest') {
      console.log('Checking Target Link Uri claim');
      if (!token['https://purl.imsglobal.org/spec/lti/claim/target_link_uri']) throw new Error('NO_TARGET_LINK_URI_CLAIM');

      console.log('Checking Resource Link Id claim');
      if (
        !token['https://purl.imsglobal.org/spec/lti/claim/resource_link'] ||
        !token['https://purl.imsglobal.org/spec/lti/claim/resource_link'].id
      )
        throw new Error('NO_RESOURCE_LINK_ID_CLAIM');
    }

    console.log('Checking LTI Version claim');
    if (!token['https://purl.imsglobal.org/spec/lti/claim/version']) throw new Error('NO_LTI_VERSION_CLAIM');
    if (token['https://purl.imsglobal.org/spec/lti/claim/version'] !== '1.3.0') throw new Error('WRONG_LTI_VERSION_CLAIM');

    console.log('Checking Deployment Id claim');
    if (!token['https://purl.imsglobal.org/spec/lti/claim/deployment_id']) throw new Error('NO_DEPLOYMENT_ID_CLAIM');

    console.log('Checking Sub claim');
    if (!token.sub) throw new Error('NO_SUB_CLAIM');

    console.log('Checking Roles claim');
    if (!token['https://purl.imsglobal.org/spec/lti/claim/roles']) throw new Error('NO_ROLES_CLAIM');
  }

}

export = new Auth();
