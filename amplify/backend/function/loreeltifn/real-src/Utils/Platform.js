const Request = require('./Request');
const Database = require('./Database');
const Auth = require('./Auth');
const url = require('fast-url-parser');
const Authorise = require('../lms/Authorise');
const crypto = require('crypto');

const cookieOptions = {
  secure: true,
  sameSite: 'None',
  signed: true,
  maxAge: 60000, // Adding max age to state cookie = 1min
};

const TOKEN_KEY_KIND = {
  JWK_SET: 'JWK_SET',
  JWK_KEY: 'JWK_KEY',
  RSA_KEY: 'RSA_KEY',
}

const StoredTokenKeys = {
  JWK_SET: null,
  JWK_KEY: null,
  RSA_KEY: null,
}

class Platform {
  async launchVerify(params, res) {
    try {
      // check the required params is present
      console.log('Received params in launchAuth: Params', params, res);
      const bbUrl = 'https://blackboard.com';
      if (params.iss === bbUrl) params.client_id = params.lti_deployment_id;
      if (!params.iss || !params.login_hint || !params.target_link_uri || !params.client_id)
        return res.status(401).send('MISSING_PARAMETERS');
      const iss = params.iss;
      console.log('params.client_id', params, params.login_hint, params.target_link_uri);
      console.info('Receiving a login request from: ' + iss);
      let platform;
      if (params.iss === bbUrl) {
        platform = await Database.getBBPlatform(iss, params.lti_deployment_id);
      } else {
        platform = await Database.getPlatform(iss, params.client_id);
      }

      console.log('platform', platform);
      if (platform) {
        console.info('Redirecting to platform authentication endpoint');

        // Create state parameter used to validade authentication response
        const state = encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``);
        console.info('Generated state: ', state); // Setting up validation info

        // Setting up validation info
        console.log('Sending Cookie. cookieOptions:', cookieOptions);
        res.cookie('state' + state, iss, cookieOptions);

        if (params.iss === bbUrl) params.client_id = platform.clientId;
        // Redirect to authentication endpoint
        const query = await Request.ltiAdvantageLogin(params, state);
        console.log('platform.authEndpoint', await platform.authEndpoint);
        console.info('Login request. Query: ', query);
        const redirectUrl = url.format({
          pathname: await platform.authEndpoint,
          query: query,
        });
        console.log('QUERY URL', redirectUrl);
        res.redirect(redirectUrl);
        // }
      } else {
        console.log('Unregistered platform attempting connection: ' + iss);
        return res.status(401).send('UNREGISTERED_PLATFORM');
      }
    } catch (err) {
      console.log(err);
      res.sendStatus(400);
    }
  }

  async authVerify(req, res) {
    try {
      console.log('******Enter into auth verify******');
      let platformDomain;
      // Retrieving cookies
      const cookies = req.signedCookies;
      console.log('Cookies received: ', cookies);

      const idtoken = req.body.id_token;

      if (idtoken) {
        console.log('Received idtoken for validation');
        // Retrieves state
        const state = req.body.state;

        // Retrieving validation parameters from cookies
        console.log('Response state: ' + state);
        var ua = req.headers['user-agent'];
        var browser = '';
        if (/safari/i.test(ua)) {
          browser = 'safari';
        }
        console.log('User agent', ua);
        console.log('Browser', browser);
        const validationCookie = cookies['state' + state];
        const validationParameters = {
          iss: validationCookie,
          maxAge: 20,
          browser: browser,
        };

        // ------------
        const valid = await Auth.validateToken(idtoken, validationParameters);

        res.clearCookie('state' + state, cookieOptions);
        console.log('Successfully validated token!');
        console.log('Valid token:', valid);
        if ((valid['iss'] === 'https://canvas.instructure.com') ||
          (valid['iss'] === 'https://canvas.beta.instructure.com') ||
          (valid['iss'] === 'https://canvas.test.instructure.com')) {
          platformDomain = 'canvas';
        } else if (valid['iss'] === 'https://blackboard.com') {
          platformDomain = 'BB';
        } else {
          // iss: 'https://crystaldelta.d2l-partners.brightspace.com'
          // iss: 'https://synthesis.brightspace.com' it will change for every clinet
          platformDomain = 'D2l';
        }
        await this.insertLog(req, valid, platformDomain);
        await Authorise.authLogin(valid, req, res, platformDomain);
      }
    } catch (err) {
      console.log(err);
      res.sendStatus(400);
    }
  }

  insertLog = async (req, params, platformDomain) => {
    let data = params['https://purl.imsglobal.org/spec/lti/claim/custom'];
    let user_id;
    let course_id;
    if (platformDomain === 'canvas') {
      data = params['https://purl.imsglobal.org/spec/lti/claim/custom'];
      user_id = data.user_id;
      course_id = data.course_id;
    } else if (platformDomain === 'D2l') {
      data = params['http://www.brightspace.com'];
      let courseData = params['https://purl.imsglobal.org/spec/lti/claim/context'];
      user_id = data.user_id;
      course_id = courseData.id;
    } else if (platformDomain === 'BB') {
      data = params['https://purl.imsglobal.org/spec/lti/claim/custom'];
      user_id = data.user_id;
      course_id = data.course_id;
    }
    const userIP = req.headers['x-forwarded-for'].split(',')[0];
    let clientDetails = {
      browserAgent: req.headers['user-agent'],
      browserDomain: req.headers.origin,
      userIpAddress: userIP,
    };
    let userInfo = {
      user_id: user_id,
      user_name: params.name,
      email: params.email,
      course_id: course_id,
      ltiClientID: params.clientId,
    };
    await Database.insertLtiLog(params.platformId, userInfo, clientDetails, userIP, 'Launched Loree application');
  };
}

module.exports = new Platform();
