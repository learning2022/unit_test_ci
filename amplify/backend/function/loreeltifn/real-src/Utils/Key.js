const jose = require('node-jose');
const fs = require('fs');
const Logger = require('./Logger').logger;

generateKeys = async (res) => {
  const keyStore = jose.JWK.createKeyStore()
  keyStore.generate('RSA', 2048, {alg: 'RS256', use: 'sig' })
  .then(result => {
    fs.writeFileSync(
      'keys.json', 
      JSON.stringify(keyStore.toJSON(true), null, '  ')
    )
  })
  getKeyStore(res);
}

getKeyStore = async (res) => {
  const keyFile = fs.readFileSync('keys.json');
  Logger.info('key file path', keyFile);
  const keyStore = await jose.JWK.asKeyStore(keyFile.toString());
  res.send(keyStore.toJSON());
}

module.exports = { generateKeys, getKeyStore };