const AWSXRay = require('aws-xray-sdk');
const AWS = AWSXRay.captureAWS(require('aws-sdk'));
const logger = require('./Logger').logger;
const logAndThrowError = require('./Util').logAndThrowError;
const docClient = new AWS.DynamoDB.DocumentClient();

class Database {
  async getNonce(nonce) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTINONCETABLE_NAME,
      Key: {
        id: nonce,
      },
    };

    try {
      const data = await docClient.get(params).promise();
      console.log('Get Nonce. Data :', data);
      if (data.item) return true;
      return false;
    } catch (err) {
      console.error('Error in Get Nonce. Error:', err);
      return err;
    }
  }

  async insertNonce(nonce) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTINONCETABLE_NAME,
      Item: {
        id: nonce,
      },
    };

    try {
      await docClient.put(params).promise();
      console.log('Inserted Nonce. Nonce:', nonce);
    } catch (err) {
      console.error('Error in Insert Nonce. Error:', err);
      return err;
    }
  }

  async getLtiApiKey(apiKeyId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIAPIKEYTABLE_NAME,
      Key: {
        id: apiKeyId,
      },
    };

    try {
      const data = await docClient.get(params).promise();
      console.log('Get LtiApiKey. Data :', JSON.stringify(data.Item));
      return data.Item;
    } catch (err) {
      console.error('Error in Get LtiApiKey. Error:', err);
      return err;
    }
  }

  async queryLtiApiKey(platformId) {
    var params = {
      TableName: process.env.API_LOREEV2API_LTIAPIKEYTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };

    try {
      const data = await docClient.query(params).promise();
      console.log('Query LtiApiKey. Data.items :', data.Items);
      return data.Items[0];
    } catch (err) {
      console.error('Error in query LtiApiKey. Error:', err);
      return err;
    }
  }

  async queryLtiAccessToken(apiKey, clientId, userId) {
    console.log('MY PARAMS-->', apiKey, clientId, userId);
    var params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      IndexName: 'byLtiApiKey',
      KeyConditionExpression: '#apiKey = :apiKeyValue',
      ExpressionAttributeValues: {
        ':apiKeyValue': apiKey,
        ':clientIdValue': clientId,
        ':userValue': userId,
      },
      ExpressionAttributeNames: {
        '#apiKey': 'ltiApiKeyID',
        '#clientId': 'ltiClientID',
        '#userId': 'user',
      },
      FilterExpression: '#clientId = :clientIdValue and #userId = :userValue',
    };

    try {
      const data = await docClient.query(params).promise();
      console.log('Query LtiAccessToken. Data.items :', data.Items);
      return data.Items;
    } catch (err) {
      console.error('Error in query LtiAccessToken. Error:', err);
      return err;
    }
  }

  async createLtiAccessToken(accessToken, username, password, roleAdmin, lms_email, ltiPlatformID) {
    console.log('Inserted accessToken. accessToken:', accessToken);
    const { v4: uuidv4 } = require('uuid');
    let tokenId = uuidv4();
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      Item: {
        id: tokenId,
        ltiApiKeyID: accessToken.ltiApiKeyID,
        ltiClientID: accessToken.ltiClientID,
        loreeUsername: username,
        loreePassword: password,
        isAdmin: roleAdmin,
        user: accessToken.user.id,
        userInfo: accessToken.user,
        accessToken: accessToken.access_token,
        refreshToken: accessToken.refresh_token,
        expiresAt: accessToken.expires_in,
        generatedAt: accessToken.generatedAt,
        lmsApiUrl: accessToken.lmsApiUrl,
        lms_email: lms_email,
        ltiPlatformID: ltiPlatformID,
        createdAt: accessToken.generatedAt,
        updatedAt: accessToken.generatedAt,
      },
    };

    try {
      await docClient.put(params).promise();
      console.log('Inserted accessToken. accessToken:', tokenId);
      return tokenId;
    } catch (err) {
      console.error('Error in creating accessToken. Error:', err);
      return err;
    }
  }

  async updateLtiAccessToken(id, token, dateGenerated, isAdmin, lms_email, refresh_token, expires_at) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      Key: {
        id: id,
      },
      UpdateExpression:
        'set #access_token = :tokenValue, #dateGen = :dateGenerated, #isAdmin = :isAdmin, #lms_email = :lms_email, #refresh_token = :refresh_token, #expires_at = :expires_at',
      ExpressionAttributeNames: {
        '#access_token': 'accessToken',
        '#dateGen': 'generatedAt',
        '#isAdmin': 'isAdmin',
        '#lms_email': 'lms_email',
        '#refresh_token': 'refreshToken',
        '#expires_at': 'expiresAt',
      },
      ExpressionAttributeValues: {
        ':tokenValue': token,
        ':dateGenerated': dateGenerated,
        ':isAdmin': isAdmin,
        ':lms_email': lms_email,
        ':refresh_token': refresh_token,
        ':expires_at': expires_at,
      },
    };
    try {
      await docClient.update(params).promise();
      console.log('updated accessToken. accessToken:', params);
    } catch (err) {
      console.error('Error in updating accessToken. Error:', err);
      return err;
    }
  }

  // temporary function to update the ltiPlatformID
  // Once it updated with all existing record, we'll remove it.

  async usersDataIDUpdate() {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
    };
    try {
      const data = await docClient.scan(params).promise();
      console.log('Query LtiAccessToken. Data.items :', data, data.Items);
      for (let userData of data.Items) {
        console.log('user client id ', userData.ltiClientID);

        const platformParam = {
          TableName: process.env.API_LOREEV2API_LTIPLATFORMTABLE_NAME,
          IndexName: 'byClientId',
          KeyConditionExpression: '#name = :value',
          ExpressionAttributeValues: { ':value': userData.ltiClientID },
          ExpressionAttributeNames: { '#name': 'clientId' },
        };

        const platformData = await docClient.query(platformParam).promise();
        console.log('platform Data', platformData.Items[0]);
        console.log('platform Data id', platformData.Items[0].id);

        const UpdateParams = {
          TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
          Key: {
            id: userData.id,
          },
          UpdateExpression: 'set #ltiPlatformID = :ltiPlatformID',
          ExpressionAttributeNames: {
            '#ltiPlatformID': 'ltiPlatformID',
          },
          ExpressionAttributeValues: {
            ':ltiPlatformID': platformData.Items[0].id,
          },
        };

        const updateID = await docClient.update(UpdateParams).promise();
        console.log('update ID', updateID);
      }
      return 'success';
    } catch (err) {
      console.error('Error in query LtiAccessToken. Error:', err);
      return err;
    }
  }

  async getPlatform(platformUrl, clientId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMTABLE_NAME,
      FilterExpression: '(#clientId = :clientId_eq) AND (#platformUrl = :platformUrl_eq)',
      ExpressionAttributeNames: {
        '#platformUrl': 'platformUrl',
        '#clientId': 'clientId',
      },
      ExpressionAttributeValues: {
        ':clientId_eq': clientId,
        ':platformUrl_eq': platformUrl,
      },
    };
    try {
      const data = await docClient.scan(params).promise();
      console.log('Get Platform. data:', data);
      if (data.Count > 0) {
        return data.Items[0];
      }
      return false;
    } catch (err) {
      console.error('Error in getPlatform. Error:', err);
      return false;
    }
  }

  async getBBPlatform(platformUrl, ltiDeploymentId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMTABLE_NAME,
      FilterExpression: '(#ltiDeploymentId = :ltiDeploymentId_eq) AND (#platformUrl = :platformUrl_eq)',
      ExpressionAttributeNames: {
        '#platformUrl': 'platformUrl',
        '#ltiDeploymentId': 'ltiDeploymentId',
      },
      ExpressionAttributeValues: {
        ':ltiDeploymentId_eq': ltiDeploymentId,
        ':platformUrl_eq': platformUrl,
      },
    };
    try {
      const data = await docClient.scan(params).promise();
      console.log('Get Platform. data:', data);
      if (data.Count > 0) {
        return data.Items[0];
      }
      return false;
    } catch (err) {
      console.error('Error in getPlatform. Error:', err);
      return false;
    }
  }

  async insertLtiLog(platformId, userDetails, browserDetails, userIP, userAction) {
    const { v4: uuidv4 } = require('uuid');
    let id = uuidv4();
    const params = {
      TableName: process.env.API_LOREEV2API_LTILOGTABLE_NAME,
      Item: {
        id: id,
        ltiPlatformID: platformId,
        userInfo: userDetails,
        clientInfo: browserDetails,
        userGeoLocation: userIP,
        userAction: userAction,
        generatedAt: new Date().toISOString(),
        createdAt: new Date().toISOString(),
        updatedAt: new Date().toISOString(),
      },
    };
    try {
      await docClient.put(params).promise();
      console.log('Inserted user log info', userDetails);
      return true;
    } catch (err) {
      console.error('Error in Insert user client log info. Error:', err);
      return true;
    }
  }

  async queryLtiPlatformKey(platformId) {
    var params = {
      TableName: process.env.API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME,
      IndexName: 'byLtiPlatform',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': platformId },
      ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
    };

    try {
      const data = await docClient.query(params).promise();
      console.log('Query LtiApiKey. Data.items :', data.Items);
      return data.Items[0];
    } catch (err) {
      console.error('Error in query LtiApiKey. Error:', err);
      return err;
    }
  }

  async getLtiToolLoginHint(loginHint) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTITOOLLOGINHINTTABLE_NAME,
      Key: {
        id: loginHint,
      },
    };
    try {
      const data = await docClient.get(params).promise();
      logger.debug('getLtiToolLoginHint Data -> ' + JSON.stringify(data.Item));
      return data.Item;
    } catch (err) {
      logAndThrowError('Error in getLtiToolLoginHint', err);
    }
  }

  async getLtiTool(id) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTITOOLTABLE_NAME,
      Key: {
        id: id,
      },
    };

    try {
      const data = await docClient.get(params).promise();
      logger.debug('getLtiTool Data -> ' + JSON.stringify(data.Item));
      return data.Item;
    } catch (err) {
      logAndThrowError('Error in getLtiTool', err);
    }
  }

  async findLtiToolByClientId(clientId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTITOOLTABLE_NAME,
      FilterExpression: '(#clientId = :clientId_eq)',
      ExpressionAttributeNames: {
        '#clientId': 'clientId',
      },
      ExpressionAttributeValues: {
        ':clientId_eq': clientId,
      },
    };
    try {
      const data = await docClient.scan(params).promise();
      logger.info('findLtiToolByClientId -> ' + JSON.stringify(data));
      return data.Items[0];
    } catch (err) {
      logAndThrowError('Error in findLtiToolByClientId', err)
    }
  }

  async fetchLtiToolByDomain(domainName) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTITOOLTABLE_NAME,
      FilterExpression: '(#domainName = :domainName_eq)',
      ExpressionAttributeNames: {
        '#domainName': 'domainName',
      },
      ExpressionAttributeValues: {
        ':domainName_eq': domainName,
      },
    };
    try {
      const data = await docClient.scan(params).promise();
      logger.info('fetchLtiToolByDomain -> ' + JSON.stringify(data));
      return data.Items[0];
    } catch (err) {
      logAndThrowError('Error in fetchLtiToolByDomain', err)
    }
  }

  async queryLtiAccessTokenByEmail(email) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      IndexName: 'byEmail',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': email },
      ExpressionAttributeNames: { '#name': 'loreeUsername' },
    };
    try {
      const accesstokenData = await docClient.query(params).promise();
      logger.debug('queryLtiAccessToken Data -> ' + JSON.stringify(accesstokenData.Item));
      return accesstokenData.Items[0];
    } catch (err) {
      logAndThrowError('Error in getLtiToolLoginHint', err);
    }
  }

  async fetchLtiToolDeployments(toolId) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTITOOLDEPLOYMENTTABLE_NAME,
      FilterExpression: '(#ltiToolDeploymentToolIdId = :ltiToolDeploymentToolIdId_eq)',
      ExpressionAttributeNames: {
        '#ltiToolDeploymentToolIdId': 'ltiToolDeploymentToolIdId',
      },
      ExpressionAttributeValues: {
        ':ltiToolDeploymentToolIdId_eq': toolId,
      },
    };
    try {
      const data = await docClient.scan(params).promise();
      logger.debug('fetchLtiToolDeployments -> ' + JSON.stringify(data));
      return data.Items[0];
    } catch (err) {
      logAndThrowError('Error in fetchLtiToolDeployments', err)
    }
  }
}

module.exports = new Database();
