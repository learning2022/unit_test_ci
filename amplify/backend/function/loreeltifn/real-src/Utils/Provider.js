/* eslint-disable */
const axios = require('axios');
const graphql = require('graphql');
const { print } = graphql;
const { getLtiPlatform } = require('../graphql/queries');

class Provider {
  async getPlatform(url, clientId) {
    console.log(`Receiving getPlatform request. url: ${url}, clientId: ${clientId}`);
    if (!url) throw new Error('MISSING_PLATFORM_URL');
    let inputData = {
      filter: {
        platformUrl: {
          eq: url,
        },
        clientId: {
          eq: clientId,
        },
      },
    };

    console.log('graphql input_data -> getPlatform', inputData);
    const result = await this.graphqlRequest(getLtiPlatform, inputData);
    console.log('LTI PLATFORM RESP==>', result);
    if (!result) return false;
    const platform = result.body.data.listLtiPlatforms.items[0];
    console.log('Plat:', platform);
    return platform;
  }

  async graphqlRequest(query, inputData) {
    console.log(`Receiving graphqlRequest. query: ${query}, inputData: ${inputData}`);
    console.log('inside graphql post request');
    try {
      const graphqlData = await axios({
        url: process.env.API_LOREEV2API_GRAPHQLAPIENDPOINTOUTPUT,
        method: 'post',
        headers: {
          'x-api-key': process.env.API_LOREEV2API_GRAPHQLAPIKEYOUTPUT,
        },
        data: {
          query: print(query),
          variables: inputData,
        },
      });
      console.log('graphql response data', JSON.stringify(graphqlData.data));
      return {
        statusCode: 200,
        body: graphqlData.data,
      };
    } catch (err) {
      console.log('Error posting to appsync :', err);
      return {
        statusCode: 500,
        body: err,
      };
    }
  }
}

module.exports = new Provider();
