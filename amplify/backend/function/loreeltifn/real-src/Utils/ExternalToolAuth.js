const jose = require('node-jose');
const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const Database = require('./Database');
const logger = require('./Logger').logger;
const AuthorisationError = require('../exceptionHandler/AuthorisationError')
const InvalidJwtError = require('../exceptionHandler/InvalidJwtError')

const SCOPE = "openid";

const authToken = async (query) => {
  const ltiToolLoginHint = await Database.getLtiToolLoginHint(query.login_hint);
  logger.debug('LTI tool -> ' + JSON.stringify(ltiToolLoginHint));

  const authValid = await validateAuthRequest(ltiToolLoginHint.clientId, query);
  logger.debug('authValid ->' + authValid);
  if(!authValid) throw new AuthorisationError('Unauthorized');
  //Generate and sign id-token
  const token = await generateIdToken(ltiToolLoginHint, query.client_id, query.nonce);
  logger.debug('token -> ' + token);
  return await signIdToken(token);
}

const validateAuthRequest = async (clientId, query) => {
  logger.debug('query -> ' + JSON.stringify(query));

  const isValidClient = clientId === query.client_id;
  const isValidScope = SCOPE === query.scope;
  const isValidResponseType = query.response_type === 'id_token';
  const isValidResponseMode = query.response_mode === 'form_post';
  const isValidPrompt = query.prompt === 'none';
  const isValidRedirectUri = !!query.redirect_uri;
  const isValidState = !!query.state;
  const isValidNonce = !!query.nonce;
  const isValidLoginHint = !!query.nonce ;
  const isAllValid = ![isValidClient, isValidScope, isValidResponseType, isValidResponseMode, isValidPrompt, isValidRedirectUri, isValidState, isValidNonce, isValidLoginHint].includes(false);
  return isAllValid;
}

const generateIdToken = async (ltiToolLoginHint, clientId, nonce) => {
  const idToken = {};
  logger.info('generateIdToken');
  const userToken = await Database.queryLtiAccessTokenByEmail(ltiToolLoginHint.loreeUserEmail);
  const ltiToolData = await Database.findLtiToolByClientId(clientId);
  const deploymentData = await Database.fetchLtiToolDeployments(ltiToolData.id)
  generateUserClaims(clientId, nonce, ltiToolLoginHint, userToken, idToken);
  generateSecurityClaims(userToken, idToken);
  generateLTIClaims(ltiToolLoginHint, idToken, deploymentData.id);
  logger.debug('generateIdToken ->' + idToken);
  return idToken;
}

// User Claims
const generateUserClaims = (clientId, nonce, ltiToolLoginHint, userToken, idToken) => {
  idToken['iss'] = ltiToolLoginHint.issuerUrl;
  idToken['sub'] = userToken.id;
  idToken['aud'] = clientId;
  idToken['azp'] = clientId;
  idToken['exp'] = Math.floor((Date.now() + 60000) / 1000);
  idToken['iat'] = Math.floor(Date.now() / 1000);
  idToken['nonce'] = nonce;
}

// Security Claims
const generateSecurityClaims = (userToken, idToken) => {
  idToken['name'] = userToken.loreeUsername;
  idToken['given_name'] = userToken.loreeUsername;
  idToken['email'] = userToken.lms_email;
  idToken['family_name'] = userToken.loreeUsername;
  idToken['middle_name'] = userToken.loreeUsername;
  idToken['picture'] = userToken.lms_email;
  idToken['locale'] = 'en-US';
}

//LTI Claims
const generateLTIClaims = (ltiToolLoginHint, idToken, deploymentId) => {
  idTokenForMessageType(ltiToolLoginHint, idToken);
  idToken['https://purl.imsglobal.org/spec/lti/claim/resource_link'] = {
    "id": "200d101f-2c14-434a-a0f3-57c2a42369fc",
    "description": "Fetch this description from canvas id-token",
    "title": "Fetch this description from canvas id-token"
  }
  idToken['https://purl.imsglobal.org/spec/lti/claim/deployment_id'] = deploymentId;
  idToken['https://purl.imsglobal.org/spec/lti/claim/message_type'] = ltiToolLoginHint.messageType;
  idToken['https://purl.imsglobal.org/spec/lti/claim/version'] = '1.3.0';
  idToken['https://purl.imsglobal.org/spec/lti/claim/roles'] = ["http://purl.imsglobal.org/vocab/lis/v2/institution/person#Student"];
}

const idTokenForMessageType = (ltiToolLoginHint, idToken) => {
  if (ltiToolLoginHint.messageType === 'LtiDeepLinkingRequest') {
    const deepLinkValues = {
      "deep_link_return_url": ltiToolLoginHint.issuerUrl,
      "accept_types": [
        "ltiResourceLink"
      ],
      "accept_presentation_document_targets": [
        "iframe",
        "window"
      ],
      "accept_media_types": "application/vnd.ims.lti.v1.ltilink",
      "auto_create": false,
      "accept_multiple": false,
      "validation_context": null,
      "errors": {
        "errors": {}
      }
    };
    idToken['https://purl.imsglobal.org/spec/lti-dl/claim/deep_linking_settings'] = deepLinkValues;
  } else {
    idToken['https://purl.imsglobal.org/spec/lti/claim/target_link_uri'] = ltiToolLoginHint.resourceLink;
  }
}

const signIdToken = async (token) => {
  const readKey = fs.readFileSync(path.resolve(__dirname, '../keys.json'));
  const keyStore = await jose.JWK.asKeyStore(readKey.toString())
  const [key] = keyStore.all({ use: 'sig' })
  
  const tokenString = JSON.stringify(token)
  return await jose.JWS.createSign({compact: true, jwk: key, fields: { typ: 'JWT' }, alg: "RS256"}, key)
    .update(tokenString)
    .final();
}

const getSelectedContentLinkFromToken = async (jwtToken) => {
  const decodedToken = jwt.decode(jwtToken, { complete: true });
  logger.info('decode token -> ' + decodedToken);
  if (!decodedToken) throw new InvalidJwtError('INVALID_JWT_RECEIVED');
  logger.info('selected content link -> ' + decodedToken['payload']['https://purl.imsglobal.org/spec/lti-dl/claim/content_items'][0]['url']);
  return await getLtiToolContentLink(decodedToken['payload']['iss'], decodedToken['payload']['https://purl.imsglobal.org/spec/lti-dl/claim/content_items'][0]['url']);
}

const getLtiToolContentLink = async (clientId, link) => {
  const ltiTool = await Database.findLtiToolByClientId(clientId);
  logger.info('ltiTool -> ' + ltiTool);
  const contentLink = `${ltiTool.issuerUrl}?id=${ltiTool.id}&content_url=${link}`
  return contentLink;
}

const getLaunchParams = async (query) => {
  let ltiTool;
  if (query.id) {
    ltiTool = await Database.getLtiTool(query.id);
  } else {
    const contentUrl = new URL(query.content_url);
    logger.debug('contentUrl -> ' + contentUrl)
    ltiTool = await Database.fetchLtiToolByDomain(contentUrl.origin)
  }
  return {
    clientId: ltiTool.clientId,
    issuerUrl: ltiTool.issuerUrl,
    oidcUrl: ltiTool.oidcUrl,
    targetLinkURI: query.content_url,
  };
}

module.exports = { authToken, getSelectedContentLinkFromToken, getLaunchParams };
