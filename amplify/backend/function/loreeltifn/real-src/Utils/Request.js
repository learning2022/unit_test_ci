'use strict';
const crypto = require('crypto');

/* Handle Requests */
class Request {
  static async ltiAdvantageLogin(request, state) {
    const query = {
      response_type: 'id_token',
      response_mode: 'form_post',
      id_token_signed_response_alg: 'RS256',
      scope: 'openid',
      client_id: request.client_id,
      redirect_uri: request.target_link_uri,
      login_hint: request.login_hint,
      nonce: encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``),
      prompt: 'none',
      state: state,
    };
    if (request.lti_message_hint) query.lti_message_hint = request.lti_message_hint;
    if (request.lti_deployment_id) query.lti_deployment_id = request.lti_deployment_id;
    return query;
  }
}

module.exports = Request;
