const logger =  require('./Logger').logger;

logAndThrowError = (message, error) => {
  logger.error(message + ' -> ' + error);
  throw new Error(message + error);
}

module.exports = { logAndThrowError };