

class ExecTimer {

  startTimers:{[key:string]:Date} = {};
  lastKey = "UNDEFINED";

  start(key:string="UNDEFINED") {
    if(this.startTimers[key] !== undefined) {
      console.debug(`ExecTimer[${key}] > START called on already-running timer (END was not called), timer replaced`);
    }
    this.lastKey = key;
    this.startTimers[key] = new Date();
  }
  end(key:string=this.lastKey) {
    const now = new Date();
    const then = this.startTimers[key];
    if(then == undefined) {
      console.debug(`ExecTimer[${key}] > END called on timer without START value, ignoring`);
      return;
    }
    delete this.startTimers[key];
    this.lastKey = "UNDEFINED";
    const seconds = (now.getTime() - then.getTime())/1000
    console.log(`ExecTimer[${key}]: ${seconds} seconds`)
  }
}

export default new ExecTimer();