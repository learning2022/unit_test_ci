const url = require('fast-url-parser');
const axios = require('axios');
const store = require('store');
const jwttoken = require('jsonwebtoken');
const qs = require('qs');
const crypto = require('crypto');

const Database = require('../Utils/Database');
const Cognito = require('../Utils/CognitoAuth');

const CLIENT_URL = process.env.LOREEV2_APP_URL;
const ENV_NAME = process.env.ENV;
const cookieOptions = {
  secure: true,
  sameSite: 'None',
  signed: true,
  maxAge: 60000, // Adding max age to state cookie = 1min
};
let lmsApiUrl;
let googleAnalyticsTrackingId;

class Authorise {
  async authLogin(params, req, res, platformDomain) {
    console.log('Received CanvasLogin. params:', params, req);
    let target_link_uri = params['https://purl.imsglobal.org/spec/lti/claim/target_link_uri'];
    let redirect_uri = target_link_uri.replace(/\/[^\/]*$/, '/token');
    console.log('updated_target_link_uri:', redirect_uri);
    googleAnalyticsTrackingId = params.trackingId;
    let apiKey = await Database.queryLtiApiKey(params.platformId);
    console.log('apiKey:', apiKey);
    let ltiPlatformKey = await Database.queryLtiPlatformKey(params.platformId);
    console.log('LtiPlatformKey:', ltiPlatformKey);
    // For diffferentiate platforms
    let data, user_id, course_id, courseLabel, username, isAdmin;
    console.log('CONTEXT', params['https://purl.imsglobal.org/spec/lti/claim/context']);
    const courseData = params['https://purl.imsglobal.org/spec/lti/claim/context'];
    store.set('platformDomain', platformDomain);
    if (platformDomain === 'canvas') {
      data = params['https://purl.imsglobal.org/spec/lti/claim/custom'];
      user_id = data.user_id;
      course_id = data.course_id;
      courseLabel = courseData.label;
      const roles_array = data.roles.split(',');
      isAdmin = roles_array.includes('Loree-Admin');
      lmsApiUrl = apiKey.lmsApiUrl ? apiKey.lmsApiUrl : apiKey.oauthLoginUrl.split('/login')[0];
    } else if (platformDomain === 'D2l') {
      console.log('D2l apiKey', apiKey);
      data = params['http://www.brightspace.com'];
      for (let role of params['https://purl.imsglobal.org/spec/lti/claim/roles']) {
        if (role === 'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator') {
          isAdmin = true;
        } else {
          isAdmin = false;
        }
      }
      user_id = data.user_id;
      username = data.username;
      course_id = courseData.id;
      courseLabel = courseData.label;
      lmsApiUrl = apiKey.lmsApiUrl;
      store.set(`user_id_${params.clientId}`, user_id);
      store.set(`user_name_${params.clientId}`, username);
    } else if (platformDomain === 'BB') {
      console.log('params', params);
      data = params['https://purl.imsglobal.org/spec/lti/claim/custom'];
      user_id = data.user_id;
      username = params.name;
      course_id = data.course_id;
      courseLabel = courseData.label;
      lmsApiUrl = apiKey.lmsApiUrl;
      const rolesArrayData =  data.admin_role.split(', '); // splited admin role by comma and space
      isAdmin = rolesArrayData.includes('LOREEADMIN')
      store.set(`user_id_${params.clientId}`, user_id);
      store.set(`user_name_${params.clientId}`, username);
    }
    store.set(`course_id_${params.clientId}_${user_id}`, course_id);
    store.set(`role_${params.clientId}_${user_id}`, isAdmin);
    store.set(`email_${params.clientId}_${user_id}`, params.email);
    console.log('EMAIL', params.email);
    const userAccessToken = await Database.queryLtiAccessToken(apiKey.id, apiKey.ltiClientID, user_id);
    console.log('****accessTokenData*****:', userAccessToken, lmsApiUrl);
    if (!userAccessToken.length > 0) {
      console.log('token not available');
      const state = encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``);
      console.info('Generated state: ', state); // Setting up validation info
      // Setting up validation info
      console.log('Sending Cookie. cookieOptions:', cookieOptions);
      res.cookie('state' + state, state, cookieOptions);
      res.cookie('ltiapi' + state, apiKey.id, cookieOptions);
      res.cookie('uniqueId' + state, `${params.clientId}_${user_id}`, cookieOptions);
      const query = await this.oauthLogin(apiKey, redirect_uri, state);
      console.log('autLogin. query:', query);
      if (platformDomain === 'D2l') {
        query.scope =
          'content:access:read content:completions:read content:toc:read content:completions:write content:modules:read news:access:read content:topics:write content:topics:read discussions:access:read discussions:forums:read discussions:posts:read discussions:topics:read lti:links:create lti:links:delete lti:links:read lti:links:update core:*:* quizzing:quizzes:read content:file:write';
      }
      const redirectUrl = url.format({
        pathname: apiKey.oauthLoginUrl,
        query: query,
      });
      console.log('redirect URL for token gen', redirectUrl);
      res.redirect(redirectUrl);
    } else {
      console.log('Already token available');
      const access_token = userAccessToken[0].accessToken;
      if (access_token !== '') {
        let generatedDate = userAccessToken[0].generatedAt;
        const expiresAt = userAccessToken[0].expiresAt;
        let userStatus = this.tokenExpired(generatedDate, expiresAt);
        const jsonData = {
          userCogId: userAccessToken[0].loreeUsername,
          userToken: userAccessToken[0].loreePassword,
          name: userAccessToken[0].userInfo.id,
          id_token: userAccessToken[0].id,
          course_id: course_id,
          course_label: courseLabel,
          platform_id: apiKey.ltiPlatformID,
          roleAdmin: isAdmin,
          trackingId: googleAnalyticsTrackingId,
          lmsApiUrl: userAccessToken[0].lmsApiUrl,
          lms_email: params.email,
          domainName: platformDomain,
          envName: ENV_NAME,
        };

        const jsonToken = await this.generateToken(jsonData, ltiPlatformKey.privatekey);
        console.log('JSON TOKEN', jsonToken);
        const token = {
          token: jsonToken,
          publicKey: ltiPlatformKey.publicKey,
        };
        console.log('QUERY DATA TO SEND', jsonData);

        if (userStatus) {
          let refresh_token = userAccessToken[0].refreshToken;
          console.log('Token Expired', refresh_token);

          let currentTime = new Date().toISOString();
          let generateNewtoken;

          if (platformDomain === 'BB') {
            generateNewtoken = await this.BBRefreshToken(refresh_token, apiKey);
          } else {
            generateNewtoken = await this.refreshDBToken(refresh_token, apiKey);
          }

          if (platformDomain === 'D2l' || platformDomain === 'BB') {
            refresh_token = generateNewtoken.data.refresh_token;
          }

          await Database.updateLtiAccessToken(
            userAccessToken[0].id,
            generateNewtoken.data.access_token,
            currentTime,
            isAdmin,
            params.email,
            refresh_token,
            generateNewtoken.data.expires_in,
          );

          console.log('Redirecting to Client. URL:', `${CLIENT_URL}/lti/canvas/login`);
          res.redirect(
            url.format({
              pathname: `${CLIENT_URL}/lti/canvas/login`,
              query: token,
            }),
          );
        } else {
          console.log('token not expired Redirecting to Client. URL:', `${CLIENT_URL}/lti/canvas/login`, userAccessToken[0].userInfo);

          res.redirect(
            url.format({
              pathname: `${CLIENT_URL}/lti/canvas/login`,
              query: token,
            }),
          );
        }
      } else {
        console.error('Token missing from DB');
      }
    }
  }

  async authToken(req, res) {
    const platformDomain = store.get('platformDomain');
    console.log('Received authToken. params:', req);

    const cookies = req.signedCookies;
    console.log('Cookies received: ', cookies);

    const receivedState = req.query.state;
    console.log('state received: ', receivedState);

    const validationState = cookies['state' + receivedState];
    console.log('validationCookie received: ', validationState);

    if (!receivedState && !validationState && validationState !== receivedState) {
      console.error('receivedState & receivedState not match.');
      throw new Error('STATE_DOES_NOT_MATCH');
    }
    console.log('Successfully verified state.');

    if (req.query.error !== undefined && req.query.error === 'access_denied') {
      //cancel oauth
      const uniqueId = cookies['uniqueId' + receivedState];
      const course_id = store.get(`course_id_${uniqueId}`);
      console.log('cancel CLICK', req.query.error, uniqueId, course_id);
      res.redirect(
        url.format({
          pathname: `${process.env.LOREEV2_APP_URL}/lti/unauth`,
          query: {
            course_id: course_id,
            lmsUrl: lmsApiUrl,
          },
        }),
      );
      return;
    }

    const apiKeyId = cookies['ltiapi' + receivedState];
    console.log('api Key ID', apiKeyId);
    let apiKey = await Database.getLtiApiKey(apiKeyId);
    console.log('apiKey:', apiKey);

    const query = await this.oauthToken(apiKey, req.query.code, req.headers.host);

    res.clearCookie('state' + receivedState, cookieOptions);
    res.clearCookie('ltiapi' + receivedState, cookieOptions);

    console.log('query:', query);
    console.log('apiKey.oauthTokenUrl:', apiKey.oauthTokenUrl);
    try {
      let genUser, lmsEmail, accessTokenData, user_id, username;
      let currentTime = new Date().toISOString();
      const tokenParams = {
        method: 'post',
        url: apiKey.oauthTokenUrl,
        data: query,
      };
      if (platformDomain === 'D2l') {
        tokenParams.headers = {
          'Content-Type': 'application/x-www-form-urlencoded',
        };
      }

      if (platformDomain === 'BB') {
        const basicAuthToken = Buffer.from(apiKey.apiClientId + ':' + apiKey.apiSecretKey).toString('base64');
        tokenParams.headers = {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Basic ${basicAuthToken}`,
        };
      }

      let accessToken = await axios(tokenParams);
      console.log('Done axios post:', accessToken.data);
      if (platformDomain === 'canvas') {
        console.log('********Enter into if block*******');
        const canvasUser = accessToken.data.user;
        user_id = canvasUser.id;
        username = canvasUser.name;
        genUser = await this.userCredential(canvasUser.global_id, username, platformDomain);
        console.log('******gen user*****', genUser);
        lmsEmail = store.get(`email_${apiKey.ltiClientID}_${user_id}`);
        console.log('loreeUser:', canvasUser, genUser);
        accessToken.data.generatedAt = currentTime;
        accessToken.data.ltiClientID = apiKey.ltiClientID;
        accessToken.data.ltiApiKeyID = apiKey.id;
        accessToken.data.lmsApiUrl = lmsApiUrl;
        accessTokenData = accessToken.data;
      } else if (platformDomain === 'D2l' || platformDomain === 'BB') {
        user_id = store.get(`user_id_${apiKey.ltiClientID}`);
        username = store.get(`user_name_${apiKey.ltiClientID}`);
        genUser = await this.userCredential(user_id, username, platformDomain);
        console.log(store.get(`email_${apiKey.ltiClientID}_${user_id}`));
        lmsEmail = store.get(`email_${apiKey.ltiClientID}_${user_id}`);
        accessTokenData = {
          ltiApiKeyID: apiKey.id,
          ltiClientID: apiKey.ltiClientID,
          user: {
            id: user_id,
            userInfo: username,
          },
          access_token: accessToken.data.access_token,
          refresh_token: accessToken.data.refresh_token,
          expires_in: accessToken.data.expires_in,
          generatedAt: currentTime,
          lmsApiUrl: lmsApiUrl,
          createdAt: currentTime,
          updatedAt: currentTime,
        };
      }
      console.log('*******Cognito get user Start*****');
      const password = await Cognito.getUser(genUser.email, genUser.name, apiKey.ltiPlatformID, lmsEmail);
      console.log('********Cognito.getUser End******');
      let course_id = store.get(`course_id_${apiKey.ltiClientID}_${user_id}`);
      let roleAdmin = store.get(`role_${apiKey.ltiClientID}_${user_id}`);
      let createdTokenId = await Database.createLtiAccessToken(
        accessTokenData,
        genUser.email,
        password,
        roleAdmin,
        lmsEmail,
        apiKey.ltiPlatformID,
      );
      console.log('Apitoken saved:');
      let jsonData = {
        userCogId: genUser.email,
        userToken: password,
        name: username,
        id_token: createdTokenId,
        course_id: course_id,
        platform_id: apiKey.ltiPlatformID,
        roleAdmin: roleAdmin,
        trackingId: googleAnalyticsTrackingId,
        lmsApiUrl: lmsApiUrl,
        lms_email: lmsEmail,
        domainName: platformDomain,
        envName: ENV_NAME,
      };

      let ltiPlatformKey = await Database.queryLtiPlatformKey(apiKey.ltiPlatformID);
      console.log('LtiPlatformKey:', ltiPlatformKey);
      const jsonToken = await this.generateToken(jsonData, ltiPlatformKey.privatekey);
      console.log('JSON TOKEN', jsonToken);

      const token = {
        token: jsonToken,
        publicKey: ltiPlatformKey.publicKey,
      };

      console.log('Redirecting to Client. URL:', `${CLIENT_URL}/lti/canvas/login`);
      res.redirect(
        url.format({
          pathname: `${CLIENT_URL}/lti/canvas/login`,
          query: token,
        }),
      );
    } catch (error) {
      console.error(error);
    }
  }

  async generateToken(payload, key) {
    const token = await jwttoken.sign(payload, key, {
      algorithm: 'RS256',
      expiresIn: 300,
    });
    return token;
  }

  async oauthLogin(request, redirect_uri, state) {
    const query = {
      client_id: request.apiClientId,
      response_type: 'code',
      redirect_uri: redirect_uri,
      state: state,
      purpose: 'Loreev2 LTI access behalf of user',
    };
    return query;
  }

  async oauthToken(apiKey, code, domainName) {
    const redirectUrl = domainName === process.env.APIDOMAIN ?`https://${domainName}/lti/token`: `https://${domainName}/${process.env.ENV}/lti/token`;
    const query = {
      grant_type: 'authorization_code',
      client_id: apiKey.apiClientId,
      client_secret: apiKey.apiSecretKey,
      redirect_uri: redirectUrl,
      code: code,
    };
    if (store.get('platformDomain') === 'D2l') {
      return qs.stringify(query);
    } else if (store.get('platformDomain') === 'BB') {
      return qs.stringify({
        grant_type: 'authorization_code',
        redirect_uri: redirectUrl,
        code: code,
      });
    } else {
      return query;
    }
  }

  // Generate email and password to create Loree user for canvas users.
  async userCredential(user_id, name, platformDomain) {
    const credential = {
      email:
        platformDomain === 'canvas'
          ? `canvas_${user_id}@example.com`
          : platformDomain === 'D2l'
            ? `d2l_${user_id}@example.com`
            : `bb_${user_id}@example.com`,
      name: name,
    };
    return credential;
  }

  tokenExpired = (date_generated, expiresAt) => {
    //check if token expired or not
    let generatedDate = new Date(date_generated);
    const max_age = expiresAt;
    const currentTime = new Date();
    var timePassed = Math.abs(generatedDate - currentTime) / 1000;
    if (timePassed > max_age) {
      return true;
    } else {
      return false;
    }
  };

  refreshDBToken = async (refresh_token, data) => {
    //get refreshed access token
    let formData = {
      grant_type: 'refresh_token',
      client_id: data.apiClientId,
      client_secret: data.apiSecretKey,
      refresh_token: `${refresh_token}`,
    };

    if (store.get('platformDomain') === 'D2l') {
      formData = qs.stringify(formData);
    }
    let refreshParams = {
      method: 'post',
      url: data.oauthTokenUrl,
      data: formData,
    };
    if (store.get('platformDomain') === 'D2l') {
      refreshParams.headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
    }
    let response = await axios(refreshParams);
    return response;
  };

  BBRefreshToken = async (oldRefreshToken, apiKeyData) => {
    const formData = qs.stringify({
      grant_type: 'refresh_token',
      refresh_token: oldRefreshToken,
    });
    const basicAuthToken = Buffer.from(apiKeyData.apiClientId + ':' + apiKeyData.apiSecretKey).toString('base64');

    const refreshParams = {
      method: 'post',
      url: apiKeyData.oauthTokenUrl,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${basicAuthToken}`,
      },
      data: formData,
    };

    let tokenResponse = await axios(refreshParams);
    return tokenResponse;
  };
}

module.exports = new Authorise();
