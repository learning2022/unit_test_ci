/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

/* Amplify Params - DO NOT EDIT
	API_LOREEV2API_GRAPHQLAPIIDOUTPUT
	API_LOREEV2API_LTIACCESSTOKENTABLE_ARN
	API_LOREEV2API_LTIACCESSTOKENTABLE_NAME
	API_LOREEV2API_LTIAPIKEYTABLE_ARN
	API_LOREEV2API_LTIAPIKEYTABLE_NAME
	API_LOREEV2API_LTILOGTABLE_ARN
	API_LOREEV2API_LTILOGTABLE_NAME
	API_LOREEV2API_LTINONCETABLE_ARN
	API_LOREEV2API_LTINONCETABLE_NAME
	API_LOREEV2API_LTIPLATFORMKEYTABLE_ARN
	API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME
	API_LOREEV2API_LTIPLATFORMTABLE_ARN
	API_LOREEV2API_LTIPLATFORMTABLE_NAME
	API_LOREEV2API_LTITOOLDEPLOYMENTTABLE_ARN
	API_LOREEV2API_LTITOOLDEPLOYMENTTABLE_NAME
	API_LOREEV2API_LTITOOLLOGINHINTTABLE_ARN
	API_LOREEV2API_LTITOOLLOGINHINTTABLE_NAME
	API_LOREEV2API_LTITOOLTABLE_ARN
	API_LOREEV2API_LTITOOLTABLE_NAME
	AUTH_LOREEV275F92347_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const express = require('express');
const bodyParser = require('body-parser');
const url = require('fast-url-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const AWSXRay = require('aws-xray-sdk');
AWSXRay.captureHTTPsGlobal(require('http'));
AWSXRay.captureHTTPsGlobal(require('https'));
AWSXRay.capturePromise();

const Platform = require('./Utils/Platform');
const Key = require('./Utils/Key');
const logger = require('./Utils/Logger').logger;
const ExternalToolAuth = require('./Utils/ExternalToolAuth');
const AuthorisationError = require('./exceptionHandler/AuthorisationError')
const InvalidJwtError = require('./exceptionHandler/InvalidJwtError')
const Authorise = require('./lms/Authorise');

// declare a new express app
const app = express();
app.use(AWSXRay.express.openSegment('loreeltifn'));

const cookieParser = require('cookie-parser');
app.use(cookieParser('MY SECRET'));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// LTI Launch for BB
app.get('/lti/launch', async (req, res) => {
  console.log('Received Launch request BB. Request: ', req);
  await Platform.launchVerify(req.query, res);
});

// LTI Launch
app.post('/lti/launch', async (req, res) => {
  console.log('Received Launch request. Request: ', req.body, req.body.client_id, req.body.iss);
  await Platform.launchVerify(req.body, res);
});

// LTI auth
app.post('/lti/auth', async (req, res) => {
  console.log('Received auth request. Request: ', req);
  await Platform.authVerify(req, res);
});

// TODO: Need to update this function
app.get('/lti/token', async (req, res) => {
  console.log('Received API token request. Request: ', req.query);
  await Authorise.authToken(req, res);
});

// LTI inside LTI JWKS key
app.get('/lti/tool/jwks', async (req, res) => {
  logger.debug('JWKS -> ' + req);
  await Key.getKeyStore(res);
});

app.get('/lti/tool/jwks/rotate', async (req, res) => {
  logger.debug('JWKS Rotate -> ' + req);
  await Key.generateKeys(res);
});

// LTI inside LTI auth
app.get('/lti/tool/authorize_redirect', async (req, res) => {
  try {
    logger.debug('authorize_redirect -> ' + JSON.stringify(req.query))
    const idToken = await ExternalToolAuth.authToken(req.query);
    res.redirect(
      url.format({
        pathname: `${process.env.LOREEV2_APP_URL}/lti/tool/auth`,
        query: {
          idToken: idToken,
          state: req.query.state,
          redirectUri: req.query.redirect_uri,
        },
      })
    );
  } catch(error) {
    if(error instanceof AuthorisationError) {
      return res.status(401).send('Unauthorized');
    }
    return res.status(500).send('Internal server error');
  }
});

app.post('/lti/tool', async (req, res) => {
  try {
    logger.debug('POST body -> ' + JSON.stringify(req.body));
    const contentUrl = await ExternalToolAuth.getSelectedContentLinkFromToken(req.body.JWT);
    logger.debug('contentUrl -> ' + contentUrl)
    res.redirect(
      url.format({
        pathname: `${process.env.LOREEV2_APP_URL}/lti/tool/content`,
        query: {
          contentUrl: contentUrl,
        },
      })
    );  
  } catch(error) {
    if(error instanceof InvalidJwtError) {
      return res.status(401).send('Invalid JWT token received');
    }
    return res.status(500).send('Internal server error');
  }
});

app.get('/lti/tool', async (req, res) => {
  try {
    logger.debug('GET lti/tool query -> ' + JSON.stringify(req.query));
    const launchParams = await ExternalToolAuth.getLaunchParams(req.query);
    logger.debug('launchParams -> ' + JSON.stringify(launchParams));
    res.redirect(
      url.format({
        pathname: `${process.env.LOREEV2_APP_URL}/lti/tool/login`,
        query: {
          launchParams: JSON.stringify(launchParams),
        },
      })
    );
  } catch(error) {
    return res.status(500).send('Internal server error');
  }
});

app.use(AWSXRay.express.closeSegment());

app.listen(3000, function () {
  console.log('App started listening on 3000');
});

module.exports = app;
