const gql = require('graphql-tag');

const getLtiPlatform = gql`
  query list($filter: ModelLtiPlatformFilterInput!) {
    listLtiPlatforms(filter: $filter) {
      items {
        id
        clientId
        platformName
        platformUrl
        authEndpoint
        accesstokenEndpoint
        publicKeyURL
        authConfig {
          method
          key
        }
      }
    }
  }
`;

module.exports = { getLtiPlatform };
