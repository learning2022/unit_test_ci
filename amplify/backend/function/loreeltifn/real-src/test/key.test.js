const chai = require('chai');
const LoreeLti = require('../app');
const expect = chai.expect;

describe('LTI inside LTI flow', function () {
  it('generate JWKS key', async () => {
    return await chai
      .request(LoreeLti)
      .get('/lti/tool/jwks')
      .then(res => {
        expect(res).to.have.status(200);
        expect(res).to.be.an('object');
      });
  });

  it('rotate JWKS key', async () => {
    return await chai
      .request(LoreeLti)
      .get('/lti/tool/jwks/rotate')
      .then(res => {
        expect(res).to.have.status(200);
        expect(res).to.be.an('object');
      });
  });
})