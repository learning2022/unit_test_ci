const nock = require('nock');
const chai = require('chai');
const chaiHttp = require('chai-http');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
chai.use(chaiHttp);
const crypto = require('crypto');

const expect = chai.expect;
const app = require('../app');

console.log = () => { };
console.error = () => { };
process.env.AWS_XRAY_CONTEXT_MISSING = 'IGNORE_ERROR';

// Valid complete token
const tokenValid = {
  exp: Math.floor(Date.now() / 1000) + 60 * 60,
  iss: 'http://localhost/canvas',
  aud: 'ClientId1',
  sub: '2',
  'https://purl.imsglobal.org/spec/lti/claim/lis': {
    person_sourcedid: null,
    course_offering_sourcedid: 'CD_002',
    validation_context: null,
    errors: { errors: {} },
  },
  'https://purl.imsglobal.org/spec/lti/claim/roles': [
    'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Administrator',
    'http://purl.imsglobal.org/vocab/lis/v2/institution/person#Instructor',
    'http://purl.imsglobal.org/vocab/lis/v2/membership#ContentDeveloper',
    'http://purl.imsglobal.org/vocab/lis/v2/system/person#User',
  ],
  'https://purl.imsglobal.org/spec/lti/claim/context': {
    id: '3',
    label: 'Raj Sandbox',
    title: 'Raj Sandbox',
    type: [Array],
  },
  'https://purl.imsglobal.org/spec/lti/claim/resource_link': { title: 'teste local', id: '5' },
  given_name: 'Raj',
  family_name: '',
  name: 'Raj',
  'https://purl.imsglobal.org/spec/lti/claim/ext': { user_username: 'rajkumar@crystaldelta.com', lms: 'canvas-2' },
  email: 'rajkumar@crystaldelta.com',
  'https://purl.imsglobal.org/spec/lti/claim/launch_presentation': {
    document_target: 'iframe',
    height: 400,
    width: 800,
    return_url: 'https://crystaldelta.instructure.com/courses/262/external_content/success/external_tool_redirect',
    locale: 'en-GB',
    validation_context: null,
    errors: { errors: {} },
  },
  'https://purl.imsglobal.org/spec/lti/claim/tool_platform': {
    guid: 'Xe2I0bpLXro2py69QEfbbmvUbau5VAzBQYUq1bUh:canvas-lms',
    name: 'Crystal Delta',
    version: 'cloud',
    product_family_code: 'canvas',
    validation_context: null,
    errors: { errors: {} },
  },
  'https://purl.imsglobal.org/spec/lti/claim/version': '1.3.0',
  'https://purl.imsglobal.org/spec/lti/claim/message_type': 'LtiResourceLinkRequest',
  'https://purl.imsglobal.org/spec/lti/claim/custom': {
    user_id: 32,
    course_id: 262,
  },
};

describe('Testing LTI 1.3 flow', function () {
  this.timeout(10000);

  it('Launch route with unregistered platform is expected to return 401 error', async () => {
    return chai
      .request(app)
      .post('/lti/launch')
      .send({ iss: 'https://unregisteredPlatform.com' })
      .then(res => {
        expect(res).to.have.status(401);
      });
  });

  it('Launch route receiving no idToken is expected to return 401 error', async () => {
    return chai
      .request(app)
      .post('/lti/launch')
      .then(res => {
        expect(res).to.have.status(401);
      });
  });

  it.skip('Launch route POST request with registered platform is expected to redirect to authenticationEndpoint', async () => {
    nock('http://localhost/canvas')
      .get(/\/AuthorizationUrl?.*/)
      .reply(200);

    return chai
      .request(app)
      .post('/lti/launch')
      .send({ iss: 'http://localhost/canvas', client_id: 'TODO', login_hint: '2', target_link_uri: 'http://localhost:3000/' })
      .then(res => {
        expect(res).to.redirectTo(/^http:\/\/localhost\/canvas\/AuthorizationUrl.*/);
        expect(res).to.have.status(200);
      });
  });

  it('Launch with BadPayload - Wrong aud claim. Expected to return 401 error', async () => {
    const token = JSON.parse(JSON.stringify(tokenValid));
    token.aud = 'WRONG_CLIENTID';
    token.nonce = encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``);
    const state = encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``);

    nock('http://localhost/canvas')
      .get('/keyset')
      .reply(200, {
        keys: [
          {
            kty: 'RSA',
            e: 'AQAB',
            kid: '123456',
            n:
              'VrJSr-xli8NfuAdk_Wem5BARmmW4BpJvXBx3MbFY_0grH9Cd7OxBwVYSwI4P4yhL27upa1_FCRwLi3raOPSJOkHEDvFwtyYZMvdYcpDYTv6JRVqbgEyZtHa-vjL1wBqqW75yPDRoyZdnA8MWrfyRUOak53ZVWHRKgBnP53oXm7M',
          },
        ],
      });
    return chai
      .request(app)
      .post('/lti/launch')
      .type('json')
      .send({ id_token: token, state: state })
      .set('Cookie', [
        'state' + state + '=s%3Ahttp%3A%2F%2Flocalhost%2Fcanvas.fsJogjTuxtbJwvJcuG4esveQAlih67sfEltuwRM6MX0; Path=/; HttpOnly;',
      ])
      .then(res => {
        expect(res).to.to.have.status(401);
      });
  });

  it('BadPayload - No LTI Version. Expected to redirect to return 401 error', async () => {
    const token = JSON.parse(JSON.stringify(tokenValid));
    token.nonce = encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``);
    delete token['https://purl.imsglobal.org/spec/lti/claim/version'];
    const state = encodeURIComponent([...Array(25)].map(_ => (((crypto.randomBytes(4).readUInt32LE() / 4294967295) * 36) | 0).toString(36)).join``);

    nock('http://localhost/canvas')
      .get('/keyset')
      .reply(200, {
        keys: [
          {
            kty: 'RSA',
            e: 'AQAB',
            kid: '123456',
            n:
              'VrJSr-xli8NfuAdk_Wem5BARmmW4BpJvXBx3MbFY_0grH9Cd7OxBwVYSwI4P4yhL27upa1_FCRwLi3raOPSJOkHEDvFwtyYZMvdYcpDYTv6JRVqbgEyZtHa-vjL1wBqqW75yPDRoyZdnA8MWrfyRUOak53ZVWHRKgBnP53oXm7M',
          },
        ],
      });
    return chai
      .request(app)
      .post('/lti/launch')
      .type('json')
      .send({ id_token: token, state: state })
      .set('Cookie', [
        'state' + state + '=s%3Ahttp%3A%2F%2Flocalhost%2Fcanvas.fsJogjTuxtbJwvJcuG4esveQAlih67sfEltuwRM6MX0; Path=/; HttpOnly;',
      ])
      .then(res => {
        expect(res).to.have.status(401);
      });
  });

  //  Need to cover all remaining functionalities oncce DB cofig done for local test.
});
