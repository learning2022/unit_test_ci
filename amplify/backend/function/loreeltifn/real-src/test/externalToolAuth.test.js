const chai = require('chai');
const LoreeLti = require('../app');
const expect = chai.expect;
const sinon = require('sinon');
const chaiHttp = require('chai-http');
const Database = require('../Utils/Database')
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
chai.use(chaiHttp);

const ltiLoginHintMock = {
  id: '6c1cac98-56a0-4885-9b49-13c36cb4ffac',
  loreeUserEmail: 'canvas_123670000000001180@example.com',
  issuerUrl: 'https://canvas.instructure.com',
  messageType: 'LtiDeepLinkingRequest',
  resourceLink: 'https://limon.h5p.com/lti/launch',
  clientId: '0000000000101',
}

const ltiAccessTokenMock = {
  id: '12345',
  loreeUsername: 'canvas_123670000000001180@example.com',
  email: 'priyanga.alagar@crystaldelta.com'
}

const ltiToolMock = {
  id: '1234567890',
  issuerUrl: 'https://canvas.instructure.com',
  clientId: '12000000000012',
  oidcUrl: 'https://limon.h5p.com/lti/login',
}

const ltiToolDeploymentMock = {
  id: 'beb3443c-3f4f-4e88-911f-4c8a91bdd43b'
}

describe('#External tool auth', () => {
  afterEach(function () {
    sinon.restore();
  });
  it('should redirect for lti deep linking request', () => {
    sinon.stub(Database, 'getLtiToolLoginHint').returns(ltiLoginHintMock);
    sinon.stub(Database, 'queryLtiAccessToken').returns(ltiAccessTokenMock);
    sinon.stub(Database, 'findLtiToolByClientId').returns(ltiToolMock);
    sinon.stub(Database, 'fetchLtiToolDeployments').returns(ltiToolDeploymentMock);
      return chai
      .request(LoreeLti)
      .get('/lti/tool/authorize_redirect')
      .query({
        scope: 'openid',
        response_type: 'id_token',
        response_mode: 'form_post',
        prompt: 'none',
        client_id: '0000000000101',
        redirect_uri: 'some-redirect_uri',
        state: 'some-unique-token',
        nonce: 'some-unique-nonce',
        login_hint: '6c1cac98-56a0-4885-9b49-13c36cb4ffac',
      }).then((res) => {
        expect(res).to.redirect;
      })
  })
    it('should redirect for lti resource request', () => {
    ltiLoginHintMock.messageType = 'LtiResourceLinkRequest'
    sinon.stub(Database, 'getLtiToolLoginHint').returns(ltiLoginHintMock);
    sinon.stub(Database, 'getLtiAccessToken').returns(ltiAccessTokenMock);
    sinon.stub(Database, 'findLtiToolByClientId').returns(ltiToolMock);
    sinon.stub(Database, 'fetchLtiToolDeployments').returns(ltiToolDeploymentMock);
      return chai
      .request(LoreeLti)
      .get('/lti/tool/authorize_redirect')
      .query({
        scope: 'openid',
        response_type: 'id_token',
        response_mode: 'form_post',
        prompt: 'none',
        client_id: '0000000000101',
        redirect_uri: 'some-redirect_uri',
        state: 'some-unique-token',
        nonce: 'some-unique-nonce',
        login_hint: '6c1cac98-56a0-4885-9b49-13c36cb4ffac',
      }).then((res) => {
        expect(res).to.redirect;
      })
  })
  it('should return 500 status when getLtiToolLoginHint throws error', () => {
    sinon.stub(Database, 'getLtiToolLoginHint').throws(new Error('Error in getLtiToolLoginHint'));
    sinon.stub(Database, 'getLtiAccessToken').returns(ltiAccessTokenMock);
    return chai
      .request(LoreeLti)
      .get('/lti/tool/authorize_redirect')
      .query({
        scope: 'openid',
        response_type: 'id_token',
        response_mode: 'form_post',
        prompt: 'form_post',
        client_id: '0000000000101',
        redirect_uri: 'some-redirect_uri',
        state: 'some-unique-token',
        nonce: 'some-unique-nonce',
        login_hint: '6c1cac98-56a0-4885-9b49-13c36cb4ffac',
        }).then((res) => {
          expect(res).to.have.status(500);
        })
  })
  it('should return 401 when client id does not match', () => {
    ltiLoginHintMock.clientId = '0000000000102'
    sinon.stub(Database, 'getLtiToolLoginHint').returns(ltiLoginHintMock);
    sinon.stub(Database, 'getLtiAccessToken').returns(ltiAccessTokenMock);
    return chai
      .request(LoreeLti)
      .get('/lti/tool/authorize_redirect')
      .query({
        scope: 'scope',
        response_type: 'id_token',
        response_mode: 'form_post',
        prompt: 'form_post',
        client_id: '0000000000101',
        redirect_uri: 'some-redirect_uri',
        state: 'some-unique-token',
        nonce: 'some-unique-nonce',
        login_hint: '6c1cac98-56a0-4885-9b49-13c36cb4ffac',
      }).then((res) => {
        expect(res).to.have.status(401);
      })
  })
})

describe('#External tool content', () => {
  afterEach(function () {
    sinon.restore();
  });
  it('should redirect to content page', () => {
    sinon.stub(Database, 'findLtiToolByClientId').returns(ltiToolMock);
    return chai
      .request(LoreeLti)
      .post('/lti/tool')
      .send({
        JWT: `eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJpc3MiOiIxMjMwMDAwMDAwMDAxMjMiLCJhdWQiOlsiaHR0cHM6XC9cL2x0Nm1oc3g1NGkuZXhlY3V0ZS1hcGkuYXAtc291dGhlYXN0LTIuYW1hem9uYXdzLmNvbVwvYW5kaWJveFwvbHRpXC90b29sIl0sImV4cCI6MTY0ODIwMDQ3OCwiaWF0IjoxNjQ4MTk5ODc4LCJub25jZSI6InkxQ2N2amdKTVVHbHNlbDVSTjlVIiwiaHR0cHM6XC9cL3B1cmwuaW1zZ2xvYmFsLm9yZ1wvc3BlY1wvbHRpXC9jbGFpbVwvZGVwbG95bWVudF9pZCI6IjIwMGQxMDFmLTJjMTQtNDM0YS1hMGYzLTU3YzJhNDIzNjlmYyIsImh0dHBzOlwvXC9wdXJsLmltc2dsb2JhbC5vcmdcL3NwZWNcL2x0aVwvY2xhaW1cL21lc3NhZ2VfdHlwZSI6Ikx0aURlZXBMaW5raW5nUmVzcG9uc2UiLCJodHRwczpcL1wvcHVybC5pbXNnbG9iYWwub3JnXC9zcGVjXC9sdGlcL2NsYWltXC92ZXJzaW9uIjoiMS4zLjAiLCJodHRwczpcL1wvcHVybC5pbXNnbG9iYWwub3JnXC9zcGVjXC9sdGktZGxcL2NsYWltXC9jb250ZW50X2l0ZW1zIjpbeyJ0eXBlIjoibHRpUmVzb3VyY2VMaW5rIiwidGl0bGUiOiJDbG9uZSBvZiBJbWFnZSBTbGlkZXIiLCJ1cmwiOiJodHRwczpcL1wvbGltb24uaDVwLmNvbVwvY29udGVudFwvMTI5MTU5MDI4OTU3Mjc0NzcxOSIsInRleHQiOiJJbWFnZSBTbGlkZXIiLCJpY29uIjp7InVybCI6Imh0dHBzOlwvXC9hcC1zb3V0aGVhc3QtMi5jZG4uaDVwLmNvbVwvbGlicmFyaWVzXC9INVAuSW1hZ2VTbGlkZXItMS4xXC9pY29uLnN2ZyIsIndpZHRoIjoyNjcsImhlaWdodCI6MTUwfSwidGh1bWJuYWlsIjp7InVybCI6Imh0dHBzOlwvXC9hcC1zb3V0aGVhc3QtMi5jZG4uaDVwLmNvbVwvbGlicmFyaWVzXC9INVAuSW1hZ2VTbGlkZXItMS4xXC9pY29uLnN2ZyIsIndpZHRoIjoyNjcsImhlaWdodCI6MTUwfSwiaWZyYW1lIjp7IndpZHRoIjo3NzksImhlaWdodCI6NjAwfSwibGluZUl0ZW0iOnsibGFiZWwiOiJDbG9uZSBvZiBJbWFnZSBTbGlkZXIiLCJzY29yZU1heGltdW0iOjEwLCJzdWJtaXNzaW9uUmV2aWV3Ijp7InJldmlld2FibGVTdGF0dXMiOlsiQ29tcGxldGVkIl19fX1dfQ.UfvlHOikOOlUMEplAA2MaEUvDirf5Xm_1d1hUmASOUWEb-gxlv6gBcI-F7sKhGTdZIlo4tiejtgCRms6JkqkKCY6r9OTCqvqVmzRSAiDH-peGz8DUMoHZR4TL9uVBZoU-VX1PJnSt44Uko-xkaADr_keNbsaE47uqM8UeAPqpHPHIlShWmB3TrTkKjE_6kaLKAir82Yn_cmNW26JUEaXvixrrFbftpyi-IX8cVpHFDNAfKQxPg5VAT2IOFUhSHOM0R9wfyYZXG1lGLADQR5ripM1TpEllgtlAGhnqWL0N0k81hazmXNPGvEYHlvMImH6Yi83vJ5ysl2-7qtGZYFeTg`,
        }).then((res) => {
          expect(res).to.redirect;
        })
  })
  it('should return 500 when findLtiToolByClientId throws error', () => {
    sinon.stub(Database, 'findLtiToolByClientId').throws(new Error('Error in findLtiToolByClientId'));
    return chai
      .request(LoreeLti)
      .post('/lti/tool')
      .send({
        JWT: `eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJpc3MiOiIxMjMwMDAwMDAwMDAxMjMiLCJhdWQiOlsiaHR0cHM6XC9cL2x0Nm1oc3g1NGkuZXhlY3V0ZS1hcGkuYXAtc291dGhlYXN0LTIuYW1hem9uYXdzLmNvbVwvYW5kaWJveFwvbHRpXC90b29sIl0sImV4cCI6MTY0ODIwMDQ3OCwiaWF0IjoxNjQ4MTk5ODc4LCJub25jZSI6InkxQ2N2amdKTVVHbHNlbDVSTjlVIiwiaHR0cHM6XC9cL3B1cmwuaW1zZ2xvYmFsLm9yZ1wvc3BlY1wvbHRpXC9jbGFpbVwvZGVwbG95bWVudF9pZCI6IjIwMGQxMDFmLTJjMTQtNDM0YS1hMGYzLTU3YzJhNDIzNjlmYyIsImh0dHBzOlwvXC9wdXJsLmltc2dsb2JhbC5vcmdcL3NwZWNcL2x0aVwvY2xhaW1cL21lc3NhZ2VfdHlwZSI6Ikx0aURlZXBMaW5raW5nUmVzcG9uc2UiLCJodHRwczpcL1wvcHVybC5pbXNnbG9iYWwub3JnXC9zcGVjXC9sdGlcL2NsYWltXC92ZXJzaW9uIjoiMS4zLjAiLCJodHRwczpcL1wvcHVybC5pbXNnbG9iYWwub3JnXC9zcGVjXC9sdGktZGxcL2NsYWltXC9jb250ZW50X2l0ZW1zIjpbeyJ0eXBlIjoibHRpUmVzb3VyY2VMaW5rIiwidGl0bGUiOiJDbG9uZSBvZiBJbWFnZSBTbGlkZXIiLCJ1cmwiOiJodHRwczpcL1wvbGltb24uaDVwLmNvbVwvY29udGVudFwvMTI5MTU5MDI4OTU3Mjc0NzcxOSIsInRleHQiOiJJbWFnZSBTbGlkZXIiLCJpY29uIjp7InVybCI6Imh0dHBzOlwvXC9hcC1zb3V0aGVhc3QtMi5jZG4uaDVwLmNvbVwvbGlicmFyaWVzXC9INVAuSW1hZ2VTbGlkZXItMS4xXC9pY29uLnN2ZyIsIndpZHRoIjoyNjcsImhlaWdodCI6MTUwfSwidGh1bWJuYWlsIjp7InVybCI6Imh0dHBzOlwvXC9hcC1zb3V0aGVhc3QtMi5jZG4uaDVwLmNvbVwvbGlicmFyaWVzXC9INVAuSW1hZ2VTbGlkZXItMS4xXC9pY29uLnN2ZyIsIndpZHRoIjoyNjcsImhlaWdodCI6MTUwfSwiaWZyYW1lIjp7IndpZHRoIjo3NzksImhlaWdodCI6NjAwfSwibGluZUl0ZW0iOnsibGFiZWwiOiJDbG9uZSBvZiBJbWFnZSBTbGlkZXIiLCJzY29yZU1heGltdW0iOjEwLCJzdWJtaXNzaW9uUmV2aWV3Ijp7InJldmlld2FibGVTdGF0dXMiOlsiQ29tcGxldGVkIl19fX1dfQ.UfvlHOikOOlUMEplAA2MaEUvDirf5Xm_1d1hUmASOUWEb-gxlv6gBcI-F7sKhGTdZIlo4tiejtgCRms6JkqkKCY6r9OTCqvqVmzRSAiDH-peGz8DUMoHZR4TL9uVBZoU-VX1PJnSt44Uko-xkaADr_keNbsaE47uqM8UeAPqpHPHIlShWmB3TrTkKjE_6kaLKAir82Yn_cmNW26JUEaXvixrrFbftpyi-IX8cVpHFDNAfKQxPg5VAT2IOFUhSHOM0R9wfyYZXG1lGLADQR5ripM1TpEllgtlAGhnqWL0N0k81hazmXNPGvEYHlvMImH6Yi83vJ5ysl2-7qtGZYFeTg`,
        }).then((res) => {
          expect(res).to.have.status(500);
        })
  })
})
describe('#External tool content launch', () => {
  afterEach(function () {
    sinon.restore();
  });
  it('should redirect to login form when id is present in query', () => {
    sinon.stub(Database, 'getLtiTool').returns(ltiToolMock);
    return chai
      .request(LoreeLti)
      .get('/lti/tool')
      .query({
        id: '1234567890',
        content_url: 'https://limon.h5p.com/content/1291590289158592689'
      }).then((res) => {
        expect(res).to.redirect;
      })
  });
  it('should redirect to login form when id is not present in query', () => {
    sinon.stub(Database, 'fetchLtiToolByDomain').returns(ltiToolMock);
    return chai
      .request(LoreeLti)
      .get('/lti/tool')
      .query({
        content_url: 'https://limon.h5p.com/content/1291590289158592689'
      }).then((res) => {
        expect(res).to.redirect;
      })
  });
  it('should return 500 when getLtiTool throws error', () => {
    sinon.stub(Database, 'getLtiTool').throws(new Error('Error in getLtiTool'));
    return chai
      .request(LoreeLti)
      .get('/lti/tool')
      .query({
        id: '1234567890',
        content_url: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool?6604f4f3-7860-4a7b-b8fa-5cf17440a218&content_url=https://limon.h5p.com/content/1291590289158592689'
      }).then((res) => {
        expect(res).to.have.status(500);
    })
  })
});
