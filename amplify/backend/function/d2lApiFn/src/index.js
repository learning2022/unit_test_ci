/* Amplify Params - DO NOT EDIT
	API_LOREEV2API_GRAPHQLAPIIDOUTPUT
	API_LOREEV2API_LTIACCESSTOKENTABLE_ARN
	API_LOREEV2API_LTIACCESSTOKENTABLE_NAME
	API_LOREEV2API_LTIAPIKEYTABLE_ARN
	API_LOREEV2API_LTIAPIKEYTABLE_NAME
	API_LOREEV2API_LTIPLATFORMKEYTABLE_ARN
	API_LOREEV2API_LTIPLATFORMKEYTABLE_NAME
	API_LOREEV2API_LTIPLATFORMTABLE_ARN
	API_LOREEV2API_LTIPLATFORMTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const Modules = require('./Api/Modules');
const Contents = require('./Api/Contents');
const AdminDashbaord = require('./Api/AdminDashboard');
const ImageUpload = require('./fileUpload/imageUpload');

const resolvers = {
  Query: {
    d2lModules: ctx => {
      return Modules.listModules(ctx.identity.claims["email"], ctx.arguments.courseId);
    },
    viewD2lContents: ctx => {
      return Contents.viewContents(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.contentId, ctx.arguments.contentType);
    },
    d2lAdminRoles: ctx => {
      return AdminDashbaord.d2lRoles(ctx.identity.claims["email"]);
    },
    fetchS3Images: ctx => {
      return ImageUpload.fetchS3Images(ctx.arguments.platformId, ctx.arguments.lmsUrl);
    },
  },
  Mutation: {
    updateD2lContents: ctx => {
      return Contents.updateContents(
        ctx.identity.claims["email"],
        ctx.arguments.courseId,
        ctx.arguments.contentId,
        ctx.arguments.contentType,
        ctx.arguments.formData,
      );
    },
    createD2lModules: ctx => {
      return Modules.createModules(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.formData);
    },
    createD2LSubContent: ctx => {
      return Modules.createSubContent(ctx.identity.claims["email"], ctx.arguments.courseId, ctx.arguments.folderId, ctx.arguments.formData);
    },
    uploadD2LImagesToS3: ctx => {
      return ImageUpload.uploadD2LImagesToS3(
        ctx.arguments.platformId,
        ctx.arguments.file,
        ctx.arguments.title,
        ctx.arguments.newTitle,
        ctx.arguments.lmsUrl,
      );
    },
  },
};

exports.handler = async event => {
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      return await resolver(event);
    }
  }
  throw new Error('Resolver not found.');
};
