const aws = require('aws-sdk');
const s3 = new aws.S3();

class D2lImageToS3 {
  //Temporary function for D2l image upload handling(For demo purpose),
  // will change it as D2l image directory API's,once we get clear API from D2l.

  s3Upload = async (param, platformId, title, lmsUrl) => {
    try {
      await s3.putObject(param).promise();
      const lmsImageUrl = lmsUrl ? `https://cd-d2l-${lmsUrl}` : process.env.IMAGE_S3_URL;
      return `${lmsImageUrl}/${platformId}/d2l-img-blocks/${title}`;
    } catch (err) {
      console.log('Error in image upload', err);
      return '';
    }
  };

  checkImageExist = async ImageKey => {
    try {
      const checkImage = await s3.headObject(ImageKey).promise();
      if (checkImage) {
        console.log('Image already exist');
        return true;
      }
    } catch (err) {
      console.log('Image not exits', err);
      return false;
    }
  };

  uploadD2LImagesToS3 = async (platformId, file, title, newTitle, lmsUrl) => {
    try {
      const base64Data = new Buffer.from(file.replace(/^data:image\/\w+;base64,/, ''), 'base64');
      const type = file.split(';')[0].split('/')[1];

      let ImageKey = {
        Bucket: process.env.BUCKET_NAME,
        Key: `${platformId}/d2l-img-blocks/${title}`,
      };
      let param = {
        ...ImageKey,
        Body: base64Data,
        ContentEncoding: 'base64',
        ContentType: `image/${type}`,
        ACL: 'public-read',
      };

      let imageNameCheck = await this.checkImageExist(ImageKey);
      console.log('imageNameCheck response', imageNameCheck);
      if (imageNameCheck) {
        console.log('duplicate image uploading...');
        param.Key = `${platformId}/d2l-img-blocks/${newTitle}`;
        return this.s3Upload(param, platformId, newTitle, lmsUrl);
      } else {
        console.log('Original Image uploading...');
        return this.s3Upload(param, platformId, title, lmsUrl);
      }
    } catch (e) {
      console.log('Error in uploading image to S3', e);
    }
  };

  fetchS3Images = async (platformId, lmsUrl) => {
    const lmsImageUrl = lmsUrl ? `https://cd-d2l-${lmsUrl}` : process.env.IMAGE_S3_URL;
    const fetchParams = {
      Bucket: process.env.BUCKET_NAME,
      Prefix: `${platformId}/d2l-img-blocks`,
    };
    let listImages = await s3.listObjectsV2(fetchParams).promise();
    listImages.Contents.sort(function (a, b) {
      return a.LastModified - b.LastModified;
    });
    listImages = listImages.Contents;
    let s3ImageArray = [];
    for (let image of listImages) {
      let imageName = image.Key.split(`${platformId}/d2l-img-blocks/`)[1];
      s3ImageArray.push({
        name: imageName,
        src: `${lmsImageUrl}/${image.Key}`,
        lastModified: image.LastModified,
      });
    }
    console.log('s3ImageArray', s3ImageArray);
    return s3ImageArray;
  };
}

module.exports = new D2lImageToS3();
