const Request = require('./Request');

class Contents {
  async viewContents(loreeUserEmail, courseId, contentId, contentType) {
    let endPoint;
    if (contentType === 'topics') {
      endPoint = `le/1.34/${courseId}/content/${contentType}/${contentId}/file`;
    } else {
      endPoint = `le/1.34/${courseId}/content/${contentType}/${contentId}`;
    }
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async updateContents(loreeUserEmail, courseId, contentId, contentType, formData) {
    let endPoint;
    console.log('Update content details', loreeUserEmail, courseId, contentId, contentType, formData, typeof formData);
    if (contentType === 'topics') {
      endPoint = `le/unstable/${courseId}/content/${contentType}/${contentId}/file`;
    } else {
      endPoint = `le/1.34/${courseId}/content/${contentType}/${contentId}`;
    }
    return await Request.axiosRequest(endPoint, 'put', formData, loreeUserEmail);
  }
}

module.exports = new Contents();
