const axios = require('axios');
const Database = require('../Database');
const qs = require('qs');

class Request {
  async getAccessToken(id) {
    const accessToken = await Database.queryLtiAccessTokenByEmail(id);
    return accessToken;
  }

  async CheckTokenValidity(userAccessToken) {
    console.log('Access token', userAccessToken);
    let refresh_token = userAccessToken.refreshToken;
    let currentTime = new Date().toISOString();
    const getApiKeyData = await Database.getApiKeyData(userAccessToken.ltiApiKeyID);
    let generateNewtoken = await this.refreshDBToken(refresh_token, getApiKeyData);
    console.log('Token refreshed', generateNewtoken);
    if (generateNewtoken) {
      await Database.updateLtiAccessToken(
        userAccessToken.id,
        generateNewtoken.data.access_token,
        currentTime,
        generateNewtoken.data.refresh_token,
      );
      return generateNewtoken.data.access_token;
    } else {
      return false;
    }
  }

  refreshDBToken = async (refresh_token, data) => {
    //get refreshed access token
    let formData = qs.stringify({
      grant_type: 'refresh_token',
      client_id: data.apiClientId,
      client_secret: data.apiSecretKey,
      refresh_token: `${refresh_token}`,
    });
    try {
      let refreshParams = {
        method: 'post',
        url: data.oauthTokenUrl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: formData,
      };

      let response = await axios(refreshParams);
      return response;
    } catch (err) {
      console.log('Refresh token failed');
      return false;
    }
  };

  async axiosRequest(url, method, postData, tokenId, multiPart = false) {
    console.log(`TOKKEN ID--> axios ${tokenId}- ${postData} - ${method} - ${url} - ${multiPart}`);
    const canvasData = await this.getAccessToken(tokenId);
    const options = {
      method: method,
      url: `${canvasData.lmsApiUrl}/d2l/api/${url}`,
      headers: {
        Authorization: `Bearer ${canvasData.accessToken}`,
      },
      data: postData.pageDetails ? postData.pageDetails : postData,
    };
    if (multiPart) options.headers['Content-Type'] = 'multipart/mixed; boundary=xxBOUNDARYxx';
    console.log('Axios Reqeust Options', options);
    try {
      const response = await axios.request(options);
      console.log('Axios Response', response);
      const responseData = {
        statusCode: response.status,
        body: response.data,
      };
      return responseData;
    } catch (err) {
      console.log('axios error', err);
      if (err.response.status === 401) {
        return this.axiosRefreshRequest(err, canvasData);
      } else {
        const responseData = {
          statusCode: err.response.status,
          body: err.response.data,
        };
        return responseData;
      }
    }
  }

  async axiosRefreshRequest(err, token) {
    const refreshToken = await this.CheckTokenValidity(token);
    if (refreshToken) {
      console.log('Inside refresh token', refreshToken);
      err.response.config.headers['Authorization'] = 'Bearer ' + refreshToken;
      try {
        console.log('Error response API', err.response.config);
        const apiResponse = await axios(err.response.config);
        const responseData = {
          statusCode: apiResponse.status,
          body: apiResponse.data,
        };
        return responseData;
      } catch (error) {
        console.log('Error response API failed', error);
        const responseData = {
          statusCode: error.response.status,
          body: error.response.data,
        };
        return responseData;
      }
    } else {
      const responseData = {
        statusCode: 401,
        body: 'Unauthorized',
      };
      return responseData;
    }
  }
}

module.exports = new Request();
