const Request = require('./Request');

class AdminDashboard {
  // fetch Roles
  async d2lRoles(loreeUserEmail) {
    const endPoint = `lp/1.26/roles/`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }
}

module.exports = new AdminDashboard();
