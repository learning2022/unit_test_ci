const Request = require('./Request');

class Modules {
  // fetch modules
  async listModules(loreeUserEmail, courseId) {
    const endPoint = `le/1.34/${courseId}/content/toc?loadDescription=true`;
    const data = {};
    return await Request.axiosRequest(endPoint, 'get', data, loreeUserEmail);
  }

  async createModules(loreeUserEmail, courseId, formData) {
    const endPoint = `le/1.41/${courseId}/content/root/?renameFileIfExists=true`;
    const data = formData;
    return await Request.axiosRequest(endPoint, 'post', data, loreeUserEmail);
  }

  async createSubContent(loreeUserEmail, courseId, folderId, formData) {
    let multiPart = false;
    if (formData.pageDetails && formData.pageDetails.includes('--xxBOUNDARYxx')) multiPart = true;
    const endPoint = `le/1.41/${courseId}/content/modules/${folderId}/structure/?renameFileIfExists=true`;
    const data = formData;
    return await Request.axiosRequest(endPoint, 'post', data, loreeUserEmail, multiPart);
  }
}

module.exports = new Modules();
