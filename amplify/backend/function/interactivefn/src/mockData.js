const h5pApiMockData = {
  h5pResponse: {
    message: 'Data fetched',
    h5pList: [
      {
        id: '164',
        h5p_type: 'Image Hotspots',
        title: 'National Quality Framework - hotspot',
        created_user: '66',
        created_by: 'Andiswamy Rajagopal',
        account_id: '18',
        created_at: '2021-06-02 13:51:18',
        updated_at: '2021-06-02 13:51:18',
        metaData: [
          {
            key: 'Course',
            value: ['Science', 'Civics', 'Math'],
          },
        ],
      },
    ],
  },
  metaDataToUpdate: [
    {
      key: 'Course',
      value: ['Science', 'Civics', 'GEO'],
    },
  ],
  updatedMetaData: {
    status: 1,
    message: 'H5p meta data updated successfully',
    Updatedh5pComponent: [
      {
        id: '0',
        h5p_id: '2',
        account_id: '0',
        metaData: '[\n{\n"key":"Course",\n"value":[\n"Science",\n"Civics",\n"GEO"\n]\n}\n]',
      },
    ],
  },
};

module.exports = h5pApiMockData;
