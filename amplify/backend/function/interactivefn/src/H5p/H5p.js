const qs = require('qs');
const axios = require('axios');

class H5P {
  h5pUserInitiate = async (email, firstName, role, platformId, platformDomain) => {
    try {
      const user_login = email.substr(0, email.indexOf('@'));
      const user = `${user_login}_${platformId}`;
      //for new user
      var data = qs.stringify({
        email: email,
        first_name: firstName,
        role: role,
        user_login: user,
        password: user_login,
        platform_id: platformId,
        platform_domain: platformDomain,
      });
      const url = `${process.env.H5P_API_URL}/registerUser`;
      const options = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  getH5PContent = async (userName, platformId) => {
    try {
      const data = qs.stringify({
        user: userName,
        platformId: platformId,
      });
      const url = `${process.env.H5P_API_URL}/getH5pContents`;
      const config = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(config);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  getAdminH5PContent = async platformId => {
    try {
      const data = qs.stringify({
        platformId: platformId,
      });
      const url = `${process.env.H5P_API_URL}/getH5PList`;
      const config = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(config);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  deleteH5PContent = async (contentId, platformId) => {
    try {
      const data = qs.stringify({
        id: contentId,
        platformId: platformId,
      });
      const url = `${process.env.H5P_API_URL}/deleteH5pContent`;
      const config = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(config);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  updateH5pMetaData = async (contentId, platformId, metaData) => {
    try {
      const data = qs.stringify({
        id: contentId,
        platformId: platformId,
        metaData: metaData,
      });
      const url = `${process.env.H5P_API_URL}/updateH5pMetaData`;
      const config = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(config);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  request = async options => {
    try {
      const response = await axios.request(options);
      console.log('Axios Response', response);
      const responseData = {
        statusCode: response.status,
        body: response.data,
      };
      return responseData;
    } catch (error) {
      console.log(error);
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
}

module.exports = new H5P();
