const nock = require('nock');
const H5P = require('./H5p/H5p');
const h5pMockData = require('./mockData');

describe('Test the Request to fetch list of h5ps', () => {
  let platformId;
  beforeEach(() => {
    process.env.H5P_API_URL = 'https://loree-h5p-dev.crystaldelta.net/index.php/wp-json/api';
    platformId = 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a';
    nock(process.env.H5P_API_URL).post('/getH5PList', `platformId=${platformId}`).reply(200, h5pMockData.h5pResponse);
    nock(process.env.H5P_API_URL).post('/updateH5pMetaData').reply(200, h5pMockData.updatedMetaData);
  });

  test('By passing platformId as a param to getAdminH5PContent function', async () => {
    //get Admin H5p contents
    const h5pLists = await H5P.getAdminH5PContent(platformId);
    expect(h5pLists.statusCode).toBe(200);
    expect(h5pLists.body.message).toEqual('Data fetched');
    expect(h5pLists.body.h5pList).not.toBeNull();
  });

  test('should return the updated meta data h5p response while updating from Admin dashboard', async () => {
    // update h5p meta data
    const h5pLists = await H5P.updateH5pMetaData('164', platformId, h5pMockData.metaDataToUpdate);
    expect(h5pLists.statusCode).toBe(200);
    expect(h5pLists.body.message).toEqual('H5p meta data updated successfully');
    expect(h5pLists.body.Updatedh5pComponent).not.toBeNull();
  });
});
