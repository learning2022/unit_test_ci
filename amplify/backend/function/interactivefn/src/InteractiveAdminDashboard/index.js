const Interactive = require('../Interactives/Interactive');
const H5p = require('../H5p/H5p');

class adminDashboardInteractive {
  async totalInteractivesCount(organization, email, platformId) {
    try {
      const interactiveResponse = await Interactive.listLoreeInteractive(organization, email);
      const userData = email.substr(0, email.indexOf('@'));
      const userName = `${userData}_${platformId}`;
      const h5pResponse = await H5p.getH5PContent(userName, platformId);
      console.log('Interactives count:', interactiveResponse, h5pResponse.h5pContents);
      var data = {
        InteractiveData: interactiveResponse.body.length,
        H5pData: h5pResponse.body.h5pContents.length,
      };
      return {
        statusCode: 200,
        body: data,
      };
    } catch (err) {
      console.error('Error in interactive. Error:', err);
      return {
        statusCode: 500,
        body: err,
      };
    }
  }
}
module.exports = new adminDashboardInteractive();
