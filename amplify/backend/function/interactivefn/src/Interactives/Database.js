const AWSXRay = require('aws-xray-sdk');
const AWS = AWSXRay.captureAWS(require('aws-sdk'));

const docClient = new AWS.DynamoDB.DocumentClient();

class Database {
  async queryLtiAccessTokenByEmail(email) {
    const params = {
      TableName: process.env.API_LOREEV2API_LTIACCESSTOKENTABLE_NAME,
      IndexName: 'byEmail',
      KeyConditionExpression: '#name = :value',
      ExpressionAttributeValues: { ':value': email },
      ExpressionAttributeNames: { '#name': 'loreeUsername' },
    };
    try {
      const accessTokenData = await docClient.query(params).promise();
      return accessTokenData.Items[0];
    } catch (err) {
      return err;
    }
    
  }
}

module.exports = new Database();
