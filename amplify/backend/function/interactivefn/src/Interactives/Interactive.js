const interativeUrl = process.env.INTERACTIVE_URL;
const axios = require('axios');
const qs = require('qs');
const Database = require('./Database.js');

class Interactive {
  library = async userId => {
    const url = `${interativeUrl}/library`;
    const options = {
      method: 'get',
      url: url,
    };
    return await this.request(options);
  };

  initiate = async (loreeUserEmail, type, email, name, organization) => {
    try {
      var initiateData = {};
      if (type == 'CANVAS') {
        const token = await Database.queryLtiAccessTokenByEmail(loreeUserEmail);
        initiateData = qs.stringify({
          email: token.loreeUsername,
          name: token.loreeUsername,
          organization: token.lmsApiUrl.replace('http://', '').replace('https://', ''),
        });
      } else {
        initiateData = qs.stringify({
          email: email,
          name: name,
          organization: organization,
        });
      }
      const url = `${interativeUrl}/user/initiate`;
      const options = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: initiateData,
      };
      console.log(options);
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  verify = async (user, orgId) => {
    try {
      var data = qs.stringify({
        user: user,
        org_id: orgId,
      });
      const url = `${interativeUrl}/user/verify`;
      const options = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  content = async (userId, orgId) => {
    try {
      const url = `${interativeUrl}/content`;
      const options = {
        method: 'get',
        url: url,
        headers: {
          user_id: userId,
          org_id: orgId,
        },
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  contentPagination = async (userId, orgId, pageId, pageLimit) => {
    try {
      const url = `${interativeUrl}/content/paginate/${pageId}/${pageLimit}`;
      const options = {
        method: 'get',
        url: url,
        headers: {
          user_id: userId,
          org_id: orgId,
        },
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  update = async (contentId, userId) => {
    try {
      var data = qs.stringify({
        user_id: userId,
      });
      const url = `${interativeUrl}/content/update/${contentId}`;
      const options = {
        method: 'put',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  request = async options => {
    try {
      const response = await axios.request(options);
      console.log('Axios Response', response);
      const responseData = {
        statusCode: response.status,
        body: response.data,
      };
      return responseData;
    } catch (error) {
      console.log(error);
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  listLoreeInteractive = async (organization, email) => {
    try {
      const url = `${interativeUrl}/content/loree/dashboard`;
      const options = {
        method: 'get',
        url: url,
        headers: {
          organization: organization,
          email: email,
        },
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };

  updateLoreeInteractiveContent = async (userId, title, contentId, childOrganization) => {
    try {
      var data = qs.stringify({
        user_id: userId,
        title: title,
        child_organization: child_organizations,
      });
      const url = `${interativeUrl}/content/edit/${contentId}`;
      const options = {
        method: 'put',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
  loreeInteractiveContentStatusUpdate = async (status, organization, childOrganization) => {
    try {
      var data = qs.stringify({
        private: status,
        organization: organization,
        child_organizations: childOrganization,
      });
      const url = `${interativeUrl}/content/status/update`;
      const options = {
        method: 'put',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
  duplicateLoreeInteractiveContent = async (email, title, contentId) => {
    try {
      var data = qs.stringify({
        email: email,
        title: title,
        id: contentId,
      });
      const url = `${interativeUrl}/content/duplicate`;
      const options = {
        method: 'post',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
  deleteLoreeInteractiveContent = async (userId, contentId) => {
    try {
      const url = `${interativeUrl}/content/${contentId}`;
      const options = {
        method: 'delete',
        url: url,
        headers: {
          user_id: userId,
        },
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
  getOrganizationAccounts = async email => {
    var data = qs.stringify({
      email: email,
    });
    try {
      const url = `${interativeUrl}/organization`;
      const options = {
        method: 'get',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
  updateOrganizationAccount = async (email, childOrganization) => {
    var data = qs.stringify({
      email: email,
      child_organizations: childOrganization,
    });
    try {
      const url = `${interativeUrl}/organization/update`;
      const options = {
        method: 'put',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
  contentShareUpdates = async (childOrganization, userId, shareStatus, contentId) => {
    var data = qs.stringify({
      child_organizations: childOrganization,
      user_id: userId,
      private: shareStatus,
    });
    try {
      const url = `${interativeUrl}/content/share/update/${contentId}`;
      const options = {
        method: 'put',
        url: url,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: data,
      };
      return await this.request(options);
    } catch (error) {
      const responseData = {
        statusCode: 500,
        body: error,
      };
      return responseData;
    }
  };
}

module.exports = new Interactive();
