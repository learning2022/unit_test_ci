/* Amplify Params - DO NOT EDIT
	API_LOREEV2API_GRAPHQLAPIIDOUTPUT
	API_LOREEV2API_LTIACCESSTOKENTABLE_ARN
	API_LOREEV2API_LTIACCESSTOKENTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const AWSXRay = require('aws-xray-sdk');
AWSXRay.captureHTTPsGlobal(require('http'));
AWSXRay.captureHTTPsGlobal(require('https'));
AWSXRay.capturePromise();

const Interactive = require('./Interactives/Interactive');
const H5P = require('./H5p/H5p');
const AdminDashboardInteractive = require('./InteractiveAdminDashboard');
const resolvers = {
  Query: {
    interativeLibrary: ctx => {
      return Interactive.library(ctx.arguments.userId);
    },
    interactiveContent: ctx => {
      return Interactive.content(ctx.arguments.userId, ctx.arguments.orgId);
    },
    interactiveContentPaginate: ctx => {
      return Interactive.contentPagination(ctx.arguments.userId, ctx.arguments.orgId, ctx.arguments.pageId, ctx.arguments.pageLimit);
    },
    getH5PContent: ctx => {
      return H5P.getH5PContent(ctx.arguments.userName, ctx.arguments.platformId);
    },
    getAdminH5PContent: ctx => {
      return H5P.getAdminH5PContent(ctx.arguments.platformId);
    },
    listLoreeInteractive: ctx => {
      return Interactive.listLoreeInteractive(ctx.arguments.organization, ctx.arguments.email);
    },
    getOrganizationAccounts: ctx => {
      return Interactive.getOrganizationAccounts(ctx.arguments.email);
    },
    AdminDashboardInteractive: ctx => {
      return AdminDashboardInteractive.totalInteractivesCount(ctx.arguments.organization, ctx.arguments.email, ctx.arguments.platformId);
    },
  },
  Mutation: {
    initiateInteractive: ctx => {
      return Interactive.initiate(
        ctx.identity.claims["email"],
        ctx.arguments.type,
        ctx.arguments.email,
        ctx.arguments.name,
        ctx.arguments.organization,
      );
    },
    verifyInteractive: ctx => {
      return Interactive.verify(ctx.arguments.user, ctx.arguments.orgId);
    },
    updateInteractiveContent: ctx => {
      return Interactive.update(ctx.arguments.contentId, ctx.arguments.userId);
    },
    updateLoreeInteractiveContent: ctx => {
      return Interactive.updateLoreeInteractiveContent(
        ctx.arguments.userId,
        ctx.arguments.title,
        ctx.arguments.contentId,
        ctx.arguments.childOrganization,
      );
    },
    loreeInteractiveContentStatusUpdate: ctx => {
      return Interactive.loreeInteractiveContentStatusUpdate(
        ctx.arguments.status,
        ctx.arguments.organization,
        ctx.arguments.childOrganization,
      );
    },
    duplicateLoreeInteractiveContent: ctx => {
      return Interactive.duplicateLoreeInteractiveContent(ctx.arguments.email, ctx.arguments.title, ctx.arguments.contentId);
    },
    updateOrganizationAccount: ctx => {
      return Interactive.updateOrganizationAccount(ctx.arguments.email, ctx.arguments.childOrganization);
    },
    contentShareUpdates: ctx => {
      return Interactive.contentShareUpdates(
        ctx.arguments.childOrganization,
        ctx.arguments.userId,
        ctx.arguments.shareStatus,
        ctx.arguments.contentId,
      );
    },
    deleteLoreeInteractiveContent: ctx => {
      return Interactive.deleteLoreeInteractiveContent(ctx.arguments.userId, ctx.arguments.contentId);
    },
    h5pUserInitiate: ctx => {
      return H5P.h5pUserInitiate(
        ctx.arguments.email,
        ctx.arguments.firstName,
        ctx.arguments.role,
        ctx.arguments.platformId,
        ctx.arguments.platformDomain,
      );
    },
    deleteH5PContent: ctx => {
      return H5P.deleteH5PContent(ctx.arguments.contentId, ctx.arguments.platformId);
    },
    updateH5pMetaData: ctx => {
      return H5P.updateH5pMetaData(ctx.arguments.contentId, ctx.arguments.platformId, ctx.arguments.metaData);
    },
  },
};

exports.handler = async event => {
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      return await resolver(event);
    }
  }
  throw new Error('Resolver not found.');
};
