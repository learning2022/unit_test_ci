# syntax=docker/dockerfile:1
FROM amazonlinux:2
RUN yum -y update && yum install -y curl
RUN curl -sL https://rpm.nodesource.com/setup_14.x | bash -
RUN yum install -y nodejs
RUN npm install -g yarn
RUN yum install -y git
RUN npm i @aws-amplify/cli@8.2.0
RUN node --version
RUN yum install python3-pip -y
