# Monitoring

## Testing Elastic APM Locally

```
# Build
export REACT_APP_LOREE_ENV=develop
export REACT_APP_GIT_COMMIT=$(node deploy/getGitCommit.js)
yarn build

# upload source maps
export APM_SECRET_KEY=GOGETTHISFROMELASTICADMIN
deploy/apm-upload-sourcemaps.sh $APM_SECRET_KEY

# run the app - note that this doesnt run off the production builds so source maps dont entirely work
yarn start-dev
```
