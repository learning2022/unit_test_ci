#!/bin/sh

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";

function do_install() {
  TPATH="${SCRIPT_DIR}/$1/src"
  cd "${TPATH}"
  echo ":${1}>"
  yarn install
  cd ${SCRIPT_DIR}
}

if [ ! -z ${1} ]; then
  do_install "${1}"
  exit $?;
fi

for dirname in amplify/backend/function/* ; do
  do_install "$dirname"
done
