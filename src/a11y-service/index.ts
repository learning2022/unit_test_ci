export { A11yChecker } from './components/A11yChecker';
export { A11yElementHighlighter, Highlight } from './components/A11yElementHighlighter';
export { useA11yContext, a11yContext } from './context/a11y-context';
export { A11yContextProvider } from './context/a11y-provider';
export { default as a11yService } from './service/a11y-service';

export type {
  A11yContext,
  QuickFixer,
  Step,
  StepImpact,
  Steps,
  StepsByImpact,
  StepsByKey,
  HighlightsOffset,
  CheckReport,
} from './types';
