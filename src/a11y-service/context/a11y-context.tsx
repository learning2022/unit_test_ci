import { createContext, useContext } from 'react';
import { A11yContext } from '../types';

// TS: context value will always be initialized by the provider so we don't need to here
export const a11yContext = createContext<A11yContext>({} as A11yContext);

export const useA11yContext = () => {
  return useContext(a11yContext);
};

class A11yGlobalContext {
  public value = {} as A11yContext;
}

export const a11yGlobalContext = new A11yGlobalContext();
