import { a11yGlobalContext } from './a11y-context';

export const a11yNotificationEvent = async () => {
  await a11yGlobalContext.value.clear();
};
