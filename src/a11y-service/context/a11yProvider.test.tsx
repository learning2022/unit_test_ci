import { a11yGlobalContext } from '../context/a11y-context';
import { A11yContextProvider } from './a11y-provider';
import { quickFixer } from '../../loree-editor/a11y-client/quick-fixer';
import React from 'react';
import getTemplate from '../../loree-editor/modules/header/template';
import { templateConfig } from '../../loree-editor/modules/header/templateMockData';
import { render, fireEvent, screen } from '@testing-library/react';
import CONSTANTS from '../../loree-editor/constant';
import a11yService from '../service/a11y-service';
import { Button } from 'react-bootstrap';
import { a11yNotificationService, A11yEventType } from '../../views/editor/observerService';
import { a11yNotificationEvent } from './a11y-tools';
import { getEditorElementById } from '../../loree-editor/utils';

describe('Get Templates with Accessibility checker', () => {
  beforeAll(() => {
    document.body.innerHTML = ``;
  });
  test('Getting the Templates with accessibility', () => {
    const getTemplateSection = getTemplate(templateConfig);
    document.body.append(getTemplateSection);
    expect(getEditorElementById(CONSTANTS.LOREE_A11Y_NOTIFICATION_COUNT)).toBeTruthy();
    expect(getEditorElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON)).toBeDisabled();
  });
});
describe('handling notification function', () => {
  beforeEach(() => {
    const getTemplateSection = getTemplate(templateConfig);
    document.body.append(getTemplateSection);
    const a11yStatus = {
      a11yIssueCount: 0,
      eventType: A11yEventType.open,
    };
    a11yNotificationService.sendMessage(a11yStatus);
  });
  test('Hide notification', () => {
    let messageContent;
    let displayStatus;
    a11yNotificationService.createObserver().subscribe((message: _Any) => {
      messageContent = message.a11yIssueCount;
      displayStatus = message.eventType;
    });
    a11yNotificationService.sendMessage({
      a11yIssueCount: 0,
      eventType: A11yEventType.open,
    });
    expect(messageContent).toBe(0);
    expect(displayStatus).toBe('rulesExecuted');
  });
});
describe('A11y checker providers', () => {
  const a11yNotificationServiceSendMessage = jest.spyOn(a11yNotificationService, 'sendMessage');
  // const i18nFetchingData = jest.spyOn(i18n, 'changeLanguage');

  beforeEach(() => {
    document.body.innerHTML = '';
    const getTemplateSection = getTemplate(templateConfig);
    document.body.append(getTemplateSection);
    render(<A11yContextProvider quickFixer={quickFixer} />);
    a11yGlobalContext.value.isProcessing = false;
    a11yGlobalContext.value.setContext({
      include: ['#loree-iframe', '#loree-iframe-content-wrapper'],
    });
    // Mock the api data
    const serviceCheckAllMockData = {
      steps: [
        {
          checkDetails: {
            all: [{ id: '', impact: '', message: '', data: '' }],
            any: [{ id: '', impact: '', message: '', data: '' }],
            none: [{ id: '', impact: '', message: '', data: '' }],
          },
          description:
            'Ensure that each non-empty data cell in a large table has one or more table headers',
          details: ['Some non-empty data cells do not have table headers'],
          docsUrl: 'https://dequeuniversity.com/rules/axe/4.3/td-has-header?application=axeAPI',
          element: `<table class="loree-iframe-content-table loree-style-ryN5Rg" style="width: 100%; height: aut…der: 1px solid #a5a5a5;" width="100%">`,
          fixedSinceLastReport: false,
          impact: 'critical',
          key: "td-has-header:/iframe[@id='loree-iframe'],/div[@id='loree-iframe-content-wrapper']/div/div/div/table",
          ruleId: 'td-has-header',
          tags: ['cat.tables', 'experimental', 'wcag2a'],
          title:
            'All non-empty td element in table larger than 3 by 3 must have an associated table header',
        },
      ],
      stepsByImpact: '',
    };
    a11yService.checkAll = jest.fn().mockImplementation(() => serviceCheckAllMockData);
    const { setActiveStep } = a11yGlobalContext.value;
    setActiveStep(0);
  });
  test('Hiding notification count section while opening A11Y tray', async () => {
    const { check, setNotificationFlag } = a11yGlobalContext.value;
    setNotificationFlag(false);
    const openButton = document.createElement('button');
    openButton.innerHTML = 'Open Accessibility';
    openButton.onclick = async () => {
      await check(true);
    };
    document.body.append(openButton);
    fireEvent.click(screen.getByText('Open Accessibility'));
    await screen.findByText('Open Accessibility');
    expect(a11yNotificationServiceSendMessage).toHaveBeenCalled();
  });
  test('Showing notification count section when notification flag as true', async () => {
    a11yGlobalContext.value.setNotificationFlag(true);
    const { check, setNotificationFlag } = a11yGlobalContext.value;
    setNotificationFlag(true);
    render(
      <Button
        variant='outline-primary'
        className='modal-footer-button my-2'
        onClick={async () => await check(true)}
      >
        Save to LMS
      </Button>,
    );
    fireEvent.click(screen.getByText('Save to LMS'));
    await screen.findByText('Save to LMS');
    expect(a11yNotificationServiceSendMessage).toHaveBeenCalled();
  });
  test('Showing notification while closing the A11y tray and save to LMS option hit', async () => {
    const a11yButton = getEditorElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON);
    const { clear } = a11yGlobalContext.value;
    await clear();
    expect(a11yButton).toBeDisabled();
    expect(a11yNotificationServiceSendMessage).toHaveBeenCalled();
  });
});

describe('A11y checker providers for Empty values', () => {
  const a11yNotificationServiceSendMessage = jest.spyOn(a11yNotificationService, 'sendMessage');
  beforeEach(() => {
    jest.clearAllMocks();
    document.body.innerHTML = '';
    const getTemplateSection = getTemplate(templateConfig);
    document.body.append(getTemplateSection);
    render(<A11yContextProvider quickFixer={quickFixer} />);
    a11yGlobalContext.value.isProcessing = false;
    a11yGlobalContext.value.setContext({
      include: ['#loree-iframe', '#loree-iframe-content-wrapper'],
    });
    a11yService.checkAll = jest.fn().mockImplementation(() => {});
  });
  test('Not show notification while closing the A11y tray, if the count is zero', async () => {
    const { clear } = a11yGlobalContext.value;
    await clear();
    expect(a11yNotificationServiceSendMessage).toHaveBeenCalledTimes(1);
    expect(a11yNotificationServiceSendMessage).toBeCalledWith({
      a11yIssueCount: 0,
      eventType: A11yEventType.open,
    });
  });
  test('A11y at opening a page', async () => {
    await a11yNotificationEvent();
    expect(a11yNotificationServiceSendMessage).toHaveBeenCalled();
  });
});
