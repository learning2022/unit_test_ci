import React, { ReactNode, useState, useMemo } from 'react';
import { ElementContext } from 'axe-core';

import { Steps, QuickFixer, A11yContext, StepsByImpact, HighlightsOffset } from '../types';
import { A11yElementHighlighter } from '../components/A11yElementHighlighter';
import { wait } from '../utils/wait';
import a11yService from '../service/a11y-service';
import { newStepsByImpact } from '../utils/new-steps-by-impact';

import { a11yContext, a11yGlobalContext } from './a11y-context';
import { a11yNotificationService, A11yEventType } from '../../views/editor/observerService';
import {
  getEditorElementById,
  removeClassToElement,
  addClassToElement,
} from '../../loree-editor/utils';
import CONSTANTS from '../../loree-editor/constant';

export type A11yContextProviderProps = {
  children?: ReactNode;
  /**
   * If true, then the <A11yElementHighlighter /> will automatically be
   * included just after the `children`. This is not recommended for
   * performance reasons. Rather include the <A11yElementHighlighter />
   * yourself in the same scrollable and positioned container as the content
   * you will be checking.
   */
  includeElementHighlighter?: boolean;
  quickFixer?: QuickFixer;
};

export function A11yContextProvider({
  children,
  includeElementHighlighter = false,
  quickFixer,
}: A11yContextProviderProps) {
  const [context, setContext] = useState<ElementContext | null>(null);
  const [steps, setSteps] = useState<Steps>([]);
  const [stepsByImpact, setStepsByImpact] = useState<StepsByImpact>(newStepsByImpact());
  const [activeStep, setActiveStep] = useState(-1);
  const [activeElem, setActiveElem] = useState<HTMLElement | null>(null);
  const [isProcessing, setIsProcessing] = useState(false);
  const [isRechecking, setIsRechecking] = useState(false);
  const [highlightsIdentity, setHighlightsIdentity] = useState({});
  const [highlightsOffset, setHighlightsOffset] = useState<HighlightsOffset>([0, 0]);
  const [notificationFlag, setNotificationFlag] = useState(false);

  const sendMessage = (notificationCount: number, a11yDisplayStatus: A11yEventType) => {
    const a11yStatus = {
      a11yIssueCount: notificationCount,
      eventType: a11yDisplayStatus,
    };
    a11yNotificationService.sendMessage(a11yStatus);
  };

  // Memoize context value so we avoid unnecessary re-renders
  const contextValue = useMemo((): A11yContext => {
    const check = async (recheck = false) => {
      if (!context || isProcessing) {
        return;
      }
      const accessibilityCloseButton = getEditorElementById(CONSTANTS.LOREE_A11Y_TRAY_CLOSE_BUTTON);
      addClassToElement(accessibilityCloseButton, 'd-none');

      if (!notificationFlag) {
        setIsProcessing(true);
        setIsRechecking(recheck);
        // Access when open
        sendMessage(0, A11yEventType.open);
      }

      // delay execution to allow UI to settle and improve performance
      await wait(0);

      const { steps, stepsByImpact } = await a11yService.checkAll(context, {}, recheck);
      if (notificationFlag && steps?.length > 0) {
        sendMessage(steps?.length, A11yEventType.close);
      }
      if (!notificationFlag) {
        setIsProcessing(false);
        setIsRechecking(false);
        setSteps(steps);
        setStepsByImpact(stepsByImpact);

        // If the user is not "rechecking" the page, or if they are rechecking but the step they were
        // looking at is now out of bounds we reset the active step.
        if (!recheck || !steps[activeStep]) {
          setActiveStep(0);
        }
      }
      removeClassToElement(accessibilityCloseButton, 'd-none');
    };

    const clear = async () => {
      const a11yButton = getEditorElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON);
      a11yButton?.setAttribute('disabled', 'true');
      if (!context) {
        return;
      }
      setSteps([]);
      setStepsByImpact(newStepsByImpact());
      setActiveStep(-1);
      const checker = await a11yService.checkAll(context, {}, false);
      if (checker?.steps?.length > 0) {
        sendMessage(checker?.steps?.length, A11yEventType.close);
      } else {
        setNotificationFlag(false);
        sendMessage(0, A11yEventType.open);
      }
    };

    const newValue: A11yContext = {
      activeElem,
      activeStep,
      check,
      clear,
      highlightsOffset,
      highlightsIdentity,
      isProcessing,
      isRechecking,
      quickFixer,
      setActiveElem,
      setActiveStep,
      setContext,
      setHighlightsOffset,
      setSteps,
      setNotificationFlag,
      steps,
      stepsByImpact,
      updateHighlights: () => setHighlightsIdentity({}),
    };

    a11yGlobalContext.value = newValue;

    return newValue;
  }, [
    activeElem,
    activeStep,
    context,
    highlightsIdentity,
    highlightsOffset,
    notificationFlag,
    isProcessing,
    isRechecking,
    quickFixer,
    steps,
    stepsByImpact,
  ]);

  return (
    <a11yContext.Provider value={contextValue}>
      {children}
      {includeElementHighlighter && <A11yElementHighlighter positionStrategy='fixed' />}
    </a11yContext.Provider>
  );
}
