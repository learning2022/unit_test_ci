import { ImpactValue, ElementContext, CheckResult, AxeResults } from 'axe-core';
import { ReactNode, Dispatch, SetStateAction } from 'react';

export type HighlightsOffset = [number | null, number | null];

export type StepImpact = NonNullable<ImpactValue>;

export type Step<Impact extends StepImpact = StepImpact> = {
  /** @see NodeResult */
  checkDetails?: {
    all: CheckResult[];
    any: CheckResult[];
    none: CheckResult[];
  };
  /** Description of issue */
  description: string;
  /** Details for each rule the element has failed */
  details?: string[];
  /** URL for full rule explanation */
  docsUrl: string;
  /** The HTML Element where the issue is occurring */
  element?: HTMLElement;
  /** If this step was fixed since the last time the report was generated with
   * a11yService.checkAll and `recheck` is true */
  fixedSinceLastReport: boolean;
  /** The severity of the issue */
  impact: Impact;
  /** Unique key derived from the ruleId and XPath for the `element` */
  key: string;
  /** The Axe rule */
  ruleId: string;
  /** @see https://github.com/dequelabs/axe-core/blob/develop/doc/API.md#axe-core-tags */
  tags: string[];
  /** The title of the issue
   *  @see Result.help */
  title: string;
};

export type Steps = Step[];

/** The steps grouped by impact level */
export type StepsByImpact = {
  [Impact in StepImpact]: Step<Impact>[];
};

/** The steps grouped by impact level */
export type StepsByKey = {
  [p: string]: Step;
};

export type CheckReport = {
  results: AxeResults;
  steps: Step[];
  stepsByImpact: StepsByImpact;
  stepsByKey: StepsByKey;
};

export type QuickFixer = (arg: { step: Step; activeStep: number }) => ReactNode;

export type A11yContext = {
  /** The current highlighted element corresponding to the active step */
  activeElem: HTMLElement | null;
  /** The index of the active step */
  activeStep: number;
  /** Method used to run the checker */
  check: (recheck?: boolean) => Promise<void>;
  /** Clear the checker results and steps */
  clear: () => void;
  /** When modified, the highlights will re-render */
  highlightsIdentity: {};
  /** Distance in pixels in the x and y direction to offset the highlights */
  highlightsOffset?: HighlightsOffset;
  /** True when the checker is actively processing the content */
  isProcessing: boolean;

  isRechecking?: boolean;
  /** The quick fixer function that will be called with the current `Step` */
  quickFixer?: QuickFixer;
  /** Sets the `activeElem` */
  setActiveElem: Dispatch<SetStateAction<HTMLElement | null>>;
  /** Sets the `activeStep` */
  setActiveStep: Dispatch<SetStateAction<number>>;
  /** Sets the `container`.
   * @example
   * <div ref={(elem) => setContext(elem)}> */
  setContext: Dispatch<SetStateAction<ElementContext | null>>;
  /** Sets the `activeStep` */
  setHighlightsOffset: Dispatch<SetStateAction<HighlightsOffset>>;
  setSteps: Dispatch<SetStateAction<Steps>>;
  /** The list of steps for each a11y issue */
  steps: Steps;
  /** The steps grouped by impact level */
  stepsByImpact: StepsByImpact;
  /** Method to trigger the issue highlights to re-render when the content is changed */
  updateHighlights: () => void;

  setNotificationFlag: Dispatch<SetStateAction<boolean>>;
};
