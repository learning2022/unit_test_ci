import axeGlobal, { RunOptions, Rule, CheckAugmented, ElementContext } from 'axe-core';
import { Steps, Step, CheckReport, StepsByKey } from '../types';
import { sortByDocumentOrder } from '../utils/sort-by-document-order';
import { newStepsByImpact } from '../utils/new-steps-by-impact';
import { CustomRule } from '../../loree-editor/a11y-client/types';

/**
 * The main accessibility service responsible for running Axe as well as registering rules/checks
 */
export class A11yService {
  readonly experimentalRules = {
    'css-orientation-lock': { enabled: true },
    'focus-order-semantics': { enabled: true },
    'hidden-content': { enabled: true },
    'label-content-name-mismatch': { enabled: true },
    'link-in-text-block': { enabled: true },
    'no-autoplay-audio': { enabled: true },
    'p-as-heading': { enabled: true },
    'table-fake-caption': { enabled: true },
    'td-has-header': { enabled: true },
    'input-image-alt': { enabled: true },
  };

  private readonly customRulesMap = new Map<string, Rule>();
  private readonly customChecksMap = new Map<string, CheckAugmented>();

  private _lastSteps?: Step[];

  private configure(axe = axeGlobal) {
    axe.configure({
      rules: Array.from(this.customRulesMap.values()),
      checks: Array.from(this.customChecksMap.values()),
    });
  }

  get lastSteps() {
    return this._lastSteps;
  }

  /**
   * Register an Axe rule.
   * @see https://github.com/dequelabs/axe-core/blob/develop/doc/rule-development.md
   * @param rule - The rule definition
   * @param axe - Optional axe global to include. Necessary when registering rules inside iframes
   */
  registerRule(rule: Rule, axe = axeGlobal) {
    this.customRulesMap.set(rule.id, rule);
    this.configure(axe);
  }

  /**
   * Register an Axe check.
   * @see https://github.com/dequelabs/axe-core/blob/develop/doc/rule-development.md
   * @param check - The check definition
   * @param axe - Optional axe global to include. Necessary when registering checks inside iframes
   */
  registerCheck(check: CheckAugmented, axe = axeGlobal) {
    this.customChecksMap.set(check.id, check);
    this.configure(axe);
  }

  /**
   * Unregister an Axe rule.
   * @param id - The rule id
   * @param axe - Optional axe global to include. Necessary when registering rules inside iframes
   */
  unregisterRule(id: string, axe = axeGlobal) {
    this.customRulesMap.delete(id);
    this.configure(axe);
  }

  /**
   * Unregister an Axe check.
   * @param id - The check id
   * @param axe - Optional axe global to include. Necessary when registering rules inside iframes
   */
  unregisterCheck(id: string, axe = axeGlobal) {
    this.customChecksMap.delete(id);
    this.configure(axe);
  }

  /**
   * Wrapper around `axe.run` that will generate a report and parse it out into `CheckReport`
   * @param context - The Axe context - See: {@link ElementContext}
   * @param [options] - Any additional options to pass to `axe.run`
   * @param [recheck] - If `true`, any steps missing from the report, but were in the previous
   *   report will be included with `fixedSinceLastReport` set to `true`
   */
  async checkAll(
    context: ElementContext,
    options?: RunOptions,
    recheck = false,
  ): Promise<CheckReport> {
    const results = await axeGlobal.run(context, {
      reporter: 'v2',
      selectors: false,
      elementRef: true,
      resultTypes: ['violations', 'incomplete'],
      rules: this.experimentalRules,
      xpath: true,
      ancestry: true,
      ...options,
    });

    const steps: Steps = [];
    const stepsByImpact = newStepsByImpact();
    const stepsByKey: StepsByKey = {};

    // Here we generate the steps by iterating over the results violations
    if (results.violations.length) {
      const customRulesId = Object.values(CustomRule);
      for (const violation of results.violations) {
        for (const node of violation.nodes) {
          // We have to assert the type here otherwise `step` gets reduced to `never`
          const impact = (node.impact ?? 'minor') as 'minor';
          // When dealing with iframes, the element will be undefined so we need to manually find it
          // using the `target` property
          const element = node.element ?? findElement(node.target);
          const step: Step<typeof impact> = {
            checkDetails: { all: node.all, any: node.any, none: node.none },
            description: violation.description,
            details: [...node.all, ...node.any, ...node.none]
              .map((r) => r.message)
              .filter((x) => Boolean(x)),
            docsUrl: !customRulesId.find((str) => str === violation.id) ? violation.helpUrl : '',
            element: element ?? undefined,
            fixedSinceLastReport: false,
            impact: impact,
            key: `${violation.id}:${node.xpath?.join(',')}`,
            ruleId: violation.id,
            tags: violation.tags,
            title: violation.help,
          };
          steps.push(step);
          stepsByImpact[step.impact].push(step);
          stepsByKey[step.key] = step;
        }
      }
    }

    // If recheck is false, save the steps. We make a shallow copy to avoid manipulating it further
    if (!recheck) {
      this._lastSteps = [...steps];
    }

    for (const step of this._lastSteps ?? []) {
      if (!stepsByKey[step.key]) {
        steps.push({ ...step, fixedSinceLastReport: true });
      }
    }

    return {
      results,
      steps: sortByDocumentOrder(steps),
      stepsByImpact,
      stepsByKey,
    };
  }
}

/**
 * Singleton of {@link A11yService}
 */
const a11yService = new A11yService();
export default a11yService;

/**
 * Locates the relevant element using {@link axeGlobal.NodeResult.target}
 */
function findElement(selectorPath: string[]) {
  let doc: Document = document;
  let result: HTMLElement | null = null;

  for (const selector of selectorPath) {
    const elem: HTMLIFrameElement | HTMLElement | null = doc.querySelector(selector);
    if (elem && 'contentDocument' in elem && elem.contentDocument) {
      doc = elem.contentDocument;
    } else {
      result = elem;
    }
  }

  return result;
}
