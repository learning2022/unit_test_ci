import fs from 'fs';

import React from 'react';
import { render, screen } from '@testing-library/react';

import { A11yExamples, A11yExamplesIFrame } from '../__fixtures__/a11y-examples';

import a11yService from './a11y-service';

describe('checkAll', () => {
  test.skip('Creates the steps', async () => {
    const { container } = render(<A11yExamples />);
    const { steps } = await a11yService.checkAll(container);
    expect(steps).toHaveLength(4);
    expect(steps).toMatchSnapshot();
  });

  test('Orders the steps by document order', async () => {
    const { container } = render(<A11yExamples examples={['link-name', 'image-alt']} />);
    const { steps } = await a11yService.checkAll(container);

    // Axe usually orders things by rule ID alphabetically
    expect(steps[0]?.ruleId).toBe('link-name');
    expect(steps[1]?.ruleId).toBe('image-alt');
  });

  test.skip('Groups the steps by impact', async () => {
    const { container } = render(<A11yExamples />);
    const { stepsByImpact } = await a11yService.checkAll(container);
    expect(stepsByImpact).toMatchObject({
      minor: [],
      critical: [{ ruleId: 'image-alt' }],
      moderate: [{ ruleId: 'no-strike' }],
      serious: [{ ruleId: 'link-name' }, { ruleId: 'p-as-heading' }],
    });
  });

  test('Groups the steps by key', async () => {
    const { container } = render(<A11yExamples />);
    const { stepsByKey } = await a11yService.checkAll(container);
    expect(stepsByKey).toMatchObject({
      "image-alt:/div[@id='example-content']/div/img": { ruleId: 'image-alt' },
      "link-name:/div[@id='example-content']/div[2]/a": { ruleId: 'link-name' },
    });
  });

  test('Sets the fixedSinceLastReport property', async () => {
    const { container } = render(<A11yExamples examples={['link-name', 'image-alt']} />);
    const { steps } = await a11yService.checkAll(container);
    expect(steps[0]?.fixedSinceLastReport).toBe(false);
    expect(steps[1]?.fixedSinceLastReport).toBe(false);

    // now fix the issue
    const img = screen.getByTestId('image-alt');
    img.setAttribute('alt', 'fixed!');
    const { steps: steps2 } = await a11yService.checkAll(container, {}, true);
    expect(steps2[0]?.fixedSinceLastReport).toBe(false);
    expect(steps2[1]?.fixedSinceLastReport).toBe(true);
  });

  // todo: Figure out how to test axe through iframes
  test.skip('Finds the elements when inside an iframe', async () => {
    render(<A11yExamplesIFrame />);

    const iframe = screen.getByTestId('A11yExamplesIFrame') as HTMLIFrameElement;
    const iframeDocument = iframe.contentDocument!;
    const div = document.createElement('div');
    render(<A11yExamples />, { container: iframeDocument.body.appendChild(div) });

    const axeScript = document.createElement('script');
    axeScript.type = 'text/javascript';
    axeScript.text = fs.readFileSync(require.resolve('axe-core/axe.min.js')).toString();
    iframeDocument.body.appendChild(axeScript);

    // console.log(iframe.contentWindow.axe);
    // screen.debug();

    const { steps, results } = await a11yService.checkAll({
      include: [[`[data-testid='A11yExamplesIFrame']`, `[data-testid='A11yExamples']`]],
    });
    console.log(results);
    expect(steps).toHaveLength(4);
  });
});
