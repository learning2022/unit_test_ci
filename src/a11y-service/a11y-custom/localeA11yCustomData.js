export const LocaleA11yCustomData = {
  scrollableregionfocusable: {
    customDescription: {
      description:
        'Elements with scrollable content must be accessible by keyboards. This is particularly important for keyboardonly users who might choose to focus on a scrollable region or static text item within a scrollable region. ',
    },
  },
  skiplink: {
    customDescription: {
      description:
        'A page must have a skiplink target before the navigation. This allows users to skip lengthy navigation elements and move to page content quickly.',
    },
  },
  ariaroledescription: {
    customDescription: {
      description:
        'The attribute ariaroledescription  should only be used on elements with implicit or explicit role values. Conflicting attribute values may disable accessibility functions and features.  ',
    },
  },
  imageredundantalt: {
    customDescription: {
      description:
        'When button and link text repeats in an alt attribute value, screen reader users will hear the information twice. This makes the alt text confusing and often meaningless.',
    },
  },
  tableduplicatename: {
    customDescription: {
      description:
        'Caption and summary table attributes must not be identical. Tables must be marked up accurately for screen reader features to work correctly.',
    },
  },
  autocompletevalid: {
    customDescription: {
      description:
        'The autocomplete attribute values must be valid and applied correctly. The purpose for each common input field is programmatically defined and based on a list of 53 Input Purposes for User Interface Components. This is important for screen readers to function correctly.',
    },
  },
  roleimgalt: {
    customDescription: {
      description:
        'Elements marked role="img" must have alternate text. Screen readers cannot convert image information to text or speech in the absence of accessible text alternatives associated with the images.',
    },
  },
  areaalt: {
    customDescription: {
      description:
        'An image map must include alternate text for the image and for each clickable area. This enables screen readers to translate and announce accurately to users.',
    },
  },
  blink: {
    customDescription: {
      description:
        'Blinking text is not supported by modern browsers. It is distracting and difficult to read particularly for users with visual, mobility and cognitive impairment. Consider other accessible methods to highlight important text.',
    },
  },
  definitionlist: {
    customDescription: {
      description:
        'Definition lists (dl) must contain correctly ordered dt and dd groups, script or template elements. Screen readers use specific conventions to read words or phrases in lists. Definition lists must be ordered correctly to ensure screen reader outputs are accurate and easily understood.',
    },
  },
  dlitem: {
    customDescription: {
      description:
        'Definition list items (dt and/or dd) must be wrapped in parent dl elements to be valid. A definition list must follow a sequential hierarchy using dl to define the list, dt to define the term, and dd to present the term’s description. This convention ensures screen reader outputs are accurate and easily understood.',
    },
  },
  framefocusablecontent: {
    customDescription: {
      description:
        'Elements %lt;frame%gt; and %lt;iframe%gt; with focusable content must not have tabindex="1". If a frame has a negative tabindex, it will prevent the browser from redirecting the focus to the content inside the frame. This will cause content skipping when navigating using a keyboard.',
    },
  },
  frametitleunique: {
    customDescription: {
      description:
        'All frame or iframe elements must have a unique title. Screen readers rely on frame titles to describe the content of the frame. Users will find it difficult to navigate if frames are not marked with a unique title attribute.',
    },
  },
  frametested: {
    customDescription: {
      description:
        'Frames must contain the axecore script. Without this, it is impossible for the tool to undertake violation checking on multiple levels of nested iframes.',
    },
  },
  frametitle: {
    customDescription: {
      description:
        'All frame or iframe elements must have a title and should never be empty. Screen readers rely on frame titles to describe the content of the frame. Users will find it difficult to navigate if frames are not marked with a unique title attribute.',
    },
  },
  imagealt: {
    customDescription: {
      description:
        'All images must have short, descriptive alternate text.  Screen readers cannot translate images of text into audible output. Alternate text provides screen reader users with meaning about the image’s content and purpose. ',
    },
  },
  inputimagealt: {
    customDescription: {
      description:
        'All image buttons must include alternate text. Text should be clear and concise and representative of the action performed when activated. An image containing only text also requires alternate text since a screen reader cannot translate images of text into audible output.',
    },
  },
  listitem: {
    customDescription: {
      description:
        'All list items (li) must be contained within ul or ol parent elements. This convention enables screen readers to accurately notify users of a list and the number of items presented in the list.',
    },
  },
  marquee: {
    customDescription: {
      description:
        'All <marquee> elements are deprecated and must not be used. They interfere with assistive technologies, increase difficulty and are distracting.',
    },
  },
  metarefresh: {
    customDescription: {
      description:
        'A page should not use <meta httpequiv="refresh"> . It prevents users controlling when refresh occurs. Such resetting through refresh is frustrating for users.',
    },
  },
  metaviewportlarge: {
    customDescription: {
      description:
        'A page must not use the userscalable="no" parameter in the <meta name="viewport"> element. It disables text scaling and zooming which is essential to users with low vision.',
    },
  },
  metaviewport: {
    customDescription: {
      description:
        'The page must not use the userscalable="no" parameter in the <meta name="viewport"> element. It disables text scaling and zooming which is essential to users with low vision.',
    },
  },
  objectalt: {
    customDescription: {
      description:
        'All embedded objects must include text alternatives. This allows screen readers to translate nontext content, such as images, into audible descriptions to screen reader users.',
    },
  },
  videocaption: {
    customDescription: {
      description:
        'An HTML5 video element must include a track element with kind="captions" set as a property. Captions must convey all meaningful auditory information in the video including dialogue, musical cues, sound effects, and other relevant and contextual information to enhance user accessibility.',
    },
  },
  ariavalidattrvalue: {
    customDescription: {
      description:
        'ARIA attributes beginning with aria- must contain valid values. User interface elements must conform to correctly spelled and valid ARIA attributes to allow assistive technologies to perform correctly.',
    },
  },
  landmarkonemain: {
    customDescription: {
      description:
        'Only one main landmark should be used to navigate to the primary content on the page.  If a page contains iframe elements, it should contain no landmarks or only one single landmark. Navigating a page is simpler for screen reader users if the content splits between one or more high-level sections. Content outside of these sections is difficult to locate.',
    },
  },
  ariaallowedattr: {
    customDescription: {
      description:
        'Elements must only use allowed ARIA attributes. ARIA dialog elements must have discernible text that clearly describe the destination, purpose, function, or action. This rule checks that each role is supplied with allowable attributes so that no conflicts with assistive technologies occur.',
    },
  },
  ariahiddenfocus: {
    customDescription: {
      description:
        'Aria-hidden elements must not contain focusable elements. This makes the element inaccessible by assistive technologies such as screen readers.   ',
    },
  },
  ariahiddenbody: {
    customDescription: {
      description:
        'Page content is not accessible to assistive technology if <body ariahidden="true">. Screen readers do not read content marked using this convention. Reconsider the location of content or interface element that should be hidden intentionally from all users – i.e., other than a body element.',
    },
  },
  ariavalidattr: {
    customDescription: {
      description:
        'ARIA attributes beginning with aria must use valid names. User interface elements must conform to correctly spelled and valid ARIA attributes to allow assistive technologies to perform correctly.',
    },
  },
  documenttitle: {
    customDescription: {
      description:
        'HTML pages must include a title element. The title should be short, descriptive text summarizing the contents of the page. Navigating pages becomes difficult for screen reader users when pages are not marked with titles.',
    },
  },
  bypass: {
    customDescription: {
      description:
        'Each page must have a main landmark to provide a mechanism to bypass repeated blocks of content or interface elements (like header and navigation) and locate the main content. Keyboard-only users benefit from direct access to the primary content on a page. ',
    },
  },
  ariarequiredchildren: {
    customDescription: {
      description:
        'Certain ARIA parent role values applied to elements must contain specific child elements and role values.  ARIA roles with missing child roles will not enable assistive technologies to function. ',
    },
  },
  ariarequiredattr: {
    customDescription: {
      description:
        'ARIA widget roles must have appropriate attributes describing the state or properties of the widget. The properties of a widget will not be announced to screen reader users if a required attribute is missing.',
    },
  },
  accesskeys: {
    customDescription: {
      description:
        'All accesskey attribute values must be unique. Applying accesskeys allows users to activate specific elements in shortcut form. Ensure values are unique to avoid conflicts with browser, keyboard, and screen reader shortcuts.',
    },
  },
  ariacommandname: {
    customDescription: {
      description:
        'ARIA command elements must have discernible text. The text must clearly describe the destination, purpose, function, or action. Screen readers will not discern elements without accessible names.',
    },
  },
  ariadialogname: {
    customDescription: {
      description:
        'ARIA dialog elements must have discernible text.  The text must clearly describe the destination, purpose, function, or action. Screen readers will not discern elements without accessible names.',
    },
  },
  ariainputfieldname: {
    customDescription: {
      description:
        'ARIA input fields must have an accessible name. Screen readers will not discern elements without accessible names.',
    },
  },
  ariametername: {
    customDescription: {
      description:
        'Aria meter elements must have discernible text. The text must clearly describe the destination, purpose, function, or action.  Screen readers will not discern elements without accessible names.',
    },
  },
  ariaprogressbarname: {
    customDescription: {
      description:
        'Aria progressbar elements must have discernible text. The text must clearly describe the destination, purpose, function, or action. Screen readers will not discern elements without accessible names.',
    },
  },
  ariatogglefieldname: {
    customDescription: {
      description:
        'Every ARIA toggle field must have an accessible name. Screen readers will not discern elements without accessible names.',
    },
  },
  ariatooltipname: {
    customDescription: {
      description:
        'Aria tooltip elements must have discernible text. The text must clearly describe the destination, purpose, function, or action. Screen readers will not discern the purpose of the elements that do not have accessible names.',
    },
  },
  ariatreeitemname: {
    customDescription: {
      description:
        'ARIA treeitem elements must have discernible text. The text must clearly describe the destination, purpose, function, or action.  Screen readers cannot discern the purpose of elements that do not have accessible names.',
    },
  },
  label: {
    customDescription: {
      description:
        'Each form element must have a programmatically associated label element to make forms accessible. Screen reader users need useful form labels to identify form fields. Adding a label to all elements eliminates any ambiguity.',
    },
  },
  htmlhaslang: {
    customDescription: {
      description:
        'A page element must contain a valid lang attribute or correspond to a valid lang code. It is important to specify the language used so that screen reader outputs pronounce the text correctly. This is important for multilingual screen reader users who access web content in multiple languages.',
    },
  },
  duplicateid: {
    customDescription: {
      description:
        'The value assigned to an ID attribute must be unique. It prevents the second instance from being missed by assistive technologies. ID attributes should not be used more than once on the same page so that each element can be differentiated. ',
    },
  },
  duplicateidactive: {
    customDescription: {
      description:
        'The value assigned to active ID attributes on focusable elements must be unique to prevent the second instance from being overlooked by assistive technologies. Focusable active elements require unique IDs for assistive technologies to distinguish one element from another.',
    },
  },
  duplicateidaria: {
    customDescription: {
      description:
        'The value assigned to an id attribute used in ARIA or in form labels must be unique. This prevents the second instance from being missed by assistive technologies. ID values used in ARIA and in labels should not be used more than once on the same page so that each element can be differentiated.',
    },
  },
  formfieldmultiplelabels: {
    customDescription: {
      description:
        'Form fields should not have multiple labels. Assigning multiple labels to the same form field causes problems for certain combinations of screen readers and browsers. ',
    },
  },
  emptyheading: {
    customDescription: {
      description:
        'All heading elements (marked by <h1> through <h6>) must include text or content. Screen readers alert users to a heading tag. Using heading elements for anything other than headings will make navigating a page confusing for users of assistive technologies.',
    },
  },
  inputbuttonname: {
    customDescription: {
      description:
        'Input buttons must have discernible text. The input-button-name rule separates functionality from the button-name rule to ensure input buttons have discernible text. Screen reader users will not understand the purpose of an image without a discernible and accessible textual name. ',
    },
  },
  validlang: {
    customDescription: {
      description:
        'A valid language must be specified to ensure text is pronounced correctly for screen reader users.',
    },
  },
  linkname: {
    customDescription: {
      description:
        'Links must have discernible text. When using link text and alternate text for images as links, make sure they are focusable and that no duplicate labels exist. This ensures that every link has an accessible name, so they are discernible by screen readers and accessible to keyboard-only users. ',
    },
  },
  ariaallowedrole: {
    customDescription: {
      description:
        'Values assigned to WAI-ARIA role attributes must be valid. Values must be spelled correctly, correspond to existing ARIA role values, and must not be abstract to correctly expose the purpose of the element. Screen readers will be impacted if the role of each element on the page is not known. ',
    },
  },
  selectname: {
    customDescription: {
      description:
        'Each select element must include a programmatically associated label element. Screen reader users require useful form labels to identify form fields accurately.',
    },
  },
  svgimgalt: {
    customDescription: {
      description:
        'SVG elements with an <img>, graphics-document or graphics-symbol role must include an accessible text alternative. Text alternatives are a primary way to make information accessible to users.',
    },
  },
  tabindex: {
    customDescription: {
      description:
        'A tabindex attribute must never have a value greater than 0 to prevent unexpected tab ordesr. This often makes the page less intuitive and gives the appearance of skipping page elements.',
    },
  },
  emptytableheader: {
    customDescription: {
      description:
        'Table header elements must have visible text. If an element is not a header, mark it up with a `td’. Visible text describing the purpose of the row makes the table more accessible.',
    },
  },
  labeltitleonly: {
    customDescription: {
      description:
        'Form <input> elements can be given a title using title or aria-describedby attributes but not both. These attributes are used to provide additional information such as hints. Hints are exposed differently to labels and can cause problems with assistive technology functions and features.',
    },
  },
  list: {
    customDescription: {
      description:
        'Ordered and unordered lists must be structured correctly. They must only contain li elements. This allows screen readers to announce lists and the items listed within lists correctly to screen reader users.',
    },
  },
  serversideimagemap: {
    customDescription: {
      description:
        'Server-side instead of client-side image maps should be used. Server-side image maps are not accessible to users who only use keyboards for page navigation.',
    },
  },
  landmarkcomplementaryistoplevel: {
    customDescription: {
      description:
        'Complementary landmarks or asides must only be used at top level. Never include aside elements or elements containing role="complementary" within other content marked up as a landmark. Nesting landmarks create confusing page structures for screen reader users.',
    },
  },
  landmarkcontentinfoistoplevel: {
    customDescription: {
      description:
        'Contentinfo landmarks must be at top level and never contained in any other landmark. The purpose of this landmark is impacted when placed within another. It prevents screen reader users from navigating to appropriate landmarks.',
    },
  },
  colorcontrast: {
    customDescription: {
      description:
        'All text elements must have sufficient contrast between foreground and background colours in accordance with WCAG 2 AA contrast ratio thresholds. Some users experience low contrast and text that is too close in brightness to the background colour can be difficult to read.',
    },
  },
  landmarknoduplicatemain: {
    customDescription: {
      description:
        'Only one main landmark should be used to navigate to the primary content of a page. If a page contains iframe elements, each element should either contain no landmarks or one single landmark. Landmarks provide a simple replacement for a skip navigation link for screen reader users.',
    },
  },
  htmllangvalid: {
    customDescription: {
      description:
        'A HTML page must contain a valid lang attribute or correspond to a valid lang code. This is important for multilingual screen reader users who may prefer a language other than the default. ',
    },
  },
  landmarkmainistoplevel: {
    customDescription: {
      description:
        'The main landmark should not be contained within another landmark. All content should be contained within distinct regions such as the header (role="banner"), content (role="main") and footer (role="contentinfo"). Navigating a page is simpler for screen reader users if the content splits between high-level sections. Content outside of these sections is difficult to locate.',
    },
  },
  headingorder: {
    customDescription: {
      description:
        'Headings must be ordered logically and presented hierarchically  h1 through to h6. When applied correctly, heading elements help users navigate the structure and content of pages, making headings valuable tools for screen reader users.',
    },
  },
  landmarknoduplicatecontentinfo: {
    customDescription: {
      description:
        'There should be a maximum of one contentinfo landmark on a page. Landmarks allow assistive technology users to navigate and locate content quickly. Missing or multiple landmarks make navigating difficult for screen reader users.',
    },
  },
  scopeattrvalid: {
    customDescription: {
      description:
        'The scope attribute must be used correctly on tables in accordance with HTML4 or HTML5 specifications. This will allow for efficient table navigation for screen reader users.',
    },
  },
  presentationroleconflict: {
    customDescription: {
      description:
        'All elements with role=”none” or role=”presentation” should not have a global ARIA attribute and should not be focusable. This will ensure elements marked to be removed from the accessibility tree are in fact removed.',
    },
  },
  nestedinteractive: {
    customDescription: {
      description:
        'Interactive controls must not include focusable descendants. Focusable elements in an interactive control are not announced by screen readers and create an empty tab stop. ',
    },
  },
  pasheading: {
    customDescription: {
      description:
        'Styled p elements must not be used to represent headings. The structure of the page will not be interpreted accurately by screen readers.',
    },
  },
  Landmarkbanneristoplevel: {
    customDescription: {
      description:
        'A banner landmark must not be contained in another landmark. Screen reader users are prevented from navigating the page layout when a banner landmark is contained in another landmark.',
    },
  },
  ariatext: {
    customDescription: {
      description:
        'Elements with role="text" must not have focusable descendants. Assistive technologies may miscommunicate or create empty tab stops when focusable descendants are presented.',
    },
  },
  noautoplayaudio: {
    customDescription: {
      description:
        '<video> or <audio> elements should not autoplay for more than three seconds without a control mechanism to stop or mute the element. Playing video or audio automatically may affect the screen reader user’s experience particularly where both screen reader and video or audio elements play simultaneously.',
    },
  },
  ariaroles: {
    customDescription: {
      description:
        'Values assigned to ARIA role values must be valid. Role values must be spelled correctly, correspond to existing ARIA role values, and must not be abstract to correctly expose the purpose of the element. Screen readers will be impacted if the role of each element is not known.',
    },
  },
  region: {
    customDescription: {
      description:
        'With the exception for skip-links, contain all content within distinct regions (such as the header, nav, main, and footer). Navigation is simpler for screen readers when content is split between multiple high-level sections. Content outside of sections is typically difficult to locate.',
    },
  },
  buttonname: {
    customDescription: {
      description:
        'The input-button-name rule separates functionality from the button-name rule to ensure that input buttons have discernible text. Buttons must have discernible text that clearly describe the destination, purpose, function, or action. Screen readers cannot discern the purpose of elements that do not have accessible names.',
    },
  },
  cssorientationlock: {
    customDescription: {
      description:
        'Never lock device orientation (e.g., portrait or landscape). Assistive technology users may be unable to access the orientation features and functions if device orientation is locked.',
    },
  },
  thhasdatacells: {
    customDescription: {
      description:
        'Each header cell must be referenced as a header or row. It is essential that tables are marked up semantically using correct header structures for screen reader features to work effectively.',
    },
  },
  ariarequiredparent: {
    customDescription: {
      description:
        'Certain ARIA roles must be contained by parent roles. Elements containing ARIA role values with missing parent element role values will not enable assistive technologies to function.',
    },
  },
  htmlxmllangmismatch: {
    customDescription: {
      description:
        'A HTML page must contain a valid lang attribute or correspond to a valid lang code. The xml:lang attribute value, if included on a html element, must duplicate the value of the lang. This is important for multilingual screen reader users who may prefer a language other than the default.',
    },
  },
  labelcontentnamemismatch: {
    customDescription: {
      description:
        'Interactive elements labelled in their content must include visible labels as part of their accessible name. It is confusing for speech input users when accessible names do not match the visible labels.',
    },
  },
  landmarkunique: {
    customDescription: {
      description:
        'Landmarks must have a unique role or role/label/title (i.e., accessible name) combination.',
    },
  },
  linkintextblock: {
    customDescription: {
      description:
        'Links appearing in blocks of text must have a colour contrast difference of at least 3:1 with the surrounding text. This ensures users who cannot distinguish between colours can locate the link.',
    },
  },
  identicallinkssamepurpose: {
    customDescription: {
      description:
        'Links with the same accessible name must serve a similar purpose. This is an important rule that helps users understand the purpose of each link in content pages.',
    },
  },
  focusordersemantics: {
    customDescription: {
      description:
        'Role attribute values must be valid and appropriate in native HTML or custom ARIA widgets. If using a custom widget, appropriate ARIA role values must be used instead of abstract roles to correctly expose the purpose of the element. Roles are important in interactive elements so that screen readers can communicate the necessary information to its users.',
    },
  },
  tablefakecaption: {
    customDescription: {
      description:
        'Tables must use caption elements. The caption element for an onscreen title ensures screen reader users can make sense of tabular data.',
    },
  },
  avoidinlinespacing: {
    customDescription: {
      description:
        'Text spacing set through style attributes must also allow for adjustments with custom stylesheets. Many people with cognitive disabilities have trouble tracking lines of single-spaced text so providing spacing between 1.5 to 2 provides a more accessible experience.',
    },
  },
  tdheadersattr: {
    customDescription: {
      description:
        'A scope attribute must be included. It notifies screen readers that everything under the column is related to the header. It is essential that tables are marked up semantically using correct header structures for screen reader features to work effectively.',
    },
  },
  tdhasheader: {
    customDescription: {
      description:
        'Non-empty data cells in a large table must have one or more table headers. It is essential that tables are marked up semantically using correct header structures for screen reader features to work effectively.',
    },
  },
  landmarknoduplicatebanner: {
    customDescription: {
      description:
        'There should be a maximum of one banner landmark on a page. Landmarks allow assistive technology users to navigate and locate content quickly. Missing or multiple landmarks make navigating difficult for screen reader users.',
    },
  },
  pagehasheadingone: {
    customDescription: {
      description:
        'A page, or at least one of its frames, must contain a h1 heading. This allows screen reader users to use keyboard shortcuts to navigate the structure and sections of the page simply and efficiently.',
    },
  },
  hiddencontent: {
    customDescription: {
      description:
        'Hidden content cannot be analyzed for accessibility violations. Display must be triggered for accessibility analysis.',
    },
  },

  /** Custom rules */
  nostrike: {
    customDescription: {
      description:
        'Strike elements should not occur on a page. Strike elements are deprecated and inaccessible.',
    },
  },
  validtablecaption: {
    customDescription: {
      description: 'Caption elements must be added in the first child element of the table.',
    },
  },
  tableheaderidassociation: {
    customDescription: {
      description:
        'Associations must be valid and programmatically determined. A failure occurs when the relationship between data cells and corresponding header cells cannot be programmatically determined because the association of id and headers attributes is faulty. ',
    },
  },
  noimagetitle: {
    customDescription: {
      description: `'Title' attributes for image elements or image tags should not be included.  Title attributes are often not supported by assistive technologies. `,
    },
  },
  tableWhiteSpace: {
    customDescription: {
      description: `White space in the table cells aren't allowed.`,
    },
  },
  tableRolePresentation: {
    customDescription: {
      description: `Table element shouldn't contains the role='presentation' attribute. `,
    },
  },
  tableNecessaryLayoutTh: {
    customDescription: {
      description: `Table element should have table header(<th>). `,
    },
  },
  tableNecessaryLayoutSummary: {
    customDescription: {
      description: `Table element should contains a non-empty summary attribute. `,
    },
  },
};
