import React from 'react';
import { A11yChecker } from './A11yChecker';
import * as a11y from '../../a11y-service/context/a11y-context';
import { fireEvent, render, screen, configure } from '@testing-library/react';
import {
  mockUseA11yContext,
  a11yCheckerContainerMockData,
} from '../../loree-editor/a11y-client/quick-fixer/mockData';
import { createDiv } from '../../loree-editor/common/dom';
import { appendElementToBody } from '../../loree-editor/utils';
import CONSTANTS from '../../loree-editor/constant';

configure({ testIdAttribute: 'class' });

describe('#A11yChecker', () => {
  describe('#When content in the editor contains a issue', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      const mock = jest.spyOn(a11y, 'useA11yContext');
      mock.mockImplementation(() => a11yCheckerContainerMockData);
      render(<A11yChecker />);
    });

    test('should render the ac tray', () => {
      const acElement = screen.queryAllByTestId('a11yWholeMainSection');
      expect(acElement.length).toBe(1);
    });

    test('when triggering the recheck button shuld refresh the ac tray', () => {
      fireEvent.click(screen.getByText('Re-check'));
      expect(a11yCheckerContainerMockData.check).toBeCalledWith(true);
    });

    test('should render with the next button to navigate to the next issue', () => {
      const nextIcon = screen.queryAllByTestId('arrowIcon')[1] as HTMLButtonElement;
      fireEvent.click(nextIcon);
      expect(a11yCheckerContainerMockData.setActiveStep).toBeCalledWith(1);
    });

    test('should render with the previous button to navigate to the previous issue', () => {
      const previousIcon = screen.queryAllByTestId('arrowIcon')[0] as HTMLButtonElement;
      fireEvent.click(previousIcon);
      expect(a11yCheckerContainerMockData.setActiveStep).toBeCalledWith(2);
    });

    test('should render with the results when the selected content has issues', () => {
      const resultSextion = screen.queryAllByTestId('resultsHeading');
      expect(resultSextion.length).toBe(2);
    });
    test('success message should show nothing when issues are found', () => {
      const resultSextion = screen.queryAllByTestId('noIssueHeading');
      expect(resultSextion.length).toBe(0);
    });
  });

  describe('#When content in the editor contains a issue and fixed', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      const mock = jest.spyOn(a11y, 'useA11yContext');
      mock.mockImplementation(() => mockUseA11yContext);
      render(<A11yChecker />);
    });
    test('success message should show when all issues are fixed', () => {
      const element = screen.queryAllByTestId('noIssueHeading')[0] as HTMLHeadingElement;
      expect(element.textContent).toBe('Awesome!');
    });
  });

  describe('when ac tray fetching the issues', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      const mock = jest.spyOn(a11y, 'useA11yContext');
      a11yCheckerContainerMockData.steps = [];
      a11yCheckerContainerMockData.isProcessing = true;
      mock.mockImplementation(() => a11yCheckerContainerMockData);
      render(<A11yChecker />);
    });

    test('should show loader', () => {
      const element = screen.queryAllByTestId(
        'resultsHeading loadingHeaderSection',
      )[0] as HTMLHeadingElement;
      const closeButton = screen.queryAllByTestId('closeButton d-none')[0];
      expect(element.textContent).toBe('Finding Issues…');
      expect(closeButton).toBeDefined();
    });
  });

  describe('#When content in the editor contains a issue with triggering rechecking', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      const mock = jest.spyOn(a11y, 'useA11yContext');
      a11yCheckerContainerMockData.steps = [];
      a11yCheckerContainerMockData.isProcessing = true;
      a11yCheckerContainerMockData.isRechecking = true;
      mock.mockImplementation(() => a11yCheckerContainerMockData);
      render(<A11yChecker />);
    });
    test('should show rechecker', () => {
      const element = screen.queryAllByTestId(
        'resultsHeading loadingHeaderSection',
      )[0] as HTMLHeadingElement;
      const closeButton = screen.queryAllByTestId('closeButton d-none')[0];
      expect(element.textContent).toBe('Re-Checking Issues');
      expect(closeButton).toBeDefined();
    });
  });

  describe(`when the content in the editor contains no issue`, () => {
    beforeEach(() => {
      jest.clearAllMocks();
      const mock = jest.spyOn(a11y, 'useA11yContext');
      mockUseA11yContext.steps = [];
      mock.mockImplementation(() => mockUseA11yContext);
      render(<A11yChecker />);
    });

    test('should show no results found', () => {
      const element = screen.queryAllByTestId('noIssueHeading')[0] as HTMLHeadingElement;
      expect(element.textContent).toBe('Awesome work!');
    });
    test('should not render the results when there is no active elements', () => {
      const resultSextion = screen.queryAllByTestId('resultsHeading');
      expect(resultSextion.length).toBe(0);
    });
  });

  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeEmptyWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    wrapper.innerHTML = '';
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }

  describe(`when the content in the editor contains no issue and the editor is empty as well`, () => {
    beforeEach(() => {
      jest.clearAllMocks();
      const mock = jest.spyOn(a11y, 'useA11yContext');
      mockUseA11yContext.steps = [];
      mock.mockImplementation(() => mockUseA11yContext);
      loreeEmptyWrapper();
      render(<A11yChecker />);
    });
    test('should show no issues happy message found when editor is empty', () => {
      const element = screen.queryAllByTestId('noIssueHeading')[0] as HTMLHeadingElement;
      expect(element.textContent).toBe('Uh oh');
    });
  });
});
