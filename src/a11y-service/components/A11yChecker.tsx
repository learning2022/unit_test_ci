import React, { Children } from 'react';
import { useA11yContext } from '../context/a11y-context';
import { ImpactColor } from './utils';
import { Button } from 'react-bootstrap';
import styles from './A11yChecker.module.scss';
import {
  convertIntoDoubleDigit,
  isContentWrapperEmpty,
  replaceHyphenSpecialCharacter,
} from '../../loree-editor/utils';
import CONSTANTS from '../../loree-editor/constant';
import NoIssue from '../../assets/images/no_issue.png';
import NoIssueEmptyPage from '../../assets/images/no_issue_emptyPage.svg';
import FindingIssues from '../../assets/images/finding_issues.png';
import RecheckingIssues from '../../assets/images/rechecking_issues.svg';
import FixedIssues from '../../assets/images/fixed_issues.png';
import { useA11yTranslation } from '../../i18n/translate';

export function A11yChecker() {
  const {
    steps,
    setActiveStep,
    activeStep,
    clear,
    isProcessing,
    isRechecking,
    check,
    stepsByImpact,
    quickFixer,
    setNotificationFlag,
  } = useA11yContext();

  setNotificationFlag(false);

  const handlePrevClick = () => {
    const next = activeStep - 1 >= 0 ? activeStep - 1 : steps.length - 1;
    setActiveStep(next);
  };

  const handleNextClick = () => {
    const next = activeStep + 1 < steps.length ? activeStep + 1 : 0;
    setActiveStep(next);
  };

  const reCheckButtonSection = (
    <Button
      variant='outline-primary'
      className='modal-footer-button my-2'
      onClick={() => void check(true)}
    >
      Re-check
    </Button>
  );
  const step = steps?.[activeStep];

  let quickFix;
  if (step) {
    quickFix = quickFixer?.({ step, activeStep });
  }

  return (
    <div className={styles.a11yWholeMainSection}>
      <div className={styles.buttons}>
        <button
          id={CONSTANTS.LOREE_A11Y_TRAY_CLOSE_BUTTON}
          className={`${styles.closeButton} d-none`}
          onClick={clear}
        >
          <svg xmlns='http://www.w3.org/2000/svg' width='8' height='8' viewBox='0 0 76.744 76.744'>
            <defs />
            <path
              className='a-close'
              d='M60.379,73.109,37.871,50.6,15.364,73.109A9,9,0,0,1,2.634,60.38L25.142,37.872,2.634,15.364A9,9,0,0,1,15.364,2.636L37.871,25.144,60.379,2.636A9,9,0,0,1,73.107,15.364L50.6,37.872,73.107,60.38A9,9,0,0,1,60.379,73.109Z'
              transform='translate(0.501 0.5)'
            />
          </svg>
        </button>
      </div>

      {!isProcessing ? (
        <></>
      ) : isRechecking ? (
        <div className={styles.responsiveFlexContent + ' ' + styles.responsiveHeight}>
          <div className={styles.severitySection + ' ' + styles.responsiveSectionDesign}>
            <div className={styles.loadingText}>
              <h5 className={styles.resultsHeading + ' ' + styles.loadingHeaderSection}>
                Re-Checking Issues
              </h5>
              <img
                alt='Re-checking Issues…'
                src={RecheckingIssues}
                className={styles.FindingIssuesImage}
              />
            </div>
          </div>
        </div>
      ) : (
        <div className={styles.responsiveFlexContent + ' ' + styles.responsiveHeight}>
          <div className={styles.severitySection + ' ' + styles.responsiveSectionDesign}>
            <div className={styles.loadingText}>
              <h5 className={styles.resultsHeading + ' ' + styles.loadingHeaderSection}>
                Finding Issues…
              </h5>
              <img
                alt='Finding Issues…'
                src={FindingIssues}
                className={styles.FindingIssuesImage}
              />
            </div>
          </div>
        </div>
      )}

      <div className={styles.responsiveFlexContent}>
        <div className={styles.severitySection + ' ' + styles.responsiveSectionDesign}>
          {!isProcessing && (
            <>
              {steps.length > 0 ? (
                <>
                  {stepsByImpact.critical.length <= 0 &&
                  stepsByImpact.serious.length <= 0 &&
                  stepsByImpact.moderate.length <= 0 &&
                  stepsByImpact.minor.length <= 0 &&
                  steps.length === activeStep + 1 ? (
                    <div className={styles.fixedHeadingSection}>
                      <img
                        alt='Fixed Issues'
                        src={FixedIssues}
                        className={styles.accessibilityImage}
                      />
                      <h5 className={styles.noIssueHeading}>Awesome!</h5>
                      <p className={styles.fixedIssuesText}>
                        You have fixed all the known issues. Keep up the great work!
                      </p>
                    </div>
                  ) : (
                    <>
                      <button
                        className={styles.arrowIcon}
                        onClick={handlePrevClick}
                        disabled={!steps.length}
                      >
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          width='8.158'
                          height='13'
                          viewBox='0 0 8.158 13'
                          className={styles.navigationIcon}
                        >
                          <path
                            id='previous'
                            d='M2747.584,141.8l3.934-3.934a1.21,1.21,0,1,0-1.711-1.711l-4.74,4.74a1.342,1.342,0,0,0,0,1.812l4.74,4.74a1.21,1.21,0,1,0,1.711-1.711Z'
                            transform='translate(-2744.214 -135.299)'
                            fill='#5a5a5a'
                            stroke='rgba(0,0,0,0)'
                            strokeMiterlimit='10'
                            strokeWidth='1'
                          />
                        </svg>
                      </button>
                      <span className={styles.resultsCount}>
                        {steps.length > 0 ? (
                          <>
                            {convertIntoDoubleDigit(activeStep + 1)} /{' '}
                            {convertIntoDoubleDigit(steps?.length)}
                          </>
                        ) : (
                          convertIntoDoubleDigit(0)
                        )}{' '}
                        ISSUES{' '}
                      </span>
                      <button
                        className={styles.arrowIcon}
                        onClick={handleNextClick}
                        disabled={!steps.length}
                      >
                        <svg
                          xmlns='http://www.w3.org/2000/svg'
                          width='8.262'
                          height='13'
                          viewBox='0 0 8.262 13'
                          className={styles.navigationIcon}
                        >
                          <path
                            id='next'
                            d='M2749,141.8l-3.934-3.934a1.21,1.21,0,1,1,1.711-1.711l4.74,4.74a1.342,1.342,0,0,1,0,1.812l-4.74,4.74a1.21,1.21,0,0,1-1.711-1.711Z'
                            transform='translate(-2744.111 -135.299)'
                            fill='#5a5a5a'
                            stroke='rgba(0,0,0,0)'
                            strokeMiterlimit='10'
                            strokeWidth='1'
                          />
                        </svg>
                      </button>
                      <dl className={styles.impactSummary}>
                        {stepsByImpact.critical.length > 0 && (
                          <ImpactColor
                            impact='critical'
                            compname='a11yChecker'
                            className={styles.impactSummaryItem}
                          >
                            <dd>{convertIntoDoubleDigit(stepsByImpact.critical.length)}</dd>
                            <dt>Critical</dt>
                          </ImpactColor>
                        )}
                        {stepsByImpact.serious.length > 0 && (
                          <ImpactColor
                            impact='serious'
                            compname='a11yChecker'
                            className={styles.impactSummaryItem}
                          >
                            <dd>{convertIntoDoubleDigit(stepsByImpact.serious.length)}</dd>
                            <dt>Serious</dt>
                          </ImpactColor>
                        )}
                        {stepsByImpact.moderate.length > 0 && (
                          <ImpactColor
                            impact='moderate'
                            compname='a11yChecker'
                            className={styles.impactSummaryItem}
                          >
                            <dd>{convertIntoDoubleDigit(stepsByImpact.moderate.length)}</dd>
                            <dt>Moderate</dt>
                          </ImpactColor>
                        )}
                        {stepsByImpact.minor.length > 0 && (
                          <ImpactColor
                            impact='minor'
                            compname='a11yChecker'
                            className={styles.impactSummaryItem}
                          >
                            <dd>{convertIntoDoubleDigit(stepsByImpact.minor.length)}</dd>
                            <dt>Minor</dt>
                          </ImpactColor>
                        )}
                      </dl>
                    </>
                  )}
                  <div className={styles.divider} />
                </>
              ) : isContentWrapperEmpty() ? (
                <>
                  <div className={styles.noIssueSection}>
                    <img
                      alt='No Page content'
                      src={NoIssueEmptyPage}
                      className={styles.accessibilityImage}
                    />
                    <h5 className={styles.noIssueHeading}>Uh oh</h5>
                    <p className={styles.responsiveNoWordbreak}>
                      Start having fun with filing empty page and then try clicking Accessibility
                      Checker
                    </p>
                  </div>
                </>
              ) : (
                <>
                  <div className={styles.noIssueSection}>
                    <img
                      alt='No Issue Found!'
                      src={NoIssue}
                      className={styles.accessibilityImage}
                    />
                    <h5 className={styles.noIssueHeading}>Awesome work!</h5>
                    <p className={styles.responsiveNoWordbreak}>
                      We have not detected a single issue, you’re smashing it!
                    </p>
                  </div>
                </>
              )}
            </>
          )}
        </div>

        {steps.length > 0 && !isProcessing && (
          <div className={styles.secondSection}>
            <div className={styles.responsiveSectionDesign + ' ' + styles.descriptionSection}>
              <Results />
            </div>
            <div className={styles.divider} />
            <>
              <div
                className={styles.reCheckButtonSection + ' ' + styles.responsiveLastSectionDesign}
              >
                {quickFix && !step?.fixedSinceLastReport && (
                  <div>
                    <h5 className={styles.resultsHeading}>Quick Fix</h5>
                    {quickFix}
                  </div>
                )}
                {!quickFix && (
                  <>
                    <h5 className={styles.resultsHeading}>Instructions</h5>
                    <p>
                      This issue can be rectified via the Loree editor directly. Once you’ve made
                      the appropriate updates, click ‘Re-check’ for validation.
                    </p>
                    {reCheckButtonSection}
                  </>
                )}
                {quickFix && step?.fixedSinceLastReport && <>{reCheckButtonSection}</>}
              </div>
            </>
          </div>
        )}
      </div>
    </div>
  );
}

const Results = () => {
  const { activeStep, steps } = useA11yContext();
  const step = steps?.[activeStep];

  const { t: translate } = useA11yTranslation();

  if (!step) {
    return null;
  }

  return (
    <>
      <h2 className={styles.resultsHeading}>
        <ImpactColor compname='a11yChecker' impact={step.impact}>
          <button
            className={!step?.fixedSinceLastReport ? styles.impactButton : styles.fixedImpactButton}
          />
        </ImpactColor>
        Accessibility Issues {convertIntoDoubleDigit(steps?.length > 0 ? activeStep + 1 : 0)}
      </h2>
      <div className={styles.results}>
        <div>
          <h3 className={styles.resultsSubHeading}>
            {translate(step.title)}
            {step.fixedSinceLastReport ? <span className={styles.fixedImpact}> - Fixed!</span> : ''}
          </h3>
          <p className={styles.stepDescription}>
            {translate(replaceHyphenSpecialCharacter(step.ruleId))}
          </p>
        </div>
        <div>
          {Boolean(step.details?.length) && (
            <details className={styles.details}>
              <summary className={styles.detailsSummary}>
                <h4 className={styles.detailsHeading}>Details</h4>
              </summary>
              <ul className={styles.detailsList}>
                {/* Index keys are usually bad so we use Children.toArray to fix them */}
                {Children.toArray(step.details?.map((summary, i) => <li key={i}>{summary}</li>))}
              </ul>
            </details>
          )}
          {step.docsUrl && (
            <p>
              <a
                href={step.docsUrl}
                className={styles.moreInfoLink}
                target='_blank'
                rel='noreferrer'
              >
                More info
              </a>
            </p>
          )}
        </div>
      </div>
    </>
  );
};
