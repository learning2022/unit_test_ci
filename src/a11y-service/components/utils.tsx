import React, { forwardRef, ElementType, ReactElement } from 'react';
import { Box, PolymorphicComponentProps } from 'react-polymorphic-box';
import clsx from 'clsx';
import { StepImpact } from '../types';

import s from './utils.module.scss';

type ForwardedRef = <E extends ElementType = 'div'>(
  props: ImpactColorProps<E>,
) => ReactElement | null;

type VisuallyHiddenProps<E extends ElementType> = PolymorphicComponentProps<E, {}>;

/**
 * Use this to visually hide an element but still make it accessible for screen readers
 * Use the `as` prop to change the type of element that will render. Defaults to `div`
 *
 * @example
 * <button>
 *   <span class='some-icon' aria-hidden />
 *   <VisuallyHidden as='span'>Do something important</VisuallyHidden>
 * </button
 */
export const VisuallyHidden: ForwardedRef = forwardRef(
  <E extends ElementType = 'div'>(
    { impact, className, compname, ...restProps }: VisuallyHiddenProps<E>,
    ref: typeof restProps.ref,
  ) => {
    const impactHighlight = <Box as='div' ref={ref} className='visuallyHidden' {...restProps} />;
    const impactChecker = (
      <Box as='div' ref={ref} className={clsx(s.visuallyHidden, className)} {...restProps} />
    );

    return compname === 'highlighter' ? impactHighlight : impactChecker;
  },
);

type ImpactColorProps<E extends ElementType> = PolymorphicComponentProps<
  E,
  {
    impact?: StepImpact | 'fixed';
    compname: string;
  }
>;

/**
 * Sets the --a11y-service-color-impact and --a11y-service-color-impact-alt
 * based on the `impact` prop.
 */
export const ImpactColor: ForwardedRef = forwardRef(
  <E extends ElementType = 'div'>(
    { impact = 'minor', className, compname, ...restProps }: ImpactColorProps<E>,
    ref: typeof restProps.ref,
  ) => {
    const severityList = {
      minor: { c: s.impactMinor, h: 'impactMinor' },
      moderate: { c: s.impactModerate, h: 'impactModerate' },
      serious: { c: s.impactSerious, h: 'impactSerious' },
      critical: { c: s.impactCritical, h: 'impactCritical' },
      fixed: { c: s.impactFixed, h: 'impactFixed' },
    };
    const impactChecker = (
      <Box as='div' ref={ref} className={clsx(className, severityList[impact].c)} {...restProps} />
    );
    const impactHighlight = (
      <Box as='div' ref={ref} className={`${className} ${severityList[impact].h}`} {...restProps} />
    );

    return compname === 'highlighter' ? impactHighlight : impactChecker;
  },
);
