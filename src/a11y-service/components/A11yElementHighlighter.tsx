import React, { useState, useEffect, MouseEventHandler } from 'react';
import { usePopper, Modifier } from 'react-popper';
import { PositioningStrategy } from '@popperjs/core';

import { useA11yContext, a11yGlobalContext } from '../context/a11y-context';
import { Step, HighlightsOffset } from '../types';
import { VisuallyHidden, ImpactColor } from './utils';
import { a11yUpdateHighlightsService } from '../../views/editor/observerService';
import CONSTANTS from '../../loree-editor/constant';

a11yUpdateHighlightsService.createObserver()?.subscribe(() => {
  const { updateHighlights, activeStep } = a11yGlobalContext.value;
  if (activeStep >= 0) {
    updateHighlights();
  }
});

export type A11yElementHighlighterProps = {
  positionStrategy?: PositioningStrategy;
};

export function A11yElementHighlighter({
  positionStrategy = 'absolute',
}: A11yElementHighlighterProps) {
  const { activeStep, steps, setActiveElem, setActiveStep, highlightsIdentity, highlightsOffset } =
    useA11yContext();
  const step = steps?.[activeStep];

  useEffect(() => {
    if (step?.element) {
      step.element.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'center' });
      setActiveElem(step.element);
    }

    return () => {
      if (step?.element) {
        setActiveElem(null);
      }
    };
  }, [step, setActiveElem]);

  if (!step) {
    return null;
  }

  return (
    <div className='a11yContainer'>
      {steps.map((step, index) => (
        <Highlight
          key={step.key}
          active={activeStep === index}
          highlightsIdentity={highlightsIdentity}
          offset={highlightsOffset}
          onClick={() => setActiveStep(index)}
          positionStrategy={positionStrategy}
          step={step}
        />
      ))}
    </div>
  );
}

type HighlightProps = {
  active?: boolean;
  highlightsIdentity: {};
  offset?: HighlightsOffset;
  onClick?: MouseEventHandler<HTMLButtonElement>;
  positionStrategy: PositioningStrategy;
  step: Step;
};
export function Highlight({
  active = false,
  highlightsIdentity,
  offset,
  onClick,
  positionStrategy,
  step,
}: HighlightProps) {
  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);
  const { styles, attributes, forceUpdate } = usePopper(step.element, popperElement, {
    placement: 'bottom-start',
    strategy: positionStrategy,
    modifiers: [
      { name: 'computeStyles', options: { adaptive: false } },
      { name: 'hide', enabled: false },
      { name: 'flip', enabled: false },
      { name: 'preventOverflow', enabled: false },
      matchWidthModifier,
    ],
  });

  useEffect(() => {
    forceUpdate?.();
  }, [forceUpdate, highlightsIdentity]);

  return (
    <ImpactColor
      className={active ? 'highlightContainer active' : 'highlightContainer'}
      ref={setPopperElement}
      style={styles.popper}
      compname='highlighter'
      impact={step.fixedSinceLastReport ? 'fixed' : step.impact}
      {...attributes.popper}
    >
      <button className='button impactSeverityButton' disabled={active} onClick={onClick}>
        <VisuallyHidden compname='highlighter'>
          Show issue for this element in the accessibility panel
        </VisuallyHidden>
      </button>
    </ImpactColor>
  );
}

const matchWidthModifier: Modifier<'cover'> = {
  name: 'cover',
  enabled: true,
  phase: 'beforeWrite',
  requires: ['computeStyles'],
  fn: ({ state }) => {
    if (!state.styles.popper) {
      state.styles.popper = {};
    }
    state.styles.popper.width = `${
      state.rects.reference.width - CONSTANTS.LOREE_A11Y_SEVERITY_WIDTH_DEDUCTION
    }px`;
    state.styles.popper.top = `-${
      state.rects.reference.height - CONSTANTS.LOREE_A11Y_SEVERITY_HEIGHT_DEDUCTION
    }px`;
    state.styles.popper.height = `${state.rects.reference.height}px`;
    state.styles.popper.left = `${CONSTANTS.LOREE_A11Y_SEVERITY_LEFT}px`;
  },
  // todo: The below may or may not be necessary
  // effect: ({ state }) => {
  //   const rect = state.elements.reference.getBoundingClientRect();
  //   state.elements.popper.style.width = `${rect.width}px`;
  // },
};
