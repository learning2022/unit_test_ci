import React from 'react';
import { A11yElementHighlighter } from '../components/A11yElementHighlighter';
import * as a11y from '../../a11y-service/context/a11y-context';
import {
  mockUseA11yContext,
  a11yCheckerContainerMockData,
} from '../../loree-editor/a11y-client/quick-fixer/mockData';
import { createElement, getNElementByClassName } from '../../loree-editor/common/dom';
import { configure, fireEvent, render, screen } from '@testing-library/react';
configure({ testIdAttribute: 'class' });

describe('#A11yElementHighlighter', () => {
  const activeElement = [
    [0, true],
    [1, false],
    [2, false],
  ];
  let elementHighlighters: HTMLCollection | undefined;
  beforeEach(() => {
    jest.clearAllMocks();
    const mock = jest.spyOn(a11y, 'useA11yContext');
    mock.mockImplementation(() => a11yCheckerContainerMockData);
    document.body.innerHTML = '';
    const styleSheet = createElement('style');
    styleSheet.innerHTML = `.highlightContainer.active {
      background: rgba(22, 163, 74, 0.2);
      opacity: 0.2;
    }`;
    document.body.append(styleSheet);
    render(<A11yElementHighlighter positionStrategy='absolute' />);
  });

  test('should render with aceesibility highliters to notify the errors', () => {
    elementHighlighters = screen.queryAllByTestId('a11yContainer')[0]?.children;
    expect(elementHighlighters?.length).toBe(3);
  });

  test.each(activeElement)('should render with the default active element', (index, selected) => {
    const element = (elementHighlighters as HTMLCollection)[index as number];
    expect(element?.classList.contains('active')).toBe(selected);
  });

  test('should open the actray which is relevant to particular highlighter when clicking on it', () => {
    const element = screen.queryAllByTestId('button impactSeverityButton')[1] as HTMLButtonElement;
    fireEvent.click(element);
    expect(a11yCheckerContainerMockData.setActiveStep).toHaveBeenCalledTimes(1);
  });
  test('Should show severity in width and height while opening the AC tray', () => {
    const severityHighlighter = getNElementByClassName('highlightContainer active');
    expect(severityHighlighter?.style.width).toBeTruthy();
  });
  test('Should show severity background color', () => {
    const severityHighlighter = getNElementByClassName('highlightContainer active');
    const style = window.getComputedStyle(severityHighlighter, '');
    const backgroundColor = style.getPropertyValue('background');
    expect(backgroundColor).toBe(`rgba(22, 163, 74, 0.2)`);
  });
  test('Should show severity in top and left while opening the AC tray', () => {
    const severityHighlighter = getNElementByClassName('highlightContainer active');
    expect(severityHighlighter?.style.top).toBeTruthy();
    expect(severityHighlighter?.style.left).toBeTruthy();
  });
});

describe('should not render the element highlighters when there is no content', () => {
  beforeEach(() => {
    const mock = jest.spyOn(a11y, 'useA11yContext');
    mockUseA11yContext.steps = [];
    mock.mockImplementation(() => mockUseA11yContext);
    render(<A11yElementHighlighter positionStrategy='absolute' />);
  });

  test('should not render the ac highliters on the elements when the container does not exists', () => {
    const elementHighlighters = screen.queryAllByTestId('highlightContainer');
    expect(elementHighlighters.length).toBe(0);
  });
});
