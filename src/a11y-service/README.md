# Crystal Delta Accessibility Service

This service uses [axe-core](https://github.com/dequelabs/axe-core#readme) to check and display accessibility issues. There are two main parts to this service.

1. The service itself
2. The UI layer

## The Service

This is where the core functionality lives. It is in charge of checking for accessibility issues, registering custom checks and rules, and transforming the Axe report into steps that can be consumed by the UI.

## UI Layer

A React based UI layer that provides components and an API to consume the results from The Service. It consists of the following main components:

### A11yContextProvider

This should wrap the parts of the app that will use the Accessibility Service.

### A11yChecker

The UI that will allow the user to step through any issues found.

### A11yElementHighlighter

Creates interactive highlight markers next to elements that have accessibility issues. Include this component in the same scrollable and positioned container as the content you will be checking.
