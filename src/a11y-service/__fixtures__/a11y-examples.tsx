import React from 'react';
import { CustomRule } from '../../loree-editor/a11y-client/types';

// language=HTML
export const html = `
<p style="font-weight: bold; font-size: 24px"><strong>Introduction</strong></p>
<hr />
<h2>The React Logo</h2>
<p>
  <a href="#example">
    <img src="https://via.placeholder.com/150" alt="" />
  </a>
  <a href="#example">
    This is a link that has also been added to the image to the left.
  </a>
</p>
<p>
  Image with no alt or title: <img src="https://via.placeholder.com/50" />
</p>
<p>
  Another image with no alt or title but also a bad aria attribute: <img src="https://via.placeholder.com/50" aria-foo="bar" />
</p>
<p>* This is a list.</p>
<p>* That has been poorly formatted.</p>
<p>* By using asterisks, instead of using</p>
<ul>
  <li>elements.</li>
</ul>
<h4>Out of order heading</h4>
<p>
  <span id="duplicate">Duplicate</span> <span id="duplicate">ID</span>
</p>
<h4>To create an <strong>inaccessible</strong> ordered list:</h4>
<p>1. Pick a ordering scheme,</p>
<p>2. Type the item number manually for each item,</p>
<p>3. Don't use</p>
<ol>
  <li>elements.</li>
</ol>
<p aria-labeledbody="true">
  This sentence contains some words that have
  <span style="background-color: #5a5a5a">low color contrast</span>, which makes them
  <span style="color: #ced4d9">difficult to read</span>.
</p>
<p>
  <strike>&lt;Strike&gt;</strike> elements are not allowed. You should use <s>the "s" tag instead</s>
</p>
<h3>An inaccessible table</h3>
<p>The below table is missing a caption and table header cells (elements).</p>
<table style="border-collapse: collapse; width: 100%" border="1">
  <tbody>
    <tr>
      <td style="width: 31.819%">&nbsp;</td>
      <td style="width: 31.819%">&nbsp;</td>
      <td style="width: 31.819%">&nbsp;</td>
    </tr>
    <tr>
      <td style="width: 31.819%">&nbsp;</td>
      <td style="width: 31.819%">&nbsp;</td>
      <td style="width: 31.819%">&nbsp;</td>
    </tr>
    <tr>
      <td style="width: 31.819%">&nbsp;</td>
      <td style="width: 31.819%">&nbsp;</td>
      <td style="width: 31.819%">&nbsp;</td>
    </tr>
  </tbody>
</table>
<p>&nbsp;</p>
`;

export const EXAMPLES = {
  'image-alt': `
    <img data-testid="image-alt" src="https://via.placeholder.com/150" />
  `,

  'link-name': `
    <a href="#example">
      <img src="https://via.placeholder.com/150" alt="" />
    </a>
  `,

  'p-as-heading': `
    <p style="font-weight: bold; font-size: 24px;">The Heading</p>
    <p style="font-weight: normal; font-size: 16px;">Some Content.</p>
  `,

  [CustomRule.NoStrike]: `
    <strike>&lt;Strike&gt;</strike> elements are not allowed. You should use <s>the "s" tag instead</s>
  `,
};

type Examples = (keyof typeof EXAMPLES)[];

const allExamples = Object.keys(EXAMPLES) as Examples;

type A11yExamplesProps = {
  examples?: Examples;
};

export function A11yExamples({ examples = allExamples }: A11yExamplesProps) {
  return (
    <div
      id='example-content' // used to narrow xpath of axe results
      data-testid='A11yExamples'
    >
      {examples.map((id) => (
        <div key={id} dangerouslySetInnerHTML={{ __html: EXAMPLES[id] }} />
      ))}
    </div>
  );
}

export function A11yExamplesIFrame({ examples = allExamples }: A11yExamplesProps) {
  return <iframe title='test iframe' data-testid='A11yExamplesIFrame' />;
}

export function A11yExamplesFull() {
  return (
    <div
      id='example-content' // used to narrow xpath of axe results
      dangerouslySetInnerHTML={{ __html: html }}
    />
  );
}
