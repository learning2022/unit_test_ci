import { ImpactValue, Check } from 'axe-core';

declare module 'axe-core' {
  export interface Rule {
    metadata?: {
      description?: string;
      help?: string;
    };
  }

  export interface CheckAugmented<Options = unknown> extends Check {
    evaluate?:
      | ((node: HTMLElement | SVGElement, options: Options, virtualNode: unknown) => boolean)
      | string;
    options?: Options;
    metadata?: {
      impact?: ImpactValue;
      messages?: {
        pass?: string;
        fail?: string;
        incomplete?: string;
      };
    };
  }
}
