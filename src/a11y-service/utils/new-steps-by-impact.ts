import { StepsByImpact } from '../types';

export const newStepsByImpact = (): StepsByImpact => ({
  minor: [],
  critical: [],
  moderate: [],
  serious: [],
});
