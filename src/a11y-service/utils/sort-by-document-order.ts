import { Steps } from '../types';

export function sortByDocumentOrder(steps: Steps) {
  return [...steps].sort(({ element: elementA }, { element: elementB }) => {
    if (elementA === elementB || !elementA || !elementB) {
      return 0;
    }

    const position = elementA.compareDocumentPosition(elementB);

    if (
      position & Node.DOCUMENT_POSITION_FOLLOWING ||
      position & Node.DOCUMENT_POSITION_CONTAINED_BY
    ) {
      return -1;
    } else if (
      position & Node.DOCUMENT_POSITION_PRECEDING ||
      position & Node.DOCUMENT_POSITION_CONTAINS
    ) {
      return 1;
    } else {
      return 0;
    }
  });
}
