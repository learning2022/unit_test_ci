/* eslint-disable */ // Remove this line when editing this file
import CONSTANTS from '../loree-editor/constant';
import { getAPMServiceVersion } from './Router';
import { Auth } from 'aws-amplify';

describe('Router', () => {
  it('generates apm version with git commit', () => {
    expect(getAPMServiceVersion('test')).toEqual(`test-${CONSTANTS.LOREE_APP_VERSION}`);
  });

  it('generates apm version without git commit', () => {
    expect(getAPMServiceVersion(undefined)).toEqual(CONSTANTS.LOREE_APP_VERSION);
  });
});

describe('APM errors', () => {
  // Before signin Authentication statement handling
  test('promise reject handling', async () => {
    // If user is not authenticated then it returns 0
    const returnValue = await Auth.currentAuthenticatedUser().then(
      response => {
        return 1;
      },
      function (err) {
        return 0;
      },
    );
    expect(returnValue).toEqual(0);
  }, 5000);
});
