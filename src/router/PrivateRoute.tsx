/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';
import featureFlags from '../featureFlags';

const PrivateRoute = ({ component: Component, ...rest }: any & { component: any }) => {
  const checkUserAuth = localStorage.getItem('ReactAmplify.TokenKey');
  return (
    <Route
      {...rest}
      render={(props) => {
        return checkUserAuth ? (
          <Component {...props} featureFlags={featureFlags} />
        ) : (
          <Redirect
            to={{
              pathname: '/',
            }}
          />
        );
      }}
    />
  );
};

export default PrivateRoute;
