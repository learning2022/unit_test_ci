import React from 'react';
import { Switch, Route, Router, Link } from 'react-router-dom';
import { ApmBase, init as initApm } from '@elastic/apm-rum';
import { ApmRoute } from '@elastic/apm-rum-react';

import { createBrowserHistory } from 'history';
import { Auth } from 'aws-amplify';

import Home from '../views/home/Home';
import Project from '../views/project';
import Dashboard from '../views/dashboard/index';
import Templates from '../views/templates/index';
import Settings from '../views/settings';
import ViewProject from '../views/project/viewProject';
import PrivateRoute from './PrivateRoute';
import { LoreeEditorInitiate } from '../views/editor/LoreeEditorInitiate';
import LtiLogin from '../lti/login';
import CanvasHome from '../lti/canvas/index';
import LtiUnAuth from '../lti/ltiUnAuth';
import { AdminDashboard } from '../lti/admin';
import { LmsAccess } from '../utils/lmsAccess';
import ClientList from '../superAdmin/clientList';
import FeatureList from '../superAdmin/featureList';
import Statistics from '../superAdmin/statistics';
import Migrate from '../superAdmin/dataMigrate/migrate';
import D2lHome from '../lti/d2l';
import BBHome from '../lti/blackBoard/';
import CONSTANTS from '../loree-editor/constant';
import LoginSecurity from '../views/settings/loginSecurity';
import ExternalToolAuth from '../loree-editor/modules/sidebar/externalTool/externalToolAuth';
import ExternalToolContent from '../loree-editor/modules/sidebar/externalTool/externalToolContent';
import ExternalToolLogin from '../loree-editor/modules/sidebar/externalTool/externalToolLogin';

let loreeUserId = '';
const history = createBrowserHistory();
const lms = new LmsAccess();
let apm: ApmBase;

const GIT_COMMIT = process.env.REACT_APP_GIT_COMMIT;
const LOREE_ENV = process.env.REACT_APP_LOREE_ENV;

export const getAPMServiceVersion = (GIT_COMMIT?: string) =>
  (GIT_COMMIT ? GIT_COMMIT + '-' : '') + CONSTANTS.LOREE_APP_VERSION;

if (['develop', 'staging', 'production', 'productionus'].includes(LOREE_ENV || '')) {
  apm = initApm({
    serviceName: 'Loree-V2',
    serviceVersion: getAPMServiceVersion(GIT_COMMIT),
    environment: LOREE_ENV,
    serverUrl: 'https://5c460b35aaaa424491760e12614fccc2.apm.ap-southeast-2.aws.cloud.es.io:443',
    // TOOD: once we get server APM working
    // distributedTracingOrigins: ['https://api.example.com']
  });
}

Auth.currentAuthenticatedUser().then(
  (response) => {
    loreeUserId = response.username;
    try {
      if (apm)
        apm.setUserContext({
          id: loreeUserId || 'unknown',
          username: response.attributes['custom:lms_email'],
        });
    } catch (e) {
      if (apm) apm.setUserContext({ id: loreeUserId || 'unknown' });
      console.error('Attempted to set set user for APM', e);
    }

    if (
      sessionStorage.getItem('envName') === 'production' ||
      sessionStorage.getItem('envName') === 'productionus'
    ) {
      let trackingId = 'G-CBK5VLYE8K';
      if (lms.getAccess()) trackingId = sessionStorage.getItem('trackingId') as string;
      history.listen((location, action) => {
        if (trackingId)
          void initiateGoogleAnalytics(
            location.pathname + location.search,
            loreeUserId,
            trackingId,
          );
      });
    }
  },
  function (_err) {
    return 0;
  },
);

const initiateGoogleAnalytics = async (
  trackingPath: string,
  loreeUserId: string,
  trackingId: string,
) => {
  const analyticsScript = document.getElementById('analytics_script');
  const analyticsDataScript = document.getElementById('analytics_data_script');
  if (analyticsScript && analyticsDataScript) {
    analyticsScript.remove();
    analyticsDataScript.remove();
  }
  const analyticsUrlScript = document.createElement('script');
  analyticsUrlScript.setAttribute('async', 'true');
  analyticsUrlScript.id = 'analytics_script';
  analyticsUrlScript.src = `https://www.googletagmanager.com/gtag/js?id=${trackingId}`;
  const analyticsFunctionScript = document.createElement('script');
  analyticsFunctionScript.id = 'analytics_data_script';
  const analyticsFunction = `window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}gtag('js', new Date()); gtag('config', '${trackingId}', {'page_path': '${trackingPath}', 'user_id': '${loreeUserId}', 'cookie_prefix': 'gaCookie','cookie_domain': 'auto', 'cookie_expires': 60*60*24*28, 'cookie_update': 'false'}); gtag('set', {'cookie_flags': 'SameSite=None;Secure'});`;
  analyticsFunctionScript.innerText = analyticsFunction;
  document.body.appendChild(analyticsUrlScript);
  document.body.appendChild(analyticsFunctionScript);
};

document.onclick = function (e: MouseEvent) {
  const avtarModal = document.getElementById(CONSTANTS.LOREE_AVTAR_MODAL);
  const modalElementIds = [
    CONSTANTS.LOREE_USER_PROFILE_EMAIL,
    CONSTANTS.LOREE_USER_PROFILE_NAME,
    CONSTANTS.LOREE_USER_PROFILE_AVTAR,
    CONSTANTS.LOREE_AVTAR_MODAL,
    CONSTANTS.LOREE_MAIN_AVTAR,
  ];
  if (e.target instanceof Element) {
    const targetId = e?.target?.id;
    const targetParentId = e?.target?.parentElement?.id;

    if (
      modalElementIds.includes(targetId) ||
      (targetParentId && modalElementIds.includes(targetParentId))
    ) {
      if (e.target.id === CONSTANTS.LOREE_MAIN_AVTAR) avtarModal?.classList.toggle('d-none');
      else avtarModal?.classList.remove('d-none');
    } else avtarModal?.classList.add('d-none');
  }
};

const NoMatch = () => {
  return (
    <>
      <p className='h1 d-flex justify-content-center mt-5'>OOPS! Page not found</p>
      <small className='d-flex justify-content-center mt-5'>
        <Link to='/'>Back to Login</Link>
      </small>
    </>
  );
};

const Routing = () => {
  return (
    <>
      <Router history={history}>
        <Switch>
          <ApmRoute exact path='/'>
            <Home />
          </ApmRoute>
          <PrivateRoute exact path='/dashboard' component={Dashboard} />
          <PrivateRoute exact path='/templates' component={Templates} />
          <PrivateRoute exact path='/project' component={Project} />
          <PrivateRoute path='/project/:id' component={ViewProject} />
          <PrivateRoute path='/loree-editor/:id' component={LoreeEditorInitiate} />
          <PrivateRoute exact path='/settings' component={Settings} />
          <PrivateRoute exact path='/security' component={LoginSecurity} />
          {/* LTI Routes */}
          <Route path='/lti/canvas/login' component={LtiLogin} />
          <PrivateRoute path='/lti/canvas/home' component={CanvasHome} />
          <PrivateRoute path='/lti/d2l/home' component={D2lHome} />
          <PrivateRoute path='/lti/bb/home' component={BBHome} />
          <PrivateRoute path='/lti/editor/:id/:type/:title' component={LoreeEditorInitiate} />
          <Route path='/lti/unauth' component={LtiUnAuth} />
          {/* Admin Routes */}
          <PrivateRoute path='/lti/editor/admin' component={AdminDashboard} />
          <PrivateRoute exact path='/superadmin/clients' component={ClientList} />
          <PrivateRoute exact path='/superadmin/:id/statistics' component={Statistics} />
          <PrivateRoute exact path='/superadmin/:id/features' component={FeatureList} />
          <PrivateRoute exact path='/superadmin/:id/migrate' component={Migrate} />
          {/* External tool route */}
          <Route path='/lti/tool/auth' component={ExternalToolAuth} />
          <Route path='/lti/tool/content' component={ExternalToolContent} />
          <Route path='/lti/tool/login' component={ExternalToolLogin} />

          <Route component={NoMatch} />
        </Switch>
      </Router>
    </>
  );
};

export default Routing;
