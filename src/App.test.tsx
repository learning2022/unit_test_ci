import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createElement } from './loree-editor/common/dom';

it('renders without crashing', () => {
  const div = createElement('div');
  ReactDOM.render(<App />, div);
});
