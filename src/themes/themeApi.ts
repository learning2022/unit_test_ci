import { API, graphqlOperation } from 'aws-amplify';
import {
  CreateCustomThemeInput,
  CreateCustomThemeMutation,
  CreateCustomThemeMutationVariables,
  DeleteCustomThemeInput,
  DeleteCustomThemeMutation,
  DeleteCustomThemeMutationVariables,
  ListCustomThemesQuery,
  UpdateCustomThemeInput,
  UpdateCustomThemeMutation,
  UpdateCustomThemeMutationVariables,
} from '../API';
import { createCustomTheme, deleteCustomTheme, updateCustomTheme } from '../graphql/mutations';
import { listCustomThemes } from '../graphql/queries';

import { CustomThemeDetails } from './themes';

export async function loadThemesFromServer(platformId: string): Promise<CustomThemeDetails[]> {
  const listThemesResponse = await API.graphql<ListCustomThemesQuery>(
    graphqlOperation(listCustomThemes),
    { platformId: platformId },
  );

  const items = listThemesResponse.data?.listCustomThemes?.items;
  if (!items) {
    return [];
  }
  return items
    .filter((i): i is NonNullable<typeof items[number]> => !!i)
    .map((i) => i.themeData)
    .filter((i): i is string => !!i)
    .map<CustomThemeDetails>((i) => {
      return JSON.parse(i);
    });
}

export async function createThemeOnServer(platformId: string, theme: CustomThemeDetails) {
  const input: CreateCustomThemeInput = {
    ltiPlatformID: platformId,
    themeData: JSON.stringify(theme),
    isShared: false,
  };

  const result = await API.graphql<CreateCustomThemeMutation>(
    graphqlOperation(createCustomTheme, { input } as CreateCustomThemeMutationVariables),
  );

  return !!result.data?.createCustomTheme;
}

export async function updateThemeOnServer(
  themeId: string,
  platformId: string,
  theme: CustomThemeDetails,
): Promise<boolean> {
  const input: UpdateCustomThemeInput = {
    id: themeId,
    ltiPlatformID: platformId,
    themeData: JSON.stringify(theme),
    isShared: false,
  };

  const result = await API.graphql<UpdateCustomThemeMutation>(
    graphqlOperation(updateCustomTheme, { input } as UpdateCustomThemeMutationVariables),
  );

  return !!result?.data?.updateCustomTheme;
}

export async function deleteThemeOnServer(themeId: string): Promise<boolean> {
  const input: DeleteCustomThemeInput = {
    id: themeId,
  };

  const result = await API.graphql<DeleteCustomThemeMutation>(
    graphqlOperation(deleteCustomTheme, { input } as DeleteCustomThemeMutationVariables),
  );

  return !!result.data?.deleteCustomTheme;
}
