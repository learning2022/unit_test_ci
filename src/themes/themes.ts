import { loadThemesFromServer } from './themeApi';

interface ThemeColor {
  name: string;
  color: string;
}

export interface CustomThemeDetails {
  name: string;
  colors: ThemeColor[];
}

export async function getThemes(platformId: string) {
  const themes = await loadThemesFromServer(platformId);
  return themes;
}
