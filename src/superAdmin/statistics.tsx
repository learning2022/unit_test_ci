/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment } from 'react';
import LeftSidenav from '../components/dashboardSideNavbar';
import { Container, Card, Col, Row, Alert } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import { listLtiAccessTokens } from '../graphql/queries';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';

class Statistics extends React.Component {
  state = {
    usersCount: 0,
  };

  async componentDidMount() {
    try {
      const accessTokens: any = await API.graphql(graphqlOperation(listLtiAccessTokens));
      console.log('Users:', accessTokens.data.listLtiAccessTokens);
    } catch (err) {
      console.error('Error fetching listLtiAccessTokens in SuperAdmin. Error:', err);
    }
  }

  render() {
    return (
      <Fragment>
        <LeftSidenav superAdminSidebar={true} />
        <Container fluid={true} className='recent-section'>
          <h3 className='mt-5 text-primary'>Statistics</h3>
          <Alert variant='info'>
            <Alert.Heading>Coming Soon! Currently in development!</Alert.Heading>
            <p>
              In this Statistics page, the individual client Statistics like Users, Custom templates, Custom blocks, Number of pages
              designed using Loree details will be displayed.
            </p>
          </Alert>
          <Row className='mt-5'>
            <Col xs={3}>
              <Card className='shadow  mb-5 bg-white rounded'>
                <Card.Body>
                  <h6 className='text-secondary'>Users</h6>
                  <Card.Title>245</Card.Title>
                  <p className='mt-3 mb-0 text-muted text-sm'>
                    <span className='text-success mr-2'>
                      <FontAwesomeIcon icon={faArrowUp} />
                      3.48%
                    </span>
                    <span className='text-nowrap'>Since last week</span>
                  </p>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={3}>
              <Card className='shadow mb-5 bg-white rounded'>
                <Card.Body>
                  <h6 className='text-secondary'>Traffic</h6>
                  <Card.Title>234,34</Card.Title>
                  <p className='mt-3 mb-0 text-muted text-sm'>
                    <span className='text-danger mr-2'>
                      <FontAwesomeIcon icon={faArrowDown} /> 3.48%
                    </span>
                    <span className='text-nowrap'>Since last week</span>
                  </p>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={3}>
              <Card className='shadow mb-5 bg-white rounded'>
                <Card.Body>
                  <h6 className='text-secondary'>Custom Templates</h6>
                  <Card.Title>30</Card.Title>
                  <p className='mt-3 mb-0 text-muted text-sm'>
                    <span className='text-danger mr-2'>
                      <FontAwesomeIcon icon={faArrowDown} /> 3.48%
                    </span>
                    <span className='text-nowrap'>Since last week</span>
                  </p>
                </Card.Body>
              </Card>
            </Col>
            <Col xs={3}>
              <Card className='shadow mb-5 bg-white rounded'>
                <Card.Body>
                  <h6 className='text-secondary'>Custom Blocks</h6>
                  <Card.Title>50</Card.Title>
                  <p className='mt-3 mb-0 text-muted text-sm'>
                    <span className='text-success mr-2'>
                      <FontAwesomeIcon icon={faArrowUp} /> 3.48%
                    </span>
                    <span className='text-nowrap'>Since last week</span>
                  </p>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </Fragment>
    );
  }
}
export default Statistics;
