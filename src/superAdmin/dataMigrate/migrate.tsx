import React from 'react';
import { Table, Button, Dropdown, Form } from 'react-bootstrap';
import XLSX from 'xlsx';
import LeftSidenav from '../../components/dashboardSideNavbar';
import ToastComponent from '../../lti/components/ToastComponent';
import {
  getLoreeV2User,
  createMigrateBlock,
  createMigrateTemplate,
  handleContentCheck,
  isUserFound,
  isTemplateMigrated,
} from './components/action';
import { UploadedDataInterface } from '../superAdminInterface';
import './migrate.scss';
import { isUploadedFileValid } from './components/fileUpload';
import { adminRoles } from '../../graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import Loading from '../../components/loader/loading';
import {
  CategorizedFeatures,
  LmsRoleAndFeatures,
  LoreeEditorFeatures,
} from '../../loree-editor/interface';
import { createLoreeRole } from '../../graphql/mutations';
import { translate } from '../../i18n/translate';

const Features: CategorizedFeatures = {
  Basic: {
    undo: 'undoredo',
    redo: 'undoredo',
    header: 'textblock',
    sourceFormat: 'textblock',
    text: 'textblock',
    runAccessibilityChecker: 'accessibilitychecker',
    codeView: 'codeproperties',
    selectionCodeEdit: 'codeproperties',
    selectionCodeView: 'codeproperties',
    outlinesToggle: 'outline',
    preview: 'preview',
  },
  Advanced: {
    iconHeadingText: 'specialblocks',
    iconLeftAligned: 'specialblocks',
    iconText: 'specialblocks',
    banner: 'banner',
    bannerWithTextBlock: 'banner',
    youtube: 'youtube',
    vimeo: 'vimeo',
    link: 'insertlink',
    html5source: 'insertbyurl',
    backgroundColourPickerHex: ['backgroundcoloroftherow', 'backgroundcolorofthecolumn'],
    delete: ['deleterow', 'deleteelement', 'deletecolumn'],
    duplicate: ['duplicaterow', 'duplicateelement'],
    editTable: ['table', 'tabledesign'],
    image: ['image', 'imagedesign', 'uploadimage'],
    margin: ['marginrow', 'margincolumn'],
    padding: ['paddingrow', 'paddingcolumn'],
    video: ['video', 'videodesign'],
  },
  'Custom Blocks': {
    saveTemplate: 'saveastemplate',
    selectionSaveAsNewBlock: ['saveascustomrow', 'saveascustomelement'],
    showUseCustomBlocksFilterSearch: ['customrowfiltersearch', 'customelementsfiltersearch'],
    showUseTemplatesFilterSearch: 'templatesfiltersearch',
  },

  Interactives: { liContent: 'myinteractive', liOption: 'myinteractive' },
  H5P: { showUseH5pBlocks: 'h5p' },
};
class Migrate extends React.Component {
  state = {
    loading: true,
    tableContent: [],
    loadType: '',
    index: 0,
    userIdList: [],
    userName: '',
    ownerId: '',
    btnName: 'Migrate',
    showToast: false,
    showToastMsg: '',
    selectedDataType: '',
    isJsonData: false,
    roleLists: [],
    lmsRoles: [],
    lmsFeatrues: [],
    disabledMigrateBtn: false,
  };

  async componentDidMount() {
    const userToken = await getLoreeV2User();
    const currentPlatformUser = userToken?.items?.filter(
      (user) =>
        user?.isAdmin && String(user?.ltiPlatformID) === String(localStorage.getItem('clientId')),
    );
    const roles = await API.graphql<_Any>(
      graphqlOperation(adminRoles, {
        loreeUserEmail:
          typeof currentPlatformUser !== 'undefined' && currentPlatformUser[0]?.loreeUsername,
        accountId: 1,
      }),
    );
    const parsedRoleList = JSON.parse(roles?.data?.adminRoles).body;
    this.setState({
      userIdList: userToken?.items ?? [],
      roleLists: parsedRoleList,
      loading: false,
    });
  }

  setLoaderStatus = (status: Boolean): void => {
    this.setState({
      loading: status,
    });
  };

  handleFileChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setLoaderStatus(true);
    const file: File = (e.target.files as FileList)[0];
    if (isUploadedFileValid(file)) {
      this.setLoaderStatus(false);
    }
    return !file.name.includes('.json') ? this.readCsvFile(e) : this.readJsonFile(file);
  };

  storeTemplateData = (data: _Any) => {
    this.setState({
      tableContent: data,
      btnName: 'Migrate',
    });
  };

  readCsvFile = (e: _Any): void => {
    /* Boilerplate to set up FileReader */
    const file = e?.target?.files[0];
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e: ProgressEvent<FileReader>) => {
      /* Parse data */
      const bstr = e.target?.result;
      const wb = XLSX.read(bstr, {
        type: rABS ? 'binary' : 'array',
        bookVBA: true,
      });
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, {
        raw: false,
      });
      /* Update state */
      this.storeTemplateData(data);
    };
    if (rABS) {
      reader.readAsBinaryString(file);
    } else {
      reader.readAsArrayBuffer(file);
    }
    this.setLoaderStatus(false);
  };

  readJsonFile = (file: File): void => {
    const fileReader: FileReader = new FileReader();
    fileReader.readAsBinaryString(file);
    fileReader.onload = (e: ProgressEvent<FileReader>) => {
      const uploadList = JSON.parse(fileReader.result + '').features; // features list
      const features = Object.values(uploadList)[0] as Array<LoreeEditorFeatures>;
      const filteredRoles = this.state.roleLists
        .filter(({ id: roleId }) =>
          Object.keys(uploadList).some((upListId) => String(roleId) === String(upListId)),
        )
        .map((item: _Any) => ({ ...item, features: uploadList[item.id] }));
      this.setState({
        isJsonData: true,
        lmsRoles: filteredRoles,
        lmsFeatrues: Object.keys(features),
        loading: false,
      });
    };
  };

  handleMigrate = async (data: UploadedDataInterface, idx: number) => {
    this.setState({
      loadType: 'REQUESTED',
      index: idx,
    });
    try {
      const migratedUser = await isUserFound(data.user_id);
      this.setState({
        userName: JSON.parse(migratedUser?.data?.migrateUser as string)[3].Value,
        ownerId: JSON.parse(migratedUser?.data?.migrateUser as string)[0].Value,
      });
      void this.saveMigrateTemplate(
        data,
        idx,
        JSON.parse(migratedUser?.data?.migrateUser as string)[2].Value,
      );
    } catch (err) {
      this.showToast(translate('superadmin.datafetchfail', { detail: err }));
    }
  };

  migrateRolesAndFeatures = () => {
    const filteredRoles = this.filterRoles(this.state.lmsRoles);
    const mappedStructure = this.mapV1toV2Features(filteredRoles);
    for (const input of mappedStructure) {
      void this.createLoreeRoles(input);
    }
    this.showToast(translate('superadmin.roleandfeaturesuccess'));
    this.setState({ disabledMigrateBtn: true });
    this.closeToast();
  };

  filterRoles = (roles: LmsRoleAndFeatures[]) => {
    return roles.filter(({ role }) => !role.toLowerCase().includes('admin'));
  };

  mapV1toV2Features = (filteredRoles: LmsRoleAndFeatures[]) => {
    return filteredRoles.map(
      ({ features, role }: { features: LoreeEditorFeatures; role: string }) => {
        const concat = this.concatFeatures(features);
        return {
          ltiPlatformID: String(localStorage.getItem('clientId')),
          name: `V1-${role}`,
          featureList: JSON.stringify(concat),
        };
      },
    );
  };

  concatFeatures = (features: LoreeEditorFeatures) => {
    return Object.entries(features).reduce((acc: _Any, [v1FeatureKey, v1FeatureProps]) => {
      if (!v1FeatureProps.value) {
        return acc;
      }
      let matched;
      for (const category in Features) {
        const v2Features = Features[category][v1FeatureKey];
        if (!v2Features) {
          continue;
        }
        if (Array.isArray(v2Features)) {
          matched = v2Features.map((str) => ({ id: str, type: category }));
        } else {
          matched = [{ id: v2Features, type: category }];
        }
        break;
      }
      if (!matched) {
        return acc;
      } else {
        const nonAccumulated = matched.filter(
          (m: { id: string }) => !acc.some((e: { id: string }) => e.id === m.id),
        );
        return [...acc, ...nonAccumulated];
      }
    }, []);
  };

  createLoreeRoles = async (input: {
    ltiPlatformID: string;
    name: string;
    featureList: string;
  }) => {
    try {
      await API.graphql(graphqlOperation(createLoreeRole, { input }));
    } catch (error) {
      console.error(error);
    }
  };

  updateTableContent = async (idx: number) => {
    const tableData = this.state.tableContent.map((item: _Any) => ({ ...item }));
    tableData[idx].migrated = true;
    this.setState({
      loadType: 'SUCCESS',
      tableContent: tableData,
    });
  };

  saveMigrateTemplate = async (data: UploadedDataInterface, idx: number, platformId: string) => {
    const templateCheck = await this.handleTemplate(data.name, idx);
    if (templateCheck) {
      let updatedData: string | undefined;
      switch (this.state.selectedDataType) {
        case 'Template':
          updatedData = await createMigrateTemplate(
            data,
            this.state.userName,
            this.state.ownerId,
            platformId,
          );
          break;
        case 'Blocks':
          updatedData = await createMigrateBlock(
            data,
            this.state.userName,
            this.state.ownerId,
            platformId,
          );
          break;
      }
      this.showToast(
        translate('superadmin.migratesuccess', {
          detail1: updatedData,
          detail2: this.state.selectedDataType,
        }),
      );
      void this.updateTableContent(idx);
    }
  };

  handleTemplate = async (title: string, idx: number) => {
    if (!(await isTemplateMigrated(title))) {
      return true;
    } else {
      this.showToast('template.alreadyupdated');
      void this.updateTableContent(idx);
      return false;
    }
  };

  showToast = (toastMsg: string) => {
    this.setState({ showToast: true, showToastMsg: toastMsg });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  render() {
    return (
      <>
        <LeftSidenav superAdminSidebar />
        <div className='recent-section'>
          <span className='h2 d-inline-block text-secondary mt-5' data-testid='migrateHeader'>
            {this.state.selectedDataType}
          </span>
          {this.state.loading ? <Loading /> : ''}
          {this.state.tableContent.length <= 0 && !this.state.isJsonData ? (
            <div className='d-flex float-right my-5'>
              <Dropdown
                className='ml-3 dropdown dropdown-layout mr-5'
                data-testid='migrateDropdown'
              >
                <Dropdown.Toggle variant='link' className='dropdown-toggle' title='Help'>
                  {translate('global.select')}
                </Dropdown.Toggle>
                <Dropdown.Menu className='canvas-help-menu' data-testid='migrateDropdownMenuList'>
                  <Dropdown.Item
                    className='btn dropdown-item'
                    key='template'
                    onClick={() =>
                      this.setState({ selectedDataType: translate('global.template') })
                    }
                  >
                    {translate('global.template')}
                  </Dropdown.Item>
                  <Dropdown.Item
                    className='btn dropdown-item'
                    key='blocks'
                    onClick={() => this.setState({ selectedDataType: translate('global.blocks') })}
                  >
                    {translate('global.blocks')}
                  </Dropdown.Item>
                  <Dropdown.Item
                    className='btn dropdown-item'
                    key='rolesAndFeatures'
                    onClick={() =>
                      this.setState({ selectedDataType: translate('global.rolesfeatures') })
                    }
                  >
                    {translate('global.rolesfeatures')}
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              <Button
                variant='primary'
                className='ml-3'
                disabled={this.state.selectedDataType === ''}
                onClick={() =>
                  this.state.selectedDataType === '' &&
                  this.showToast(translate('superadmin.selectoption'))
                }
              >
                <input
                  type='file'
                  id='file'
                  disabled={this.state.selectedDataType === ''}
                  accept={this.state.selectedDataType === 'Roles & Features' ? '.json' : '.csv'}
                  data-testid='fileInput'
                  className='migrateFileUploadedInput'
                  onChange={(e) => this.handleFileChange(e)}
                />
                <label htmlFor='file' className='uploadButton'>
                  {translate('global.upload')}
                </label>
              </Button>
            </div>
          ) : (
            <>
              <Button className='my-5 float-right' onClick={() => window.location.reload()}>
                {translate('global.uploadnew')}
              </Button>
              {!this.state.isJsonData ? (
                <Table responsive borderless size='sm' className='table-page my-5'>
                  <thead>
                    <tr>
                      <th>{translate('superadmin.userid')}</th>
                      <th>{translate('superadmin.consumerkey')}</th>
                      <th>{translate('superadmin.catid')}</th>
                      <th>{translate('global.name')}</th>
                      {this.state.selectedDataType === 'Template' && (
                        <th>{translate('template.templatetype1')}</th>
                      )}
                      {this.state.selectedDataType === 'Blocks' && (
                        <th>{translate('superadmin.sharedept')}</th>
                      )}
                      <th>{translate('superadmin.url')}</th>
                      <th />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.tableContent.map((content: _Any, idx: number) => {
                      return (
                        <tr key={idx} className='admin-table'>
                          <td> {content.user_id}</td>
                          <td>{content.consumer_key}</td>
                          <td>{content.category_id}</td>
                          <td className='migrateDataTextWrap'>{content.name}</td>
                          {this.state.selectedDataType === 'Template' && (
                            <td>{content.global === '0' ? 'Not global' : 'Global'}</td>
                          )}
                          {this.state.selectedDataType === 'Blocks' && <td>{content.dep_id}</td>}
                          <td className='migrateDataTextWrap'>{content.url}</td>
                          <td>
                            <Button
                              variant='primary'
                              size='sm'
                              className='migrateBtn'
                              onClick={
                                content.migrated
                                  ? undefined
                                  : async () => await this.handleMigrate(content, idx)
                              }
                              disabled={
                                !handleContentCheck(content.user_id, this.state.userIdList) ||
                                content.migrated
                              }
                            >
                              {this.state.loadType === 'REQUESTED' && this.state.index === idx
                                ? 'Loading...'
                                : content.migrated
                                ? 'Migrated'
                                : this.state.btnName}
                            </Button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              ) : (
                <div className='json-table-wrapper mt-4'>
                  <Table responsive size='sm' className='table-page'>
                    <thead>
                      <tr>
                        <th className='align-middle border-bottom-0'>Features</th>
                        {this.state.lmsRoles.map(({ label, role }) => {
                          return (
                            <th className='align-middle border-bottom-0' key={label}>
                              {role}
                            </th>
                          );
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.lmsFeatrues.map((feature, idx) => (
                        <tr key={idx}>
                          <td className='text-capitalize font-weight-bold'>{feature}</td>
                          {this.state.lmsRoles.map(({ features }, idx2) => (
                            <td key={idx2} className='text-center'>
                              <Form.Check
                                type='switch'
                                style={{ marginBottom: 0, marginTop: 0, paddingTop: 5 }}
                                defaultChecked={(features[feature] as _Any)?.value}
                              />
                            </td>
                          ))}
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                  <Button
                    variant='primary'
                    size='sm'
                    className='float-right mt-3'
                    onClick={() => this.migrateRolesAndFeatures()}
                    disabled={this.state.disabledMigrateBtn}
                  >
                    {translate('global.migrate')}
                  </Button>
                </div>
              )}
            </>
          )}
        </div>
        <ToastComponent
          showToast={this.state.showToast}
          toastMessage={this.state.showToastMsg}
          closeToast={this.closeToast}
        />
      </>
    );
  }
}

export default Migrate;
