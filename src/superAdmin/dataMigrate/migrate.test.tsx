import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Migrate from './migrate';
import { shallow } from 'enzyme';
import { StorageMock } from '../../utils/storageMock';

global.sessionStorage = new StorageMock() as _Any;

jest.mock('../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Migrate', () => {
  beforeEach(() => {
    render(
      <BrowserRouter>
        <Migrate />
      </BrowserRouter>,
    );
  });
  describe('migrate component', () => {
    test('componet is present in dom', () => {
      const linkElement = screen.getByText('global.upload');
      expect(linkElement).toBeInTheDocument();
    });
  });
  describe('#upload button', () => {
    test('file upload button', async () => {
      expect(screen.getByTestId('fileInput').className).toBe('migrateFileUploadedInput');
    });
  });
  describe('#Migrate DataOptions', () => {
    const list = ['global.blocks', 'global.template', 'global.rolesfeatures'];
    test('Select dropdown exist with Select text', async () => {
      expect(screen.getByTestId('migrateDropdown')).toHaveTextContent('global.select');
    });
    test.each(list)('Dropdown menu list type exists', async (li) => {
      (screen.getByTestId('migrateDropdown')?.firstChild as HTMLElement).click();
      expect(screen.getByText(li)).toBeInTheDocument();
    });
    test('selected migrate option header rendered', async () => {
      (screen.getByTestId('migrateDropdown')?.firstChild as HTMLElement).click();
      const feature = document.querySelector('.canvas-help-menu')?.childNodes[2] as HTMLElement;
      feature.click();
      expect(screen.getByTestId('migrateHeader')).toHaveTextContent(feature?.textContent as string);
    });
  });
});

describe('#Migrate Roles & Features Functions', () => {
  let wrapper: _Any;
  let instance: _Any;
  const userData = [
    {
      id: 1,
      role: 'Admin',
      label: 'Admin',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
    {
      id: 3,
      role: 'StudentEnrollment',
      label: 'Student',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
    {
      id: 5,
      role: 'Teacher',
      label: 'Teacher',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
    {
      id: 6,
      role: 'Designer',
      label: 'Designer',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
    {
      id: 9,
      role: 'Account Admin',
      label: 'Account admin',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
  ];

  const filteredData = [
    {
      id: 3,
      role: 'StudentEnrollment',
      label: 'Student',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
    {
      id: 5,
      role: 'Teacher',
      label: 'Teacher',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
    {
      id: 6,
      role: 'Designer',
      label: 'Designer',
      features: {
        devicePreview: { label: 'Device Preview', value: true, basicDisabled: false },
        delete: { label: 'Delete', value: true, basicDisabled: true },
        editTable: { label: 'Edit Table', value: true, basicDisabled: false },
        image: { label: 'Image', value: true, basicDisabled: false },
        youtube: { label: 'YouTube', value: true, basicDisabled: false },
      },
    },
  ];

  const concatStructure = [
    { id: 'deleterow', type: 'Advanced' },
    { id: 'deleteelement', type: 'Advanced' },
    { id: 'deletecolumn', type: 'Advanced' },
    { id: 'table', type: 'Advanced' },
    { id: 'tabledesign', type: 'Advanced' },
    { id: 'image', type: 'Advanced' },
    { id: 'imagedesign', type: 'Advanced' },
    { id: 'uploadimage', type: 'Advanced' },
    { id: 'youtube', type: 'Advanced' },
  ];
  beforeAll(() => {
    sessionStorage.setItem('clientId', '123456789');
    wrapper = shallow(<Migrate />);
    instance = wrapper.instance();
    instance.filterRoles = jest.fn().mockReturnValueOnce(filteredData);
    instance.mapV1toV2Features = jest.fn().mockReturnValueOnce(filteredData);
    instance.createLoreeRoles = jest.fn();
  });
  test('migrate features executes all sub functions', () => {
    instance.migrateRolesAndFeatures();
    expect(instance.filterRoles).toHaveBeenCalledTimes(1);
    expect(instance.mapV1toV2Features).toHaveBeenCalledTimes(1);
    expect(instance.createLoreeRoles).toHaveBeenCalledTimes(filteredData.length);
  });
  test('roles are filtered from userData', () => {
    const filteredData = instance.filterRoles(userData);
    expect(filteredData).toEqual(filteredData);
  });
  test.each(filteredData)('features are concated are properly', (data) => {
    const concatFeatures = instance.concatFeatures(data.features);
    expect(concatFeatures).not.toEqual(data.features);
    expect(concatFeatures).toEqual(concatStructure);
  });
  test('Migrate button is diabled after migration', () => {
    expect(wrapper.state('disabledMigrateBtn')).toBe(true);
  });
});
