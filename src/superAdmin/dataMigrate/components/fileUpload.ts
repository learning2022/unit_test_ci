export const isUploadedFileValid = (files: File): Boolean => {
  if (!files) return false;
  let isFileValid = false;
  const fileName = files.name;
  const ext = fileName.split('.').pop();
  const types = ['csv', 'json'];
  isFileValid = types.includes(ext as string);
  return isFileValid;
};
