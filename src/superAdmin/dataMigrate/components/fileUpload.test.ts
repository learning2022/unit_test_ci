import { isUploadedFileValid } from './fileUpload';

describe('#file upload', () => {
  let file: File;
  describe('#csv file', () => {
    beforeEach(() => {
      file = new File(['(⌐□_□)'], 'template-cd.csv');
    });
    test('file is csv', () => {
      expect(isUploadedFileValid(file)).toEqual(true);
    });
  });

  describe('#image file', () => {
    let file: File;
    beforeEach(() => {
      file = new File(['(⌐□_□)'], 'sample.png', { type: 'image/png' });
    });
    test('file is not csv', () => {
      expect(isUploadedFileValid(file)).toEqual(false);
    });
  });

  describe('#json file', () => {
    let file: File;
    beforeEach(() => {
      file = new File(['(⌐□_□)'], 'roles.json', { type: 'application/json' });
    });
    test('file is json', () => {
      expect(isUploadedFileValid(file)).toEqual(true);
    });
  });
});
