import { API, graphqlOperation } from 'aws-amplify';
import {
  CreateCustomBlockMutation,
  ListLtiAccessTokensQuery,
  MigrateUserQuery,
  CreateCustomTemplateMutation,
  ListCustomTemplatesQuery,
} from '../../../API';
import {
  createCustomBlock,
  createSharedBlocks,
  createCustomTemplate,
  createGlobalTemplates,
} from '../../../graphql/mutations';
import config from './../../../aws-exports';
import { listCustomTemplates, listLtiAccessTokens, migrateUser } from '../../../graphql/queries';
import { uploadToS3 } from '../../../lti/admin/globalImagesUpload/uploadImageToS3';
import { UploadedDataInterface } from '../../superAdminInterface';
const { aws_user_files_s3_bucket_region: region, aws_user_files_s3_bucket: bucket } = config;

// For fetching content from s3 url
export const fetchHTMLContent = async (key: string) => {
  try {
    const contentUrl = `https://wysiwyg-page-builder.s3.ap-southeast-2.amazonaws.com/${key}`;
    const content = await fetch(contentUrl);
    return await content.text();
  } catch (error) {
    console.log('Failed to fetch page: ', error);
    throw error;
  }
};

export const getLoreeV2User = async () => {
  const accessTokens = await API.graphql<ListLtiAccessTokensQuery>(
    graphqlOperation(listLtiAccessTokens, { ltiPlatformID: localStorage.getItem('clientId') }),
  );
  return accessTokens?.data?.listLtiAccessTokens;
};

export const handleContentCheck = (userId: string, userIdList: Array<_Any>) => {
  return userIdList.some((data: { user: string }) => data.user === userId);
};

export const createMigrateTemplate = async (
  data: UploadedDataInterface,
  userName: string,
  ownerId: string,
  platformId: string,
) => {
  let content = await fetchHTMLContent(data.url);
  content = await uploadToS3(content, 'My-Templates', platformId);
  const thumb = {
    bucket: bucket,
    key: `${localStorage.getItem('clientId')}/Thumbnail/unnamed.png`,
    region: region,
  };
  const templatePayload: _Any = {
    title: data.name,
    categoryID: '867ecd33-0ac9-4915-98bf-8fadb6fc01e3',
    content: content,
    thumbnail: thumb,
    status: data.status !== '0',
    createdBy: userName,
    isGlobal: data.global !== '0',
    active: data.status !== '0',
    owner: ownerId,
    ltiPlatformID: localStorage.getItem('clientId'),
  };
  const createMigrateTemplate = await API.graphql<CreateCustomTemplateMutation>(
    graphqlOperation(createCustomTemplate, { input: templatePayload }),
  );
  if (data.global === '1') {
    templatePayload.customTemplateID = createMigrateTemplate?.data?.createCustomTemplate?.id;
    templatePayload.content = content;
    delete templatePayload.isGlobal;
    delete templatePayload.owner;
    await API.graphql(graphqlOperation(createGlobalTemplates, { input: templatePayload }));
  }
  return data.global === '1' ? 'Custom and Global' : 'Custom';
};

export const createMigrateBlock = async (
  data: UploadedDataInterface,
  userName: string,
  ownerId: string,
  platformId: string,
) => {
  let content = await fetchHTMLContent(data.url);
  content = await uploadToS3(content, 'My-Blocks', platformId);
  const thumb = {
    bucket: bucket,
    key: `${localStorage.getItem('clientId')}/Thumbnail/unnamed.png`,
    region: region,
  };
  const customBlockInput: _Any = {
    title: data.name,
    categoryID: '867ecd33-0ac9-4915-98bf-8fadb6fc01e3',
    type: 'Elements',
    content: `<div id="custom-block">${content}</div>`,
    thumbnail: thumb,
    status: true,
    createdBy: userName,
    isGlobal: false,
    owner: ownerId,
    active: true,
    isShared: data.shared !== '0',
    ltiPlatformID: localStorage.getItem('clientId'),
  };
  const createMigrateBlock = await API.graphql<CreateCustomBlockMutation>(
    graphqlOperation(createCustomBlock, { input: customBlockInput }),
  );
  if (data.shared === '1') {
    customBlockInput.customBlockID = createMigrateBlock?.data?.createCustomBlock?.id;
    customBlockInput.content = content;
    customBlockInput.sharedAccountId = data.depId;
    delete customBlockInput.isShared;
    delete customBlockInput.isGlobal;
    delete customBlockInput.owner;
    await API.graphql(graphqlOperation(createSharedBlocks, { input: customBlockInput }));
  }
  return data.shared === '1' ? 'Custom and Shared' : 'Custom';
};

export const isUserFound = async (userId: string) => {
  const userToken = await getLoreeV2User();
  for (const userMatch of userToken?.items ?? []) {
    if (userId === userMatch?.user) {
      const userDetail = await API.graphql<MigrateUserQuery>(
        graphqlOperation(migrateUser, {
          email: userMatch?.loreeUsername,
        }),
      );
      return userDetail;
    }
  }
};
export const listCustomTemplatesQuery = async () => {
  const templateData = await API.graphql<ListCustomTemplatesQuery>(
    graphqlOperation(listCustomTemplates, { platformId: localStorage.getItem('clientId') }),
  );
  return templateData.data?.listCustomTemplates?.items ?? [];
};

export const isTemplateMigrated = async (title: string) => {
  const customTemplates = await listCustomTemplatesQuery();
  return customTemplates.some((i: _Any) => i.title.toLowerCase() === title.toLowerCase());
};
