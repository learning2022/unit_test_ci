import API from '@aws-amplify/api';
import {
  getLoreeV2User,
  fetchHTMLContent,
  handleContentCheck,
  listCustomTemplatesQuery,
  isTemplateMigrated,
} from './action';
import { accessToken, templateData } from './migrateMockData';

describe('#dataMigrateData', () => {
  describe('#gettingLoreeV2User', () => {
    beforeEach(() => {
      const data = { data: { listLtiAccessTokens: accessToken } };
      API.graphql = jest.fn().mockImplementation(() => data);
    });
    test('loreev2 user count', async () => {
      const userData = (await getLoreeV2User()) as any;
      expect(userData.items.length).toEqual(3);
    });
  });

  describe('#Fileupload', () => {
    test('html file returns', async () => {
      expect(await fetchHTMLContent('blocks/BUCVDWFYGZH3K4M5P7Q8RATBUC/test.html')).toEqual(
        '<p id="ihsw54h" style="box-sizing: border-box; padding: 20px; text-align: left; font-family: Helvetica; font: none;">Insert Text Here.</p>',
      );
    }, 15000);
  });

  describe('#checkUser', () => {
    test('user found', async () => {
      expect(handleContentCheck('27', accessToken.items)).toEqual(true);
    });
    test('user not found', async () => {
      expect(handleContentCheck('97', accessToken.items)).toEqual(false);
    });
  });

  describe('#gettingLoreeV2Templates', () => {
    beforeEach(() => {
      const data = { data: { listCustomTemplates: templateData } };
      API.graphql = jest.fn().mockImplementation(() => data);
    });
    test('loreev2 template list', async () => {
      const templateData = await listCustomTemplatesQuery();
      expect(templateData.length).toEqual(2);
    });
  });
  describe('#checkingTemplateIsMigrated', () => {
    beforeEach(() => {
      const data = { data: { listCustomTemplates: templateData } };
      API.graphql = jest.fn().mockImplementation(() => data);
    });
    test('template already migrated', async () => {
      const templateData = await isTemplateMigrated('test-8');
      expect(templateData).toEqual(true);
    });
    test('template not migrated', async () => {
      const templateData = await isTemplateMigrated('test-10');
      expect(templateData).toEqual(false);
    });
  });
});
