/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import {
  createCustomStyle,
  createLoreeFeature,
  createLoreeRole,
  createLtiApiKey,
  createLtiPlatform,
  createLtiPlatformKey,
  updateLtiApiKey,
  updateLtiPlatform,
} from '../graphql/mutations';
import ToastComponent from '../lti/components/ToastComponent';
import { refreshPlatformKeys } from '../graphql/queries';
import { translate } from '../i18n/translate';

interface OnBoardProps {
  showModal: boolean;
  closeModal: any;
  modalType: string;
  selectedClientId: any;
  selectedAPIData: any;
  clientList: any;
}
class OnBoardModal extends React.Component<OnBoardProps> {
  state = {
    platformId: '',
    platformName: '',
    clientId: '',
    domainName: '',
    GAtrackingId: '',
    saveBtnActiveState: true,
    apiClientId: '',
    apiSecretKey: '',
    organisationUrl: '',
    clientSecret: '',
    showToast: false,
    toastMessage: '',
    platformList: this.props.clientList,
    btnStatus: false,
  };

  handleUserInput = (e: any) => {
    const name = e.target.name;
    const value = e.target.value;
    if (value !== '') {
      this.setState({ saveBtnActiveState: false });
    }
    this.setState({
      [name]: value,
    });
    console.log(name, value);
  };

  handleCreate = async () => {
    this.setState({ btnStatus: true });
    const inputVal = {
      accesstokenEndpoint: `${this.state.domainName}/login/oauth2/auth`,
      authConfig: { key: `${this.state.domainName}/api/lti/security/jwks`, method: 'JWK_SET' },
      platformName: this.state.platformName,
      clientId: this.state.clientId,
      googleAnalyticsTrackingId: this.state.GAtrackingId,
      platformUrl: this.state.domainName,
      authEndpoint: `${this.state.domainName}/api/lti/authorize_redirect`,
    };
    const createLTI: any = await API.graphql(
      graphqlOperation(createLtiPlatform, { input: inputVal }),
    );
    const APIKeyInput = {
      apiClientId: this.state.apiClientId,
      apiSecretKey: this.state.apiSecretKey,
      ltiClientID: this.state.clientId,
      ltiPlatformID: createLTI.data.createLtiPlatform.id,
      oauthLoginUrl: `${this.state.organisationUrl}/login/oauth2/auth`,
      oauthTokenUrl: `${this.state.organisationUrl}/login/oauth2/token`,
    };
    const createAPI: any = await API.graphql(
      graphqlOperation(createLtiApiKey, { input: APIKeyInput }),
    );
    const platformKeyInput = {
      ltiPlatformID: createLTI.data.createLtiPlatform.id,
    };
    const createLtiPlatformKeyTable: any = await API.graphql(
      graphqlOperation(createLtiPlatformKey, { input: platformKeyInput }),
    );
    const loreeFeatureInput = {
      featureList:
        '[{"featureList":[{"id":"outline","label":"Outline"},{"id":"undoredo","label":"Undo/Redo"},{"id":"codeproperties","label":"Code Properties"},{"id":"preview","label":"Preview"},{"id":"accessibilitychecker","label":"Accessibility checker"},{"id":"textblock","label":"Text Block"},{"id":"fontstyles","label":"Font styles"}],"name":"Basic"},{"featureList":[{"id":"duplicaterow","label":"Duplicate row"},{"id":"deleterow","label":"Delete row"},{"id":"changerow","label":"Change row"},{"id":"marginrow","label":"Margin row"},{"id":"paddingrow","label":"Padding row"},{"id":"backgroundcoloroftherow","label":"Background color of the row"},{"id":"rowstructure","label":"Row structure"},{"id":"moverow","label":"Move row"},{"id":"copyandpaste","label":"copy and paste"},{"id":"deletecolumn","label":"Delete column"},{"id":"changecolumn","label":"Change column"},{"id":"margincolumn","label":"Margin - column"},{"id":"paddingcolumn","label":"Padding - column"},{"id":"backgroundcolorofthecolumn","label":"Background color of the column"},{"id":"movecolumn","label":"Move column"},{"id":"duplicateelement","label":"Duplicate - Element"},{"id":"deleteelement","label":"Delete - Element"},{"id":"moveelement","label":"Move - Element"},{"id":"image","label":"Image"},{"id":"banner","label":"Banner"},{"id":"uploadimage","label":"Upload image"},{"id":"insertlink","label":"Insert Link"},{"id":"imagedesign","label":"Image - Design"},{"id":"video","label":"Video"},{"id":"uploadnewvideo","label":"Upload new video"},{"id":"youtube","label":"Youtube"},{"id":"vimeo","label":"Vimeo"},{"id":"insertbyurl","label":"Insert by URL"},{"id":"videodesign","label":"Video - Design"},{"id":"divider","label":"Divider"},{"id":"dividerdesign","label":"Divider - Design"},{"id":"table","label":"Table"},{"id":"tabledesign","label":"Table - Design"}, {"id":"specialblocks","label":"Special Blocks"}],"name":"Advanced"},{"featureList":[{"id":"customrowfiltersearch","label":"Custom Row (Filter   Search)"},{"id":"customelementsfiltersearch","label":"Custom Elements (Filter   Search)"},{"id":"templatesfiltersearch","label":"Templates (Filter   Search)"},{"id":"saveastemplate","label":"Save as template"},{"id":"saveascustomrow","label":"Save as custom row"},{"id":"saveascustomelement","label":"Save as custom element"}],"name":"Custom Blocks"},{"featureList":[{"id":"myinteractive","label":"My Interactives"}],"name":"Interactives"},{"featureList":[{"id":"h5p","label":"H5P"}],"name":"H5P"}]',
      ltiPlatformID: createLTI.data.createLtiPlatform.id,
    };
    const customStyleInput = {
      ltiPlatformID: createLTI.data.createLtiPlatform.id,
      customLink:
        '[{"color":"#b01a8b","text-decoration":"Overline","text-decoration-style":"Dotted","text-style":"Italic","font":"Arial"}]',
      customHeader:
        '[{"h1":{"size":"66px","font":"Arial"}},{"h2":{"size":"54px","font":"Arial"}},{"h3":{"size":"46px","font":"Arial"}},{"h4":{"size":"34px","font":"SourceSansPro"}},{"h5":{"size":"28px","font":"Arial"}},{"h6":{"size":"22px","font":"Arial"}},{"paragraph":{"size":"20px","font":"SourceSansPro"}}]',
      customColor: '[{"color":"#1c29b2"}]',
      customFont:
        '[{"fontFamily":"ABeeZee, sans-serif","name":"ABeeZee","url":"https://fonts.googleapis.com/css2?family=ABeeZee"}]',
    };
    const customStyleVal: any = await API.graphql(
      graphqlOperation(createCustomStyle, { input: customStyleInput }),
    );
    const createLoreeFeat: any = await API.graphql(
      graphqlOperation(createLoreeFeature, { input: loreeFeatureInput }),
    );
    const roles = ['Teacher', 'Designer'];
    for (const data of roles) {
      const roleInput = {
        featureList:
          '[{"id":"outline","type":"Basic"},{"id":"undoredo","type":"Basic"},{"id":"codeproperties","type":"Basic"},{"id":"preview","type":"Basic"},{"id":"textblock","type":"Basic"},{"id":"fontstyles","type":"Basic"},{"id":"duplicaterow","type":"Advanced"},{"id":"deleterow","type":"Advanced"},{"id":"changerow","type":"Advanced"},{"id":"marginrow","type":"Advanced"},{"id":"paddingrow","type":"Advanced"},{"id":"backgroundcoloroftherow","type":"Advanced"},{"id":"rowstructure","type":"Advanced"},{"id":"moverow","type":"Advanced"},{"id":"copyandpaste","type":"Advanced"},{"id":"deletecolumn","type":"Advanced"},{"id":"changecolumn","type":"Advanced"},{"id":"margincolumn","type":"Advanced"},{"id":"paddingcolumn","type":"Advanced"},{"id":"backgroundcolorofthecolumn","type":"Advanced"},{"id":"movecolumn","type":"Advanced"},{"id":"duplicateelement","type":"Advanced"},{"id":"deleteelement","type":"Advanced"},{"id":"moveelement","type":"Advanced"},{"id":"banner","type":"Advanced"},{"id":"uploadimage","type":"Advanced"},{"id":"insertlink","type":"Advanced"},{"id":"imagedesign","type":"Advanced"},{"id":"video","type":"Advanced"},{"id":"uploadnewvideo","type":"Advanced"},{"id":"vimeo","type":"Advanced"},{"id":"insertbyurl","type":"Advanced"},{"id":"videodesign","type":"Advanced"},{"id":"divider","type":"Advanced"},{"id":"dividerdesign","type":"Advanced"},{"id":"table","type":"Advanced"},{"id":"tabledesign","type":"Advanced"},{"id":"specialblocks","type":"Advanced"},{"id":"customrowfiltersearch","type":"Custom Blocks"},{"id":"customelementsfiltersearch","type":"Custom Blocks"},{"id":"templatesfiltersearch","type":"Custom Blocks"},{"id":"saveastemplate","type":"Custom Blocks"},{"id":"saveascustomrow","type":"Custom Blocks"},{"id":"saveascustomelement","type":"Custom Blocks"},{"id":"myinteractive","type":"Interactives"},{"id":"image","type":"Advanced"},{"id":"youtube","type":"Advanced"},{"id":"h5p","type":"H5P"}]',
        name: data,
        ltiPlatformID: createLTI.data.createLtiPlatform.id,
      };
      const loreeRole: any = await API.graphql(
        graphqlOperation(createLoreeRole, { input: roleInput }),
      );
      console.log('loreerole', loreeRole);
    }
    const refreshPlatformKey: any = await API.graphql(
      graphqlOperation(refreshPlatformKeys, { platformId: createLTI.data.createLtiPlatform.id }),
    );
    console.log(refreshPlatformKey);
    console.log('LTI', createLTI);
    console.log('API', createAPI);
    console.log('Key tabe', createLtiPlatformKeyTable);
    console.log('Feature', createLoreeFeat);
    console.log('CusomStyle', customStyleVal);
    this.props.closeModal();
    this.setState({
      showToast: true,
      toastMessage: translate('platform.updatesuccess'),
      btnStatus: false,
    });
  };

  handleUpdate = async () => {
    this.setState({ btnStatus: true });
    const inputVal = {
      id: this.props.selectedClientId.id,
      accesstokenEndpoint: this.state.domainName
        ? `${this.state.domainName}/login/oauth2/auth`
        : this.props.selectedClientId.accesstokenEndpoint,
      authConfig: this.props.selectedClientId.authConfig,
      platformName: this.state.platformName
        ? this.state.platformName
        : this.props.selectedClientId.platformName,
      clientId: this.state.clientId ? this.state.clientId : this.props.selectedClientId.clientId,
      googleAnalyticsTrackingId: this.state.GAtrackingId
        ? this.state.GAtrackingId
        : this.props.selectedClientId.googleAnalyticsTrackingId,
      platformUrl: this.state.domainName
        ? this.state.domainName
        : this.props.selectedClientId.platformUrl,
    };
    const updateLTI: any = await API.graphql(
      graphqlOperation(updateLtiPlatform, { input: inputVal }),
    );
    const APIKeyInput = {
      id: this.props.selectedAPIData.id,
      apiClientId: this.state.apiClientId
        ? this.state.apiClientId
        : this.props.selectedAPIData.apiClientId,
      apiSecretKey: this.state.apiSecretKey
        ? this.state.apiSecretKey
        : this.props.selectedAPIData.apiSecretKey,
      ltiClientID: this.state.clientId
        ? this.state.clientId
        : this.props.selectedAPIData.ltiClientID,
      ltiPlatformID: this.props.selectedAPIData.ltiPlatformID,
      oauthLoginUrl: this.state.organisationUrl
        ? `${this.state.organisationUrl}/login/oauth2/auth`
        : this.props.selectedAPIData.oauthLoginUrl,
      oauthTokenUrl: this.state.organisationUrl
        ? `${this.state.organisationUrl}/login/oauth2/token`
        : this.props.selectedAPIData.oauthTokenUrl,
    };
    const updateAPI: any = await API.graphql(
      graphqlOperation(updateLtiApiKey, { input: APIKeyInput }),
    );
    console.log(updateLTI);
    console.log(updateAPI);
    this.props.closeModal();
    this.setState({
      showToast: true,
      toastMessage: translate('platform.updatesuccess'),
      btnStatus: false,
    });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  render() {
    return (
      <>
        <Modal
          centered
          show={this.props.showModal}
          onHide={this.props.closeModal}
          backdrop='static'
          keyboard={false}
          size='lg'
        >
          <Modal.Header closeButton>
            <Modal.Title>
              <h5 className='text-primary'>{translate('platform.onboardclient')}</h5>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.props.modalType === 'Edit' && (
              <Form.Group>
                <Form.Label>{translate('platform.platformid')}</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder={translate('platform.platformid')}
                  autoComplete='off'
                  defaultValue={
                    this.props.selectedClientId.id ? this.props.selectedClientId.id : ''
                  }
                  maxLength={100}
                  readOnly
                />
              </Form.Group>
            )}
            <Form.Group>
              <Form.Label>{translate('global.name')}</Form.Label>
              <Form.Control
                required
                type='text'
                placeholder='Name'
                name={translate('global.name')}
                autoComplete='off'
                defaultValue={
                  this.props.selectedClientId.platformName
                    ? this.props.selectedClientId.platformName
                    : ''
                }
                maxLength={100}
                onChange={this.handleUserInput}
              />
            </Form.Group>
            <Row>
              <Col xs={6}>
                <Form.Group>
                  <Form.Label>{translate('platform.lticlientid')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    placeholder={translate('platform.lticlientid')}
                    autoComplete='off'
                    name='clientId'
                    defaultValue={
                      this.props.selectedClientId.clientId
                        ? this.props.selectedClientId.clientId
                        : ''
                    }
                    maxLength={100}
                    onChange={this.handleUserInput}
                  />
                </Form.Group>
              </Col>
              <Col xs={6}>
                <Form.Group>
                  <Form.Label>{translate('platform.ltisecretkey')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    placeholder={translate('platform.ltisecretkey')}
                    name='clientSecret'
                    autoComplete='off'
                    defaultValue={
                      this.props.selectedClientId.clientSecret
                        ? this.props.selectedClientId.clientSecret
                        : ''
                    }
                    maxLength={100}
                    onChange={this.handleUserInput}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <Form.Group>
                  <Form.Label>{translate('platform.apiclientid')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    name='apiClientId'
                    placeholder={translate('platform.apiclientid')}
                    autoComplete='off'
                    defaultValue={
                      this.props.selectedAPIData.apiClientId
                        ? this.props.selectedAPIData.apiClientId
                        : ''
                    }
                    maxLength={100}
                    onChange={this.handleUserInput}
                  />
                </Form.Group>
              </Col>
              <Col xs={6}>
                <Form.Group>
                  <Form.Label>{translate('platform.apisecretkey')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    name='apiSecretKey'
                    placeholder={translate('platform.apisecretkey')}
                    autoComplete='off'
                    defaultValue={
                      this.props.selectedAPIData.apiSecretKey
                        ? this.props.selectedAPIData.apiSecretKey
                        : ''
                    }
                    maxLength={100}
                    onChange={this.handleUserInput}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col xs={6}>
                <Form.Group>
                  <Form.Label>{translate('platform.domainname')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    placeholder={translate('platform.domain')}
                    name='domainName'
                    autoComplete='off'
                    defaultValue={
                      this.props.selectedClientId.platformUrl
                        ? this.props.selectedClientId.platformUrl
                        : ''
                    }
                    maxLength={100}
                    onChange={this.handleUserInput}
                  />
                </Form.Group>
              </Col>
              <Col xs={6}>
                <Form.Group>
                  <Form.Label>{translate('platform.orgurl')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    placeholder={translate('platform.orgurl')}
                    autoComplete='off'
                    name='organisationUrl'
                    defaultValue={
                      this.props.selectedAPIData.oauthLoginUrl
                        ? this.props.selectedAPIData.oauthLoginUrl.split('/login/oauth2/auth')[0]
                        : ''
                    }
                    maxLength={100}
                    onChange={this.handleUserInput}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group>
              <Form.Label>{translate('platform.gatrackingid')}</Form.Label>
              <Form.Control
                required
                type='text'
                placeholder={translate('platform.gatrackingid')}
                name='GAtrackingId'
                autoComplete='off'
                defaultValue={
                  this.props.selectedClientId.googleAnalyticsTrackingId
                    ? this.props.selectedClientId.googleAnalyticsTrackingId
                    : ''
                }
                maxLength={100}
                onChange={this.handleUserInput}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer className='d-flex justify-content-center'>
            <Button variant='secondary' onClick={this.props.closeModal}>
              {translate('global.close')}
            </Button>
            <Button
              variant='primary'
              onClick={
                this.props.modalType === 'Create'
                  ? async () => await this.handleCreate()
                  : async () => await this.handleUpdate()
              }
              disabled={
                this.state.saveBtnActiveState ? this.state.saveBtnActiveState : this.state.btnStatus
              }
            >
              {this.props.modalType === 'Create'
                ? this.state.btnStatus
                  ? translate('global.creating')
                  : translate('global.create')
                : this.state.btnStatus
                ? translate('global.updatingdot')
                : translate('global.update')}
            </Button>
          </Modal.Footer>
        </Modal>
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
      </>
    );
  }
}

export default OnBoardModal;
