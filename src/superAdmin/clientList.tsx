/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { listLtiApiKeys, listLtiPlatforms } from '../../src/graphql/queries';
import { Table, Row, Col, Button, Container } from 'react-bootstrap';
import LeftSidenav from '../components/dashboardSideNavbar';
import Loading from '../components/loader/loading';
import OnBoardModal from './onBoardModal';
import GlobalStatistics from './globalStatistics';

class ClientList extends React.Component {
  state = {
    platformList: [],
    loading: true,
    onBoardModal: false,
    modalType: '',
    selectedClientId: '',
    selectedApiData: '',
    clientsCount: 0,
  };

  async componentDidMount() {
    const ltiPlatforms: any = await API.graphql(graphqlOperation(listLtiPlatforms));
    this.setState({
      platformList: ltiPlatforms.data.listLtiPlatforms.items,
      loading: false,
      clientsCount: ltiPlatforms.data.listLtiPlatforms.items.length,
    });
    localStorage.setItem('clientId', '');
  }

  handleOnboardModal = async (modalType: any, clientId: any) => {
    if (modalType === 'Edit') {
      const APIKeyData: any = await API.graphql(
        graphqlOperation(listLtiApiKeys, {
          filter: {
            ltiPlatformID: {
              eq: clientId.id,
            },
          },
        }),
      );
      this.setState({
        selectedApiData: APIKeyData.data.listLtiApiKeys.items[0],
      });
    }
    this.setState({
      onBoardModal: true,
      modalType: modalType,
      selectedClientId: clientId,
    });
  };

  handleCloseModal = () => {
    this.setState({ onBoardModal: false });
  };

  render() {
    return (
      <Fragment>
        <LeftSidenav />
        <Container fluid={true} className='recent-section'>
          <Row>
            <Col>
              <h3 className='mt-5 text-primary'>All Clients</h3>
              <GlobalStatistics clientsCount={this.state.clientsCount} />
              <br></br>
              <Button onClick={() => this.handleOnboardModal('Create', '')} className='float-right mr-3 mb-3' disabled>
                On-Board Client
              </Button>
              {/* it's temporary code for enabling featurelist in standalone , once we enable payment option, it flow will be changed */}
              <Link
                to={`/superadmin/STANDALONE/features`}
                onClick={() => localStorage.setItem('clientId', 'STANDALONE')}
                className=' float-right btn btn-primary mr-3 mb-3'
              >
                Loree standalone
              </Link>

              {this.state.loading ? (
                <Loading />
              ) : (this.state.platformList as Array<any>).length > 0 ? (
                <Table responsive striped hover>
                  <thead className='bg-dark text-white'>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Client ID</th>
                      <th>LMS</th>
                      <th>GA tracking Id</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {(this.state.platformList as Array<any>).map((client: any, index: any) => (
                      <tr key={index}>
                        <td>
                          <Link
                            to={`/superadmin/${client.id}/features`}
                            onClick={() => localStorage.setItem('clientId', client.id)}
                            className='btn btn-link'
                          >
                            {client.id}
                          </Link>
                        </td>
                        <td>{client.platformName}</td>
                        <td>{client.clientId}</td>
                        <td>{client.platformUrl}</td>
                        <td>{client.googleAnalyticsTrackingId}</td>
                        <td>
                          <Button onClick={() => this.handleOnboardModal('Edit', client)} disabled size='sm'>
                            Edit
                          </Button>
                        </td>
                      </tr>
                    ))}
                    <OnBoardModal
                      showModal={this.state.onBoardModal}
                      closeModal={this.handleCloseModal}
                      modalType={this.state.modalType}
                      selectedClientId={this.state.selectedClientId}
                      selectedAPIData={this.state.selectedApiData}
                      clientList={this.state.platformList}
                    />
                  </tbody>
                </Table>
              ) : (
                <div className='container'>
                  <div className='p-3'>No record found</div>
                </div>
              )}
            </Col>
          </Row>
        </Container>
      </Fragment>
    );
  }
}

export default ClientList;
