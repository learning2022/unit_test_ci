import React from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { updateLoreeFeature } from '../graphql/mutations';
import { listLoreeFeatures } from '../graphql/queries';
import LeftSidenav from '../components/dashboardSideNavbar';
import Loading from '../components/loader/loading';
import ConfrimModal from './confirmModal';
import ToastComponent from '../lti/components/ToastComponent';
import { Accordion, Button, Card, Container, Form, Alert } from 'react-bootstrap';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ListLoreeFeaturesQuery, LoreeFeature } from '../API';
import { translate } from '../i18n/translate';

interface Category {
  name: string;
  featureList: { label: string }[];
  [p: string]: unknown;
}

interface EditorProps {
  match: {
    path: '';
    params: {
      id: string;
    };
  };
}

interface State {
  featureList?: LoreeFeature | null;
  loading: boolean;
  activeSubAccordion: number;
  selectedFeatureList: (LoreeFeature | null)[];
  updatedFeatureList: string;
  showModal: boolean;
  showToast: boolean;
  toastMessage: string;
  btnEnable: boolean;
}

class FeatureList extends React.Component<EditorProps, State> {
  state: State = {
    featureList: null,
    loading: true,
    activeSubAccordion: 0,
    selectedFeatureList: [],
    updatedFeatureList: '',
    showModal: false,
    showToast: false,
    toastMessage: '',
    btnEnable: true,
  };

  async componentDidMount() {
    try {
      const features = await API.graphql<ListLoreeFeaturesQuery>(
        graphqlOperation(listLoreeFeatures, {
          filter: {
            ltiPlatformID: {
              eq: 'DEFAULT',
            },
          },
        }),
      );
      const selectedFeatures = await API.graphql<ListLoreeFeaturesQuery>(
        graphqlOperation(listLoreeFeatures, {
          filter: {
            ltiPlatformID: { eq: this.props.match.params.id },
          },
        }),
      );
      this.setState({
        loading: false,
        featureList: features.data?.listLoreeFeatures?.items?.[0],
        selectedFeatureList: selectedFeatures.data?.listLoreeFeatures?.items ?? [],
        updatedFeatureList: selectedFeatures.data?.listLoreeFeatures?.items?.[0]?.featureList ?? '',
      });
    } catch (err) {
      console.error('Error fetching featureList in SuperAdmin. Error:', err);
    }
  }

  // Handle select all sub features on click main feature
  saveFeature = async () => {
    const checkedElementFeatures = [];
    const selectedFeature = [];
    const defaultFeatures = this.state.featureList;
    const checkedElement = document.getElementsByClassName('loree-feature');
    for (const checkedFeature of checkedElement) {
      const firstChildNode = checkedFeature.childNodes[0] as HTMLElement | HTMLInputElement;
      if ('checked' in firstChildNode && firstChildNode.checked) {
        checkedElementFeatures.push(firstChildNode.id);
      }
    }
    for (const checkData of JSON.parse(defaultFeatures?.featureList ?? '')) {
      if (checkedElementFeatures.includes(checkData.name)) {
        selectedFeature.push(checkData);
      }
    }
    const inputValues = {
      id: this.state.selectedFeatureList[0]?.id,
      ltiPlatformID: this.props.match.params.id,
      featureList: JSON.stringify(selectedFeature),
    };
    try {
      await API.graphql(graphqlOperation(updateLoreeFeature, { input: inputValues }));
      this.setState({
        showModal: false,
        showToast: true,
        toastMessage: translate('superadmin.featureupdatesuccess'),
      });
    } catch (err) {
      this.setState({
        showModal: false,
        showToast: true,
        toastMessage: translate('superadmin.featureupdatefail'),
      });
    }
  };

  // For default check
  handleTypeChecked = (categoryName: string) => {
    const selectedFeatureName = [];
    const UpdatedList = this.state.updatedFeatureList;
    for (const updatedFeature of JSON.parse(UpdatedList)) {
      selectedFeatureName.push(updatedFeature.name);
    }
    return selectedFeatureName.includes(categoryName);
  };

  // Handle Accordion arrow direction
  handleSubAccordion = (index: number): void => {
    const active = this.state.activeSubAccordion === index ? -1 : index;
    this.setState({ activeSubAccordion: active });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  getCategories(): Category[] {
    return JSON.parse(this.state.featureList?.featureList ?? '');
  }

  render() {
    return (
      <>
        <LeftSidenav superAdminSidebar />
        <Container fluid className='recent-section'>
          <h3 className='mt-5 text-primary'>{translate('superadmin.featurelist')}</h3>
          <Alert variant='info'>
            <Alert.Heading>{translate('superadmin.managefeatures')}</Alert.Heading>
            <p className='mb-0'>{translate('superadmin.onlyenabled')}</p>
            <p>{translate('superadmin.carefulupdating')}</p>
          </Alert>
          {this.state.loading ? (
            <Loading />
          ) : (
            <>
              <Accordion defaultActiveKey='0' className='border-bottom mb-4'>
                {this.getCategories().map((category, idx) => (
                  <Card className='border-0 mb-0' key={idx}>
                    <Card.Header className='border-top bg-white pt-1 pb-1 border-bottom-0'>
                      <div className='d-inline-block ml-4'>
                        <Form.Check
                          type='checkbox'
                          id={`${category.name}`}
                          className='loree-feature font-weight-bold'
                          defaultChecked={this.handleTypeChecked(category.name)}
                          onChange={() => this.setState({ btnEnable: false })}
                        />
                      </div>
                      <div className='d-inline-block'>
                        <Accordion.Toggle
                          as={Button}
                          variant='link'
                          eventKey={`${idx}`}
                          className='pt-2 pb-2 mb-1 text-decoration-none'
                          id='feature-accordion-toggle'
                          onClick={() => this.handleSubAccordion(idx)}
                        >
                          <div className='d-inline-block mr-2' style={{ marginLeft: '-50px' }}>
                            <FontAwesomeIcon
                              icon={
                                this.state.activeSubAccordion === idx ? faAngleDown : faAngleRight
                              }
                              className='float-right text-dark'
                            />
                          </div>
                          <span className='ml-4'>{category.name}</span>
                        </Accordion.Toggle>
                      </div>
                    </Card.Header>
                    <Accordion.Collapse eventKey={`${idx}`}>
                      <Card.Body className='pt-2 pb-2'>
                        <ul style={{ columns: '3' }} className='list-unstyled pl-3 pt-2'>
                          {category.featureList.map((feature, fIdx: number) => (
                            <li key={fIdx}>-{feature.label}</li>
                          ))}
                        </ul>
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                ))}
              </Accordion>
              <Button
                variant='primary'
                onClick={() => {
                  this.setState({ showModal: true });
                }}
                disabled={this.state.btnEnable}
              >
                {translate('superadmin.updatefeatures')}
              </Button>
            </>
          )}
        </Container>

        <ConfrimModal
          showModal={this.state.showModal}
          closeModal={this.closeModal}
          saveFeature={this.saveFeature}
        />
        <ToastComponent
          showToast={this.state.showToast}
          toastMessage={this.state.toastMessage}
          closeToast={this.closeToast}
        />
      </>
    );
  }
}
export default FeatureList;
