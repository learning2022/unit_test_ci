/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Button, Modal } from 'react-bootstrap';

interface ConfrimFeaturesProps {
  showModal: any;
  closeModal: any;
  saveFeature: any;
}

class ConfirmFeatures extends React.Component<ConfrimFeaturesProps> {
  render() {
    return (
      <Modal show={this.props.showModal} onHide={this.props.closeModal} backdrop='static' keyboard={false} centered>
        <Modal.Header closeButton>
          <Modal.Title>
            <h5 className='text-primary'>Confirm</h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure, you want to update this?</p>
          <p>It will take effect immediately to clients.</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={this.props.closeModal}>
            Cancel
          </Button>
          <Button variant='primary' onClick={this.props.saveFeature}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
export default ConfirmFeatures;
