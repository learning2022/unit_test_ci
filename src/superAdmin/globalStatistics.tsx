/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import { listLtiAccessTokens, listCustomTemplates, listCustomBlocks } from '../graphql/queries';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';

interface StatisticsProps {
  clientsCount: number;
}

class GlobalStatistics extends React.Component<StatisticsProps> {
  state = {
    usersCount: 0,
    customTemplateCount: 0,
    customBlockCount: 0,
    loading: true,
  };

  async componentDidMount() {
    try {
      const accessTokens: any = await API.graphql(graphqlOperation(listLtiAccessTokens));
      const customTemplates: any = await API.graphql(graphqlOperation(listCustomTemplates));
      const customBlocks: any = await API.graphql(graphqlOperation(listCustomBlocks));
      this.setState({
        loading: false,
        usersCount: accessTokens.data.listLtiAccessTokens.items.length,
        customTemplateCount: customTemplates.data.listCustomTemplates.items.length,
        customBlockCount: customBlocks.data.listCustomBlocks.items.length,
      });
      console.log('Users:', accessTokens.data.listLtiAccessTokens);
      console.log('customTemplates:', customTemplates.data.listCustomTemplates);
      console.log('customBlocks:', accessTokens.data.listLtiAccessTokens);
    } catch (err) {
      console.error('Error:', err);
    }
  }

  render() {
    return (
      <Fragment>
        <Row className='mt-3'>
          <Col xs={3}>
            <Card className='shadow  mb-5 bg-white rounded'>
              <Card.Body>
                <h6 className='text-secondary'>Clients</h6>
                <Card.Title className='h5'>{this.props.clientsCount}</Card.Title>
                <p className='mt-3 mb-0 text-muted text-sm'>
                  <span className='text-success mr-2'>
                    <FontAwesomeIcon icon={faArrowUp} /> 2
                  </span>
                  <span className='text-nowrap'>Since last month</span>
                </p>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={3}>
            <Card className='shadow mb-5 bg-white rounded'>
              <Card.Body>
                <h6 className='text-secondary'>Users</h6>
                <Card.Title className='h5'>{this.state.usersCount}</Card.Title>
                <p className='mt-3 mb-0 text-muted text-sm'>
                  <span className='text-success mr-2'>
                    <FontAwesomeIcon icon={faArrowUp} /> 3.48%
                  </span>
                  <span className='text-nowrap'>Since last month</span>
                </p>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={3}>
            <Card className='shadow mb-5 bg-white rounded'>
              <Card.Body>
                <h6 className='text-secondary'>Custom Templates</h6>
                <Card.Title className='h5'>{this.state.customTemplateCount}</Card.Title>
                <p className='mt-3 mb-0 text-muted text-sm'>
                  <span className='text-danger mr-2'>
                    <FontAwesomeIcon icon={faArrowDown} /> 3.48%
                  </span>
                  <span className='text-nowrap'>Since last month</span>
                </p>
              </Card.Body>
            </Card>
          </Col>
          <Col xs={3}>
            <Card className='shadow mb-5 bg-white rounded'>
              <Card.Body>
                <h6 className='text-secondary'>Custom Blocks</h6>
                <Card.Title className='h5'>{this.state.customBlockCount}</Card.Title>
                <p className='mt-3 mb-0 text-muted text-sm'>
                  <span className='text-success mr-2'>
                    <FontAwesomeIcon icon={faArrowUp} /> 3.48%
                  </span>
                  <span className='text-nowrap'>Since last month</span>
                </p>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Fragment>
    );
  }
}
export default GlobalStatistics;
