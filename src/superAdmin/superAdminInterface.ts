export interface UploadedDataInterface {
  user_id: string;
  consumerKey: string;
  categoryId: string;
  name: string;
  global: string;
  depId: string;
  url: string;
  migrated: string;
  status?: string;
  shared?: string;
}
