import React, { Suspense, useEffect, useState } from 'react';
import { i18n } from 'i18next';
import { I18nextProvider } from 'react-i18next';
import loreei18n from './i18n/i18n';
import './assets/stylesheets/App.scss';
import { Container } from 'react-bootstrap';
import Loading from './components/loader/loading';

const Routing = React.lazy(async () => await import('./router/Router'));

function Loader() {
  return (
    <Container>
      <Loading />
      <p className='loaderViewText mt-2' />
    </Container>
  );
}

function App() {
  const [i18nValue, setI18nValue] = useState<i18n | null>(null);

  useEffect(() => {
    loreei18n
      .then((i) => {
        setI18nValue(i);
      })
      .catch((e) => {
        throw new Error(e);
      });
  }, []);

  if (i18nValue === null) {
    return <Loader />;
  }

  return (
    <>
      <I18nextProvider i18n={i18nValue}>
        <Suspense fallback={<Loader />}>
          <Routing />
        </Suspense>
      </I18nextProvider>
    </>
  );
}

export default App;
