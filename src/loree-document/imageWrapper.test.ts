import CONSTANTS from '../loree-editor/constant';
import { ensure } from '../utils/generalUtils';
import {
  addLinkToImageInsideWrapper,
  getImageInsideWrapper,
  getImageLinkFromWrapper,
  getImageWrapperFromImage,
  isImageWrapper,
  removeLinkFromImageInWrapper,
  wrapImage,
} from './imageWrapper';

describe('ImageWrapperUtil Tests', () => {
  describe('isImageWrapper', () => {
    test('tests false', () => {
      const div = document.createElement('div');
      expect(isImageWrapper(div)).toBeFalsy();
    });

    test('tests true', () => {
      const div = document.createElement('div');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);
      expect(isImageWrapper(div)).toBeTruthy();
    });

    test('is false on null', () => {
      expect(isImageWrapper(null)).toBeFalsy();
    });

    test('is false on non div', () => {
      const div = document.createElement('span');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);
      expect(isImageWrapper(div)).toBeFalsy();
    });
  });

  describe('getImageInsideWrapper', () => {
    test('null element returns null', () => {
      expect(getImageInsideWrapper(null)).toBeNull();
    });

    test('If not image wrapper, return null', () => {
      const div = document.createElement('div');
      expect(getImageInsideWrapper(div)).toBeNull();
    });

    test('If image wrapper, return first image', () => {
      const div = document.createElement('div');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);
      const img = document.createElement('img');
      div.appendChild(img);
      expect(getImageInsideWrapper(div)).toBe(img);
    });

    test('If image wrapper, and no first image, return null', () => {
      const div = document.createElement('div');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);
      expect(getImageInsideWrapper(div)).toBeNull();
    });
  });

  describe('getImageWrapperFromImage', () => {
    test('returns null on non-image', () => {
      const div = document.createElement('div');
      expect(getImageWrapperFromImage(div)).toBeNull();
      div.remove();
    });
    test('returns null on image with no parent', () => {
      const img = document.createElement('img');
      expect(getImageWrapperFromImage(img)).toBeNull();
      img.remove();
    });
    test('returns wrapper on image with wrapper', () => {
      const img = document.createElement('img');
      const div = wrapImage(img);
      expect(getImageWrapperFromImage(img)).toBe(div);
      img.remove();
    });
    test('returns wrapper on linked image with wrapper', () => {
      const img = document.createElement('img');
      const a = document.createElement('a');
      a.appendChild(img);
      const div = wrapImage(a);
      expect(getImageWrapperFromImage(img)).toBe(div);
      img.remove();
    });
    test('returns wrapper arapped image', () => {
      const img = document.createElement('img');
      const a = document.createElement('a');
      a.appendChild(img);
      const div = wrapImage(a);
      expect(getImageWrapperFromImage(div)).toBe(div);
      img.remove();
    });
  });

  describe('getImageLinkFromWrapper', function () {
    test('returns null on non image wrapper', () => {
      document.body.innerHTML = '<div><a><img></a></div>';

      const div = document.body.querySelector('div');

      const l = getImageLinkFromWrapper(div);
      expect(l).toBeNull();
    });
    test('returns null on null element', () => {
      const l = getImageLinkFromWrapper(null);
      expect(l).toBeNull();
    });

    test('returns anchor from image wrapper if exists', () => {
      document.body.innerHTML = '<div><div>gdfgd</div><a id="z"><img></a></div>';

      const div = document.body.querySelector('div');
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const l = getImageLinkFromWrapper(div);
      expect(l).toBeTruthy();
      expect(l?.id).toEqual('z');
    });

    test('returns null from image wrapper if no anchor exists', () => {
      document.body.innerHTML = '<div><img></div>';

      const div = document.body.querySelector('div');
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const l = getImageLinkFromWrapper(div);
      expect(l).toBeNull();
    });

    test('returns null when link is on text', () => {
      document.body.innerHTML = '<div><div><a>dfgdfgd</a></div><img></div>';

      const div = document.body.querySelector('div');
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const l = getImageLinkFromWrapper(div);
      expect(l).toBeNull();
    });
  });

  describe('addLinkToImageInsideWrapper', function () {
    test('returns existing anchor if exists', () => {
      document.body.innerHTML = '<div><a id="z"><img></a></div>';

      const div = document.body.querySelector('div')!;
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const a = addLinkToImageInsideWrapper(div);
      expect(a.id).toEqual('z');
    });

    test('adds anchor and returns it if not exists', () => {
      document.body.innerHTML = '<div><img></div>';

      const div = ensure(document.body.querySelector('div'));
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const a = addLinkToImageInsideWrapper(div);
      expect(document.body.innerHTML).toEqual(
        '<div class="loree-iframe-content-image-wrapper"><a><img></a></div>',
      );
      expect(document.body.querySelector('a')).toBe(a);
    });

    test('created anchor is not clickable', () => {
      document.body.innerHTML = '<div><img></div>';

      const div = ensure(document.body.querySelector('div'));
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const a = addLinkToImageInsideWrapper(div);
      const e = new Event('click');
      jest.spyOn(e, 'preventDefault');
      a.dispatchEvent(e);
      expect(e.preventDefault).toHaveBeenCalled();
    });
  });

  describe('removeLilnkFromImageInWrapper', function () {
    test('does nothing if no anchor', () => {
      document.body.innerHTML = '<div><img></div>';

      const div = ensure(document.body.querySelector('div'));
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      removeLinkFromImageInWrapper(div);
      expect(document.body.innerHTML).toEqual(
        '<div class="loree-iframe-content-image-wrapper"><img></div>',
      );
    });
    test('removes anchor if exists', () => {
      document.body.innerHTML = '<div><a><img></a></div>';

      const div = ensure(document.body.querySelector('div'));
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      removeLinkFromImageInWrapper(div);
      expect(document.body.innerHTML).toEqual(
        '<div class="loree-iframe-content-image-wrapper"><img></div>',
      );
    });
  });

  describe('wrapImage', function () {
    test('return existing wrapper if exists', async () => {
      document.body.innerHTML = '<div><img></div>';

      const div = ensure(document.body.querySelector('div'));
      div?.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);

      const img = ensure(document.body.querySelector('img'));

      const el = wrapImage(img);
      expect(el).toBe(div);
    });

    test('add new wrapper if none exists', async () => {
      document.body.innerHTML = '<img>';

      const img = ensure(document.body.querySelector('img'));

      const el = wrapImage(img);
      expect(el).toBeTruthy();
      expect(el?.tagName).toEqual('DIV');
      expect(isImageWrapper(el)).toBeTruthy();
      expect(document.body.innerHTML).toEqual(
        '<div class="loree-iframe-content-image-wrapper"><img></div>',
      );
    });

    test('handles existing parent', async () => {
      document.body.innerHTML = '<h2><img></h2>';

      const img = ensure(document.body.querySelector('img'));

      const el = wrapImage(img);
      expect(el).toBeTruthy();
      expect(el?.tagName).toEqual('DIV');
      expect(isImageWrapper(el)).toBeTruthy();
      expect(document.body.innerHTML).toEqual(
        '<h2><div class="loree-iframe-content-image-wrapper"><img></div></h2>',
      );
    });
  });
});
