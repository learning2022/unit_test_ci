import { removeElement, wrapChildElements } from '../loree-editor/common/dom';
import {
  addLinkToImageInsideWrapper,
  getImageLinkFromWrapper,
  isImageWrapper,
  removeLinkFromImageInWrapper,
} from './imageWrapper';

/**
 * Set the link details on the anchor element
 * @param element - anchor element to set details on
 * @param openInNewTab boolean
 * @param url url to set
 */
const setLinkDetailsOnElement = (
  element: HTMLAnchorElement,
  openInNewTab: boolean,
  url: string,
) => {
  element.href = url;
  if (openInNewTab) {
    element.target = 'blank';
  } else {
    element.removeAttribute('target');
    //    element.target = '';
  }
};

export function addLinkToElement(
  element: HTMLElement,
  url: string,
  newWindow: boolean,
): HTMLAnchorElement {
  if (isImageWrapper(element)) {
    const link = addLinkToImageInsideWrapper(element);
    setLinkDetailsOnElement(link, newWindow, url);
    return link;
  }

  if (element?.parentElement?.tagName === 'A') {
    const anchor = element.parentElement as HTMLAnchorElement;
    setLinkDetailsOnElement(anchor, newWindow, url);
    return anchor;
  }

  const newAnchor = document.createElement('a');
  setLinkDetailsOnElement(newAnchor, newWindow, url);
  wrapChildElements(element, newAnchor);
  return newAnchor;
}

export function removeLinkFromElement(element: HTMLElement | null) {
  if (!element) {
    return;
  }

  if (isImageWrapper(element)) {
    removeLinkFromImageInWrapper(element);
    return;
  }

  if (element.tagName === 'A') {
    removeElement(element);
    return;
  }

  if (element.parentElement?.tagName === 'A') {
    removeElement(element.parentElement);
  }
}

export function getLinkForElement(
  element: HTMLElement | null,
): { url: string; newWindow: boolean } | null {
  if (!element) {
    return null;
  }

  if (isImageWrapper(element)) {
    const l = getImageLinkFromWrapper(element);
    if (!l) {
      return null;
    }
    return { url: l.href, newWindow: l.target !== '' };
  }

  if (element.tagName === 'A') {
    const l = element as HTMLAnchorElement;
    return { url: l.href, newWindow: l.target !== '' };
  }

  if (element.parentElement?.tagName === 'A') {
    const l = element.parentElement as HTMLAnchorElement;
    return { url: l.href, newWindow: l.target !== '' };
  }

  if (element.children.length > 0 && element.children[0].tagName === 'A') {
    const link = element.children[0] as HTMLAnchorElement;
    return { url: link.href, newWindow: link.target !== '' };
  }
  return null;
}
