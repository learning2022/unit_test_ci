import CONSTANTS from '../loree-editor/constant';
import { addLinkToElement, getLinkForElement, removeLinkFromElement } from './links';

describe('links', function () {
  describe('addLinkToElement', function () {
    test('add URL and target on image wrapper', () => {
      document.body.innerHTML = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><img></div>`;
      const div = document.body.querySelector('div')!;
      addLinkToElement(div, 'http://x.com/', true);

      const anchor = div.querySelector('a')!;
      // expect(document.body.innerHTML).toEqual('');
      expect(anchor.href).toEqual('http://x.com/');
      expect(anchor.getAttribute('target')).toEqual('blank');
    });

    test('removes target if necessary', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><a href="${u}" target="_blank"><img></a></div>`;
      const div = document.body.querySelector('div')!;
      addLinkToElement(div, 'http://x.com/', false);

      const anchor = div.querySelector('a')!;
      // expect(document.body.innerHTML).toEqual('');
      expect(anchor.href).toEqual('http://x.com/');
      expect(anchor.getAttribute('target')).toBeNull();
    });
  });

  describe('getLinkForElement', function () {
    test('returns null on null element', () => {
      expect(getLinkForElement(null)).toBeNull();
    });

    test('returns null on image wrapper with no link', () => {
      document.body.innerHTML = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><img></div>`;

      const div = document.body.querySelector('div')!;
      expect(getLinkForElement(div)).toBeNull();
    });

    test('returns link details on image wrapper with link', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><a href="${u}"><img></a></div>`;

      const div = document.body.querySelector('div')!;
      const l = getLinkForElement(div)!;
      expect(l.url).toEqual(u);
      expect(l.newWindow).toBeFalsy();
    });

    test('returns link details on image wrapper with link and target', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><a href="${u}" target="_blank"><img></a></div>`;

      const div = document.body.querySelector('div')!;
      const l = getLinkForElement(div)!;
      expect(l.url).toEqual(u);
      expect(l.newWindow).toBeTruthy();
    });

    test('returns link details on anchor', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<a href="${u}" target="_blank"><img></a>`;

      const anchor = document.body.querySelector('a')!;
      const l = getLinkForElement(anchor)!;
      expect(l.url).toEqual(u);
      expect(l.newWindow).toBeTruthy();
    });

    test('returns link details on element wrapped in A', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<a href="${u}" target="_blank"><img></a>`;

      const img = document.body.querySelector('img')!;
      const l = getLinkForElement(img)!;
      expect(l.url).toEqual(u);
      expect(l.newWindow).toBeTruthy();
    });

    test('returns when no link is found', () => {
      document.body.innerHTML = `<img>`;

      const img = document.body.querySelector('img')!;
      const l = getLinkForElement(img)!;
      expect(l).toBeNull();
    });
  });

  describe('removeLinkFromElement', function () {
    test('does nothing when element is null', () => {
      expect(() => removeLinkFromElement(null)).not.toThrow();
    });

    test('removes an anchor if passed', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<a href="${u}" target="_blank"><img></a>`;

      const anchor = document.body.querySelector('a');
      removeLinkFromElement(anchor);
      expect(document.body.innerHTML).toEqual('<img>');
    });

    test('removes an anchor if parent element is A', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<a href="${u}" target="_blank"><img></a>`;

      const img = document.body.querySelector('img');
      removeLinkFromElement(img);
      expect(document.body.innerHTML).toEqual('<img>');
    });

    test('removes an anchor from image wrapper', () => {
      const u = 'http://x.com/';
      document.body.innerHTML = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><a href="${u}" target="_blank"><img></a></div>`;

      const div = document.body.querySelector('div')!;

      removeLinkFromElement(div);
      expect(document.body.innerHTML).toEqual(
        `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}"><img></div>`,
      );
    });
  });
});
