import CONSTANTS from '../loree-editor/constant';
import {
  elementHasClass,
  removeElement,
  wrapChildElements,
  wrapElement,
} from '../loree-editor/common/dom';

export function disableImageLinkClick(e: Event) {
  e.preventDefault();
}

/**
 * return true if an element is an Image Wrapper
 */
export function isImageWrapper(element: HTMLElement | null) {
  return (
    element &&
    element.tagName === 'DIV' &&
    (elementHasClass(element, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
      elementHasClass(element, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION))
  );
}

export function getImageInsideWrapper(imageWrapper: HTMLElement | null): HTMLImageElement | null {
  if (!imageWrapper || !isImageWrapper(imageWrapper)) {
    return null;
  }

  return imageWrapper.querySelector('IMG');
}

export function getImageLinkFromWrapper(
  imageWrapper: HTMLElement | null,
): HTMLAnchorElement | null {
  if (!imageWrapper || !isImageWrapper(imageWrapper)) {
    return null;
  }

  let el: HTMLAnchorElement | null = null;

  const links = imageWrapper.querySelectorAll('A');
  links.forEach((l) => {
    if (l.querySelector('img')) {
      el = l as HTMLAnchorElement;
    }
  });

  return el; // imageWrapper.querySelector('A');
}

// export function hasImageLinkInsideWrapper(imageWrapper: HTMLElement | null): boolean {
//   return getImageInsideWrapper(imageWrapper) !== null;
// }

export function repairImageWrapper(imageWrapper: Element) {
  const parentElement = imageWrapper.parentElement;

  if (!parentElement || parentElement.tagName !== 'A') {
    return;
  }
  parentElement.parentElement?.appendChild(imageWrapper);
  wrapChildElements(imageWrapper, parentElement);
}

export function addLinkToImageInsideWrapper(imageWrapper: HTMLElement): HTMLAnchorElement {
  let anchor = getImageLinkFromWrapper(imageWrapper);
  if (anchor) {
    return anchor;
  }
  anchor = document.createElement('a');
  const img = imageWrapper.querySelector('img');

  if (img) {
    wrapElement(img, anchor);
    anchor.onclick = disableImageLinkClick;
  }
  return anchor;
}

export function getImageWrapperFromImage(imageElement: HTMLElement): HTMLDivElement | null {
  if (isImageWrapper(imageElement)) {
    return imageElement as HTMLDivElement;
  }

  if (imageElement.tagName !== 'IMG') return null;

  const parent = imageElement.parentElement;
  if (!parent || isImageWrapper(parent)) {
    return parent as HTMLDivElement;
  }
  if (isImageWrapper(parent?.parentElement)) {
    return parent.parentElement as HTMLDivElement;
  }
  return null;
}

export function wrapImage(imageElement: HTMLElement): HTMLDivElement {
  const wrapper = getImageWrapperFromImage(imageElement);
  if (wrapper) {
    return wrapper;
  }

  const div = document.createElement('div');
  div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);
  if (imageElement.parentElement) {
    wrapChildElements(imageElement.parentElement, div);
  } else {
    div.appendChild(imageElement);
  }
  repairImageWrapper(div);
  return div;
}

export function removeLinkFromImageInWrapper(imageWrapper: HTMLElement) {
  const anchor = getImageLinkFromWrapper(imageWrapper);
  if (!anchor) {
    return;
  }

  removeElement(anchor);
}
