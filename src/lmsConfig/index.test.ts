/* eslint-disable */ // Remove this line when editing this file
import { StorageMock } from '../utils/storageMock';
import * as lmsConfig from './index';

describe('# lms config', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
  });
  describe('#isCanvasOrBB', () => {
    test('returns bb path', () => {
      sessionStorage.setItem('domainName', 'BB');
      expect(lmsConfig.isCanvasAndBB()).toEqual(true);
    });
    test('returns d2l path', () => {
      sessionStorage.setItem('domainName', 'D2l');
      expect(lmsConfig.isCanvasAndBB()).toEqual(false);
    });
  });
  describe('# path configuration', () => {
    test('returns bb path', () => {
      sessionStorage.setItem('domainName', 'BB');
      expect(lmsConfig.lmsUrlRedirection()).toEqual('/lti/bb/home');
    });
    test('returns d2l path', () => {
      sessionStorage.setItem('domainName', 'D2l');
      expect(lmsConfig.lmsUrlRedirection()).toEqual('/lti/d2l/home');
    });
    test('returns canvas path', () => {
      sessionStorage.setItem('domainName', 'canvas');
      expect(lmsConfig.lmsUrlRedirection()).toEqual('/lti/canvas/home');
    });
    test('returns dashboard path', () => {
      sessionStorage.setItem('domainName', '');
      expect(lmsConfig.lmsUrlRedirection()).toEqual('/dashboard');
    });
  });
  describe('# lms user manual url', () => {
    test('returns bb user manual path', () => {
      sessionStorage.setItem('domainName', 'BB');
      expect(lmsConfig.lmsUserManual()).toEqual(
        'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/1841432047/Loree+-+Blackboard+Original+view+User+Manual',
      );
    });
    test('returns d2l user manual path', () => {
      sessionStorage.setItem('domainName', 'D2l');
      expect(lmsConfig.lmsUserManual()).toEqual(
        'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/1632764030/Loree+-+D2L+User+Manual',
      );
    });
    test('returns canvas user manual path', () => {
      sessionStorage.setItem('domainName', 'canvas');
      expect(lmsConfig.lmsUserManual()).toEqual(
        'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/856227907/Loree+2.0+End+User+Manual',
      );
    });
    test('returns dashboard user manual path', () => {
      sessionStorage.setItem('domainName', '');
      expect(lmsConfig.lmsUserManual()).toEqual(
        'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/856227907/Loree+2.0+End+User+Manual',
      );
    });
  });
  describe('#isCanvas', () => {
    beforeEach(() => {
      sessionStorage.setItem('domainName', 'canvas');
    });
    test('returns true for canvas session', () => {
      expect(lmsConfig.isCanvas()).toEqual(true);
    });
    test('not returns true for other session', () => {
      expect(lmsConfig.isBB()).toEqual(false);
    });
  });
  describe('#isBB', () => {
    beforeEach(() => {
      sessionStorage.setItem('domainName', 'BB');
    });
    test('returns true for BB session', () => {
      expect(lmsConfig.isBB()).toEqual(true);
    });
    test('not returns true for other session', () => {
      expect(lmsConfig.isD2l()).toEqual(false);
    });
  });
  describe('#isD2l', () => {
    beforeEach(() => {
      sessionStorage.setItem('domainName', 'D2l');
    });
    test('returns true for D2l session', () => {
      expect(lmsConfig.isD2l()).toEqual(true);
    });
    test('not returns true for other session', () => {
      expect(lmsConfig.isCanvas()).toEqual(false);
    });
  });
});
