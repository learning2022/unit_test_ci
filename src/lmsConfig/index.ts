import { Auth } from 'aws-amplify';
import { LmsLoreeRole } from '../API';
import CONSTANTS from '../loree-editor/constant';
import { PlatformBaseInfo } from '../loree-editor/interface';

export const isCanvasAndBB = () => {
  const currentDomainName = sessionStorage.getItem('domainName');
  switch (currentDomainName) {
    case 'D2l':
      return false;
    case 'BB':
      return true;
    case 'canvas':
      return true;
  }
};

export const isCanvas = () => {
  return sessionStorage.getItem('domainName') === 'canvas';
};

export const isBB = () => {
  return sessionStorage.getItem('domainName') === 'BB';
};

export const isD2l = () => {
  return sessionStorage.getItem('domainName') === 'D2l';
};

export const lmsUrlRedirection = () => {
  switch (sessionStorage.getItem('domainName')) {
    case 'canvas':
      return `/lti/canvas/home`;
    case 'BB':
      return `/lti/bb/home`;
    case 'D2l':
      return `/lti/d2l/home`;
    default:
      return '/dashboard';
  }
};

export const lmsUserManual = () => {
  switch (sessionStorage.getItem('domainName')) {
    case 'canvas':
      return CONSTANTS.LOREE_CANVAS_USER_MANUAL;
    case 'BB':
      return CONSTANTS.LOREE_BB_ORIGINAL_USER_MANUAL;
    case 'D2l':
      return CONSTANTS.LOREE_D2L_USER_MANUAL;
    default:
      return CONSTANTS.LOREE_CANVAS_USER_MANUAL;
  }
};

export const getLmsRoleId = (featureCategory: LmsLoreeRole) => {
  return isCanvas() ? parseInt(featureCategory?.lmsRoleId ?? '0') : featureCategory?.lmsRoleId;
};

export const getCurrentPlatform = async (): Promise<PlatformBaseInfo> => {
  const currentAuthenticatedUser = await Auth.currentAuthenticatedUser();
  const ltiPlatformID = currentAuthenticatedUser.attributes['custom:platform'];
  const currentUserId = currentAuthenticatedUser.attributes.sub;
  return {
    ltiPlatformID,
    currentUserId,
  };
};
