/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const canvasModules = /* GraphQL */ `
  query CanvasModules(
    $courseId: String
    $pageIndex: String
    $pageItems: String
  ) {
    canvasModules(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
    )
  }
`;
export const canvasModuleItems = /* GraphQL */ `
  query CanvasModuleItems($courseId: String, $moduleId: String) {
    canvasModuleItems(courseId: $courseId, moduleId: $moduleId)
  }
`;
export const canvasPages = /* GraphQL */ `
  query CanvasPages(
    $courseId: String
    $pageIndex: String
    $pageItems: String
    $orderType: String
    $sortingValue: String
  ) {
    canvasPages(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
      orderType: $orderType
      sortingValue: $sortingValue
    )
  }
`;
export const canvasAssignments = /* GraphQL */ `
  query CanvasAssignments($courseId: String) {
    canvasAssignments(courseId: $courseId)
  }
`;
export const canvasCourseNavigation = /* GraphQL */ `
  query CanvasCourseNavigation(
    $courseId: String
    $pageIndex: String
    $pageItems: String
  ) {
    canvasCourseNavigation(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
    )
  }
`;
export const canvasAnnouncements = /* GraphQL */ `
  query CanvasAnnouncements(
    $courseId: String
    $pageIndex: String
    $pageItems: String
  ) {
    canvasAnnouncements(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
    )
  }
`;
export const canvasQuizzes = /* GraphQL */ `
  query CanvasQuizzes(
    $courseId: String
    $pageIndex: String
    $pageItems: String
  ) {
    canvasQuizzes(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
    )
  }
`;
export const canvasFiles = /* GraphQL */ `
  query CanvasFiles($courseId: String, $pageIndex: String, $pageItems: String) {
    canvasFiles(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
    )
  }
`;
export const canvasDiscussions = /* GraphQL */ `
  query CanvasDiscussions(
    $courseId: String
    $pageIndex: String
    $pageItems: String
  ) {
    canvasDiscussions(
      courseId: $courseId
      pageIndex: $pageIndex
      pageItems: $pageItems
    )
  }
`;
export const canvasImages = /* GraphQL */ `
  query CanvasImages($courseId: String) {
    canvasImages(courseId: $courseId)
  }
`;
export const kalturaVideos = /* GraphQL */ `
  query KalturaVideos($courseId: String, $platformId: String) {
    kalturaVideos(courseId: $courseId, platformId: $platformId)
  }
`;
export const getKalturaVideo = /* GraphQL */ `
  query GetKalturaVideo(
    $userId: String
    $entryId: String
    $platformId: String
  ) {
    getKalturaVideo(userId: $userId, entryId: $entryId, platformId: $platformId)
  }
`;
export const interativeLibrary = /* GraphQL */ `
  query InterativeLibrary($userId: String) {
    interativeLibrary(userId: $userId)
  }
`;
export const interactiveContent = /* GraphQL */ `
  query InteractiveContent($userId: String, $orgId: String) {
    interactiveContent(userId: $userId, orgId: $orgId)
  }
`;
export const interactiveContentPaginate = /* GraphQL */ `
  query InteractiveContentPaginate(
    $userId: String
    $orgId: String
    $pageId: String
    $pageLimit: String
  ) {
    interactiveContentPaginate(
      userId: $userId
      orgId: $orgId
      pageId: $pageId
      pageLimit: $pageLimit
    )
  }
`;
export const listLoreeInteractive = /* GraphQL */ `
  query ListLoreeInteractive($organization: String, $email: String) {
    listLoreeInteractive(organization: $organization, email: $email)
  }
`;
export const adminAccounts = /* GraphQL */ `
  query AdminAccounts {
    adminAccounts
  }
`;
export const adminSubAccounts = /* GraphQL */ `
  query AdminSubAccounts($accountId: String) {
    adminSubAccounts(accountId: $accountId)
  }
`;
export const adminRoles = /* GraphQL */ `
  query AdminRoles($loreeUserEmail: String, $accountId: String) {
    adminRoles(loreeUserEmail: $loreeUserEmail, accountId: $accountId)
  }
`;
export const courseDetails = /* GraphQL */ `
  query CourseDetails($courseId: String) {
    courseDetails(courseId: $courseId)
  }
`;
export const accountByCourse = /* GraphQL */ `
  query AccountByCourse($accountId: String) {
    accountByCourse(accountId: $accountId)
  }
`;
export const canvasClient = /* GraphQL */ `
  query CanvasClient($platformId: String) {
    canvasClient(platformId: $platformId)
  }
`;
export const refreshPlatformKeys = /* GraphQL */ `
  query RefreshPlatformKeys($platformId: String) {
    refreshPlatformKeys(platformId: $platformId)
  }
`;
export const getH5PContent = /* GraphQL */ `
  query GetH5PContent($userName: String, $platformId: String) {
    getH5PContent(userName: $userName, platformId: $platformId)
  }
`;
export const getAdminH5PContent = /* GraphQL */ `
  query GetAdminH5PContent($platformId: String) {
    getAdminH5PContent(platformId: $platformId)
  }
`;
export const adminDashboardInteractive = /* GraphQL */ `
  query AdminDashboardInteractive(
    $organization: String
    $email: String
    $platformId: String
  ) {
    AdminDashboardInteractive(
      organization: $organization
      email: $email
      platformId: $platformId
    )
  }
`;
export const d2lModules = /* GraphQL */ `
  query D2lModules($courseId: String) {
    d2lModules(courseId: $courseId)
  }
`;
export const viewD2lContents = /* GraphQL */ `
  query ViewD2lContents(
    $courseId: String
    $contentId: String
    $contentType: String
  ) {
    viewD2lContents(
      courseId: $courseId
      contentId: $contentId
      contentType: $contentType
    )
  }
`;
export const viewBbCourses = /* GraphQL */ `
  query ViewBbCourses($courseId: String) {
    viewBbCourses(courseId: $courseId)
  }
`;
export const viewBbContents = /* GraphQL */ `
  query ViewBbContents($courseId: String) {
    viewBbContents(courseId: $courseId)
  }
`;
export const listBbCourseImages = /* GraphQL */ `
  query ListBbCourseImages($courseId: String) {
    listBbCourseImages(courseId: $courseId)
  }
`;
export const viewBbContentsById = /* GraphQL */ `
  query ViewBbContentsById($courseId: String, $contentId: String) {
    viewBbContentsById(courseId: $courseId, contentId: $contentId)
  }
`;
export const viewBbPageContentsById = /* GraphQL */ `
  query ViewBbPageContentsById($courseId: String, $contentId: String) {
    viewBbPageContentsById(courseId: $courseId, contentId: $contentId)
  }
`;
export const d2lAdminRoles = /* GraphQL */ `
  query D2lAdminRoles {
    d2lAdminRoles
  }
`;
export const dashboardStatistics = /* GraphQL */ `
  query DashboardStatistics {
    dashboardStatistics
  }
`;
export const fetchS3Images = /* GraphQL */ `
  query FetchS3Images($platformId: String, $lmsUrl: String) {
    fetchS3Images(platformId: $platformId, lmsUrl: $lmsUrl)
  }
`;
export const getOrganizationAccounts = /* GraphQL */ `
  query GetOrganizationAccounts($email: String) {
    getOrganizationAccounts(email: $email)
  }
`;
export const fetchInstitutionRoles = /* GraphQL */ `
  query FetchInstitutionRoles {
    fetchInstitutionRoles
  }
`;
export const courseEnrollment = /* GraphQL */ `
  query CourseEnrollment($courseId: String) {
    courseEnrollment(courseId: $courseId)
  }
`;
export const listChildCourseImages = /* GraphQL */ `
  query ListChildCourseImages($courseId: String, $resourceId: String) {
    listChildCourseImages(courseId: $courseId, resourceId: $resourceId)
  }
`;
export const listCourseImagesById = /* GraphQL */ `
  query ListCourseImagesById($courseId: String, $resourceId: String) {
    listCourseImagesById(courseId: $courseId, resourceId: $resourceId)
  }
`;
export const migrateUser = /* GraphQL */ `
  query MigrateUser($email: String) {
    migrateUser(email: $email)
  }
`;
export const getCategory = /* GraphQL */ `
  query GetCategory($id: ID!) {
    getCategory(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      name
      projects {
        items {
          id
          title
          categoryID
          category {
            id
            ltiPlatformID
            loreeOrganisationID
            name
            createdAt
            updatedAt
          }
          pages {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listCategorys = /* GraphQL */ `
  query ListCategorys(
    $filter: ModelCategoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCategorys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getProject = /* GraphQL */ `
  query GetProject($id: ID!) {
    getProject(id: $id) {
      id
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      pages {
        items {
          id
          projectID
          project {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          title
          content
          state
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listProjects = /* GraphQL */ `
  query ListProjects(
    $filter: ModelProjectFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listProjects(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        pages {
          items {
            id
            projectID
            title
            content
            state
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPage = /* GraphQL */ `
  query GetPage($id: ID!) {
    getPage(id: $id) {
      id
      projectID
      project {
        id
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        pages {
          items {
            id
            projectID
            title
            content
            state
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      title
      content
      state
      createdAt
      updatedAt
    }
  }
`;
export const listPages = /* GraphQL */ `
  query ListPages(
    $filter: ModelPageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPages(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        projectID
        project {
          id
          title
          categoryID
          category {
            id
            ltiPlatformID
            loreeOrganisationID
            name
            createdAt
            updatedAt
          }
          pages {
            nextToken
          }
          createdAt
          updatedAt
        }
        title
        content
        state
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCustomTemplate = /* GraphQL */ `
  query GetCustomTemplate($id: ID!) {
    getCustomTemplate(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const listCustomTemplates = /* GraphQL */ `
  query ListCustomTemplates(
    $filter: ModelCustomTemplateFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCustomTemplates(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        thumbnail {
          bucket
          key
          region
        }
        content
        status
        isGlobal
        isShared
        createdBy
        active
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getGlobalTemplates = /* GraphQL */ `
  query GetGlobalTemplates($id: ID!) {
    getGlobalTemplates(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      customTemplateID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const listGlobalTemplatess = /* GraphQL */ `
  query ListGlobalTemplatess(
    $filter: ModelGlobalTemplatesFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGlobalTemplatess(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        customTemplateID
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        thumbnail {
          bucket
          key
          region
        }
        content
        status
        createdBy
        active
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSharedTemplates = /* GraphQL */ `
  query GetSharedTemplates($id: ID!) {
    getSharedTemplates(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      customTemplateID
      sharedAccountId
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const listSharedTemplatess = /* GraphQL */ `
  query ListSharedTemplatess(
    $filter: ModelSharedTemplatesFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSharedTemplatess(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        title
        customTemplateID
        sharedAccountId
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        thumbnail {
          bucket
          key
          region
        }
        content
        status
        createdBy
        active
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getFile = /* GraphQL */ `
  query GetFile($id: ID!) {
    getFile(id: $id) {
      id
      name
      size
      location {
        bucket
        key
        region
      }
      type
      thumbnail {
        bucket
        key
        region
      }
      createdAt
      updatedAt
    }
  }
`;
export const listFiles = /* GraphQL */ `
  query ListFiles(
    $filter: ModelFileFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listFiles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        size
        location {
          bucket
          key
          region
        }
        type
        thumbnail {
          bucket
          key
          region
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiPlatform = /* GraphQL */ `
  query GetLtiPlatform($id: ID!) {
    getLtiPlatform(id: $id) {
      id
      clientId
      clientSecret
      platformName
      platformUrl
      authEndpoint
      accesstokenEndpoint
      publicKeyURL
      authConfig {
        method
        key
      }
      googleAnalyticsTrackingId
      ltiDeploymentId
      loreeOrganisationID
      loreeOrganisation {
        id
        name
        ltiPlatforms {
          items {
            id
            clientId
            clientSecret
            platformName
            platformUrl
            authEndpoint
            accesstokenEndpoint
            publicKeyURL
            googleAnalyticsTrackingId
            ltiDeploymentId
            loreeOrganisationID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      ltiApiKey {
        items {
          id
          ltiPlatformID
          loreeOrganisationID
          ltiClientID
          oauthLoginUrl
          oauthTokenUrl
          apiClientId
          apiSecretKey
          lmsApiUrl
          ltiAccessToken {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLtiPlatforms = /* GraphQL */ `
  query ListLtiPlatforms(
    $filter: ModelLtiPlatformFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiPlatforms(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        clientId
        clientSecret
        platformName
        platformUrl
        authEndpoint
        accesstokenEndpoint
        publicKeyURL
        authConfig {
          method
          key
        }
        googleAnalyticsTrackingId
        ltiDeploymentId
        loreeOrganisationID
        loreeOrganisation {
          id
          name
          ltiPlatforms {
            nextToken
          }
          createdAt
          updatedAt
        }
        ltiApiKey {
          items {
            id
            ltiPlatformID
            loreeOrganisationID
            ltiClientID
            oauthLoginUrl
            oauthTokenUrl
            apiClientId
            apiSecretKey
            lmsApiUrl
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiApiKey = /* GraphQL */ `
  query GetLtiApiKey($id: ID!) {
    getLtiApiKey(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      ltiClientID
      oauthLoginUrl
      oauthTokenUrl
      apiClientId
      apiSecretKey
      lmsApiUrl
      ltiAccessToken {
        items {
          id
          ltiApiKeyID
          ltiClientID
          ltiPlatformID
          loreeOrganisationID
          loreeUsername
          loreePassword
          user
          userInfo
          roles
          accessToken
          refreshToken
          isAdmin
          generatedAt
          lmsApiUrl
          expiresAt
          lms_email
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLtiApiKeys = /* GraphQL */ `
  query ListLtiApiKeys(
    $filter: ModelLtiApiKeyFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiApiKeys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        ltiClientID
        oauthLoginUrl
        oauthTokenUrl
        apiClientId
        apiSecretKey
        lmsApiUrl
        ltiAccessToken {
          items {
            id
            ltiApiKeyID
            ltiClientID
            ltiPlatformID
            loreeOrganisationID
            loreeUsername
            loreePassword
            user
            userInfo
            roles
            accessToken
            refreshToken
            isAdmin
            generatedAt
            lmsApiUrl
            expiresAt
            lms_email
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiPlatformKey = /* GraphQL */ `
  query GetLtiPlatformKey($id: ID!) {
    getLtiPlatformKey(id: $id) {
      id
      ltiPlatformID
      kid
      privatekey
      publicKey
      createdAt
      updatedAt
    }
  }
`;
export const listLtiPlatformKeys = /* GraphQL */ `
  query ListLtiPlatformKeys(
    $filter: ModelLtiPlatformKeyFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiPlatformKeys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        kid
        privatekey
        publicKey
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiAccessToken = /* GraphQL */ `
  query GetLtiAccessToken($id: ID!) {
    getLtiAccessToken(id: $id) {
      id
      ltiApiKeyID
      ltiClientID
      ltiPlatformID
      loreeOrganisationID
      loreeUsername
      loreePassword
      user
      userInfo
      roles
      accessToken
      refreshToken
      isAdmin
      generatedAt
      lmsApiUrl
      expiresAt
      lms_email
      createdAt
      updatedAt
    }
  }
`;
export const listLtiAccessTokens = /* GraphQL */ `
  query ListLtiAccessTokens(
    $filter: ModelLtiAccessTokenFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiAccessTokens(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiApiKeyID
        ltiClientID
        ltiPlatformID
        loreeOrganisationID
        loreeUsername
        loreePassword
        user
        userInfo
        roles
        accessToken
        refreshToken
        isAdmin
        generatedAt
        lmsApiUrl
        expiresAt
        lms_email
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiLog = /* GraphQL */ `
  query GetLtiLog($id: ID!) {
    getLtiLog(id: $id) {
      id
      ltiPlatformID
      userInfo
      clientInfo
      userGeoLocation
      userAction
      generatedAt
      createdAt
      updatedAt
    }
  }
`;
export const listLtiLogs = /* GraphQL */ `
  query ListLtiLogs(
    $filter: ModelLtiLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiLogs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        userInfo
        clientInfo
        userGeoLocation
        userAction
        generatedAt
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCustomBlock = /* GraphQL */ `
  query GetCustomBlock($id: ID!) {
    getCustomBlock(id: $id) {
      id
      title
      categoryID
      ltiPlatformID
      loreeOrganisationID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      type
      content
      thumbnail {
        bucket
        key
        region
      }
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const listCustomBlocks = /* GraphQL */ `
  query ListCustomBlocks(
    $filter: ModelCustomBlockFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCustomBlocks(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        categoryID
        ltiPlatformID
        loreeOrganisationID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        type
        content
        thumbnail {
          bucket
          key
          region
        }
        status
        isGlobal
        isShared
        createdBy
        active
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getGlobalBlocks = /* GraphQL */ `
  query GetGlobalBlocks($id: ID!) {
    getGlobalBlocks(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      customBlockID
      title
      type
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const listGlobalBlockss = /* GraphQL */ `
  query ListGlobalBlockss(
    $filter: ModelGlobalBlocksFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGlobalBlockss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        customBlockID
        title
        type
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        thumbnail {
          bucket
          key
          region
        }
        content
        status
        createdBy
        active
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSharedBlocks = /* GraphQL */ `
  query GetSharedBlocks($id: ID!) {
    getSharedBlocks(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      type
      customBlockID
      categoryID
      sharedAccountId
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const listSharedBlockss = /* GraphQL */ `
  query ListSharedBlockss(
    $filter: ModelSharedBlocksFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSharedBlockss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        title
        type
        customBlockID
        categoryID
        sharedAccountId
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        thumbnail {
          bucket
          key
          region
        }
        content
        status
        createdBy
        active
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCustomStyle = /* GraphQL */ `
  query GetCustomStyle($id: ID!) {
    getCustomStyle(id: $id) {
      id
      ltiPlatformID
      loreeOrganisationID
      customColor
      customHeader
      customFont
      customLink
      createdAt
      updatedAt
    }
  }
`;
export const listCustomStyles = /* GraphQL */ `
  query ListCustomStyles(
    $filter: ModelCustomStyleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCustomStyles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        customColor
        customHeader
        customFont
        customLink
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getKalturaConfig = /* GraphQL */ `
  query GetKalturaConfig($id: ID!) {
    getKalturaConfig(id: $id) {
      id
      ltiPlatformID
      partnerID
      appToken
      tokenID
      createdAt
      updatedAt
    }
  }
`;
export const listKalturaConfigs = /* GraphQL */ `
  query ListKalturaConfigs(
    $filter: ModelKalturaConfigFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listKalturaConfigs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        partnerID
        appToken
        tokenID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLoreeFeature = /* GraphQL */ `
  query GetLoreeFeature($id: ID!) {
    getLoreeFeature(id: $id) {
      id
      ltiPlatformID
      featureList
      createdAt
      updatedAt
    }
  }
`;
export const listLoreeFeatures = /* GraphQL */ `
  query ListLoreeFeatures(
    $filter: ModelLoreeFeatureFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLoreeFeatures(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        featureList
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLoreeRole = /* GraphQL */ `
  query GetLoreeRole($id: ID!) {
    getLoreeRole(id: $id) {
      id
      ltiPlatformID
      name
      description
      featureList
      lmsLoreeRoles {
        items {
          id
          ltiPlatformID
          loreeRoleID {
            id
            ltiPlatformID
            name
            description
            featureList
            createdAt
            updatedAt
          }
          lmsRole
          lmsBaseRoleType
          lmsRoleId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLoreeRoles = /* GraphQL */ `
  query ListLoreeRoles(
    $filter: ModelLoreeRoleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLoreeRoles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        name
        description
        featureList
        lmsLoreeRoles {
          items {
            id
            ltiPlatformID
            lmsRole
            lmsBaseRoleType
            lmsRoleId
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLmsLoreeRole = /* GraphQL */ `
  query GetLmsLoreeRole($id: ID!) {
    getLmsLoreeRole(id: $id) {
      id
      ltiPlatformID
      loreeRoleID {
        id
        ltiPlatformID
        name
        description
        featureList
        lmsLoreeRoles {
          items {
            id
            ltiPlatformID
            lmsRole
            lmsBaseRoleType
            lmsRoleId
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      lmsRole
      lmsBaseRoleType
      lmsRoleId
      createdAt
      updatedAt
    }
  }
`;
export const listLmsLoreeRoles = /* GraphQL */ `
  query ListLmsLoreeRoles(
    $filter: ModelLmsLoreeRoleFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLmsLoreeRoles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        loreeRoleID {
          id
          ltiPlatformID
          name
          description
          featureList
          lmsLoreeRoles {
            nextToken
          }
          createdAt
          updatedAt
        }
        lmsRole
        lmsBaseRoleType
        lmsRoleId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getHFivePUsers = /* GraphQL */ `
  query GetHFivePUsers($id: ID!) {
    getHFivePUsers(id: $id) {
      id
      email
      loreeUser
      ltiPlatformID
      createdAt
      updatedAt
    }
  }
`;
export const listHFivePUserss = /* GraphQL */ `
  query ListHFivePUserss(
    $filter: ModelHFivePUsersFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHFivePUserss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        email
        loreeUser
        ltiPlatformID
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getInteractivesMetaData = /* GraphQL */ `
  query GetInteractivesMetaData($id: ID!) {
    getInteractivesMetaData(id: $id) {
      id
      ltiPlatformID
      metaKey
      metaValue
      isRetired
      createdAt
      updatedAt
    }
  }
`;
export const listInteractivesMetaDatas = /* GraphQL */ `
  query ListInteractivesMetaDatas(
    $filter: ModelInteractivesMetaDataFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listInteractivesMetaDatas(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        ltiPlatformID
        metaKey
        metaValue
        isRetired
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLmsImage = /* GraphQL */ `
  query GetLmsImage($id: ID!) {
    getLmsImage(id: $id) {
      id
      lms
      lmsImageId
      lmsImageUUID
      lmsImageName
      lmsImageDisplayName
      lmsImageURL
      lmsImageType
      s3ObjectKey
      s3PublicObjectKey
      s3GlobalTemplateKey
      s3GlobalRowsKey
      s3GlobalElementsKey
      s3SharedTemplateKey
      s3SharedRowsKey
      s3SharedElementsKey
      lmsImageMetaData
      s3MetaData
      createdAt
      updatedAt
    }
  }
`;
export const listLmsImages = /* GraphQL */ `
  query ListLmsImages(
    $filter: ModelLmsImageFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLmsImages(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        lms
        lmsImageId
        lmsImageUUID
        lmsImageName
        lmsImageDisplayName
        lmsImageURL
        lmsImageType
        s3ObjectKey
        s3PublicObjectKey
        s3GlobalTemplateKey
        s3GlobalRowsKey
        s3GlobalElementsKey
        s3SharedTemplateKey
        s3SharedRowsKey
        s3SharedElementsKey
        lmsImageMetaData
        s3MetaData
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLoreeOrganisation = /* GraphQL */ `
  query GetLoreeOrganisation($id: ID!) {
    getLoreeOrganisation(id: $id) {
      id
      name
      ltiPlatforms {
        items {
          id
          clientId
          clientSecret
          platformName
          platformUrl
          authEndpoint
          accesstokenEndpoint
          publicKeyURL
          authConfig {
            method
            key
          }
          googleAnalyticsTrackingId
          ltiDeploymentId
          loreeOrganisationID
          loreeOrganisation {
            id
            name
            createdAt
            updatedAt
          }
          ltiApiKey {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLoreeOrganisations = /* GraphQL */ `
  query ListLoreeOrganisations(
    $filter: ModelLoreeOrganisationFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLoreeOrganisations(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        name
        ltiPlatforms {
          items {
            id
            clientId
            clientSecret
            platformName
            platformUrl
            authEndpoint
            accesstokenEndpoint
            publicKeyURL
            googleAnalyticsTrackingId
            ltiDeploymentId
            loreeOrganisationID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLoreeUser = /* GraphQL */ `
  query GetLoreeUser($id: ID!) {
    getLoreeUser(id: $id) {
      id
      loreeOrganisationID
      name
      email
      username
      externalIds {
        key
        value
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLoreeUsers = /* GraphQL */ `
  query ListLoreeUsers(
    $filter: ModelLoreeUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLoreeUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        loreeOrganisationID
        name
        email
        username
        externalIds {
          key
          value
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiTool = /* GraphQL */ `
  query GetLtiTool($id: ID!) {
    getLtiTool(id: $id) {
      id
      ltiPlatformID
      toolName
      issuerUrl
      oidcUrl
      redirectURI
      targetLinkURI
      jwksUrl
      domainName
      clientId
      clientSecret
      toolDeployments {
        items {
          id
          toolId {
            id
            ltiPlatformID
            toolName
            issuerUrl
            oidcUrl
            redirectURI
            targetLinkURI
            jwksUrl
            domainName
            clientId
            clientSecret
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLtiTools = /* GraphQL */ `
  query ListLtiTools(
    $filter: ModelLtiToolFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiTools(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        toolName
        issuerUrl
        oidcUrl
        redirectURI
        targetLinkURI
        jwksUrl
        domainName
        clientId
        clientSecret
        toolDeployments {
          items {
            id
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiToolLoginHint = /* GraphQL */ `
  query GetLtiToolLoginHint($id: ID!) {
    getLtiToolLoginHint(id: $id) {
      id
      issuerUrl
      clientId
      loreeUserEmail
      messageType
      resourceLink
      createdAt
      updatedAt
    }
  }
`;
export const listLtiToolLoginHints = /* GraphQL */ `
  query ListLtiToolLoginHints(
    $filter: ModelLtiToolLoginHintFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiToolLoginHints(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        issuerUrl
        clientId
        loreeUserEmail
        messageType
        resourceLink
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getLtiToolDeployment = /* GraphQL */ `
  query GetLtiToolDeployment($id: ID!) {
    getLtiToolDeployment(id: $id) {
      id
      toolId {
        id
        ltiPlatformID
        toolName
        issuerUrl
        oidcUrl
        redirectURI
        targetLinkURI
        jwksUrl
        domainName
        clientId
        clientSecret
        toolDeployments {
          items {
            id
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listLtiToolDeployments = /* GraphQL */ `
  query ListLtiToolDeployments(
    $filter: ModelLtiToolDeploymentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listLtiToolDeployments(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        toolId {
          id
          ltiPlatformID
          toolName
          issuerUrl
          oidcUrl
          redirectURI
          targetLinkURI
          jwksUrl
          domainName
          clientId
          clientSecret
          toolDeployments {
            nextToken
          }
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCustomTheme = /* GraphQL */ `
  query GetCustomTheme($id: ID!) {
    getCustomTheme(id: $id) {
      id
      ltiPlatformID
      isShared
      themeData
      createdAt
      updatedAt
    }
  }
`;
export const listCustomThemes = /* GraphQL */ `
  query ListCustomThemes(
    $filter: ModelCustomThemeFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCustomThemes(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        ltiPlatformID
        isShared
        themeData
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const categoryByPlatform = /* GraphQL */ `
  query CategoryByPlatform(
    $ltiPlatformID: ID
    $sortDirection: ModelSortDirection
    $filter: ModelCategoryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    categoryByPlatform(
      ltiPlatformID: $ltiPlatformID
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const templateByOwner = /* GraphQL */ `
  query TemplateByOwner(
    $ltiPlatformID: ID
    $owner: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCustomTemplateFilterInput
    $limit: Int
    $nextToken: String
  ) {
    templateByOwner(
      ltiPlatformID: $ltiPlatformID
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        ltiPlatformID
        loreeOrganisationID
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        thumbnail {
          bucket
          key
          region
        }
        content
        status
        isGlobal
        isShared
        createdBy
        active
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const byLtiPlatformByGeneratedAt = /* GraphQL */ `
  query ByLtiPlatformByGeneratedAt(
    $ltiPlatformID: ID
    $generatedAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelLtiLogFilterInput
    $limit: Int
    $nextToken: String
  ) {
    byLtiPlatformByGeneratedAt(
      ltiPlatformID: $ltiPlatformID
      generatedAt: $generatedAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        ltiPlatformID
        userInfo
        clientInfo
        userGeoLocation
        userAction
        generatedAt
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const blockByOwner = /* GraphQL */ `
  query BlockByOwner(
    $ltiPlatformID: ID
    $owner: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelCustomBlockFilterInput
    $limit: Int
    $nextToken: String
  ) {
    blockByOwner(
      ltiPlatformID: $ltiPlatformID
      owner: $owner
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        title
        categoryID
        ltiPlatformID
        loreeOrganisationID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        type
        content
        thumbnail {
          bucket
          key
          region
        }
        status
        isGlobal
        isShared
        createdBy
        active
        owner
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
