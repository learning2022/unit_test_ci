/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const updateCanvasModule = /* GraphQL */ `
  mutation UpdateCanvasModule(
    $courseId: String
    $moduleId: String
    $moduleName: String
  ) {
    updateCanvasModule(
      courseId: $courseId
      moduleId: $moduleId
      moduleName: $moduleName
    )
  }
`;
export const deleteCanvasModule = /* GraphQL */ `
  mutation DeleteCanvasModule($courseId: String, $moduleId: String) {
    deleteCanvasModule(courseId: $courseId, moduleId: $moduleId)
  }
`;
export const viewCanvasPage = /* GraphQL */ `
  mutation ViewCanvasPage($courseId: String, $pageId: String) {
    viewCanvasPage(courseId: $courseId, pageId: $pageId)
  }
`;
export const updateCanvasPage = /* GraphQL */ `
  mutation UpdateCanvasPage(
    $courseId: String
    $pageUrl: String
    $pageName: String
  ) {
    updateCanvasPage(
      courseId: $courseId
      pageUrl: $pageUrl
      pageName: $pageName
    )
  }
`;
export const deleteCanvasPage = /* GraphQL */ `
  mutation DeleteCanvasPage($courseId: String, $pageUrl: String) {
    deleteCanvasPage(courseId: $courseId, pageUrl: $pageUrl)
  }
`;
export const duplicateCanvasPage = /* GraphQL */ `
  mutation DuplicateCanvasPage($courseId: String, $pageUrl: String) {
    duplicateCanvasPage(courseId: $courseId, pageUrl: $pageUrl)
  }
`;
export const saveCanvasPage = /* GraphQL */ `
  mutation SaveCanvasPage(
    $courseId: String
    $pageID: String
    $editorContent: String
    $uploadAndConvertFromBackendFeatureToggle: Boolean
  ) {
    saveCanvasPage(
      courseId: $courseId
      pageID: $pageID
      editorContent: $editorContent
      uploadAndConvertFromBackendFeatureToggle: $uploadAndConvertFromBackendFeatureToggle
    )
  }
`;
export const viewCanvasDiscussion = /* GraphQL */ `
  mutation ViewCanvasDiscussion($courseId: String, $discussionId: String) {
    viewCanvasDiscussion(courseId: $courseId, discussionId: $discussionId)
  }
`;
export const updateCanvasDiscussion = /* GraphQL */ `
  mutation UpdateCanvasDiscussion(
    $courseId: String
    $discussionId: String
    $discussionName: String
  ) {
    updateCanvasDiscussion(
      courseId: $courseId
      discussionId: $discussionId
      discussionName: $discussionName
    )
  }
`;
export const saveCanvasDiscussion = /* GraphQL */ `
  mutation SaveCanvasDiscussion(
    $courseId: String
    $discussionID: String
    $editorContent: String
    $uploadAndConvertFromBackendFeatureToggle: Boolean
  ) {
    saveCanvasDiscussion(
      courseId: $courseId
      discussionID: $discussionID
      editorContent: $editorContent
      uploadAndConvertFromBackendFeatureToggle: $uploadAndConvertFromBackendFeatureToggle
    )
  }
`;
export const viewCanvasAssignment = /* GraphQL */ `
  mutation ViewCanvasAssignment($courseId: String, $assignmentId: String) {
    viewCanvasAssignment(courseId: $courseId, assignmentId: $assignmentId)
  }
`;
export const updateCanvasAssignment = /* GraphQL */ `
  mutation UpdateCanvasAssignment(
    $courseId: String
    $assignmentId: String
    $assignmentName: String
  ) {
    updateCanvasAssignment(
      courseId: $courseId
      assignmentId: $assignmentId
      assignmentName: $assignmentName
    )
  }
`;
export const saveCanvasAssignment = /* GraphQL */ `
  mutation SaveCanvasAssignment(
    $courseId: String
    $assignmentID: String
    $editorContent: String
    $uploadAndConvertFromBackendFeatureToggle: Boolean
  ) {
    saveCanvasAssignment(
      courseId: $courseId
      assignmentID: $assignmentID
      editorContent: $editorContent
      uploadAndConvertFromBackendFeatureToggle: $uploadAndConvertFromBackendFeatureToggle
    )
  }
`;
export const updateModuleItem = /* GraphQL */ `
  mutation UpdateModuleItem(
    $courseId: String
    $moduleId: String
    $itemId: String
    $title: String
  ) {
    updateModuleItem(
      courseId: $courseId
      moduleId: $moduleId
      itemId: $itemId
      title: $title
    )
  }
`;
export const deleteModuleItem = /* GraphQL */ `
  mutation DeleteModuleItem(
    $courseId: String
    $moduleId: String
    $itemId: String
  ) {
    deleteModuleItem(courseId: $courseId, moduleId: $moduleId, itemId: $itemId)
  }
`;
export const duplicatePageFromModule = /* GraphQL */ `
  mutation DuplicatePageFromModule(
    $courseId: String
    $moduleId: String
    $pageUrl: String
  ) {
    duplicatePageFromModule(
      courseId: $courseId
      moduleId: $moduleId
      pageUrl: $pageUrl
    )
  }
`;
export const createToCanvas = /* GraphQL */ `
  mutation CreateToCanvas(
    $courseId: String
    $moduleId: String
    $Name: String
    $type: String
  ) {
    createToCanvas(
      courseId: $courseId
      moduleId: $moduleId
      Name: $Name
      type: $type
    )
  }
`;
export const viewCanvasAssignments = /* GraphQL */ `
  mutation ViewCanvasAssignments($courseId: String, $assignmentId: String) {
    viewCanvasAssignments(courseId: $courseId, assignmentId: $assignmentId)
  }
`;
export const publishToCanvas = /* GraphQL */ `
  mutation PublishToCanvas(
    $courseId: String
    $moduleId: String
    $itemId: String
    $pageId: String
    $assignmentId: String
    $discussionId: String
    $quizId: String
    $status: Boolean
    $type: String
  ) {
    publishToCanvas(
      courseId: $courseId
      moduleId: $moduleId
      itemId: $itemId
      pageId: $pageId
      assignmentId: $assignmentId
      discussionId: $discussionId
      quizId: $quizId
      status: $status
      type: $type
    )
  }
`;
export const imageUpload = /* GraphQL */ `
  mutation ImageUpload(
    $courseId: String
    $name: String
    $type: String
    $path: String
  ) {
    imageUpload(courseId: $courseId, name: $name, type: $type, path: $path)
  }
`;
export const imageS3Upload = /* GraphQL */ `
  mutation ImageS3Upload(
    $platformId: String
    $path: String
    $title: String
    $type: String
  ) {
    imageS3Upload(
      platformId: $platformId
      path: $path
      title: $title
      type: $type
    )
  }
`;
export const uploadImagesToS3 = /* GraphQL */ `
  mutation UploadImagesToS3($globalHtmlContent: String, $type: String) {
    uploadImagesToS3(globalHtmlContent: $globalHtmlContent, type: $type)
  }
`;
export const videoUpload = /* GraphQL */ `
  mutation VideoUpload(
    $courseId: String
    $name: String
    $path: String
    $platformId: String
  ) {
    videoUpload(
      courseId: $courseId
      name: $name
      path: $path
      platformId: $platformId
    )
  }
`;
export const uploadD2LImagesToS3 = /* GraphQL */ `
  mutation UploadD2LImagesToS3(
    $platformId: String
    $file: String
    $title: String
    $newTitle: String
    $lmsUrl: String
  ) {
    uploadD2LImagesToS3(
      platformId: $platformId
      file: $file
      title: $title
      newTitle: $newTitle
      lmsUrl: $lmsUrl
    )
  }
`;
export const initiateInteractive = /* GraphQL */ `
  mutation InitiateInteractive(
    $tokenId: String
    $type: String
    $email: String
    $name: String
    $organization: String
  ) {
    initiateInteractive(
      tokenId: $tokenId
      type: $type
      email: $email
      name: $name
      organization: $organization
    )
  }
`;
export const verifyInteractive = /* GraphQL */ `
  mutation VerifyInteractive($user: String, $orgId: String) {
    verifyInteractive(user: $user, orgId: $orgId)
  }
`;
export const updateInteractiveContent = /* GraphQL */ `
  mutation UpdateInteractiveContent(
    $contentId: String
    $userId: String
    $status: Boolean
  ) {
    updateInteractiveContent(
      contentId: $contentId
      userId: $userId
      status: $status
    )
  }
`;
export const updateLoreeInteractiveContent = /* GraphQL */ `
  mutation UpdateLoreeInteractiveContent(
    $userId: String
    $title: String
    $contentId: String
    $childOrganization: AWSJSON
  ) {
    updateLoreeInteractiveContent(
      userId: $userId
      title: $title
      contentId: $contentId
      childOrganization: $childOrganization
    )
  }
`;
export const contentShareUpdates = /* GraphQL */ `
  mutation ContentShareUpdates(
    $childOrganization: AWSJSON
    $userId: String
    $shareStatus: Boolean
    $contentId: String
  ) {
    contentShareUpdates(
      childOrganization: $childOrganization
      userId: $userId
      shareStatus: $shareStatus
      contentId: $contentId
    )
  }
`;
export const updateOrganizationAccount = /* GraphQL */ `
  mutation UpdateOrganizationAccount(
    $email: String
    $childOrganization: AWSJSON
  ) {
    updateOrganizationAccount(
      email: $email
      childOrganization: $childOrganization
    )
  }
`;
export const deleteLoreeInteractiveContent = /* GraphQL */ `
  mutation DeleteLoreeInteractiveContent($userId: String, $contentId: String) {
    deleteLoreeInteractiveContent(userId: $userId, contentId: $contentId)
  }
`;
export const loreeInteractiveContentStatusUpdate = /* GraphQL */ `
  mutation LoreeInteractiveContentStatusUpdate(
    $status: Boolean
    $organization: String
    $childOrganization: AWSJSON
  ) {
    loreeInteractiveContentStatusUpdate(
      status: $status
      organization: $organization
      childOrganization: $childOrganization
    )
  }
`;
export const duplicateLoreeInteractiveContent = /* GraphQL */ `
  mutation DuplicateLoreeInteractiveContent(
    $email: String
    $title: String
    $contentId: String
  ) {
    duplicateLoreeInteractiveContent(
      email: $email
      title: $title
      contentId: $contentId
    )
  }
`;
export const h5pUserInitiate = /* GraphQL */ `
  mutation H5pUserInitiate(
    $email: String
    $firstName: String
    $role: String
    $platformId: String
    $platformDomain: String
  ) {
    h5pUserInitiate(
      email: $email
      firstName: $firstName
      role: $role
      platformId: $platformId
      platformDomain: $platformDomain
    )
  }
`;
export const deleteH5PContent = /* GraphQL */ `
  mutation DeleteH5PContent($contentId: String, $platformId: String) {
    deleteH5PContent(contentId: $contentId, platformId: $platformId)
  }
`;
export const updateH5pMetaData = /* GraphQL */ `
  mutation UpdateH5pMetaData(
    $contentId: String
    $platformId: String
    $metaData: AWSJSON
  ) {
    updateH5pMetaData(
      contentId: $contentId
      platformId: $platformId
      metaData: $metaData
    )
  }
`;
export const updateD2lContents = /* GraphQL */ `
  mutation UpdateD2lContents(
    $courseId: String
    $contentId: String
    $contentType: String
    $formData: AWSJSON
  ) {
    updateD2lContents(
      courseId: $courseId
      contentId: $contentId
      contentType: $contentType
      formData: $formData
    )
  }
`;
export const createD2lModules = /* GraphQL */ `
  mutation CreateD2lModules($courseId: String, $formData: AWSJSON) {
    createD2lModules(courseId: $courseId, formData: $formData)
  }
`;
export const updateBbContents = /* GraphQL */ `
  mutation UpdateBbContents(
    $courseId: String
    $contentId: String
    $updateContentData: AWSJSON
  ) {
    updateBbContents(
      courseId: $courseId
      contentId: $contentId
      updateContentData: $updateContentData
    )
  }
`;
export const updateUltraContent = /* GraphQL */ `
  mutation UpdateUltraContent(
    $courseId: String
    $contentId: String
    $updateContentData: AWSJSON
  ) {
    updateUltraContent(
      courseId: $courseId
      contentId: $contentId
      updateContentData: $updateContentData
    )
  }
`;
export const createD2LSubContent = /* GraphQL */ `
  mutation CreateD2LSubContent(
    $courseId: String
    $folderId: String
    $formData: AWSJSON
  ) {
    createD2LSubContent(
      courseId: $courseId
      folderId: $folderId
      formData: $formData
    )
  }
`;
export const createBbRootContent = /* GraphQL */ `
  mutation CreateBbRootContent($courseId: String, $contentData: AWSJSON) {
    createBbRootContent(courseId: $courseId, contentData: $contentData)
  }
`;
export const createBbChildContent = /* GraphQL */ `
  mutation CreateBbChildContent(
    $courseId: String
    $contentId: String
    $contentData: AWSJSON
  ) {
    createBbChildContent(
      courseId: $courseId
      contentId: $contentId
      contentData: $contentData
    )
  }
`;
export const createBbContent = /* GraphQL */ `
  mutation CreateBbContent(
    $courseId: String
    $contentId: String
    $contentData: AWSJSON
  ) {
    createBbContent(
      courseId: $courseId
      contentId: $contentId
      contentData: $contentData
    )
  }
`;
export const bbCourseImageUpload = /* GraphQL */ `
  mutation BbCourseImageUpload(
    $courseId: String
    $file: String
    $fileName: String
    $fileType: String
    $parentId: String
  ) {
    bbCourseImageUpload(
      courseId: $courseId
      file: $file
      fileName: $fileName
      fileType: $fileType
      parentId: $parentId
    )
  }
`;
export const uploadBbUltraFileToS3 = /* GraphQL */ `
  mutation UploadBbUltraFileToS3(
    $platformId: String
    $file: String
    $title: String
    $newTitle: String
  ) {
    uploadBBUltraFileToS3(
      platformId: $platformId
      file: $file
      title: $title
      newTitle: $newTitle
    )
  }
`;
export const createCategory = /* GraphQL */ `
  mutation CreateCategory(
    $input: CreateCategoryInput!
    $condition: ModelCategoryConditionInput
  ) {
    createCategory(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      name
      projects {
        items {
          id
          title
          categoryID
          category {
            id
            ltiPlatformID
            loreeOrganisationID
            name
            createdAt
            updatedAt
          }
          pages {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateCategory = /* GraphQL */ `
  mutation UpdateCategory(
    $input: UpdateCategoryInput!
    $condition: ModelCategoryConditionInput
  ) {
    updateCategory(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      name
      projects {
        items {
          id
          title
          categoryID
          category {
            id
            ltiPlatformID
            loreeOrganisationID
            name
            createdAt
            updatedAt
          }
          pages {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteCategory = /* GraphQL */ `
  mutation DeleteCategory(
    $input: DeleteCategoryInput!
    $condition: ModelCategoryConditionInput
  ) {
    deleteCategory(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      name
      projects {
        items {
          id
          title
          categoryID
          category {
            id
            ltiPlatformID
            loreeOrganisationID
            name
            createdAt
            updatedAt
          }
          pages {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createProject = /* GraphQL */ `
  mutation CreateProject(
    $input: CreateProjectInput!
    $condition: ModelProjectConditionInput
  ) {
    createProject(input: $input, condition: $condition) {
      id
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      pages {
        items {
          id
          projectID
          project {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          title
          content
          state
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateProject = /* GraphQL */ `
  mutation UpdateProject(
    $input: UpdateProjectInput!
    $condition: ModelProjectConditionInput
  ) {
    updateProject(input: $input, condition: $condition) {
      id
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      pages {
        items {
          id
          projectID
          project {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          title
          content
          state
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteProject = /* GraphQL */ `
  mutation DeleteProject(
    $input: DeleteProjectInput!
    $condition: ModelProjectConditionInput
  ) {
    deleteProject(input: $input, condition: $condition) {
      id
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      pages {
        items {
          id
          projectID
          project {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          title
          content
          state
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createPage = /* GraphQL */ `
  mutation CreatePage(
    $input: CreatePageInput!
    $condition: ModelPageConditionInput
  ) {
    createPage(input: $input, condition: $condition) {
      id
      projectID
      project {
        id
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        pages {
          items {
            id
            projectID
            title
            content
            state
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      title
      content
      state
      createdAt
      updatedAt
    }
  }
`;
export const updatePage = /* GraphQL */ `
  mutation UpdatePage(
    $input: UpdatePageInput!
    $condition: ModelPageConditionInput
  ) {
    updatePage(input: $input, condition: $condition) {
      id
      projectID
      project {
        id
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        pages {
          items {
            id
            projectID
            title
            content
            state
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      title
      content
      state
      createdAt
      updatedAt
    }
  }
`;
export const deletePage = /* GraphQL */ `
  mutation DeletePage(
    $input: DeletePageInput!
    $condition: ModelPageConditionInput
  ) {
    deletePage(input: $input, condition: $condition) {
      id
      projectID
      project {
        id
        title
        categoryID
        category {
          id
          ltiPlatformID
          loreeOrganisationID
          name
          projects {
            nextToken
          }
          createdAt
          updatedAt
        }
        pages {
          items {
            id
            projectID
            title
            content
            state
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      title
      content
      state
      createdAt
      updatedAt
    }
  }
`;
export const createCustomTemplate = /* GraphQL */ `
  mutation CreateCustomTemplate(
    $input: CreateCustomTemplateInput!
    $condition: ModelCustomTemplateConditionInput
  ) {
    createCustomTemplate(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const updateCustomTemplate = /* GraphQL */ `
  mutation UpdateCustomTemplate(
    $input: UpdateCustomTemplateInput!
    $condition: ModelCustomTemplateConditionInput
  ) {
    updateCustomTemplate(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const deleteCustomTemplate = /* GraphQL */ `
  mutation DeleteCustomTemplate(
    $input: DeleteCustomTemplateInput!
    $condition: ModelCustomTemplateConditionInput
  ) {
    deleteCustomTemplate(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const createGlobalTemplates = /* GraphQL */ `
  mutation CreateGlobalTemplates(
    $input: CreateGlobalTemplatesInput!
    $condition: ModelGlobalTemplatesConditionInput
  ) {
    createGlobalTemplates(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customTemplateID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const updateGlobalTemplates = /* GraphQL */ `
  mutation UpdateGlobalTemplates(
    $input: UpdateGlobalTemplatesInput!
    $condition: ModelGlobalTemplatesConditionInput
  ) {
    updateGlobalTemplates(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customTemplateID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const deleteGlobalTemplates = /* GraphQL */ `
  mutation DeleteGlobalTemplates(
    $input: DeleteGlobalTemplatesInput!
    $condition: ModelGlobalTemplatesConditionInput
  ) {
    deleteGlobalTemplates(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customTemplateID
      title
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const createSharedTemplates = /* GraphQL */ `
  mutation CreateSharedTemplates(
    $input: CreateSharedTemplatesInput!
    $condition: ModelSharedTemplatesConditionInput
  ) {
    createSharedTemplates(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      customTemplateID
      sharedAccountId
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const updateSharedTemplates = /* GraphQL */ `
  mutation UpdateSharedTemplates(
    $input: UpdateSharedTemplatesInput!
    $condition: ModelSharedTemplatesConditionInput
  ) {
    updateSharedTemplates(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      customTemplateID
      sharedAccountId
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const deleteSharedTemplates = /* GraphQL */ `
  mutation DeleteSharedTemplates(
    $input: DeleteSharedTemplatesInput!
    $condition: ModelSharedTemplatesConditionInput
  ) {
    deleteSharedTemplates(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      customTemplateID
      sharedAccountId
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const createFile = /* GraphQL */ `
  mutation CreateFile(
    $input: CreateFileInput!
    $condition: ModelFileConditionInput
  ) {
    createFile(input: $input, condition: $condition) {
      id
      name
      size
      location {
        bucket
        key
        region
      }
      type
      thumbnail {
        bucket
        key
        region
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateFile = /* GraphQL */ `
  mutation UpdateFile(
    $input: UpdateFileInput!
    $condition: ModelFileConditionInput
  ) {
    updateFile(input: $input, condition: $condition) {
      id
      name
      size
      location {
        bucket
        key
        region
      }
      type
      thumbnail {
        bucket
        key
        region
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteFile = /* GraphQL */ `
  mutation DeleteFile(
    $input: DeleteFileInput!
    $condition: ModelFileConditionInput
  ) {
    deleteFile(input: $input, condition: $condition) {
      id
      name
      size
      location {
        bucket
        key
        region
      }
      type
      thumbnail {
        bucket
        key
        region
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLtiPlatform = /* GraphQL */ `
  mutation CreateLtiPlatform(
    $input: CreateLtiPlatformInput!
    $condition: ModelLtiPlatformConditionInput
  ) {
    createLtiPlatform(input: $input, condition: $condition) {
      id
      clientId
      clientSecret
      platformName
      platformUrl
      authEndpoint
      accesstokenEndpoint
      publicKeyURL
      authConfig {
        method
        key
      }
      googleAnalyticsTrackingId
      ltiDeploymentId
      loreeOrganisationID
      loreeOrganisation {
        id
        name
        ltiPlatforms {
          items {
            id
            clientId
            clientSecret
            platformName
            platformUrl
            authEndpoint
            accesstokenEndpoint
            publicKeyURL
            googleAnalyticsTrackingId
            ltiDeploymentId
            loreeOrganisationID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      ltiApiKey {
        items {
          id
          ltiPlatformID
          loreeOrganisationID
          ltiClientID
          oauthLoginUrl
          oauthTokenUrl
          apiClientId
          apiSecretKey
          lmsApiUrl
          ltiAccessToken {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiPlatform = /* GraphQL */ `
  mutation UpdateLtiPlatform(
    $input: UpdateLtiPlatformInput!
    $condition: ModelLtiPlatformConditionInput
  ) {
    updateLtiPlatform(input: $input, condition: $condition) {
      id
      clientId
      clientSecret
      platformName
      platformUrl
      authEndpoint
      accesstokenEndpoint
      publicKeyURL
      authConfig {
        method
        key
      }
      googleAnalyticsTrackingId
      ltiDeploymentId
      loreeOrganisationID
      loreeOrganisation {
        id
        name
        ltiPlatforms {
          items {
            id
            clientId
            clientSecret
            platformName
            platformUrl
            authEndpoint
            accesstokenEndpoint
            publicKeyURL
            googleAnalyticsTrackingId
            ltiDeploymentId
            loreeOrganisationID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      ltiApiKey {
        items {
          id
          ltiPlatformID
          loreeOrganisationID
          ltiClientID
          oauthLoginUrl
          oauthTokenUrl
          apiClientId
          apiSecretKey
          lmsApiUrl
          ltiAccessToken {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiPlatform = /* GraphQL */ `
  mutation DeleteLtiPlatform(
    $input: DeleteLtiPlatformInput!
    $condition: ModelLtiPlatformConditionInput
  ) {
    deleteLtiPlatform(input: $input, condition: $condition) {
      id
      clientId
      clientSecret
      platformName
      platformUrl
      authEndpoint
      accesstokenEndpoint
      publicKeyURL
      authConfig {
        method
        key
      }
      googleAnalyticsTrackingId
      ltiDeploymentId
      loreeOrganisationID
      loreeOrganisation {
        id
        name
        ltiPlatforms {
          items {
            id
            clientId
            clientSecret
            platformName
            platformUrl
            authEndpoint
            accesstokenEndpoint
            publicKeyURL
            googleAnalyticsTrackingId
            ltiDeploymentId
            loreeOrganisationID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      ltiApiKey {
        items {
          id
          ltiPlatformID
          loreeOrganisationID
          ltiClientID
          oauthLoginUrl
          oauthTokenUrl
          apiClientId
          apiSecretKey
          lmsApiUrl
          ltiAccessToken {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLtiApiKey = /* GraphQL */ `
  mutation CreateLtiApiKey(
    $input: CreateLtiApiKeyInput!
    $condition: ModelLtiApiKeyConditionInput
  ) {
    createLtiApiKey(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      ltiClientID
      oauthLoginUrl
      oauthTokenUrl
      apiClientId
      apiSecretKey
      lmsApiUrl
      ltiAccessToken {
        items {
          id
          ltiApiKeyID
          ltiClientID
          ltiPlatformID
          loreeOrganisationID
          loreeUsername
          loreePassword
          user
          userInfo
          roles
          accessToken
          refreshToken
          isAdmin
          generatedAt
          lmsApiUrl
          expiresAt
          lms_email
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiApiKey = /* GraphQL */ `
  mutation UpdateLtiApiKey(
    $input: UpdateLtiApiKeyInput!
    $condition: ModelLtiApiKeyConditionInput
  ) {
    updateLtiApiKey(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      ltiClientID
      oauthLoginUrl
      oauthTokenUrl
      apiClientId
      apiSecretKey
      lmsApiUrl
      ltiAccessToken {
        items {
          id
          ltiApiKeyID
          ltiClientID
          ltiPlatformID
          loreeOrganisationID
          loreeUsername
          loreePassword
          user
          userInfo
          roles
          accessToken
          refreshToken
          isAdmin
          generatedAt
          lmsApiUrl
          expiresAt
          lms_email
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiApiKey = /* GraphQL */ `
  mutation DeleteLtiApiKey(
    $input: DeleteLtiApiKeyInput!
    $condition: ModelLtiApiKeyConditionInput
  ) {
    deleteLtiApiKey(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      ltiClientID
      oauthLoginUrl
      oauthTokenUrl
      apiClientId
      apiSecretKey
      lmsApiUrl
      ltiAccessToken {
        items {
          id
          ltiApiKeyID
          ltiClientID
          ltiPlatformID
          loreeOrganisationID
          loreeUsername
          loreePassword
          user
          userInfo
          roles
          accessToken
          refreshToken
          isAdmin
          generatedAt
          lmsApiUrl
          expiresAt
          lms_email
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLtiPlatformKey = /* GraphQL */ `
  mutation CreateLtiPlatformKey(
    $input: CreateLtiPlatformKeyInput!
    $condition: ModelLtiPlatformKeyConditionInput
  ) {
    createLtiPlatformKey(input: $input, condition: $condition) {
      id
      ltiPlatformID
      kid
      privatekey
      publicKey
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiPlatformKey = /* GraphQL */ `
  mutation UpdateLtiPlatformKey(
    $input: UpdateLtiPlatformKeyInput!
    $condition: ModelLtiPlatformKeyConditionInput
  ) {
    updateLtiPlatformKey(input: $input, condition: $condition) {
      id
      ltiPlatformID
      kid
      privatekey
      publicKey
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiPlatformKey = /* GraphQL */ `
  mutation DeleteLtiPlatformKey(
    $input: DeleteLtiPlatformKeyInput!
    $condition: ModelLtiPlatformKeyConditionInput
  ) {
    deleteLtiPlatformKey(input: $input, condition: $condition) {
      id
      ltiPlatformID
      kid
      privatekey
      publicKey
      createdAt
      updatedAt
    }
  }
`;
export const createLtiAccessToken = /* GraphQL */ `
  mutation CreateLtiAccessToken(
    $input: CreateLtiAccessTokenInput!
    $condition: ModelLtiAccessTokenConditionInput
  ) {
    createLtiAccessToken(input: $input, condition: $condition) {
      id
      ltiApiKeyID
      ltiClientID
      ltiPlatformID
      loreeOrganisationID
      loreeUsername
      loreePassword
      user
      userInfo
      roles
      accessToken
      refreshToken
      isAdmin
      generatedAt
      lmsApiUrl
      expiresAt
      lms_email
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiAccessToken = /* GraphQL */ `
  mutation UpdateLtiAccessToken(
    $input: UpdateLtiAccessTokenInput!
    $condition: ModelLtiAccessTokenConditionInput
  ) {
    updateLtiAccessToken(input: $input, condition: $condition) {
      id
      ltiApiKeyID
      ltiClientID
      ltiPlatformID
      loreeOrganisationID
      loreeUsername
      loreePassword
      user
      userInfo
      roles
      accessToken
      refreshToken
      isAdmin
      generatedAt
      lmsApiUrl
      expiresAt
      lms_email
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiAccessToken = /* GraphQL */ `
  mutation DeleteLtiAccessToken(
    $input: DeleteLtiAccessTokenInput!
    $condition: ModelLtiAccessTokenConditionInput
  ) {
    deleteLtiAccessToken(input: $input, condition: $condition) {
      id
      ltiApiKeyID
      ltiClientID
      ltiPlatformID
      loreeOrganisationID
      loreeUsername
      loreePassword
      user
      userInfo
      roles
      accessToken
      refreshToken
      isAdmin
      generatedAt
      lmsApiUrl
      expiresAt
      lms_email
      createdAt
      updatedAt
    }
  }
`;
export const createLtiLog = /* GraphQL */ `
  mutation CreateLtiLog(
    $input: CreateLtiLogInput!
    $condition: ModelLtiLogConditionInput
  ) {
    createLtiLog(input: $input, condition: $condition) {
      id
      ltiPlatformID
      userInfo
      clientInfo
      userGeoLocation
      userAction
      generatedAt
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiLog = /* GraphQL */ `
  mutation UpdateLtiLog(
    $input: UpdateLtiLogInput!
    $condition: ModelLtiLogConditionInput
  ) {
    updateLtiLog(input: $input, condition: $condition) {
      id
      ltiPlatformID
      userInfo
      clientInfo
      userGeoLocation
      userAction
      generatedAt
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiLog = /* GraphQL */ `
  mutation DeleteLtiLog(
    $input: DeleteLtiLogInput!
    $condition: ModelLtiLogConditionInput
  ) {
    deleteLtiLog(input: $input, condition: $condition) {
      id
      ltiPlatformID
      userInfo
      clientInfo
      userGeoLocation
      userAction
      generatedAt
      createdAt
      updatedAt
    }
  }
`;
export const createCustomBlock = /* GraphQL */ `
  mutation CreateCustomBlock(
    $input: CreateCustomBlockInput!
    $condition: ModelCustomBlockConditionInput
  ) {
    createCustomBlock(input: $input, condition: $condition) {
      id
      title
      categoryID
      ltiPlatformID
      loreeOrganisationID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      type
      content
      thumbnail {
        bucket
        key
        region
      }
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const updateCustomBlock = /* GraphQL */ `
  mutation UpdateCustomBlock(
    $input: UpdateCustomBlockInput!
    $condition: ModelCustomBlockConditionInput
  ) {
    updateCustomBlock(input: $input, condition: $condition) {
      id
      title
      categoryID
      ltiPlatformID
      loreeOrganisationID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      type
      content
      thumbnail {
        bucket
        key
        region
      }
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const deleteCustomBlock = /* GraphQL */ `
  mutation DeleteCustomBlock(
    $input: DeleteCustomBlockInput!
    $condition: ModelCustomBlockConditionInput
  ) {
    deleteCustomBlock(input: $input, condition: $condition) {
      id
      title
      categoryID
      ltiPlatformID
      loreeOrganisationID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      type
      content
      thumbnail {
        bucket
        key
        region
      }
      status
      isGlobal
      isShared
      createdBy
      active
      owner
      createdAt
      updatedAt
    }
  }
`;
export const createGlobalBlocks = /* GraphQL */ `
  mutation CreateGlobalBlocks(
    $input: CreateGlobalBlocksInput!
    $condition: ModelGlobalBlocksConditionInput
  ) {
    createGlobalBlocks(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customBlockID
      title
      type
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const updateGlobalBlocks = /* GraphQL */ `
  mutation UpdateGlobalBlocks(
    $input: UpdateGlobalBlocksInput!
    $condition: ModelGlobalBlocksConditionInput
  ) {
    updateGlobalBlocks(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customBlockID
      title
      type
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const deleteGlobalBlocks = /* GraphQL */ `
  mutation DeleteGlobalBlocks(
    $input: DeleteGlobalBlocksInput!
    $condition: ModelGlobalBlocksConditionInput
  ) {
    deleteGlobalBlocks(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customBlockID
      title
      type
      categoryID
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const createSharedBlocks = /* GraphQL */ `
  mutation CreateSharedBlocks(
    $input: CreateSharedBlocksInput!
    $condition: ModelSharedBlocksConditionInput
  ) {
    createSharedBlocks(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      type
      customBlockID
      categoryID
      sharedAccountId
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const updateSharedBlocks = /* GraphQL */ `
  mutation UpdateSharedBlocks(
    $input: UpdateSharedBlocksInput!
    $condition: ModelSharedBlocksConditionInput
  ) {
    updateSharedBlocks(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      type
      customBlockID
      categoryID
      sharedAccountId
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const deleteSharedBlocks = /* GraphQL */ `
  mutation DeleteSharedBlocks(
    $input: DeleteSharedBlocksInput!
    $condition: ModelSharedBlocksConditionInput
  ) {
    deleteSharedBlocks(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      title
      type
      customBlockID
      categoryID
      sharedAccountId
      category {
        id
        ltiPlatformID
        loreeOrganisationID
        name
        projects {
          items {
            id
            title
            categoryID
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      thumbnail {
        bucket
        key
        region
      }
      content
      status
      createdBy
      active
      createdAt
      updatedAt
    }
  }
`;
export const createCustomStyle = /* GraphQL */ `
  mutation CreateCustomStyle(
    $input: CreateCustomStyleInput!
    $condition: ModelCustomStyleConditionInput
  ) {
    createCustomStyle(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customColor
      customHeader
      customFont
      customLink
      createdAt
      updatedAt
    }
  }
`;
export const updateCustomStyle = /* GraphQL */ `
  mutation UpdateCustomStyle(
    $input: UpdateCustomStyleInput!
    $condition: ModelCustomStyleConditionInput
  ) {
    updateCustomStyle(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customColor
      customHeader
      customFont
      customLink
      createdAt
      updatedAt
    }
  }
`;
export const deleteCustomStyle = /* GraphQL */ `
  mutation DeleteCustomStyle(
    $input: DeleteCustomStyleInput!
    $condition: ModelCustomStyleConditionInput
  ) {
    deleteCustomStyle(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeOrganisationID
      customColor
      customHeader
      customFont
      customLink
      createdAt
      updatedAt
    }
  }
`;
export const createKalturaConfig = /* GraphQL */ `
  mutation CreateKalturaConfig(
    $input: CreateKalturaConfigInput!
    $condition: ModelKalturaConfigConditionInput
  ) {
    createKalturaConfig(input: $input, condition: $condition) {
      id
      ltiPlatformID
      partnerID
      appToken
      tokenID
      createdAt
      updatedAt
    }
  }
`;
export const updateKalturaConfig = /* GraphQL */ `
  mutation UpdateKalturaConfig(
    $input: UpdateKalturaConfigInput!
    $condition: ModelKalturaConfigConditionInput
  ) {
    updateKalturaConfig(input: $input, condition: $condition) {
      id
      ltiPlatformID
      partnerID
      appToken
      tokenID
      createdAt
      updatedAt
    }
  }
`;
export const deleteKalturaConfig = /* GraphQL */ `
  mutation DeleteKalturaConfig(
    $input: DeleteKalturaConfigInput!
    $condition: ModelKalturaConfigConditionInput
  ) {
    deleteKalturaConfig(input: $input, condition: $condition) {
      id
      ltiPlatformID
      partnerID
      appToken
      tokenID
      createdAt
      updatedAt
    }
  }
`;
export const createLoreeFeature = /* GraphQL */ `
  mutation CreateLoreeFeature(
    $input: CreateLoreeFeatureInput!
    $condition: ModelLoreeFeatureConditionInput
  ) {
    createLoreeFeature(input: $input, condition: $condition) {
      id
      ltiPlatformID
      featureList
      createdAt
      updatedAt
    }
  }
`;
export const updateLoreeFeature = /* GraphQL */ `
  mutation UpdateLoreeFeature(
    $input: UpdateLoreeFeatureInput!
    $condition: ModelLoreeFeatureConditionInput
  ) {
    updateLoreeFeature(input: $input, condition: $condition) {
      id
      ltiPlatformID
      featureList
      createdAt
      updatedAt
    }
  }
`;
export const deleteLoreeFeature = /* GraphQL */ `
  mutation DeleteLoreeFeature(
    $input: DeleteLoreeFeatureInput!
    $condition: ModelLoreeFeatureConditionInput
  ) {
    deleteLoreeFeature(input: $input, condition: $condition) {
      id
      ltiPlatformID
      featureList
      createdAt
      updatedAt
    }
  }
`;
export const createLoreeRole = /* GraphQL */ `
  mutation CreateLoreeRole(
    $input: CreateLoreeRoleInput!
    $condition: ModelLoreeRoleConditionInput
  ) {
    createLoreeRole(input: $input, condition: $condition) {
      id
      ltiPlatformID
      name
      description
      featureList
      lmsLoreeRoles {
        items {
          id
          ltiPlatformID
          loreeRoleID {
            id
            ltiPlatformID
            name
            description
            featureList
            createdAt
            updatedAt
          }
          lmsRole
          lmsBaseRoleType
          lmsRoleId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLoreeRole = /* GraphQL */ `
  mutation UpdateLoreeRole(
    $input: UpdateLoreeRoleInput!
    $condition: ModelLoreeRoleConditionInput
  ) {
    updateLoreeRole(input: $input, condition: $condition) {
      id
      ltiPlatformID
      name
      description
      featureList
      lmsLoreeRoles {
        items {
          id
          ltiPlatformID
          loreeRoleID {
            id
            ltiPlatformID
            name
            description
            featureList
            createdAt
            updatedAt
          }
          lmsRole
          lmsBaseRoleType
          lmsRoleId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLoreeRole = /* GraphQL */ `
  mutation DeleteLoreeRole(
    $input: DeleteLoreeRoleInput!
    $condition: ModelLoreeRoleConditionInput
  ) {
    deleteLoreeRole(input: $input, condition: $condition) {
      id
      ltiPlatformID
      name
      description
      featureList
      lmsLoreeRoles {
        items {
          id
          ltiPlatformID
          loreeRoleID {
            id
            ltiPlatformID
            name
            description
            featureList
            createdAt
            updatedAt
          }
          lmsRole
          lmsBaseRoleType
          lmsRoleId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLmsLoreeRole = /* GraphQL */ `
  mutation CreateLmsLoreeRole(
    $input: CreateLmsLoreeRoleInput!
    $condition: ModelLmsLoreeRoleConditionInput
  ) {
    createLmsLoreeRole(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeRoleID {
        id
        ltiPlatformID
        name
        description
        featureList
        lmsLoreeRoles {
          items {
            id
            ltiPlatformID
            lmsRole
            lmsBaseRoleType
            lmsRoleId
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      lmsRole
      lmsBaseRoleType
      lmsRoleId
      createdAt
      updatedAt
    }
  }
`;
export const updateLmsLoreeRole = /* GraphQL */ `
  mutation UpdateLmsLoreeRole(
    $input: UpdateLmsLoreeRoleInput!
    $condition: ModelLmsLoreeRoleConditionInput
  ) {
    updateLmsLoreeRole(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeRoleID {
        id
        ltiPlatformID
        name
        description
        featureList
        lmsLoreeRoles {
          items {
            id
            ltiPlatformID
            lmsRole
            lmsBaseRoleType
            lmsRoleId
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      lmsRole
      lmsBaseRoleType
      lmsRoleId
      createdAt
      updatedAt
    }
  }
`;
export const deleteLmsLoreeRole = /* GraphQL */ `
  mutation DeleteLmsLoreeRole(
    $input: DeleteLmsLoreeRoleInput!
    $condition: ModelLmsLoreeRoleConditionInput
  ) {
    deleteLmsLoreeRole(input: $input, condition: $condition) {
      id
      ltiPlatformID
      loreeRoleID {
        id
        ltiPlatformID
        name
        description
        featureList
        lmsLoreeRoles {
          items {
            id
            ltiPlatformID
            lmsRole
            lmsBaseRoleType
            lmsRoleId
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      lmsRole
      lmsBaseRoleType
      lmsRoleId
      createdAt
      updatedAt
    }
  }
`;
export const createHFivePUsers = /* GraphQL */ `
  mutation CreateHFivePUsers(
    $input: CreateHFivePUsersInput!
    $condition: ModelHFivePUsersConditionInput
  ) {
    createHFivePUsers(input: $input, condition: $condition) {
      id
      email
      loreeUser
      ltiPlatformID
      createdAt
      updatedAt
    }
  }
`;
export const updateHFivePUsers = /* GraphQL */ `
  mutation UpdateHFivePUsers(
    $input: UpdateHFivePUsersInput!
    $condition: ModelHFivePUsersConditionInput
  ) {
    updateHFivePUsers(input: $input, condition: $condition) {
      id
      email
      loreeUser
      ltiPlatformID
      createdAt
      updatedAt
    }
  }
`;
export const deleteHFivePUsers = /* GraphQL */ `
  mutation DeleteHFivePUsers(
    $input: DeleteHFivePUsersInput!
    $condition: ModelHFivePUsersConditionInput
  ) {
    deleteHFivePUsers(input: $input, condition: $condition) {
      id
      email
      loreeUser
      ltiPlatformID
      createdAt
      updatedAt
    }
  }
`;
export const createInteractivesMetaData = /* GraphQL */ `
  mutation CreateInteractivesMetaData(
    $input: CreateInteractivesMetaDataInput!
    $condition: ModelInteractivesMetaDataConditionInput
  ) {
    createInteractivesMetaData(input: $input, condition: $condition) {
      id
      ltiPlatformID
      metaKey
      metaValue
      isRetired
      createdAt
      updatedAt
    }
  }
`;
export const updateInteractivesMetaData = /* GraphQL */ `
  mutation UpdateInteractivesMetaData(
    $input: UpdateInteractivesMetaDataInput!
    $condition: ModelInteractivesMetaDataConditionInput
  ) {
    updateInteractivesMetaData(input: $input, condition: $condition) {
      id
      ltiPlatformID
      metaKey
      metaValue
      isRetired
      createdAt
      updatedAt
    }
  }
`;
export const deleteInteractivesMetaData = /* GraphQL */ `
  mutation DeleteInteractivesMetaData(
    $input: DeleteInteractivesMetaDataInput!
    $condition: ModelInteractivesMetaDataConditionInput
  ) {
    deleteInteractivesMetaData(input: $input, condition: $condition) {
      id
      ltiPlatformID
      metaKey
      metaValue
      isRetired
      createdAt
      updatedAt
    }
  }
`;
export const createLmsImage = /* GraphQL */ `
  mutation CreateLmsImage(
    $input: CreateLmsImageInput!
    $condition: ModelLmsImageConditionInput
  ) {
    createLmsImage(input: $input, condition: $condition) {
      id
      lms
      lmsImageId
      lmsImageUUID
      lmsImageName
      lmsImageDisplayName
      lmsImageURL
      lmsImageType
      s3ObjectKey
      s3PublicObjectKey
      s3GlobalTemplateKey
      s3GlobalRowsKey
      s3GlobalElementsKey
      s3SharedTemplateKey
      s3SharedRowsKey
      s3SharedElementsKey
      lmsImageMetaData
      s3MetaData
      createdAt
      updatedAt
    }
  }
`;
export const updateLmsImage = /* GraphQL */ `
  mutation UpdateLmsImage(
    $input: UpdateLmsImageInput!
    $condition: ModelLmsImageConditionInput
  ) {
    updateLmsImage(input: $input, condition: $condition) {
      id
      lms
      lmsImageId
      lmsImageUUID
      lmsImageName
      lmsImageDisplayName
      lmsImageURL
      lmsImageType
      s3ObjectKey
      s3PublicObjectKey
      s3GlobalTemplateKey
      s3GlobalRowsKey
      s3GlobalElementsKey
      s3SharedTemplateKey
      s3SharedRowsKey
      s3SharedElementsKey
      lmsImageMetaData
      s3MetaData
      createdAt
      updatedAt
    }
  }
`;
export const deleteLmsImage = /* GraphQL */ `
  mutation DeleteLmsImage(
    $input: DeleteLmsImageInput!
    $condition: ModelLmsImageConditionInput
  ) {
    deleteLmsImage(input: $input, condition: $condition) {
      id
      lms
      lmsImageId
      lmsImageUUID
      lmsImageName
      lmsImageDisplayName
      lmsImageURL
      lmsImageType
      s3ObjectKey
      s3PublicObjectKey
      s3GlobalTemplateKey
      s3GlobalRowsKey
      s3GlobalElementsKey
      s3SharedTemplateKey
      s3SharedRowsKey
      s3SharedElementsKey
      lmsImageMetaData
      s3MetaData
      createdAt
      updatedAt
    }
  }
`;
export const createLoreeOrganisation = /* GraphQL */ `
  mutation CreateLoreeOrganisation(
    $input: CreateLoreeOrganisationInput!
    $condition: ModelLoreeOrganisationConditionInput
  ) {
    createLoreeOrganisation(input: $input, condition: $condition) {
      id
      name
      ltiPlatforms {
        items {
          id
          clientId
          clientSecret
          platformName
          platformUrl
          authEndpoint
          accesstokenEndpoint
          publicKeyURL
          authConfig {
            method
            key
          }
          googleAnalyticsTrackingId
          ltiDeploymentId
          loreeOrganisationID
          loreeOrganisation {
            id
            name
            createdAt
            updatedAt
          }
          ltiApiKey {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLoreeOrganisation = /* GraphQL */ `
  mutation UpdateLoreeOrganisation(
    $input: UpdateLoreeOrganisationInput!
    $condition: ModelLoreeOrganisationConditionInput
  ) {
    updateLoreeOrganisation(input: $input, condition: $condition) {
      id
      name
      ltiPlatforms {
        items {
          id
          clientId
          clientSecret
          platformName
          platformUrl
          authEndpoint
          accesstokenEndpoint
          publicKeyURL
          authConfig {
            method
            key
          }
          googleAnalyticsTrackingId
          ltiDeploymentId
          loreeOrganisationID
          loreeOrganisation {
            id
            name
            createdAt
            updatedAt
          }
          ltiApiKey {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLoreeOrganisation = /* GraphQL */ `
  mutation DeleteLoreeOrganisation(
    $input: DeleteLoreeOrganisationInput!
    $condition: ModelLoreeOrganisationConditionInput
  ) {
    deleteLoreeOrganisation(input: $input, condition: $condition) {
      id
      name
      ltiPlatforms {
        items {
          id
          clientId
          clientSecret
          platformName
          platformUrl
          authEndpoint
          accesstokenEndpoint
          publicKeyURL
          authConfig {
            method
            key
          }
          googleAnalyticsTrackingId
          ltiDeploymentId
          loreeOrganisationID
          loreeOrganisation {
            id
            name
            createdAt
            updatedAt
          }
          ltiApiKey {
            nextToken
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLoreeUser = /* GraphQL */ `
  mutation CreateLoreeUser(
    $input: CreateLoreeUserInput!
    $condition: ModelLoreeUserConditionInput
  ) {
    createLoreeUser(input: $input, condition: $condition) {
      id
      loreeOrganisationID
      name
      email
      username
      externalIds {
        key
        value
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLoreeUser = /* GraphQL */ `
  mutation UpdateLoreeUser(
    $input: UpdateLoreeUserInput!
    $condition: ModelLoreeUserConditionInput
  ) {
    updateLoreeUser(input: $input, condition: $condition) {
      id
      loreeOrganisationID
      name
      email
      username
      externalIds {
        key
        value
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLoreeUser = /* GraphQL */ `
  mutation DeleteLoreeUser(
    $input: DeleteLoreeUserInput!
    $condition: ModelLoreeUserConditionInput
  ) {
    deleteLoreeUser(input: $input, condition: $condition) {
      id
      loreeOrganisationID
      name
      email
      username
      externalIds {
        key
        value
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLtiTool = /* GraphQL */ `
  mutation CreateLtiTool(
    $input: CreateLtiToolInput!
    $condition: ModelLtiToolConditionInput
  ) {
    createLtiTool(input: $input, condition: $condition) {
      id
      ltiPlatformID
      toolName
      issuerUrl
      oidcUrl
      redirectURI
      targetLinkURI
      jwksUrl
      domainName
      clientId
      clientSecret
      toolDeployments {
        items {
          id
          toolId {
            id
            ltiPlatformID
            toolName
            issuerUrl
            oidcUrl
            redirectURI
            targetLinkURI
            jwksUrl
            domainName
            clientId
            clientSecret
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiTool = /* GraphQL */ `
  mutation UpdateLtiTool(
    $input: UpdateLtiToolInput!
    $condition: ModelLtiToolConditionInput
  ) {
    updateLtiTool(input: $input, condition: $condition) {
      id
      ltiPlatformID
      toolName
      issuerUrl
      oidcUrl
      redirectURI
      targetLinkURI
      jwksUrl
      domainName
      clientId
      clientSecret
      toolDeployments {
        items {
          id
          toolId {
            id
            ltiPlatformID
            toolName
            issuerUrl
            oidcUrl
            redirectURI
            targetLinkURI
            jwksUrl
            domainName
            clientId
            clientSecret
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiTool = /* GraphQL */ `
  mutation DeleteLtiTool(
    $input: DeleteLtiToolInput!
    $condition: ModelLtiToolConditionInput
  ) {
    deleteLtiTool(input: $input, condition: $condition) {
      id
      ltiPlatformID
      toolName
      issuerUrl
      oidcUrl
      redirectURI
      targetLinkURI
      jwksUrl
      domainName
      clientId
      clientSecret
      toolDeployments {
        items {
          id
          toolId {
            id
            ltiPlatformID
            toolName
            issuerUrl
            oidcUrl
            redirectURI
            targetLinkURI
            jwksUrl
            domainName
            clientId
            clientSecret
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createLtiToolLoginHint = /* GraphQL */ `
  mutation CreateLtiToolLoginHint(
    $input: CreateLtiToolLoginHintInput!
    $condition: ModelLtiToolLoginHintConditionInput
  ) {
    createLtiToolLoginHint(input: $input, condition: $condition) {
      id
      issuerUrl
      clientId
      loreeUserEmail
      messageType
      resourceLink
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiToolLoginHint = /* GraphQL */ `
  mutation UpdateLtiToolLoginHint(
    $input: UpdateLtiToolLoginHintInput!
    $condition: ModelLtiToolLoginHintConditionInput
  ) {
    updateLtiToolLoginHint(input: $input, condition: $condition) {
      id
      issuerUrl
      clientId
      loreeUserEmail
      messageType
      resourceLink
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiToolLoginHint = /* GraphQL */ `
  mutation DeleteLtiToolLoginHint(
    $input: DeleteLtiToolLoginHintInput!
    $condition: ModelLtiToolLoginHintConditionInput
  ) {
    deleteLtiToolLoginHint(input: $input, condition: $condition) {
      id
      issuerUrl
      clientId
      loreeUserEmail
      messageType
      resourceLink
      createdAt
      updatedAt
    }
  }
`;
export const createLtiToolDeployment = /* GraphQL */ `
  mutation CreateLtiToolDeployment(
    $input: CreateLtiToolDeploymentInput!
    $condition: ModelLtiToolDeploymentConditionInput
  ) {
    createLtiToolDeployment(input: $input, condition: $condition) {
      id
      toolId {
        id
        ltiPlatformID
        toolName
        issuerUrl
        oidcUrl
        redirectURI
        targetLinkURI
        jwksUrl
        domainName
        clientId
        clientSecret
        toolDeployments {
          items {
            id
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateLtiToolDeployment = /* GraphQL */ `
  mutation UpdateLtiToolDeployment(
    $input: UpdateLtiToolDeploymentInput!
    $condition: ModelLtiToolDeploymentConditionInput
  ) {
    updateLtiToolDeployment(input: $input, condition: $condition) {
      id
      toolId {
        id
        ltiPlatformID
        toolName
        issuerUrl
        oidcUrl
        redirectURI
        targetLinkURI
        jwksUrl
        domainName
        clientId
        clientSecret
        toolDeployments {
          items {
            id
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteLtiToolDeployment = /* GraphQL */ `
  mutation DeleteLtiToolDeployment(
    $input: DeleteLtiToolDeploymentInput!
    $condition: ModelLtiToolDeploymentConditionInput
  ) {
    deleteLtiToolDeployment(input: $input, condition: $condition) {
      id
      toolId {
        id
        ltiPlatformID
        toolName
        issuerUrl
        oidcUrl
        redirectURI
        targetLinkURI
        jwksUrl
        domainName
        clientId
        clientSecret
        toolDeployments {
          items {
            id
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createCustomTheme = /* GraphQL */ `
  mutation CreateCustomTheme(
    $input: CreateCustomThemeInput!
    $condition: ModelCustomThemeConditionInput
  ) {
    createCustomTheme(input: $input, condition: $condition) {
      id
      ltiPlatformID
      isShared
      themeData
      createdAt
      updatedAt
    }
  }
`;
export const updateCustomTheme = /* GraphQL */ `
  mutation UpdateCustomTheme(
    $input: UpdateCustomThemeInput!
    $condition: ModelCustomThemeConditionInput
  ) {
    updateCustomTheme(input: $input, condition: $condition) {
      id
      ltiPlatformID
      isShared
      themeData
      createdAt
      updatedAt
    }
  }
`;
export const deleteCustomTheme = /* GraphQL */ `
  mutation DeleteCustomTheme(
    $input: DeleteCustomThemeInput!
    $condition: ModelCustomThemeConditionInput
  ) {
    deleteCustomTheme(input: $input, condition: $condition) {
      id
      ltiPlatformID
      isShared
      themeData
      createdAt
      updatedAt
    }
  }
`;
