import { configureAxe } from 'jest-axe';
import { a11yService } from './a11y-service';

const axe = configureAxe({
  rules: {
    region: { enabled: false },
    ...a11yService.experimentalRules,
  },
});

export default axe;
