import 'aws-amplify';
import { DocumentNode } from 'graphql/language/ast';
import { GraphQLOptions, GraphQLResult } from '@aws-amplify/api-graphql';

declare module 'aws-amplify' {
  export declare const graphqlOperation: <Q extends string | DocumentNode, V extends {}>(
    query: Q,
    variables: V = {},
  ) => { query: Q; variables: V };

  export class APIClass {
    graphql<Data>(
      options: GraphQLOptions,
      additionalHeaders?: { [key: string]: string },
    ): Promise<GraphQLResult<Data>>;
  }
}
