import i18next from 'i18next';
import { useTranslation } from 'react-i18next';

export const translate = i18next.t;

export function useLoreeTranslation() {
  const res = useTranslation('loree');
  return { t: res.t };
}

export function useA11yTranslation() {
  const res = useTranslation('a11y');
  return { t: res.t };
}
