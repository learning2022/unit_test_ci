/* The file is to disable features until they are ready for prime time */
const featureFlags = {
  thememanager: false,
};
export default featureFlags;
