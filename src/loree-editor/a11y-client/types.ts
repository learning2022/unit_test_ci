import type axeCore from 'axe-core';

export enum CustomRule {
  NoStrike = 'no-strike',
  ValidTableCaption = 'valid-table-caption',
  NoImageTitle = 'no-image-title',
  TableHeaderId = 'table-header-id-association',
  TableWhiteSpaceId = 'tableWhiteSpace',
  TableRolePresentationId = 'tableRolePresentation',
  TableNecessaryLayoutTh = 'tableNecessaryLayoutTh',
  TableNecessaryLayoutSummary = 'tableNecessaryLayoutSummary',
}

export enum CustomCheck {
  CaptionIsFirstChild = 'caption-is-first-child-check',
  StrikeCheck = 'strike-check',
  ImageTitleCheck = 'image-title-check',
  TableHeaderIdCheck = 'table-header-id-check',
  TableWhiteSpaceIdCheck = 'tableWhiteSpaceCheck',
  TableRolePresentationIdCheck = 'tableRolePresentationCheck',
  TableNecessaryLayoutThCheck = 'tableNecessaryLayoutThCheck',
  TableNecessaryLayoutSummaryCheck = 'tableNecessaryLayoutSummaryCheck',
}

export const TAG_CUSTOM = 'custom';

export interface IFrameWindow extends Window {
  axe?: typeof axeCore;
}
