import CONSTANTS from '../../constant';
import { init } from '../init';
import { a11yGlobalContext } from '../../../a11y-service/context/a11y-context';

import { A11yContextProvider } from '../../../a11y-service/context/a11y-provider';
import { quickFixer } from '../quick-fixer';
import React from 'react';
import { render } from '@testing-library/react';
import { CustomRule } from '../types';
import { appendElementToBody, getEditorElementById } from '../../utils';
import { createElement, createDiv } from '../../common/dom';
import {
  tableHeaderIdAssociationCheckup,
  tableWhiteSpaceChecker,
  tablePresentationRoleChecker,
  tableThChecker,
  tableNonEmptySummaryChecker,
} from './CustomRuleFunctions';

describe('Table header-id association', () => {
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createElement('div', CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  function addToContentWrapper(element: HTMLElement) {
    const wrapperContent = getEditorElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    wrapperContent?.append(element);
  }

  beforeEach(() => {
    document.body.innerHTML = '';
    render(<A11yContextProvider quickFixer={quickFixer} />);

    const a11ySpacer = createDiv(CONSTANTS.LOREE_SPACE_A11Y_CHECKER);
    appendElementToBody(a11ySpacer);

    init();

    a11yGlobalContext.value.isProcessing = false;
    a11yGlobalContext.value.setContext({
      include: ['#loree-iframe', '#loree-iframe-content-wrapper'],
    });
    loreeWrapper();
  });
  test('Table with no association', async () => {
    const tableElement = createElement('table', 'tablewithoutAssociation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th id="a1">First</th><th id="a2">Second</th></tr><tr><td>1</td><td>2</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.TableHeaderId);
  });
  test('Table with association', async () => {
    const tableElement = createElement('table', 'tablewithAssociation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th id="a1">First</th><th id="a2">Second</th></tr><tr><td headers="a1">1</td><td headers="a2">2</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).not.toBe(CustomRule.TableHeaderId);
  });
  test('TableHeaderIdCheckAssociation', () => {
    const tableElement = createElement('table', 'tablewithAssociation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th id="a1">First</th><th id="a2">Second</th></tr><tr><td headers="a1">1</td><td headers="a2">2</td></tr>`;
    expect(tableHeaderIdAssociationCheckup(tableElement)).toBe(true);
  });
  test.each([
    { tableData: 'Table Data', expected: true },
    { tableData: '', expected: false },
    { tableData: '     ', expected: false },
  ])('Table White space rule for table data tag(<td>)', ({ tableData, expected }) => {
    const tableElement = createElement('table', 'whiteSpaceIssue');
    tableElement.innerHTML = `<tr><th>Table header</th></tr><tr><td>` + tableData + `</td></tr>`;
    expect(tableWhiteSpaceChecker(tableElement)).toBe(expected);
  });
  test.each([
    { tableHead: 'Table Header', expected: true },
    { tableHead: '', expected: false },
    { tableHead: '     ', expected: false },
  ])('Table White space rule for table head tag(<th>)', ({ tableHead, expected }) => {
    const tableElement = createElement('table', 'whiteSpaceIssue');
    tableElement.innerHTML = `<tr><th>` + tableHead + `</th></tr><tr><td>Table Data</td></tr>`;
    expect(tableWhiteSpaceChecker(tableElement)).toBe(expected);
  });
  test('Table with whitespace in editor issue', async () => {
    const tableElement = createElement('table', 'tablewithWhitespace');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th>First</th><th>Second</th></tr><tr><td>1</td><td></td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.TableWhiteSpaceId);
  });
  test.each([
    { roleValue: 'presentation', expected: false },
    { roleValue: 'Presentation', expected: false },
    { roleValue: 'preSENtatION', expected: false },
    { roleValue: 'NotPresentation', expected: true },
  ])('Table element presentation role', ({ roleValue, expected }) => {
    const tableElement = createElement('table', 'presentationRoleIssue');
    tableElement.setAttribute('role', roleValue);
    expect(tablePresentationRoleChecker(tableElement)).toBe(expected);
  });
  test('Table with role presentation editor issue', async () => {
    const tableElement = createElement('table', 'tablewithAssociation');
    tableElement.setAttribute('role', 'presentation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th>First</th></tr><tr><td>1</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.TableRolePresentationId);
  });
  test('#Table element has no <th> tag', () => {
    const tableElement = createElement('table', 'tableHasNoThTag');
    tableElement.innerHTML =
      '<caption>This is a caption</caption><tr><td>First table data</td></tr>';
    expect(tableThChecker(tableElement)).toBeFalsy();
  });
  test('Table without table header in A11y checker', async () => {
    const tableElement = createElement('table', 'tablewithNoHeader');
    tableElement.innerHTML = `<caption>This is a caption</caption><tr><td>First table data</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.TableNecessaryLayoutTh);
  });
  test.each([
    { summaryValue: '', expected: false },
    { summaryValue: ' ', expected: false },
    { summaryValue: 'This is a table summary content', expected: true },
  ])('#Table element has non-summary attribute', ({ summaryValue, expected }) => {
    const tableElement = createElement('table', 'tableSummaryTestcases');
    tableElement.setAttribute('summary', summaryValue);
    expect(tableNonEmptySummaryChecker(tableElement)).toBe(expected);
  });
  test('Table empty summary table header in A11y checker', async () => {
    const tableElement = createElement('table', 'tablewithNonSummary');
    tableElement.setAttribute('summary', '');
    tableElement.innerHTML = `<caption>This is a caption</caption><tr><th>First table header</th></tr><tr><td>First table data</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.TableNecessaryLayoutSummary);
  });
});
