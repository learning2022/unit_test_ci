import { CheckAugmented } from 'axe-core';
import { CustomCheck } from '../types';

export const strikeCheck: CheckAugmented = {
  id: CustomCheck.StrikeCheck,
  enabled: true,
  evaluate: (node) => {
    return node.tagName === 'STRIKE';
  },
  metadata: {
    messages: {
      // fail: 'Some message',
    },
  },
};
