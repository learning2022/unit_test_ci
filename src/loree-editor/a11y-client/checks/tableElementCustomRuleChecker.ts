import { CheckAugmented } from 'axe-core';
import { CustomCheck } from '../types';
import {
  tableHeaderIdAssociationCheckup,
  tableWhiteSpaceChecker,
  tablePresentationRoleChecker,
  tableThChecker,
  tableNonEmptySummaryChecker,
} from './CustomRuleFunctions';

export const TableHeaderIdChecker: CheckAugmented = {
  id: CustomCheck.TableHeaderIdCheck,
  enabled: true,
  evaluate: (node) => tableHeaderIdAssociationCheckup(node),
  metadata: {
    messages: {
      fail: 'The failure occurs when the relationship between data cells and corresponding header cells cannot be programmatically determined correctly because the association of id and headers attributes is faulty.',
    },
  },
};

export const TableWhiteSpaceIdChecker: CheckAugmented = {
  id: CustomCheck.TableWhiteSpaceIdCheck,
  enabled: true,
  evaluate: (node) => tableWhiteSpaceChecker(node),
  metadata: {
    messages: {
      fail: 'For every header/cell in the table should contains a value',
    },
  },
};

export const TablePresentationRoleIdChecker: CheckAugmented = {
  id: CustomCheck.TableRolePresentationIdCheck,
  enabled: true,
  evaluate: (node) => tablePresentationRoleChecker(node),
  metadata: {
    messages: {
      fail: `Role = 'presentation' should not be tolerated for table element`,
    },
  },
};

export const TableNecessaryLayoutThChecker: CheckAugmented = {
  id: CustomCheck.TableNecessaryLayoutThCheck,
  enabled: true,
  evaluate: (node) => tableThChecker(node),
  metadata: {
    messages: {
      fail: `Table element must have <TH> tag`,
    },
  },
};

export const TableNecessaryLayoutSummaryChecker: CheckAugmented = {
  id: CustomCheck.TableNecessaryLayoutSummaryCheck,
  enabled: true,
  evaluate: (node) => tableNonEmptySummaryChecker(node),
  metadata: {
    messages: {
      fail: `Table Summary attribute should not be an empty value`,
    },
  },
};
