import { CheckAugmented } from 'axe-core';
import { CustomCheck } from '../types';

export const ImageTitleCheck: CheckAugmented = {
  id: CustomCheck.ImageTitleCheck,
  enabled: true,
  evaluate: (node) => {
    return !node.hasAttribute('title');
  },
  metadata: {
    messages: {
      fail: '<Image> Elements must not have title attribute',
    },
  },
};
