import { CheckAugmented } from 'axe-core';
import { CustomCheck } from '../types';
import { tableHeaderIdAssociationCheckup } from './CustomRuleFunctions';

export const TableHeaderIdChecker: CheckAugmented = {
  id: CustomCheck.TableHeaderIdCheck,
  enabled: true,
  evaluate: (node) => tableHeaderIdAssociationCheckup(node),
  metadata: {
    messages: {
      fail: 'The failure occurs when the relationship between data cells and corresponding header cells cannot be programmatically determined correctly because the association of id and headers attributes is faulty.',
    },
  },
};
