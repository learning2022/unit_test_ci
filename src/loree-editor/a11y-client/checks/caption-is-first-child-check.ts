import { CheckAugmented } from 'axe-core';
import { CustomCheck } from '../types';

export const captionIsFirstChildCheck: CheckAugmented = {
  id: CustomCheck.CaptionIsFirstChild,
  enabled: true,
  evaluate: (node) => {
    return node.children[0].tagName === 'CAPTION';
  },
  metadata: {
    messages: {
      fail: '<caption> Elements must be the first child of the table',
    },
  },
};
