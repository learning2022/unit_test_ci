import CONSTANTS from '../../constant';
import { init } from '../init';
import { a11yGlobalContext } from '../../../a11y-service/context/a11y-context';

import { A11yContextProvider } from '../../../a11y-service/context/a11y-provider';
import { quickFixer } from '../quick-fixer';
import React from 'react';
import { render } from '@testing-library/react';
import { CustomRule } from '../types';
import { appendElementToBody, getEditorElementById } from '../../utils';
import { createElement, createDiv } from '../../common/dom';
import { tableHeaderIdAssociationCheckup } from './CustomRuleFunctions';

describe('Table header-id association', () => {
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createElement('div', CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  function addToContentWrapper(element: HTMLElement) {
    const wrapperContent = getEditorElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    wrapperContent?.append(element);
  }

  beforeEach(() => {
    document.body.innerHTML = '';
    render(<A11yContextProvider quickFixer={quickFixer} />);

    const a11ySpacer = createDiv(CONSTANTS.LOREE_SPACE_A11Y_CHECKER);
    appendElementToBody(a11ySpacer);

    init();

    a11yGlobalContext.value.isProcessing = false;
    a11yGlobalContext.value.setContext({
      include: ['#loree-iframe', '#loree-iframe-content-wrapper'],
    });
    loreeWrapper();
  });
  test('Table with no association', async () => {
    const tableElement = createElement('table', 'tablewithoutAssociation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th id="a1">First</th><th id="a2">Second</th></tr><tr><td>1</td><td>2</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.TableHeaderId);
  });
  test('Table with association', async () => {
    const tableElement = createElement('table', 'tablewithAssociation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th id="a1">First</th><th id="a2">Second</th></tr><tr><td headers="a1">1</td><td headers="a2">2</td></tr>`;
    addToContentWrapper(tableElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).not.toBe(CustomRule.TableHeaderId);
  });
  test('TableHeaderIdCheckAssociation', () => {
    const tableElement = createElement('table', 'tablewithAssociation');
    tableElement.innerHTML = `<caption>For testing table association</caption><tr><th id="a1">First</th><th id="a2">Second</th></tr><tr><td headers="a1">1</td><td headers="a2">2</td></tr>`;
    expect(tableHeaderIdAssociationCheckup(tableElement)).toBe(true);
  });
});
