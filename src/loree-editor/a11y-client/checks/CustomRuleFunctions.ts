export function tableHeaderIdAssociationCheckup(node: HTMLElement | SVGElement) {
  let tDataCounter = 0;
  let tHeadFlag = false;
  const theaderElement = Array.from(node.getElementsByTagName('th'));
  for (const value of theaderElement) {
    if (value.hasAttribute('id')) {
      tHeadFlag = true;
    }
  }
  if (tHeadFlag) {
    const tDataElement = node.getElementsByTagName('td');
    let child = tDataElement[tDataCounter] as Node | null;
    while (child !== null) {
      if (child.nodeName === 'TD') {
        const childElement = child as HTMLElement;
        if (!childElement.hasAttribute('headers')) {
          return false;
        }
      }
      if (child.nextSibling === child.lastChild) {
        tDataCounter++;
        if (tDataCounter === tDataElement.length) {
          break;
        }
        child = tDataElement[tDataCounter];
      } else {
        child = child.nextSibling;
      }
    }
  }
  return true;
}

export function tableWhiteSpaceChecker(node: HTMLElement | SVGElement) {
  const theaderElement = Array.from(node.getElementsByTagName('th'));
  const tDataElement = node.getElementsByTagName('td');
  for (const value of theaderElement) {
    if (value.textContent?.trim() === '') {
      return false;
    }
  }
  for (const value of tDataElement) {
    if (value.textContent?.trim() === '') {
      return false;
    }
  }
  return true;
}

export function tablePresentationRoleChecker(node: HTMLElement | SVGElement) {
  if (node.hasAttribute('role')) {
    return node.getAttribute('role')?.toLowerCase().trim() !== 'presentation';
  }
  return true;
}

export function tableThChecker(node: HTMLElement | SVGElement) {
  if (node.getElementsByTagName('th').length > 0) {
    return true;
  }
  return false;
}

export function tableNonEmptySummaryChecker(node: HTMLElement | SVGElement) {
  if (node.hasAttribute('summary')) {
    return node.getAttribute('summary')?.trim() !== '';
  }
  return true;
}
