import CONSTANTS from '../../constant';
import { init } from '../init';
import { a11yGlobalContext } from '../../../a11y-service/context/a11y-context';

import { A11yContextProvider } from '../../../../src/a11y-service/context/a11y-provider';
import { quickFixer } from '../../../../src/loree-editor/a11y-client/quick-fixer';
import React from 'react';
import { render } from '@testing-library/react';
import { CustomRule } from '../types';
import { appendElementToBody, getEditorElementById } from '../../utils';
import { createElement, createDiv } from '../../common/dom';

describe('Image title attribute - custom rules', () => {
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createElement('div', CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  function addToContentWrapper(element: HTMLElement) {
    const wrapperContent = getEditorElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    wrapperContent?.append(element);
  }

  beforeEach(() => {
    document.body.innerHTML = '';
    render(<A11yContextProvider quickFixer={quickFixer} />);

    const a11ySpacer = createDiv(CONSTANTS.LOREE_SPACE_A11Y_CHECKER);
    appendElementToBody(a11ySpacer);

    init();

    a11yGlobalContext.value.isProcessing = false;
    a11yGlobalContext.value.setContext({
      include: ['#loree-iframe', '#loree-iframe-content-wrapper'],
    });
    loreeWrapper();
  });
  test('Image element with title attribute', async () => {
    const imageElement = createElement('img', 'firstImageElement');
    imageElement.setAttribute('title', 'this is an issue');
    addToContentWrapper(imageElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.NoImageTitle);
  });
  test('Image Element without title attribute', async () => {
    const imageElement = createElement('img', 'secondImageElement');
    imageElement.setAttribute('alt', 'this is an issue');
    addToContentWrapper(imageElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).not.toBe(CustomRule.NoImageTitle);
  });
  test('Non image Element with title attribute', async () => {
    const imageElement = createElement('p', 'paragraphTitleElement');
    imageElement.setAttribute('title', 'Free Web tutorials');
    addToContentWrapper(imageElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).not.toBe(CustomRule.NoImageTitle);
  });
});
