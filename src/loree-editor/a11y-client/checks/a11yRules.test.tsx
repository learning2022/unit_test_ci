import CONSTANTS from '../../constant';
import { init } from '../init';
import { a11yGlobalContext } from '../../../a11y-service/context/a11y-context';

import { A11yContextProvider } from '../../../../src/a11y-service/context/a11y-provider';
import { quickFixer } from '../../../../src/loree-editor/a11y-client/quick-fixer';
import React from 'react';
import { render } from '@testing-library/react';
import { CustomRule } from '../types';
import {
  appendElementToBody,
  getEditorElementById,
  getEditorElementsByClassName,
} from '../../utils';
import { createElement, createinputElement, createDiv } from '../../common/dom';
import Base from '../../../loree-editor/base';
import ReactDOM from 'react-dom';

describe('A11y Custom rules', () => {
  let baseInstance: Base;
  let divElement: HTMLElement;
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  function addToContentWrapper(element: HTMLElement) {
    const wrapperContent = getEditorElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    wrapperContent?.append(element);
  }

  beforeEach(() => {
    document.body.innerHTML = '';
    render(<A11yContextProvider quickFixer={quickFixer} />);

    const a11ySpacer = createDiv(CONSTANTS.LOREE_SPACE_A11Y_CHECKER);
    appendElementToBody(a11ySpacer);

    init();

    a11yGlobalContext.value.isProcessing = false;
    a11yGlobalContext.value.setContext({
      include: ['#loree-iframe', '#loree-iframe-content-wrapper'],
    });
    loreeWrapper();

    baseInstance = new Base();
  });

  test('From Axe-core', async () => {
    const imageElement = createElement('img', 'imageWithoutAltAttribute');
    addToContentWrapper(imageElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe('image-alt');
    expect(a11yGlobalContext.value.steps[1]?.docsUrl).not.toBe('');
  });
  test('From custom rules', async () => {
    const imageElement = createElement('img', 'imageWithoutTitleAttribute');
    imageElement.setAttribute('title', 'Now from axe-core rule');
    addToContentWrapper(imageElement);
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe(CustomRule.NoImageTitle);
    expect(a11yGlobalContext.value.steps[1]?.docsUrl).toBe('');
  });

  function columnContent() {
    divElement = createDiv('columnWithIdIssue', CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
    ReactDOM.render(
      <div id='ColumnContent' style={{ display: 'block' }}>
        <p id='ColumnContent' contentEditable='true' />
        <div id='deleteYesButton' />
      </div>,
      divElement,
    );
    addToContentWrapper(divElement);
    const deleteYesButton = createinputElement('deleteColumnYes', '', 'radio');
    deleteYesButton.checked = true;
    document.body.append(deleteYesButton);
  }

  function rowContent() {
    divElement = createDiv('rowWithIdIssue', CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
    ReactDOM.render(
      <div id='RowContent' style={{ display: 'block' }}>
        <p id='RowContent' contentEditable='true' />
        <div id='deleteRowYesButton' />
      </div>,
      divElement,
    );
    addToContentWrapper(divElement);
    const deleteYesButton = createinputElement('deleteRowYes', '', 'radio');
    deleteYesButton.checked = true;
    document.body.append(deleteYesButton);
  }

  function duplicateContent() {
    divElement = createDiv('duplicateIssue', CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
    ReactDOM.render(
      <div id='dupContent1' style={{ display: 'block' }}>
        <img src='image.png' alt='altValue' title='abc' />
      </div>,
      divElement,
    );
    addToContentWrapper(divElement);
  }

  function moveContent() {
    const previousElement = createDiv('firstContent', '');
    previousElement.innerHTML = '<p>This is a paragraph element</p>';
    addToContentWrapper(previousElement);
    divElement = createDiv('moveElement', CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
    ReactDOM.render(
      <div id={CONSTANTS.LOREE_IFRAME_CONTENT_ROW} style={{ display: 'block' }}>
        <img src='image.png' alt='altValue' title='abc' />
      </div>,
      divElement,
    );
    addToContentWrapper(divElement);
    const nextElement = createElement(
      'div',
      CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER,
      'finalContent',
    );
    previousElement.innerHTML = '<p>This is a paragraph element';
    addToContentWrapper(nextElement);
  }

  function loadEditorContent() {
    baseInstance.showElementSectionOptions = jest.fn();
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
    baseInstance.getSelectedElement = jest.fn().mockImplementationOnce(() => divElement);
    baseInstance.changeSelectedElement(divElement);
    baseInstance.handleAfterDeleteContent();
    const a11yTray = document.createElement('div');
    a11yTray.id = CONSTANTS.LOREE_MAIN_A11YCHECKER;
    document.body.append(a11yTray);
  }

  test('To move elements left when Accessibility tray opened', async () => {
    moveContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    baseInstance.moveElementLeft();
    const { activeStep } = a11yGlobalContext.value;
    expect(activeStep).toBe(0);
  });

  test('To move elements right when Accessibility tray opened', async () => {
    moveContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    baseInstance.moveElementRight();
    const { activeStep } = a11yGlobalContext.value;
    expect(activeStep).toBe(0);
  });

  test('To move elements top when Accessibility tray opened', async () => {
    moveContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    baseInstance.isMoveTop();
    const { activeStep } = a11yGlobalContext.value;
    expect(activeStep).toBe(0);
  });

  test('To move elements bottom when Accessibility tray opened', async () => {
    moveContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    baseInstance.isMoveBottom();
    const { activeStep } = a11yGlobalContext.value;
    expect(activeStep).toBe(0);
  });

  test('To edit the row types when A11y checker is in open state', async () => {
    moveContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    baseInstance.isEdit();
    const { activeStep } = a11yGlobalContext.value;
    expect(activeStep).toBe(0);
  });

  test('To delete the column while A11Y in open state - refresh the A11Y tray', async () => {
    columnContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe('duplicate-id');
    expect(a11yGlobalContext.value.steps.length).toBe(2);
    await baseInstance.deleteColumn();
    expect(getEditorElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)[0]).toBeFalsy();
    expect(a11yGlobalContext.value.steps.length).toBe(1);
  });
  test('To delete the row while A11Y in open state - refresh the A11Y tray', async () => {
    rowContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps[1]?.ruleId).toBe('duplicate-id');
    expect(a11yGlobalContext.value.steps.length).toBe(2);
    await baseInstance.deleteRow();
    expect(getEditorElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)[0]).toBeFalsy();
    expect(a11yGlobalContext.value.steps.length).toBe(1);
  });
  test('To delete the Column while A11Y in closed state', async () => {
    columnContent();
    loadEditorContent();
    await baseInstance.deleteColumn();
    expect(getEditorElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)[0]).toBeFalsy();
  });
  test('To delete the row while A11Y in closed state', async () => {
    rowContent();
    loadEditorContent();
    await baseInstance.deleteRow();
    expect(getEditorElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)[0]).toBeFalsy();
  });

  test('To duplicate the element while opening the AC tray', async () => {
    duplicateContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps.length).toBe(2);
    await baseInstance.isDuplicate();
    expect(a11yGlobalContext.value.steps.length).toBe(5);
  });

  test('To copy and paste the element while opening the AC tray', async () => {
    duplicateContent();
    loadEditorContent();
    const { check, isProcessing } = a11yGlobalContext.value;
    await check(isProcessing);
    expect(a11yGlobalContext.value.steps.length).toBe(2);
    baseInstance.isCopied();
    await baseInstance.isPaste();
    expect(a11yGlobalContext.value.steps.length).toBe(4);
  });
});
