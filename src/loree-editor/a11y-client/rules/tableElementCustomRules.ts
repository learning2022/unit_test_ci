import { Rule } from 'axe-core';
import { CustomRule, CustomCheck, TAG_CUSTOM } from '../types';

export const tableHeaderId: Rule = {
  id: CustomRule.TableHeaderId,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.TableHeaderIdCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Incorrectly associating table headers and content via the headers and id attributes ',
  },
};

export const tableWhiteSpaceId: Rule = {
  id: CustomRule.TableWhiteSpaceId,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.TableWhiteSpaceIdCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Table contains empty cells',
  },
};

export const tablePresentationRoleId: Rule = {
  id: CustomRule.TableRolePresentationId,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.TableRolePresentationIdCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Table element contains role attribute with presentation value',
  },
};

export const tableNecessaryLayoutTh: Rule = {
  id: CustomRule.TableNecessaryLayoutTh,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.TableNecessaryLayoutThCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Table element should contain th element',
  },
};

export const tableNecessaryLayoutSummary: Rule = {
  id: CustomRule.TableNecessaryLayoutSummary,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.TableNecessaryLayoutSummaryCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Non-empty summary attribute must be added for Table element',
  },
};
