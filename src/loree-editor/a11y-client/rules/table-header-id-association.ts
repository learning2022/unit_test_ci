import { Rule } from 'axe-core';
import { CustomRule, CustomCheck, TAG_CUSTOM } from '../types';

export const tableHeaderId: Rule = {
  id: CustomRule.TableHeaderId,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.TableHeaderIdCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Incorrectly associating table headers and content via the headers and id attributes ',
  },
};
