import { Rule } from 'axe-core';
import { CustomRule, CustomCheck, TAG_CUSTOM } from '../types';

export const noImageTitleRule: Rule = {
  id: CustomRule.NoImageTitle,
  selector: 'img',
  impact: 'serious',
  all: [CustomCheck.ImageTitleCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Image element title attribute are not allowed',
  },
};
