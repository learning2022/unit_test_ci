import { Rule } from 'axe-core';
import { CustomRule, CustomCheck, TAG_CUSTOM } from '../types';

export const validTableCaptionRule: Rule = {
  id: CustomRule.ValidTableCaption,
  selector: 'table',
  impact: 'moderate',
  all: [CustomCheck.CaptionIsFirstChild],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    help: 'Table captions are valid',
  },
};
