import { Rule } from 'axe-core';
import { CustomRule, CustomCheck, TAG_CUSTOM } from '../types';

export const noStrikeRule: Rule = {
  id: CustomRule.NoStrike,
  selector: 'strike',
  impact: 'moderate',
  none: [CustomCheck.StrikeCheck],
  tags: [TAG_CUSTOM],
  enabled: true,
  metadata: {
    description: 'Strike Elements are not allowed',
    help: 'No strike elements',
  },
};
