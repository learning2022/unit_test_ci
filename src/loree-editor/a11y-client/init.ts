import type axeGlobal from 'axe-core';
import { a11yService } from '../../a11y-service';

import { noStrikeRule } from './rules/no-strike-rule';
import { strikeCheck } from './checks/strike-check';

import { captionIsFirstChildCheck } from './checks/caption-is-first-child-check';
import { validTableCaptionRule } from './rules/valid-table-caption';

import { noImageTitleRule } from './rules/no-image-title';
import { ImageTitleCheck } from './checks/image-title-check';

import {
  tableHeaderId,
  tableWhiteSpaceId,
  tablePresentationRoleId,
  tableNecessaryLayoutTh,
  tableNecessaryLayoutSummary,
} from './rules/tableElementCustomRules';
import {
  TableHeaderIdChecker,
  TableWhiteSpaceIdChecker,
  TablePresentationRoleIdChecker,
  TableNecessaryLayoutThChecker,
  TableNecessaryLayoutSummaryChecker,
} from './checks/tableElementCustomRuleChecker';

export function init(axe?: typeof axeGlobal) {
  // No Strike
  a11yService.registerRule(noStrikeRule, axe);
  a11yService.registerCheck(strikeCheck, axe);

  // Table Captions
  a11yService.registerRule(validTableCaptionRule, axe);
  a11yService.registerCheck(captionIsFirstChildCheck, axe);

  // Image Title attribute
  a11yService.registerRule(noImageTitleRule, axe);
  a11yService.registerCheck(ImageTitleCheck, axe);

  // Table headers and id association
  a11yService.registerRule(tableHeaderId, axe);
  a11yService.registerCheck(TableHeaderIdChecker, axe);

  // Table element to restrict white spaces in cell
  a11yService.registerRule(tableWhiteSpaceId, axe);
  a11yService.registerCheck(TableWhiteSpaceIdChecker, axe);

  // Table element to restrict presentation role
  a11yService.registerRule(tablePresentationRoleId, axe);
  a11yService.registerCheck(TablePresentationRoleIdChecker, axe);

  // Table element should contain th element
  a11yService.registerRule(tableNecessaryLayoutTh, axe);
  a11yService.registerCheck(TableNecessaryLayoutThChecker, axe);

  // Table element should contain non-empty summary attribute
  a11yService.registerRule(tableNecessaryLayoutSummary, axe);
  a11yService.registerCheck(TableNecessaryLayoutSummaryChecker, axe);
}
