import { ReactPortal } from 'react';
import { ImageAlt } from './image-alt';
import { quickFixer } from './index';
import {
  stepImageMockData,
  stepIframeMockData,
  stepTableMockData,
  stepImageTitleMockData,
} from './mockData';
import { Step } from '../../../a11y-service';
import { TableCaption } from './table-caption';
import { FrameTitle } from './video-tiitle';
import { ImageTitle } from './imageTitle';

describe('#ImageAltLayout', () => {
  const inputData = [
    [stepImageMockData, ImageAlt, 1],
    [stepImageTitleMockData, ImageTitle, 2],
    [stepTableMockData, TableCaption, 3],
    [stepIframeMockData, FrameTitle, 4],
  ];

  test.each(inputData)(
    'should render the quickFix component based on the ruleId',
    (props, component, stepCount) => {
      const activeStep = stepCount as number;
      const step = props as Step;
      const quickFixElement = quickFixer?.({ step, activeStep });
      expect((quickFixElement as ReactPortal).type).toBe(component);
    },
  );

  test('should return null when the element doesn"t contain errors', () => {
    const step = stepImageMockData;
    const activeStep = 1;
    step.ruleId = '';
    const quickFixElement = quickFixer?.({ step, activeStep });
    expect(quickFixElement as ReactPortal).toBeNull();
  });
});
