import React from 'react';
import * as a11y from '../../../a11y-service/context/a11y-context';
import { ImageAlt } from './image-alt';
import { stepImageMockData, mockUseA11yContext } from './mockData';
import { fireEvent, render, configure, RenderResult } from '@testing-library/react';
import { getElementsByTagName } from '../../utils';

configure({ testIdAttribute: 'id' });

describe('#ImageAltLayout', () => {
  let elements: HTMLCollection;
  beforeEach(() => {
    render(<ImageAlt step={stepImageMockData} activeStep={1} />);
  });

  test('should render with a label', () => {
    const element = getElementsByTagName('label')[0];
    expect(element.textContent).toEqual('Alt * \u00a0');
  });
  test('should render with a input field', () => {
    const element = getElementsByTagName('input')[0] as HTMLInputElement;
    expect(element.type).toBe('text');
  });
  test('should render with a checkBox', () => {
    const element = getElementsByTagName('input')[1] as HTMLInputElement;
    expect(element.type).toBe('checkbox');
  });
  test('checkBox should render with a label', () => {
    const element = getElementsByTagName('label')[1];
    expect(element.textContent).toEqual('\u00a0 Decorative image');
  });

  test('should render with apply button', () => {
    elements = getElementsByTagName('button');
    expect(elements[0].textContent).toBe('Apply');
  });
  test('apply button should be disabled when the input field is empty', () => {
    const element = elements[0] as HTMLButtonElement;
    expect(element.disabled).toBe(true);
  });
  test('should render with recheck button', () => {
    elements = getElementsByTagName('button');
    expect(elements[1].textContent).toBe('Re-check');
  });
});

describe('#ImageAltOuickFix', () => {
  let wrapper: RenderResult;
  const inputValue = [[''], ['         ']];
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    wrapper = render(<ImageAlt step={stepImageMockData} activeStep={1} />);
    stepImageMockData.element?.removeAttribute('alt');
  });
  test('image element should not include alt attribute', () => {
    expect(stepImageMockData.element).not.toHaveAttribute('alt');
  });

  test('should apply the alt value to the image when the input value is not empty', () => {
    const element = wrapper.getByTestId('image-alt') as HTMLInputElement;
    fireEvent.change(element, { target: { value: 'imageAlt' } });
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBeFalsy();
    fireEvent.click(buttonElement);
    expect(stepImageMockData.element).toHaveAttribute('alt');
  });

  test.each(inputValue)('should disable apply button when the input value is empty', (text) => {
    const element = wrapper.getByTestId('image-alt') as HTMLInputElement;
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    fireEvent.change(element, { target: { value: text } });
    expect(buttonElement.disabled).toBe(true);
    expect(stepImageMockData.element).not.toHaveAttribute('alt');
  });

  test('should apply empty value when the checkBox is checked', () => {
    const element = wrapper.getByTestId('image-alt') as HTMLInputElement;
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    fireEvent.change(element, { target: { value: '' } });
    const checkBoxElement = wrapper.getByTestId('checkbox') as HTMLInputElement;
    fireEvent.click(checkBoxElement, { currentTarget: { checked: true } });
    expect(checkBoxElement.checked).toBe(true);
    expect(buttonElement.disabled).toBeFalsy();
    fireEvent.click(buttonElement);
    expect(stepImageMockData.element).toHaveAttribute('alt');
  });

  test('should trigger the mock function when the recheck button is clicked', () => {
    const element = getElementsByTagName('button')[1];
    fireEvent.click(element);
    expect(mock).toHaveBeenCalled();
  });

  test('should disable the apply button when checkbox is unchecked and the alt value is empty', () => {
    const element = wrapper.getByTestId('image-alt') as HTMLInputElement;
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    fireEvent.change(element, { target: { value: '' } });
    const checkBoxElement = wrapper.getByTestId('checkbox') as HTMLInputElement;
    fireEvent.click(checkBoxElement, { currentTarget: { checked: true } });
    fireEvent.click(checkBoxElement, { currentTarget: { checked: false } });
    expect(checkBoxElement.checked).toBe(false);
    expect(buttonElement.disabled).toBe(true);
    fireEvent.click(buttonElement);
    expect(stepImageMockData.element).not.toHaveAttribute('alt');
  });
});

describe('#State Refresh when the component re-rendered', () => {
  let wrapper: RenderResult;
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    wrapper = render(<ImageAlt step={stepImageMockData} activeStep={1} />);
    stepImageMockData.element?.removeAttribute('alt');
  });

  test('should change the value of the input box and checkbox', () => {
    const element = wrapper.getByTestId('image-alt') as HTMLInputElement;
    fireEvent.change(element, { target: { value: 'imageAlt' } });
    expect(element.value).toBe('imageAlt');
    const checkBoxElement = wrapper.getByTestId('checkbox') as HTMLInputElement;
    fireEvent.click(checkBoxElement, { currentTarget: { checked: true } });
    expect(checkBoxElement.checked).toBe(true);
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBeFalsy();
  });

  test('should reset the values of the component once it"s re rendered', () => {
    const element = wrapper.getByTestId('image-alt') as HTMLInputElement;
    expect(element.value).toBe('');
    const checkBoxElement = wrapper.getByTestId('checkbox') as HTMLInputElement;
    expect(checkBoxElement.checked).toBe(false);
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBe(true);
  });
});
