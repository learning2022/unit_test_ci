import React from 'react';
import * as a11y from '../../../a11y-service/context/a11y-context';
import { TableCaption } from './table-caption';
import { stepTableMockData, mockUseA11yContext } from './mockData';
import { fireEvent, render, configure, RenderResult } from '@testing-library/react';
import { getElementsByTagName } from '../../utils';

configure({ testIdAttribute: 'id' });

describe('#TableCaptionLayout', () => {
  let elements: HTMLCollection;
  beforeEach(() => {
    render(<TableCaption step={stepTableMockData} activeStep={1} />);
  });

  test('should render with a label', () => {
    const element = getElementsByTagName('label')[0];
    expect(element.textContent).toEqual('caption * \u00a0');
  });
  test('should render with a input field', () => {
    const element = getElementsByTagName('input')[0] as HTMLInputElement;
    expect(element.type).toBe('text');
  });

  test('should render with apply button', () => {
    elements = getElementsByTagName('button');
    expect(elements[0].textContent).toBe('Apply');
  });
  test('apply button should be disabled when the input field is empty', () => {
    const element = elements[0] as HTMLButtonElement;
    expect(element.disabled).toBe(true);
  });
  test('should render with recheck button', () => {
    elements = getElementsByTagName('button');
    expect(elements[1].textContent).toBe('Re-check');
  });
});

describe('#TableCaptionQuickFix', () => {
  let wrapper: RenderResult;
  const inputValue = [[''], ['         ']];
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    wrapper = render(<TableCaption step={stepTableMockData} activeStep={1} />);
    stepTableMockData.element?.firstChild?.remove();
  });
  test('table should not contain the caption element', () => {
    const captionElement = stepTableMockData.element?.firstChild as HTMLElement;
    expect(captionElement).toBeNull();
  });

  test('should append the captoin to the table when the input value is not empty', () => {
    const element = wrapper.getByTestId('caption') as HTMLInputElement;
    fireEvent.change(element, { target: { value: 'table-caption' } });
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBeFalsy();
    fireEvent.click(buttonElement);
    const captionElement = stepTableMockData.element?.firstChild as HTMLElement;
    expect(captionElement.tagName).toBe('CAPTION');
    expect(captionElement.textContent).toBe('table-caption');
    // change the table-caption innerText
    fireEvent.change(element, { target: { value: 'caption' } });
    fireEvent.click(buttonElement);
    expect(captionElement.textContent).toBe('caption');
  });

  test.each(inputValue)('should disable apply button when the input value is empty', (text) => {
    const element = wrapper.getByTestId('caption') as HTMLInputElement;
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    fireEvent.change(element, { target: { value: text } });
    expect(buttonElement.disabled).toBe(true);
    const captionElement = stepTableMockData.element?.firstChild as HTMLElement;
    expect(captionElement).toBeNull();
  });
  test('should trigger the mock function when the recheck button is clicked', () => {
    const element = getElementsByTagName('button')[1];
    fireEvent.click(element);
    expect(mock).toHaveBeenCalled();
  });
});

describe('#State Refresh', () => {
  let wrapper: RenderResult;
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    wrapper = render(<TableCaption step={stepTableMockData} activeStep={1} />);
    stepTableMockData.element?.firstChild?.remove();
  });

  test('should change the value of the input box and apply button status', () => {
    const element = wrapper.getByTestId('caption') as HTMLInputElement;
    fireEvent.change(element, { target: { value: 'table-caption' } });
    expect(element.value).toBe('table-caption');
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBeFalsy();
  });

  test('should reset the values of the component once it"s re rendered', () => {
    const element = wrapper.getByTestId('caption') as HTMLInputElement;
    expect(element.value).toBe('');
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBe(true);
  });
});
