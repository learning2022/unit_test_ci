import React, { useState, FormEventHandler, useEffect } from 'react';
import { Step } from '../../../a11y-service';
import { useA11yContext } from '../../../a11y-service/context/a11y-context';
import { createElement } from '../../common/dom';
import CONSTANTS from '../../constant';
import { QuickFixerLayout } from './common-layout';

type Props = {
  step: Step;
  activeStep: number;
};
export function TableCaption({ step, activeStep }: Props) {
  const [altValue, setAltValue] = useState('');
  const [disabled, setDisable] = useState(true);

  const { check } = useA11yContext();

  useEffect(() => {
    setAltValue('');
    setDisable(true);
  }, [activeStep]);

  const handleInputChange = (value: string) => {
    if (value.trim() !== '') {
      updateValueInState(value, false);
    } else {
      updateValueInState('', true);
    }
  };

  const handleTableCaptionSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    if (!step.element || disabled) return;
    const captionElem = step.element.querySelector('caption');
    if (!captionElem) {
      const captionElement = createElement('caption');
      captionElement.className = `${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} ${CONSTANTS.LOREE_TABLE_CAPTION_ELEMENT}`;
      captionElement.contentEditable = 'true';
      captionElement.setAttribute(
        'style',
        'caption-side: bottom; border-width: 0; border-style: solid; border-color: #000000;  padding: 5px; margin: 0px;',
      );
      step.element.prepend(captionElement);
      captionElement.innerHTML = altValue.trim();
    } else {
      captionElem.innerHTML = altValue.trim();
    }
    updateValueInState('', true);
    void check(true);
  };

  const updateValueInState = (inputText: string, disable: boolean) => {
    setAltValue(inputText);
    setDisable(disable);
  };

  const props = {
    type: 'caption',
    value: altValue,
    handleSubmit: handleTableCaptionSubmit,
    disabled: disabled,
    handleChange: handleInputChange,
  };

  return <QuickFixerLayout {...props} />;
}
