import React from 'react';
import { ApplyAndRecheck, QuickFixerLayout } from './common-layout';
import ReactDOM from 'react-dom';
import { fireEvent, render } from '@testing-library/react';
import * as a11y from '../../../a11y-service/context/a11y-context';
import { mockUseA11yContext } from './mockData';
import { getElementsByTagName } from '../../utils';

describe('#render common components of quickFixer', () => {
  describe('#ApplyAndRecheck', () => {
    let elements: HTMLCollection;
    const mock = jest.spyOn(a11y, 'useA11yContext');
    beforeEach(() => {
      mock.mockImplementation(() => mockUseA11yContext);
      render(<ApplyAndRecheck disabled={false} />);
    });

    test('should render with an apply button', () => {
      elements = getElementsByTagName('button');
      expect(elements[0].textContent).toBe('Apply');
    });

    test('apply button should have the type as submit', () => {
      const element = elements[0] as HTMLButtonElement;
      expect(element.type).toBe('submit');
    });

    test('should render with a recheck button', () => {
      expect(elements[1].textContent).toBe('Re-check');
    });

    test('when click on re-check should triger a function', () => {
      fireEvent.click(elements[1]);
      expect(mock).toHaveBeenCalled();
    });
  });

  describe('#QuickFixerLayout', () => {
    const props = {
      type: 'caption',
      disabled: true,
      handleSubmit: jest.fn(),
      handleChange: jest.fn(),
      value: '',
    };
    beforeAll(() => {
      getElementsByTagName('html')[0].innerHTML = '';
      const domDiv = document.createElement('div');
      ReactDOM.render(<QuickFixerLayout {...props} />, domDiv);
      document.body.append(domDiv);
    });

    test('should render with a label', () => {
      const element = getElementsByTagName('label');
      expect(element.length).toBe(1);
    });

    test('should render with a input box', () => {
      const element = getElementsByTagName('input');
      expect(element.length).toBe(1);
      fireEvent.change(element[0]);
    });
  });
});
