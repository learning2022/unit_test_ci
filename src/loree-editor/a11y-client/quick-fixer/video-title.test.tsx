import React from 'react';
import * as a11y from '../../../a11y-service/context/a11y-context';
import { FrameTitle } from './video-tiitle';
import { stepIframeMockData, mockUseA11yContext } from './mockData';
import { fireEvent, render, configure, RenderResult } from '@testing-library/react';
import { getElementsByTagName } from '../../utils';

configure({ testIdAttribute: 'id' });

describe('#VidoeTitleLayout', () => {
  let elements: HTMLCollection;
  beforeEach(() => {
    render(<FrameTitle step={stepIframeMockData} activeStep={1} />);
  });

  test('should render with a label', () => {
    const element = getElementsByTagName('label')[0];
    expect(element.textContent).toEqual('title * \u00a0');
  });
  test('should render with a input field', () => {
    const element = getElementsByTagName('input')[0] as HTMLInputElement;
    expect(element.type).toBe('text');
  });

  test('should render with apply button', () => {
    elements = getElementsByTagName('button');
    expect(elements[0].textContent).toBe('Apply');
  });
  test('apply button should be disabled when the input field is empty', () => {
    const element = elements[0] as HTMLButtonElement;
    expect(element.disabled).toBe(true);
  });
  test('should render with recheck button', () => {
    elements = getElementsByTagName('button');
    expect(elements[1].textContent).toBe('Re-check');
  });
});

describe('#VidoeTitleLayoutOuickFix', () => {
  let wrapper: RenderResult;
  const inputValue = [[''], ['         ']];
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    wrapper = render(<FrameTitle step={stepIframeMockData} activeStep={1} />);
    stepIframeMockData.element?.removeAttribute('title');
  });
  test('iframe element should not include title attribute', () => {
    expect(stepIframeMockData.element).not.toHaveAttribute('title');
  });

  test('should apply the title value to the frame when the input value is not empty', () => {
    const element = wrapper.getByTestId('title') as HTMLInputElement;
    fireEvent.change(element, { target: { value: 'videoTitle' } });
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBeFalsy();
    fireEvent.click(buttonElement);
    expect(stepIframeMockData.element).toHaveAttribute('title');
  });

  test.each(inputValue)('should disable apply button when the input value is empty', (text) => {
    const element = wrapper.getByTestId('title') as HTMLInputElement;
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    fireEvent.change(element, { target: { value: text } });
    expect(buttonElement.disabled).toBe(true);
    expect(stepIframeMockData.element).not.toHaveAttribute('title');
  });
  test('should trigger the mock function when the recheck button is clicked', () => {
    const element = getElementsByTagName('button')[1];
    fireEvent.click(element);
    expect(mock).toHaveBeenCalled();
  });
});

describe('#State Refresh', () => {
  let wrapper: RenderResult;
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    wrapper = render(<FrameTitle step={stepIframeMockData} activeStep={1} />);
    stepIframeMockData.element?.removeAttribute('title');
  });

  test('should change the value of the input box and apply button status', () => {
    const element = wrapper.getByTestId('title') as HTMLInputElement;
    fireEvent.change(element, { target: { value: 'videoTitle' } });
    expect(element.value).toBe('videoTitle');
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBeFalsy();
  });

  test('should reset the values of the component once it"s re rendered', () => {
    const element = wrapper.getByTestId('title') as HTMLInputElement;
    expect(element.value).toBe('');
    const buttonElement = getElementsByTagName('button')[0] as HTMLButtonElement;
    expect(buttonElement.disabled).toBe(true);
  });
});
