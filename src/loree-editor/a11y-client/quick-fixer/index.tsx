import React from 'react';

import { QuickFixer } from '../../../a11y-service';
import { ImageAlt } from './image-alt';
import { ImageTitle } from './imageTitle';
import { CustomRule } from '../types';
import { TableCaption } from './table-caption';
import { FrameTitle } from './video-tiitle';

export const quickFixer: QuickFixer = ({ step, activeStep }) => {
  switch (step.ruleId) {
    case 'image-alt':
      return <ImageAlt step={step} activeStep={activeStep} />;
    case 'frame-title':
      return <FrameTitle step={step} activeStep={activeStep} />;
    case CustomRule.ValidTableCaption:
      return <TableCaption step={step} activeStep={activeStep} />;
    case CustomRule.NoImageTitle:
      return <ImageTitle step={step} />;
  }
  return null;
};
