import React, { FormEventHandler } from 'react';
import { Button } from 'react-bootstrap';
import { useA11yContext } from '../../../a11y-service/context/a11y-context';

type Props = {
  disabled: boolean;
};
type QuickFixer = {
  type: string;
  value: string;
  disabled: boolean;
  handleSubmit: FormEventHandler;
  handleChange: Function;
};

export function ApplyAndRecheck({ disabled }: Props) {
  const { check } = useA11yContext();

  return (
    <>
      <Button variant='outline-primary' className='alert-button' type='submit' disabled={disabled}>
        Apply
      </Button>
      <Button
        variant='outline-primary'
        className='modal-footer-button my-2'
        onClick={() => void check(true)}
      >
        Re-check
      </Button>
    </>
  );
}

export function QuickFixerLayout(props: QuickFixer) {
  return (
    <form onSubmit={props.handleSubmit}>
      <label className='a11y-image-alt text-capitalize'>
        {props.type} * &nbsp;
        <input
          maxLength={256}
          value={props.value}
          id={props.type}
          onChange={(e) => props.handleChange(e.currentTarget.value)}
          autoComplete='off'
        />
      </label>
      <ApplyAndRecheck disabled={props.disabled} />
    </form>
  );
}
