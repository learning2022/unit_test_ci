import React from 'react';
import * as a11y from '../../../a11y-service/context/a11y-context';
import { ImageTitle } from './imageTitle';
import { stepImageMockData, mockUseA11yContext } from './mockData';
import { fireEvent, render } from '@testing-library/react';
import { getElementsByTagName } from '../../utils';

describe('#ImageAltLayout', () => {
  let elements: HTMLCollection;

  const spanLabel = `Select ‘Clear Title’ to remove the title attribute from the image.`;
  beforeEach(() => {
    render(<ImageTitle step={stepImageMockData} />);
  });

  test('should render with a label', () => {
    const element = getElementsByTagName('span')[0];
    expect(element.textContent).toEqual(spanLabel);
  });
  test('should render with Erase title button', () => {
    elements = getElementsByTagName('button');
    expect(elements[0].textContent).toBe('Clear Title');
  });
  test('should render with recheck button', () => {
    elements = getElementsByTagName('button');
    expect(elements[1].textContent).toBe('Re-check');
  });
});

describe('Custom rule for Image title removal', () => {
  const mock = jest.spyOn(a11y, 'useA11yContext');
  beforeEach(() => {
    mock.mockImplementation(() => mockUseA11yContext);
    render(<ImageTitle step={stepImageMockData} />);
    stepImageMockData.element?.setAttribute('title', 'Title added here');
  });
  test('image element should not include alt attribute', () => {
    expect(stepImageMockData.element).toHaveAttribute('title');
  });
  test('should trigger the removeTitle function when the Erase button is clicked', () => {
    const element = getElementsByTagName('button')[0];
    fireEvent.click(element);
    expect(stepImageMockData.element).not.toHaveAttribute('title');
    expect(mock).toHaveBeenCalled();
  });
  test('should trigger the mock function when the recheck button is clicked', () => {
    const element = getElementsByTagName('button')[1];
    fireEvent.click(element);
    expect(mock).toHaveBeenCalled();
  });
});
