import React, { useState, FormEventHandler } from 'react';
import { Step } from '../../../a11y-service';

type Props = {
  step: Step;
};
export function ValidTableCaption({ step }: Props) {
  const [value, setValue] = useState('');

  const handleSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    if (value && step.element) {
      let captionElem = step.element.querySelector('caption');
      if (!captionElem) {
        captionElem = document.createElement('caption');
        step.element.prepend(captionElem);
      }
      captionElem.innerHTML = value;
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        New Table Caption
        <input value={value} onChange={(e) => setValue(e.currentTarget.value)} />
      </label>
      <button type='submit'>Update</button>
    </form>
  );
}
