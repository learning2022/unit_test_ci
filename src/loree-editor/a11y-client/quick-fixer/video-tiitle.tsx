import React, { useState, FormEventHandler, useEffect } from 'react';
import { Step } from '../../../a11y-service';
import { useA11yContext } from '../../../a11y-service/context/a11y-context';
import { QuickFixerLayout } from './common-layout';

type Props = {
  step: Step;
  activeStep: number;
};
export function FrameTitle({ step, activeStep }: Props) {
  const [altValue, setAltValue] = useState('');
  const [disabled, setDisable] = useState(true);

  const { check } = useA11yContext();

  useEffect(() => {
    setAltValue('');
    setDisable(true);
  }, [activeStep]);

  const handleInputChange = (value: string) => {
    if (value.trim() !== '') {
      updateValueInState(value, false);
    } else {
      updateValueInState('', true);
    }
  };

  const handleSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    if (disabled) return;
    step.element?.setAttribute('title', altValue.trim());
    updateValueInState('', true);
    void check(true);
  };

  const updateValueInState = (inputText: string, disable: boolean) => {
    setAltValue(inputText);
    setDisable(disable);
  };

  const props = {
    type: 'title',
    value: altValue,
    disabled: disabled,
    handleSubmit: handleSubmit,
    handleChange: handleInputChange,
  };

  return <QuickFixerLayout {...props} />;
}
