import React, { useState, FormEventHandler, useEffect } from 'react';
import { Step } from '../../../a11y-service';
import { useA11yContext } from '../../../a11y-service/context/a11y-context';
import { ApplyAndRecheck } from './common-layout';

type Props = {
  step: Step;
  activeStep: number;
};
export function ImageAlt({ step, activeStep }: Props) {
  const [altValue, setAltValue] = useState('');
  const [disabled, setDisable] = useState(true);
  const [checked, setChecked] = useState(false);

  const { check } = useA11yContext();

  useEffect(() => {
    setAltValue('');
    setDisable(true);
    setChecked(false);
  }, [activeStep]);

  const handleInputChange = (value: string) => {
    if (value.trim() !== '' || checked) {
      updateValueInState(value, false);
    } else {
      updateValueInState('', true);
    }
  };

  const handleCheckBoxChange = (checked: boolean) => {
    setChecked(checked);
    if (checked) {
      setDisable(false);
    } else if (altValue === '' && !checked) {
      setDisable(true);
    }
  };

  const handleSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    if (disabled) return;
    step.element?.setAttribute('alt', altValue.trim());
    updateValueInState('', true);
    setChecked(false);
    void check(true);
  };

  const updateValueInState = (inputText: string, disable: boolean) => {
    setAltValue(inputText);
    setDisable(disable);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label className='a11y-image-alt text-capitalize'>
        Alt * &nbsp;
        <input
          id='image-alt'
          maxLength={256}
          type='text'
          value={altValue}
          onChange={(e) => handleInputChange(e.currentTarget.value)}
          autoComplete='off'
        />
      </label>
      <label className='a11y-image-alt'>
        <input
          id='checkbox'
          type='checkbox'
          checked={checked}
          onClick={(e) => handleCheckBoxChange(e.currentTarget.checked)}
        />
        &nbsp; Decorative image
      </label>
      <ApplyAndRecheck disabled={disabled} />
    </form>
  );
}
