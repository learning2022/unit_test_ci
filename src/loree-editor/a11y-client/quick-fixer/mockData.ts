import { Step, A11yContext } from '../../../a11y-service';
import { createElement } from '../../common/dom';

const imageElement = createElement('img') as HTMLImageElement;
imageElement.src = 'https://crystaldelta.instructure.com/courses/875/files/230540';
imageElement.className = 'loree-iframe-content-image';

Element.prototype.scrollIntoView = jest.fn();

const iframeElement = createElement('iframe') as HTMLIFrameElement;
iframeElement.src = 'https://www.youtube.com/watch?v=DLX62G4lc44';
iframeElement.allowFullscreen = true;

const tableElement = createElement('table') as HTMLTableElement;
tableElement.innerHTML = `<tbody><tr class="loree-style-LVUZ3q"><td class="loree-style-P8zqkH"><p class="loree-iframe-content-element loree-style-lLo6P1">Insert text here</p></td></tr>
<tr class="loree-style-LVUZ3q"><td class="loree-style-P8zqkH"><p class="loree-iframe-content-element loree-style-lLo6P1">Insert text here</p></td></tr></tbody>`;

export const dummyContent = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.`;

export const stepImageMockData: Step = {
  checkDetails: {
    all: [],
    any: [
      {
        id: 'has-alt',
        data: null,
        relatedNodes: Array(0),
        impact: 'critical',
        message: 'Element does not have an alt attribute',
      },
      {
        id: 'aria-label',
        data: null,
        relatedNodes: Array(0),
        impact: 'serious',
        message: 'aria-label attribute does not exist or is empty',
      },
      {
        id: 'aria-labelledby',
        data: null,
        relatedNodes: Array(0),
        impact: 'serious',
        message:
          'aria-labelledby attribute does not exist, referenc…o not exist or references elements that are empty',
      },
      {
        id: 'non-empty-title',
        data: null,
        relatedNodes: Array(0),
        impact: 'serious',
        message: 'Element has no title attribute',
      },
      {
        id: 'presentational-role',
        data: null,
        relatedNodes: Array(0),
        impact: 'minor',
        message:
          'Element\'s default semantics were not overridden with role="none" or role="presentation"',
      },
    ],
    none: [],
  },
  description: 'Ensures <img> elements have alternate text or a role of none or presentation',
  details: [],
  docsUrl: 'https://dequeuniversity.com/rules/axe/4.3/image-alt?application=axeAPI',
  element: imageElement,
  fixedSinceLastReport: false,
  impact: 'critical',
  key: "image-alt:/iframe[@id='loree-iframe'],/div[@id='loree-iframe-content-wrapper']/div/div/div[3]/img",
  ruleId: 'image-alt',
  tags: ['cat.text-alternatives', 'wcag2a', 'wcag111', 'section508', 'section508.22.a', 'ACT'],
  title: 'Images must have alternate text',
};

export const stepImageTitleMockData: Step = {
  checkDetails: {
    all: [],
    any: [
      {
        relatedNodes: Array(0),
        data: null,
        id: 'image-title-check',
        impact: 'serious',
        message: '<Image> Elements must not have title attribute',
      },
    ],
    none: [],
  },
  description: '',
  details: ['<Image> Elements must not have title attribute'],
  docsUrl: '',
  element: imageElement,
  fixedSinceLastReport: false,
  impact: 'serious',
  key: "no-image-title:/iframe[@id='loree-iframe'],/div[@id='loree-iframe-content-wrapper']/div/div/div[3]/img",
  ruleId: 'no-image-title',
  tags: ['custom'],
  title: 'Image element title attribute are not allowed',
};
export const mockUseA11yContext: A11yContext = {
  activeElem: null,
  activeStep: 0,
  check: jest.fn(),
  clear: jest.fn(),
  highlightsIdentity: {},
  highlightsOffset: [0, 0],
  isProcessing: false,
  setActiveElem: jest.fn(),
  setActiveStep: jest.fn(),
  setContext: jest.fn(),
  setHighlightsOffset: jest.fn(),
  steps: [stepImageMockData],
  setSteps: jest.fn(),
  stepsByImpact: {
    minor: [],
    critical: [],
    serious: [],
    moderate: [],
  },
  updateHighlights: jest.fn(),
  setNotificationFlag: jest.fn(),
};

export const stepTableMockData: Step = {
  checkDetails: stepImageMockData.checkDetails,
  description: '',
  details: ['<caption> Elements must be the first child of the table'],
  docsUrl: 'https://dequeuniversity.com/rules/axe/4.3/image-alt?application=axeAPI',
  element: tableElement,
  fixedSinceLastReport: false,
  impact: 'moderate',
  key: "valid-table-caption:/iframe[@id='loree-iframe'],/div[@id='loree-iframe-content-wrapper']/div/div/div/table",
  ruleId: 'valid-table-caption',
  tags: ['custom'],
  title: 'Table captions are valid',
};

export const stepIframeMockData: Step = {
  checkDetails: stepImageMockData.checkDetails,
  description: 'Ensures <iframe> and <frame> elements have an accessible name',
  details: [
    'Element has no title attribute',
    'aria-label attribute does not exist or is empty',
    'aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty',
    'Element\'s default semantics were not overridden with role="none" or role="presentation"',
  ],
  docsUrl: 'https://dequeuniversity.com/rules/axe/4.3/image-alt?application=axeAPI',
  element: iframeElement,
  fixedSinceLastReport: false,
  impact: 'serious',
  key: "frame-title:/iframe[@id='loree-iframe'],/div[@id='loree-iframe-content-wrapper']/div/div/div[2]/div/div/iframe",
  ruleId: 'frame-title',
  tags: ['cat.text-alternatives', 'wcag2a', 'wcag241', 'wcag412', 'section508', 'section508.22.i'],
  title: 'Frames must have an accessible name',
};

export const a11yCheckerContainerMockData: A11yContext = {
  activeElem: null,
  activeStep: 0,
  check: jest.fn(),
  clear: jest.fn(),
  highlightsIdentity: {},
  highlightsOffset: [0, 67],
  isProcessing: false,
  setActiveElem: jest.fn(),
  setActiveStep: jest.fn(),
  setContext: jest.fn(),
  setHighlightsOffset: jest.fn(),
  steps: [stepTableMockData, stepIframeMockData, stepImageMockData],
  setSteps: jest.fn(),
  stepsByImpact: {
    minor: [],
    critical: [],
    serious: [],
    moderate: [],
  },
  updateHighlights: jest.fn(),
  setNotificationFlag: jest.fn(),
};
