import React from 'react';
import { Step } from '../../../a11y-service';
import { useA11yContext } from '../../../a11y-service/context/a11y-context';
import { Button } from 'react-bootstrap';

type Props = {
  step: Step;
};
export function ImageTitle({ step }: Props) {
  const { check } = useA11yContext();

  const removeTitleAttribute = () => {
    step.element?.removeAttribute('title');
    void check(true);
  };

  return (
    <>
      <span className='a11y-image-title'>
        Select ‘Clear Title’ to remove the title attribute from the image.
      </span>
      <div>
        <Button
          variant='outline-primary'
          className='alert-button'
          onClick={() => removeTitleAttribute()}
        >
          Clear Title
        </Button>
        <Button
          variant='outline-primary'
          className='modal-footer-button my-2'
          onClick={() => void check(true)}
        >
          Re-check
        </Button>
      </div>
    </>
  );
}
