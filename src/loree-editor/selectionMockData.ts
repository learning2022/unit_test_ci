export const spanSelection = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 4,
  endOffset: 5,
} as unknown as Selection;

export const spanPreviousSelection = {
  commonAncestorContainer: {
    nextSibling: {
      nodeName: 'LI',
    },
  },
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 8,
  endOffset: 9,
};

export const brSelection = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 2,
  endOffset: 3,
} as unknown as Selection;

export const brPreviousSelection = {
  commonAncestorContainer: {
    nodeName: 'P',
  },
  startContainer: {
    nodeName: 'UL',
    nextSibling: {
      nodeName: 'UL',
    },
  },
  endContainer: {
    nodeName: 'UL',
  },
  startOffset: 0,
  endOffset: 0,
};

export const selection = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 2,
  endOffset: 3,
} as unknown as Selection;

export const previousSelection = {
  commonAncestorContainer: {
    nextSibling: {
      nodeName: 'LI',
    },
  },
  startOffset: 5,
  endOffset: 7,
};
