/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, {
  ChangeEvent,
  KeyboardEvent,
  useCallback,
  useState,
  useMemo,
  CSSProperties,
  useEffect,
} from 'react';
import styled from 'styled-components';
import { ReactComponent as CloseIcon } from '../../../assets/Icons/_editor/close.svg';
import { useLoreeTranslation } from '../../../i18n/translate';

interface LinkEditorProps {
  initialUrl: string;
  initialNewWindow: boolean;
  forceNewWindow: boolean;
  onChange: (url: string, newWindow: boolean) => void;
  onRemove: () => void;
  onClose: () => void;
}

function urlValidatorUrl(): RegExp {
  return /^(?:(?:https?|http|ftp|smtp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
}

const DeleteButton = styled.button`
  background-color: transparent;
  border: none;
  margin: 0;
  padding: 0;
`;

export default function LinkEditor(props: LinkEditorProps) {
  const { initialUrl, initialNewWindow, onChange, onRemove, forceNewWindow, onClose } = props;

  const [url, setUrl] = useState<string>(initialUrl);
  const [newWindow, setNewWindow] = useState<boolean>(initialNewWindow);
  const [isValidUrl, setIsValidUrl] = useState<boolean>(true);
  const { t: translate } = useLoreeTranslation();

  const onUrlChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const newUrl = e.target.value;
      setUrl(newUrl);
      if (!newUrl || urlValidatorUrl().test(newUrl)) {
        setIsValidUrl(true);
      } else {
        setIsValidUrl(false);
      }
    },
    [setIsValidUrl],
  );

  const onNewWindowChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setNewWindow((v) => !v);
  }, []);

  useEffect(() => {
    if (urlValidatorUrl().test(url)) {
      onChange(url, newWindow);
    }
  }, [newWindow, onChange, url]);

  const onKeyboard = useCallback(
    (e: KeyboardEvent<HTMLDivElement>) => {
      if (e.key === 'Enter' && isValidUrl && url !== '') {
        onChange(url, newWindow);
        onClose();
      } else {
        setIsValidUrl(false);
      }
    },
    [isValidUrl, onChange, url, newWindow, onClose],
  );

  const style: CSSProperties = useMemo(() => {
    if (isValidUrl) {
      return {};
    }
    return { borderColor: 'red' };
  }, [isValidUrl]);

  return (
    <div onKeyDown={onKeyboard}>
      <input
        id='loree-image-user-anchor-link-inputx'
        className='link-options-alignment-wrapper-input-image'
        placeholder='https://www.crystaldelta.com'
        style={style}
        value={url}
        onChange={onUrlChange}
      />
      <DeleteButton
        aria-label={translate('global.remove')}
        role='button'
        className='link-options-remove-link'
        style={{ marginTop: '-2px', cursor: 'pointer', left: '-8px' }}
        onClick={onRemove}
      >
        <CloseIcon width={15} height={8} />
      </DeleteButton>
      <div>
        <input
          type='checkbox'
          className='link-options-checkbox'
          id='loree-link-option-tab-checkbox-imagex'
          checked={newWindow || forceNewWindow}
          onChange={onNewWindowChange}
          name='newWindowCheckbox'
          disabled={forceNewWindow}
        />
        <label
          className='link-options-checkbox-text'
          htmlFor='loree-link-option-tab-checkbox-imagex'
        >
          {translate('modal.openlinkinnewtab')}
        </label>
      </div>
    </div>
  );
}
