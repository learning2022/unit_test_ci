/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { ReactComponent as CloseIcon } from '../../../assets/Icons/_editor/close.svg';
import { useLoreeTranslation } from '../../../i18n/translate';

interface LinkInfoProps {
  url: string;
  onRemove: () => void;
}

export function LinkInfo(props: LinkInfoProps) {
  const { url, onRemove } = props;
  const { t: translate } = useLoreeTranslation();

  return (
    <>
      <a href={url} target='_blank' rel='noreferrer'>
        {url}
      </a>
      <span
        className='ml-2 float-right justify-content-end'
        style={{ marginTop: '-2px', cursor: 'pointer' }}
        onClick={onRemove}
        title={translate('linkinfo.remove')}
      >
        <CloseIcon width={15} height={8} />
      </span>
    </>
  );
}
