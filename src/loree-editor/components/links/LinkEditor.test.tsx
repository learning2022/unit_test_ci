import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import axe from '../../../axeHelper';
import LinkEditor from './LinkEditor';

describe('<LinkEditor>', function () {
  test('renders', () => {
    const { container } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={jest.fn()}
        onRemove={jest.fn()}
        onClose={jest.fn()}
      />,
    );
    expect(container).toBeInTheDocument();
  });

  test('passes accessibility', async () => {
    const { container } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={jest.fn()}
        onRemove={jest.fn()}
        onClose={jest.fn()}
      />,
    );
    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });

  test('changing url and pressing enter updates url', () => {
    const onChange = jest.fn();
    const onClose = jest.fn();
    const { getByRole } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={onChange}
        onClose={onClose}
        onRemove={jest.fn()}
      />,
    );

    const url = 'http://xyz.com';
    const input = getByRole('textbox');
    fireEvent.change(input, { target: { value: url } });
    fireEvent.keyDown(input, { key: 'Enter' });
    expect(onChange).toHaveBeenCalledWith(url, false);
  });

  test('should throw error while pressing enter when the url field is empty', () => {
    const onChange = jest.fn();
    const onClose = jest.fn();
    const { getByRole } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={onChange}
        onClose={onClose}
        onRemove={jest.fn()}
      />,
    );

    const input = getByRole('textbox');
    fireEvent.change(input, { target: { value: '' } });
    fireEvent.keyDown(input, { key: 'Enter' });
    expect(input.style.borderColor).toBe('red');
  });

  test('changing new window checkbox updates url', () => {
    const onChange = jest.fn();
    const onClose = jest.fn();
    const { getByRole } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={onChange}
        onClose={onClose}
        onRemove={jest.fn()}
      />,
    );

    const url = 'http://xyz.com';
    const input = getByRole('textbox');
    const cb = getByRole('checkbox');
    fireEvent.click(cb);
    fireEvent.change(input, { target: { value: url } });
    fireEvent.keyDown(input, { key: 'Enter' });
    expect(onChange).toHaveBeenCalledWith(url, true);
  });

  test('initial values are set', () => {
    const onChange = jest.fn();
    const url = 'http://xyz.com';
    const { getByRole } = render(
      <LinkEditor
        initialUrl={url}
        initialNewWindow
        forceNewWindow={false}
        onChange={onChange}
        onRemove={jest.fn()}
        onClose={jest.fn()}
      />,
    );

    const input = getByRole('textbox');
    const cb = getByRole('checkbox');
    expect(cb).toBeChecked();
    expect(input).toHaveValue(url);
  });

  test('clicking remove button calls onRemove', () => {
    const onRemove = jest.fn();
    const url = 'http://xyz.com';
    const { getByRole } = render(
      <LinkEditor
        initialUrl={url}
        initialNewWindow
        forceNewWindow={false}
        onChange={jest.fn()}
        onRemove={onRemove}
        onClose={jest.fn()}
      />,
    );

    const del = getByRole('button');
    fireEvent.click(del);
    expect(onRemove).toHaveBeenCalled();
  });

  test('force new window locks new window button', () => {
    const onChange = jest.fn();
    const { getByRole } = render(
      <LinkEditor
        initialUrl='http://x.com/'
        initialNewWindow={false}
        forceNewWindow
        onChange={onChange}
        onRemove={jest.fn()}
        onClose={jest.fn()}
      />,
    );

    const cb = getByRole('checkbox');
    expect(cb).toBeChecked();
    expect(cb).toBeDisabled();
  });

  test('bad url does not call update', () => {
    const onChange = jest.fn();
    const { getByRole } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={onChange}
        onRemove={jest.fn()}
        onClose={jest.fn()}
      />,
    );

    const url = 'bleh';
    const input = getByRole('textbox');
    fireEvent.change(input, { target: { value: url } });
    fireEvent.keyDown(input, { key: 'Enter' });
    expect(onChange).not.toHaveBeenCalled();
  });

  test('bad url highlights url text input', () => {
    const onChange = jest.fn();
    const { getByRole } = render(
      <LinkEditor
        initialUrl=''
        initialNewWindow={false}
        forceNewWindow={false}
        onChange={onChange}
        onRemove={jest.fn()}
        onClose={jest.fn()}
      />,
    );

    const url = 'bleh';
    const input = getByRole('textbox');
    fireEvent.change(input, { target: { value: url } });
    fireEvent.keyDown(input, { key: 'Enter' });
    expect(input.style.borderColor).toBe('red');
  });
});
