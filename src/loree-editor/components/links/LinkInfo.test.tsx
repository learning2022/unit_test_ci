import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { LinkInfo } from './LinkInfo';

describe('<LinkInfo>', () => {
  test('renders', () => {
    const { container } = render(<LinkInfo url='hello' onRemove={jest.fn()} />);
    expect(container).toBeInTheDocument();
  });

  test('clicking x calls onRemove', () => {
    const r = jest.fn();
    const { getByTitle } = render(<LinkInfo url='hello' onRemove={r} />);

    const button = getByTitle('linkinfo.remove');
    fireEvent.click(button);

    expect(r).toHaveBeenCalled();
  });
});
