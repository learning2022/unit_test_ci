const DEFAULTSTYLE = {
  // Navigation styles
  navigationPadding: '12px 20px',
  navigationMargin: '0px',

  // Table styles
  tablecaptionSide: 'bottom',
  tableCaptionFontFamily: 'Source Sans Pro',
  tablecaptionBorderWidth: '0',
  tablecaptionBorderStyle: 'solid',
  tablecaptionBorderColor: '#000000',
  tablecaptionPadding: '5px',
  tablecaptionMargin: '0px;',
};

export default DEFAULTSTYLE;
