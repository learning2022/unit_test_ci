import CONSTANTS from '../../src/loree-editor/constant';
import { IFrameWindow } from './a11y-client/types';
import { CustomHeaderType, HeaderListType, UserLanguageStore } from './interface';
import validHosts from './validHosts.json';
import crypto from 'crypto';
import {
  getElementByQuerySelector,
  getElementById,
  getElementsByQuerySelectorAll,
} from './common/dom';
import { apm } from '@elastic/apm-rum';

let selectedElementType: string = '';
let selectedBlock: HTMLElement | null = null;
let userLanguageStore: UserLanguageStore = {
  selectedMiniMenuLanguage: '',
};

export const isValidHost = (host: string): boolean => {
  return validHosts.includes(host);
};

export const generateRandomString = (length: number): string => {
  return getRandomString(length).substring(0, length);
};

export const getRandomString = (length: number): string => {
  return crypto.randomBytes(length).toString('hex');
};

export const generateDynamicClassNameForElement = (element: HTMLElement): string | null => {
  let className = null;
  if (element) {
    if (element.className.includes(CONSTANTS.LOREE_CLASS_NAME_PREFIX)) {
      element.classList.forEach((name: string): void => {
        if (name.includes(CONSTANTS.LOREE_CLASS_NAME_PREFIX)) className = name;
      });
    } else {
      className = `${CONSTANTS.LOREE_CLASS_NAME_PREFIX}${generateRandomString(6)}`;
    }
  }
  return className;
};

export const getEditorElementsByClassName = (name: string) => {
  return document.getElementsByClassName(name);
};

export const getEditorElementById = (id: string) => {
  return document.getElementById(id);
};

export const getIframedocumentElementsByClassName = (iframeDocument: _Any, name: string): _Any => {
  return iframeDocument?.getElementsByClassName(name);
};

export const getIframedocumentElementById = (iframeDocument: _Any, id: string): _Any => {
  return iframeDocument?.getElementById(id);
};

export const appendElementToBody = (element: HTMLElement) => {
  document.body.append(element);
};

export const isClassExist = (sourceClassList: string[], targetClassList: string[]): boolean => {
  return sourceClassList.some((name) => targetClassList.includes(name));
};

export const setSelectedElementType = (type: string) => {
  selectedElementType = type;
};
export const getSelectedElementType = (): string => {
  return selectedElementType;
};

export const addClassToElement = (element: HTMLElement | null, className: string) => {
  element?.classList?.add(className);
};

export const removeClassToElement = (element: HTMLElement | null, className: string) => {
  element?.classList?.remove(className);
};

export const setSelectedBlock = (element: HTMLElement) => {
  selectedBlock = element;
};

export const getSelectedBlock = (): HTMLElement => {
  return selectedBlock as HTMLElement;
};

export const isClassListContains = (element: HTMLElement | null, className: string): boolean => {
  return element ? element.classList.contains(className) : false;
};

export const hideTextOptionSubElements = (
  iframeDocument: HTMLDocument | null,
  textOptionSubElements: string[],
) => {
  textOptionSubElements.map((id: string) => {
    const element = iframeDocument?.getElementById(id);
    if (element && element.style.display !== 'none') element.style.display = 'none';
    return element;
  });
};

export const convertIntoDoubleDigit = (num: number) => {
  if (num > 9) {
    return '' + num;
  } else {
    return ('0' + num).slice(-2);
  }
};
export const resizeSaveIconElement = (iframeWindow: IFrameWindow | null) => {
  if (!iframeWindow) return;
  iframeWindow?.addEventListener('resize', function () {
    const saveIconWrapper = document.getElementById(CONSTANTS.LOREE_SAVE_ICON_ELEMENT);
    if (saveIconWrapper) {
      saveIconWrapper.style.width = iframeWindow.innerWidth + 'px';
    }
  });
};

export const getElementsByTagName = (type: string) => {
  return document.getElementsByTagName(type);
};

export const getElementByTagName = (selectedElement: HTMLElement, tagName: string): HTMLElement => {
  return selectedElement.getElementsByTagName(tagName)[0] as HTMLElement;
};

export const getElementByClassName = (
  selectedElement: HTMLElement,
  className: string,
): HTMLElement => {
  return selectedElement.getElementsByClassName(className)[0] as HTMLElement;
};

export const removeStyleToElement = (element: HTMLElement, property: string) => {
  element.style.removeProperty(property);
};

export const toggleElementVisibility = (elementId: string, visibility: boolean) => {
  const element = getEditorElementById(elementId);
  const parent = element?.parentElement;
  if (element && parent) {
    if (!visibility) {
      parent.style.display = 'none';
    } else if (parent.style.display === 'none') removeStyleToElement(parent, 'display');
  }
};

export const checkHasFontFamily = (
  fontList: HeaderListType[],
  element: CustomHeaderType,
  headerType: string,
  pageType: string,
) => {
  const hashFont = fontList.find((v) => v.name === element[headerType].font);
  if (hashFont) return;
  element[headerType].font = pageType === 'editor' ? CONSTANTS.DEFAULT_EDITOR_FONT_FAMILY : '';
};

export const checkFontFamily = (
  headerList: CustomHeaderType[],
  fontList: HeaderListType[],
  pageType: string,
) => {
  headerList?.forEach((element: CustomHeaderType, index: number) => {
    const headerIndex = index + 1;
    if (index >= 0 && index <= 5) {
      const headerType = 'h' + headerIndex;
      checkHasFontFamily(fontList, element, headerType, pageType);
    } else {
      const headerType = 'paragraph';
      checkHasFontFamily(fontList, element, headerType, pageType);
    }
  });
};

export const replaceHyphenSpecialCharacter = (content: string): string => {
  return content.replace(/-|_/g, '');
};

export const getIframeDocument = (): HTMLDocument | null => {
  const iframe: HTMLIFrameElement | null = document.getElementById(
    CONSTANTS.LOREE_IFRAME,
  ) as HTMLIFrameElement;
  if (iframe) return iframe.contentDocument;
  return null;
};

export const isContentWrapperEmpty = (): boolean => {
  return (
    getIframeDocument()?.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER)?.innerHTML === ''
  );
};

export const removeSelectedElementSection = (): void => {
  const activeElement = getElementById(`${CONSTANTS.LOREE_SIDEBAR_ELEMENT_COLLAPSE}`);
  if (!activeElement?.childNodes) {
    return;
  }
  const elements = Array.prototype.slice.call(activeElement.childNodes[0].childNodes);
  for (const element of elements) {
    element.childNodes[0].classList.remove('active');
  }
};

export const captureErrorLog = (errorType: string, error: _Any) => {
  apm.captureError(error);
  console.error(`Error in ${errorType}`, error);
};

export const createBorderStyleOptions = (): string => {
  const options = ['Dashed', 'Dotted', 'Double', 'Solid'];
  let optionList = '';
  for (const opt of options) {
    const optionElement: HTMLOptionElement = document.createElement('option');
    optionElement.value = opt.toLowerCase();
    optionElement.innerHTML = opt;
    optionList += optionElement.outerHTML;
  }
  return optionList;
};

export const selectedIndexForBorderStyle = (parent: HTMLElement, borderStyle: string): number =>
  Array.from(parent.children).findIndex((opt) => (opt as HTMLOptionElement).value === borderStyle);

export const hideHeaderLanguageMenu = () => {
  const languageMenu = getElementByQuerySelector('.loree-language-menu');
  if (!languageMenu) return;
  languageMenu.classList.remove('show');
};

export const setColorNavAlignment = (element: HTMLElement, color: string) => {
  return (element.style.stroke = color);
};

export const getCurrentI18nLanguage = (): string => {
  let currentLanguage = localStorage.getItem('loreeLng');
  return (
    (currentLanguage = currentLanguage?.includes('-')
      ? currentLanguage.split('-')[0]
      : currentLanguage) ?? 'en'
  );
};

export const setCurrentI18nLanguage = (langId: string) => {
  localStorage.setItem('loreeLng', langId);
};

export const getUserLanguageStore = () => {
  return userLanguageStore;
};

export const updateUserLanguageStore = (key: string, value: string) => {
  userLanguageStore = { ...userLanguageStore, [key]: value };
};

export const addUserLanguageAsDefaultToElement = (element: Element | HTMLElement) => {
  const getTextElements = getElementsByQuerySelectorAll(
    'h1, h2, h3, h4, h5, h6, p,caption',
    element,
  );
  const Language =
    process.env.REACT_APP_ENABLE_LOCALISATION === 'false'
      ? 'en'
      : getUserLanguageStore().selectedMiniMenuLanguage !== ''
      ? getUserLanguageStore().selectedMiniMenuLanguage
      : getCurrentI18nLanguage();
  if (getTextElements.length) {
    for (const textElement of getTextElements) {
      textElement.setAttribute('lang', Language);
    }
  }
  if (!getTextElements.length && element.classList.contains('loree-iframe-content-element')) {
    element.setAttribute('lang', Language);
  }
};
