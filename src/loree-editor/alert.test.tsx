import {
  customComponentsAlert,
  hideCustomComponentsAlert,
  customElementAlert,
  customElementAlertRedirectDashboard,
} from './alert';
import CONSTANTS from './constant';

jest.mock('../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#alert', () => {
  beforeEach(() => {
    const doc = document.body;
    const parentWrapper = document.createElement('div');
    parentWrapper.id = CONSTANTS.LOREE_WRAPPER;
    doc.appendChild(parentWrapper);
  });
  afterEach(() => {
    document.getElementsByTagName('html')[0].innerHTML = '';
  });
  describe('#CustomBlock', () => {
    test('render the alert when customBlock is selected', () => {
      customComponentsAlert('custom row');
      const element = document.getElementById(CONSTANTS.LOREE_CUSTOM_ELEMENTS_ALERT_CONTAINER);
      expect(element?.className).toStrictEqual(
        'custom-element-alert-container p-2 d-flex mx-auto position-absolute align-items-center',
      );
      expect(element?.hasChildNodes()).toStrictEqual(true);
    });
    test('remove the alert when customBock added in editor', () => {
      hideCustomComponentsAlert();
      const element = document.getElementById(CONSTANTS.LOREE_CUSTOM_ELEMENTS_ALERT_CONTAINER);
      expect(element).toBeNull();
    });
    test('Check update alert message', () => {
      customElementAlert('template', 'updated');
      const element = document.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
      expect(element?.childNodes[1].textContent).toEqual(
        'Custom template updated alert.successfully',
      );
    });

    test('Check update alert message for redirecting the Dashboard', () => {
      customElementAlertRedirectDashboard();
      const element = document.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
      expect(element?.childNodes[1].textContent).toEqual('alert.redirectingtodashboard');
    });
  });
});
