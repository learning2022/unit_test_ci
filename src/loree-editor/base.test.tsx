import React from 'react';
import Base from './base';
import CONSTANTS from './constant';
import * as alert from './alert';
import * as baseObject from './base';
import Row from './modules/sidebar/row';
import RowSet from './row';
import {
  baseMockData,
  imgAnchorMockData,
  selectedElement,
  iFrameWrapperMockData,
  selectionMockData,
  tableMockData,
  colorPickerMockData,
  imageWithText,
  interactiveElementMockData,
  externalToolElementMockData,
  selectedRowElement,
  selectedColumnElement,
  sidebarElement,
  createAndAppendIframeToDOM,
  loreeWrapper,
  containerWrappedWithLoreeWrapper,
  emptyContainerElement,
  emptyRowColumnElement,
  loreeContentWrapperElement,
} from './baseMockData';
import ReactDOM from 'react-dom';
import { fireEvent, render, screen, configure } from '@testing-library/react';
import {
  appendElementToBody,
  getEditorElementById,
  getEditorElementsByClassName,
  getElementsByTagName,
  getIframedocumentElementById,
} from './utils';
import Iframe from './modules/iframe/iframe';
import {
  createDiv,
  createElement,
  getElementById,
  getElementsByClassName,
  getIFrameElementById,
  getNElementByClassName,
} from './common/dom';
import Design from './modules/sidebar/design';
import BlockOptions from './modules/blockOptions/blockOptions';
import { featuresList } from './modules/textOptions/baseFeatureListMockData';
import Bootstrap from './modules/iframe/templates/bootstrap';
import { templateConfig } from './modules/header/templateMockData';
import * as utils from './utils';
import { isSelectionFirstElementInContent, showLinkOption } from './base/baseUtil';
import ContainerDesign from './modules/sidebar/containerDesign';
import { mockColorPickerInstance } from './modules/colorPicker/utils';
import { modalContainer } from './modules/customBlocks/customBlockHandler';
import { customBlockInfoModal } from './modules/customBlocks/customBlockEvents';
import { isContainer, setEditMode } from '../utils/saveContent';

jest.mock('../../src/i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

configure({ testIdAttribute: 'class' });

let baseInstance: Base;
const customBlockType = [
  ['Row', 'custom row'],
  ['Template', 'template'],
  ['Elements', 'custom element'],
];

describe('Block options', () => {
  const iframe = createElement('iframe') as HTMLIFrameElement;
  beforeEach(() => {
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
    iFrameWrapperMockData(iframe);
    const loreeWrapper = document.createElement('div');
    loreeWrapper.id = CONSTANTS.LOREE_WRAPPER;
    document.body.appendChild(loreeWrapper);
    const designInstance = new Design();
    const blockOptions = new BlockOptions();
    baseInstance = new Base();
    baseInstance.updateFeaturesList(featuresList);
    const sampleParagraph = createElement('p', '', CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT);
    sampleParagraph.innerHTML = 'Lorem Ipsum dolor sit Amet';
    designInstance.initiate({});
    designInstance.attachDesignContent('element', sampleParagraph, { fontstyles: true });
    baseInstance.showDesignSection('element', sampleParagraph);
    blockOptions.initiate({});
    process.env.REACT_APP_ENABLE_LOCALISATION_MINI_MENU = 'true';
  });
  afterAll(() => {
    process.env.REACT_APP_ENABLE_LOCALISATION_MINI_MENU = 'false';
  });
  test('click on delete icon deletes element and hides design section', () => {
    const sidebarDesign = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION);
    expect(sidebarDesign?.style.display).toEqual('block');
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON)?.click();
    expect(sidebarDesign?.style.display).toEqual('none');
  });
  test('external link option exist for image with text element', () => {
    const divWrapper = createDiv('sampleDiv');
    divWrapper.innerHTML = imageWithText;
    baseInstance.changeSelectedElement(divWrapper.firstChild as HTMLElement);
    expect(showLinkOption()).toEqual(true);
    baseInstance.showBlockOptions(false, false, true);
    const externalLinkBtn = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_EXTERNAL_LINK_BUTTON,
    ) as HTMLElement;
    expect(externalLinkBtn.style.display).toBe('inline-flex');
  });
  test('save icon tooltip title', () => {
    baseInstance.showBlockOptions(false, true, false);
    expect(
      iframe.contentDocument
        ?.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_SAVE_BUTTON)
        ?.getAttribute('data-original-title'),
    ).toEqual('Save Elements');
  });
  test('delete column alert', () => {
    const column = iframe.contentDocument?.getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN,
    )[0];
    baseInstance.changeSelectedElement(column as HTMLElement);

    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON)?.click();
    const columnAlertWrapper = document.getElementById(CONSTANTS.LOREE_DELETE_COLUMN_WRAPPER);
    expect(columnAlertWrapper).toBeInTheDOM();

    (columnAlertWrapper?.getElementsByClassName('btn-icon')[0] as HTMLElement).click();
    expect(document.getElementById(CONSTANTS.LOREE_DELETE_COLUMN_WRAPPER)).not.toBeInTheDOM();
  });
  test('delete row alert', () => {
    const row = iframe.contentDocument?.getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_CONTENT_ROW,
    )[0];
    baseInstance.changeSelectedElement(row as HTMLElement);

    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON)?.click();
    const rowAlertWrapper = document.getElementById(CONSTANTS.LOREE_DELETE_ROW_WRAPPER);
    expect(document.getElementById(CONSTANTS.LOREE_DELETE_ROW_WRAPPER)).toBeInTheDOM();

    (rowAlertWrapper?.getElementsByClassName('btn-icon')[0] as HTMLElement).click();
    expect(document.getElementById(CONSTANTS.LOREE_DELETE_COLUMN_WRAPPER)).not.toBeInTheDOM();
  });

  test('languages option must be enabled for text element', () => {
    const divWrapper = createDiv('testDiv');
    divWrapper.innerHTML = `<p>Insert Text here</p>`;
    baseInstance.changeSelectedElement(divWrapper.firstChild as HTMLElement);
    baseInstance.showBlockOptions(false, false, true);
    expect(getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON)?.style.display).toBe(
      'inline-flex',
    );
    expect(
      getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON)?.getAttribute('title'),
    ).toEqual('blockoption.languages');
  });
  test('languages option lists must be hidden before click', () => {
    const wrapper = getIFrameElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
    );
    expect(wrapper.style.display).toBe('none');
  });
  test('language Iso label button must be visible and default code must be shown before lang selection', () => {
    const divWrapper = createDiv('loreeColumn');
    divWrapper.innerHTML = `<p>Insert Text here</p>`;
    baseInstance.changeSelectedElement(divWrapper.firstChild as HTMLElement);
    expect(getIFrameElementById(CONSTANTS.LOREE_LANGUAGE_ISO_LABEL_BUTTON)?.style.display).toBe(
      'inline-flex',
    );
    expect(getIFrameElementById(CONSTANTS.LOREE_LANGUAGE_ISO_LABEL_BUTTON)?.innerHTML).toBe('(en)');
  });
  test('language lists must be shown on click and length should equal the loree language lists', () => {
    const languageButton = getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON);
    const wrapper = getIFrameElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
    );
    languageButton.click();
    expect(wrapper.style.display).toBe('flex');
    expect(wrapper.childElementCount).toEqual(CONSTANTS.LOREE_MINI_MENU_LANGUAGE_LISTS.length);
  });
  test('language dropdown list has english as selected and position as top by default', () => {
    const languageButton = getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON);
    const wrapper = getIFrameElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
    );
    languageButton.click();
    expect((wrapper.children[0] as HTMLElement).textContent).toBe('(en) English');
    expect((wrapper.children[0] as HTMLElement).classList.contains('active-language-option')).toBe(
      true,
    );
  });
  test('the selected element must have the lang attribute on selecting a language from list', () => {
    const wrapper = getIFrameElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
    );
    for (const lang of wrapper.children) {
      (lang as HTMLElement).click();
      expect(baseInstance.getSelectedElement() as HTMLElement).toHaveAttribute(
        'lang',
        (lang as HTMLElement).id,
      );
    }
  });
  test('selected language must be moved to top of language list', () => {
    const wrapper = getIFrameElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
    );
    const selectedLang = 'ta';
    for (const lang of wrapper.children) {
      if (selectedLang === (lang as HTMLElement).id) (lang as HTMLElement).click();
    }
    const languageButton = getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON);
    languageButton.click();
    expect((wrapper.children[0] as HTMLElement).textContent?.split(' ')[0]).toEqual(
      `(${selectedLang})`,
    );
  });
  test('langauage option must not be visible for non text elements', () => {
    const imgElement = createElement('img');
    baseInstance.changeSelectedElement(imgElement);
    baseInstance.showBlockOptions(false, false, true);
    const languageButton = getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON);
    expect(languageButton.style.display).toBe('none');
    expect(getIFrameElementById(CONSTANTS.LOREE_LANGUAGE_ISO_LABEL_BUTTON)?.style.display).toBe(
      'none',
    );
  });
  test('hide selection function must hide Iso code label', () => {
    baseInstance.hideLanguageIsoLabel = jest.fn();
    baseInstance.hideSelections();
    expect(baseInstance.hideLanguageIsoLabel).toBeCalledTimes(1);
  });
});

describe('testcase for customcolorpicker', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
    baseInstance = new Base();
    const element = document.createElement('div');
    element.innerHTML =
      '<input class="pcr-save" value="Apply" type="button" aria-label="save and close">';
    document.body.append(element);
  });

  test('userBorderwidth function should called one time when customecolorpicker called', () => {
    const spyuserBorderColor = jest.spyOn(baseInstance, 'userBorderWidth');
    baseInstance.customiseTextBorderColorPicker();
    expect(spyuserBorderColor).toBeCalledTimes(1);
  });

  test('clicking save button in the customiseTextBorderColorPicker', () => {
    baseInstance.customiseTextBackgroundColorPickr();
    document.body.firstChild?.addEventListener('click', function () {
      baseInstance.userBorderColor = jest.fn().mockImplementation(() => 'colorchange');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(baseInstance.userBorderColor()).toBe('colorchange');
  });

  test('clicking clear button in the customiseTextBorderColorPicker', () => {
    baseInstance.customiseTextBackgroundColorPickr();
    document.body.firstChild?.addEventListener('click', function () {
      baseInstance.userBorderColor = jest.fn().mockImplementation(() => '#FFFFFF');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(baseInstance.userBorderColor()).toBe('#FFFFFF');
  });

  test('clicking save button in the customiseTextColorPickr', () => {
    baseInstance.customiseTextBackgroundColorPickr();
    document.body.firstChild?.addEventListener('click', function () {
      baseInstance.userBorderColor = jest.fn().mockImplementation(() => 'colorchange');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(baseInstance.userBorderColor()).toBe('colorchange');
  });

  test('clicking cancel button in the customiseTextColorPickr', () => {
    baseInstance.customiseTextBackgroundColorPickr();
    document.body.firstChild?.addEventListener('click', function () {
      baseInstance.userBorderColor = jest.fn().mockImplementation(() => 'colorchange');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(baseInstance.userBorderColor()).toBe('colorchange');
  });

  test('clicking clear button in the customisebackGroundColorPickr', () => {
    baseInstance.customiseTextBackgroundColorPickr();
    document.body.firstChild?.addEventListener('click', function () {
      baseInstance.userBorderColor = jest.fn().mockImplementation(() => '#FFFFFF');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(baseInstance.userBorderColor()).toBe('#FFFFFF');
  });
});

describe('functional testcase for setuserForegroundColor', () => {
  beforeEach(() => {
    baseInstance = new Base();
  });
  test('testing the setuserForegroundColor function', () => {
    const color = '#FFFFFF';
    const returnvalue = baseInstance.setuserForegroundColor(color);
    expect(returnvalue).toBe('#FFFFFF');
  });
});

describe('#CustomBlocks', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    const doc = document.body;
    const parentWrapper = document.createElement('div');
    parentWrapper.id = CONSTANTS.LOREE_IFRAME;
    parentWrapper.innerHTML = `
    <div id=${CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER}></div><div id=${CONSTANTS.LOREE_HEADER}></div>
    <div id='save-icon-position'></div>
    <div id=${CONSTANTS.LOREE_SIDEBAR}>
    <div id=${CONSTANTS.LOREE_ADD_ROW_BUTTON_WRAPPER}></div>
    <div id=${CONSTANTS.LOREE_ADD_ELEMENT_BUTTON}></div>
    <div id=${CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER}></div>
    <div id=${CONSTANTS.LOREE_INSERT_ROW_BUTTON}></div>`;
    doc.append(parentWrapper);
    baseInstance = new Base();
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
  });
  test.each(customBlockType)(
    `append the cancel & add button to the selected custom block`,
    (category) => {
      baseInstance.handleLoreeEditorPointerEvents = jest
        .fn()
        .mockImplementation((action, category) => true);
      baseInstance.handleCustomBlockAppendButton('Column');
      expect(baseInstance.handleLoreeEditorPointerEvents).toBeCalledTimes(1);
    },
  );
  test.each(customBlockType)(
    `disable the pointer events to the entire editor when custom block is selected`,
    (category) => {
      baseInstance.handleLoreeEditorPointerEvents('disable', category);
      expect(
        getEditorElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER)?.style.pointerEvents,
      ).toEqual('none');
    },
  );
  test.each(customBlockType)(
    `alert should not be enabled when custom block added in editor`,
    (category, alertText) => {
      const enableAlert = jest.spyOn(alert, 'customComponentsAlert');
      baseInstance.handleLoreeEditorPointerEvents('enable', category);
      expect(enableAlert).toBeCalledTimes(0);
      expect(enableAlert).not.toBeCalledWith(alertText);
    },
  );
  test.each(customBlockType)(
    `enable alert when custom block is selected`,
    (category, alertText) => {
      const enableAlert = jest.spyOn(alert, 'customComponentsAlert');
      baseInstance.handleLoreeEditorPointerEvents('disable', category);
      expect(enableAlert).toBeCalledTimes(1);
      expect(enableAlert).toBeCalledWith(alertText);
    },
  );
  test.each(customBlockType)(
    `alert should not be disabled when custom block is selected`,
    (category) => {
      const disableAlert = jest.spyOn(alert, 'hideCustomComponentsAlert');
      baseInstance.handleLoreeEditorPointerEvents('disable', category);
      expect(disableAlert).toBeCalledTimes(0);
    },
  );
  test.each(customBlockType)(
    `disable the alert when selected custom block added in editor`,
    (category) => {
      const disableAlert = jest.spyOn(alert, 'hideCustomComponentsAlert');
      baseInstance.handleLoreeEditorPointerEvents('enable', category);
      expect(disableAlert).toBeCalledTimes(1);
    },
  );
});

describe('#hide for block Options', () => {
  const iframeObject = new Iframe();
  beforeEach(() => {
    const iframe = document.createElement('iframe');
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    wrapper.innerHTML =
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div class="row loree-iframe-content-menu navigation2 element-highlight" style="display: inline-flex; width:100%; margin: 0px; color:#000000;"><nav class="navbar navbar-expand-sm navbar-light loree-iframe-content-menu" style="padding: 0px; margin-left: auto; margin-right: 0px; flex: 1;"><ul class="navbar-nav loree-iframe-content-menu loree-iframe-content-element" style="margin: 0px 0px 0px auto; list-style: none; padding: 0px; flex-wrap: wrap; justify-content: flex-end;"><li class="loree-iframe-content-menu nav-item loree-iframe-content-element" style="padding: 0px;"><p class="loree-navigate-option-items loree-iframe-content-element" style="padding: 12px 20px; margin: 0px; border-width: 0px; border-style: solid; border-color: #000000; font-family: Open Sans; font-size: 16px;"> Menu&nbsp;</p></li><li class="loree-iframe-content-menu nav-item loree-iframe-content-element" style="padding: 0px;"><p class="loree-navigate-option-items loree-iframe-content-element" style="padding: 12px 20px; margin: 0px; border-width: 0px; border-style: solid; border-color: #000000; font-family: Open Sans; font-size: 16px;"> Menu Item</p></li><li class="loree-iframe-content-menu nav-item loree-iframe-content-element" style="padding: 0px;"><p class="loree-navigate-option-items loree-iframe-content-element" style="padding: 12px 20px; border-width: 0px; border-style: solid; border-color: #000000; margin: 0px; font-family: Open Sans; font-size: 16px;"> Menu Item</p></li><li class="loree-iframe-content-menu nav-item loree-iframe-content-element" style="padding: 0px;"><p class="loree-navigate-option-items loree-iframe-content-element" style="padding: 12px 20px; border-width: 0px; border-style: solid; border-color: #000000; margin: 0px; font-family: Open Sans; font-size: 16px;"> Menu Item</p></li><li class="loree-iframe-content-menu nav-item loree-iframe-content-element" style="padding: 0px;"><p class="loree-navigate-option-items loree-iframe-content-element" style="padding: 12px 20px; border-width: 0px; border-style: solid; border-color: #000000; margin: 0px; font-family: Open Sans; font-size: 16px;"> Menu Item</p></li></ul></nav></div></div></div>';
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(wrapper.outerHTML);
    iframeDoc?.close();
    document.body.append(iframe);
    iframeObject.addIconClickIfPresent = jest.fn();
    iframeObject.closeLanguageOption = jest.fn();
    iframeObject.getDocument = jest.fn().mockImplementation(() => document);
    iframeObject.attachEventListeners();
  });

  test('hide the blockoption to the click event', () => {
    const hideBlockOptions = jest.spyOn(iframeObject, 'hideBlockOptions');
    const contentWrapper = getIframedocumentElementById(
      document,
      CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
    ) as HTMLElement;
    const dataBlockOption = document.querySelector('#loree-block-options-wrapper') as HTMLElement;
    fireEvent.click(contentWrapper, { detail: 2 });
    expect(hideBlockOptions).toHaveBeenCalled();
    expect(dataBlockOption.style.display).toBe('none');
  });
});

describe('removing highlighted section on selected element', () => {
  let specialElementText: HTMLElement;
  let videoTag: HTMLElement;
  let hideColumnIcon: jest.SpyInstance;
  let hideMenuIcon: jest.SpyInstance;
  beforeEach(() => {
    specialElementText = document.createElement('div');
    videoTag = document.createElement('div');
    specialElementText.className = `${CONSTANTS.LOREE_ELEMENT_HIGHLIGHT} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}`;
    videoTag.id = CONSTANTS.LOREE_IFRAME_VIDEO_ID;
    baseInstance = new Base();
    hideColumnIcon = jest.spyOn(baseInstance, 'hideHamburgerIconForColumn');
    hideMenuIcon = jest.spyOn(baseInstance, 'hideHamburgerIconForMenu');
  });

  afterEach(() => {
    specialElementText.outerHTML = '';
    videoTag.outerHTML = '';
  });

  test('remove element highlight class', () => {
    baseInstance.removeHighlightSelectedElement(specialElementText);
    expect(specialElementText).not.toHaveClass(CONSTANTS.LOREE_ELEMENT_HIGHLIGHT);
  });

  test('element with LOREE_IFRAME_CONTENT_ICON id', () => {
    baseInstance.removeHighlightSelectedElement(specialElementText);
    expect(hideColumnIcon).toBeCalled();
    expect(hideMenuIcon).toBeCalled();
  });

  test('element not having LOREE_IFRAME_CONTENT_ICON id', () => {
    baseInstance.removeHighlightSelectedElement(videoTag);
    expect(hideColumnIcon).not.toHaveBeenCalled();
    expect(hideMenuIcon).not.toHaveBeenCalled();
  });
});

describe('#editorUserActions', () => {
  let wrapper: HTMLElement;
  const baseInstance = new Base();
  const rowInstance = new Row();
  function rowColStructure() {
    const rowSection = document.createElement('div');
    rowSection.id = CONSTANTS.LOREE_SIDEBAR_ROW_SECTION;
    rowInstance.attachContent(rowSection, { customrowfiltersearch: '' });
    return rowSection;
  }
  function imgWrapper() {
    const imgWrapper = document.createElement('div');
    imgWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
    const imgElement = document.createElement('img');
    imgElement.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE;
    imgWrapper.appendChild(imgElement);
    return imgWrapper;
  }
  function elementWithCustomStyle() {
    const ptag = document.createElement('p');
    ptag.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    ptag.style.cssText = 'font-size:20px; font-family:Arial';
    return ptag;
  }
  beforeEach(() => {
    document.body.innerHTML = '';
    createAndAppendIframeToDOM();
    const sidebar = document.createElement('div');
    sidebar.className = 'sidebarElementsWrapper';
    document.body.appendChild(sidebar);
    wrapper = document.createElement('div');
    wrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    baseInstance.handleSelectedContentChanges = jest.fn();
    rowInstance.handleSelectedContentChanges = jest.fn();
    baseInstance.showSidebar = jest.fn();
    baseInstance.hideDesignSection = jest.fn();
    baseInstance.hideContainerDesignSection = jest.fn();
    baseInstance.hideMenuDesignSection = jest.fn();
    baseInstance.hideTableDesignSection = jest.fn();
    baseInstance.hideLanguageIsoLabel = jest.fn();
  });
  test('add row function stored in undo redohideDes; history', () => {
    const oneColRow = rowColStructure().children[0].children[0] as HTMLElement;
    rowInstance.getSelectedElement = jest.fn().mockImplementation(() => wrapper);
    oneColRow.click();
    expect(rowInstance.handleSelectedContentChanges).toBeCalledTimes(1);
  });
  test('add image element function stored in undo redo history', () => {
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() => imgWrapper() as HTMLElement);
    baseInstance.appendImageToSelectedColumn(imgWrapper().outerHTML, wrapper);
    expect(baseInstance.handleSelectedContentChanges).toBeCalledTimes(1);
  });

  test('add text element function stored in undo redo history', () => {
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => wrapper);
    baseInstance.attachCustomizeStyleToElement = jest
      .fn()
      .mockImplementation(() => elementWithCustomStyle());
    baseInstance.appendElementToSelectedColumn(
      `<p class='loree-iframe-content-element'>Insert text here</p>`,
    );
    expect(baseInstance.handleSelectedContentChanges).toBeCalledTimes(1);
  });

  test('is duplicate function stored in undo redo history', async () => {
    await baseInstance.isDuplicate();
    const expectedCalls = [
      baseInstance.handleSelectedContentChanges,
      baseInstance.hideDesignSection,
      baseInstance.hideContainerDesignSection,
      baseInstance.hideMenuDesignSection,
      baseInstance.hideTableDesignSection,
      baseInstance.hideLanguageIsoLabel,
    ];
    expectedCalls.forEach((arg) => expect(arg).toHaveBeenNthCalledWith(1));
  });

  test('is delete function stored in undo redo history', () => {
    baseInstance.isDelete();
    expect(baseInstance.handleSelectedContentChanges).toBeCalledTimes(1);
  });

  test('is save function not stored in undo redo history', () => {
    baseInstance.isSave();
    expect(baseInstance.handleSelectedContentChanges).not.toBeCalled();
  });
});

describe('#handleUserActionFromUndoRedo', () => {
  function iFrameWrapper() {
    const iframe = document.createElement('iframe');
    iframe.id = CONSTANTS.LOREE_IFRAME;
    document.body.appendChild(iframe);
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(wrapper.outerHTML);
    iframeDoc?.close();
    return iframe.contentDocument as Document;
  }
  function rowColStructure() {
    const row = document.createElement('div');
    row.className = CONSTANTS.LOREE_IFRAME_CONTENT_ROW;
    const col = document.createElement('div');
    col.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN;
    const ptag = document.createElement('p');
    ptag.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    col.appendChild(ptag);
    row.appendChild(col);
    return row;
  }
  beforeEach(() => {
    baseInstance.hideSelections = jest.fn();
    baseInstance.getDocument = jest.fn().mockImplementation(() => iFrameWrapper());
  });
  test('initial history state updates', () => {
    baseInstance.handleSelectedContentChanges();
    expect(baseObject.history.length).toBe(1);
    expect(baseObject.userClickAction).toBe(1);
  });
  test('on user action event the history state updates', () => {
    (iFrameWrapper().childNodes[0] as HTMLElement).innerHTML = rowColStructure().outerHTML;
    baseInstance.getHtml = jest.fn().mockImplementation(() => iFrameWrapper());
    baseInstance.handleSelectedContentChanges();
    expect(baseObject.history.length).toBe(2);
    expect(baseObject.userClickAction).toBe(2);
  });
  test('undo action', () => {
    baseInstance.handleUndoHistory();
    baseInstance.handleUndoHistory();
    expect(baseObject.history.length).toBe(2);
    expect(baseObject.userClickAction).toBe(0);
  });
  test('redo action', () => {
    baseInstance.handleRedoHistory();
    expect(baseObject.history.length).toBe(2);
    expect(baseObject.userClickAction).toBe(1);
  });
});

describe('Given a loree editor', () => {
  describe('While setting HTML into an Editor', () => {
    function iFrameWrapper() {
      const iframe = document.createElement('iframe');
      iframe.id = CONSTANTS.LOREE_IFRAME;
      const wrapper = document.createElement('div');
      wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
      iframe.appendChild(wrapper);
      document.body.appendChild(iframe);
      const iframeDoc = iframe.contentWindow?.document;
      iframeDoc?.open().write(wrapper.outerHTML);
      iframeDoc?.close();
      return iframe.contentDocument as Document;
    }
    beforeEach(() => {
      baseInstance = new Base();
      baseInstance.getDocument = jest.fn().mockImplementation(() => iFrameWrapper());
    });

    test('It should remove the empty tags from html of P tag element', () => {
      const removeEmptyTagsSpy = jest.spyOn(baseInstance, 'removeEmptyTags');
      const convertContentSpy = jest.spyOn(baseInstance, 'convertContent');
      baseInstance.setHtml(baseMockData.htmlPTagContent);
      expect(convertContentSpy).toHaveBeenCalledTimes(1);
      expect(removeEmptyTagsSpy).toHaveBeenCalledTimes(2);
      expect(convertContentSpy).toReturnWith(baseMockData.singlRowColPTagContent);
      expect(removeEmptyTagsSpy).toHaveLastReturnedWith(baseMockData.cleanedHtmlPTagContent);
    });
    test('It should remove the empty tags from html of DIV tag element', () => {
      const removeEmptyTagsSpy = jest.spyOn(baseInstance, 'removeEmptyTags');
      const convertContentSpy = jest.spyOn(baseInstance, 'convertContent');
      baseInstance.setHtml(baseMockData.htmlDivTagContent);
      expect(convertContentSpy).toHaveBeenCalledTimes(1);
      expect(removeEmptyTagsSpy).toHaveBeenCalledTimes(2);
      expect(convertContentSpy).toReturnWith(baseMockData.singlRowColDivTagContent);
      expect(removeEmptyTagsSpy).toHaveLastReturnedWith(baseMockData.cleanedHtmlDivTagContent);
    });
  });
});

describe('#AddOrRemoveMinHeight', () => {
  let baseInstance: Base;
  const selection = new DOMParser().parseFromString(selectedElement, 'text/xml');
  function iFrameWrapper() {
    const iframe = document.createElement('iframe');
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    wrapper.append(selection.firstChild as HTMLElement);
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(wrapper.outerHTML);
    iframeDoc?.close();
    return iframe.contentDocument as Document;
  }
  beforeEach(() => {
    jest.clearAllMocks();
    baseInstance = new Base();
    baseInstance.getDocument = jest.fn().mockImplementation(() => iFrameWrapper());
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() => selection.firstChild as HTMLElement);
  });

  test('addMInheighttoAddElementButton', () => {
    const firstChild = selection.firstChild as HTMLElement;
    baseInstance.addMinHeightToSelectedElement(firstChild);
    expect(firstChild.classList.length).toBe(3);
    expect(firstChild.classList[2]).toBe('coloumn-minHeight');
  });

  test('addMinheight when showing add element button to the container', () => {
    baseInstance.addMinHeightToSelectedElement = jest
      .fn()
      .mockImplementation(() => selection.firstChild as HTMLElement);
    baseInstance.showAddRowButtonToSelectedContainer();
    expect(baseInstance.addMinHeightToSelectedElement).toHaveBeenCalledTimes(1);
  });

  test('addMinheight when showing close element button', () => {
    baseInstance.addMinHeightToSelectedElement = jest
      .fn()
      .mockImplementation(() => selection.firstChild as HTMLElement);
    baseInstance.appendCloseAddElementButtonToSelectedElement();
    expect(baseInstance.addMinHeightToSelectedElement).toHaveBeenCalledTimes(1);
  });

  test('addMinheight when showing close element button to the container', () => {
    baseInstance.addMinHeightToSelectedElement = jest
      .fn()
      .mockImplementation(() => selection.firstChild as HTMLElement);
    baseInstance.appendCloseRowButtonToSelectedContainer();
    expect(baseInstance.addMinHeightToSelectedElement).toHaveBeenCalledTimes(1);
  });

  test('RemoveMInheighttoAddElementButton', () => {
    baseInstance.removeMinHeightFromColumnAndContainer();
    expect(baseInstance.getDocument).toHaveBeenCalledTimes(2);
  });

  test('removeMinheight when hiding the add element button', () => {
    baseInstance.removeMinHeightFromColumnAndContainer = jest.fn();
    baseInstance.hideAddElementButtonToSelectedElement();
    expect(baseInstance.removeMinHeightFromColumnAndContainer).toHaveBeenCalledTimes(1);
  });

  test('removeMinheight when hiding the add element button to container', () => {
    baseInstance.removeMinHeightFromColumnAndContainer = jest.fn();
    baseInstance.hideAddRowButtonToSelectedContainer();
    expect(baseInstance.removeMinHeightFromColumnAndContainer).toHaveBeenCalledTimes(1);
  });
});

describe('#removeQuickLink', () => {
  let removeIcon: HTMLElement;
  beforeEach(() => {
    const doc = document.body;
    const wrapper = document.createElement('iframe');
    wrapper.id = CONSTANTS.LOREE_ANCHOR_LINK_WRAPPER;
    removeIcon = document.createElement('span');
    removeIcon.innerHTML = 'x';
    removeIcon.className = 'ml-2 float-right justify-content-end';
    removeIcon.id = CONSTANTS.LOREE_QUICK_LINK_REMOVE_URL;
    removeIcon.onclick = () => baseInstance.removeQuickLink(imgAnchorMockData());
    wrapper.appendChild(removeIcon);
    doc.appendChild(wrapper);
  });
  test('when remove the quick link url', () => {
    const removeAnchorLinkPopper = jest.spyOn(baseInstance, 'removeQuickLink');
    removeIcon.addEventListener('click', function (e: MouseEvent) {
      return e;
    });
    removeIcon.dispatchEvent(new Event('click'));
    expect(removeAnchorLinkPopper).toBeCalledTimes(1);
  });
});

describe('#Clear Editor', () => {
  beforeAll(() => {
    document.body.innerHTML = '';
    const domElement = document.createElement('div');
    ReactDOM.render(
      <div id='elementBar'>
        <div key='section' className='section' style={{ display: 'block' }} />
        <div
          key='addRow'
          id={CONSTANTS.LOREE_ADD_ROW_BUTTON_FOR_CONTAINER}
          style={{ display: 'block' }}
        />
        <div
          key='closeRow'
          id={CONSTANTS.LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER}
          style={{ display: 'block' }}
        />
        <div key='inserRow' id={CONSTANTS.LOREE_INSERT_ROW_BUTTON} style={{ display: 'block' }} />
        <div
          key='closeInsertRow'
          id={CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON}
          style={{ display: 'block' }}
        />
      </div>,
      domElement,
    );
    document.body.appendChild(domElement);
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => domElement);
    baseInstance.closeLanguageOption = jest.fn();
    baseInstance.showLanguageIsoLabel = jest.fn();
    baseInstance.hideLanguageIsoLabel = jest.fn();
    baseInstance.handleAfterDeleteContent();
  });

  test('should hide the sections in the element bar', () => {
    const element = document.getElementsByClassName('section')[0] as HTMLElement;
    expect(element.style.display).toBe('none');
  });

  test('should hide the add row button of an container', () => {
    const element = getEditorElementById(CONSTANTS.LOREE_ADD_ROW_BUTTON_FOR_CONTAINER);
    expect(element?.style.display).toBe('none');
  });

  test('should hide the add row cancel button of an container', () => {
    const element = getEditorElementById(CONSTANTS.LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER);
    expect(element?.style.display).toBe('none');
  });

  test('should hide the insert row button in between rows', () => {
    const element = getEditorElementById(CONSTANTS.LOREE_INSERT_ROW_BUTTON);
    expect(element?.style.display).toBe('none');
  });

  test('should hide the insert row cancel button in between rows', () => {
    const element = getEditorElementById(CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON);
    expect(element?.style.display).toBe('none');
  });
});

describe('#hideAnchorOption', () => {
  test('removed the function hideBlockOptionsToSelectedElements added in hideAnchorOption i', () => {
    baseInstance.hideBlockOptionsToSelectedElements = jest.fn();
    baseInstance.hideAnchorOption();
    expect(baseInstance.hideBlockOptionsToSelectedElements).toHaveBeenCalledTimes(0);
  });
});

describe('#sidebarElements', () => {
  let containerElement: _Any = null;
  const section: HTMLElement = document.createElement('div');
  section.className = CONSTANTS.LOREE_IFRAME_CONTENT_ROW;
  section.innerHTML = `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}><p>Insert Text Here</p></div>`;
  function tableElement() {
    const tdWrapper = document.createElement('td');
    tdWrapper.className = 'element-highlight';
    const pElement = document.createElement('p');
    pElement.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    tdWrapper.appendChild(pElement);
    return tdWrapper;
  }
  const containerDesignInstance = new ContainerDesign();
  beforeEach(() => {
    const iframe = document.createElement('iframe');
    baseInstance = new Base();
    const div = document.createElement('div');
    div.innerHTML = `
    <button class="sidebarElement" style=""><div id="loree-design-section-container-element" class="icon-block"></div><div class="label">Container</div></button>
    `;
    document.body.appendChild(div);
    const mathRandomSpy = jest.spyOn(baseInstance, 'getDocument');
    mathRandomSpy.mockImplementation(() => iFrameWrapperMockData(iframe).contentDocument);
    containerElement = getEditorElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_CONTAINER_ELEMENT,
    ) as HTMLButtonElement;
    baseInstance.changeSelectedElement(tableElement() as HTMLElement);
  });
  test('container element should be visible at initial', () => {
    expect(containerElement?.style.display).not.toBe('none');
  });
  test('container element is hidden when th / td is selected', () => {
    baseInstance.handleAddElementButtonClick();
    baseInstance.changeSelectedElement(tableElement() as HTMLElement);
    expect(containerElement?.parentElement?.style.display).toBe('none');
  });
  test('contaniner element is visible after clicking add element button', () => {
    baseInstance.handleAddElementButtonClick();
    baseInstance.changeSelectedElement(section);
    expect(containerElement?.style.display).not.toBe('none');
  });

  test('containerDesignBaseUtilites', () => {
    containerDesignInstance.attachLabel(document.body);
    const container = createDiv();
    container.innerHTML = `<div style="display:flex; margin:0px; padding: 5px;" class="loree-iframe-content-container-wrapper coloumn-minHeight element-highlight element-hover-highlight"><div class="loree-iframe-content-container" style="width:100%;"></div></div>`;
    containerDesignInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() => container.childNodes[0] as HTMLElement);
    document.body.innerHTML = `<div id=${CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER}></div>`;
    containerDesignInstance.initiate({});
    containerDesignInstance.attachContainerOptions();
    expect(
      getElementsByClassName(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS),
    ).toHaveLength(2);
    const containerBgButton = getElementById(
      CONSTANTS.LOREE_CONTAINER_BACKGROUND_COLOR_BUTTON,
    ) as HTMLButtonElement;
    mockColorPickerInstance(
      containerBgButton,
      `${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS} container-background-picker`,
      '#FFFFFF',
    );
    containerBgButton.click();
    baseInstance.hideContainerDesignSection();
    expect(
      getElementsByClassName(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS),
    ).toHaveLength(0);
  });
});

describe('Editor document content format', () => {
  let IframeObject;

  beforeEach(() => {
    IframeObject = new Iframe();
    document.body.innerHTML = '';

    document.execCommand = jest.fn();
    IframeObject.addIconClickIfPresent = jest.fn();

    render(
      <div id='loreeIframeElement'>
        <p key='paragraph' className='contentTest' contentEditable='true'>
          This is the first line content!.
        </p>
      </div>,
    );

    IframeObject.getDocument = jest.fn().mockImplementation(() => document);
    IframeObject.attachEventListeners();
  });

  test('restricting &nbsp; while entering', () => {
    const pTag = screen.queryAllByTestId('contentTest')[0];

    fireEvent.keyDown(pTag, {
      key: 'Enter',
      code: 'Enter',
      charCode: 13,
    });

    expect(document.execCommand).toHaveBeenCalled();
  });

  test('Should check the select previous style', () => {
    baseInstance.selectPreviousStyle = jest.fn().mockImplementation(() => selectionMockData);
    expect(selectionMockData.anchorNode?.parentElement?.style?.fontStyle).toBe('italic');
    expect(selectionMockData.anchorNode?.parentElement?.style?.fontWeight).toBe('bold');
    expect(selectionMockData.anchorNode?.parentElement?.style?.color).toBe('rgb(207, 63, 63)');
  });
});

describe('#show preview modal', () => {
  const baseInstance = new Base();
  beforeEach(() => {
    const parentDiv = createElement('div', CONSTANTS.LOREE_PREVIEW_MODAL_BACKGROUND);
    parentDiv.style.display = 'none';
    const wrapper = createElement('div', CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER);
    wrapper.style.display = 'none';
    const parentIframe = createElement('div', CONSTANTS.LOREE_PREVIEW_MODAL_CONTENT_BODY);
    const iframeElement = createElement('iframe') as HTMLIFrameElement;
    iframeElement.id = 'loree-preview';
    const previewContent = createElement('div', 'previewContent');
    previewContent.innerHTML = 'Hello';
    iframeElement.appendChild(previewContent);
    parentIframe.appendChild(iframeElement);
    wrapper.appendChild(parentIframe);
    parentDiv.appendChild(wrapper);
    document.body.append(parentDiv);
    const iframeDoc = iframeElement.contentWindow?.document;
    iframeDoc?.open().write(previewContent.outerHTML);
    iframeDoc?.close();
    baseInstance.showPreviewModal();
  });
  test('show preview modal background', () => {
    expect(getEditorElementById(CONSTANTS.LOREE_PREVIEW_MODAL_BACKGROUND)?.style.display).toEqual(
      'block',
    );
  });
  test('show preview modal wrapper', () => {
    expect(getEditorElementById(CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER)?.style.display).toEqual(
      'block',
    );
  });
  test('show preview modal iframe', () => {
    expect(getEditorElementById('loree-preview')?.innerHTML).toEqual(
      `<div id="previewContent">Hello</div>`,
    );
  });
  test('show preview modal iframe content', () => {
    expect(getEditorElementById('previewContent')?.innerHTML).toEqual('Hello');
  });
});

describe('#IsDeleteEventHandler', () => {
  beforeAll(() => {
    baseInstance = new Base();
    document.body.insertAdjacentHTML('beforeend', tableMockData);
    baseInstance.cleanUp = jest.fn();
    baseInstance.showElementSectionOptions = jest.fn();
  });
  test('if td is present after deleting the inner child element', () => {
    const element = document.getElementsByTagName('td')[0];
    baseInstance.changeSelectedElement(element.childNodes[0] as HTMLElement);
    baseInstance.isDelete();
    expect(element).toBeInTheDocument();
    expect(element.innerHTML).toBe('');
  });
  test('if th is present after deleting the inner child element', () => {
    const element = document.getElementsByTagName('th')[0];
    baseInstance.changeSelectedElement(element.childNodes[0] as HTMLElement);
    baseInstance.isDelete();
    expect(element).toBeInTheDocument();
    expect(element.innerHTML).toBe('');
  });
});

describe('#disableNewTabLinkForD2L', () => {
  let checkBox: HTMLInputElement;
  beforeEach(() => {
    checkBox = document.createElement('input');
    checkBox.id = 'link-option-tab-checkbox';
    baseInstance = new Base();
  });

  test('when lms is not d2l', () => {
    sessionStorage.setItem('domainName', 'canvas');
    baseInstance.disableNewTabLinkForD2L(checkBox);
    expect(checkBox.disabled).toBe(false);
    expect(checkBox.checked).toBe(false);
  });

  test('when lms is d2l', () => {
    sessionStorage.setItem('domainName', 'D2l');
    baseInstance.disableNewTabLinkForD2L(checkBox);
    expect(checkBox.disabled).toBe(true);
    expect(checkBox.checked).toBe(true);
  });
});

describe('color picker events', () => {
  let rowColBorderColorPicker: HTMLElement;
  let rowColBgColorPicker: HTMLElement;
  let borderInput: HTMLInputElement;
  let borderStyle: HTMLSelectElement;
  let borderInputColor: HTMLInputElement;
  let bgInputColor: HTMLInputElement;
  let applyButton: HTMLInputElement;
  let cancelButton: HTMLInputElement;
  let clearButton: HTMLInputElement;
  beforeEach(() => {
    jest.clearAllMocks();
    createAndAppendIframeToDOM();
    const doc = document.body;
    const parentWrapper = document.createElement('div');
    parentWrapper.id = CONSTANTS.LOREE_IFRAME;
    parentWrapper.innerHTML = colorPickerMockData;
    parentWrapper.innerHTML += baseMockData.cleanedHtmlDivTagContent;
    doc.append(parentWrapper);
    baseInstance = new Base();
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
    rowColBorderColorPicker = document.getElementById(
      CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
    ) as HTMLElement;
    borderInput = rowColBorderColorPicker?.getElementsByClassName(
      'rowColumnBorderInput',
    )[0] as HTMLInputElement;
    borderStyle = rowColBorderColorPicker?.getElementsByClassName(
      'rowColumnBorderSelect',
    )[0] as HTMLSelectElement;
    borderInputColor = rowColBorderColorPicker?.getElementsByClassName(
      'pcr-result',
    )[0] as HTMLInputElement;
    applyButton = rowColBorderColorPicker?.getElementsByClassName(
      'pcr-save',
    )[0] as HTMLInputElement;
    cancelButton = rowColBorderColorPicker?.getElementsByClassName(
      'pcr-cancel',
    )[0] as HTMLInputElement;
    clearButton = rowColBorderColorPicker?.getElementsByClassName(
      'pcr-clear',
    )[0] as HTMLInputElement;
  });
  test('click on border clear should clear input and userBorderColor variable must be empty', () => {
    baseInstance.customiseTextBorderColorPicker();
    const pcrBorderPicker = document.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
    );
    pcrBorderPicker?.childNodes[4]?.dispatchEvent(new Event('click'));
    expect((pcrBorderPicker?.firstChild as HTMLInputElement)?.value).toBe('#FFFFFF');
    expect(baseInstance.userBorderColor()).toBe('#FFFFFF');
    expect(baseInstance.userBorderWidth()).toBe('0');
    expect(baseInstance.userBorderStyle()).toBe('Solid');
  });
  test('row border color events', () => {
    rowColBorderColorPicker?.classList.add('row-panel-active');
    baseInstance.customiseRowColumnBorderColorPicker();
    borderInput.value = '10';
    borderStyle.selectedIndex = 1;
    borderInputColor.value = '#D5D5D5';
    applyButton.click();
    expect(baseInstance.userRowBorderColor()).toBe('#D5D5D5');
    expect(baseInstance.userRowBorderWidth()).toBe('10');
  });
  test('column values stays with default value', () => {
    expect(baseInstance.userColumnBorderColor()).toBe('#FFFFFF');
    expect(baseInstance.userColumnBorderWidth()).toBe('0');
  });
  test('cancel action on row border color picker', () => {
    borderInput.value = '';
    const previousStyle = baseInstance.getUserRowBorderStyle();
    cancelButton.click();
    expect(borderStyle.selectedIndex).toEqual(
      utils.selectedIndexForBorderStyle(borderStyle, previousStyle),
    );
  });
  test('clear action to reset all row border values', () => {
    clearButton.click();
    expect(baseInstance.userRowBorderColor()).toBe('#FFFFFF');
    expect(baseInstance.userRowBorderWidth()).toBe('0');
    expect(baseInstance.getUserRowBorderStyle()).toBe('solid');
  });
  test('column border color events', () => {
    rowColBorderColorPicker.classList.remove('row-panel-active');
    baseInstance.customiseRowColumnBorderColorPicker();
    borderInput.value = '20';
    borderStyle.selectedIndex = 2;
    borderInputColor.value = '#65229';
    applyButton.click();
    expect(baseInstance.userColumnBorderColor()).toBe('#65229');
    expect(baseInstance.userColumnBorderWidth()).toBe('20');
  });
  test('row values stays with default value', () => {
    expect(baseInstance.userRowBorderColor()).toBe('#FFFFFF');
    expect(baseInstance.userRowBorderWidth()).toBe('0');
  });
  test('clear action to reset all col border values', () => {
    clearButton.click();
    expect(baseInstance.userColumnBorderColor()).toBe('#FFFFFF');
    expect(baseInstance.userColumnBorderWidth()).toBe('0');
  });
  test('update row border style', () => {
    expect(baseInstance.getUserRowBorderStyle()).toBe('solid');
    baseInstance.updateRowBorderStyle('double');
    expect(baseInstance.getUserRowBorderStyle()).toBe('double');
  });
  test('default column style functions', () => {
    expect(baseInstance.userColumnBorderColor()).toBe('#FFFFFF');
    expect(baseInstance.getColumnBorderStyle()).toBe('solid');
    expect(baseInstance.userColumnBorderWidth()).toBe('0');
  });
  test('update of column styles', () => {
    baseInstance.updateUserColumnBorderColor('#FF0000');
    baseInstance.updateColumnBorderStyle('Dashed');
    baseInstance.updateUserColumnBorderWidth('7');
    expect(baseInstance.userColumnBorderColor()).toBe('#FF0000');
    expect(baseInstance.getColumnBorderStyle()).toBe('Dashed');
    expect(baseInstance.userColumnBorderWidth()).toBe('7');
  });
  describe('rowColBgColorPicker', () => {
    beforeEach(() => {
      rowColBgColorPicker = getElementById(
        CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER,
      );
      bgInputColor = rowColBgColorPicker?.getElementsByClassName(
        'pcr-result',
      )[0] as HTMLInputElement;
      applyButton = rowColBgColorPicker?.getElementsByClassName('pcr-save')[0] as HTMLInputElement;
      cancelButton = rowColBgColorPicker?.getElementsByClassName(
        'pcr-cancel',
      )[0] as HTMLInputElement;
      clearButton = rowColBgColorPicker?.getElementsByClassName('pcr-clear')[0] as HTMLInputElement;
      baseInstance.closeLanguageOption = jest.fn();
      baseInstance.showLanguageIsoLabel = jest.fn();
      baseInstance.hideLanguageIsoLabel = jest.fn();
    });
    test('initial layout block options status', () => {
      expect(baseInstance.getSelectedBlockOptionMenu()).toBe(false);
    });
    test('row col bg default color before change', () => {
      expect(baseInstance.userRowBackGroundColor()).toBe('#FFFFFF');
      expect(baseInstance.userColumnBackGroundColor()).toBe('#FFFFFF');
    });
    test('getSelectedElement status if row is selected', () => {
      baseInstance.changeSelectedElement(
        getNElementByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_ROW),
      );
      expect(baseInstance.getSelectedBlockOptionMenu()).toBe(true);
    });
    test('getSelectedElement status if column is selected', () => {
      baseInstance.changeSelectedElement(
        getNElementByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN),
      );
      expect(baseInstance.getSelectedBlockOptionMenu()).not.toBe(true);
    });
    test('row background color events', () => {
      rowColBorderColorPicker?.classList.add('row-panel-active');
      baseInstance.customiseRowColumnBackGroundColorPicker();
      bgInputColor.value = '#D5D5D5';
      applyButton.click();
      expect(baseInstance.userRowBackGroundColor()).toBe('#D5D5D5');
    });
    test('col background color events, should have no conflicts with row', () => {
      rowColBorderColorPicker?.classList.remove('row-panel-active');
      baseInstance.customiseRowColumnBackGroundColorPicker();
      bgInputColor.value = '#FF0000';
      applyButton.click();
      expect(baseInstance.userColumnBackGroundColor()).toBe('#FF0000');
    });
    test('cancel event should have the previous color of respective element', () => {
      bgInputColor.value = '#A103FC';
      cancelButton.click();
      expect(baseInstance.userColumnBackGroundColor()).not.toBe('#A103FC');
      expect(baseInstance.userColumnBackGroundColor()).toBe('#FF0000');
      rowColBorderColorPicker?.classList.remove('row-panel-active');
      baseInstance.customiseRowColumnBackGroundColorPicker();
      bgInputColor.value = '#FCBA03';
      cancelButton.click();
      expect(baseInstance.userRowBackGroundColor()).toBe('#D5D5D5');
    });
    test('click on background clear should clear input and userBgColor variable must be empty', () => {
      baseInstance.customiseRowColumnBackGroundColorPicker();
      rowColBgColorPicker?.childNodes[4]?.dispatchEvent(new Event('click'));
      expect((rowColBgColorPicker?.firstChild as HTMLInputElement)?.value).toBe('#FFFFFF');
      expect(baseInstance.userBackgroundColor()).toBe('');
    });
  });
});

describe('multiple font properties', () => {
  baseInstance = new Base();
  test('paragraph -> applied font families and font sizes should be returned', () => {
    const pTag = createElement('p');
    pTag.style.fontFamily = 'ABeeZee';
    pTag.style.fontSize = '16px';
    pTag.innerHTML = `<span class="loree-iframe-content-element" contenteditable="true" style="font-family: &quot;Almendra SC&quot;;">Insert</span> <span class="loree-iframe-content-element" contenteditable="true" style="font-family: &quot;Alex Brush&quot;; font-size: 23px;">text</span> <span class="loree-iframe-content-element element-highlight" contenteditable="true" style="font-family: &quot;Dancing Script&quot;;">here</span>`;
    expect(baseInstance.checkMultiSelectFontStyles(Array.from(pTag.childNodes), [], [])).toEqual({
      isMultiFontApplied: true,
      font: [
        ['"Almendra SC"', '"Alex Brush"', '"Dancing Script"'],
        ['', '23px'],
      ],
    });
  });
  test('entire list -> applied font families and font sizes should be returned', () => {
    const orderedList = createElement('ol');
    orderedList.style.fontFamily = 'ABeeZee';
    orderedList.style.fontSize = '20px';
    orderedList.innerHTML = `<li class="loree-iframe-content-element element-highlight" style="padding: 5px; margin: 0px 0px 10px;">Insert text here</li><li class="loree-iframe-content-element" style="padding: 5px; margin: 0px 0px 10px;">Insert <span class="loree-iframe-content-element element-highlight" contenteditable="true" style="font-family: &quot;Dancing Script&quot;;">text</span> <span class="loree-iframe-content-element" contenteditable="true" style="font-size: 16px;">here</span><br></li><li class="loree-iframe-content-element" style="padding: 5px; margin: 0px 0px 10px;"><span class="loree-iframe-content-element" contenteditable="true" style="font-family: &quot;Arial Regular&quot;;">Insert</span> text here<br></li>`;
    expect(
      baseInstance.checkMultiSelectFontStyles(Array.from(orderedList.childNodes), [], []),
    ).toEqual({
      isMultiFontApplied: true,
      font: [
        ['', '"Dancing Script"', '"Arial Regular"'],
        ['', '16px'],
      ],
    });
  });
  test('single list item -> applied font families and font sizes should be returned', () => {
    const listItem = createElement('li');
    listItem.innerHTML = `Insert <span class="loree-iframe-content-element element-highlight" contenteditable="true" style="font-family: &quot;Dancing Script&quot;;">text</span> <span class="loree-iframe-content-element element-highlight" contenteditable="true" style="font-size: 18px;">here</span><br>`;
    expect(
      baseInstance.checkMultiSelectFontStyles(Array.from(listItem.childNodes), [], []),
    ).toEqual({
      isMultiFontApplied: true,
      font: [
        ['', '"Dancing Script"'],
        ['', '18px'],
      ],
    });
  });
  test('text with link ->  applied font families and font sizes should be returned', () => {
    const textWithLink = createElement('p');
    textWithLink.style.fontFamily = 'ABeeZee';
    textWithLink.style.fontSize = '20px';
    textWithLink.innerHTML = `Insert <a href="https://crystaldelta.atlassian.net/secure/RapidBoard.jspa?rapidView=57&amp;projectKey=LOREE&amp;modal=detail&amp;selectedIssue=LOREE-1531" title="https://crystaldelta.atlassian.net/secure/RapidBoard.jspa?rapidView=57&amp;projectKey=LOREE&amp;modal=detail&amp;selectedIssue=LOREE-1531" class="loree-iframe-content-element element-highlight" style="color: rgb(176, 26, 139); text-decoration: underline dotted; font-family: &quot;Dancing Script&quot;; font-style: italic;" target="blank"><span class="loree-iframe-content-element" contenteditable="true" style="font-size: 16px;">text</span></a> <span class="loree-iframe-content-element element-highlight" contenteditable="true" style="font-family: Amiko;">here</span>`;
    expect(
      baseInstance.checkMultiSelectFontStyles(Array.from(textWithLink.childNodes), [], []),
    ).toEqual({
      isMultiFontApplied: true,
      font: [
        ['ABeeZee', '"Dancing Script"', '', 'Amiko'],
        ['20px', '', '16px'],
      ],
    });
  });
});

describe('#updateSpecialElement', () => {
  let baseInstance: Base;
  function iFrameWrapper() {
    const iframe = document.createElement('iframe');
    iframe.id = CONSTANTS.LOREE_IFRAME;
    document.body.appendChild(iframe);
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(wrapper.outerHTML);
    iframeDoc?.close();
    return iframe.contentDocument as Document;
  }

  test('width should be reset when copy pasted', () => {
    baseInstance = new Base();
    baseInstance.getDocument = jest.fn().mockImplementation(() => iFrameWrapper());
    const imageElement = createElement(
      'img',
      '',
      CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE + ' ' + CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT,
    );
    imageElement.style.width = '150px';
    appendElementToBody(imageElement);
    baseInstance.updateSpecialElementProperty(document.body);
    const specialElementImages = getElementsByTagName('IMG')[0] as HTMLElement;
    expect(specialElementImages.style.width).toBe('');
  });
});

describe('image selction', () => {
  let baseInstance: Base;
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    const topButton = createDiv(CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON);
    const downButton = createDiv(CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON);
    iframe.appendChild(topButton);
    iframe.appendChild(downButton);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  beforeEach(() => {
    jest.clearAllMocks();
    document.body.innerHTML = '';
    baseInstance = new Base();
    loreeWrapper();
  });
  test('image with link selection', () => {
    const wholeDiv = createDiv('', 'whole div');
    wholeDiv.innerHTML = `<div></div><a><div class = ${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}><img></div></a><div></div>`;
    document.body.append(wholeDiv);
    baseInstance.changeSelectedElement(
      document.getElementsByClassName(
        CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER,
      )[0] as HTMLElement,
    );
    baseInstance.checkBlockOptions(false, false, true);
    const iframeDocument = baseInstance.getDocument();
    const topButtonDisplay = iframeDocument?.getElementById(
      `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON}`,
    );
    const downButtonDisplay = iframeDocument?.getElementById(
      `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON}`,
    );
    expect(topButtonDisplay?.style.display).toBe('inline-flex');
    expect(downButtonDisplay?.style.display).toBe('inline-flex');
  });
  test('image with text preset link selection arrow keys', () => {
    const wholeDiv = createDiv('', 'whole div');
    wholeDiv.innerHTML = `<div></div><div><div class = ${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT}><img></div></div><div></div>`;
    document.body.append(wholeDiv);
    baseInstance.changeSelectedElement(
      getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)[0] as HTMLElement,
    );
    baseInstance.checkBlockOptions(false, false, true);
    const iframeDocument = baseInstance.getDocument();
    const topButtonDisplay = iframeDocument?.getElementById(
      `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON}`,
    );
    const downButtonDisplay = iframeDocument?.getElementById(
      `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON}`,
    );
    expect(topButtonDisplay?.style.display).toBe('inline-flex');
    expect(downButtonDisplay?.style.display).toBe('inline-flex');
  });
});
describe('on click, hideAnchorOption function should be called', () => {
  const pElement = createElement('p');
  baseInstance = new Base();

  beforeEach(() => {
    jest.clearAllMocks();
    document.body.innerHTML = Bootstrap;
  });

  test('when row hamburger icon is clicked', () => {
    baseInstance.showHamburgerIconForRow(pElement);
    const spysetForhideAnchorOption = jest.spyOn(baseInstance, 'hideAnchorOption');
    const rowHamburgerIcon = getElementById(CONSTANTS.LOREE_ROW_HAMBURGER_ICON);
    rowHamburgerIcon.click();
    expect(spysetForhideAnchorOption).toBeCalledTimes(1);
  });

  test('when column hamburger icon is clicked', () => {
    baseInstance.showHamburgerIconForColumn(pElement);
    const spysetForhideAnchorOption = jest.spyOn(baseInstance, 'hideAnchorOption');
    const columnHamburgerIcon = getElementById(CONSTANTS.LOREE_COLUMN_HAMBURGER_ICON);
    columnHamburgerIcon.click();
    expect(spysetForhideAnchorOption).toBeCalledTimes(1);
  });

  test('when table hamburger icon is clicked', () => {
    baseInstance.showHamburgerIconForTable(pElement);
    const spysetForhideAnchorOption = jest.spyOn(baseInstance, 'hideAnchorOption');
    const tableHamburgerIcon = getElementById(CONSTANTS.LOREE_TABLE_HAMBURGER_ICON);
    tableHamburgerIcon.click();
    expect(spysetForhideAnchorOption).toBeCalledTimes(1);
  });

  test('when menu hamburger icon is clicked', () => {
    baseInstance.showHamburgerIconForMenu(pElement);
    const spysetForhideAnchorOption = jest.spyOn(baseInstance, 'hideAnchorOption');
    const navigationHamburgerIcon = getElementById(CONSTANTS.LOREE_NAVIGATE_HAMBURGER_ICON);
    navigationHamburgerIcon.click();
    expect(spysetForhideAnchorOption).toBeCalledTimes(1);
  });
});

describe('Add row button', () => {
  document.body.innerHTML = Bootstrap;
  const rowInstance = new Row();

  test('click on add row button', () => {
    document.body.innerHTML += `<div id='loree-sidebar-content-wrapper'></div>`;
    rowInstance.initiate(templateConfig);
    const addRowButton = getElementById(CONSTANTS.LOREE_ADD_ROW_BUTTON);
    expect(getElementById('loree-sidebar-row-section').style.display).toEqual('none');
    addRowButton.click();
    baseInstance.handleAddRowButtonClick();
    expect(getElementById('loree-sidebar-row-section').style.display).toEqual('block');
  });
  test('click on 2 column row should open subsidebar', () => {
    document.body.innerHTML += `<div id='loree-sub-sidebar'></div>`;
    getElementById('twoColumns').click();
    rowInstance.handleMultipleRowItems(RowSet[2]);
    expect(getElementById('loree-sub-sidebar').style.display).toEqual('flex');
  });
  test('click on 3 column row should change subsidebar content', () => {
    getElementById('twoColumns').click();
    rowInstance.handleMultipleRowItems(RowSet[3]);
    const subSidebarRowsWrapper = getEditorElementsByClassName(
      'subSidebarRowsWrapper',
    )[0] as HTMLElement;
    expect(subSidebarRowsWrapper.childNodes.length).toEqual(8);
  });
  test('click on close row button should disable row sidebar and hide subsidebar', () => {
    const closeRowButton = getElementById(CONSTANTS.LOREE_CLOSE_ROW_BUTTON);
    closeRowButton.click();
    baseInstance.handleCloseRowButton();
    expect(getElementById('loree-sidebar-row-section-button')).toBeDisabled();
    expect(getElementById('loree-sub-sidebar').style.display).toEqual('none');
  });
  test('check elment hide disable or not in add element button clicking time', () => {
    const addElementButton = getElementById(
      CONSTANTS.LOREE_ADD_ELEMENT_BUTTON,
    ) as HTMLButtonElement;
    addElementButton.click();
    baseInstance.handleCloseAddElementButtonClick();
    expect(getElementById('loree-add-element-button').style.display).toEqual('none');
  });
});
describe('interactive selction', () => {
  let baseInstance: Base;
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    const topButton = createDiv(CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON);
    iframe.appendChild(topButton);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  beforeEach(() => {
    jest.clearAllMocks();
    document.body.innerHTML = '';
    baseInstance = new Base();
    loreeWrapper();
  });
  test('interactive with edit option when selection', () => {
    const wrapper = createDiv('', 'wrapper');
    wrapper.innerHTML = `${interactiveElementMockData}`;
    document.body.append(wrapper);
    baseInstance.changeSelectedElement(
      getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER)[0] as HTMLElement,
    );
    baseInstance.checkBlockOptions(false, false, true);
    const iframeDocument = baseInstance.getDocument();
    const downButtonDisplay = iframeDocument?.getElementById(
      `${CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON}`,
    );
    expect(downButtonDisplay?.style.display).toBe('inline-flex');
  });
});

describe('#external tool element', () => {
  let wrapper: HTMLElement;
  const baseInstance = new Base();
  beforeEach(() => {
    wrapper = createDiv();
    wrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    document.body.appendChild(wrapper);
    const selectedWrapper = getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER)[0];
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => selectedWrapper);
  });

  test('verify whether external tool iframe is appended', () => {
    baseInstance.appendElementToSelectedColumn(externalToolElementMockData);
    expect(
      getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER)[0],
    ).toBeInTheDocument();
  });
});

describe('#SaveCustomBlocks', () => {
  const baseInstance = new Base();
  const customBlocks = [
    { element: selectedRowElement, elementType: 'Row', title: 'Save Custom Row' },
    { element: selectedColumnElement, elementType: 'Element', title: 'Save Custom Element' },
  ];
  beforeEach(() => {
    const loreeWrapper = createDiv('div');
    loreeWrapper.id = CONSTANTS.LOREE_WRAPPER;
    loreeWrapper.append(modalContainer());
    document.body.appendChild(loreeWrapper);
  });

  test('should be undefined when no element is selected', () => {
    baseInstance.changeSelectedElement(null);
    expect(baseInstance.isSave()).toBeUndefined();
  });

  test.each(customBlocks)(
    'should render the modal when save CustomBlock Icon is triggered',
    ({ element, elementType, title }) => {
      const selectedElement = createDiv('div');
      selectedElement.innerHTML = element;
      baseInstance.changeSelectedElement(selectedElement.firstChild as HTMLDivElement);
      customBlockInfoModal(elementType, selectedElement.firstChild as HTMLDivElement);
      baseInstance.isSave();
      expect(getElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_TITLE).innerHTML).toBe(title);
    },
  );
});

describe('#check selected element on custom block edit mode', () => {
  let baseInstance: Base;
  beforeEach(() => {
    document.body.innerHTML = '';
    baseInstance = new Base();
    loreeWrapper();
    setEditMode(true);
  });
  test('selected element must be loree content wrapper on initial', () => {
    isContainer.active = false;
    baseInstance.getDocument = jest
      .fn()
      .mockImplementation(
        () =>
          (document.body.getElementsByClassName('loreeIframe')[0] as HTMLIFrameElement)
            .contentDocument,
      );
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() =>
        baseInstance.getDocument()?.getElementById('loree-iframe-content-wrapper'),
      );
    baseInstance.appendContentToIframeContentWrapper(emptyRowColumnElement);
    expect(baseInstance.getSelectedElement()?.outerHTML).toEqual(loreeContentWrapperElement);
  });
  test('when selected element is container it must be changed to loree content wrapper', () => {
    isContainer.active = true;
    baseInstance.getDocument = jest
      .fn()
      .mockImplementation(
        () =>
          (document.body.getElementsByClassName('loreeIframe')[0] as HTMLIFrameElement)
            .contentDocument,
      );
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() =>
        baseInstance.getDocument()?.getElementById('loree-iframe-content-wrapper'),
      );
    baseInstance.appendContentToIframeContentWrapper(emptyRowColumnElement);
    const column = baseInstance
      .getDocument()
      ?.getElementsByClassName('loree-iframe-content-column')[0] as HTMLElement;
    column.innerHTML = emptyContainerElement;
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(
        () =>
          baseInstance
            .getDocument()
            ?.getElementsByClassName('loree-iframe-content-container-wrapper')[0],
      );
    baseInstance.appendContentToIframeContentWrapper(emptyRowColumnElement);
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() =>
        baseInstance.getDocument()?.getElementById('loree-iframe-content-wrapper'),
      );
    expect(baseInstance.getSelectedElement()?.outerHTML).toEqual(containerWrappedWithLoreeWrapper);
  });
});

describe('#hideItemsInSidebarForTable', () => {
  let baseInstance: Base;

  beforeEach(() => {
    baseInstance = new Base();
    jest.clearAllMocks();
  });

  test.each([
    CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_ELEMENT,
    CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT,
    CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT,
    CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT,
    CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT,
  ])('verify whether sidebar elements are not displayed', (id) => {
    document.body.innerHTML = sidebarElement(id);
    baseInstance.hideItemsInSidebarForTable();
    const sideBarElement = getElementById(id)?.parentElement;
    expect(sideBarElement?.style.display).toBe('none');
  });
});

describe('#showItemsInSidebarHiddenForTable', () => {
  let baseInstance: Base;

  beforeEach(() => {
    baseInstance = new Base();
    jest.clearAllMocks();
  });

  test.each([
    CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_ELEMENT,
    CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT,
    CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT,
    CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT,
    CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT,
  ])('verify whether sidebar elements are not displayed', (id) => {
    document.body.innerHTML = sidebarElement(id);
    baseInstance.showItemsInSidebarHiddenForTable();
    const sideBarElement = getElementById(id)?.parentElement;
    expect(sideBarElement?.style.display).toBe('inline-flex');
  });
});

describe('check the edit status of the customBlock', () => {
  let iframe;
  beforeEach(() => {
    iframe = createElement('iframe') as HTMLIFrameElement;
    iFrameWrapperMockData(iframe);
  });

  test('selected row or element is not in the edit state', () => {
    expect(isSelectionFirstElementInContent()).toBeFalsy();
  });

  test('selected row or element is in the edit state', () => {
    const element = getIFrameElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    baseInstance.changeSelectedElement(element.firstChild as HTMLElement);
    expect(isSelectionFirstElementInContent()).toBeTruthy();
  });
});
