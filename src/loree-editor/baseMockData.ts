import { createDiv } from './common/dom';
import CONSTANTS from './constant';
import Bootstrap from './modules/iframe/templates/bootstrap';
import { appendElementToBody } from './utils';

export const baseMockData = {
  htmlPTagContent: `<link rel="stylesheet" href="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/227805/canvasThemeUpdated.css">
            <p><iframe id="iqfvl" src="https://player.vimeo.com/video/74966156?title=0&amp;byline=0&amp;portrait=0" width="640" height="360" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" data-mce-fragment="1" class="" style="width: 90%; margin: 0 0 0 50px;"></iframe></p><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/154568/Loree_js.js"></script>`,

  htmlDivTagContent: `<link rel="stylesheet" href="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/227805/canvasThemeUpdated.css">
            <div>Text</div><script src="https://instructure-uploads.s3.amazonaws.com/account_123670000000000001/attachments/154568/Loree_js.js"></script>`,

  singlRowColPTagContent: `
            <p style="padding: 5px;" class="loree-iframe-content-element"><div class="loree-iframe-content-video-wrapper" style="padding: 5px;margin:0 0 10px;"><div class="loree-iframe-video-content" style="position:relative;width: 90%;display: inline-flex;max-width: 100%;max-height: 100%;"><div style="padding: 42% 0px 0px;display: flex;"><iframe id="iqfvl" src="https://player.vimeo.com/video/74966156?title=0&amp;byline=0&amp;portrait=0" width="640" height="360" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" data-mce-fragment="1" class="loree-no-pointer" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; max-width: 100%;"></iframe></div></div></div></p>`,

  cleanedHtmlPTagContent: `
            <div class="row loree-iframe-content-row" style="position:relative; margin: 0px; padding: 10px"><div class="col-12 loree-iframe-content-column" style="padding: 10px"><div class="loree-iframe-content-video-wrapper" style="padding: 5px;margin:0 0 10px;"><div class="loree-iframe-video-content" style="position:relative;width: 90%;display: inline-flex;max-width: 100%;max-height: 100%;"><div style="padding: 42% 0px 0px;display: flex;"><iframe id="iqfvl" src="https://player.vimeo.com/video/74966156?title=0&amp;byline=0&amp;portrait=0" width="640" height="360" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" data-mce-fragment="1" class="loree-no-pointer" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; max-width: 100%;"></iframe></div></div></div></div></div>`,

  singlRowColDivTagContent: `
            <div><p style="margin: 0px; padding: 5px;" class="loree-iframe-content-element">Text</p></div>`,

  cleanedHtmlDivTagContent: `
            <div class="row loree-iframe-content-row" style="position:relative; margin: 0px; padding: 10px"><div class="col-12 loree-iframe-content-column" style="padding: 10px"><div><p style="margin: 0px; padding: 5px;" class="loree-iframe-content-element">Text</p></div></div></div>`,
};

export const selectedElement = `<div style="display:flex; margin:0px; padding: 5px;" class="loree-iframe-content-container-wrapper element-highlight"><div class="loree-iframe-content-container" style="width:100%;"></div></div>`;

export const imgAnchorMockData = () => {
  const doc1 = document.body;
  const parentWrapper = document.createElement('div');
  parentWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN;
  doc1.appendChild(parentWrapper);
  const anchorTag = document.createElement('a');
  anchorTag.href = '';
  parentWrapper.appendChild(anchorTag);
  const divTag = document.createElement('div');
  divTag.id = 'test-wrapper';
  divTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
  anchorTag.appendChild(divTag);
  const imgTag = document.createElement('img');
  imgTag.src = '';
  imgTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE;
  divTag.appendChild(imgTag);
  parentWrapper.appendChild(divTag);
  return parentWrapper;
};

export const iFrameWrapperMockData = (iframe: HTMLIFrameElement) => {
  iframe.id = CONSTANTS.LOREE_IFRAME;
  document.body.appendChild(iframe);
  const wrapper = document.createElement('div');
  wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
  wrapper.innerHTML = `<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column coloumn-minHeight column-highlight element-highlight" style="padding: 10px; background: #d39595;"><h3 class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: &quot;Dancing Script&quot;; font-size: 46px;">Header</h3><p class="loree-iframe-content-element" style="border-width: 5px; border-style: dashed; border-color: #d917df; color: #000000; font-family: ABeeZee; font-size: 20px; padding: 5px; margin: 0px 0px 10px; background-color: #ffffff;">Insert <a href="https://www.google.com/" title="https://www.google.com/" class="loree-iframe-content-element" style="color: #b01a8b; text-decoration: overline dotted; font-family: Arial; font-style: italic;" target="blank" onclick="event.preventDefault()"><span class="loree-iframe-content-element bStyle " style=" font-style: italic;">text</span></a> here</p></div></div>`;
  wrapper.innerHTML += Bootstrap;
  const iframeDoc = iframe.contentWindow?.document;
  iframeDoc?.open().write(wrapper.outerHTML);
  iframeDoc?.close();
  return iframe;
};

export const createAndAppendIframeToDOM = () => {
  const iframeElement = document.createElement('iframe');
  iframeElement.id = CONSTANTS.LOREE_IFRAME;
  document.body.append(iframeElement);
};

export const textMockData = () => {
  const parentSapmpleDiv = document.createElement('div');
  parentSapmpleDiv.className = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS;
  const sampleDiv = document.createElement('p');
  sampleDiv.id = 'dummyContent';
  sampleDiv.className = 'dummyContent';
  sampleDiv.innerHTML = 'Lorem Ipsum dolor sit Amet';
  parentSapmpleDiv.append(sampleDiv);
  document.body.append(parentSapmpleDiv);
};

export const selectionMockData = {
  anchorNode: {
    nodeName: '#text',
    nextSibling: null,
    firstChild: null,
    parentElement: {
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      innerText: 'Inner',
      tagName: 'p',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        color: 'rgb(207, 63, 63)',
      },
    } as HTMLElement,
    parentNode: {
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      innerText: 'Inner',
      tagName: 'div',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        size: 66,
        color: 'rgb(207, 63, 63)',
      },
    } as unknown as Node,
    getAttribute: {},
  } as HTMLElement,
  type: 'Range',
} as unknown as Selection;

export const tableMockData =
  '<table class="loree-iframe-content-table" style="width:100%; height:auto; border: 1px solid #A5A5A5;border-collapse: collapse;"><tbody><tr style="border: 1px solid #A5A5A5;"><th style="border: 1px solid #A5A5A5;padding: 5px;"><h3 class="loree-iframe-content-element element-highlight" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: &quot;Source Sans Pro&quot;; font-size: 36px;" contenteditable="true"> Header</h3></th><th style="border: 1px solid #A5A5A5;padding: 5px;"><h3 class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: &quot;Source Sans Pro&quot;; font-size: 36px;"> Header</h3></th></tr><tr style="border: 1px solid #A5A5A5;"><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: &quot;Source Sans Pro&quot;; font-size: 16px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: &quot;Source Sans Pro&quot;; font-size: 16px;"> Insert text here</p></td></tr></tbody></table>';
export const colorPickerMockData = `
<div id=${CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER}></div>
<div id=${CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER}><input class='custom-hex-color-input' value='#FFFFFF'></input><input class="pcr-save" value="Apply" type="button" aria-label="save and close">
<input class="pcr-cancel" value="Cancel" type="button" aria-label="cancel and close"><button class='pcr-clear'>clear</button></div><div id=${CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER}><input class='custom-hex-color-input' value='#EC22D9'></input><input class="pcr-save" value="Apply" type="button" aria-label="save and close">
<input class="pcr-cancel" value="Cancel" type="button" aria-label="cancel and close"><button class='pcr-clear'>clear</button></div><div id=${CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER}><input class='pcr-result' value='#FFFFFF'></input><input class="pcr-save" value="Apply" type="button" aria-label="save and close">
<input class="pcr-cancel" value="Cancel" type="button" aria-label="cancel and close"><button class='pcr-clear'>clear</button></div><div id=${CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER}><div class="rowColumnBorderOptionWrapper"><div class="rowColumnBorderLabel">Width</div><input class="rowColumnBorderInput" name="rowColumnBorderInput" id="loree-row-column-border-width-color-picker" type="number" autocomplete="off"><select class="rowColumnBorderSelect" name="rowColumnBorderSelect" id="loree-row-column-border-style-color-picker">
<option value="solid">Solid</option><option value="dashed">Dashed</option><option value="dotted">Dotted</option><option value="double">Double</option></select></div><input class='pcr-result' value='#FF0000'></input><input class="pcr-save" value="Apply" type="button" aria-label="save and close"><input class="pcr-cancel" value="Cancel" type="button" aria-label="cancel and close"><button class='pcr-clear'>clear</button></div>`;

export const imageWithText = `<div style="display:  flex; align-items: center;position: relative;margin:0 0 10px;padding:5px;" class="loree-iframe-content-image-text element-highlight element-hover-highlight"><img alt="" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" id="ImageWithText" src="https://crystaldelta.instructure.com/files/373414/download?download_frd=1&amp;verifier=xeJ7t2KUv97PW96QcmXRSXgHJL69hRVv4l5IP0Zk"><div style="position: absolute;color: #000000;padding: 15px;max-width:80%;top: 35%;" class=""><h4 class="loree-iframe-content-element" style="color: rgb(0, 0, 0); border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); padding: 5px; margin: 0px 0px 10px; font-family: Helvetica; font-size: 34px;">Header</h4><p class="loree-iframe-content-element" style="color: rgb(0, 0, 0); border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); padding: 5px; margin: 0px 0px 10px; font-family: ABeeZee; font-size: 20px;">Insert text wrapper</p></div></div>`;

export const interactiveElementMockData = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER} loree-style-062306"><iframe class="${CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT} loree-style-d30cbf" title="SHAN_ACCORDION_20" id="Accordion_c0ad1eed5c789e58653bbe9752a40c36" frameborder="0" width="100%" height="400" scrolling="no" src="https://stg.loree-interactive.crystaldelta.net/Accordion/c0ad1eed5c789e58653bbe9752a40c36"></iframe></div>`;

export const externalToolElementMockData = `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER} loree-style-062306"><iframe class="${CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_ELEMENT} loree-style-d30cbf" title="SHAN_ACCORDION_20" id="Accordion_c0ad1eed5c789e58653bbe9752a40c36" frameborder="0" width="100%" height="400" scrolling="no" src="https://stg.loree-interactive.crystaldelta.net/Accordion/c0ad1eed5c789e58653bbe9752a40c36"></iframe></div>`;

export const selectedRowElement = `<div class="loree-iframe-content-row row"><div class="col-12 loree-iframe-content-column"><p class="loree-iframe-content-element">Insert text here</</div></div>`;

export const selectedColumnElement = `<div class="col-12 loree-iframe-content-column coloumn-minHeight"><p class="loree-iframe-content-element">Insert text here</p></div>`;

export const paragraphMock = `<p style="margin: 0px; padding: 5px;" class="loree-iframe-content-element">Insert Text here</p>`;

export const headerMock = `<h3 style="margin: 0px; padding: 5px;" class="loree-iframe-content-element">Insert Text here</h3>`;

export const sidebarElement = (elementType: string) => `<button><div id="${elementType}"></button>`;

export const loreeWrapper = () => {
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('loree-wrapper');
  iframeDocument.title = 'loree iframe';
  iframe.innerHTML = '';
  appendElementToBody(iframeDocument);
  iframe.id = CONSTANTS.LOREE_IFRAME;
  iframe.className = 'loreeIframe';
  const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
  iframe.appendChild(wrapper);
  document.body.appendChild(iframe);
  const iframeDoc = iframe.contentWindow?.document;
  iframeDoc?.open().write(iframe.innerHTML);
  iframeDoc?.close();
};

export const loreeContentWrapperElement =
  '<div id="loree-iframe-content-wrapper" class="column-highlight"><div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column column-highlight element-highlight" style="padding:10px;"></div></div></div>';

export const containerWrappedWithLoreeWrapper =
  '<div id="loree-iframe-content-wrapper" class="column-highlight"><div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column column-highlight" style="padding:10px;"><div style="display:flex; margin:0px; padding: 5px;" class="loree-iframe-content-container-wrapper column-highlight"><div class="loree-iframe-content-container" style="width:100%;"><div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column column-highlight element-highlight" style="padding:10px;"></div></div></div></div></div></div></div>';

export const emptyRowColumnElement =
  '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column coloumn-minHeight column-highlight element-highlight" style="padding:10px;"></div></div>';

export const emptyContainerElement =
  '<div style="display:flex; margin:0px; padding: 5px;" class="loree-iframe-content-container-wrapper coloumn-minHeight element-highlight"><div class="loree-iframe-content-container" style="width:100%;"></div></div>';
