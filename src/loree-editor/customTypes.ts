export enum CustomElement {
  title = 'title',
  createdBy = 'createdBy',
  updatedAt = 'updatedAt',
  createdAt = 'createdAt',
  category = 'category',
}
