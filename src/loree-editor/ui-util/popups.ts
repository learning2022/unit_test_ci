export function getPopupWrapperDiv(doc: Document, zIndex: string = '4000'): HTMLDivElement {
  const div = doc.createElement('div');
  doc.body.appendChild(div);
  div.style.zIndex = zIndex;
  div.style.overflow = 'visible';
  return div;
}
