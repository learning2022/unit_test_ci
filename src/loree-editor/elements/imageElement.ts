import { ElementInterface } from '../interface';
import { elementIcon } from '../modules/sidebar/iconHolder';
import { translate } from '../../i18n/translate';
import CONSTANTS from '../constant';

export const imageElement: ElementInterface = {
  name: translate('element.images'),
  id: 'image',
  template: `${elementIcon.imageIcon}`,
  multiple: true,
  content: '',
  contents: [
    {
      name: 'FullImage',
      id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
      innerContent: [],
      template: `
      <div class='block-hover' id='fullImage'>
        <div class='p-4 block-height image-background'></div>
        <p class='text-center block-textName'>${translate('element.image')}</p>
      </div>`,
      content: `<div style="margin:0 0 10px;padding:5px;display: flex;" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER} ><img alt="loree" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} src="" /></div>`,
    },
    {
      name: 'ImageWithText',
      id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
      innerContent: [],
      template: `<div class='block-hover' id='imageWithText'>
      <div class='p-4 block-height image-background'>
        <p class='w-50 image-block-text' />
        <p class='w-50 image-block-text' />
        <p class='block-line image-block-text' />
      </div>
      <p class='text-center block-textName'>${translate('element.imagewithtext')}</p>
    </div>`,
      content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}><div style="display:  flex; align-items: center;position: relative;margin:0 0 10px;padding:5px;" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT}><img alt="loree" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} id ="ImageWithText" src=""><div style="position: absolute;color: #000000;padding: 15px;max-width:80%;top: 35%;"><h4 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px;">Header</h4><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px;">Insert text wrapper</p></div></div></div>`,
    },
    {
      name: 'ImageWithCaption',
      id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
      innerContent: [],
      template: `<div class='block-hover' id='imageWithCaption'>
      <div class='p-4 block-height centerImage-background mt-1'>
        <div class='image-background p-4 image-caption'></div>
        <p class='w-50 block-text m-1' />
        <p class='block-line block-text m-1' />
      </div>
      <p class='text-center block-textName'>${translate('element.imagewithcaption')}</p>
    </div>`,
      content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER} style="margin:0 0 10px;padding:5px;"><img alt="loree" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} src="" /></div><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px;">Insert text wrapper</p>`,
    },
    {
      name: 'CenteredImage',
      id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
      innerContent: [],
      template: `<div class='block-hover' id='centeredImage'>
      <div class='pl-4 pr-4 block-height centerImage-background'>
        <div class='w-100 h-100 center-image'></div>
      </div>
      <p class='text-center block-textName'>${translate('element.centeredimage')}</p>
    </div>`,
      content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}  style="text-align:center;margin:0 0 10px;padding:5px;"><img alt="loree" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} src="" /></div>`,
    },
    {
      name: 'leftImageWithText',
      id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
      innerContent: [],
      template: ` <div class='block-hover' id='leftImageWithText'>
      <div class=' row block-height m-1'>
        <div class='col-6 imgaewithText'></div>
        <div class='col-6 centerImage-background'>
          <div class='mt-4'>
            <p class='block-line block-text' />
            <p class='block-line block-text' />
            <p class='w-50 block-text' />
          </div>
        </div>
      </div>
      <p class='text-center block-textName'>${translate('element.leftalignedwithtext')}</p>
    </div>`,
      content: `<div class="row" style="margin:0 0 10px;"><div class="col-md-6" style="padding:0px"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}  style="height:auto; display: flex;padding:5px;margin:0 0 10px;"><img alt="loree" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE}  src="" /></div></div><div class="col-md-6" style="padding:0px"><h4 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">Header</h4><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">Insert text here</p></div></div>`,
    },
    {
      name: 'rightImageWithText',
      id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
      innerContent: [],
      template: `<div class='block-hover' id='rightImageWithText'>
      <div class=' row block-height m-1'>
        <div class='col-6 centerImage-background'>
          <div class='mt-4'>
            <p class='block-line block-text' />
            <p class='block-line block-text' />
            <p class='w-50 block-text' />
          </div>
        </div>
        <div class='col-6 imgaewithText'></div>
      </div>
      <p class='text-center block-textName'>${translate('element.rightalignedwithtext')}</p>
    </div>`,
      content: `<div class="row" style="margin:0 0 10px;"><div class="col-md-6" style="padding:0px"><h4 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">LoreumIpsum</h4><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">LoreumIpsum LoreumIpsumLoreumIpsumLoreumIpsum LoreumIpsum</p></div><div class="col-md-6" style="padding:0px"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}  style="height:auto; display: flex;padding:5px;margin:0 0 10px;"><img alt="loree" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE}  src="" /></div></div></div>`,
    },
  ],
};
