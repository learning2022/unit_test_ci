import { ElementInterface } from '../interface';
import { translate } from '../../i18n/translate';
import CONSTANTS from '../constant';
export const specialElement: ElementInterface = {
  name: 'Special Elements',
  id: 'specialblock',
  template: `<svg xmlns="http://www.w3.org/2000/svg" width="74" height="71.019" viewBox="0 0 74 71.019">
  <path id="special_elements" d="M50.816,69.757,66.932,41.989a16.215,16.215,0,0,1,3.957,4.079L56.405,71.019A16.56,16.56,0,0,1,50.816,69.757ZM3.923,70.97A3.89,3.89,0,0,1,0,67.111V42.789a3.889,3.889,0,0,1,3.923-3.854H29.6a3.888,3.888,0,0,1,3.92,3.854V67.111A3.889,3.889,0,0,1,29.6,70.97ZM2.846,42.789V67.111a1.068,1.068,0,0,0,1.077,1.062H29.6a1.07,1.07,0,0,0,1.077-1.062V42.789A1.069,1.069,0,0,0,29.6,41.732H3.923A1.067,1.067,0,0,0,2.846,42.789Zm70.022,7.357a15.841,15.841,0,0,1,.743,4.806A16.159,16.159,0,0,1,60.989,70.618ZM43.586,63.833,58.064,38.886a16.465,16.465,0,0,1,5.6,1.262L47.54,67.916A16.209,16.209,0,0,1,43.586,63.833Zm-2.722-8.88A16.158,16.158,0,0,1,53.494,39.286L41.608,59.754A15.815,15.815,0,0,1,40.864,54.952ZM44.4,32.035a3.9,3.9,0,0,1-3.93-3.858V3.854A3.892,3.892,0,0,1,44.4,0H70.074A3.89,3.89,0,0,1,74,3.854V28.176a3.893,3.893,0,0,1-3.926,3.858ZM43.322,3.854V28.176A1.074,1.074,0,0,0,44.4,29.238H70.074a1.071,1.071,0,0,0,1.081-1.062V3.854A1.068,1.068,0,0,0,70.074,2.8H44.4A1.07,1.07,0,0,0,43.322,3.854Zm-39.4,28.18A3.89,3.89,0,0,1,0,28.176V3.854A3.887,3.887,0,0,1,3.923,0H29.6a3.886,3.886,0,0,1,3.92,3.854V28.176a3.889,3.889,0,0,1-3.92,3.858ZM2.846,3.854V28.176a1.071,1.071,0,0,0,1.077,1.062H29.6a1.073,1.073,0,0,0,1.077-1.062V3.854A1.069,1.069,0,0,0,29.6,2.8H3.923A1.067,1.067,0,0,0,2.846,3.854Z" transform="translate(74 71.019) rotate(180)"/>
</svg>`,
  multiple: true,
  content: '',
  contents: [
    {
      name: 'Icon with Paragraph',
      id: CONSTANTS.LOREE_IFRAME_ICON_PRAGRAPH_ID,
      innerContent: [
        {
          name: 'Icon with Text',
          id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
          template: ` <div class="block-hover" id="leftImageWithText">
          <div class="row m-1 special-block-border">
            <div class="special-block-icon ml-2 mt-4"></div>
            <div class="col-9">
              <div class="mt-4">
                <p class="w-75 block-text"></p>
                <p class="w-75 block-text"></p>
                <p class="block-line block-text"></p>
              </div>
            </div>
          </div>
          <b class="font-weight-bold text-left block-textName">${translate(
            'element.iconwithtext',
          )}</b>
        </div>`,
          content: `<div class="row ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="margin:0;display:flex;flex-wrap:nowrap"><div class="d-flex justify-content-start align-items-start ${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="height:auto; max-width:20%;padding:5px;margin: 0 0 10px;"><img alt="loree" style="height:auto;border-width:0;border-style:solid;border-color:#000000;" class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" src="" /></div><p class="${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px;width:100%;">Insert text here</p></div>`,
        },
        {
          name: 'Icon with Heading',
          id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
          template: ` <div class="block-hover" id="leftImageWithText">
          <div class="row m-1 special-block-border">
            <div class="special-block-icon ml-2 mt-4"></div>
            <div class="col-9">
              <div class="mt-4">
              <p class="w-75 block-text-heading"></p>
              </div>
            </div>
          </div>
          <b class="font-weight-bold text-left block-textName">${translate(
            'element.iconwithheading',
          )}</b>
        </div>`,
          content: `<div class="row  ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="margin:0;display:flex;flex-wrap:nowrap"><div class="d-flex justify-content-start align-items-start ${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="height:auto;max-width:20%;padding:5px;margin: 0 0 10px;"><img alt="loree" style="height:auto;border-width:0;border-style:solid;border-color:#000000;" class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" src="" /></div><h4 class="${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px;width:100%">Header</h4></div>`,
        },
        {
          name: 'Icon with Heading and Text',
          id: CONSTANTS.LOREE_IFRAME_IMAGE_ID,
          template: ` <div class="block-hover" id="leftImageWithText">
          <div class="row m-1 special-block-border">
            <div class="special-block-icon ml-2 mt-4"></div>
            <div class="col-9">
              <div class="mt-4">
                <p class="w-75 block-text-heading"></p>
                <p class="w-75 block-text"></p>
                <p class="block-line block-text"></p>
              </div>
            </div>
          </div>
          <b class="font-weight-bold text-left block-textName">${translate(
            'element.iconwithheadingandtext',
          )}</b>
        </div>`,
          content: `<div class="row ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="margin:0;display:flex;flex-wrap:nowrap"><div class="d-flex justify-content-start align-items-start ${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="height:auto;max-width:20%;padding:5px;margin: 0 0 10px;"><img alt="loree" style="height:auto;border-width:0;border-style:solid;border-color:#000000;" class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" src="" /></div><div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="padding:0px;width:100%;"><h4 class="${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin: 0 0 0px;width:100%;">Header</h4><p class="${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} ${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}" style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin: 0px 0px 10px;width:100%;">Insert text here</p></div></div>`,
        },
      ],
      template: ` <div class="block-hover" id="leftImageWithText">
      <div class="row m-1 special-block-border">
        <div class="special-block-icon ml-2 mt-4"></div>
        <div class="col-9">
          <div class="mt-4">
            <p class="w-75 block-text"></p>
            <p class="w-75 block-text"></p>
            <p class="block-line block-text"></p>
          </div>
        </div>
      </div>
      <b class="font-weight-bold text-left block-textName" style="padding: 5px;">${translate(
        'element.iconwithparagraph',
      )}</b>
    </div>`,
      content: '',
    },
    {
      name: 'Navigation Menu',
      id: CONSTANTS.LOREE_IFRAME_NAVIGATE_ID,
      innerContent: [
        {
          name: 'Navigation 1',
          id: CONSTANTS.LOREE_IFRAME_NAVIGATE_ID,
          template: `<div class="block-hover"><div class="row m-1 special-block-border"><div class="col-4" style="padding: 3px;"><div class="mt-4"><p class="w-100 navigate-text-heading"></p></div></div><div class="col-4" style="padding: 3px;"><div class="mt-4"><p class="w-100 navigate-text"></p></div></div><div class="col-4" style="padding: 3px;"><div class="mt-4"><p class="w-100 navigate-text"></p></div></div></div><div class="block-textNameContent"><b class="font-weight-bold text-left block-textName">${translate(
            'element.textnavigation',
          )}</b></div></div>`,
          content: `<div class="row ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } navigation1" style="display: flex; width:100%; margin: 0px; color:#000000;"><nav class="navbar navbar-expand-sm navbar-light ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="padding: 0px; width: 100%; flex: 1;"><ul class="navbar-nav ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="margin: 0px 0px 0px 0px; list-style: none; padding: 0px; flex-wrap: wrap; justify-content: flex-start;"><li class="${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } nav-item ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 0px;"><p class="${
            CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS
          } ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li></ul></nav></div>`,
        },
        {
          name: 'Navigation 2',
          id: CONSTANTS.LOREE_IFRAME_NAVIGATE_ID,
          template: `<div class="block-hover"><div class="row m-1 special-block-border"><div class="col-12" style="padding: 5px;"><div class="mt-12" style="display: inline-flex; height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" width="152" height="13.037" viewBox="0 0 152 13.037" style="margin: auto 0; width: 100%;"><g id="left_image_right_text" data-name="left_image &amp; right_text" transform="translate(30 -5.482)"><rect id="Rectangle_3238" data-name="Rectangle 3238" width="40" height="12" transform="translate(41 6)" fill="#585858"></rect><rect id="Rectangle_3240" data-name="Rectangle 3240" width="40" height="12" transform="translate(82 6)" fill="#909090"></rect><path id="Union_168" data-name="Union 168" d="M1,13.037a.945.945,0,0,1-.739-.376,1.341,1.341,0,0,1-.21-1.183L2.956.805A1.04,1.04,0,0,1,3.868,0,.993.993,0,0,1,4.84.656L6.846,5.478l1.2-1.7A.955.955,0,0,1,9.335,3.5a.916.916,0,0,1,.35.381L12.01,7.8l.455-.921a.921.921,0,0,1,.469-.5.974.974,0,0,1,.725-.047.931.931,0,0,1,.6.6l2,4.366a1.386,1.386,0,0,1-.109,1.312.961.961,0,0,1-.794.434ZM3.736,1.007.834,11.68a.578.578,0,0,0,.073.507.156.156,0,0,0,.1.068H15.357a.17.17,0,0,0,.122-.087.62.62,0,0,0,.039-.57L13.512,7.216a.411.411,0,0,1-.021-.057.138.138,0,0,0-.091-.094.149.149,0,0,0-.108.007.138.138,0,0,0-.071.079.361.361,0,0,1-.019.043l-.5,1.014a.724.724,0,0,1-.372.387.75.75,0,0,1-.561.026.718.718,0,0,1-.406-.348L8.975,4.259c-.007-.012-.014-.025-.02-.037a.128.128,0,0,0-.05-.057.135.135,0,0,0-.181.04L7.444,6.015a.732.732,0,0,1-.454.3.741.741,0,0,1-.874-.5L4.092.95c-.044-.1-.11-.169-.168-.169h0C3.86.785,3.778.858,3.736,1.007ZM6.769,5.587l0,0Zm6.269-1.535A2,2,0,0,1,11.013,2.09a2.026,2.026,0,0,1,4.05.008,1.992,1.992,0,0,1-2.025,1.954Z" transform="translate(-30 5.482)"></path></g></svg></div></div></div><div class="block-textNameContent"><b class="font-weight-bold text-left block-textName">${translate(
            'element.navigationwithimagealignedleft',
          )}</b></div></div>`,
          content: `<div class="row ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } navigation2" style="display: flex; width:100%; margin: 0px; color:#000000;"><div class=${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION
          }><img class="loree-iframe-content-image  ${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO
          }" src='https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg' alt="navigation_logo" style="height: auto; width: 80px;" /></div><nav class="navbar navbar-expand-sm navbar-light ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="padding: 0px; margin-left: auto; margin-right: 0px; flex: 1;"><ul class="navbar-nav  ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="margin: 0px 0px 0px 0px; list-style: none; padding: 0px; margin-right: 0px !important; margin-left: auto; flex-wrap: wrap; justify-content: flex-end;"><li class="${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } nav-item ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 0px;"><p class="${
            CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS
          } ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #000000;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li></ul></nav></div>`,
        },
        {
          name: 'Navigation 3',
          id: CONSTANTS.LOREE_IFRAME_NAVIGATE_ID,
          template: `<div class="block-hover"><div class="row m-1 special-block-border"><div class="col-12" style="padding: 5px;"><div class="mt-12" style="display: inline-flex; height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" width="152.374" height="13.036" viewBox="0 0 152.374 13.036" style="margin: auto 0; width: 100%;"><g id="left_text_right_image" data-name="left_text &amp; right_image" transform="translate(0 -5.482)"><rect id="Rectangle_3238" data-name="Rectangle 3238" width="40" height="12" transform="translate(41 6)" fill="#585858"></rect><rect id="Rectangle_3240" data-name="Rectangle 3240" width="40" height="12" transform="translate(0 6)" fill="#909090"></rect><path id="Union_168" data-name="Union 168" d="M1,13.036a.945.945,0,0,1-.739-.376,1.341,1.341,0,0,1-.21-1.183L2.956.805A1.04,1.04,0,0,1,3.868,0a.993.993,0,0,1,.972.654L6.846,5.478l1.2-1.7A.955.955,0,0,1,9.335,3.5a.916.916,0,0,1,.35.381L12.009,7.8l.455-.921a.921.921,0,0,1,.469-.5.973.973,0,0,1,.725-.047.931.931,0,0,1,.6.6l2,4.366a1.386,1.386,0,0,1-.109,1.312.961.961,0,0,1-.794.434ZM3.736,1.007.834,11.68a.578.578,0,0,0,.073.507.156.156,0,0,0,.1.068H15.357a.17.17,0,0,0,.122-.087.62.62,0,0,0,.039-.57L13.511,7.216a.411.411,0,0,1-.021-.057.138.138,0,0,0-.091-.094.149.149,0,0,0-.108.007.138.138,0,0,0-.071.079.361.361,0,0,1-.019.043l-.5,1.014a.724.724,0,0,1-.372.387.75.75,0,0,1-.561.026.718.718,0,0,1-.406-.348L8.975,4.259c-.007-.012-.014-.025-.02-.037a.128.128,0,0,0-.05-.057.135.135,0,0,0-.181.04L7.444,6.014a.732.732,0,0,1-.454.3.741.741,0,0,1-.874-.5L4.091.95c-.044-.1-.11-.169-.168-.169h0C3.86.785,3.778.858,3.736,1.007ZM6.769,5.587l0,0Zm6.269-1.535A2,2,0,0,1,11.013,2.09a2.026,2.026,0,0,1,4.05.008,1.992,1.992,0,0,1-2.025,1.954Z" transform="translate(135.999 5.482)"></path></g></svg></div></div></div><div class="block-textNameContent"><b class="font-weight-bold text-left block-textName">${translate(
            'element.navigationwithimagealignedright',
          )}</b></div></div>`,
          content: `<div class="row ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } navigation3" style="display: flex; width:100%; margin: 0px; color:#000000;"><nav class="navbar navbar-expand-sm navbar-light ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="padding: 0px; margin-left: 0px; margin-right: auto; flex: 1;"><ul class="navbar-nav  ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="margin: 0px 0px 0px 0px; list-style: none; padding: 0px; margin-right: auto !important; margin-left: 0px; flex-wrap: wrap; justify-content: flex-start;"><li class="${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } nav-item ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 0px;"><p class="${
            CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS
          } ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li></ul></nav><div class=${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION
          } style="text-align: right; "><img class="loree-iframe-content-image  ${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO
          }" src="https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg" alt="navigation_logo" style="height: auto; width: 80px;" /></div></div>`,
        },
        {
          name: 'Navigation 9',
          id: CONSTANTS.LOREE_IFRAME_NAVIGATE_ID,
          template: `<div class="block-hover"><div class="row m-1 special-block-border"><div class="col-12" style="padding: 5px;"><div class="mt-12" style="display: inline-flex; height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" width="152.376" height="13.037" viewBox="0 0 152.376 13.037" style="margin: auto 0; width: 100%;"><g id="two_images_opposite" transform="translate(30 -5.482)"><rect id="Rectangle_3238" data-name="Rectangle 3238" width="40" height="12" transform="translate(6 6)" fill="#585858"></rect><rect id="Rectangle_3240" data-name="Rectangle 3240" width="40" height="12" transform="translate(47 6)" fill="#909090"></rect><path id="Union_168" data-name="Union 168" d="M1,13.037a.945.945,0,0,1-.739-.376,1.341,1.341,0,0,1-.21-1.183L2.956.805A1.04,1.04,0,0,1,3.868,0,.993.993,0,0,1,4.84.656L6.846,5.478l1.2-1.7A.955.955,0,0,1,9.335,3.5a.916.916,0,0,1,.35.381L12.01,7.8l.455-.921a.921.921,0,0,1,.469-.5.974.974,0,0,1,.725-.047.931.931,0,0,1,.6.6l2,4.366a1.386,1.386,0,0,1-.109,1.312.961.961,0,0,1-.794.434ZM3.736,1.007.834,11.68a.578.578,0,0,0,.073.507.156.156,0,0,0,.1.068H15.357a.17.17,0,0,0,.122-.087.62.62,0,0,0,.039-.57L13.512,7.216a.411.411,0,0,1-.021-.057.138.138,0,0,0-.091-.094.149.149,0,0,0-.108.007.138.138,0,0,0-.071.079.361.361,0,0,1-.019.043l-.5,1.014a.724.724,0,0,1-.372.387.75.75,0,0,1-.561.026.718.718,0,0,1-.406-.348L8.975,4.259c-.007-.012-.014-.025-.02-.037a.128.128,0,0,0-.05-.057.135.135,0,0,0-.181.04L7.444,6.015a.732.732,0,0,1-.454.3.741.741,0,0,1-.874-.5L4.092.95c-.044-.1-.11-.169-.168-.169h0C3.86.785,3.778.858,3.736,1.007ZM6.769,5.587l0,0Zm6.269-1.535A2,2,0,0,1,11.013,2.09a2.026,2.026,0,0,1,4.05.008,1.992,1.992,0,0,1-2.025,1.954Z" transform="translate(-30 5.482)"></path><path id="Union_189" data-name="Union 189" d="M1,13.037a.945.945,0,0,1-.739-.376,1.341,1.341,0,0,1-.21-1.183L2.956.805A1.04,1.04,0,0,1,3.868,0,.993.993,0,0,1,4.84.656L6.846,5.478l1.2-1.7A.955.955,0,0,1,9.335,3.5a.916.916,0,0,1,.35.381L12.01,7.8l.455-.921a.921.921,0,0,1,.469-.5.974.974,0,0,1,.725-.047.931.931,0,0,1,.6.6l2,4.366a1.386,1.386,0,0,1-.109,1.312.961.961,0,0,1-.794.434ZM3.736,1.007.834,11.68a.578.578,0,0,0,.073.507.156.156,0,0,0,.1.068H15.357a.17.17,0,0,0,.122-.087.62.62,0,0,0,.039-.57L13.512,7.216a.411.411,0,0,1-.021-.057.138.138,0,0,0-.091-.094.149.149,0,0,0-.108.007.138.138,0,0,0-.071.079.361.361,0,0,1-.019.043l-.5,1.014a.724.724,0,0,1-.372.387.75.75,0,0,1-.561.026.718.718,0,0,1-.406-.348L8.975,4.259c-.007-.012-.014-.025-.02-.037a.128.128,0,0,0-.05-.057.135.135,0,0,0-.181.04L7.444,6.015a.732.732,0,0,1-.454.3.741.741,0,0,1-.874-.5L4.092.95c-.044-.1-.11-.169-.168-.169h0C3.86.785,3.778.858,3.736,1.007ZM6.769,5.587l0,0Zm6.269-1.535A2,2,0,0,1,11.013,2.09a2.026,2.026,0,0,1,4.05.008,1.992,1.992,0,0,1-2.025,1.954Z" transform="translate(106 5.482)"></path></g></svg></div></div></div><div class="block-textNameContent"><b class="font-weight-bold text-left block-textName">${translate(
            'element.navigationwithtwoimagealignedcenter',
          )}</b></div></div>`,
          content: `<div class="row ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } navigation9" style="display: flex; width:100%; margin: 0px; color:#000000;"><div class=${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION
          } style="text-align: left;"><img class="loree-iframe-content-image  ${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO
          }" src="https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg" alt="navigation_logo" style="height: auto; width: 80px;" /></div><nav class="navbar navbar-expand-sm navbar-light ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="padding: 0px; margin-left: auto; margin-right: auto; flex: 1;"><ul class="navbar-nav ${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          }" style="margin: 0px 0px 0px 0px; list-style: none; padding: 0px; margin-right: auto !important; margin-left: auto; flex-wrap: wrap; justify-content: center;"><li class="${
            CONSTANTS.LOREE_IFRAME_CONTENT_MENU
          } nav-item ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 0px;"><p class="${
            CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS
          } ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
            CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
          }" style="padding: 12px 20px; border-width: 0; border-style: solid; border-color: #000000;  margin: 0px;">${translate(
            'element.menuitem',
          )}</p></li></ul></nav><div class=${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION
          } style="text-align: right;"><img class="loree-iframe-content-image  ${
            CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO
          }" src="https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg" alt="navigation_logo" style="height: auto; width: 80px;" /></div></div>`,
        },
      ],
      template: `<div class="block-hover" id="${CONSTANTS.LOREE_NAVIGATION_MAIN_MENU_SECTION}"><div class="row m-1 special-block-border"><div class="col-4" style="padding: 3px;"><div class="mt-4"><p class="w-100 navigate-text-heading"</p></div></div><div class="col-4" style="padding: 3px;"><div class="mt-4"><p class="w-100 navigate-text"></p></div></div><div class="col-4" style="padding: 3px;"><div class="mt-4"><p class="w-100 navigate-text"></p></div></div></div><b class="font-weight-bold text-left block-textName" style="padding: 5px;">Navigation</b></div>`,
      content: '',
    },
  ],
};
