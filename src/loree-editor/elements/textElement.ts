import { ElementInterface } from '../interface';
import { elementIcon } from '../modules/sidebar/iconHolder';
import CONSTANTS from '../constant';
import { translate } from '../../i18n/translate';

export const textElement: ElementInterface = {
  name: 'Text',
  id: 'Text',
  template: `${elementIcon.textIcon}`,
  multiple: true,
  content: '',
  contents: [
    {
      name: 'Header',
      id: 'Header',
      innerContent: [],
      template: `<div class='block-hover' id='header'>
      <div class='box-height p-4'>
        <p class='block-text-heading' />
      </div>
      <p class='text-center block-textName'>${translate('element.header')}</p>
    </div>`,
      content: `<h3 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="border-width: 0; border-style: solid; border-color: #000000;color: #000000;padding: 5px; margin:0 0 10px;">Header</h3>`,
    },
    {
      name: 'Paragraph',
      id: 'paragraph',
      innerContent: [],
      template: `<div class='block-hover'true id='paragraph'>
      <div class='p-4 box-height'>
        <p class='block-text' />
        <p class='block-text' />
        <p class='text-block-line block-text' />
      </div>
      <p class='text-center block-textName'>${translate('element.paragraph')}</p>
    </div>`,
      content: `<p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="border-width: 0; border-style: solid; border-color: #000000;color: #000000; padding: 5px; margin:0 0 10px;">Insert text here</p>`,
    },
    {
      name: 'HeaderWithParagraph',
      id: 'headerWithParagraph',
      innerContent: [],
      template: `<div class='block-hover' id='headerWithParagraph'>
      <div class='box-height p-4'>
        <p class='block-text-heading' />
        <p class='block-text' />
        <p class='text-block-line block-text' />
      </div>
      <p class='text-center block-textName'>${translate('element.headingwithparagraph')}</p>
    </div>`,
      content: `<h3 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="border-width: 0; border-style: solid; border-color: #000000;color: #000000; padding: 5px; margin:0 0 10px;">Header</h3><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="border-width: 0; border-style: solid; border-color: #000000;color: #000000;  padding: 5px;  margin:0 0 10px;">Insert text here</p>`,
    },
    {
      name: 'QuoteBlock',
      id: 'quoteBlock',
      innerContent: [],
      template: `<div class='block-hover' id='quoteBlock'>
      <div class='box-height px-3 py-1'>
        <span class='quote-block ml-n3'>${elementIcon.blockQuoteIcon}</span>
        <p class='font-weight-bold mt-n3 mb-0 quote-block-content'>Lorem Ipsum Lorem</p>
        <p class='font-weight-bold mb-0 quote-block-content'>Ipsum Lorem</p>
        <p class='font-italic quote-block-author'>- Author Name</p>
      </div>
      <p class='text-center block-textName'>${translate('element.quoteblock')} </p>
    </div>`,
      content: `
        <p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}  style="color: #000000;font-weight: bold;font-family:Be Vietnam; border-width: 0; border-style: solid; border-color: #000000; padding: 5px; margin:0 0 10px;">
          "
        </p>
        <p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="border-width: 0; border-style: solid; border-color: #000000;color: #000000; padding: 5px; margin:0 0 10px;">Lorem Ipsum Lorem IpsumLoremIpsumLorem Ipsum any quotes here applied will be displayed</p>
        <p class="${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="color: #000000;font-style: italic;display:block;margin:0 0 10px; border-width: 0; border-style: solid; border-color: #000000; padding: 5px">- Author Name</p>
        `,
    },
    {
      name: 'sourceFormat',
      id: 'sourceFormat',
      innerContent: [],
      template: `<div class='block-hover' id='sourceFormat'>
      <div class='box-height d-flex justify-content-center align-items-center'>
        <p class='font-weight-bold paste-source'>${elementIcon.pasetAnySource}</p>
      </div>
      <p class='text-center block-textName'>${translate('element.sourceformat')}</p>
    </div>`,
      content: `<div style="display: inline-block; width: 100%; padding: 5px; margin:0 0 10px;" class=${CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE}><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="margin:0 0 10px; padding: 5px;">Paste any content</p></div>`,
    },
  ],
};
