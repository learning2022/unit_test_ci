import { getElementsByClassName } from '../common/dom';
import CONSTANTS from '../constant';
import { imageElement } from './imageElement';
describe('image section', () => {
  const imageContents = imageElement.contents;
  test.each(imageContents)('removal of width', (imageContent) => {
    document.body.innerHTML = imageContent.content;
    expect(getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER)[0]).not.toHaveStyle(
      'width: 100%',
    );
  });
});
