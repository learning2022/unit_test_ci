import { ElementInterface } from './interface';
import { elementIcon, dividerIconSet } from './modules/sidebar/iconHolder';
import CONSTANTS from './constant';
import { tableElement } from './elements/tableElement';
import { specialElement } from './elements/specialElement';
import { textElement } from './elements/textElement';
import { imageElement } from './elements/imageElement';
import { translate } from '../i18n/translate';

const ELEMENTS: Array<ElementInterface> = [
  imageElement,
  {
    name: translate('global.videos'),
    id: 'video',
    template: `${elementIcon.videoIcon}`,
    multiple: true,
    content: '',
    contents: [
      {
        name: 'Video',
        id: CONSTANTS.LOREE_IFRAME_VIDEO_ID,
        innerContent: [],
        template: `
        <div class='video-block-hover' id='video'>
          <div class='video-block-height justify-cotent-center'>
            <div class='video-background video'></div>
          </div>
          <p class='font-weight-bold text-center video-block-textName'>${translate(
            'element.video',
          )}</p>
        </div>`,
        content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER} style="display: flex;padding: 5px;margin: 0 0 10px;"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO} style="position:relative;width: 100%;display: inline-flex;max-width: 100%;max-height: 100%;border-width: 0;border-style: solid;border-color: #000000"><div style="padding: 42% 0px 0px;display: flex;"><video class=${CONSTANTS.LOREE_NO_POINTER} style="position:absolute;top:0;left:0;width:100%;height:100%;max-width:100%;" controls src=""></video><iframe class=${CONSTANTS.LOREE_NO_POINTER} src="" frameborder="0" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width: 100%;"></iframe></div></div></div>`,
      },
      {
        name: 'videoWithText',
        id: CONSTANTS.LOREE_IFRAME_VIDEO_ID,
        innerContent: [],
        template: `
        <div class='video-block-hover' id='videoWithText'>
          <div class='video-block-height video-background' style='padding-left: 15px;'>
          <div class='d-inline-flex align-items-center'>
            <div>
              <p class='video-block-text video-block-text-long' />
              <p class='video-block-text video-block-text-long' />
              <p class='video-block-text video-block-text-short' />
            </div>
          </div>
          </div>
          <p class='font-weight-bold text-center video-block-textName'>${translate(
            'element.videowithtext',
          )}</p>
        </div>`,
        content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER} style="display: flex;padding: 5px;margin: 0 0 10px;"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO} style="position:relative;width: 100%;display: inline-flex;max-width: 100%;max-height: 100%;border-width: 0;border-style: solid;border-color: #000000"><div style="padding: 42% 0px 0px;display: flex;"><video class=${CONSTANTS.LOREE_NO_POINTER} style="position:absolute;top:0;left:0;width:100%;height:100%;max-width:100%;" controls src=""></video><iframe class=${CONSTANTS.LOREE_NO_POINTER} src="" frameborder="0" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width: 100%;"></iframe><div style="position: absolute;color: #000000;padding: 15px;top: 35%;max-width:90%;"><h4 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px">Header</h4><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:0 0 10px;">Insert text wrapper</p></div></div></div></div>`,
      },
      {
        name: 'videoAndTextAligned',
        id: CONSTANTS.LOREE_IFRAME_VIDEO_ID,
        innerContent: [],
        template: `
        <div class='video-block-hover' id='videoAndTextAligned'>
          <div class='video-block-height row justify-content-between align-items-center'>
            <div class='col-6'>
              <p class='video-block-text video-block-text-long' />
              <p class='video-block-text video-block-text-long' />
              <p class='video-block-text video-block-text-short' />
            </div>
            <div class='video-with-text-wrapper col-6 align-items-center'>
              <div class= 'video-background video-with-text'></div>
            </div>
          </div>
          <p class='font-weight-bold text-center video-block-textName'>${translate(
            'element.videoandtextaligned',
          )}</p>
        </div>`,
        content: `<div class="row" style="margin:0;"><div class="col-md-6" style="padding:0px;position: relative;"><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">LoreumIpsum LoreumIpsumLoreumIpsumLoreumIpsum LoreumIpsum</p></div><div class="col-md-6" style="padding:0px;"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER} style="display: flex;padding: 5px;"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO} style="position:relative;width: 100%;display: inline-flex;max-width: 100%;max-height: 100%;border-width: 0;border-style: solid;border-color: #000000"><div style="padding: 42% 0px 0px;display: flex;"><video class=${CONSTANTS.LOREE_NO_POINTER} style="position:absolute;top:0;left:0;width:100%;height:100%;max-width:100%;" controls src=""></video><iframe class=${CONSTANTS.LOREE_NO_POINTER} src="" frameborder="0" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width: 100%;"></iframe></div></div></div></div></div>`,
      },
      {
        name: 'videoWithHeadingParagraph',
        id: CONSTANTS.LOREE_IFRAME_VIDEO_ID,
        innerContent: [],
        template: `
        <div class='video-block-hover' id='videoWithHeadingParagraph'>
          <div class='video-block-height row justify-content-between align-items-center'>
            <div class='col-6'>
              <p class='video-block-text video-block-heading' />
              <p class='video-block-text video-block-text-long' />
              <p class='video-block-text video-block-text-short' />
            </div>
            <div class='video-with-text-wrapper col-6 align-items-center'>
              <div class= 'video-background video-with-text'></div>
            </div>
          </div>
          <p class='font-weight-bold text-center video-block-textName'>${translate(
            'element.videowithheadingandparagraph',
          )}</p>
        </div>`,
        content: `<div class="row" style="margin:0;"><div class="col-md-6" style="padding:0px;"><h4 class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000;border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">LoreumIpsum</h4><p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="color: #000000; border-width: 0px;border-style: solid;border-color: #000000;padding:5px;margin:5px">LoreumIpsum LoreumIpsumLoreumIpsumLoreumIpsum LoreumIpsum</p></div><div class="col-md-6" style="padding:0px;"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER} style="display: flex;padding: 5px;"><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO} style="position:relative;width: 100%;display: inline-flex;max-width: 100%;max-height: 100%;border-width: 0;border-style: solid;border-color: #000000"><div style="padding: 42% 0px 0px;display: flex;"><video class=${CONSTANTS.LOREE_NO_POINTER} style="position:absolute;top:0;left:0;width:100%;height:100%;max-width:100%;" controls src=""></video><iframe class=${CONSTANTS.LOREE_NO_POINTER} src="" frameborder="0" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width: 100%;"></iframe></div></div></div></div></div>`,
      },
    ],
  },
  textElement,
  {
    name: 'Divider',
    id: 'Divider',
    template: `${elementIcon.dividerIcon}`,
    multiple: true,
    content: '',
    contents: [
      {
        name: 'Line',
        id: CONSTANTS.LOREE_IFRAME_LINE_ID,
        innerContent: [],
        template: `<div class="d-flex flex-column editor-submenu-option">
        <div class="submenu-icon-item d-flex align-items-center justify-content-center">${
          dividerIconSet.verticalLineDivider
        }</div>
        <label class="submenu-label-item font-weight-bold mt-1 mb-3 text-center">${translate(
          'element.line',
        )}</label>
        </div>`,
        content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER}  style="max-width:100%;padding: 5px; margin:0 0 10px;"><hr style="vertical-align:middle;margin:0px;width:100%;max-width:100%;border-width:3px 0 0 0;border-style:solid;border-color:#585858;display:inline-flex;" class=${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER} /></div>`,
      },
      {
        name: 'Space',
        id: CONSTANTS.LOREE_IFRAME_SPACE_ID,
        innerContent: [],
        template: `<div class="d-flex flex-column editor-submenu-option">
        <div class="submenu-icon-item d-flex align-items-center justify-content-center">${
          dividerIconSet.verticalSpaceDivider
        }</div>
        <label class="submenu-label-item font-weight-bold mt-1 mb-3 text-center">${translate(
          'element.space',
        )}</label>
        </div>`,
        content: `<div class=${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER}  style="display: flex; max-width:100%; margin:0 0 10px; padding: 5px;"><div style="width:100%;height:100%;max-width:100%;min-height:60px;" class=${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER}></div>`,
      },
    ],
  },
  tableElement,
  {
    name: 'Container',
    id: CONSTANTS.LOREE_DESIGN_SECTION_CONTAINER_ELEMENT,
    template: `<svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 104 104"><g id="Layer_2" data-name="Layer 2"><g id="Layer_4" data-name="Layer 4"><path d="M98.48,0h-93A5.52,5.52,0,0,0,0,5.52V19.35a5.52,5.52,0,0,0,5.52,5.52h93A5.52,5.52,0,0,0,104,19.35V5.52A5.52,5.52,0,0,0,98.48,0ZM100,19.35a1.52,1.52,0,0,1-1.52,1.52h-93A1.52,1.52,0,0,1,4,19.35V5.52A1.52,1.52,0,0,1,5.52,4h93A1.52,1.52,0,0,1,100,5.52Z"/><path d="M98.48,79.13h-93A5.52,5.52,0,0,0,0,84.65V98.48A5.52,5.52,0,0,0,5.52,104h93A5.52,5.52,0,0,0,104,98.48V84.65A5.52,5.52,0,0,0,98.48,79.13ZM100,98.48A1.52,1.52,0,0,1,98.48,100h-93A1.52,1.52,0,0,1,4,98.48V84.65a1.52,1.52,0,0,1,1.52-1.52h93A1.52,1.52,0,0,1,100,84.65Z"/><polygon points="93.3 44.27 60.59 70.05 76.11 70.05 93.3 56.5 93.3 44.27"/><path d="M87.79,34H80.23L34.42,70.05H49.94L92.71,36.34A5.8,5.8,0,0,0,87.79,34Z"/><polygon points="10.7 59.73 43.41 33.95 27.89 33.95 10.7 47.5 10.7 59.73"/><path d="M93.3,65.71V64.9l-6.54,5.15h1C90.83,70.05,93.3,68.11,93.3,65.71Z"/><path d="M11.29,67.66a5.8,5.8,0,0,0,4.92,2.39h7.56L69.58,34H54.06Z"/><path d="M10.7,38.29v.81L17.24,34h-1C13.17,34,10.7,35.89,10.7,38.29Z"/></g></g></svg>`,
    multiple: false,
    content: `<div style="display:flex; margin:0px; padding: 5px;" class=${CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER}><div class=${CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER} style="width:100%;"></div></div>`,
    contents: [],
  },
  specialElement,
  {
    name: 'Custom Elements',
    id: CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT,
    template: `${elementIcon.customElementIcon}`,
    multiple: false,
    content: '',
    contents: [],
  },
  {
    name: 'Loree Interactive',
    id: CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT,
    template: `${elementIcon.interactiveIcon}`,
    multiple: false,
    content: '',
    contents: [],
  },
  {
    name: 'H5P Interactive',
    id: CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT,
    template: `${elementIcon.h5pIcon}`,
    multiple: false,
    content: '',
    contents: [],
  },
  {
    name: 'Embed URL',
    id: CONSTANTS.LOREE_IFRAME_EMBED_URL_ELEMENT,
    template: `${elementIcon.embedUrl}`,
    multiple: false,
    content: '',
    contents: [],
  },
];

export default ELEMENTS;
