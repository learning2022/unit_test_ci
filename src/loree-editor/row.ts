import { RowsInterface } from './interface';
import CONSTANTS from './constant';

const ROWS: Array<RowsInterface> = [
  {
    name: 'Custom Rows',
    id: 'customRows',
    template: `<div class='sidebarRow'><div style="flex: 1;background-color: #909090"><span style="color:#ffffff; margin: auto;">Custom Rows</span></div></div> `,
    htmlString: '',
  },
  {
    name: 'One Column',
    id: 'oneColumn',
    template: `<div class='sidebarRow'><div style="flex: 1;"></div></div> `,
    multiple: false,
    htmlString: `
    <div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>`,
    contents: [],
  },
  {
    name: 'Two Columns',
    id: 'twoColumns',
    template: `<div class='sidebarRow'><div style="flex: 1;"></div></div><div class='sidebarRow'><div style="flex: 1;"></div></div>`,
    multiple: true,
    htmlString: '',
    contents: [
      {
        name: 'Two Columns 50X50',
        id: 'twoColumns50X50',
        template: `
          <div class='sidebarRow' style="flex: 1;"><div>50</div></div>
          <div class='sidebarRow' style="flex: 1;"><div>50</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px"></div><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px"></div></div>
				`,
      },
      {
        name: 'Two Columns 60X40',
        id: 'twoColumns60X40',
        template: `
          <div class='sidebarRow' style="flex: 0.6;"><div>60</div></div>
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div></div>
				`,
      },
      {
        name: 'Two Columns 40X60',
        id: 'twoColumns40X60',
        template: `
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
          <div class='sidebarRow' style="flex: 0.6;"><div>60</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
      {
        name: 'Two Columns 70X30',
        id: 'twoColumns70X30',
        template: `
          <div class='sidebarRow' style="flex: 0.7;"><div>70</div></div>
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>
				`,
      },
      {
        name: 'Two Columns 30X70',
        id: 'twoColumns30X70',
        template: `
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
          <div class='sidebarRow' style="flex: 0.7;"><div>70</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div><div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>
				`,
      },
      {
        name: 'Two Columns 80X20',
        id: 'twoColumns80X20',
        template: `
          <div class='sidebarRow' style="flex: 0.8;"><div>80</div></div>
          <div class='sidebarRow' style="flex: 0.2;"><div>20</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>
				`,
      },
      {
        name: 'Two Columns 20X80',
        id: 'twoColumns20X80',
        template: `
          <div class='sidebarRow' style="flex: 0.2;"><div>20</div></div>
          <div class='sidebarRow' style="flex: 0.8;"><div>80</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div><div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>
				`,
      },
      {
        name: 'Two Columns 90X10',
        id: 'twoColumns90X10',
        template: `
          <div class='sidebarRow' style="flex: 0.9;"><div>90</div></div>
          <div class='sidebarRow' style="flex: 0.1;"><div>10</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div><div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>
				`,
      },
      {
        name: 'Two Columns 10X90',
        id: 'twoColumns10X90',
        template: `
          <div class='sidebarRow' style="flex: 0.1;"><div>10</div></div>
          <div class='sidebarRow' style="flex: 0.9;"><div>90</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div><div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px;"></div></div>
				`,
      },
    ],
  },
  {
    name: 'Three Columns',
    id: 'threeColumns',
    template: `
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
    `,
    multiple: true,
    htmlString: ``,
    contents: [
      {
        name: 'Three Columns 33X33X33',
        id: 'threeColumns33X33X33 ',
        template: `
          <div class='sidebarRow' style="flex: 1;"><div>33</div></div>
          <div class='sidebarRow' style="flex: 1;"><div>33</div></div>
          <div class='sidebarRow' style="flex: 1;"><div>33</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px"></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px"></div><div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px"></div></div>
				`,
      },
      {
        name: 'Three Columns 20X60X20',
        id: 'threeColumns20X60X20',
        template: `
          <div class='sidebarRow' style="flex: 0.2;"><div>20</div></div>
          <div class='sidebarRow' style="flex: 0.6;"><div>60</div></div>
          <div class='sidebarRow' style="flex: 0.2;"><div>20</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div></div>
				`,
      },
      {
        name: 'Three Columns 40X40X20',
        id: 'threeColumns40X40X20',
        template: `
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
          <div class='sidebarRow' style="flex: 0.2;"><div>20</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
      {
        name: 'Three Columns 20X40X40',
        id: 'threeColumns20X40X40',
        template: `
          <div class='sidebarRow' style="flex: 0.2;"><div>20</div></div>
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
      {
        name: 'Three Columns 30X30X40',
        id: 'threeColumns30X30X40',
        template: `
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
      {
        name: 'Three Columns 40X30X30',
        id: 'threeColumns40X30X30',
        template: `
          <div class='sidebarRow' style="flex: 0.4;"><div>40</div></div>
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
      {
        name: 'Three Columns 10X30X60',
        id: 'threeColumns10X30X60',
        template: `
          <div class='sidebarRow' style="flex: 0.1;"><div>10</div></div>
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
          <div class='sidebarRow' style="flex: 0.6;"><div>60</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
      {
        name: 'Three Columns 60X30X10',
        id: 'threeColumns60X30X10',
        template: `
          <div class='sidebarRow' style="flex: 0.6;"><div>60</div></div>
          <div class='sidebarRow' style="flex: 0.3;"><div>30</div></div>
          <div class='sidebarRow' style="flex: 0.1;"><div>10</div></div>
        `,
        htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding: 10px; position:relative; margin: 0px"><div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;"></div><div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding: 10px;;"></div></div>
				`,
      },
    ],
  },
  {
    name: 'Four Columns',
    id: 'fourColumns',
    template: `
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
    `,
    multiple: false,
    htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div></div>
        `,
    contents: [],
  },
  {
    name: 'Five Columns',
    id: 'fiveColumns',
    template: `
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
    `,
    multiple: false,
    htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row" style="padding:10px; position:relative; margin: 0px"><div class="col-12 col-md-6 col-xl-5col ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}" style="padding:10px"></div><div class="col-12 col-md-6 col-xl-5col ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}" style="padding:10px"></div><div class="col-12 col-md-6 col-xl-5col ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}" style="padding:10px"></div><div class="col-12 col-md-6 col-xl-5col ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}" style="padding:10px"></div><div class="col-12 col-md-6 col-xl-5col ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}" style="padding:10px"></div></div>
        `,
    contents: [],
  },
  {
    name: 'Six Columns',
    id: 'sixColumns',
    template: `
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
      <div class='sidebarRow'><div style="flex: 1;"></div></div>
    `,
    multiple: false,
    htmlString: `<div class="${CONSTANTS.LOREE_IFRAME_CONTENT_ROW} row"  style="padding:10px; position:relative; margin: 0px"><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div><div class="col-lg-2 col-md-4 col-sm-12 col-xs-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}"  style="padding:10px"></div></div>
        `,
    contents: [],
  },
];

export default ROWS;
