/* eslint-disable */ // Remove this line when editing this file
import Base from './base';
import CONSTANTS from './constant';

const base_option = new Base();
let mainElement: HTMLElement;
let finalElement: HTMLElement;
let rowElement: HTMLElement;

beforeAll(() => {
  finalElement = document.createElement('div');
  rowElement = document.createElement('div');
  rowElement.className = 'row';
  mainElement = document.createElement('div');
  mainElement.className = CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT;
  rowElement.appendChild(mainElement);
  finalElement.append(rowElement);
});
afterAll(() => {
  finalElement.innerHTML = '';
});
describe('moving top', () => {
  test('Up arrow with topend', () => {
    base_option.changeSelectedElement(mainElement);
    const result = base_option.moveElementLeft();
    //expect(result).toEqual(0);
  });
  test('Up arrow with top element', () => {
    const topElement = document.createElement('div');
    topElement.className = 'row';
    finalElement.insertBefore(topElement, rowElement);

    base_option.changeSelectedElement(mainElement);
    const result = base_option.moveElementLeft();
    //expect(result).toHaveClass(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT);
  });
});
describe('moving bottom', () => {
  test('Down arrow with bottomend', () => {
    finalElement.lastChild?.remove();
    base_option.changeSelectedElement(mainElement);
    const result = base_option.moveElementRight();
    //expect(result).toEqual(0);
  });
  test('Down arrow with bottom element', () => {
    const lastElement = document.createElement('div');
    lastElement.className = 'row';
    finalElement.append(lastElement);

    base_option.changeSelectedElement(mainElement);
    const result = base_option.moveElementRight();
    //expect(result).toHaveClass(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT);
  });
});
