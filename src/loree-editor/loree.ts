import Base from './base';
import Header from './modules/header/header';
import Sidebar from './modules/sidebar/sidebar';
import Space from './modules/space/space';
import Iframe from './modules/iframe/iframe';
import { ConfigInterface } from './interface';
import CONSTANTS from './constant';
class Loree extends Base {
  id: string;
  header: Header;
  sidebar: Sidebar;
  space: Space;
  iframe: Iframe;

  constructor(id: string) {
    super();
    this.id = id;
    this.header = new Header();
    this.sidebar = new Sidebar();
    this.space = new Space();
    this.iframe = new Iframe();
    this.attachWrapper();
  }

  initiate = (config: ConfigInterface = {}): void => {
    this.header.initiate(config);
    this.sidebar.initiate(config);
    this.space.initiate();
    this.iframe.initiate();
    this.iframe.attachIframeContent(config);
    this.iframe.attachEventListeners();
    this.iframe.attachCustomFonts(config);
    this.getCustomHeaderStyle(config);
    this.updateFeaturesList(config);
  };

  attachWrapper = (): void => {
    const wrapper: HTMLElement | null = document.getElementById(this.id);
    if (wrapper) {
      const loreeWrapper = document.createElement('div');
      loreeWrapper.id = CONSTANTS.LOREE_WRAPPER;
      loreeWrapper.className = 'loree';
      wrapper.appendChild(loreeWrapper);
    }
  };
}

export default Loree;
