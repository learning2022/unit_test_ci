const CONSTANTS = {
  // Loree app update version
  LOREE_APP_VERSION: 'A2202.10',
  LOREE_BB_APP_VERSION: 'A2202.10',
  // home page course content fetch
  LOREE_PER_PAGE_ITEM: 50,
  // Quick links side bar min item
  LOREE_QUICKLINK_MIN_ITEM: 10,
  // Wrapper
  LOREE_WRAPPER: 'loree-wrapper',

  // Dynamic class name prefix
  LOREE_CLASS_NAME_PREFIX: 'loree-style-',
  LOREE_IFRAME_STYLE_WRAPPER: 'loree-iframe-style-wrapper',

  // Header
  LOREE_HEADER: 'loree-header',
  LOREE_HEADER_HOME_BUTTON: 'loree-header-home-button',
  LOREE_HEADER_TEMPLATE_BUTTON: 'loree-header-template-button',
  LOREE_HEADER_REDO_BUTTON: 'loree-header-redo-button',
  LOREE_HEADER_UNDO_BUTTON: 'loree-header-undo-button',
  LOREE_HEADER_LANGUAGE_WRAPPER: 'loree-header-language-wrapper',
  LOREE_HEADER_TITLE: 'loree-header-title',
  LOREE_HEADER_DESKTOP_BUTTON: 'loree-header-desktop-button',
  LOREE_HEADER_TABLET_BUTTON: 'loree-header-tablet-button',
  LOREE_HEADER_MOBILE_BUTTON: 'loree-header-mobile-button',
  LOREE_HEADER_PREVIEW_BUTTON: 'loree-header-preview-button',
  LOREE_HEADER_REVEAL_CODE_BUTTON: 'loree-header-reveal-code-button',
  LOREE_HEADER_OUTLINE_BUTTON: 'loree-header-outline-button',
  LOREE_HEADER_ACCESSIBILITY_BUTTON: 'loree-header-accessibility-button',
  LOREE_HEADER_ACCESSIBILITY_MODAL_WRAPPER: 'loree-header-accessibility-modal-wrapper',
  LOREE_HEADER_ACCESSIBILITY_WRAPPER: 'loree-header-accessibility-wrapper',

  // Sidebar
  LOREE_SIDEBAR: 'loree-sidebar',
  LOREE_SIDEBAR_OPEN_BUTTON: 'loree-sidebar-open-button',
  LOREE_SIDEBAR_CLOSE_BUTTON: 'loree-sidebar-close-button',
  LOREE_SUB_SIDEBAR: 'loree-sub-sidebar',
  LOREE_SIDEBAR_CONTENT_WRAPPER: 'loree-sidebar-content-wrapper',
  LOREE_SIDEBAR_ROW_SECTION: 'loree-sidebar-row-section',
  LOREE_SIDEBAR_ROW_SECTION_BUTTON: 'loree-sidebar-row-section-button',
  LOREE_SIDEBAR_ROW_COLLAPSE: 'loree-sidebar-row-collapse',
  LOREE_SIDEBAR_DESIGN_SECTION: 'loree-sidebar-design-section',
  LOREE_SIDEBAR_DESIGN_SECTION_BUTTON: 'loree-sidebar-design-section-button',
  LOREE_SIDEBAR_DESIGN_COLLAPSE: 'loree-sidebar-design-collapse',
  LOREE_SIDEBAR_DESIGN_VIDEO_THUMBNAIL_HEADER: 'loree-sidebar-design-video-thumbnail-header',
  LOREE_SIDEBAR_DESIGN_VIDEO_REPLACE_BUTTON_WRAPPER:
    'loree-sidebar-design-video-replace-button-wrapper',
  LOREE_SIDEBAR_DESIGN_VIDEO_REPLACE_BUTTON: 'loree-sidebar-design-video-replace-button',
  LOREE_SIDEBAR_DESIGN_REPLACE_BUTTON: 'loree-sidebar-design-replace-button',
  LOREE_SIDEBAR_DESIGN_EDIT_BUTTON: 'loree-sidebar-design-edit-button',
  LOREE_SIDEBAR_DESIGN_REMOVE_BUTTON: 'loree-sidebar-design-remove-button',
  LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION: 'loree-sidebar-designEditOptions',
  LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS: 'loree-sidebar-design-element-details',
  LOREE_SIDEBAR_DESIGN_SECTION_SELECTED_IMAGE_NAME:
    'loree-sidebar-design-section-selected-image-name',
  LOREE_DESIGN_SECTION_SHAPE_BLOCK_WRAPPER: 'loree-design-section-shape-block-wrapper',
  LOREE_DESIGN_SECTION_SHAPE_BLOCK_HEADING: 'loree-design-section-shape-block-heading',
  LOREE_DESIGN_SECTION_SHAPE_BLOCK_COLLAPSE: 'loree-design-section-shape-block-collapse',
  LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_WRAPPER:
    'loree-design-section-video-control-block-wrapper',
  LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_HEADING:
    'loree-design-section-video-control-block-heading',
  LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_COLLAPSE:
    'loree-design-section-video-control-block-collapse',
  LOREE_DESIGN_SECTION_VIDEO_CONTROL_AUTO_PLAY: 'loree-design-section-video-control-auto-play',
  LOREE_DESIGN_SECTION_VIDEO_CONTROL_LOOP_VIDEO: 'loree-design-section-video-control-loop-video',
  LOREE_DESIGN_SECTION_VIDEO_CONTROL_ALLOW_FULL_SCREEN:
    'loree-design-section-video-control-allow-full-screen',
  LOREE_DESIGN_SECTION_SIZE_BLOCK_WRAPPER: 'loree-design-section-size-block-wrapper',
  LOREE_DESIGN_SECTION_SIZE_BLOCK_COLLAPSE: 'loree-design-section-size-block-collapse',
  LOREE_DESIGN_SECTION_SIZE_BLOCK_HEADING: 'loree-design-section-size-block-heading',
  LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH: 'loree-design-section-size-full-width',
  LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT: 'loree-design-section-size-width-input',
  LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT: 'loree-design-section-size-height-input',
  LOREE_DESIGN_SECTION_BORDER_WIDTH_INPUT: 'loree-design-section-border-width-input',
  LOREE_DESIGN_SECTION_TRIANGLE_SHAPE_BLOCK: 'loree-design-section-triangle-shape-block',
  LOREE_DESIGN_SECTION_SQUARE_SHAPE_BLOCK: 'loree-design-section-square-shape-block',
  LOREE_DESIGN_SECTION_CIRCLE_SHAPE_BLOCK: 'loree-design-section-circle-shape-block',
  LOREE_DESIGN_SECTION_IMAGE_PREVIEW_WRAPPER: 'loree-design-section-image-preview-wrapper',
  LOREE_DESIGN_SECTION_IMAGE_VIEW: 'loree-design-section-image-view',
  LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT: 'loree-design-section-interactive-element',
  LOREE_DESIGN_SECTION_CONTAINER_ELEMENT: 'loree-design-section-container-element',
  LOREE_DESIGN_SECTION_H5P_ELEMENT: 'loree-design-section-H5P-element',
  LOREE_TABLE_DESIGN_LEFT_ALIGNMENT: 'loree-table-design-left-alignment',
  LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT: 'loree-table-design-right-alignment',
  LOREE_TABLE_DESIGN_CENTER_ALIGNMENT: 'loree-table-design-center-alignment',
  LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT: 'loree-container-design-left-alignment',
  LOREE_CONTAINER_DESIGN_RIGHT_ALIGNMENT: 'loree-container-design-right-alignment',
  LOREE_CONTAINER_DESIGN_CENTER_ALIGNMENT: 'loree-container-design-center-alignment',
  LOREE_CONTAINER_BACKGROUND_COLOR_BUTTON: 'loree-container-background-color-button',
  LOREE_IFRAME_EMBED_URL_ELEMENT: 'loree-design-section-embed-url-element',
  LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT: 'loree-design-section-external-tool-element',
  LOREE_AVTAR_MODAL: 'loree-avtar-modal',
  LOREE_MAIN_AVTAR: 'loree-main-avtar',
  LOREE_USER_PROFILE_EMAIL: 'user-profile-email',
  LOREE_USER_PROFILE_NAME: 'user-profile-name',
  LOREE_USER_PROFILE_AVTAR: 'loree-user-profile-avtar',

  // Global template block
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT: 'loree-sidebar-custom-template-search-input',
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT_VALUE:
    'loree-sidebar-custom-template-search-input-value',
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_LIST:
    'loree-sidebar-custom-global-template-category-list',
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_ICON:
    'loree-sidebar-custom-template-search-filter-icon',
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_DROPDOWN:
    'loree-sidebar-custom-global-template-category-dropdown',
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_DROPDOWN_MENU:
    'loree-sidebar-custom-global-template-category-dropdown-menu',
  LOREE_SIDEBAR_CUSTOM_TEMPLATE: 'loree-sidebar-custom-template',

  // custom block
  LOREE_SIDEBAR_CUSTOM_BLOCK_MENU_BUTTON: 'loree-sidebar-custom-block-menu-btn',

  // custom modal
  LOREE_CUSTOM_MODAL_BODY: 'loree-custom-modal-body',

  // Table section
  LOREE_SIDEBAR_TABLE_DESIGN_SECTION: 'loree-sidebar-table-design-section',
  LOREE_SIDEBAR_TABLE_DESIGN_SECTION_BUTTON: 'loree-sidebar-table-design-section-button',
  LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER:
    'loree-sidebar-table-design-section-accordion-wrapper',
  LOREE_SIDEBAR_TABLE_DESIGN_SECTION_BORDER_STYLE:
    'loree-sidebar-table-design-section-border-style',
  LOREE_SIDEBAR_TABLE_DESIGN_SECTION_COLOR_PICKERS: 'loree-table-design-color-pickers',
  LOREE_TABLE_CAPTION_ELEMENT: 'loree-table-caption-element',

  // Navigate section
  LOREE_NAVIGATION_MAIN_MENU_SECTION: 'loree-navigation-main-menu-section',
  LOREE_SIDEBAR_MENU_DESIGN_SECTION: 'loree-sidebar-menu-design-section',
  LOREE_SIDEBAR_MENU_DESIGN_SECTION_BUTTON: 'loree-sidebar-menu-design-section-button',
  LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER:
    'loree-sidebar-menu-design-section-accordion-wrapper',
  LOREE_SIDEBAR_MENU_DESIGN_SECTION_BORDER_STYLE: 'loree-sidebar-menu-design-section-border-style',
  LOREE_SIDEBAR_MENU_DESIGN_SECTION_COLOR_PICKERS: 'loree-menu-design-color-pickers',
  LOREE_NAVIGATION_IMAGE_SECTION: 'loree-navigation-image-section',
  LOREE_NAVIGATION_IMAGE_LOGO: 'loree-navigation-image-logo',

  // Container section
  LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION: 'loree-sidebar-container-design-section',
  LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER:
    'loree-sidebar-container-design-section-accordion-wrapper',
  LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_BORDER_STYLE:
    'loree-sidebar-container-design-section-border-style',
  LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS: 'loree-container-design-color-pickers',

  // Design section border block
  LOREE_DESIGN_SECTION_BORDER_BLOCK_WRAPPER: 'loree-design-section-border-block-wrapper',
  LOREE_DESIGN_SECTION_BORDER_BLOCK_COLLAPSE: 'loree-design-section-border-block-collapse',
  LOREE_DESIGN_SECTION_BORDER_BLOCK_HEADING: 'loree-design-section-border-block-heading',
  LOREE_DESIGN_SECTION_BORDER_COLOR_PICKER: 'loree-design-section-border-color-picker',
  LOREE_DESIGN_SECTION_BORDER_STYLE_INPUT: 'loree-design-section-border-style-input',
  LOREE_IMG_BORDER_COLOR_PICKER_WRAPPER: 'loree-img-border-color-picker-wrapper',
  LOREE_IMG_BORDER_COLOR_PICKER: 'loree-img-border-color-picker',

  // Colour pickers
  LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS: 'loree-design-color-pickers',
  LOREE_DESIGN_SECTION_IMAGE_BORDER_COLOR_PICKER: 'loree-design-section-image-border-color-picker',

  // Design section background block
  LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_WRAPPER: 'loree-design-section-background-block-wrapper',
  LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_COLLAPSE: 'loree-design-section-background-block-collapse',
  LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_HEADING: 'loree-design-section-background-block-heading',

  // Design alignment block
  LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_WRAPPER: 'loree-design-section-alignment-block-wrapper',
  LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_COLLAPSE: 'loree-design-section-alignment-block-collapse',
  LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_HEADING: 'loree-design-section-alignment-block-heading',
  LOREE_DESIGN_SECTION_LEFT_ALIGN_BUTTON: 'loree-design-section-left-align-button',
  LOREE_DESIGN_SECTION_RIGHT_ALIGN_BUTTON: 'loree-design-section-right-align-button',
  LOREE_DESIGN_SECTION_CENTER_ALIGN_BUTTON: 'loree-design-section-center-align-button',

  // Design section TRANSFORM Block
  LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_WRAPPER: 'loree-design-section-transform-block-wrapper',
  LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_COLLAPSE: 'loree-design-section-transform-block-collapse',
  LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_HEADING: 'loree-design-section-transform-block-heading',
  LOREE_IMAGE_HORIZONTAL_TRANSFORM: 'loree-image-horizontal-transform',
  LOREE_IMAGE_VERTICAL_TRANSFORM: 'loree-image-vertical-transform',

  // Design section space Block
  LOREE_DESIGN_SECTION_SPACE_BLOCK_WRAPPER: 'loree-design-section-space-block-wrapper',
  LOREE_DESIGN_SECTION_SPACE_BLOCK_COLLAPSE: 'loree-design-section-space-block-collapse',
  LOREE_DESIGN_SECTION_SPACE_BLOCK_HEADING: 'loree-design-section-space-block-heading',
  LOREE_SIDEBAR_ACTIVE_ROW: 'active-sidebarRow',
  LOREE_DESIGN_SECTION_SPACE_LEFT_PADDING_INPUT: 'loree-design-section-space-left-padding-input',
  LOREE_DESIGN_SECTION_SPACE_TOP_PADDING_INPUT: 'loree-design-section-space-top-padding-input',
  LOREE_DESIGN_SECTION_SPACE_BOTTOM_PADDING_INPUT:
    'loree-design-section-space-bottom-padding-input',
  LOREE_DESIGN_SECTION_SPACE_RIGHT_PADDING_INPUT: 'loree-design-section-space-right-padding-input',
  LOREE_DESIGN_SECTION_SPACE_LEFT_MARGIN_INPUT: 'loree-design-section-space-left-margin-input',
  LOREE_DESIGN_SECTION_SPACE_TOP_MARGIN_INPUT: 'loree-design-section-space-top-margin-input',
  LOREE_DESIGN_SECTION_SPACE_BOTTOM_MARGIN_INPUT: 'loree-design-section-space-bottom-margin-input',
  LOREE_DESIGN_SECTION_SPACE_RIGHT_MARGIN_INPUT: 'loree-design-section-space-right-margin-input',

  // Design section quic link
  LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_WRAPPER: 'loree-design-section-quick-link-block-wrapper',
  LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_HEADING: 'loree-design-section-quick-link-block-heading',
  LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_COLLAPSE: 'loree-design-section-quick-link-block-collapse',
  LOREE_QUICK_LINK_MODULES_COLLAPSE: 'loree-quick-link-modules-collapse',
  LOREE_QUICK_LINK_PAGES_COLLAPSE: 'loree-quick-link-pages-collapse',
  LOREE_QUICK_LINK_DISCUSSION_COLLAPSE: 'loree-quick-link-discussion-collapse',
  LOREE_QUICK_LINK_QUIZ_COLLAPSE: 'loree-quick-link-quiz-collapse',
  LOREE_QUICK_LINK_ASSIGNMENT_COLLAPSE: 'loree-quick-link-assignment-collapse',
  LOREE_QUICK_LINK_ANNOUNCEMENT_COLLAPSE: 'loree-quick-link-announcement-collapse',
  LOREE_QUICK_LINK_COURSENAVIGATION_COLLAPSE: 'loree-quick-link-coursenavigation-collapse',
  LOREE_QUICK_LINK_FILES_COLLAPSE: 'loree-quick-link-files-collapse',
  LOREE_QUICK_LINK_URL_APPEND: 'loree-quick-link-url-append',
  LOREE_QUICK_LINK_REMOVE_URL: 'loree-quick-link-remove-url',
  LOREE_D2L_QUICK_LINK_POP_UP: 'loree-d2l-quick-link-pop-up',
  LOREE_BB_QUICK_LINK_POP_UP: 'loree-bb-quick-link-pop-up',

  // Element
  LOREE_SIDEBAR_ELEMENT_SECTION: 'loree-sidebar-element-section',
  LOREE_SIDEBAR_ELEMENT_SECTION_BUTTON: 'loree-sidebar-element-section-button',
  LOREE_SIDEBAR_ELEMENT_COLLAPSE: 'loree-sidebar-element-collapse',

  // Space
  LOREE_SPACE: 'loree-space',
  LOREE_AXE_SCRIPT: 'loree-axe-script',
  LOREE_SPACE_HELP_ICON: 'loree-space-help-icon',
  LOREE_SPACE_HELP_ICON_BUTTON: 'loree-space-help-icon-button',
  LOREE_SPACE_POWEREDBY: 'loree-space-poweredby',

  // A11Y CHECKER
  LOREE_SPACE_A11Y_CHECKER: 'loree-space-a11y-checker',
  LOREE_MAIN_A11YCHECKER: 'loree-main-a11ychecker',
  LOREE_A11Y_NOTIFICATION_COUNT: 'loree-a11y-notification-count',
  LOREE_A11Y_TRAY_CLOSE_BUTTON: 'loree-a11y-tray-close-button',
  LOREE_A11Y_SEVERITY_WIDTH_DEDUCTION: 7,
  LOREE_A11Y_SEVERITY_HEIGHT_DEDUCTION: 2,
  LOREE_A11Y_SEVERITY_LEFT: 3,

  // Save Icon
  LOREE_FOOTER_SAVE_ICON_POSITION: 'save-icon-position',
  LOREE_SAVE_ICON_OPTIONS: 'loree-save-icon-options',
  LOREE_SAVE_ICON_ELEMENT: 'loree-save-icon-element',

  // Iframe
  LOREE_IFRAME: 'loree-iframe',
  LOREE_IFRAME_CONTENT_WRAPPER: 'loree-iframe-content-wrapper',
  LOREE_IFRAME_CONTENT_WRAPPER_CLASS: 'loreeContentWrapper',
  LOREE_ADD_ROW_BUTTON_WRAPPER: 'loree-add-row-button-wrapper',
  LOREE_ADD_ROW_BUTTON: 'loree-add-row-button',
  LOREE_CLOSE_ROW_BUTTON: 'loree-add-row-close-button',
  LOREE_ADD_ELEMENT_BUTTON: 'loree-add-element-button',
  LOREE_CLOSE_ADD_ELEMENT_BUTTON: 'loree-close-add-element-button',
  LOREE_INSERT_ROW_BUTTON: 'loree-insert-row-button',
  LOREE_CLOSE_INSERT_ROW_BUTTON: 'loree-close-insert-row-button',

  // Container
  LOREE_ADD_ROW_BUTTON_FOR_CONTAINER: 'loree-add-row-button-for-container',
  LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER: 'loree-close-row-button-for-container',

  LOREE_NEW_PAGE_ALERT_CONTAINER: 'loree-new-page-alert-container',

  // content classname
  LOREE_IFRAME_CONTENT_ROW: 'loree-iframe-content-row',
  LOREE_IFRAME_CONTENT_COLUMN: 'loree-iframe-content-column',
  LOREE_IFRAME_CONTENT_IMAGE_WRAPPER: 'loree-iframe-content-image-wrapper',
  LOREE_IFRAME_CONTENT_IMAGE: 'loree-iframe-content-image',
  LOREE_IFRAME_CONTENT_IMAGE_TEXT: 'loree-iframe-content-image-text',
  LOREE_IFRAME_CONTENT_ELEMENT: 'loree-iframe-content-element',
  LOREE_IFRAME_CONTENT_VIDEO_WRAPPER: 'loree-iframe-content-video-wrapper',
  LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER: 'loree-iframe-content-embed-url-wrapper',
  LOREE_IFRAME_CONTENT_VIDEO: 'loree-iframe-video-content',
  LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER: 'loree-iframe-content-divider-wrapper',
  LOREE_IFRAME_CONTENT_DIVIDER: 'loree-iframe-content-divider',
  LOREE_IFRAME_CONTENT_SPACE: 'loree-iframe-content-space',
  LOREE_IFRAME_CONTENT_TABLE_ELEMENT: 'loree-iframe-content-table-element',
  LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT: 'loree-iframe-content-custom-element',
  LOREE_IFRAME_CONTENT_TABLE_WRAPPER: 'loree-iframe-content-table-wrapper',
  LOREE_IFRAME_CONTENT_MENU: 'loree-iframe-content-menu',
  LOREE_IFRAME_CONTENT_TABLE: 'loree-iframe-content-table',
  LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER: 'loree-iframe-content-interactive-wrapper',
  LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT: 'loree-iframe-content-interactive-element',
  LOREE_IFRAME_CONTENT_H5P_WRAPPER: 'loree-iframe-content-H5P-wrapper',
  LOREE_IFRAME_CONTENT_H5P_ELEMENT: 'loree-iframe-content-H5P-element',
  LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER: 'loree-iframe-content-external-tool-wrapper',
  LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_ELEMENT: 'loree-iframe-content-external-tool-element',
  LOREE_IFRAME_CONTENT_COPY_PASTE: 'loree-iframe-content-copy-paste',
  LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER: 'loree-iframe-content-container-wrapper',
  LOREE_IFRAME_CONTENT_CONTAINER: 'loree-iframe-content-container',
  LOREE_IFRAME_CONTENT_ICON_TEXT: 'loree-iframe-content-icon-text',
  LOREE_SPECIAL_BLOCK_ICON_IMAGE_STATIC_SIZE: '20%',

  // loree no pointer
  LOREE_NO_POINTER: 'loree-no-pointer',

  // Text Highlight
  LOREE_TEXT_OPTION_ICON_HIGHLIGHT: 'text-options-icon-highlight',

  // content highlight classname
  LOREE_ROW_HIGHLIGHT: 'row-highlight',
  LOREE_COLUMN_HIGHLIGHT: 'column-highlight',
  LOREE_ELEMENT_HIGHLIGHT: 'element-highlight',

  // content hover highlight classname
  LOREE_ROW_HOVER_HIGHLIGHT: 'row-hover-highlight',
  LOREE_COLUMN_HOVER_HIGHLIGHT: 'column-hover-highlight',
  LOREE_ELEMENT_HOVER_HIGHLIGHT: 'element-hover-highlight',

  // hamburger icon
  LOREE_ROW_HAMBURGER_ICON: 'loree-iframe-row-hamburger-icon',
  LOREE_COLUMN_HAMBURGER_ICON: 'loree-iframe-column-hamburger-icon',
  LOREE_TABLE_HAMBURGER_ICON: 'loree-iframe-table-hamburger-icon',
  LOREE_CONTAINER_HAMBURGER_ICON: 'loree-iframe-container-hamburger-icon',
  LOREE_NAVIGATE_HAMBURGER_ICON: 'loree-navigate-hamburger-icon',

  // text options
  LOREE_TEXT_OPTIONS_WRAPPER: 'loree-text-options-wrapper',
  LOREE_TEXT_OPTIONS_ARROW_REFERENCE_ID: 'loree-text-options-arrow-reference-id',
  LOREE_TEXT_OPTIONS_ARROW: 'loree-text-options-arrow',
  LOREE_TEXT_OPTIONS_SPACING_BUTTON: 'loree-text-options-spacing-button',
  LOREE_TEXT_OPTIONS_SPACING_CONTENT_WRAPPER: 'loree-text-options-spacing-content-wrapper',
  LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON: 'loree-text-options-paragraph-button',
  LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER: 'loree-text-options-paragraph-content-wrapper',
  LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_BUTTON: 'loree-text-options-foreground-color-button',
  LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER:
    'loree-text-options-foreground-color-picker-wrapper',
  LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER: 'loree-text-options-foreground-color-picker',
  LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_BUTTON: 'loree-text-options-bg-color-button',
  LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER: 'loree-text-options-bg-color-picker-wrapper',
  LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER: 'loree-text-options-bg-color-picker',
  LOREE_TEXT_OPTIONS_BORDER_COLOR_BUTTON: 'loree-text-options-border-color-button',
  LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER: 'loree-text-options-border-color-picker-wrapper',
  LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER: 'loree-text-options-border-color-picker',
  LOREE_TEXT_OPTIONS_WORD_BUTTON: 'loree-text-options-word-button',
  LOREE_TEXT_OPTIONS_WORD_CONTENT_WRAPPER: 'loree-text-options-word-content-wrapper',
  LOREE_TEXT_OPTIONS_ALIGNMENT_BUTTON: 'loree-text-options-alignment-button',
  LOREE_TEXT_OPTIONS_ALIGNMENT_CONTENT_WRAPPER: 'loree-text-options-alignment-content-wrapper',
  LOREE_TEXT_OPTIONS_LINK_BUTTON: 'loree-text-options-link-button',
  LOREE_TEXT_USER_ANCHOR_LINK_INPUT: 'loree-text-user-anchor-link-input',
  LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER: 'loree-text-options-link-content-wrapper',
  LOREE_IMAGE_USER_ANCHOR_LINK_INPUT: 'loree-image-user-anchor-link-input',
  LOREE_LINK_OPTION_TAB_CHECKBOX_IMAGE: 'loree-link-option-tab-checkbox-image',
  LOREE_IMAGE_OPTIONS_LINK_CONTENT_WRAPPER: 'loree-image-options-link-content-wrapper',
  LOREE_ANCHOR_LINK_WRAPPER: 'loree-text-anchor-link-wrapper',
  LOREE_TEXT_OPTIONS_ANCHOR_ARROW: 'loree-text-options-anchor-arrow',
  LOREE_TEXT_ANCHOR_ARROW_REFERENCE_ID: 'loree-text-options-anchor-arrow-reference-id',
  LOREE_TEXT_OPTIONS_BOLD_BUTTON: 'loree-text-option-bold-button',
  LOREE_TEXT_OPTIONS_ITALIC_BUTTON: 'loree-text-option-italic-button',
  LOREE_TEXT_OPTIONS_UPPERCASE_BUTTON: 'loree-text-option-uppercase-button',
  LOREE_TEXT_OPTIONS_LOWERCASE_BUTTON: 'loree-text-option-lowercase-button',
  LOREE_TEXT_OPTIONS_TITLECASE_BUTTON: 'loree-text-option-titlecase-button',
  LOREE_TEXT_OPTIONS_UNDERLINE_BUTTON: 'loree-text-option-underline-button',
  LOREE_TEXT_OPTIONS_STRIKETHROUGH_BUTTON: 'loree-text-option-strikethrough-button',
  LOREE_TEXT_OPTIONS_SUPERSCRIPT_BUTTON: 'loree-text-option-superscript-button',
  LOREE_TEXT_OPTIONS_SUBSCRIPT_BUTTON: 'loree-text-option-subscript-button',
  LOREE_TEXT_ALIGN_LEFT_BUTTON: 'loree-text-align-text-left',
  LOREE_TEXT_ALIGN_RIGHT_BUTTON: 'loree-text-align-text-right',
  LOREE_TEXT_ALIGN_CENTER_BUTTON: 'loree-text-align-text-center',
  LOREE_TEXT_ALIGN_JUSTIFY_BUTTON: 'loree-text-align-text-justify',
  LOREE_TEXT_OPTIONS_HEADER4_BUTTON: 'loree-text-option-headerH4-button',
  LOREE_TEXT_OPTIONS_HEADER5_BUTTON: 'loree-text-option-headerH5-button',
  LOREE_TEXT_OPTIONS_HEADER6_BUTTON: 'loree-text-option-headerH6-button',
  LOREE_TEXT_OPTIONS_HEADER1_BUTTON: 'loree-text-option-headerH1-button',
  LOREE_TEXT_OPTIONS_HEADER2_BUTTON: 'loree-text-option-headerH2-button',
  LOREE_TEXT_OPTIONS_HEADER3_BUTTON: 'loree-text-option-headerH3-button',
  LOREE_TEXT_OPTIONS_NORMAL_PARAGRAPH_BUTTON: 'loree-text-option-paragraph-button',
  LOREE_TEXT_OPTIONS_LINE_HEIGHT_BUTTON: 'loree-text-line-height-option-button',
  LOREE_TEXT_OPTIONS_LINE_SPACE_BUTTON: 'loree-text-line-space-option-button',
  LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON: 'loree-text-options-unorder-list-button',
  LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON: 'loree-text-options-order-list-button',

  LOREE_SELECTED_ANCHOR_TEXT_ELEMENT: 'loree-selected-anchor-text-element',
  LOREE_EDIT_ATTACHED_USER_LINK: 'loree-edit-attached-user-link',
  LOREE_EDIT_USER_ATTACHED_LINK_ICON: 'loree-edit-user-attached-link-icon',
  LOREE_DELETE_USER_ATTACHED_LINK_ICON: 'loree-delete-user-attached-link-icon',
  // uploader
  LOREE_IMAGE_UPLOADER_INPUT: 'loree-image-uploader-input',
  LOREE_IFRAME_IMAGE_ID: 'loree-editor-image',
  LOREE_IFRAME_ICON_PRAGRAPH_ID: 'loree-icon-paragraph-option',
  LOREE_IFRAME_NAVIGATE_ID: 'loree-navigate-option',
  LOREE_IFRAME_NAVIGATE_ITEMS: 'loree-navigate-option-items',
  LOREE_MEDIA_PROGRESS_BAR: 'loree-media-progress-bar',
  LOREE_MEDIA_PROGRESS_BAR_TEXT: 'loree-media-progress-bar-text',
  LOREE_IMAGE_UPLOADER_VALIDATION_SIZE: 500000, // input value in bytes eg: 500kb - 500000
  LOREE_IMAGE_UPLOADER_VALIDATION_SIZE_RESTRICTION_BB: 5000000, // 5mb file restriction in bb
  LOREE_IMAGE_BY_URL_INPUT: 'loree-image-by-url-input',
  LOREE_IMAGE_MODAL_CONTENT_HEADER: 'loree-image-modal-content-header',
  LOREE_IMAGE_MODAL_WRAPPER_CLOSE: 'loree-image-modal-wrapper-close',
  LOREE_IMAGE_MODAL_CONTENT_DIVIDER: 'loree-image-modal-content-divider',
  LOREE_IMAGE_MODAL_CONTENT_BODY: 'loree-image-modal-content-body',
  LOREE_IMAGE_MODAL_CONTENT_BODY_LEFT_CONTAINER: 'loree-image-modal-content-body-left-container',
  LOREE_IMAGE_MODAL_NAV_LOREE_IMAGE: 'loree-image-modal-nav-loree-images',
  LOREE_IMAGE_MODAL_NAV_LMS_IMAGE: 'loree-image-modal-nav-lms-images',
  LOREE_IMAGE_MODAL_NAV_MY_IMAGE: 'loree-image-modal-nav-my-images',
  LOREE_IMAGE_MODAL_LOREE_IMAGE_CONTAINER: 'loree-image-modal-loree-image-container',
  LOREE_IMAGE_MODAL_LMS_IMAGE_CONTAINER: 'loree-image-modal-lms-image-container',
  LOREE_IMAGE_MODAL_MY_IMAGE_CONTAINER: 'loree-image-modal-my-image-container',
  LOREE_IMAGE_MODAL_FILTER_CONTAINER: 'loree-image-modal-filters-container',
  LOREE_IMAGE_MODAL_FILTER_SEARCH_BOX: 'loree-image-modal-filter-search-box',
  LOREE_IMAGE_MODAL_FILTER_SEARCH_ICON: 'loree-image-modal-filter-search-icon',
  LOREE_IMAGE_MODAL_FILTER_BUTTON: 'loree-image-modal-filter-button',
  LOREE_IMAGE_MODAL_FILTER_DROPDOWN: 'loree-image-modal-filter-dropdown',
  LOREE_IMAGE_LIST_CONTAINER: 'loree-image-list-container',
  LOREE_MY_IMAGE_LIST_CONTAINER: 'loree-my-image-list-container',
  LOREE_IMAGE_MODAL_CONTENT_BODY_MIDDLE_CONTAINER:
    'loree-image-modal-content-body-middle-container',
  LOREE_IMAGE_MODAL_CONTENT_BODY_RIGHT_CONTAINER: 'loree-image-modal-content-body-right-container',
  LOREE_IMAGE_LOCAL_UPLOAD_INPUT: 'loree-image-local-upload-input',
  LOREE_IMAGE_LOCAL_UPLOAD_INPUT_MESSAGE: 'loree-image-local-upload-input-message',
  LOREE_IMAGE_BYURL_INPUT: 'loree-image-youtube-input',
  LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY: 'loree-image-details-modal-content-body',
  LOREE_IMAGE_DETAILS_PREVIEW: 'loree-image-details-preview',
  LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT: 'loree-image-details-modal-title-input',
  LOREE_IMAGE_DETAILS_MODAL_THUMBNAIL_BLOCK: 'loree-image-details-modal-thumbnail-block',
  LOREE_IMAGE_DETAILS_THUMBNAIL_IMAGE: 'loree-image-details-thumbnail-image',
  LOREE_IMAGE_DETAILS_THUMBNAIL_LABEL: 'loree-image-details-thumbnail-label',
  LOREE_IMAGE_DETAILS_THUMBNAIL_INPUT: 'loree-image-details-thumbnail-input',
  LOREE_IMAGE_MODAL_CONTENT_FOOTER: 'loree-image-modal-content-footer',
  LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON: 'loree-image-modal-content-footer-back-button',
  LOREE_IMAGE_MODAL_CONTENT_FOOTER_CANCEL_BUTTON: 'loree-image-modal-content-footer-cancel-button',
  LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON: 'loree-image-modal-content-footer-add-button',
  LOREE_IMAGE_MODAL_CONTENT: 'loree-image-modal-content',
  LOREE_IMAGE_MODAL_BACKGROUND: 'loree-image-modal-background',
  LOREE_IMAGE_MODAL_WRAPPER: 'loree-image-modal-wrapper',
  LOREE_IMAGE_MODAL_DIALOG: 'loree-image-modal-dialog',
  LOREE_IMAGE_LOCAL_UPLOAD_INPUT_ERROR_MESSAGE: 'loree-image-local-upload-input-error-message',
  LOREE_IMAGE_LOCAL_UPLOAD_INPUT_FILESIZE_ERROR_MESSAGE:
    'loree-image-local-upload-input-filesize-error-message',
  LOREE_LMS_IMAGE_LIST_CONTAINER: 'loree-lms-image-list-container',
  LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT: 'loree-image-details-modal-alt-input',
  LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT:
    'loree-image-deatils-modal-decorative-option-input',
  LOREE_IMAGE_MODAL_LABEL_FOR_FILE_NAME: 'loree-image-modal-label-for-file-name',
  LOREE_IMAGE_MODAL_LABEL_FOR_ALT_TEXT: 'loree-image-modal-label-for-alt-text',
  LOREE_IMAGE_EDIT_MODAL: 'loree-image-edit-modal',
  LOREE_EDIT_IMAGE_STYLES_PREVIEW: 'loree-edit-image-styles-preview',
  LOREE_IMAGE_EDIT_BUTTON: 'loree-image-edit-button',
  LOREE_IMAGE_MODAL_CONTENT_FOOTER_APPLY_BUTTON: 'loree-image-modal-content-footer-apply-button',
  LOREE_IMAGE_EDIT_MODAL_CONTENT_FOOTER_BACK_BUTTON:
    'loree-image-edit-modal-content-footer-back-button',
  LOREE_EDIT_IMAGE_MODAL_HEADER: 'edit-image-modal-header',
  LOREE_IMAGE_VIEW_MODAL: 'image-view-modal',
  LOREE_SECOND_TRAY_EDIT_IMAGE: 'second-tray-edit-image',
  LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT: 'loree-image-details-modal-title-alert',
  // video uploader
  LOREE_VIDEO_UPLOADER_INPUT: 'loree-video-uploader-input',
  LOREE_IFRAME_VIDEO_ID: 'loree-editor-video',
  LOREE_VIDEO_UPLOADER_VALIDATION_SIZE: 500000000,
  // line element
  LOREE_IFRAME_LINE_ID: 'loree-editor-line',
  LOREE_IFRAME_SPACE_ID: 'loree-editor-space',

  // alert
  LOREE_DELETE_COLUMN_WRAPPER: 'loree-delete-column-wrapper',
  LOREE_DELETE_ROW_WRAPPER: 'loree-delete-row-wrapper',
  LOREE_DUPLICATE_COLUMN_WRAPPER: 'loree-duplicate-column-wrapper',
  LOREE_COLUMN_PROPS_EDIT_WRAPPER: 'loree-column-props-edit-wrapper',
  LOREE_FILE_UPLOADER_ALERT_WRAPPER: 'loree-file-uploader-alert-wrapper',
  LOREE_SUCCESS_ALERT_CONTAINER: 'loree-success-alert-container',
  LOREE_PAGE_SUCCESS_ALERT_CONTAINER: 'loree-page-success-alert-container',
  LOREE_NEW_PAGE_OPTIONS: 'loree-new-page-options-wrapper',
  LOREE_EXIT_TO_HOME_WRAPPER: 'loree-exit-to-home-wrapper',
  LOREE_CUSTOM_ELEMENTS_ALERT_CONTAINER: 'loree-custom-elements-alert-container',
  LOREE_NAVIGATION_EDIT_WRAPPER: 'loree-navigation-edit-wrapper',

  // block options
  LOREE_BLOCK_OPTIONS_WRAPPER: 'loree-block-options-wrapper',
  LOREE_BLOCK_OPTIONS_DUPLICATE_BUTTON: 'loree-block-options-duplicate-button',
  LOREE_BLOCK_OPTIONS_SAVE_BUTTON: 'loree-block-options-save-button',
  LOREE_BLOCK_OPTIONS_DELETE_BUTTON: 'loree-block-options-delete-button',
  LOREE_BLOCK_OPTIONS_COPY_BUTTON: 'loree-block-options-copy-button',
  LOREE_BLOCK_OPTIONS_PASTE_BUTTON: 'loree-block-options-paste-button',
  LOREE_BLOCK_OPTIONS_ROW_EDIT_BUTTON: 'loree-block-options-edit-row-button',
  LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON: 'loree-block-options-interactive-edit-button',
  LOREE_BLOCK_OPTIONS_COLUMN_EDIT_BUTTON: 'loree-block-options-edit-column-button',
  LOREE_BLOCK_OPTIONS_MOVE_LEFT_BUTTON: 'loree-block-options-move-left-button',
  LOREE_BLOCK_OPTIONS_MOVE_RIGHT_BUTTON: 'loree-block-options-move-right-button',
  LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON: 'loree-block-options-move-top-button',
  LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON: 'loree-block-options-move-bottom-button',
  LOREE_BLOCK_OPTIONS_EXTERNAL_LINK_BUTTON: 'loree-block-options-external-link-button',
  LOREE_LANGUAGE_ISO_LABEL_BUTTON: 'loree-language-iso-label-button',
  LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON: 'loree-block-options-language-button',
  LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER:
    'loree-block-options-tooltip-language-options-wrapper',
  LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER:
    'loree-row-column-background-color-picker-wrapper',
  LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER: 'loree-row-column-background-color-picker',

  // Row column border option
  LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER: 'loree-row-column-border-color-picker-wrapper',
  LOREE_ROW_COLUMN_BORDER_COLOR_PICKER: 'loree-row-column-border-color-picker',
  LOREE_ROW_COLUMN_BORDER_WIDTH_COLOR_PICKER: 'loree-row-column-border-width-color-picker',
  LOREE_ROW_COLUMN_BORDER_STYLE_COLOR_PICKER: 'loree-row-column-border-style-color-picker',

  // block edit options
  LOREE_BLOCK_EDIT_OPTIONS_WRAPPER: 'loree-block-edit-options-wrapper',
  LOREE_BLOCK_EDIT_OPTIONS_ARROW: 'loree-block-edit-options-arrow',
  LOREE_BLOCK_EDIT_OPTIONS_ARROW_REFERENCE_ID: 'loree-block-edit-options-arrow-reference-id',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_SECTION_WRAPPER:
    'loree-block-edit-options-alignment-section-wrapper',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_LEFT: 'marginLeft',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_RIGHT: 'marginRight',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_TOP: 'marginTop',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_BOTTOM: 'marginBottom',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_LEFT: 'paddingLeft',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_RIGHT: 'paddingRight',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_TOP: 'paddingTop',
  LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_BOTTOM: 'paddingBottom',
  LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_SECTION_WRAPPER:
    'loree-block-edit-options-color-picker-section-wrapper',
  LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_BUTTON: 'loree-block-edit-options-color-picker-button',
  LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_SECTION_WRAPPER:
    'loree-block-edit-options-border-color-picker-section-wrapper',
  LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON:
    'loree-block-edit-options-border-color-picker-button',
  LOREE_BLOCK_EDIT_OPTIONS_SECTION_DIVIDER: 'loree-block-edit-options-section-divider',
  LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON: 'loree-block-edit-options-back-button',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_PARENT: 'loree-block-edit-options-column-set-parent',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_ONE: 'one-col-set',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_TWO: 'two-col-set',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_THREE: 'three-col-set',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_FOUR: 'four-col-set',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_FIVE: 'five-col-set',
  LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_SIX: 'six-col-set',

  // block edit options two column set
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_PARENT: 'loree-block-edit-options-two-column-set-parent',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_FIFTY_FIFTY: 'fiftyFifty',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_SIXTY_FOURTY: 'sixtyFourty',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_FOURTY_SIXTY: 'fourtySixty',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_SEVENTY_THIRTY: 'seventyThirty',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_THIRTY_SEVENTY: 'thirtySeventy',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_EIGHTY_TWENTY: 'eightyTwenty',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_TWENTY_EIGHTY: 'twentyEighty',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_NINTY_TEN: 'NintyTen',
  LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_TEN_NINTY: 'tenNinty',

  // block edit options three column set
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_PARENT:
    'loree-block-edit-options-three-column-set-parent',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_THRITYTHREE_THRITYTHREE_THRITYTHREE:
    'thirtythreeThirtythreeThirtythree',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_TWENTY_SIXTY_TWENTY: 'twentySixtyTwenty',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_FOURTY_FOURTY_TWENTY: 'fourtyFourtyTwenty',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_TWENTY_FOURTY_FOURTY: 'twentyFourtyFourty',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_THIRTY_THIRTY_FOURTY: 'thirtyThirtyFourty',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_FOURTY_THIRTY_THIRTY: 'fourtyThirtyThirty',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_TEN_THIRTY_SIXTY: 'tenThirtySixty',
  LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_SIXTY_THIRTY_TEN: 'sixtyThirtyTen',

  // video modal
  LOREE_VIDEO_MODAL_BACKGROUND: 'loree-video-modal-background',
  LOREE_VIDEO_MODAL_WRAPPER: 'loree-video-modal-wrapper',
  LOREE_VIDEO_MODAL_WRAPPER_CLOSE: 'loree-video-modal-wrapper-close',
  LOREE_VIDEO_MODAL_DIALOG: 'loree-video-modal-dialog',
  LOREE_VIDEO_MODAL_CONTENT: 'loree-video-modal-content',
  LOREE_VIDEO_MODAL_CONTENT_HEADER: 'loree-video-modal-content-header',
  LOREE_VIDEO_MODAL_CONTENT_DIVIDER: 'loree-video-modal-content-divider',
  LOREE_VIDEO_MODAL_CONTENT_BODY: 'loree-video-modal-content-body',
  LOREE_VIDEO_MODAL_CONTENT_BODY_LEFT_CONTAINER: 'loree-video-modal-content-body-left-container',
  LOREE_VIDEO_MODAL_CONTENT_BODY_MIDDLE_CONTAINER:
    'loree-video-modal-content-body-middle-container',
  LOREE_VIDEO_MODAL_CONTENT_BODY_RIGHT_CONTAINER: 'loree-video-modal-content-body-right-container',
  LOREE_VIDEO_MODAL_NAV_LOREE_VIDEOS: 'loree-video-modal-nav-loree-videos',
  LOREE_VIDEO_MODAL_LOREE_VIDEO_CONTAINER: 'loree-video-modal-loree-video-container',
  LOREE_VIDEO_MODAL_FILTER_CONTAINER: 'loree-video-modal-filters-container',
  LOREE_VIDEO_MODAL_FILTER_SEARCH_BOX: 'loree-video-modal-filter-search-box',
  LOREE_VIDEO_MODAL_FILTER_SEARCH_ICON: 'loree-video-modal-filter-search-icon',
  LOREE_VIDEO_MODAL_FILTER_BUTTON: 'loree-video-modal-filter-button',
  LOREE_VIDEO_MODAL_FILTER_DROPDOWN: 'loree-video-modal-filter-dropdown',
  LOREE_VIDEO_LIST_CONTAINER: 'loree-video-list-container',
  LOREE_VIDEO_LOCAL_UPLOAD_INPUT: 'loree-video-local-upload-input',
  LOREE_VIDEO_LOCAL_UPLOAD_INPUT_MESSAGE: 'loree-video-local-upload-input-message',
  LOREE_VIDEO_YOUTUBE_INPUT: 'loree-video-youtube-input',
  LOREE_VIDEO_VIMEO_INPUT: 'loree-video-vimeo-input',
  LOREE_VIDEO_BY_URL_INPUT: 'loree-video-by-url-input',
  LOREE_VIDEO_MODAL_CONTENT_FOOTER: 'loree-video-modal-content-footer',
  LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON: 'loree-video-modal-content-footer-add-button',
  LOREE_VIDEO_MODAL_CONTENT_FOOTER_CANCEL_BUTTON: 'loree-video-modal-content-footer-cancel-button',
  LOREE_VIDEO_MODAL_CONTENT_FOOTER_BACK_BUTTON: 'loree-video-modal-content-footer-back-button',
  LOREE_VIDEO_DETAILS_MODAL_DECORATIVE_OPTION_INPUT:
    'loree-video-details-modal-decorative-option-input',
  LOREE_VIDEO_DETAILS_MODAL_ALT_INPUT: 'loree-video-details-modal-alt-input',
  LOREE_VIDEO_MODAL_LABEL_FOR_ALT_TEXT: 'loree-video-modal-label-for-alt-text',
  // Video details Modal
  LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY: 'loree-video-details-modal-content-body',
  LOREE_VIDEO_DETAILS_PREVIEW: 'loree-video-details-preview',
  LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT: 'loree-video-details-modal-title-input',
  LOREE_VIDEO_DETAILS_MODAL_THUMBNAIL_BLOCK: 'loree-video-details-modal-thumbnail-block',
  LOREE_VIDEO_DETAILS_THUMBNAIL_LABEL: 'loree-video-details-thumbnail-label',
  LOREE_VIDEO_DETAILS_THUMBNAIL_INPUT: 'loree-video-details-thumbnail-input',
  LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE_WRAPPER: 'loree-video-details-thumbnail-image-wrapper',
  LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE: 'loree-video-details-thumbnail-image',
  LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL_WRAPPER:
    'loree-video-details-thumbnail-remove-label-wrapper',
  LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL: 'loree-video-details-thumbnail-remove-label',
  LOREE_UPLOAD_PROGRESS_BLOCK: 'loree-upload-progress-block',
  LOREE_MEDIA_PROGRESS_SECTION: 'loree-media-progress-section',
  LOREE_UPLOADING_ITEM_LABEL: 'loree-uploading-item-label',
  // Banner modal
  LOREE_BANNER_IMAGE_MODAL_WRAPPER: 'loree-banner-image-modal-wrapper',
  LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION: 'loree-banner-image-modal-image-section',
  LOREE_BANNER_IMAGE_MODAL_IMAGE: 'loree-banner-image-modal-image',
  LOREE_BANNER_IMAGE_MODAL_HIGHLIGHTER: 'loree-banner-image-modal-highlighter',
  LOREE_BANNER_IMAGE_MODAL_OPTION_SECTION: 'loree-banner-image-modal-option-section',
  LOREE_BANNER_IMAGE_MODAL_OPTION_TOP_BUTTON: 'loree-banner-image-modal-option-top-button',
  LOREE_BANNER_IMAGE_MODAL_OPTION_CENTER_BUTTON: 'loree-banner-image-modal-option-center-button',
  LOREE_BANNER_IMAGE_MODAL_OPTION_BOTTOM_BUTTON: 'loree-banner-image-modal-option-bottom-button',
  LOREE_BANNER_IMAGE_MODAL_APPLY_BUTTON: 'loree-banner-image-modal-apply-button',
  LOREE_BANNER_IMAGE: 'loree-banner-image',
  LOREE_BANNER_IMAGE_IDENTITY: 'loree-banner-image-identity',
  LOREE_BANNER_IMAGEHEIGHT_VALIDATION: 256,

  // color pickr
  LOREE_COLOR_PICKR_ADD_SWATCH: 'pcr-custom-add-swatch-color',
  LOREE_COLOR_PICKR_DISPLAY_ERROR: 'pcr-display-error-section',

  // Custom block
  LOREE_CUSTOM_BLOCK_ADD_BUTTON: 'loree-custom-block-add-button',
  LOREE_SIDEBAR_CUSTOM_BLOCK_THUMBNAIL_WRAPPER: 'loree-sidebar-custom-block-thumbnail-wrapper',
  LOREE_SIDEBAR_CUSTOM_BLOCK_THUMBNAIL: 'loree-sidebar-custom-block-thumbnail',
  LOREE_SIDEBAR_CUSTOM_ELEMENTS_THUMBNAIL_WRAPPER:
    'loree-sidebar-custom-elements-thumbnail-wrapper',
  LOREE_SIDEBAR_CUSTOM_ROW_THUMBNAIL_WRAPPER: 'loree-sidebar-custom-row-thumbnail-wrapper',
  LOREE_CUSTOM_BLOCK_MODAL_WRAPPER: 'loree-custom-block-modal-wrapper',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_WRAPPER: 'loree-custom-block-delete-modal-wrapper',
  LOREE_CUSTOM_BLOCK_MODAL_CONTENT: 'loree-custom-block-modal-content',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_CONTENT: 'loree-custom-block-delete-modal-content',
  LOREE_CUSTOM_BLOCK_BACKDROP: 'loree-custom-block-backdrop',
  LOREE_CUSTOM_BLOCK_DELETE_BACKDROP: 'loree-custom-block-delete-backdrop',
  LOREE_CUSTOM_BLOCK_EDIT_BACKDROP: 'loree-custom-block-edit-backdrop',
  LOREE_INTERACTIVE_BLOCK_EDIT_BACKDROP: 'loree-interactive-block-editmodal-backdrop',
  LOREE_H5P_BLOCK_EDIT_BACKDROP: 'loree-H5P-block-editmodal-backdrop',
  LOREE_CUSTOM_BLOCK_MODAL_CONTAINER: 'loree-custom-block-modal-container',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_CONTAINER: 'loree-custom-block-delete-modal-container',
  LOREE_CUSTOM_BLOCK_MODAL_HEADER: 'loree-custom-block-modal-header',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_HEADER: 'loree-custom-block-delete-modal-header',
  LOREE_CUSTOM_BLOCK_MODAL_TITLE: 'loree-custom-block-modal-title',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_TITLE: 'loree-custom-block-delete-modal-title',
  LOREE_CUSTOM_BLOCK_CLOSE_ICON: 'loree-custom-block-close-icon',
  LOREE_CUSTOM_BLOCK_DELETE_CLOSE_ICON: 'loree-custom-block-delete-close-icon',
  LOREE_CUSTOM_BLOCK_TEMPLATE_NAME_LABEL: 'loree-custom-block-template-name-label',
  LOREE_CUSTOM_BLOCK_TEMPLATE_NAME: 'loree-custom-block-template-name',
  LOREE_CUSTOM_BLOCK_CATEGORY_NAME_LABEL: 'loree-custom-block-category-name-label',
  LOREE_CUSTOM_BLOCK_NEW_CATEGORY_GROUP: 'loree-custom-block-new-category-group',
  LOREE_INTERACTIVE_BLOCK_NEW_CATEGORY_GROUP: 'loree-interactive-block-new-category-group',
  LOREE_CUSTOM_BLOCK_CATEGORY_NAME: 'loree-custom-block-category-name',
  LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN: 'loree-custom-block-category-dropdown',
  LOREE_INTERACTIVE_BLOCK_CATEGORY_DROPDOWN: 'loree-interactive-block-category-dropdown',
  LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN_ITEM: 'loree-custom-block-category-dropdown-item',
  LOREE_INTERACTIVE_BLOCK_CATEGORY_DROPDOWN_ITEM: 'loree-interactive-block-category-dropdown-item',
  LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN_CONTAINER: 'loree-custom-block-category-dropdown-container',
  LOREE_CUSTOM_BLOCK_NEW_CATEGORY_OPTION: 'loree-custom-block-new-category-option',
  LOREE_INTERACTIVE_BLOCK_NEW_CATEGORY_OPTION: 'loree-interactive-block-new-category-option',
  LOREE_CUSTOM_BLOCK_MODAL_FOOTER: 'loree-custom-block-modal-footer',
  LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER: 'loree-interactive-block-modal-footer',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_FOOTER: 'loree-custom-block-delete-modal-footer',
  LOREE_CUSTOM_BLOCK_MODAL_FOOTER_CANCEL_BUTTON: 'loree-custom-block-modal-footer-cancel-button',
  LOREE_CUSTOM_BLOCK_DELETE_MODAL_FOOTER_CANCEL_BUTTON:
    'loree-custom-block-delete-modal-footer-cancel-button',
  LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON: 'loree-custom-block-modal-footer-save-button',
  LOREE_CUSTOM_BLOCK_MODAL_FOOTER_DELETE_BUTTON: 'loree-custom-block-modal-footer-delete-button',
  LOREE_CUSTOM_BLOCK_MODAL_FOOTER_ARCHIVE_BUTTON: 'loree-custom-block-modal-footer-archive-button',
  LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_DELETE_BUTTON:
    'loree-interactive-block-modal-footer-delete-button',
  LOREE_H5P_BLOCK_MODAL_FOOTER_DELETE_BUTTON: 'loree-H5P-block-modal-footer-delete-button',
  LOREE_CUSTOM_BLOCK_MODAL_FOOTER_EDIT_BUTTON: 'loree-custom-block-modal-footer-edit-button',
  LOREE_SIDEBAR_CUSTOM_ROW_INPUT_WRAPPER: 'loree-sidebar-custom-row-input-wrapper',
  LOREE_SIDEBAR_CUSTOM_ELEMENT_INPUT_WRAPPER: 'loree-sidebar-custom-element-input-wrapper',
  LOREE_ROWS_LINK_ACCORDION_WRAPPER: 'loree-rows-link-accordion-wrapper',
  LOREE_ELEMENTS_LINK_ACCORDION_WRAPPER: 'loree-elements-link-accordion-wrapper',
  LOREE_CUSTOM_BLOCK_ALL_CATEGORY: 'loree_custom_block_all_category',
  LOREE_CUSTOM_GLOBAL_TEMPLATE_ALL_CATEGORY: 'loree_custom_global_template_all_category',
  LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY: 'loree_custom_block_label_all_category',
  LOREE_CUSTOM_BLOCK_GLOBAL_TEMPLATE_ALL_CATEGORY:
    'loree_custom_global_template_label_all_category',
  LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_WRAPPER: 'loree-sidebar-custom-global-template-wrapper',
  LOREE_BLOCK_OPTIONS_INTERACTIVE_BUTTON: 'loree-block-options-interactive-button',
  LOREE_IFRAME_INTERACTIVE_CREATE_MODAL: 'loree-iframe-interactive-create-modal',
  LOREE_BLOCK_OPTIONS_H5P_BUTTON: 'loree-block-options-H5P-button',
  LOREE_IFRAME_H5P_CREATE_MODAL: 'loree-iframe-H5P-create-modal',
  LOREE_IFRAME_LTI_CREATE_MODAL: 'loree-iframe-LTI-create-modal',
  LOREE_TEMPLATE_BLOCK_WRAPPER: 'loree-template-block-wrapper',
  LOREE__ROWS_API_LOADER: 'sidebarRowsLoader',
  LOREE_ELEMENTS_API_LOADER: 'sidebarElementsLoader',
  LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE: 'loree-h5p-my-interactives-block-collapse',
  LOREE_H5P_SHARED_INTERACTIVES_BLOCK_COLLAPSE: 'loree-h5p-shared-interactives-block-collapse',
  LOREE_H5P_DEPARTMENT_BLOCK_COLLAPSE: 'loree-h5p-department-interactives-block-collapse',
  LOREE_H5P_INTERACTIVES_SECTION: 'loree-h5p-interactives-section',

  // view code modal
  LOREE_VIEW_CODE_MODAL_BACKGROUND: 'loree-view-code-modal-background',
  LOREE_VIEW_CODE_MODAL_WRAPPER: 'loree-view-code-modal-wrapper',
  LOREE_VIEW_CODE_MODAL_WRAPPER_CLOSE: 'loree-view-code-modal-wrapper-close',
  LOREE_VIEW_CODE_MODAL_DIALOG: 'loree-view-code-modal-dialog',
  LOREE_VIEW_CODE_MODAL_CONTENT: 'loree-view-code-modal-content',
  LOREE_VIEW_CODE_MODAL_CONTENT_HEADER: 'loree-view-code-modal-content-header',
  LOREE_VIEW_CODE_MODAL_CONTENT_DIVIDER: 'loree-view-code-modal-content-divider',
  LOREE_VIEW_CODE_MODAL_CONTENT_BODY: 'loree-view-code-modal-content-body',
  LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER: 'loree-view-code-modal-content-footer',
  LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER_BUTTON_WRAPPER:
    'loree-view-code-modal-content-footer-button-wrapper',
  LOREE_VIEW_CODE_MODAL_CONTENT_BODY_HIERARCHY_SECTION:
    'loree-view-code-modal-content-body-hierarchy-section',
  LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ELEMENT: 'loree-view-code-modal-content-body-element',
  LOREE_VIEW_CODE_MODAL_CONTENT_BODY_COLUMN: 'loree-view-code-modal-content-body-column',
  LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ROW: 'loree-view-code-modal-content-body-row',
  LOREE_VIEW_CODE_MODAL_CONTENT_BODY_PAGE: 'loree-view-code-modal-content-body-page',
  LOREE_VIEW_CODE_MODAL_EDITOR_HTML_CODE_COPY_BUTTON:
    'loree-view-code-modal-editor-html-code-copy-button',
  LOREE_VIEW_CODE_MODAL_EDITOR_CSS_CODE_COPY_BUTTON:
    'loree-view-code-modal-editor-css-code-copy-button',
  LOREE_VIEW_CODE_MODAL_EDITOR_CANCEL: 'loree-view-code-modal-editor-cancel',
  LOREE_VIEW_CODE_MODAL_EDITOR_CANCEL_UPDATE: 'loree-view-code-modal-editor-cancel-update',
  LOREE_VIEW_CODE_MODAL_EDITOR_UPDATE: 'loree-view-code-modal-editor-update',
  LOREE_VIEW_CODE_MODAL_EDITOR_EDIT: 'loree-view-code-modal-editor-edit',
  LOREE_VIEW_CODE_MODAL_HTML_CODE_EDITOR: 'loree-view-code-modal-html-code-editor',
  LOREE_VIEW_CODE_MODAL_CSS_CODE_EDITOR: 'loree-view-code-modal-css-code-editor',
  LOREE_VIEW_CODE_MODAL_HTML_CODE_TEXTAREA: 'loree-view-code-modal-html-code-textarea',
  LOREE_VIEW_CODE_MODAL_CSS_CODE_TEXTAREA: 'loree-view-code-modal-css-code-textarea',
  LOREE_VIEW_CODE_MODAL_EXPORT_BUTTON_WRAPPER: 'loree-view-code-modal-export-button-wrapper',
  LOREE_VIEW_CODE_MODAL_EXPORT_BUTTON: 'loree-view-code-modal-export-button',
  LOREE_VIEW_CODE_MODAL_DISABLED_EXPORT_BUTTON: 'loree-view-code-modal-disabled-export-button',
  LOREE_IFRAME_EXTERNAL_CANVAS_STYLE_WRAPPER: 'loree-iframe-external-canvas-style-wrapper',
  LOREE_VIEW_CODE_MODAL_EDIT_ALERT: 'loree-view-code-modal-edit-alert',
  // interactive block
  LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER:
    'loree-sidebar-interactive-elements-thumbnail-wrapper',
  LOREE_SIDEBAR_INTERACTIVE_ROW_THUMBNAIL_WRAPPER:
    'loree-sidebar-interactive-row-thumbnail-wrapper',
  LOREE_INTERACTIVE_BLOCK_MODAL_WRAPPER: 'loree-interactive-block-modal-wrapper',
  LOREE_INTERACTIVE_BLOCK_MODAL_CONTENT: 'loree-interactive-block-modal-content',
  LOREE_INTERACTIVE_BLOCK_BACKDROP: 'loree-interactive-block-backdrop',
  LOREE_INTERACTIVE_BLOCK_MODAL_CONTAINER: 'loree-interactive-block-modal-container',
  LOREE_INTERACTIVE_BLOCK_MODAL_HEADER: 'loree-interactive-block-modal-header',
  LOREE_INTERACTIVE_BLOCK_MODAL_TITLE: 'loree-interactive-block-modal-title',
  LOREE_INTERACTIVE_BLOCK_CLOSE_ICON: 'loree-interactive-block-close-icon',
  LOREE_INTERACTIVE_EDIT_BLOCK_CLOSE_ICON: 'loree-interactive-edit-block-close-icon',
  LOREE_INTERACTIVE_BLOCK_TEMPLATE_NAME_LABEL: 'loree-interactive-block-template-name-label',
  LOREE_INTERACTIVE_BLOCK_TEMPLATE_NAME: 'loree-interactive-block-template-name',
  LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_CANCEL_BUTTON:
    'loree-interactive-block-modal-footer-cancel-button',
  LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_SAVE_BUTTON:
    'loree-interactive-block-modal-footer-save-button',
  LOREE_SIDEBAR_INTERACTIVE_ROW_INPUT_WRAPPER: 'loree-sidebar-interactive-row-input-wrapper',
  LOREE_INTERACTIVE_BLOCK_ALL_CATEGORY: 'loree_interactive_block_all_category',
  LOREE_INTERACTIVE_BLOCK_LABEL_ALL_CATEGORY: 'loree_interactive_block_label_all_category',

  // h5p block
  LOREE_SIDEBAR_H5P_ELEMENTS_THUMBNAIL_WRAPPER: 'loree-sidebar-H5P-elements-thumbnail-wrapper',
  LOREE_SIDEBAR_H5P_ROW_THUMBNAIL_WRAPPER: 'loree-sidebar-H5P-row-thumbnail-wrapper',
  LOREE_H5P_BLOCK_MODAL_WRAPPER: 'loree-H5P-block-modal-wrapper',
  LOREE_H5P_BLOCK_MODAL_CONTENT: 'loree-H5P-block-modal-content',
  LOREE_H5P_BLOCK_BACKDROP: 'loree-H5P-block-backdrop',
  LOREE_H5P_BLOCK_MODAL_CONTAINER: 'loree-H5P-block-modal-container',
  LOREE_H5P_BLOCK_MODAL_HEADER: 'loree-H5P-block-modal-header',
  LOREE_H5P_BLOCK_MODAL_TITLE: 'loree-H5P-block-modal-title',
  LOREE_H5P_BLOCK_CLOSE_ICON: 'loree-H5P-block-close-icon',
  LOREE_H5P_EDIT_BLOCK_CLOSE_ICON: 'loree-H5P-edit-block-close-icon',
  LOREE_H5P_BLOCK_TEMPLATE_NAME_LABEL: 'loree-H5P-block-template-name-label',
  LOREE_H5P_BLOCK_TEMPLATE_NAME: 'loree-H5P-bk-template-name',
  LOREE_H5P_BLOCK_MODAL_FOOTER_CANCEL_BUTTON: 'loree-H5P-block-modal-footer-cancel-button',
  LOREE_H5P_BLOCK_MODAL_FOOTER_SAVE_BUTTON: 'loree-H5P-block-modal-footer-save-button',
  LOREE_SIDEBAR_H5P_ROW_INPUT_WRAPPER: 'loree-sidebar-H5P-row-input-wrapper',
  // preview modal
  LOREE_PREVIEW_MODAL_BACKGROUND: 'loree-preview-modal-background',
  LOREE_PREVIEW_MODAL_WRAPPER: 'loree-preview-modal-wrapper',
  LOREE_PREVIEW_MODAL_WRAPPER_CLOSE: 'loree-preview-modal-wrapper-close',
  LOREE_PREVIEW_MODAL_DIALOG: 'loree-preview-modal-dialog',
  LOREE_PREVIEW_MODAL_CONTENT: 'loree-preview-modal-content',
  LOREE_PREVIEW_MODAL_CONTENT_HEADER: 'loree-preview-modal-content-header',
  LOREE_PREVIEW_MODAL_CONTENT_DIVIDER: 'loree-preview-modal-content-divider',
  LOREE_PREVIEW_MODAL_CONTENT_BODY: 'loree-preview-modal-content-body',
  LOREE_PREVIEW_MODAL_MOBILE_VIEW: 'loree-preview-modal-mobile-view',
  LOREE_PREVIEW_MODAL_TABLET_VIEW: 'loree-preview-modal-tablet-view',
  LOREE_PREVIEW_MODAL_DESKTOP_VIEW: 'loree-preview-modal-desktop-view',
  // Embed URL
  LOREE_EMBED_URL_MODAL_BACKGROUND: 'loree-embed-url-modal-background',
  LOREE_EMBED_URL_MODAL_WRAPPER: 'loree-embed-url-modal-wrapper',
  LOREE_EMBED_URL_MODAL_DIALOG: 'loree-embed-url-modal-dialog',
  LOREE_EMBED_URL_MODAL_CONTENT: 'loree-embed-url-modal-content',
  LOREE_EMBED_URL_MODAL_CONTENT_HEADER: 'loree-embed-url-modal-content-header',
  LOREE_EMBED_URL_MODAL_CONTENT_DIVIDER: 'loree-embed-url-modal-content-divider',
  LOREE_EMBED_URL_MODAL_CONTENT_BODY: 'loree-embed-url-modal-content-body',
  LOREE_EMBED_URL_MODAL_CONTENT_FOOTER: 'loree-embed-url-modal-content-footer',
  LOREE_EMBED_URL_MODAL_CONTENT_FOOTER_ADD_BUTTON:
    'loree-embed-url-modal-content-footer-add-button',
  LOREE_EMBED_URL_MODAL_CONTENT_FOOTER_CANCEL_BUTTON:
    'loree-embed-url-modal-content-footer-cancel-button',
  LOREE_EMBED_URL_MODAL_WRAPPER_CLOSE: 'loree-embed-url-modal-wrapper-close',
  LOREE_EMBED_URL_IFRAME_WRAPPER: 'loree-embed-url-iframe-wrapper',
  LOREE_EMBED_URL_IFRAME_INPUT: 'loree-embed-url-iframe-input',
  LOREE_EMBED_URL_IFRAME_INFO: 'loree-embed-url-iframe-info',
  LOREE_EMBED_URL_USER_INPUT_WRAPPER: 'loree-embed-url-user-input-wrapper',
  LOREE_EMBED_URL_IFRAME_TITLE: 'loree-embed-url-iframe-title',
  LOREE_EMBED_URL_DIMENSIONS_INPUT: 'loree-embed-url-dimensions-input',
  LOREE_EMBED_URL_FULL_WIDTH: 'loree-embed-url-full-width',
  // Icon
  LOREE_IFRAME_CONTENT_ICON: 'loree-iframe-content-icon',
  // Template
  LOREE_GLOBAL_TEMPLATES_COLLAPSE: 'loree-global-templates-collapse',
  LOREE_MY_TEMPLATES_COLLAPSE: 'loree-my-templates-collapse',
  LOREE_SHARED_TEMPLATES_COLLAPSE: 'loree-shared-templates-collapse',
  LOREE_GLOBAL_ROWS_COLLAPSE: 'loree-global-rows-collapse',
  LOREE_MY_ROWS_COLLAPSE: 'loree-my-rows-collapse',
  LOREE_SHARED_ROWS_COLLAPSE: 'loree-shared-rows-collapse',
  LOREE_GLOBAL_ELEMENTS_COLLAPSE: 'loree-global-elements-collapse',
  LOREE_MY_ELEMENTS_COLLAPSE: 'loree-my-elements-collapse',
  LOREE_SHARED_ELEMENTS_COLLAPSE: 'loree-shared-elements-collapse',
  LOREE_SHARE_TEMPLATE_OPTIONS: 'loree-share-template-options',
  LOREE_SHARED_OPTIONS_LOADER: 'loree-shared-options-loader',
  LOREE_TEMPLATE_API_LOADER: 'sidebarTemplateLoader',
  LOREE_CUSTOM_BLOCK_SHARE_INPUT: 'loree-share-custom-block-input',

  // custom blocks

  LOREE_UPDATE_INFO_CONTENT: 'update-info-content-text',
  LOREE_ROW_NAME: 'loree-row-name',
  LOREE_CUSTOM_ELEMENT_SEARCH_INPUT: 'loree-sidebar-custom-elements-search-input-value',
  LOREE_CUSTOM_ROW_SEARCH_INPUT: 'loree-sidebar-custom-row-search-input-value',

  // Localisation
  LOREE_MINI_MENU_LANGUAGE_LISTS: [
    {
      language: 'Arabic',
      isoCode: 'ar',
      snippet: 'macrolanguage,Standard Arabicisarb',
    },
    {
      language: 'Bengali',
      isoCode: 'bn',
      snippet: 'also known as Bangla',
    },
    {
      language: 'Chinese',
      isoCode: 'zh',
      snippet: 'macrolanguage',
    },
    {
      language: 'Danish',
      isoCode: 'da',
      snippet: '',
    },
    {
      language: 'Dutch ',
      isoCode: 'nl',
      snippet:
        'Flemish is not to be confused with the closely relatedWest Flemishwhich is referred to asVlaams(Dutch for "Flemish") in ISO639-3and has theISO639-3codevls',
    },
    {
      language: 'English',
      isoCode: 'en',
      snippet: '',
    },
    {
      language: 'French',
      isoCode: 'fr',
      snippet: '',
    },
    {
      language: 'German',
      isoCode: 'de',
      snippet: '',
    },
    {
      language: 'Hebrew',
      isoCode: 'he',
      snippet: 'Modern Hebrew. Code changed in 1989 from original ISO639:1988,iw.[1]',
    },
    {
      language: 'Hindi',
      isoCode: 'hi',
      snippet: '',
    },
    {
      language: 'Indonesian',
      isoCode: 'id',
      snippet: 'covered by macrolanguagems/msa. Changed in 1989 from original ISO639:1988,in.[1]',
    },
    {
      language: 'Italian',
      isoCode: 'it',
      snippet: '',
    },
    {
      language: 'Japanese',
      isoCode: 'ja',
      snippet: '',
    },
    {
      language: 'Korean',
      isoCode: 'ko',
      snippet: '',
    },
    {
      language: 'Latin',
      isoCode: 'la',
      snippet: 'ancient',
    },
    {
      language: 'Malay',
      isoCode: 'ms',
      snippet: 'macrolanguage,Standard Malayiszsm,Indonesianisid/ind',
    },
    {
      language: 'Persian',
      isoCode: 'fa',
      snippet: 'macrolanguage, also known as Farsi',
    },
    {
      language: 'Portuguese',
      isoCode: 'pt',
      snippet: '',
    },
    {
      language: 'Spanish',
      isoCode: 'es',
      snippet: '',
    },
    {
      language: 'Tamil',
      isoCode: 'ta',
      snippet: '',
    },
  ],
  // set Timeout values
  LOREE_ALERT_SET_TIME_OUT: 3000,
  // auto save interval time
  LOREE_AUTO_SAVE_INTERVAL: 60000,
  // loree canvas user manual link
  LOREE_CANVAS_USER_MANUAL:
    'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/856227907/Loree+2.0+End+User+Manual',
  LOREE_CANVAS_SUPPORT_PORTAL: 'https://crystaldelta.atlassian.net/servicedesk/customer/portal/15',
  LOREE_CANVAS_WHATS_NEW:
    'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/2333671425/Release+notes+for+Loree+-+Canvas+A2202.10',
  // loree bb user manual link
  LOREE_BB_ORIGINAL_USER_MANUAL:
    'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/1841432047/Loree+-+Blackboard+Original+view+User+Manual',
  LOREE_BB_WHATS_NEW:
    'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/2333638674/Release+notes+for+Loree+Blackboard+A2202.10',
  // loree d2l user manual link
  LOREE_D2L_USER_MANUAL:
    'https://crystaldelta.atlassian.net/wiki/spaces/WPBEUD/pages/1632764030/Loree+-+D2L+User+Manual',
  // loree external CSS, JS URLs
  LOREE_CSS_URL:
    'https://wysiwyg-page-builder.s3.ap-southeast-2.amazonaws.com/Loree+CSS+and+JS/Loree_CSS.css',
  LOREE_JS_URL: 'https://loree-files.s3-ap-southeast-2.amazonaws.com/Loree-2.0.js',
  LOREE_CSS_URL_BB:
    'https://alt-60189327d4180.blackboard.com/bbcswebdav/library/Library%20Content/css/Loree.css',
  LOREE_JS_URL_BB:
    'https://alt-60189327d4180.blackboard.com/bbcswebdav/library/Library%20Content/js/Loree.js',
  LOREE_BRIGHTSPACE_JS_URL:
    'https://loree-files.s3.ap-southeast-2.amazonaws.com/Loree-brightspace.js',

  // loree-defaultcolorand Width
  Default_AppliedStyleProperty_BorderColor: 'black',
  Default_AppliedStyleProperty_BorderWidth: '0px',

  // set default editor font family
  DEFAULT_EDITOR_FONT_FAMILY: 'Source Sans Pro',
};
export default CONSTANTS;
