import React from 'react';
import ReactDOM from 'react-dom';
import Floater from '../../components/shared/Floater';
import { LinkInfo } from '../components/links/LinkInfo';
import { getPopupWrapperDiv } from '../ui-util/popups';

let removalFunction: () => void = () => {};

export function showLinkInfoPopup(
  referenceElement: HTMLElement,
  onRemove: () => void,
  url: string,
  iframe?: HTMLIFrameElement,
) {
  const div = getPopupWrapperDiv(iframe?.contentDocument ?? document);

  removalFunction = () => {
    ReactDOM.unmountComponentAtNode(div);
    div.remove();
  };

  ReactDOM.render(
    <Floater
      referenceElement={referenceElement}
      onClose={hideLinkInfoPopup}
      iframe={iframe}
      noBackground
      placement='bottom'
      arrow
      zIndex={2}
      className='loree-text-options-anchor-link'
      component={<LinkInfo onRemove={onRemove} url={url} />}
    />,
    div,
  );
}

export function hideLinkInfoPopup() {
  removalFunction();
  removalFunction = () => {};
}
