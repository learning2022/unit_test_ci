import CONSTANTS from '../constant';
import {
  getHighestChildUnderColumn,
  getURLFromAnchor,
  isColumn,
  isParentAColumn,
} from './loreeDomUtil';

describe('loreeDom Tests', () => {
  describe('isColumn', () => {
    test('tests false', () => {
      const div = document.createElement('div');
      expect(isColumn(div)).toBeFalsy();
    });

    test('tests true', () => {
      const div = document.createElement('div');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
      expect(isColumn(div)).toBeTruthy();
    });

    test('is false on null', () => {
      expect(isColumn(null)).toBeFalsy();
    });

    test('is false on non div', () => {
      const div = document.createElement('span');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
      expect(isColumn(div)).toBeFalsy();
    });
  });

  describe('isParentAColumn', () => {
    test('tests true', () => {
      const div = document.createElement('div');
      const div2 = document.createElement('div');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
      div.appendChild(div2);
      expect(isParentAColumn(div2)).toBeTruthy();
    });
    test('tests false', () => {
      const div = document.createElement('div');
      const div2 = document.createElement('div');
      div.appendChild(div2);
      expect(isParentAColumn(div2)).toBeFalsy();
    });
  });

  describe('getHighestChildUnderColumn', () => {
    test('works correctly', () => {
      const div = document.createElement('div');
      const div2 = document.createElement('div');
      const div3 = document.createElement('div');
      div.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
      div.appendChild(div2);
      div2.appendChild(div3);

      expect(getHighestChildUnderColumn(div3)).toBe(div2);
    });

    test('null if not found', () => {
      const div = document.createElement('div');
      const div2 = document.createElement('div');
      const div3 = document.createElement('div');
      div.appendChild(div2);
      div2.appendChild(div3);

      expect(getHighestChildUnderColumn(div3)).toBeNull();
    });

    test('handles null element', () => {
      expect(getHighestChildUnderColumn(null)).toBeNull();
    });
  });

  describe('getURLFromAnchor', () => {
    test('returns email address is href is mailto', async () => {
      document.body.innerHTML = '<a href="mailto:mickey@mouse.com"><img></a>';
      const anchor = document.body.querySelector('a')!;

      expect(getURLFromAnchor(anchor)).toEqual('mickey@mouse.com');
    });

    test('returns url is href is url', async () => {
      document.body.innerHTML = '<a href="http://mouse.com/"><img></a>';
      const anchor = document.body.querySelector('a')!;

      expect(getURLFromAnchor(anchor)).toEqual('http://mouse.com/');
    });
  });
});
