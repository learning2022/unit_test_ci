import React from 'react';
import { hideLinkInfoPopup, showLinkInfoPopup } from './linkInfoPopup';

jest.mock('../../components/shared/Floater', () => () => <div data-testid='1' />);
jest.mock('../components/links/LinkInfo', () => () => <div data-testid='2' />);

describe('showLinkInfoPopup', function () {
  test('show works', () => {
    document.body.innerHTML = '<div></div>';
    const div = document.body.querySelector('div')!;
    showLinkInfoPopup(div, jest.fn(), '');
    expect(document.body.innerHTML).toMatchSnapshot();
  });
  test('hide works', () => {
    document.body.innerHTML = '<div></div>';
    const div = document.body.querySelector('div')!;
    showLinkInfoPopup(div, jest.fn(), '');
    hideLinkInfoPopup();
    expect(document.body.innerHTML).toMatchSnapshot();
  });
});
