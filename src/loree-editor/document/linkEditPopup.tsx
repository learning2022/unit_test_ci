import React from 'react';
import ReactDOM from 'react-dom';
import Floater from '../../components/shared/Floater';
import LinkEditor from '../components/links/LinkEditor';
import { getPopupWrapperDiv } from '../ui-util/popups';

let removalFunction: () => void = () => {};

export function hideLinkEditPopup() {
  removalFunction();
  removalFunction = () => {};
}

export function showLinkEditPopup(
  referenceElement: HTMLElement,
  onSet: (url: string, newWindow: boolean) => void,
  onRemove: () => void,
  onClose: () => void,
  initialUrl: string,
  newWindow: boolean,
  forceNewWindow: boolean,
  iframe?: HTMLIFrameElement,
) {
  const div = getPopupWrapperDiv(iframe?.contentDocument ?? document);

  removalFunction = () => {
    ReactDOM.unmountComponentAtNode(div);
    div.remove();
  };

  const closeFunction = () => {
    onClose();
    hideLinkEditPopup();
  };

  ReactDOM.render(
    <Floater
      referenceElement={referenceElement}
      onClose={closeFunction}
      iframe={iframe}
      placement='bottom'
      arrow
      className='link-options-alignment-wrapper'
      component={
        <LinkEditor
          onChange={onSet}
          onClose={closeFunction}
          initialUrl={initialUrl}
          initialNewWindow={newWindow}
          onRemove={onRemove}
          forceNewWindow={forceNewWindow}
        />
      }
    />,
    div,
  );
}
