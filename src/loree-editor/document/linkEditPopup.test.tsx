import React from 'react';
import { hideLinkEditPopup, showLinkEditPopup } from './linkEditPopup';
// import {render} from '@testing-library/react';

jest.mock('../../components/shared/Floater', () => () => <div data-testid='1' />);
jest.mock('../components/links/LinkEditor', () => () => <div data-testid='2' />);

describe('showLinkEditPopup', function () {
  test('show works', () => {
    document.body.innerHTML = '<div></div>';
    const div = document.body.querySelector('div')!;
    showLinkEditPopup(div, jest.fn(), jest.fn(), jest.fn(), '', false, false);
    expect(document.body.innerHTML).toMatchSnapshot();
  });
  test('hide works', () => {
    document.body.innerHTML = '<div></div>';
    const div = document.body.querySelector('div')!;
    showLinkEditPopup(div, jest.fn(), jest.fn(), jest.fn(), '', false, false);
    hideLinkEditPopup();
    expect(document.body.innerHTML).toMatchSnapshot();
  });
});
