import CONSTANTS from '../constant';
import { elementHasClass } from '../common/dom';

/**
 * Navigate upwards form an Element until we get to the one just below the column div
 * @param element
 */
export function getHighestChildUnderColumn(element: HTMLElement | null): HTMLElement | null {
  let e: HTMLElement | null = element;
  while (e && !isParentAColumn(e)) {
    e = e.parentElement;
  }
  return e;
}

/**
 * True if the parent of the element is a column
 * @param element Element
 * @returns boolean
 */
export function isParentAColumn(element: HTMLElement) {
  return isColumn(element.parentElement);
}

/**
 * Is the element a column?
 * @param element
 * @returns
 */
export function isColumn(element: HTMLElement | null) {
  return (
    element &&
    elementHasClass(element, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) &&
    element.tagName === 'DIV'
  );
}

export function getURLFromAnchor(anchorElement: HTMLAnchorElement): string {
  if (anchorElement?.href?.includes('@')) {
    const mailTo = anchorElement.href.split(':');
    return mailTo[1];
  }
  return anchorElement.href;
}
