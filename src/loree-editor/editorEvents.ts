/* eslint-disable */ // Remove this line when editing this file
import Base from './base';
class EditorEvents extends Base {
  message = {
    p: {
      insertText: 'Insert Text Here',
    },
  };

  handleKeyEvents = (event: KeyboardEvent) => {
    if (event.keyCode === 13 && !event.shiftKey) this.handleEnterKeyEvent(event);
  };

  handleEnterKeyEvent = (e: KeyboardEvent) => {
    const browserAgent = navigator?.userAgent;
    if (browserAgent.indexOf('Chrome') > -1) {
      e.preventDefault();
      const selectedElement: any = this.getDocument()?.getSelection();
      if (selectedElement) {
        //event based on caret position
        this.eventBasedOnCaretPosition(selectedElement);
      }
    }
    return;
  };

  eventBasedOnCaretPosition = (selectedElement: any) => {
    const selection = selectedElement,
      range = selection.getRangeAt(0),
      br = document.createElement('br'),
      textNode = document.createTextNode('\u00a0');
    range.deleteContents();
    range.insertNode(br);
    range.collapse(false);
    range.insertNode(textNode);
    range.selectNodeContents(textNode);

    selection.removeAllRanges();
    selection.addRange(range);
    return;
  };
}

export default EditorEvents;
