/* eslint-disable @typescript-eslint/no-explicit-any */

import CONSTANTS from './constant';
import { LmsAccess } from '../utils/lmsAccess';
import { browserPromptHandler } from '../utils/saveContent';
import { translate } from '../i18n/translate';

const closeIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="7.586" height="7.587" viewBox="0 0 7.586 7.587">
<path id="close" d="M-5641.125-5116.2l-2.672-2.673-2.673,2.673a.657.657,0,0,1-.929,0,.657.657,0,0,1,0-.929l2.673-2.673-2.673-2.673a.656.656,0,0,1,0-.928.659.659,0,0,1,.929,0l2.673,2.673,2.672-2.673a.658.658,0,0,1,.928,0,.655.655,0,0,1,0,.928l-2.673,2.673,2.673,2.673a.656.656,0,0,1,0,.929.654.654,0,0,1-.464.192A.656.656,0,0,1-5641.125-5116.2Z" transform="translate(5647.591 5123.591)"/>
</svg>`;
const uploadLoading =
  '<svg xmlns="http://www.w3.org/2000/svg" width="20.867" height="20.867" viewBox="0 0 20.867 20.867"><g transform="translate(9.822)"><path class="a" d="M241.612,0A.611.611,0,0,0,241,.611V4.54a.611.611,0,0,0,1.223,0V.611A.611.611,0,0,0,241.612,0Z" transform="translate(-241.001)"/></g><g transform="translate(9.822 15.716)"><path class="b" d="M241.612,385.6a.611.611,0,0,0-.611.611v3.929a.611.611,0,1,0,1.223,0v-3.929A.611.611,0,0,0,241.612,385.6Z" transform="translate(-241.001 -385.601)"/></g><g transform="translate(4.911 1.316)"><path class="c" d="M123.6,35.993l-1.964-3.4a.611.611,0,0,0-1.059.611l1.964,3.4a.611.611,0,0,0,1.059-.611Z" transform="translate(-120.499 -32.284)"/></g><g transform="translate(12.769 14.926)"><path class="b" d="M316.405,369.935l-1.964-3.4a.611.611,0,0,0-1.059.611l1.964,3.4a.611.611,0,0,0,1.059-.611Z" transform="translate(-313.3 -366.226)"/></g><g transform="translate(1.316 4.911)"><path class="d" d="M36.606,122.543l-3.4-1.964a.611.611,0,1,0-.611,1.059l3.4,1.965a.611.611,0,0,0,.611-1.059Z" transform="translate(-32.286 -120.497)"/></g><g transform="translate(14.926 12.769)"><path class="e" d="M370.547,315.345l-3.4-1.964a.611.611,0,0,0-.611,1.059l3.4,1.964a.611.611,0,0,0,.611-1.059Z" transform="translate(-366.227 -313.299)"/></g><g transform="translate(0 9.822)"><path class="f" d="M4.54,241H.611a.611.611,0,1,0,0,1.223H4.54a.611.611,0,1,0,0-1.223Z" transform="translate(0 -241.002)"/></g><g transform="translate(15.716 9.822)"><path class="e" d="M390.142,241h-3.929a.611.611,0,0,0,0,1.223h3.929a.611.611,0,0,0,0-1.223Z" transform="translate(-385.602 -241.002)"/></g><g transform="translate(1.316 12.769)"><path class="g" d="M36.83,313.605a.611.611,0,0,0-.835-.224l-3.4,1.964A.611.611,0,1,0,33.2,316.4l3.4-1.964A.611.611,0,0,0,36.83,313.605Z" transform="translate(-32.287 -313.299)"/></g><g transform="translate(14.926 4.911)"><path class="e" d="M370.769,120.806a.611.611,0,0,0-.835-.224l-3.4,1.964a.611.611,0,1,0,.611,1.059l3.4-1.964A.611.611,0,0,0,370.769,120.806Z" transform="translate(-366.226 -120.501)"/></g><g transform="translate(4.911 14.926)"><path class="g" d="M123.38,366.308a.611.611,0,0,0-.835.224l-1.964,3.4a.611.611,0,1,0,1.059.611l1.964-3.4A.611.611,0,0,0,123.38,366.308Z" transform="translate(-120.499 -366.226)"/></g><g transform="translate(12.769 1.316)"><path class="e" d="M316.181,32.369a.611.611,0,0,0-.835.224L313.381,36a.611.611,0,1,0,1.059.611l1.964-3.4A.611.611,0,0,0,316.181,32.369Z" transform="translate(-313.299 -32.287)"/></g></svg>';
const upLoadingCancel =
  '<svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13"><path d="M0,6a6,6,0,1,1,6,6A6,6,0,0,1,0,6Zm.48,0A5.52,5.52,0,1,0,6,.481,5.526,5.526,0,0,0,.479,6ZM7.354,8.122,6,6.767,4.644,8.122a.542.542,0,0,1-.766-.766L5.233,6,3.878,4.646a.542.542,0,0,1,.766-.766L6,5.235,7.354,3.879a.542.542,0,0,1,.766.766L6.765,6,8.12,7.356a.542.542,0,1,1-.766.766Z" transform="translate(0.501 0.499)"/></svg>';
export const successAlertIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="21.002" height="21.002" viewBox="0 0 21.002 21.002">
<path id="alert" d="M17481,15919a10,10,0,1,1,10,10A10.012,10.012,0,0,1,17481,15919Zm1.484,0a8.519,8.519,0,1,0,8.516-8.521A8.526,8.526,0,0,0,17482.482,15919Zm8.367,5.459a.8.8,0,0,1-.137-.039.975.975,0,0,1-.127-.07.772.772,0,0,1-.113-.093.818.818,0,0,1-.094-.109c-.025-.044-.047-.084-.07-.132s-.029-.093-.043-.137a.771.771,0,0,1,0-.289,1.319,1.319,0,0,1,.043-.137,1.378,1.378,0,0,1,.07-.132.6.6,0,0,1,.094-.109,1.184,1.184,0,0,1,.113-.1.862.862,0,0,1,.127-.066.823.823,0,0,1,.137-.044.761.761,0,0,1,.672.207.409.409,0,0,1,.088.109.476.476,0,0,1,.07.132.637.637,0,0,1,.043.137.771.771,0,0,1,0,.289.736.736,0,0,1-.043.137.538.538,0,0,1-.07.132.5.5,0,0,1-.088.109.61.61,0,0,1-.113.093,1.357,1.357,0,0,1-.129.07.709.709,0,0,1-.141.039.79.79,0,0,1-.145.018A.812.812,0,0,1,17490.85,15924.46Zm-.584-3.637v-6.558a.74.74,0,1,1,1.48,0v6.558a.74.74,0,1,1-1.48,0Z" transform="translate(-17480.498 -15908.5)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/>
</svg>`;

export const hideDeleteColumnAlert = (): void => {
  const delColumnWrapper = document.getElementById(CONSTANTS.LOREE_DELETE_COLUMN_WRAPPER);
  if (delColumnWrapper) {
    delColumnWrapper.remove();
  }
};

export const hideDeleteRowAlert = (): void => {
  const delRowWrapper = document.getElementById(CONSTANTS.LOREE_DELETE_ROW_WRAPPER);
  if (delRowWrapper) {
    delRowWrapper.remove();
  }
};
const hideDuplicateColumnAlert = (): void => {
  const duplicateColumnWrapper = document.getElementById(CONSTANTS.LOREE_DUPLICATE_COLUMN_WRAPPER);
  if (duplicateColumnWrapper) {
    duplicateColumnWrapper.remove();
  }
};
export const columnPropsAlert = (): void => {
  const ColumnPropsWrapper = document.getElementById(CONSTANTS.LOREE_COLUMN_PROPS_EDIT_WRAPPER);
  if (ColumnPropsWrapper) {
    ColumnPropsWrapper.remove();
  }
};
const hideNewPageAlert = (): void => {
  const NewPageAlertWrapper = document.getElementById(CONSTANTS.LOREE_NEW_PAGE_ALERT_CONTAINER);
  if (NewPageAlertWrapper) {
    NewPageAlertWrapper.remove();
  }
};
const hideNewPageOptionsAlert = (): void => {
  const newPageOptionsWrapper = document.getElementById(CONSTANTS.LOREE_NEW_PAGE_OPTIONS);
  if (newPageOptionsWrapper) {
    newPageOptionsWrapper.remove();
  }
};
const hideExitToHomeAlert = (): void => {
  browserPromptHandler(true);
  const exitToHomeWrapper = document.getElementById(CONSTANTS.LOREE_EXIT_TO_HOME_WRAPPER);
  if (exitToHomeWrapper) {
    exitToHomeWrapper.remove();
  }
};
export const hideUploader = (id: string): void => {
  const LoreeMediaProgressSection = document.getElementById(
    CONSTANTS.LOREE_MEDIA_PROGRESS_SECTION,
  ) as HTMLElement;
  if (LoreeMediaProgressSection?.childElementCount > 1) {
    const currentList = document.getElementById(`${CONSTANTS.LOREE_MEDIA_PROGRESS_SECTION}_${id}`);
    if (currentList) currentList.remove();
  } else {
    const uploader = document.getElementById(CONSTANTS.LOREE_UPLOAD_PROGRESS_BLOCK);
    if (uploader) uploader.remove();
  }
  const uploadListCount = document.getElementById('upload-video-count');
  if (uploadListCount)
    uploadListCount.innerHTML = LoreeMediaProgressSection.childElementCount.toString();
};
const uploadProgressToggle = (): void => {
  const LoreeMediaProgressSection = document.getElementById(CONSTANTS.LOREE_MEDIA_PROGRESS_SECTION);
  LoreeMediaProgressSection?.classList.toggle('d-none');
  LoreeMediaProgressSection?.classList.toggle('d-flex');
  const LoreeMediaProgressCaret = document.getElementById('upload-caret-position');
  LoreeMediaProgressCaret?.classList.toggle('right-caret');
  LoreeMediaProgressCaret?.classList.toggle('down-caret');
  const LoreeUploadProgressWrapper = document.getElementById(CONSTANTS.LOREE_UPLOAD_PROGRESS_BLOCK);
  LoreeUploadProgressWrapper?.classList.toggle('col-2');
  LoreeUploadProgressWrapper?.classList.toggle('col-3');
};
export const uploadProgressPosition = (): void => {
  const loreeSideBarForProgress = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
  const LoreeUploadProgressWrapper = document.getElementById(CONSTANTS.LOREE_UPLOAD_PROGRESS_BLOCK);
  if (loreeSideBarForProgress?.style.display === 'flex') {
    LoreeUploadProgressWrapper?.classList.add('upload-popUp-margin');
  } else {
    LoreeUploadProgressWrapper?.classList.remove('upload-popUp-margin');
  }
};
export const successMessageContainerDisappear = (): void => {
  const hideSuccessAlert = document.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
  if (hideSuccessAlert) {
    setTimeout(() => {
      hideSuccessAlert.classList.add('d-none');
    }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
  }
};
export const newPageAlertContainerDisappear = (): void => {
  const hideSuccessAlert = document.getElementById(CONSTANTS.LOREE_NEW_PAGE_ALERT_CONTAINER);
  if (hideSuccessAlert) {
    setTimeout(() => {
      hideSuccessAlert.classList.add('d-none');
    }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
  }
};

export const savePageAlertContainerDisapper = (): void => {
  const alert = document.getElementById(CONSTANTS.LOREE_PAGE_SUCCESS_ALERT_CONTAINER);
  if (alert) {
    setTimeout(() => {
      alert.remove();
    }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
  }
};

export const customElementAlert = (typeOfElement: string, type: string): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const customSuccessContainer = document.createElement('div');
    customSuccessContainer.id = CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER;
    customSuccessContainer.className =
      'custom-success-container p-2 d-none mx-auto position-absolute align-items-center justify-content-between';
    if (type !== 'deleted') customSuccessContainer.style.top = '-8px';
    const customSuccessMessage = document.createElement('p');
    customSuccessMessage.className = 'mb-0 d-inline-block';
    customSuccessMessage.innerHTML = `Custom ${typeOfElement} <span class="custom-${type}">${type}</span> ${translate(
      'alert.successfully',
    )}`;
    const custSuccessIcon = document.createElement('div');
    custSuccessIcon.innerHTML = successAlertIcon;
    custSuccessIcon.className = 'd-inline-block mr-1';
    customSuccessContainer.appendChild(custSuccessIcon);
    customSuccessContainer.appendChild(customSuccessMessage);
    customSuccessContainer.style.top = '50px';
    loreeWrapper.appendChild(customSuccessContainer);
  }
};

export const customElementAlertRedirectDashboard = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const customSuccessContainer = document.createElement('div');
    customSuccessContainer.id = CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER;
    customSuccessContainer.className =
      'custom-success-container p-2 d-none mx-auto position-absolute align-items-center justify-content-between';
    const customSuccessMessage = document.createElement('p');
    customSuccessMessage.className = 'mb-0 d-inline-block';
    customSuccessMessage.innerHTML = translate('alert.redirectingtodashboard');
    const custSuccessIcon = document.createElement('div');
    custSuccessIcon.innerHTML = successAlertIcon;
    custSuccessIcon.className = 'd-inline-block mr-1';
    customSuccessContainer.appendChild(custSuccessIcon);
    customSuccessContainer.appendChild(customSuccessMessage);
    customSuccessContainer.style.top = '50px';
    customSuccessContainer.style.width = '160px';
    loreeWrapper.appendChild(customSuccessContainer);
  }
};

export const interactiveElementAlert = (
  typeOfElement: string,
  type: string,
  position: HTMLElement | string,
): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const customSuccessContainer = document.createElement('div');
    customSuccessContainer.id = CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER;
    customSuccessContainer.className =
      'custom-success-container p-2 d-none mx-auto position-absolute align-items-center justify-content-between';
    if (type !== 'delete') customSuccessContainer.style.top = '-8px';
    const customSuccessMessage = document.createElement('p');
    customSuccessMessage.className = 'mb-0 d-inline-block';
    customSuccessMessage.innerHTML = `${
      typeOfElement === translate('modal.interactive')
        ? translate('modal.interactive')
        : typeOfElement === translate('modal.sharedinteractive')
        ? translate('modal.sharedinteractive')
        : null
    } ${
      type === 'add'
        ? `<span class="custom-added">${translate('alert.added')}</span>`
        : type === 'save'
        ? `<span class="custom-saved">${translate('alert.saved')}</span>`
        : `<span class="custom-deleted">${translate('alert.deleted')}</span>`
    } succesfully`;
    const custSuccessIcon = document.createElement('div');
    custSuccessIcon.innerHTML = successAlertIcon;
    custSuccessIcon.className = 'd-inline-block mr-1';
    customSuccessContainer.appendChild(custSuccessIcon);
    customSuccessContainer.appendChild(customSuccessMessage);
    if (position === '') {
      customSuccessContainer.style.top = '50px';
      loreeWrapper.appendChild(customSuccessContainer);
    }
  }
};

export const savePageAlert = (message: string): void => {
  const alert = document.getElementById(CONSTANTS.LOREE_PAGE_SUCCESS_ALERT_CONTAINER);
  if (alert) alert.remove();
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const customSuccessContainer = document.createElement('div');
    customSuccessContainer.id = CONSTANTS.LOREE_PAGE_SUCCESS_ALERT_CONTAINER;
    customSuccessContainer.className =
      'custom-success-container p-2 d-flex mx-auto position-absolute align-items-center';
    const customSuccessMessage = document.createElement('p');
    customSuccessMessage.className = 'mb-0 ml-2 d-inline-block';
    customSuccessMessage.innerHTML = message;
    const custSuccessIcon = document.createElement('div');
    custSuccessIcon.innerHTML = successAlertIcon;
    custSuccessIcon.className = 'd-inline-block mr-1';
    customSuccessContainer.appendChild(custSuccessIcon);
    customSuccessContainer.appendChild(customSuccessMessage);
    customSuccessContainer.style.top = '50px';
    loreeWrapper.appendChild(customSuccessContainer);
  }
};

export const newPageAlertMessage = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const newPageAlertContainer = document.createElement('div');
    newPageAlertContainer.id = CONSTANTS.LOREE_NEW_PAGE_ALERT_CONTAINER;
    newPageAlertContainer.className =
      'new-page-alert-container p-2 d-none ml-auto position-absolute flex-column';
    const newPageAlertClose = document.createElement('div');
    newPageAlertClose.innerHTML = closeIcon;
    newPageAlertClose.className = 'd-flex justify-content-end new-page-alert-close';
    newPageAlertClose.onclick = (): void => hideNewPageAlert();
    const newPageAlertContent = document.createElement('div');
    newPageAlertContent.className = 'd-flex align-items-center justify-content-between';
    const customSuccessMessage = document.createElement('p');
    customSuccessMessage.className = 'mb-0 ml-2';
    customSuccessMessage.innerHTML = `<span class="new-page-created">${translate(
      'alert.newpagehasbeencreated',
    )}</span><span class="existing-page-saved">${translate(
      'alert.existingpagehasbeensavedsuccessfully',
    )}</span>`;
    const custSuccessIcon = document.createElement('div');
    custSuccessIcon.innerHTML = successAlertIcon;
    newPageAlertContainer.appendChild(newPageAlertClose);
    newPageAlertContainer.appendChild(newPageAlertContent);
    newPageAlertContent.appendChild(custSuccessIcon);
    newPageAlertContent.appendChild(customSuccessMessage);
    loreeWrapper.appendChild(newPageAlertContainer);
  }
};

export const uploadVal = (percent: number, id: string): void => {
  const progressBar = document.getElementById(`${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${id}`) as any;
  if (progressBar) {
    progressBar.style.width = `${percent}%`;
    progressBar.ariaValuenow = percent;
    const progressBarText = document.getElementById(
      `${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR_TEXT}_${id}`,
    ) as HTMLElement;
    progressBarText.innerHTML = `${percent}%`;
  }
};

export const appendProgressBarList = (id: string): void => {
  // Upload Progress Content
  const uploadProgressContentWrapper = document.getElementById(
    CONSTANTS.LOREE_MEDIA_PROGRESS_SECTION,
  ) as HTMLElement;
  const uploadProgressContent = document.createElement('ul');
  uploadProgressContent.className = 'upload-progress-content flex-column py-2 mx-2 mb-0 pl-0';
  uploadProgressContent.id = `${CONSTANTS.LOREE_MEDIA_PROGRESS_SECTION}_${id}`;
  const uploadingItem = document.createElement('li');
  uploadingItem.className = 'uploading-item mb-2';
  const uploadingItemlabel = document.createElement('label');
  uploadingItemlabel.className = 'uploading-item-label mb-0';
  uploadingItemlabel.id = `${CONSTANTS.LOREE_UPLOADING_ITEM_LABEL}_${id}`;
  uploadingItemlabel.innerHTML = translate('alert.defaulttitlenamefromlocalcomputer');
  const uploadingProgressBar = document.createElement('div');
  uploadingProgressBar.className = 'd-flex justify-content-between align-items-center';
  uploadingProgressBar.innerHTML = `<div class="progress"><div class="progress-bar" role="progress-bar" id="${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${id}" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div></div>`;
  const uploadingCancel = document.createElement('div');
  uploadingCancel.className = 'd-flex';
  uploadingCancel.id = `loree-upload-cancel_${id}`;
  uploadingCancel.innerHTML = upLoadingCancel;
  uploadingCancel.onclick = (): void => hideUploader(id);
  const uploadPercentage = document.createElement('div');
  uploadPercentage.className = 'text-right upload-percent';
  uploadPercentage.id = `${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR_TEXT}_${id}`;
  uploadPercentage.innerHTML = '0%';

  uploadingItem.appendChild(uploadingItemlabel);
  uploadingItem.appendChild(uploadingProgressBar);
  uploadingItem.appendChild(uploadPercentage);
  uploadingProgressBar.appendChild(uploadingCancel);
  uploadProgressContent.appendChild(uploadingItem);
  uploadProgressContentWrapper?.appendChild(uploadProgressContent);
  const uploadListCount = document.getElementById('upload-video-count');
  if (uploadListCount)
    uploadListCount.innerHTML = uploadProgressContentWrapper.childElementCount.toString();
};

export const uploadProgressBlock = (id: string): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);

  if (loreeWrapper) {
    const uploadProgressWrapper = document.createElement('div');
    uploadProgressWrapper.className =
      'upload-progress-block-wrapper col-2 flex-column position-absolute mx-auto upload-popUp-margin p-0';
    uploadProgressWrapper.id = CONSTANTS.LOREE_UPLOAD_PROGRESS_BLOCK;
    // Upload Progress Header
    const uploadProgressHeader = document.createElement('div');
    uploadProgressHeader.className =
      'd-flex upload-progress-header justify-content-between align-items-center';
    uploadProgressHeader.onclick = (): void => uploadProgressToggle();
    const uploadProgressHeaderleft = document.createElement('div');
    uploadProgressHeaderleft.className = 'upload-progress-header-left d-flex';
    uploadProgressHeaderleft.innerHTML = uploadLoading;
    const uploadProgressHeaderLabel = document.createElement('div');
    uploadProgressHeaderLabel.className = 'upload-progress-label font-weight-bold pl-2';
    uploadProgressHeaderLabel.innerHTML = `${translate(
      'modal.uploading',
    )} (<span id="upload-video-count">${translate('alert.1')}</span>)`;
    const uploadProgressHeaderight = document.createElement('div');
    uploadProgressHeaderight.innerHTML =
      '<i class="editor-caret right-caret" id="upload-caret-position"></i>';
    uploadProgressHeaderight.style.marginTop = '-3px';
    const uploadProgressContentWrapper = document.createElement('div');
    uploadProgressContentWrapper.id = CONSTANTS.LOREE_MEDIA_PROGRESS_SECTION;
    uploadProgressContentWrapper.className = 'd-none flex-column';

    uploadProgressHeaderleft.appendChild(uploadProgressHeaderLabel);
    uploadProgressHeader.appendChild(uploadProgressHeaderleft);
    uploadProgressHeader.appendChild(uploadProgressHeaderight);
    uploadProgressWrapper.appendChild(uploadProgressHeader);
    uploadProgressWrapper.appendChild(uploadProgressContentWrapper);
    loreeWrapper.appendChild(uploadProgressWrapper);
    appendProgressBarList(id);
  }
};

export const newPageOptionsAlert = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const editModeAlertWrapper = document.createElement('div');
    editModeAlertWrapper.className = 'editor-alert-modal';
    editModeAlertWrapper.style.display = 'none';
    editModeAlertWrapper.id = CONSTANTS.LOREE_NEW_PAGE_OPTIONS;
    const editAlertContent = document.createElement('div');
    editAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
      'modal.newpageoptions',
    )}</h5>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideNewPageOptionsAlert();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body';
    const alertBodyPlainContent = document.createElement('p');
    alertBodyPlainContent.className = 'editor-modal-statement';
    alertBodyPlainContent.innerHTML = translate('modal.newpagehasbeencreated');
    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalYes = document.createElement('button');
    modalYes.className = 'btn btn-primary mx-1';
    modalYes.innerHTML = translate('global.opennewpage');
    const modalNo = document.createElement('button');
    modalNo.className = 'mx-1 btn btn-primary';
    modalNo.innerHTML = translate('global.cancel');
    const continueWorking = document.createElement('button');
    continueWorking.className = 'mx-1 btn btn-primary';
    continueWorking.innerHTML = translate('modal.continueworkingonthecurrentpage');
    editAlertContent.appendChild(alertHeader); // header append
    editAlertContent.appendChild(alertBody);
    alertBody.appendChild(alertBodyPlainContent);
    editAlertContent.appendChild(alertFooter);
    alertFooter.appendChild(modalNo);
    alertFooter.appendChild(modalYes);
    alertFooter.appendChild(continueWorking);
    editModeAlertWrapper.appendChild(editAlertContent);
    loreeWrapper.appendChild(editModeAlertWrapper);
  }
};

export const exitToHomeAlert = (): void => {
  const lms = new LmsAccess();
  const domainName = sessionStorage.getItem('domainName')?.toLowerCase();
  const redirectUrl = lms.getAccess() ? `/lti/${domainName}/home` : '/dashboard';
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const editModeAlertWrapper = document.createElement('div');
    editModeAlertWrapper.className = 'editor-alert-modal';
    editModeAlertWrapper.style.display = 'flex';
    editModeAlertWrapper.id = CONSTANTS.LOREE_EXIT_TO_HOME_WRAPPER;
    const editAlertContent = document.createElement('div');
    editAlertContent.className = 'col-11 col-md-6 col-xl-4 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
      'modal.leavesite',
    )}</h5>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideExitToHomeAlert();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body';
    const alertBodyPlainContent = document.createElement('p');
    alertBodyPlainContent.className = 'editor-modal-statement';
    alertBodyPlainContent.innerHTML = translate('modal.areyousuretoexittheeditor');
    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalYes = document.createElement('button');
    modalYes.className = 'btn btn-primary mx-1';
    modalYes.innerHTML = translate('modal.save&exit');
    const modalNo = document.createElement('button');
    modalNo.className = 'editor-btn-primary mx-1';
    modalNo.innerHTML = translate('global.exit');
    modalNo.onclick = (e): void => {
      window.location.replace(redirectUrl);
    };
    const continueWorking = document.createElement('button');
    continueWorking.className = 'btn btn-primary mx-1 modal-footer-button';
    continueWorking.innerHTML = translate('global.cancel');
    continueWorking.onclick = (): void => hideExitToHomeAlert();
    editAlertContent.appendChild(alertHeader); // header append
    editAlertContent.appendChild(alertBody);
    alertBody.appendChild(alertBodyPlainContent);
    editAlertContent.appendChild(alertFooter);
    alertFooter.appendChild(continueWorking);
    alertFooter.appendChild(modalNo);
    editModeAlertWrapper.appendChild(editAlertContent);
    loreeWrapper.appendChild(editModeAlertWrapper);
  }
};

export const deleteColumnAlert = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const editModeAlertWrapper = document.createElement('div');
    editModeAlertWrapper.className = 'editor-alert-modal';
    editModeAlertWrapper.style.display = 'flex';
    editModeAlertWrapper.id = CONSTANTS.LOREE_DELETE_COLUMN_WRAPPER;
    const editAlertContent = document.createElement('div');
    editAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
      'global.delete',
    )}</h5>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideDeleteColumnAlert();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body';
    const radioBlock = document.createElement('div');
    radioBlock.className = 'custom-radio-group d-flex flex-column px-3';
    const radioOption1 = document.createElement('div');
    radioOption1.className = 'd-flex';
    const radioOption2 = document.createElement('div');
    radioOption2.className = 'd-flex';
    const radioInput1 = document.createElement('input');
    radioInput1.type = 'radio';
    radioInput1.checked = true;
    radioInput1.name = 'delete column';
    radioInput1.id = 'deleteColumnYes';
    const radioLabel1 = document.createElement('label');
    radioLabel1.htmlFor = 'deleteColumnYes';
    radioLabel1.innerHTML = translate('modal.deletethecolumn');
    const radioInput2 = document.createElement('input');
    radioInput2.type = 'radio';
    radioInput2.name = 'delete column';
    radioInput2.id = 'clearContentColumn';
    const radioLabel2 = document.createElement('label');
    radioLabel2.htmlFor = 'clearContentColumn';
    radioLabel2.innerHTML = translate('modal.clearthecontentinthecolumn');
    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalYes = document.createElement('button');
    modalYes.className = 'btn editor-btn-primary mx-1';
    modalYes.innerHTML = translate('global.yes');
    modalYes.id = 'deleteYesButton';
    const modalNo = document.createElement('button');
    modalNo.className = 'btn mx-1';
    modalNo.innerHTML = translate('global.no');
    modalNo.onclick = (): void => hideDeleteColumnAlert();
    editAlertContent.appendChild(alertHeader); // header append
    editAlertContent.appendChild(alertBody);
    editAlertContent.appendChild(alertFooter);
    alertBody.appendChild(radioBlock);
    radioBlock.appendChild(radioOption1);
    radioOption1.appendChild(radioInput1);
    radioOption1.appendChild(radioLabel1);
    radioBlock.appendChild(radioOption2);
    radioOption2.appendChild(radioInput2);
    radioOption2.appendChild(radioLabel2);
    alertFooter.appendChild(modalNo);
    alertFooter.appendChild(modalYes);
    editModeAlertWrapper.appendChild(editAlertContent);
    loreeWrapper.appendChild(editModeAlertWrapper);
  }
};

export const deleteRowAlert = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const editModeAlertWrapper = document.createElement('div');
    editModeAlertWrapper.className = 'editor-alert-modal';
    editModeAlertWrapper.style.display = 'flex';
    editModeAlertWrapper.id = CONSTANTS.LOREE_DELETE_ROW_WRAPPER;
    const editAlertContent = document.createElement('div');
    editAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
      'modal.deleterow',
    )}</h5>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideDeleteRowAlert();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body';
    const radioBlock = document.createElement('div');
    radioBlock.className = 'custom-radio-group d-flex flex-column px-3';
    const radioOption1 = document.createElement('div');
    radioOption1.className = 'd-flex';
    const radioOption2 = document.createElement('div');
    radioOption2.className = 'd-flex';
    const radioInput1 = document.createElement('input');
    radioInput1.type = 'radio';
    radioInput1.checked = true;
    radioInput1.name = 'delete row';
    radioInput1.id = 'deleteRowYes';
    const radioLabel1 = document.createElement('label');
    radioLabel1.htmlFor = 'deleteRowYes';
    radioLabel1.innerHTML = translate('modal.deletetherow');
    const radioInput2 = document.createElement('input');
    radioInput2.type = 'radio';
    radioInput2.name = 'delete row';
    radioInput2.id = 'clearContentRow';
    const radioLabel2 = document.createElement('label');
    radioLabel2.htmlFor = 'clearContentRow';
    radioLabel2.innerHTML = translate('modal.cleartheccontentintherow');

    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalYes = document.createElement('button');
    modalYes.className = 'btn editor-btn-primary mx-1';
    modalYes.innerHTML = translate('global.yes');
    modalYes.id = 'deleteRowYesButton';
    const modalNo = document.createElement('button');
    modalNo.className = 'btn modal-footer-button mx-1';
    modalNo.innerHTML = translate('global.no');
    modalNo.onclick = (): void => hideDeleteRowAlert();
    editAlertContent.appendChild(alertHeader); // header append
    editAlertContent.appendChild(alertBody);
    editAlertContent.appendChild(alertFooter);
    alertBody.appendChild(radioBlock);
    radioBlock.appendChild(radioOption1);
    radioOption1.appendChild(radioInput1);
    radioOption1.appendChild(radioLabel1);
    radioBlock.appendChild(radioOption2);
    radioOption2.appendChild(radioInput2);
    radioOption2.appendChild(radioLabel2);

    alertFooter.appendChild(modalNo);
    alertFooter.appendChild(modalYes);
    editModeAlertWrapper.appendChild(editAlertContent);
    loreeWrapper.appendChild(editModeAlertWrapper);
  }
};

export const duplicateColumnAlert = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const editModeAlertWrapper = document.createElement('div');
    editModeAlertWrapper.className = 'editor-alert-modal';
    editModeAlertWrapper.style.display = 'none';
    editModeAlertWrapper.id = CONSTANTS.LOREE_DUPLICATE_COLUMN_WRAPPER;
    const editAlertContent = document.createElement('div');
    editAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
      'modal.duplicatecolumn',
    )}</h5>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideDuplicateColumnAlert();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body';
    const radioBlock = document.createElement('div');
    radioBlock.className = 'custom-radio-group d-flex flex-column px-3';
    const radioOption1 = document.createElement('div');
    radioOption1.className = 'd-flex';
    const radioOption2 = document.createElement('div');
    radioOption2.className = 'd-flex';
    const radioInput1 = document.createElement('input');
    radioInput1.type = 'radio';
    radioInput1.checked = true;
    radioInput1.name = 'Clone column';
    radioInput1.id = 'removeExisting';
    const radioLabel1 = document.createElement('label');
    radioLabel1.htmlFor = 'removeExisting';
    radioLabel1.innerHTML = translate('modal.removeexistingcontentandduplicate');
    const radioInput2 = document.createElement('input');
    radioInput2.type = 'radio';
    radioInput2.name = 'Clone column';
    radioInput2.id = 'duplicateAndNew';
    const radioLabel2 = document.createElement('label');
    radioLabel2.htmlFor = 'duplicateAndNew';
    radioLabel2.innerHTML = translate('modal.duplicateandcreateanewcolumn');
    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalYes = document.createElement('button');
    modalYes.className = 'btn btn-primary mx-1 modal-footer-button';
    modalYes.innerHTML = translate('global.yes');
    const modalNo = document.createElement('button');
    modalNo.className = 'btn btn-primary mx-1';
    modalNo.innerHTML = translate('global.no');

    editAlertContent.appendChild(alertHeader); // header append
    editAlertContent.appendChild(alertBody);
    editAlertContent.appendChild(alertFooter);
    alertBody.appendChild(radioBlock);
    radioBlock.appendChild(radioOption1);
    radioOption1.appendChild(radioInput1);
    radioOption1.appendChild(radioLabel1);
    radioBlock.appendChild(radioOption2);
    radioOption2.appendChild(radioInput2);
    radioOption2.appendChild(radioLabel2);
    alertFooter.appendChild(modalYes);
    alertFooter.appendChild(modalNo);
    editModeAlertWrapper.appendChild(editAlertContent);
    loreeWrapper.appendChild(editModeAlertWrapper);
  }
};

export const ColumnPropsAlert = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const editModeAlertWrapper = document.createElement('div');
    editModeAlertWrapper.className = 'editor-alert-modal';
    editModeAlertWrapper.style.display = 'flex';
    editModeAlertWrapper.id = CONSTANTS.LOREE_COLUMN_PROPS_EDIT_WRAPPER;
    const editAlertContent = document.createElement('div');
    editAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
      'modal.columnpropeties',
    )}</h5>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => columnPropsAlert();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body';
    const radioBlock = document.createElement('div');
    radioBlock.className = 'custom-radio-group d-flex flex-column px-3';
    const radioOption1 = document.createElement('div');
    radioOption1.className = 'd-flex';
    const radioOption2 = document.createElement('div');
    radioOption2.className = 'd-flex';
    const radioInput1 = document.createElement('input');
    radioInput1.type = 'radio';
    radioInput1.checked = true;
    radioInput1.name = 'column properties edit';
    radioInput1.id = 'placeInPreviousCol';
    const radioLabel1 = document.createElement('label');
    radioLabel1.htmlFor = 'placeInPreviousCol';
    radioLabel1.innerHTML = translate('modal.placethecontentsinthepreviouscolumn');
    const radioInput2 = document.createElement('input');
    radioInput2.type = 'radio';
    radioInput2.name = 'column properties edit';
    radioInput2.id = 'deleteContentCol';
    const radioLabel2 = document.createElement('label');
    radioLabel2.htmlFor = 'deleteContentCol';
    radioLabel2.innerHTML = translate('modal.deletebbothccontentsandcolumn');
    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalYes = document.createElement('button');
    modalYes.id = 'deleteColumnProps';
    modalYes.className = 'btn editor-btn-primary mx-1';
    modalYes.innerHTML = translate('global.yes');
    const modalNo = document.createElement('button');
    modalNo.className = 'btn modal-footer-button mx-1';
    modalNo.innerHTML = translate('global.no');
    modalNo.onclick = (): void => columnPropsAlert();

    editAlertContent.appendChild(alertHeader); // header append
    editAlertContent.appendChild(alertBody);
    editAlertContent.appendChild(alertFooter);
    alertBody.appendChild(radioBlock);
    radioBlock.appendChild(radioOption1);
    radioOption1.appendChild(radioInput1);
    radioOption1.appendChild(radioLabel1);
    radioBlock.appendChild(radioOption2);
    radioOption2.appendChild(radioInput2);
    radioOption2.appendChild(radioLabel2);
    alertFooter.appendChild(modalNo);
    alertFooter.appendChild(modalYes);
    editModeAlertWrapper.appendChild(editAlertContent);
    loreeWrapper.appendChild(editModeAlertWrapper);
  }
};

export const customComponentsAlert = (message: string) => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (!loreeWrapper) return false;
  const customComponentsAlertContainer = document.createElement('div');
  customComponentsAlertContainer.id = CONSTANTS.LOREE_CUSTOM_ELEMENTS_ALERT_CONTAINER;
  customComponentsAlertContainer.className =
    'custom-element-alert-container p-2 d-flex mx-auto position-absolute align-items-center';
  customComponentsAlertContainer.style.width =
    message === 'custom row' ? '35%' : message === 'custom element' ? '37%' : '34%';
  const customComponentsAlertMessage = document.createElement('p');
  customComponentsAlertMessage.className = 'mb-0 ml-2 d-inline-block';
  customComponentsAlertMessage.innerHTML = translate(
    'alert.inordertocontinueeditingyoumusteither',
    {
      add: `<b>${translate('global.add')}</b>`,
      cancel: `<b>${translate('global.cancel')}</b>`,
      detail: message,
    },
  );
  const custSuccessIcon = document.createElement('div');
  custSuccessIcon.innerHTML = successAlertIcon;
  custSuccessIcon.className = 'd-inline-block';
  customComponentsAlertContainer.appendChild(custSuccessIcon);
  customComponentsAlertContainer.appendChild(customComponentsAlertMessage);
  loreeWrapper.appendChild(customComponentsAlertContainer);
};

export const navigationRemoveAlert = (): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (!loreeWrapper) return;
  const editModeAlertWrapper = document.createElement('div');
  editModeAlertWrapper.className = 'editor-alert-modal';
  editModeAlertWrapper.style.display = 'flex';
  editModeAlertWrapper.id = CONSTANTS.LOREE_NAVIGATION_EDIT_WRAPPER;
  const editAlertContent = document.createElement('div');
  editAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
  // Header
  const alertHeader = document.createElement('div');
  alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
  alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
    'modal.modifymenuitem',
  )}</h5>`;
  const alertCloseButton = document.createElement('div');
  alertCloseButton.className = 'btn-icon';
  alertCloseButton.innerHTML = closeIcon;
  alertCloseButton.onclick = (): void => closeMenuAlert();
  alertHeader.appendChild(alertCloseButton);
  // Body
  const alertBody = document.createElement('div');
  alertBody.className = 'modal-body';
  const messageContent = document.createElement('div');
  messageContent.innerHTML = `<span class="navbar-alert-text">${translate(
    'modal.areyousureyouwanttoreducethemenuitems',
  )}</span>`;
  // Footer
  const alertFooter = document.createElement('div');
  alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
  const modalYes = document.createElement('button');
  modalYes.id = 'deleteMenuProps';
  modalYes.className = 'btn editor-btn-primary mx-1';
  modalYes.innerHTML = translate('global.yes');
  const modalNo = document.createElement('button');
  modalNo.id = 'deleteMenuPropsStop';
  modalNo.className = 'btn modal-footer-button mx-1';
  modalNo.innerHTML = translate('global.no');

  editAlertContent.appendChild(alertHeader); // header append
  editAlertContent.appendChild(alertBody);
  editAlertContent.appendChild(alertFooter);
  alertBody.appendChild(messageContent);
  alertFooter.appendChild(modalNo);
  alertFooter.appendChild(modalYes);
  editModeAlertWrapper.appendChild(editAlertContent);
  loreeWrapper.appendChild(editModeAlertWrapper);
};

export const closeMenuAlert = (): void => {
  const ColumnPropsWrapper = document.getElementById(CONSTANTS.LOREE_NAVIGATION_EDIT_WRAPPER);
  if (!ColumnPropsWrapper) return;
  ColumnPropsWrapper.remove();
};

export const hideCustomComponentsAlert = (): void =>
  document.getElementById(CONSTANTS.LOREE_CUSTOM_ELEMENTS_ALERT_CONTAINER)?.remove();
