import CONSTANTS from './constant';
import { hideLinkEditPopup } from './document/linkEditPopup';
import STYLE_CONSTANTS from './styleConstant';
import { createPopper, Placement, State, VirtualElement } from '@popperjs/core';
import {
  customElementAlert,
  deleteColumnAlert,
  deleteRowAlert,
  duplicateColumnAlert,
  newPageAlertContainerDisappear,
  newPageAlertMessage,
  newPageOptionsAlert,
  successMessageContainerDisappear,
  uploadProgressPosition,
  hideDeleteColumnAlert,
  hideDeleteRowAlert,
  customComponentsAlert,
  hideCustomComponentsAlert,
  savePageAlert,
  savePageAlertContainerDisapper,
} from './alert';
import { LmsAccess } from '../utils/lmsAccess';
import { WordStyler } from './modules/textOptions/native-styler/wordStyler';
import {
  customBlockInfoModal,
  getEditContentDetails,
  refreshCheckBox,
  setSharedBlockStatus,
} from './modules/customBlocks/customBlockEvents';
import Design from './modules/sidebar/design';
import { videoModalIcons } from './modules/sidebar/iconHolder';
import { interactiveBlockInfoModal } from './modules/interactive';
import {
  customBlockEditStatus,
  customBlockType,
  handleAutoSaveOnCustomBlockAppend,
  isContainer,
} from '../utils/saveContent';
import { textOptionIcon } from './modules/textOptions/UI/icons';

import {
  getSelectedImageElementDetails,
  getSelectedVideoElementDetails,
  getSelectedImageElementStyle,
  getSelectedElementStyle,
  getSelectedSpaceElementStyle,
  getSelectedVideoElementStyle,
  getSelectedLineElementStyle,
} from './modules/sidebar/designStyle';

import * as baseText from './base/contentStyle';
import { Migrate } from './base/migrate';
import { ElementPicker } from './base/elementPicker';
import DashboardStyle from './base/dashboardStyles';
import EditorSelection, { getNodesInRange } from './editorSelection/selection';
import { ConfigInterface, FeatureInterface, IframePopperInstance } from './interface';
import {
  externalLinkCheckBoxCheck,
  openExternalinkOptionValuesCheck,
} from './modules/externalLinkOption';
import { a11yGlobalContext } from '../a11y-service/context/a11y-context';
import { IFrameWindow } from './a11y-client/types';
import {
  addUserLanguageAsDefaultToElement,
  getEditorElementById,
  getElementByClassName,
  hideHeaderLanguageMenu,
  isValidHost,
  selectedIndexForBorderStyle,
  setSelectedElementType,
  toggleElementVisibility,
} from './utils';
import {
  isClassListContains,
  showDelete,
  showLinkOption,
  removeClassFromElement,
  isInteractiveElement,
  showLanguageOption,
  isSelectionFirstElementInContent,
} from './base/baseUtil';
import { CustomHeader } from './modules/textOptions/UI/textOptionsData';
import { a11yUpdateHighlightsService } from '../views/editor/observerService';
import { isD2l } from '../lmsConfig';
import hideColorPickerofElementSection, {
  rgbToHex,
  validateHTMLColorHex,
} from './modules/colorPicker/utils';
import ContainerDesign from './modules/sidebar/containerDesign';
import {
  findUpwardsByTag,
  getElementById,
  getElementsByClassName,
  getIFrameElement,
  getIFrameElementById,
  isElementOneOf,
  wrapElement,
} from './common/dom';
import { setSharedTemplateStatus } from './modules/customBlocks/customTemplateEvents';
import { getHighestChildUnderColumn, getURLFromAnchor } from './document/loreeDomUtil';
import {
  disableImageLinkClick,
  getImageWrapperFromImage,
  isImageWrapper,
} from '../loree-document/imageWrapper';
import { hideLinkInfoPopup, showLinkInfoPopup } from './document/linkInfoPopup';
import { removeLinkFromElement } from '../loree-document/links';
import { translate } from '../i18n/translate';

const lms = new LmsAccess();

export let selectedElement: HTMLElement | null = null;
let copiedElement: HTMLElement | null;
let isElementCopied = false;
let selectedElementType: string | null;
let selectedElements: Array<HTMLElement> = []; // to handle multiple elements
let inlineStyleIcons: Array<HTMLElement | null> = [];
let tagNodeIcons: Array<HTMLElement | ChildNode | null> = [];
let userLineHeight = '1.5';
let userWordSpacing = '0';
let openInNewTab = false;
let anchorUrl = '';
let fetchOpenInNewTab = false;
let previousSelection: Range | null = null;
// text-options-color-picker
let userColor = '#000000';
let userBGColor = '';
let userBorderColor = '#FFFFFF';
let userBorderStyle = '';
let userTempBorderStyle = '';
let userBorderWidth = '0';
// edit-row-column-color-picker
let isRowEditOptionBlock = false;
let userRowBorderColor = '#FFFFFF';
let userRowBorderWidth = '0';
const userCustomStyle = {
  row: {
    borderStyle: 'solid',
    backgroundColor: '#FFFFFF',
  },
  column: {
    borderColor: '#FFFFFF',
    borderStyle: 'solid',
    borderWidth: '0',
    backgroundColor: '#FFFFFF',
  },
};
// popper elements
let addElementPopperInstance: {
  update: () => void;
  state?: State;
  destroy?: () => void;
  forceUpdate?: () => void;
} | null = null;
let blockEditOptionsPopperInstance: {
  update: () => void;
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
} | null = null;
let blockOptionsPopperInstance: _Any = null;
let textOptionsPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let rowHamburgerIconPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let columnHamburgerIconPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let tableHamburgerIconPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let menuHamburgerIconPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let containerHamburgerIconPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
export let foregroundColorPickerPopperInstance: IframePopperInstance | null = null;
export let textBackgroundColorPickerPopperInstance: IframePopperInstance | null = null;
export let borderColorPickerPopperInstance: IframePopperInstance | null = null;
export let backgroundColorPickerPopperInstance: IframePopperInstance | null = null;
export let rowColumnBorderColorPickerPopperInstance: IframePopperInstance | null = null;
let paragraphOptionPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let spacingOptionPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let wordOptionPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let languageOptionPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let languageIsoLabelPopperInstance: {
  update: () => void;
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
} | null = null;
let alignmentOptionPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let linkOptionPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
let anchorLinkPopperInstance: {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
} | null = null;
// design section elements
let updateTableDesignSection: (() => void) | null = null;
let updateMenuDesignSection: (() => void) | null = null;
let updateContainerDesignSection: (() => void) | null = null;
// config to get canvas show/hide features
let featuresList: FeatureInterface | null | undefined = null;
let configList: ConfigInterface | null | undefined = null;
const customHeaderStyle: _Any[string] = [];
// Undo redo
export let history: string[] = [];
export let userClickAction = 0;

class Base {
  designSec = new Design();
  wordStylerObj?: WordStyler;
  containerDesign?: ContainerDesign;
  iframeDocument?: HTMLDocument;
  migrate = new Migrate();
  eventDetails = 2;
  isSelectAll = false;
  elementPicker = new ElementPicker();
  dashboardStyle = new DashboardStyle();
  editorSelection = new EditorSelection();
  applyFontFamily = false;
  elementType: string = '';

  updateFeaturesList = (config: ConfigInterface): void => {
    configList = config;
    featuresList = config.features;
  };

  getFeatureList = () => {
    return featuresList;
  };

  getConfigList = () => {
    return configList;
  };

  changeSelectedElement = (element: HTMLElement | null): void => {
    this.cleanUp();
    selectedElement = element;
    this.showElementSectionOptions();
    this.closeLanguageOption();
    if (!selectedElement) return;
    if (!showLanguageOption(selectedElement)) {
      this.hideLanguageIsoLabel();
      return;
    }
    hideHeaderLanguageMenu();
    if (process.env.REACT_APP_ENABLE_LOCALISATION_MINI_MENU === 'true') {
      this.showLanguageIsoLabel(selectedElement);
    }
  };

  getSelectedElement = (): HTMLElement | null => {
    return selectedElement;
  };

  getPreviousSelectedElement = (): Range => {
    return previousSelection as Range;
  };

  setPreviousSelection = (value: Range | null) => {
    previousSelection = value;
  };

  getBlockOptionPopper = (): HTMLElement | null => {
    return blockOptionsPopperInstance;
  };

  removeMinHeightFromColumnAndContainer = (): void => {
    const ids = [
      CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN,
      CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER,
    ];
    ids.forEach((id) => {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const elements = iframeDocument.getElementsByClassName(id);
        Array.from(elements).forEach((element) => {
          element.classList.remove('coloumn-minHeight');
        });
      }
    });
  };

  addMinHeightToSelectedElement = (selectedElement: HTMLElement | null): void => {
    if (!selectedElement) return;
    if (
      selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) ||
      selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER)
    ) {
      selectedElement.classList.add('coloumn-minHeight');
    }
  };

  showElementSectionOptions = (): void => {
    let elementType = '';
    if (selectedElement) {
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS))
        elementType = 'contentWrapper';
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ROW))
        elementType = 'row';
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN))
        elementType = 'column';
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE))
        elementType = 'copyPaste';
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER)
      ) {
        elementType = 'container';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE)
      ) {
        elementType = 'image';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO)
      ) {
        elementType = 'video';
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER)) {
        elementType = 'embedUrl';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER)
      ) {
        elementType = 'divider';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE) ||
        selectedElement.tagName === 'TBODY' ||
        selectedElement.tagName === 'TR'
      ) {
        elementType = 'table';
      }
      if (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH') {
        elementType = 'tableData';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT)
      ) {
        elementType = 'loree-interactives';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_H5P_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_H5P_ELEMENT)
      ) {
        elementType = 'H5P';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_ELEMENT)
      ) {
        elementType = 'loree-external-tool';
      }

      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        elementType = 'element';
      }
      if (
        isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_MENU) ||
        isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)
      ) {
        elementType = 'menu';
      }
      switch (elementType) {
        case 'contentWrapper':
          this.handleSelectedContentWrapperEvent();
          break;
        case 'row':
          this.handleSelectedRowEvent();
          this.initiateCustomBlock();
          this.hideGlobalTemplate();
          break;
        case 'column':
          this.handleSelectedColumnEvent();
          this.initiateCustomBlock();
          this.hideGlobalTemplate();
          break;
        case 'image':
          this.handleSelectedImageEvent();
          break;
        case 'video':
          this.handleSelectedVideoEvent();
          break;
        case 'embedUrl':
          this.handleSelectedEmbedUrlEvent();
          break;
        case 'copyPaste':
          this.handleSelectedCopyPasteElementEvent();
          break;
        case 'container':
          this.handleSelectedContainerElementEvent();
          break;
        case 'element':
          this.handleSelectedElementEvent();
          this.initiateCustomBlock();
          this.hideGlobalTemplate();
          break;
        case 'divider':
          this.handleSelectedDividerEvent();
          break;
        case 'table':
          this.handleSelectedTableEvent();
          break;
        case 'tableData':
          this.handleSelectedTableDataEvent();
          break;
        case 'menu':
          this.handleSelectedMenuEvent();
          this.hideTextOptions();
          break;
        case 'loree-interactives':
          this.handleSelectedLoreeInteractiveEvent();
          break;
        case 'loree-external-tool':
          this.handleSelectedExternalToolEvent();
          break;
        case 'H5P':
          this.handleSelectedH5PEvent();
          break;
        case '':
          this.hideDesignSection();
          break;
      }
      setSelectedElementType(elementType);
    }
  };

  initiateCustomBlock = () => {
    let elementType = '';
    if (selectedElement?.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_ROW}`))
      elementType = 'Row';
    if (selectedElement?.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}`))
      elementType = 'Elements';
    void customBlockInfoModal(elementType, selectedElement);
    void interactiveBlockInfoModal(elementType, selectedElement);
  };

  cleanUp = (): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      this.hideBlockOptionsToSelectedElements();
      let elementType = '';
      if (selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS}`))
        elementType = 'contentWrapper';
      if (selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_ROW}`))
        elementType = 'row';
      if (selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}`))
        elementType = 'column';
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE))
        elementType = 'copyPaste';
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER)
      ) {
        elementType = 'container';
      }
      if (
        selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}`) ||
        selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE}`)
      ) {
        elementType = 'image';
      }
      if (
        selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER}`) ||
        selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO}`)
      ) {
        elementType = 'video';
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER)) {
        elementType = 'embedURL';
      }
      if (
        selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER}`) ||
        selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER}`)
      ) {
        elementType = 'divider';
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        elementType = 'element';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE) ||
        selectedElement.tagName === 'TBODY' ||
        selectedElement.tagName === 'TR'
      ) {
        elementType = 'table';
      }
      if (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH') {
        elementType = 'tableData';
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER)) {
        elementType = 'loree-interactive';
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_H5P_WRAPPER)) {
        elementType = 'H5P';
      }
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER)
      ) {
        elementType = 'loree-external-tool';
      }
      if (this.isNavigationMenuCleanUp()) {
        elementType = 'menu';
      }
      switch (elementType) {
        case 'row':
          this.hideInsertRowButton();
          this.hideCloseInsertRowButton();
          this.hideAddElementButtonToSelectedElement();
          this.hideCloseElementButtonToSelectedElement();
          this.removeHighlightSelectedRow(selectedElement);
          this.hideTextOptions();
          break;
        case 'column':
          this.hideAddElementButtonToSelectedElement();
          this.hideCloseElementButtonToSelectedElement();
          this.removeHighlightSelectedColumn(selectedElement);
          this.hideTextOptions();
          break;
        case 'contentWrapper':
          this.hideAddElementButtonToSelectedElement();
          this.hideCloseElementButtonToSelectedElement();
          this.collapseRowSection();
          this.hideCloseRowButton();
          this.showAddRowButton();
          this.hideTextOptions();
          break;
        case 'image':
          this.hideAddElementButtonToSelectedElement();
          this.removeHighlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
          this.hideTextOptions();
          break;
        case 'video':
          this.hideAddElementButtonToSelectedElement();
          this.removeHighlightSelectedElement(selectedElement);
          this.hideTextOptions();
          break;
        case 'embedURL':
          this.hideAddElementButtonToSelectedElement();
          this.removeHighlightSelectedElement(selectedElement);
          this.hideTextOptions();
          break;
        case 'copyPaste':
          this.removeContentEditableAttribute(selectedElement);
          this.hideAddElementButtonToSelectedElement();
          this.removeHighlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
          this.hideTextOptions();
          break;
        case 'container':
          this.hideAddRowButtonToSelectedContainer();
          this.removeHighlightSelectedElement(selectedElement);
          this.hideTextOptions();
          this.hideColorPickersOfContainerSection();
          break;
        case 'element':
          this.hideAddElementButtonToSelectedElement();
          this.removeHighlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
          this.hideTextOptions();
          break;
        case 'divider':
          this.hideAddElementButtonToSelectedElement();
          this.removeHighlightSelectedElement(selectedElement);
          this.hideTextOptions();
          break;
        case 'table':
          this.removeHighlightSelectedElement(selectedElement);
          this.hideTextOptions();
          break;
        case 'menu':
          this.removeHighlightSelectedElement(selectedElement);
          this.hideTextOptions();
          break;
        case 'tableData':
          this.removeHighlightSelectedElement(selectedElement);
          this.hideAddElementButtonToSelectedElement();
          this.hideTextOptions();
          break;
        case 'loree-interactive':
          this.removeHighlightSelectedElement(selectedElement);
          this.hideAddElementButtonToSelectedElement();
          this.hideTextOptions();
          break;
        case 'H5P':
          this.removeHighlightSelectedElement(selectedElement);
          this.hideAddElementButtonToSelectedElement();
          this.hideTextOptions();
          break;
        case 'loree-external-tool':
          this.removeHighlightSelectedElement(selectedElement);
          this.hideAddElementButtonToSelectedElement();
          this.hideTextOptions();
          break;
        default:
          this.hideMenuDesignSection();
      }
    }
  };

  getDesiredSelectedElement = (element: HTMLElement): HTMLElement | null => {
    return element?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)
      ? (element.closest('.row') as HTMLElement)
      : element;
  };

  handleSelectedRowEvent = () => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return false;
    if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)) {
      this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
      selectedElementType = 'ELEMENT';
      this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    } else {
      this.hideInsertRowButton();
      this.hideCloseInsertRowButton();
      this.showInsertRowButton();
      this.showRowSection();
      this.collapseRowSection();
      this.disableRowSection();
      this.hideDesignSection();
      this.hideTableDesignSection();
      this.hideMenuDesignSection();
      this.hideContainerDesignSection();
      this.hideElementSection();
      this.hideCustomElementSection();
      this.hideHamburgerIconForRow();
      this.hideHamburgerIconForColumn();
      this.hideHamburgerIconForTable();
      this.hideHamburgerIconForMenu();
      this.hideHamburgerIconForContainer();
      this.hideAddElementButtonToSelectedElement();
      this.removeImageOnDesignSection();
      this.highlightSelectedRow(selectedElement);
      selectedElementType = 'ROW';
      isRowEditOptionBlock = true;
      this.showBlockOptions(true, false, false); // row = true, column = false, element = false
    }
  };

  handleSelectedColumnEvent = (): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.hideInsertRowButton();
    this.hideCloseInsertRowButton();
    this.addMinHeightToSelectedElement(selectedElement);
    this.showAddElementButtonToSelectedElement();
    this.hideCloseRowButtonToSelectedContainer();
    this.hideDesignSection();
    this.hideTableDesignSection();
    this.hideMenuDesignSection();
    this.hideContainerDesignSection();
    // show and disable element section
    this.showElementSection();
    this.collapseElementSection();
    this.hideCustomElementSection();
    this.disableElementSection();
    this.hideRowSection();
    this.hideInteractiveElementSection();
    this.hideH5PElementSection();
    this.showItemsInSidebarHiddenForTable();
    this.hideHamburgerIconForColumn();
    this.hideHamburgerIconForTable();
    this.hideHamburgerIconForMenu();
    this.hideHamburgerIconForContainer();
    this.removeImageOnDesignSection();
    this.highlightSelectedColumn(selectedElement);
    selectedElementType = 'COLUMN';
    isRowEditOptionBlock = false;
    this.showBlockOptions(false, true, false); // row = false, column = true, element = false
  };

  handleSelectedCopyPasteElementEvent = (): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      this.addContentEditableAttribute(selectedElement);
      this.hideAddElementButtonToSelectedElement();
      this.removeImageOnDesignSection();
      const column = this.getParentColumn(selectedElement);
      if (column) {
        this.showHamburgerIconForColumn(column);
        if (column.parentElement) {
          const row = column.parentElement;
          this.showHamburgerIconForRow(row);
        }
      }
      this.showBlockOptions(false, false, true); // row = false, column = false, element = true
      this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
      this.showSidebar();
      this.hideSubSidebar();
      this.removeSelectedElementSection();
      this.disableElementSection();
      this.disableRowSection();
      this.collapseElementSection();
      this.hideTableDesignSection();
      this.hideMenuDesignSection();
      this.hideDesignSection();
      this.hideContainerDesignSection();
      // this.showDesignSection('element', selectedElement);
      // getSelectedElementStyle(selectedElement);
    }
  };

  handleSelectedContainerElementEvent = (): void => {
    let selectedContainer = this.getSelectedElement();
    if (selectedContainer) {
      if (selectedContainer.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER}`)) {
        selectedContainer = selectedContainer.parentElement as HTMLElement;
      }
    }
    selectedElement = selectedContainer;
    if (!selectedElement) return;
    this.showAddRowButtonToSelectedContainer();
    this.removeImageOnDesignSection();
    const column = this.getParentColumn(selectedElement);
    if (column) {
      this.showHamburgerIconForColumn(column);
      if (column.parentElement) {
        const row = column.parentElement;
        this.showHamburgerIconForRow(row);
      }
    }
    selectedElementType = 'ELEMENT';
    this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    this.highlightSelectedElement(
      selectedElement.id === CONSTANTS.LOREE_IFRAME_CONTENT_ICON
        ? (selectedElement.closest('.row') as HTMLElement)
        : selectedElement,
    );
    this.showSidebar();
    this.hideSubSidebar();
    this.removeSelectedElementSection();
    this.showRowSection();
    this.collapseRowSection();
    this.disableRowSection();
    this.hideElementSection();
    this.disableElementSection();
    this.collapseElementSection();
    this.hideTableDesignSection();
    this.hideMenuDesignSection();
    this.hideDesignSection();
    this.hideRowSection();
    this.showContainerDesignSection();
    if (updateContainerDesignSection) updateContainerDesignSection();
  };

  private readonly isNavigationBar = (): boolean => {
    return (
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_MENU)
    );
  };

  private readonly isNavigationBlock = (): boolean => {
    return (
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_MENU) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)
    );
  };

  private readonly isNavigationSection = (): boolean => {
    return (
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_MENU) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)
    );
  };

  private readonly isNavigationInnerSection = (): boolean => {
    return (
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)
    );
  };

  private readonly isNavigationMenuCleanUp = (): boolean => {
    return (
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_MENU) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)
    );
  };

  handleSelectedElementEvent = (): void => {
    if (selectedElement) {
      this.hideAddElementButtonToSelectedElement();
      this.removeImageOnDesignSection();

      if (this.isNavigationSection()) {
        this.highlightSelectedElement(selectedElement.closest('.row') as HTMLElement);
      } else {
        this.highlightSelectedElement(selectedElement);
      }
      selectedElement = this.elementPicker.getElement(
        selectedElement,
        'handleSelectedElementEvent',
      );
      selectedElementType = 'ELEMENT';
      this.showBlockOptions(false, false, true); // row = false, column = false, element = true
      const column = this.getParentColumn(selectedElement);
      if (column) {
        this.showHamburgerIconForColumn(column);
        if (column.parentElement) {
          const row = column.parentElement;
          if (!customBlockEditStatus) this.showHamburgerIconForRow(row);
        }
      }
      if (this.isNavigationSection()) {
        this.highlightSelectedElement(selectedElement.closest('.row') as HTMLElement);
        this.showHamburgerIconForMenu(selectedElement.closest('.row') as HTMLElement);
      } else {
        this.highlightSelectedElement(selectedElement);
      }
      this.showSidebar();
      this.hideSubSidebar();
      this.removeSelectedElementSection();
      this.disableElementSection();
      this.disableRowSection();
      this.collapseElementSection();
      this.hideTableDesignSection();
      this.hideMenuDesignSection();
      this.hideContainerDesignSection();
      this.expandDesignSection();
      this.showDesignSection('element', selectedElement);
      getSelectedElementStyle(selectedElement);
    }
  };

  getParentColumn = (element: HTMLElement): HTMLElement | undefined => {
    const parent = element?.parentElement as HTMLElement;
    if (!parent) return;
    if (parent.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) {
      return parent;
    }
    return this.getParentColumn(parent);
  };

  handleSelectedDividerEvent = (): void => {
    let selectedDividerElement = this.getSelectedElement();
    if (selectedDividerElement) {
      let element = 'divider';
      if (
        selectedDividerElement.classList.contains(
          `${CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER}`,
        )
      ) {
        element = 'wrapper';
      }
      if (element === 'divider') {
        selectedDividerElement = selectedDividerElement.parentElement as HTMLElement;
      }
      selectedElement = selectedDividerElement;
      this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
      this.showSidebar();
      this.hideSubSidebar();
      this.hideTableDesignSection();
      this.hideMenuDesignSection();
      this.hideContainerDesignSection();
      this.removeSelectedElementSection();
      this.disableElementSection();
      this.collapseElementSection();
      this.expandDesignSection();
      const column = this.getParentColumn(selectedElement);
      if (column) {
        this.showHamburgerIconForColumn(column);
        if (column.parentElement) {
          const row = column.parentElement;
          if (customBlockType !== 'Elements') this.showHamburgerIconForRow(row);
        }
      }
      const selectedLine = selectedElement.getElementsByTagName('hr')[0];
      const selectedSpace = selectedElement.getElementsByClassName(
        CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER,
      )[0] as HTMLDivElement;
      if (selectedLine) {
        this.showDesignSection('line', selectedElement);
        getSelectedLineElementStyle(selectedLine);
      } else {
        if (selectedSpace) {
          this.showDesignSection('space', selectedElement);
          getSelectedSpaceElementStyle(selectedSpace);
        }
      }
      selectedElementType = 'ELEMENT';
      this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    }
  };

  updateTableSectionUpdateMethod = (fn: (() => void) | null): void => {
    if (fn) {
      updateTableDesignSection = fn;
    }
  };

  updateMenuSectionUpdateMethod = (fn: (() => void) | null): void => {
    if (fn) {
      updateMenuDesignSection = fn;
    }
  };

  updateContainerSectionUpdateMethod = (fn: (() => void) | null): void => {
    if (fn) {
      updateContainerDesignSection = fn;
    }
  };

  handleSuperKeysClick = (meta = false, control = false, shift = false): void => {
    if (meta || control) {
      const selectedElement = this.getSelectedElement();
      if (selectedElement && selectedElement.tagName === 'TD') {
        this.handleClickWithMeta();
        return;
      }
    }
    if (shift) {
      const selectedElement = this.getSelectedElement();
      if (selectedElement && selectedElement.tagName === 'TD') {
        this.handleClickWithShift(selectedElement);
        return;
      }
    }
    if (!meta && !control && !shift) {
      this.handleNotSuperKeysClick();
    }
  };

  handleClickWithShift = (element: HTMLElement): void => {
    if (selectedElements.length >= 1) {
      this.hideAddElementButtonToSelectedElement();

      selectedElements.forEach((element) => {
        this.removeHighlightSelectedElement(element);
      });

      selectedElements = selectedElements.slice(0, 1);
      this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElements[0]));

      const originColumnElement = selectedElements[0] as HTMLTableCellElement;
      const originParentElement = selectedElements[0].parentElement as HTMLTableRowElement;
      const originRowIndex = originParentElement.rowIndex;
      const originColumnIndex = originColumnElement.cellIndex;

      const selectedColumnElement = element as HTMLTableCellElement;
      const selectedParentElement = element.parentElement as HTMLTableRowElement;
      const selectedRowIndex = selectedParentElement.rowIndex;
      const selectedColumnIndex = selectedColumnElement.cellIndex;

      let startRow = originRowIndex;
      let startColumn = originColumnIndex;
      let endRow = selectedRowIndex;
      let endColumn = selectedColumnIndex;

      if (endRow < startRow) {
        startRow = selectedRowIndex;
        endRow = originRowIndex;
      }
      if (endColumn < startColumn) {
        startColumn = selectedColumnIndex;
        endColumn = originColumnIndex;
      }

      const tableBody = this.elementPicker.getElement(element, 'handleClickWithShift');
      for (let i = startRow; i <= endRow; i++) {
        const row = tableBody.children[i];
        const columns = row.children;
        for (let j = startColumn; j <= endColumn; j++) {
          const selectedColumn = columns[j] as HTMLElement;
          if (selectedElements[0] !== selectedColumn) selectedElements.push(selectedColumn);
        }
      }
    } else {
      selectedElements.push(element);
    }
    selectedElements.forEach((element) => {
      this.highlightSelectedElement(this.getDesiredSelectedElement(element));
    });
  };

  handleClickWithMeta = (): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) this.addElementToSelectedElements(selectedElement);
  };

  handleNotSuperKeysClick = (): void => {
    this.removeElementsFromSelectedElements();
  };

  addElementToSelectedElements = (element: HTMLElement): void => {
    if (selectedElements.length >= 1) {
      this.hideAddElementButtonToSelectedElement();
    }
    let notExists = true;
    const cell = element as HTMLTableCellElement;
    const row = element.parentElement as HTMLTableRowElement;
    selectedElements.forEach((selectedElement) => {
      const selectedCell = selectedElement as HTMLTableCellElement;
      const selectedRow = selectedElement.parentElement as HTMLTableRowElement;
      if (cell.cellIndex === selectedCell.cellIndex && row.rowIndex === selectedRow.rowIndex) {
        notExists = false;
      }
    });
    if (notExists) selectedElements.push(element);
    selectedElements.forEach((element) => {
      if (this.isNavigationBlock()) {
        this.highlightSelectedElement(element.closest('.row') as HTMLElement);
      } else {
        this.highlightSelectedElement(element);
      }
    });
  };

  removeElementsFromSelectedElements = (): void => {
    selectedElements.forEach((element) => {
      this.removeHighlightSelectedElement(element);
    });
    selectedElements = [];
    if (selectedElement) this.addElementToSelectedElements(selectedElement);
  };

  getSelectedElements = (): Array<HTMLElement> => {
    return selectedElements;
  };

  handleSelectedTableEvent = (): void => {
    let selectedTableElement = this.getSelectedElement();
    if (selectedTableElement) {
      if (selectedTableElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_TABLE}`)) {
        selectedTableElement = selectedTableElement.parentElement as HTMLElement;
      }
      if (selectedTableElement.tagName === 'TBODY' || selectedTableElement.tagName === 'TR') {
        const parent = this.elementPicker.getElement(
          selectedTableElement,
          'handleSelectedTableEvent',
        );
        if (parent) {
          selectedTableElement = parent.parentElement;
        }
      }
      selectedElement = selectedTableElement;
      this.hideRowSection();
      this.showSidebar();
      this.hideSubSidebar();
      this.hideDesignSection();
      this.hideContainerDesignSection();
      this.hideElementSection();
      this.hideMenuDesignSection();
      if (updateTableDesignSection) updateTableDesignSection();
      this.showTableDesignSection();
      if (selectedElement) {
        this.showHamburgerIconForTable(selectedElement);
        this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
        this.hideAddElementButtonToSelectedElement();
        selectedElementType = 'ELEMENT';
        this.showBlockOptions(false, false, true); // row = false, column = false, element = true
        if (selectedElement.parentElement) {
          const column = selectedElement.parentElement;
          if (column) {
            this.showHamburgerIconForColumn(column);
            if (column.parentElement) {
              const row = column.parentElement;
              if (customBlockType !== 'Elements') this.showHamburgerIconForRow(row);
            }
          }
        }
      }
    }
  };

  handleSelectedMenuEvent = (): void => {
    let selectedMenuElement = this.getSelectedElement();
    if (selectedMenuElement) {
      if (selectedMenuElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_ICON}`)) {
        selectedMenuElement = selectedMenuElement.parentElement as HTMLElement;
      }
      if (selectedMenuElement.tagName === 'P') {
        let parent = selectedMenuElement;
        while (parent && parent.tagName !== 'DIV') {
          if (parent.parentElement) {
            parent = parent.parentElement;
          }
        }
        if (parent) {
          selectedMenuElement = parent.parentElement;
        }
      }
      selectedElement = selectedMenuElement as HTMLElement;
      if (!selectedElement.classList.contains('row'))
        selectedElement = selectedElement.closest('.row') as HTMLElement;
      selectedElementType = 'ROW';
      this.showBlockOptions(true, false, false); // row = true, column = false, element = false

      this.hideRowSection();
      this.showSidebar();
      this.hideSubSidebar();
      this.hideDesignSection();
      this.hideContainerDesignSection();
      this.hideElementSection();
      this.hideTableDesignSection();
      if (updateMenuDesignSection) updateMenuDesignSection();
      this.showMenuDesignSection();
      if (selectedElement) {
        this.showHamburgerIconForMenu(selectedElement);
        this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
        this.hideAddElementButtonToSelectedElement();
        selectedElementType = 'ELEMENT';
        this.showBlockOptions(false, false, true); // row = false, column = false, element = true
        if (selectedElement.parentElement) {
          const column = selectedElement.parentElement;
          if (column) {
            this.showHamburgerIconForColumn(column);
            if (column.parentElement) {
              const row = column.parentElement;
              if (customBlockType !== 'Elements') this.showHamburgerIconForRow(row);
            }
          }
        }
      }
    }
  };

  handleSelectedLoreeInteractiveEvent = (): void => {
    let selectedInteractiveElement = this.getSelectedElement();
    if (!selectedInteractiveElement) return;
    if (
      selectedInteractiveElement.classList.contains(
        `${CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT}`,
      )
    ) {
      selectedInteractiveElement = selectedInteractiveElement.parentElement as HTMLElement;
    }
    selectedElement = selectedInteractiveElement;
    this.showSidebar();
    this.hideSubSidebar();
    this.hideDesignSection();
    this.hideInteractiveElementSection();
    this.highlightSelectedElement(selectedElement);
    selectedElementType = 'ELEMENT';
    this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    if (selectedElement.parentElement) {
      const column = selectedElement.parentElement;
      if (column) {
        this.showHamburgerIconForColumn(column);
        if (column.parentElement) {
          const row = column.parentElement;
          this.showHamburgerIconForRow(row);
        }
      }
    }
  };

  handleSelectedExternalToolEvent = (): void => {
    const selectedExternalToolElement = this.getSelectedElement();
    if (!selectedExternalToolElement) return;
    this.showSidebar();
    this.hideSubSidebar();
    this.hideDesignSection();
    this.hideInteractiveElementSection();
    this.highlightSelectedElement(selectedExternalToolElement);
    selectedElementType = 'ELEMENT';
    this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    if (!selectedExternalToolElement.parentElement) return;
    const column = selectedExternalToolElement.parentElement;
    if (!column) return;
    this.showHamburgerIconForColumn(column);
    if (!column.parentElement) return;
    const row = column.parentElement;
    this.showHamburgerIconForRow(row);
  };

  handleSelectedH5PEvent = (): void => {
    let selectedInteractiveElement = this.getSelectedElement();
    if (!selectedInteractiveElement) return;
    if (
      selectedInteractiveElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_H5P_ELEMENT}`)
    ) {
      selectedInteractiveElement = selectedInteractiveElement.parentElement as HTMLElement;
    }
    selectedElement = selectedInteractiveElement;
    this.showSidebar();
    this.hideSubSidebar();
    this.hideDesignSection();
    this.hideH5PElementSection();
    this.highlightSelectedElement(selectedElement);
    selectedElementType = 'ELEMENT';
    this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    if (selectedElement.parentElement) {
      const column = selectedElement.parentElement;
      if (column) {
        this.showHamburgerIconForColumn(column);
        if (column.parentElement) {
          const row = column.parentElement;
          this.showHamburgerIconForRow(row);
        }
      }
    }
  };

  handleSelectedTableDataEvent = (): void => {
    const selectedTableDataElement = this.getSelectedElement();
    if (selectedTableDataElement) {
      const parent = this.elementPicker.getElement(
        selectedTableDataElement,
        'handleSelectedTableDataEvent',
      );
      if (parent) {
        const tableParent = parent.parentElement;
        if (tableParent) {
          this.showHamburgerIconForTable(tableParent);
          if (tableParent.parentElement) {
            const column = tableParent.parentElement;
            if (column) {
              this.showHamburgerIconForColumn(column);
              if (column.parentElement) {
                const row = column.parentElement;
                if (customBlockType !== 'Elements') this.showHamburgerIconForRow(row);
              }
            }
          }
        }
      }
      this.highlightSelectedElement(selectedTableDataElement);
      if (updateTableDesignSection) updateTableDesignSection();
      this.showSidebar();
      this.hideSubSidebar();
      this.hideElementSection();
      this.hideDesignSection();
      this.hideMenuDesignSection();
      this.hideContainerDesignSection();
      this.showTableDesignSection();
      this.showAddElementButtonToSelectedElement();
    }
  };

  showItemsInSidebarHiddenForTable = (): void => {
    const tableElement = getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_ELEMENT,
    )?.parentElement;
    const customElement = getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT,
    )?.parentElement;
    const interactiveElement = getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT,
    )?.parentElement;
    const h5pElement = getElementById(CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT)?.parentElement;
    const externalToolElement = getElementById(
      CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT,
    )?.parentElement;

    if (tableElement) tableElement.style.display = 'inline-flex';
    if (customElement) customElement.style.display = 'inline-flex';
    if (interactiveElement) interactiveElement.style.display = 'inline-flex';
    if (h5pElement) h5pElement.style.display = 'inline-flex';
    if (externalToolElement) externalToolElement.style.display = 'inline-flex';
  };

  hideItemsInSidebarForTable = (): void => {
    const tableElement = getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_ELEMENT,
    )?.parentElement;
    const customElement = getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT,
    )?.parentElement;
    const interactiveElement = getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT,
    )?.parentElement;
    const h5pElement = getElementById(CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT)?.parentElement;
    const externalToolElement = getElementById(
      CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT,
    )?.parentElement;

    if (tableElement) tableElement.style.display = 'none';
    if (customElement) customElement.style.display = 'none';
    if (interactiveElement) interactiveElement.style.display = 'none';
    if (h5pElement) h5pElement.style.display = 'none';
    if (externalToolElement) externalToolElement.style.display = 'none';
    toggleElementVisibility(CONSTANTS.LOREE_DESIGN_SECTION_CONTAINER_ELEMENT, false);
  };

  handleSelectedImageEvent = (): void => {
    let selectedImageElement = this.getSelectedElement();
    if (selectedImageElement) {
      let element = 'image';
      const wrapper = getImageWrapperFromImage(selectedImageElement);

      if (wrapper) {
        selectedImageElement = wrapper;
        element = 'wrapper';
      }
      if (element === 'image') {
        selectedImageElement = selectedImageElement.parentElement as HTMLElement;
        if (selectedImageElement.tagName === 'A' && selectedImageElement.parentElement) {
          selectedImageElement = selectedImageElement.parentElement;
        }
      }

      selectedElement = selectedImageElement;
      this.highlightSelectedElement(this.getDesiredSelectedElement(selectedElement));
      this.showSidebar();
      this.hideSubSidebar();
      this.removeSelectedElementSection();
      this.disableElementSection();
      this.collapseElementSection();
      this.expandDesignSection();
      this.hideRowSection();
      this.hideColorPickersOfDesignSection();
      this.hideTableDesignSection();
      this.hideMenuDesignSection();
      this.hideHamburgerIconForMenu();
      this.hideHamburgerIconForContainer();
      this.hideContainerDesignSection();
      const selectedImage = selectedElement.querySelector('img');
      if (selectedImage) {
        selectedImage.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)
          ? this.showDesignSection('specialElement', selectedElement)
          : this.showDesignSection('image', selectedElement);
        getSelectedImageElementDetails(selectedImage);
        getSelectedImageElementStyle(selectedImage);
        this.fetchImageOnDesignSection(selectedImage.src);
        const column = this.getParentColumn(selectedElement);
        if (column) {
          this.showHamburgerIconForColumn(column);
          if (column.parentElement) {
            const row = column.parentElement;
            if (customBlockType !== 'Elements') this.showHamburgerIconForRow(row);
          }
        }
        if (selectedImage.classList.contains(CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO)) {
          this.highlightSelectedElement(selectedElement.closest('.row') as HTMLElement);
          this.showHamburgerIconForMenu(selectedElement.closest('.row') as HTMLElement);
        }
      }
      selectedElementType = 'ELEMENT';
      this.showBlockOptions(false, false, true); // row = false, column = false, element = true
    }
  };

  getVideoWrapper = (element: HTMLElement): HTMLElement | undefined => {
    const parent = element.parentElement as HTMLElement;
    if (parent.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)) {
      return parent;
    }
    this.getVideoWrapper(parent);
  };

  getVideoControls = (videoTag: HTMLVideoElement | HTMLElement, type: string): void => {
    const autoPlayCheckBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_AUTO_PLAY,
    );
    const loopCheckBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_LOOP_VIDEO,
    );
    const fullScreenBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_ALLOW_FULL_SCREEN,
    );
    const videoSrc: string = (videoTag as HTMLVideoElement).src;
    if (autoPlayCheckBoxVal && loopCheckBoxVal && fullScreenBoxVal) {
      (autoPlayCheckBoxVal as HTMLInputElement).checked = false;
      (loopCheckBoxVal as HTMLInputElement).checked = false;
      (fullScreenBoxVal as HTMLInputElement).checked = false;
      if (type === 'video') {
        if ((videoTag as HTMLVideoElement).autoplay) {
          (autoPlayCheckBoxVal as HTMLInputElement).checked = true;
        }
        if ((videoTag as HTMLVideoElement).loop) {
          (loopCheckBoxVal as HTMLInputElement).checked = true;
        }
        if (videoTag.classList.contains('allow-fullscreen')) {
          (fullScreenBoxVal as HTMLInputElement).checked = true;
        }
      } else {
        if (videoSrc.search('autoplay=1') > 0) {
          (autoPlayCheckBoxVal as HTMLInputElement).checked = true;
        }
        if (videoSrc.search('loop=1') > 0) {
          (loopCheckBoxVal as HTMLInputElement).checked = true;
        }
        if (videoTag.hasAttribute('allowfullscreen')) {
          (fullScreenBoxVal as HTMLInputElement).checked = true;
        }
      }
    }
  };

  handleSelectedEmbedUrlEvent = () => {
    this.showBlockOptions(false, false, true); // row = false, column = false, element = true
  };

  handleSelectedVideoEvent = (): void => {
    let selectedVideoElement = this.getSelectedElement();
    if (selectedVideoElement) {
      let element = 'video';
      if (selectedVideoElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)) {
        element = 'wrapper';
      }
      if (element === 'video') {
        selectedVideoElement = this.getVideoWrapper(selectedVideoElement) as HTMLElement;
      }
      if (selectedVideoElement) {
        selectedElement = selectedVideoElement;
        this.highlightSelectedElement(selectedElement);
        this.showSidebar();
        this.hideSubSidebar();
        this.removeSelectedElementSection();
        this.disableElementSection();
        this.collapseElementSection();
        this.hideTableDesignSection();
        this.hideMenuDesignSection();
        this.hideContainerDesignSection();
        this.expandDesignSection();
        let selectedVideo: HTMLVideoElement | HTMLElement | null;

        let iframeTag = null;
        if (selectedElement.getElementsByTagName('iframe').length > 0) {
          iframeTag = selectedElement.getElementsByTagName('iframe')[0] as HTMLElement;
        }
        let videoTag = null;
        if (selectedElement.getElementsByTagName('video').length > 0) {
          videoTag = selectedElement.getElementsByTagName('video')[0];
        }
        let videoType = '';

        if (videoTag) {
          selectedVideo = videoTag;
          videoType = 'video';
        } else {
          selectedVideo = iframeTag;
          videoType = 'iframe';
        }

        this.showDesignSection('video', selectedElement);
        if (selectedVideo) {
          getSelectedVideoElementDetails(selectedVideo);
          if (videoTag?.hasAttribute('poster')) {
            this.fetchImageOnDesignSection(videoTag.poster);
          }
          this.getVideoControls(selectedVideo, videoType);
          getSelectedVideoElementStyle(selectedVideo);
          const column = this.getParentColumn(selectedElement);
          if (column) {
            this.showHamburgerIconForColumn(column);
            if (column.parentElement) {
              const row = column.parentElement;
              this.showHamburgerIconForRow(row);
            }
          }
        }
        selectedElementType = 'ELEMENT';
        this.showBlockOptions(false, false, true); // row = false, column = false, element = true
      }
    }
  };

  handleSelectedContentWrapperEvent = (): void => {
    this.hideDesignSection();
    this.hideTableDesignSection();
    this.hideMenuDesignSection();
    this.hideContainerDesignSection();
    this.removeImageOnDesignSection();
  };

  highlightSelectedRow = (element: HTMLElement): void => {
    if (element) {
      element.classList.add(CONSTANTS.LOREE_ROW_HIGHLIGHT);
    }
  };

  removeHighlightSelectedRow = (element: HTMLElement): void => {
    if (element) {
      element.classList.remove(CONSTANTS.LOREE_ROW_HIGHLIGHT);
    }
  };

  highlightSelectedColumn = (element: HTMLElement): void => {
    if (element) {
      element.classList.add(CONSTANTS.LOREE_COLUMN_HIGHLIGHT);
      if (element.parentElement) {
        if (customBlockType !== 'Elements') this.showHamburgerIconForRow(element.parentElement);
      }
    }
  };

  removeHighlightSelectedColumn = (element: HTMLElement): void => {
    if (element) {
      element.classList.remove(CONSTANTS.LOREE_COLUMN_HIGHLIGHT);
      if (element.parentElement) {
        this.removeHighlightSelectedRow(element.parentElement);
      }
    }
  };

  highlightSelectedElement = (element: HTMLElement | null): void => {
    if (element) {
      if (this.isNavigationInnerSection()) {
        const navigationElement = element.closest('.row') as HTMLElement;
        navigationElement?.classList.add(CONSTANTS.LOREE_ELEMENT_HIGHLIGHT);
      }
      if (element.getAttribute('id') !== CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER) {
        element.classList.add(CONSTANTS.LOREE_ELEMENT_HIGHLIGHT);
      }
    }
  };

  removeHighlightSelectedElement = (element: HTMLElement | null): void => {
    if (element?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)) {
      this.hideHamburgerIconForColumn();
      this.hideHamburgerIconForMenu();
    }
    if (this.isNavigationInnerSection()) {
      const navigationElement = element?.closest('.row') as HTMLElement;
      navigationElement?.classList.remove(CONSTANTS.LOREE_ELEMENT_HIGHLIGHT);
    }
    element?.classList.remove(CONSTANTS.LOREE_ELEMENT_HIGHLIGHT);
  };

  showHamburgerIconForRow = (element: HTMLElement): void => {
    if (element) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const rowHamburgerIcon = iframeDocument.getElementById(CONSTANTS.LOREE_ROW_HAMBURGER_ICON);
        if (rowHamburgerIcon) {
          rowHamburgerIcon.onclick = (): void => {
            this.hideAnchorOption();
            this.changeSelectedElement(element);
          };
          if (rowHamburgerIconPopperInstance) {
            rowHamburgerIconPopperInstance.destroy();
            rowHamburgerIconPopperInstance = null;
            rowHamburgerIcon.style.display = 'none';
          }
          rowHamburgerIconPopperInstance = createPopper(element, rowHamburgerIcon, {
            placement: 'top-start',
            modifiers: [
              {
                name: 'computeStyles',
                options: {
                  gpuAcceleration: false, // true by default
                },
              },
              {
                name: 'flip',
                options: {
                  fallbackPlacements: [],
                },
              },
              {
                name: 'offset',
                options: {
                  offset: () => [-20, -20],
                },
              },
              {
                name: 'preventOverflow',
                options: {
                  mainAxis: true,
                },
              },
              {
                name: 'keepTogether',
                options: {
                  keepTogether: { enabled: false }, // true by default
                },
              },
            ],
          });
          rowHamburgerIcon.style.display = 'inline-flex';
        }
      }
    }
  };

  hideHamburgerIconForRow = (): void => {
    if (rowHamburgerIconPopperInstance) {
      rowHamburgerIconPopperInstance.destroy();
      rowHamburgerIconPopperInstance = null;
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const rowHamburgerIcon = iframeDocument.getElementById(CONSTANTS.LOREE_ROW_HAMBURGER_ICON);
        if (rowHamburgerIcon) {
          rowHamburgerIcon.style.display = 'none';
        }
      }
    }
  };

  showHamburgerIconForColumn = (element: HTMLElement): void => {
    if (element) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const columnHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_COLUMN_HAMBURGER_ICON,
        );
        if (columnHamburgerIcon) {
          columnHamburgerIcon.onclick = (): void => {
            this.hideAnchorOption();
            this.changeSelectedElement(element);
          };
          if (columnHamburgerIconPopperInstance) {
            columnHamburgerIconPopperInstance.destroy();
            columnHamburgerIconPopperInstance = null;
            columnHamburgerIcon.style.display = 'none';
          }
          columnHamburgerIconPopperInstance = createPopper(element, columnHamburgerIcon, {
            placement: 'top-start',
            modifiers: [
              {
                name: 'computeStyles',
                options: {
                  gpuAcceleration: false, // true by default
                },
              },
              {
                name: 'flip',
                options: {
                  fallbackPlacements: [],
                },
              },
              {
                name: 'offset',
                options: {
                  offset: () => [0, -20],
                },
              },
              {
                name: 'preventOverflow',
                options: {
                  mainAxis: true,
                },
              },
              {
                name: 'keepTogether',
                options: {
                  keepTogether: { enabled: false }, // true by default
                },
              },
            ],
          });
          columnHamburgerIcon.style.display = 'inline-flex';
        }
      }
    }
  };

  hideHamburgerIconForColumn = (): void => {
    if (columnHamburgerIconPopperInstance) {
      columnHamburgerIconPopperInstance.destroy();
      columnHamburgerIconPopperInstance = null;
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const columnHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_COLUMN_HAMBURGER_ICON,
        );
        if (columnHamburgerIcon) {
          columnHamburgerIcon.style.display = 'none';
        }
      }
    }
  };

  showHamburgerIconForTable = (element: HTMLElement): void => {
    if (element) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const tableHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_TABLE_HAMBURGER_ICON,
        );
        if (tableHamburgerIcon) {
          tableHamburgerIcon.onclick = (): void => {
            this.hideAnchorOption();
            this.changeSelectedElement(element);
          };
          if (tableHamburgerIconPopperInstance) {
            tableHamburgerIconPopperInstance.destroy();
            tableHamburgerIconPopperInstance = null;
            tableHamburgerIcon.style.display = 'none';
          }
          tableHamburgerIconPopperInstance = createPopper(element, tableHamburgerIcon, {
            placement: 'top-start',
            modifiers: [
              {
                name: 'computeStyles',
                options: {
                  gpuAcceleration: false, // true by default
                },
              },
              {
                name: 'flip',
                options: {
                  fallbackPlacements: [],
                },
              },
              {
                name: 'offset',
                options: {
                  offset: () => [-24, -35],
                },
              },
              {
                name: 'preventOverflow',
                options: {
                  mainAxis: true,
                },
              },
              {
                name: 'keepTogether',
                options: {
                  keepTogether: { enabled: false }, // true by default
                },
              },
            ],
          });
          tableHamburgerIcon.style.display = 'inline-flex';
        }
      }
    }
  };

  hideHamburgerIconForTable = (): void => {
    if (tableHamburgerIconPopperInstance) {
      tableHamburgerIconPopperInstance.destroy();
      tableHamburgerIconPopperInstance = null;
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const tableHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_TABLE_HAMBURGER_ICON,
        );
        if (tableHamburgerIcon) {
          tableHamburgerIcon.style.display = 'none';
        }
      }
    }
  };

  showHamburgerIconForMenu = (element: HTMLElement): void => {
    if (element) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const menuHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_NAVIGATE_HAMBURGER_ICON,
        );
        if (menuHamburgerIcon) {
          menuHamburgerIcon.onclick = (): void => {
            this.hideAnchorOption();
            this.changeSelectedElement(element);
          };
          if (menuHamburgerIconPopperInstance) {
            menuHamburgerIconPopperInstance.destroy();
            menuHamburgerIconPopperInstance = null;
            menuHamburgerIcon.style.display = 'none';
          }
          menuHamburgerIconPopperInstance = createPopper(element, menuHamburgerIcon, {
            placement: 'top',
            modifiers: [
              {
                name: 'computeStyles',
                options: {
                  gpuAcceleration: false, // true by default
                },
              },
              {
                name: 'flip',
                options: {
                  fallbackPlacements: ['top', 'bottom'],
                },
              },
              {
                name: 'offset',
                options: {
                  offset: () => [-125, 0],
                },
              },
              {
                name: 'preventOverflow',
                options: {
                  mainAxis: true,
                },
              },
              {
                name: 'keepTogether',
                options: {
                  keepTogether: { enabled: false }, // true by default
                },
              },
            ],
          });
          menuHamburgerIcon.style.display = 'inline-flex';
        }
      }
    }
  };

  hideHamburgerIconForMenu = (): void => {
    if (menuHamburgerIconPopperInstance) {
      menuHamburgerIconPopperInstance.destroy();
      menuHamburgerIconPopperInstance = null;
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const menuHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_NAVIGATE_HAMBURGER_ICON,
        );
        if (menuHamburgerIcon) {
          menuHamburgerIcon.style.display = 'none';
        }
      }
    }
  };

  showHamburgerIconForContainer = (element: HTMLElement): void => {
    if (element) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const containerHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_CONTAINER_HAMBURGER_ICON,
        );
        if (containerHamburgerIcon) {
          containerHamburgerIcon.onclick = (): void => {
            this.hideAnchorOption();
            this.changeSelectedElement(element);
          };
          if (containerHamburgerIconPopperInstance) {
            containerHamburgerIconPopperInstance.destroy();
            containerHamburgerIconPopperInstance = null;
            containerHamburgerIcon.style.display = 'none';
          }
          containerHamburgerIconPopperInstance = createPopper(element, containerHamburgerIcon, {
            placement: 'top-start',
            modifiers: [
              {
                name: 'computeStyles',
                options: {
                  gpuAcceleration: false, // true by default
                },
              },
              {
                name: 'flip',
                options: {
                  fallbackPlacements: [],
                },
              },
              {
                name: 'offset',
                options: {
                  offset: () => [-24, -24],
                },
              },
              {
                name: 'preventOverflow',
                options: {
                  mainAxis: true,
                },
              },
              {
                name: 'keepTogether',
                options: {
                  keepTogether: { enabled: false }, // true by default
                },
              },
            ],
          });
          containerHamburgerIcon.style.display = 'inline-flex';
        }
      }
    }
  };

  hideHamburgerIconForContainer = (): void => {
    if (containerHamburgerIconPopperInstance) {
      containerHamburgerIconPopperInstance.destroy();
      containerHamburgerIconPopperInstance = null;
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const containerHamburgerIcon = iframeDocument.getElementById(
          CONSTANTS.LOREE_CONTAINER_HAMBURGER_ICON,
        );
        if (containerHamburgerIcon) {
          containerHamburgerIcon.style.display = 'none';
        }
      }
    }
  };

  addContentEditableAttribute = (element: HTMLElement): void => {
    if (element) {
      this.hideAddElementButtonToSelectedElement();
      element.setAttribute('contenteditable', 'true');
    }
  };

  removeContentEditableAttribute = (element: HTMLElement): void => {
    if (element) {
      element.removeAttribute('contenteditable');
    }
  };

  shrinkSpaceWidth = (): void => {
    const space = document.getElementById(CONSTANTS.LOREE_SPACE);
    if (space) {
      space.style.width = `calc(100% - ${STYLE_CONSTANTS.SIDEBAR_WIDTH}px)`;
      space.style.left = `${STYLE_CONSTANTS.SIDEBAR_WIDTH}px`;
      a11yGlobalContext.value.setHighlightsOffset([
        STYLE_CONSTANTS.SIDEBAR_WIDTH,
        STYLE_CONSTANTS.A11Y_CHECKER_OFFSET_Y,
      ]);
    }
  };

  expandSpaceWidth = (): void => {
    const space = document.getElementById(CONSTANTS.LOREE_SPACE);
    if (space) {
      space.style.width = '100%';
      space.style.left = '0px';
      a11yGlobalContext.value.setHighlightsOffset([0, STYLE_CONSTANTS.A11Y_CHECKER_OFFSET_Y]);
    }
  };

  getDocument = (): HTMLDocument | null => {
    const iframe: HTMLIFrameElement | null = document.getElementById(
      CONSTANTS.LOREE_IFRAME,
    ) as HTMLIFrameElement;
    if (iframe) return iframe.contentDocument;
    return null;
  };

  getIframeWindow = () => {
    const iframe: HTMLIFrameElement | null = document.getElementById(
      CONSTANTS.LOREE_IFRAME,
    ) as HTMLIFrameElement;
    if (iframe) return iframe.contentWindow as IFrameWindow;
    return null;
  };

  applyStyleToSelectedElement = (property: string, value: string): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      if (property && value) {
        const style = selectedElement.style;
        style.setProperty(property, value);
      }
    }
  };

  applyAlignmentToTable = (property: string, value: string): void => {
    const selectedElement = this.getSelectedElement()?.children[0] as HTMLElement;
    if (selectedElement) {
      if (property && value !== undefined) {
        const style = selectedElement.style;
        style.setProperty(property, value);
      }
    }
  };

  showSidebar = (): void => {
    const sidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    if (!sidebar) return;
    const sidebarInteractiveElements = document.getElementById(
      'loree-sidebar-interactive-element-input-wrapper',
    );
    if (sidebarInteractiveElements) {
      sidebarInteractiveElements.classList.remove('d-block');
      sidebarInteractiveElements.classList.add('d-none');
    }
    this.hideH5PElementSection();
    sidebar.style.display = 'flex';
    this.shrinkSpaceWidth();
    this.hideSidebarOpenButton();
    uploadProgressPosition();
    const sidebarElementLoader = document.getElementsByClassName(
      CONSTANTS.LOREE_ELEMENTS_API_LOADER,
    )[0];
    sidebarElementLoader?.classList.add('d-none');
    const sidebarRowLoader = document.getElementsByClassName(CONSTANTS.LOREE__ROWS_API_LOADER)[0];
    sidebarRowLoader?.classList.add('d-none');
  };

  showSidebarOpenButton = (): void => {
    const sidebarOpenButton: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_OPEN_BUTTON,
    );
    if (sidebarOpenButton) {
      sidebarOpenButton.style.display = 'inline-flex';
    }
  };

  hideSidebarOpenButton = (): void => {
    const sidebarOpenButton: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_OPEN_BUTTON,
    );
    if (sidebarOpenButton) {
      sidebarOpenButton.style.display = 'none';
    }
  };

  hideSidebar = (): void => {
    const sidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    if (sidebar) {
      sidebar.style.display = 'none';
      this.expandSpaceWidth();
      this.showSidebarOpenButton();
      this.hideSubSidebar();
      this.hideCloseRowButton();
      this.showAddRowButton();
      this.removeSelectedRowSection();
      this.removeSelectedElementSection();
      uploadProgressPosition();
    }
  };

  showSubSidebar = (): void => {
    const subSidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SUB_SIDEBAR);
    if (subSidebar) {
      subSidebar.style.display = 'flex';
    }
  };

  hideSubSidebar = (): void => {
    const subSidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SUB_SIDEBAR);
    if (subSidebar) {
      subSidebar.style.display = 'none';
    }
  };

  hideGlobalTemplate = (): void => {
    const customGlobalTemplate = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_WRAPPER,
    );
    const sidebarTemplateLoader = getElementsByClassName('sidebarTemplateLoader')[0];
    sidebarTemplateLoader?.remove();
    customGlobalTemplate?.remove();
    setSharedTemplateStatus();
  };

  handleAddRowButtonClick = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const contentWrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      this.changeSelectedElement(contentWrapper);
      this.hideAddRowButton();
      this.hideGlobalTemplate();
      this.hideCustomRowSection();
      this.showSidebar();
      this.hideHamburgerIconForMenu();
      this.hideHamburgerIconForTable();
      this.hideSubSidebar();
      this.showRowSection();
      this.enableRowSection();
      this.expandRowSection();
      this.hideElementSection();
      this.collapseElementSection();
      this.showCloseRowButton();
      this.hideCloseElementButtonToSelectedElement();
      this.hideDesignSection();
      this.removeImageOnDesignSection();
      this.hideAnchorOption();
      uploadProgressPosition();
    }
  };

  handleCloseRowButton = (): void => {
    this.hideCloseRowButton();
    this.showAddRowButton();
    this.disableRowSection();
    this.hideSubSidebar();
    this.removeSelectedRowSection();
    this.changeSelectedElement(null);
    this.hideAddElementButtonToSelectedElement();
    this.hideCloseElementButtonToSelectedElement();
    this.hideDesignSection();
    this.removeImageOnDesignSection();
    uploadProgressPosition();
  };

  hideAddRowButton = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const addRowButton: HTMLElement | null = iframeDocument.getElementById(
        CONSTANTS.LOREE_ADD_ROW_BUTTON,
      );
      if (addRowButton) {
        addRowButton.style.display = 'none';
      }
    }
  };

  showAddRowButtonWrapper = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const addRowButtonWrapper: HTMLElement | null = iframeDocument.getElementById(
        CONSTANTS.LOREE_ADD_ROW_BUTTON_WRAPPER,
      );
      if (addRowButtonWrapper?.style.display === 'none') {
        addRowButtonWrapper.style.display = 'block';
      }
    }
  };

  hideAddRowButtonWrapper = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const addRowButtonWrapper: HTMLElement | null = iframeDocument.getElementById(
        CONSTANTS.LOREE_ADD_ROW_BUTTON_WRAPPER,
      );
      if (addRowButtonWrapper) {
        addRowButtonWrapper.style.display = 'none';
      }
    }
  };

  showAddRowButton = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const addRowButton: HTMLElement | null = iframeDocument.getElementById(
        CONSTANTS.LOREE_ADD_ROW_BUTTON,
      );
      if (addRowButton) {
        addRowButton.style.display = 'inline-flex';
      }
    }
  };

  hideCloseRowButton = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const closeRowButton: HTMLElement | null = iframeDocument.getElementById(
        CONSTANTS.LOREE_CLOSE_ROW_BUTTON,
      );
      if (closeRowButton) {
        closeRowButton.style.display = 'none';
      }
    }
  };

  showCloseRowButton = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const closeRowButton: HTMLElement | null = iframeDocument.getElementById(
        CONSTANTS.LOREE_CLOSE_ROW_BUTTON,
      );
      if (closeRowButton) {
        closeRowButton.style.display = 'inline-flex';
      }
    }
  };

  showRowSection = (): void => {
    const rowSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION);
    if (!rowSection) return;
    rowSection.style.display = 'block';
    this.showCustomRowSection();
    this.hideCustomRowSection();
  };

  hideRowSection = (): void => {
    const rowSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION);
    if (!rowSection) return;
    rowSection.style.display = 'none';
    this.hideCustomRowSection();
  };

  showCustomRowSection = (): void => {
    const row = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE);
    row?.childNodes.forEach((child) => {
      if (
        (child as HTMLElement).id !== 'customRows' &&
        (child as HTMLElement).id !== CONSTANTS.LOREE_SIDEBAR_CUSTOM_ROW_INPUT_WRAPPER &&
        (child as HTMLElement).classList.contains('d-none')
      ) {
        (child as HTMLElement).classList.remove('d-none');
      }
    });
  };

  hideCustomRowSection = (): void => {
    const customRowInputWrapper = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_ROW_INPUT_WRAPPER,
    );
    const sidebarRowsLoader = getElementsByClassName('sidebarRowsLoader')[0];
    sidebarRowsLoader?.remove();
    customRowInputWrapper?.remove();
    setSharedBlockStatus('row');
  };

  disableRowSection = (): void => {
    const rowButton = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_ROW_SECTION_BUTTON,
    ) as HTMLButtonElement;
    if (rowButton) {
      rowButton.disabled = true;
    }
  };

  enableRowSection = (): void => {
    const rowButton = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_ROW_SECTION_BUTTON,
    ) as HTMLButtonElement;
    if (rowButton) {
      rowButton.disabled = false;
    }
  };

  expandRowSection = (): void => {
    const rowSectionButton = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION_BUTTON);
    rowSectionButton?.classList.remove('collapsed');
    const rowCollapse = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE);
    if (rowCollapse) {
      rowCollapse.classList.add('show');
    }
  };

  collapseRowSection = (): void => {
    const rowCollapse = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE);
    const rowSectionButton = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION_BUTTON);
    if (rowCollapse) {
      rowCollapse.classList.remove('show');
      rowSectionButton?.classList.add('collapsed');
    }
    this.hideCustomRowSection();
    this.showCustomRowSection();
  };

  showDesignSection = (elementType: string, selectedElement: HTMLElement): void => {
    this.hideGlobalTemplate();
    this.hideRowSection();
    this.hideElementSection();
    const designSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION);
    if (!designSection) return;
    designSection.style.display = 'block';
    this.designSec.attachDesignContent(elementType, selectedElement, featuresList);
  };

  hideDesignSection = (): void => {
    const designSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION);
    if (!designSection) return;
    designSection.style.display = 'none';
    this.hideColorPickersOfDesignSection();
  };

  disableDesignSection = (): void => {
    const designButton = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_BUTTON,
    ) as HTMLButtonElement;
    if (designButton) {
      designButton.disabled = true;
    }
  };

  enableDesignSection = (): void => {
    const designButton = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_BUTTON,
    ) as HTMLButtonElement;
    if (designButton) {
      designButton.disabled = false;
    }
  };

  expandDesignSection = (): void => {
    const designButton = document.getElementById('loree-sidebar-design-section-button');
    designButton?.classList.remove('collapsed');
    const designCollapse = document.getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_COLLAPSE);
    if (designCollapse) {
      designCollapse.classList.add('show');
      this.hideColorPickersOfDesignSection();
    }
  };

  collapseDesignSection = (): void => {
    const designCollapse = document.getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_COLLAPSE);
    if (designCollapse) {
      designCollapse.classList.remove('show');
      this.hideColorPickersOfDesignSection();
    }
  };

  removeSelectedRowSection = (): void => {
    const activeRow = document.getElementById(`${CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE}`);
    if (activeRow) {
      const rows = Array.prototype.slice.call(activeRow.childNodes);
      rows.forEach((row) => {
        row.classList.remove('active');
      });
    }
  };

  appendContentToIframeContentWrapper = (html: string): void => {
    let selectedElement =
      customBlockEditStatus && isContainer.active
        ? this.getDocument()?.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER)
        : this.getSelectedElement();
    if (isContainer.active) isContainer.active = false;
    if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER)) {
      selectedElement = selectedElement.children[0] as HTMLElement;
    }
    if (selectedElement) {
      const temp = document.createElement('div');
      temp.innerHTML = html;
      const element = temp.children[0] as HTMLElement;
      selectedElement.lastElementChild?.tagName === 'SCRIPT'
        ? selectedElement.insertBefore(element, selectedElement.lastElementChild)
        : selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)
        ? selectedElement.insertAdjacentElement('afterend', element)
        : selectedElement.appendChild(element);
      const column = element.children[0] as HTMLElement;
      this.changeSelectedElement(column);
      this.handleSelectedContentChanges();
    }
  };

  appendImageToSelectedColumn = (html: string, selectedElement: HTMLElement | null): void => {
    if (selectedElement) {
      const temp = document.createElement('div');
      temp.innerHTML = html;
      const content = temp.getElementsByTagName('img')[0];
      const tempChildrens = Array.from(temp.children);
      tempChildrens.forEach((children) => {
        addUserLanguageAsDefaultToElement(children);
        selectedElement.appendChild(children);
      });
      this.changeSelectedElement(content);
      this.collapseElementSection();
      this.disableElementSection();
      this.hideCloseElementButtonToSelectedElement();
      const selectedImage = content.getElementsByTagName('img')[0];
      if (selectedImage) {
        getSelectedImageElementDetails(selectedImage);
        getSelectedImageElementStyle(selectedImage);
        this.fetchImageOnDesignSection(selectedImage.src);
      }
      this.handleSelectedContentChanges();
    }
  };

  showTableDesignSection = (): void => {
    const tableDesignSection = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION,
    );
    if (!tableDesignSection) return;
    this.hideCustomElementSection();
    this.hideGlobalTemplate();
    tableDesignSection.style.display = 'block';
  };

  hideTableDesignSection = (): void => {
    this.removeElementsFromSelectedElements();
    const tableDesignSection = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION,
    );
    if (!tableDesignSection) return;
    tableDesignSection.style.display = 'none';
    hideColorPickerofElementSection(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_COLOR_PICKERS);
  };

  showMenuDesignSection = (): void => {
    const menuDesignSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION);
    if (!menuDesignSection) return;
    this.hideCustomElementSection();
    this.hideGlobalTemplate();
    menuDesignSection.style.display = 'block';
  };

  hideMenuDesignSection = (): void => {
    this.removeElementsFromSelectedElements();
    const menuDesignSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION);
    if (!menuDesignSection) return;
    menuDesignSection.style.display = 'none';
  };

  showContainerDesignSection = (): void => {
    const containerDesignSection = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION,
    );
    if (!containerDesignSection) return;
    containerDesignSection.style.display = 'block';
    this.hideCustomElementSection();
    this.hideGlobalTemplate();
  };

  hideContainerDesignSection = (): void => {
    const containerDesignSection = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION,
    );
    if (!containerDesignSection) return;
    containerDesignSection.style.display = 'none';
    this.hideColorPickersOfContainerSection();
  };

  handleAddElementButtonClick = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      this.hideGlobalTemplate();
      this.showSidebar();
      this.showAddRowButton();
      this.hideCloseRowButton();
      this.disableRowSection();
      this.collapseRowSection();
      this.enableElementSection();
      this.hideCustomElementSection();
      this.showElementSection();
      this.expandElementSection();
      this.hideAddElementButtonToSelectedElement();
      this.appendCloseAddElementButtonToSelectedElement();
      this.hideDesignSection();
      this.hideTableDesignSection();
      toggleElementVisibility(CONSTANTS.LOREE_DESIGN_SECTION_CONTAINER_ELEMENT, true);
      this.hideMenuDesignSection();
      this.hideContainerDesignSection();
      this.removeImageOnDesignSection();
      this.showItemsInSidebarHiddenForTable();
      this.hideInteractiveElementSection();
      this.hideH5PElementSection();
      if (
        selectedElement &&
        (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH')
      ) {
        this.hideItemsInSidebarForTable();
      }
    }
  };

  handleCloseAddElementButtonClick = (): void => {
    const iframeDocument = this.getDocument();
    const selectedElement = this.getSelectedElement();
    if (iframeDocument) {
      this.hideSubSidebar();
      this.hideCloseRowButton();
      this.showAddRowButton();
      this.collapseElementSection();
      this.hideCustomElementSection();
      this.disableElementSection();
      this.hideCloseElementButtonToSelectedElement();
      this.showAddElementButtonToSelectedElement();
      this.removeSelectedElementSection();
      this.hideDesignSection();
      this.removeImageOnDesignSection();
      this.hideInteractiveElementSection();
      this.hideH5PElementSection();
    }
    if (selectedElement && (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH')) {
      this.showTableDesignSection();
    }
  };

  handleInsertRowButtonClick = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    this.hideGlobalTemplate();
    this.hideCustomRowSection();
    this.showSidebar();
    this.hideSubSidebar();
    this.showRowSection();
    this.enableRowSection();
    this.expandRowSection();
    this.hideElementSection();
    this.collapseElementSection();
    this.hideCloseElementButtonToSelectedElement();
    this.hideDesignSection();
    this.removeImageOnDesignSection();
    this.appendCloseInsertRowButton();
  };

  handleCloseInsertRowButtonClick = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    this.hideCloseInsertRowButton();
    this.showInsertRowButton();
    this.disableRowSection();
    this.hideSubSidebar();
    this.removeSelectedRowSection();
    this.collapseRowSection();
    this.hideAddElementButtonToSelectedElement();
    this.hideCloseElementButtonToSelectedElement();
    this.hideDesignSection();
    this.removeImageOnDesignSection();
  };

  showElementSection = (): void => {
    const elementSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION);
    if (!elementSection) return;
    elementSection.style.display = '';
  };

  hideElementSection = (): void => {
    const elementSection = document.getElementById(CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION);
    if (!elementSection) return;
    elementSection.style.display = 'none';
  };

  expandElementSection = (): void => {
    const elementButton = document.getElementById('loree-sidebar-element-section-button');
    const elementCollapse = document.getElementById('loree-sidebar-element-wrapper');
    elementButton?.classList.remove('collapsed');
    elementCollapse?.classList.add('show');
  };

  collapseElementSection = (): void => {
    const elementCollapse = document.getElementById('loree-sidebar-element-wrapper');
    if (elementCollapse) {
      elementCollapse.classList.remove('show');
    }
  };

  disableElementSection = (): void => {
    const elementButton = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION,
    ) as HTMLButtonElement;
    if (elementButton) {
      elementButton.disabled = true;
      elementButton.style.display = 'none';
    }
  };

  enableElementSection = (): void => {
    const elementButton = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION_BUTTON,
    ) as HTMLButtonElement;
    if (elementButton) {
      elementButton.disabled = false;
      if (elementButton.style.display === 'none') elementButton.style.display = '';
    }
  };

  hideCustomElementSection = (): void => {
    const customElementSection = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_ELEMENT_INPUT_WRAPPER,
    );
    const sidebarElementsLoader = getElementsByClassName('sidebarElementsLoader')[0];
    sidebarElementsLoader?.remove();
    customElementSection?.remove();
    setSharedBlockStatus('element');
  };

  refreshCustomBlockSearch = (blockType: string) => {
    const elementSearchInput = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_ELEMENT_SEARCH_INPUT,
    ) as HTMLInputElement;
    const rowSearchInput = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_ROW_SEARCH_INPUT,
    ) as HTMLInputElement;
    switch (blockType) {
      case 'elements':
        if (!elementSearchInput) return;
        elementSearchInput.value = '';
        break;
      case 'row':
        if (!rowSearchInput) return;
        rowSearchInput.value = '';
        break;
    }
    refreshCheckBox(blockType);
  };

  removeSelectedElementSection = (): void => {
    const activeElement = getElementById(`${CONSTANTS.LOREE_SIDEBAR_ELEMENT_COLLAPSE}`);
    if (!activeElement?.childNodes) return;
    const elements = Array.prototype.slice.call(activeElement.childNodes[0].childNodes);
    elements.forEach((element) => {
      element.childNodes[0].classList.remove('active');
    });
  };

  hideInteractiveElementSection = (): void => {
    const interactiveElementSection = document.getElementById(
      'loree-sidebar-interactive-element-input-wrapper',
    );
    if (interactiveElementSection) {
      interactiveElementSection.classList.remove('d-block');
      interactiveElementSection.classList.add('d-none');
    }
  };

  hideH5PElementSection = (): void => {
    const H5PElementSection = document.getElementById('loree-sidebar-H5P-element-input-wrapper');
    if (H5PElementSection) {
      H5PElementSection?.remove();
    }
  };

  appendElementToSelectedColumn = (html: string): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      if (selectedElement?.classList.contains('element-highlight')) {
        selectedElement.classList.remove('element-highlight');
      }
      const temp = document.createElement('div');
      temp.innerHTML = html;
      Array.from(temp.children).forEach((child) => {
        let element = child as HTMLElement;
        element = this.attachCustomizeStyleToElement(element);
        selectedElement.appendChild(element);
        this.changeSelectedElement(element);
        addUserLanguageAsDefaultToElement(element);
        this.hideLanguageIsoLabel();
      });
      this.collapseElementSection();
      this.disableElementSection();
      this.hideCloseElementButtonToSelectedElement();
      this.handleSelectedContentChanges();
    }
  };

  appandCustomElementTOSelectedColumn = (html: string): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      if (selectedElement?.classList.contains('element-highlight')) {
        selectedElement.classList.remove('element-highlight');
      }
      const temp = document.createElement('div');
      temp.innerHTML = html;
      Array.from(temp.children).forEach((child) => {
        const element = child as HTMLElement;
        selectedElement.appendChild(element);
        this.changeSelectedElement(element);
      });
      this.collapseElementSection();
      this.disableElementSection();
      this.hideCloseElementButtonToSelectedElement();
      this.handleSelectedContentChanges();
    }
  };

  attachCustomizeStyleToElement(element: HTMLElement) {
    const tagName = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'paragraph'];
    let tag = element.tagName.toLowerCase();
    if (tag === 'p') {
      tag = 'paragraph';
    }
    if (tagName.includes(tag)) {
      element.style.fontFamily = customHeaderStyle[tag].font;
      element.style.fontSize = customHeaderStyle[tag].size;
    }
    for (let i = 0; i < element.children.length; i++) {
      this.attachCustomizeStyleToElement(element.children[i] as HTMLElement);
    }
    return element;
  }

  getCustomHeaderStyle(custom: ConfigInterface) {
    const tagName = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'paragraph'];
    for (let i = 0; i < custom.customHeaderStyle.customHeader.length; i++) {
      customHeaderStyle[tagName[i]] = custom.customHeaderStyle.customHeader[i][tagName[i]];
    }
  }

  appendLineElementToSelectedColumn = (html: string): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      const temp = document.createElement('div');
      temp.innerHTML = html;
      const content = temp.childNodes[0] as HTMLElement;
      if (
        !selectedElement.previousSibling &&
        !selectedElement.nextSibling &&
        !selectedElement.firstChild
      ) {
        selectedElement.setAttribute('style', 'min-height: 0px');
      }
      selectedElement.appendChild(content);
      this.changeSelectedElement(content);
      this.collapseElementSection();
      this.disableElementSection();
      this.hideCloseElementButtonToSelectedElement();
      const selectedLine = content.getElementsByTagName('hr')[0];
      if (selectedLine) {
        getSelectedLineElementStyle(selectedLine);
      }
      this.handleSelectedContentChanges();
    }
  };

  elementAddButtonPlacement = (
    selectedElement: Element | VirtualElement,
    ElementButton: HTMLElement,
  ): void => {
    addElementPopperInstance = createPopper(selectedElement, ElementButton, {
      placement: 'bottom',
      modifiers: [
        {
          name: 'computeStyles',
          options: {
            gpuAcceleration: false, // true by default
          },
        },
        {
          name: 'flip',
          options: {
            fallbackPlacements: [],
          },
        },
        {
          name: 'offset',
          options: {
            offset: () => [0, -36],
          },
        },
        {
          name: 'keepTogether',
          options: {
            keepTogether: { enabled: false },
          },
        },
      ],
    });
  };

  insertAddRowButtonPlacement = (
    selectedElement: Element | VirtualElement,
    ElementButton: HTMLElement,
  ): void => {
    addElementPopperInstance = createPopper(selectedElement, ElementButton, {
      placement: 'bottom',
      modifiers: [
        {
          name: 'computeStyles',
          options: {
            gpuAcceleration: false, // true by default
          },
        },
        {
          name: 'flip',
          options: {
            fallbackPlacements: [],
          },
        },
        {
          name: 'offset',
          options: {
            offset: () => [0, -18],
          },
        },
        {
          name: 'keepTogether',
          options: {
            keepTogether: { enabled: false },
          },
        },
      ],
    });
  };

  blockOptionsPlacement = (
    selectedElement: Element | import('@popperjs/core').VirtualElement,
    blockOptions: HTMLElement,
    isBlockSection: boolean,
    position: string,
    placements: string[],
  ): void => {
    blockOptionsPopperInstance = createPopper(selectedElement, blockOptions, {
      placement: position as Placement,
      modifiers: [
        {
          name: 'computeStyles',
          options: {
            gpuAcceleration: false, // true by default
          },
        },
        {
          name: 'flip',
          options: {
            fallbackPlacements: placements,
          },
        },
        {
          name: 'offset',
          options: {
            offset: () => [0, isBlockSection ? -43 : 0],
          },
        },
        {
          name: 'preventOverflow',
          options: {
            mainAxis: true,
          },
        },
        {
          name: 'keepTogether',
          options: {
            keepTogether: { enabled: false }, // true by default
          },
        },
      ],
    });
  };

  blockEditOptionsPlacement = (
    arrow: HTMLElement,
    editButton: HTMLElement,
    blockEditOption: HTMLElement,
  ): void => {
    blockEditOptionsPopperInstance = createPopper(editButton, blockEditOption, {
      placement: 'bottom',
      modifiers: [
        {
          name: 'computeStyles',
          options: {
            gpuAcceleration: false, // true by default
          },
        },
        {
          name: 'flip',
          options: {
            fallbackPlacements: [],
          },
        },
        {
          name: 'offset',
          options: {
            offset: () => [0, 20],
          },
        },
        {
          name: 'arrow',
          options: {
            element: arrow,
          },
        },
        {
          name: 'preventOverflow',
          options: {
            mainAxis: true,
            padding: { left: 20, right: 25 },
          },
        },
        {
          name: 'keepTogether',
          options: {
            keepTogether: { enabled: false }, // true by default
          },
        },
      ],
    });
  };

  showAddElementButtonToSelectedElement = (): void => {
    this.hideCloseElementButtonToSelectedElement();
    const selectedElement = this.getSelectedElement();
    const iframeDocument = this.getDocument();
    if (selectedElement && iframeDocument) {
      const addElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_ADD_ELEMENT_BUTTON}`,
      ) as HTMLElement;
      if (!addElementButton) return;
      this.elementAddButtonPlacement(selectedElement, addElementButton);
      addElementButton.style.display = 'inline-flex';
    }
  };

  hideAddElementButtonToSelectedElement = (): void => {
    this.removeMinHeightFromColumnAndContainer();
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const addElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_ADD_ELEMENT_BUTTON}`,
      ) as HTMLElement;
      if (addElementButton) addElementButton.style.display = 'none';
    }
  };

  showInsertRowButton = (): void => {
    this.hideCloseElementButtonToSelectedElement();
    const selectedElement = this.getSelectedElement();
    const iframeDocument = this.getDocument();
    if (selectedElement && iframeDocument && selectedElement.nextElementSibling) {
      const insertRowButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_INSERT_ROW_BUTTON}`,
      ) as HTMLElement;
      if (!insertRowButton) return;
      this.insertAddRowButtonPlacement(selectedElement, insertRowButton);
      insertRowButton.style.display = 'inline-flex';
    }
  };

  hideInsertRowButton = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const addElementButton = iframeDocument.getElementById(
      `${CONSTANTS.LOREE_INSERT_ROW_BUTTON}`,
    ) as HTMLElement;
    if (!addElementButton) return;
    addElementButton.style.display = 'none';
  };

  appendCloseInsertRowButton = (): void => {
    const iframeDocument = this.getDocument();
    const selectedElement = this.getSelectedElement();
    if (selectedElement && iframeDocument && selectedElement.nextElementSibling) {
      const closeAddElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON}`,
      ) as HTMLElement;
      this.insertAddRowButtonPlacement(selectedElement, closeAddElementButton);
      closeAddElementButton.style.display = 'inline-flex';
    }
  };

  hideCloseInsertRowButton = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const closeAddElementButton = iframeDocument.getElementById(
      `${CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON}`,
    ) as HTMLElement;
    if (!closeAddElementButton) return;
    closeAddElementButton.style.display = 'none';
  };

  appendCloseAddElementButtonToSelectedElement = (): void => {
    const iframeDocument = this.getDocument();
    const selectedElement = this.getSelectedElement();
    this.addMinHeightToSelectedElement(selectedElement);
    if (selectedElement && iframeDocument) {
      const closeAddElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_CLOSE_ADD_ELEMENT_BUTTON}`,
      ) as HTMLElement;
      if (!closeAddElementButton) return;
      this.elementAddButtonPlacement(selectedElement, closeAddElementButton);
      closeAddElementButton.style.display = 'inline-flex';
    }
  };

  hideCloseElementButtonToSelectedElement = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const closeAddElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_CLOSE_ADD_ELEMENT_BUTTON}`,
      ) as HTMLElement;
      if (!closeAddElementButton) return;
      closeAddElementButton.style.display = 'none';
    }
  };

  hideBlockOptionsToSelectedElements = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const blockEditOptions = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER}`,
      ) as HTMLElement;
      this.hideBlockOptions();
      this.hideBlockEditOptions(blockEditOptions);
    }
  };

  generateGetBoundingClientRect = (x: number, y: number) => {
    return () => ({
      width: 0,
      height: 0,
      top: y,
      right: x,
      bottom: y,
      left: x,
    });
  };

  getSelectedText = () => {
    const iframe = this.getDocument();
    if (iframe) {
      const selectedText = iframe.getSelection();
      return selectedText;
    }
  };

  initiateTextOptionsPopper = (): void => {
    this.hideAnchorOption();
    if (featuresList?.fontstyles) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const virtualElement = {
          getBoundingClientRect: this.generateGetBoundingClientRect(0, 0),
        };
        const selectedText = iframeDocument.getSelection() as Selection;
        if (selectedText) {
          const linkNode = findUpwardsByTag(selectedText.anchorNode, 'a');
          if (linkNode) {
            anchorUrl = linkNode.href;
          }

          // when user apply any style to that text-link, 'A' will be parent otherwise it will come as anchorNode
          // const anchorNode = selectedText.anchorNode as HTMLAnchorElement;
          // const anchorNodeParent = anchorNode?.parentElement as HTMLAnchorElement;
          // if (
          //   anchorNodeParent?.parentElement?.tagName === 'A' ||
          //   anchorNodeParent?.tagName === 'A' ||
          //   anchorNode?.tagName === 'A'
          // ) {
          //   anchorUrl =
          //     anchorNodeParent.href !== undefined
          //       ? anchorNodeParent.href
          //       : anchorNode.href !== undefined
          //       ? anchorNode.href
          //       : (anchorNodeParent?.parentElement as HTMLAnchorElement)?.href;
          // }
          if (selectedText?.toString().trim().length > 0) {
            const contentWrapper = iframeDocument.body;
            const textOptions = iframeDocument.getElementById(
              CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER,
            ) as HTMLElement;
            const arrow = iframeDocument.querySelector(
              `#${CONSTANTS.LOREE_TEXT_OPTIONS_ARROW_REFERENCE_ID}`,
            ) as HTMLElement;
            if (contentWrapper && textOptions) {
              const editorPosition = contentWrapper.getBoundingClientRect();
              previousSelection = selectedText.getRangeAt(0);
              this.getSelectedDetails(selectedText, textOptions);
              const selectedTextElement = this.getSelectedElement();
              if (
                isClassListContains(selectedTextElement, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS) ||
                isClassListContains(selectedTextElement, CONSTANTS.LOREE_TABLE_CAPTION_ELEMENT)
              ) {
                this.modifyConversionTextPopupIcons('none');
              } else {
                this.modifyConversionTextPopupIcons('flex');
              }
              const selectedTextPosition = selectedText?.getRangeAt(0)?.getBoundingClientRect();
              const textOptionsPopperStyle = textOptions?.getBoundingClientRect();
              virtualElement.getBoundingClientRect = this.generateGetBoundingClientRect(
                selectedTextPosition.x,
                selectedTextPosition.y,
              );
              const topDistanceDiff = selectedTextPosition.top - editorPosition.top;
              const textOptionsPopperHeight = textOptionsPopperStyle.height + 10; // additionally 10px added for arrow height
              let placementTop: boolean = false;
              if (topDistanceDiff > textOptionsPopperHeight) {
                placementTop = true;
              }
              textOptionsPopperInstance = createPopper(
                virtualElement as VirtualElement,
                textOptions,
                {
                  placement: placementTop ? 'top' : 'bottom',
                  modifiers: [
                    {
                      name: 'computeStyles',
                      options: {
                        gpuAcceleration: false, // true by default
                      },
                    },
                    {
                      name: 'flip',
                      options: {
                        fallbackPlacements: [],
                      },
                    },
                    {
                      name: 'offset',
                      options: {
                        offset: () => [0, placementTop ? 10 : 35], // for popper surround space
                      },
                    },
                    {
                      name: 'arrow',
                      options: {
                        element: arrow,
                      },
                    },
                    {
                      name: 'keepTogether',
                      options: {
                        keepTogether: { enabled: true }, // true by default
                      },
                    },
                    {
                      name: 'eventListeners',
                      options: {
                        scroll: false, // true by default
                        resize: true, // true by default
                      },
                    },
                  ],
                },
              );
              textOptions.style.visibility = 'visible';
            }
          }
        }
      }
    }
  };

  modifyConversionTextPopupIcons = (display: string) => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    this.getContentElementById(
      iframeDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON,
    )?.setAttribute('style', 'display: ' + display);
    this.getContentElementById(
      iframeDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON,
    )?.setAttribute('style', 'display: ' + display);
    this.getContentElementById(
      iframeDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON,
    )?.setAttribute('style', 'display: ' + display);
  };

  getContentElementById(iframeDocument: HTMLDocument, id: string): HTMLElement | null {
    return iframeDocument.getElementById(id);
  }

  resetAnchorUrl = () => {
    anchorUrl = '';
    fetchOpenInNewTab = false;
  };

  destroyTextOptionsPopperInstance = (): void => {
    if (textOptionsPopperInstance) {
      textOptionsPopperInstance?.destroy();
      textOptionsPopperInstance = null;
      this.hidePopperInstances();
    }
  };

  hideTextOptions = () => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const textOptions = iframeDocument.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER,
    ) as HTMLElement;
    if (!textOptions) return;
    textOptions.style.visibility = 'hidden';
    this.destroyTextOptionsPopperInstance();
  };

  /*
   * display link attached to text
   */
  inputLinkHandler = (
    url: string,
    customLink: { [x: string]: string; color: string; font: string } | '',
  ) => {
    if (!this.validateWindowSelection()) return;
    this.wordStylerObj?.handleLinkForSelection(url, customLink);
    const checkBox = this.getDocument()?.getElementById(
      'link-option-tab-checkbox',
    ) as HTMLInputElement;
    this.handleLinkCheckBox(checkBox, 'click');
    const range = document.createRange();
    const selectedText = this.getDocument()?.getSelection();
    if (selectedText?.type !== 'Range') {
      if (
        !this.elementPicker.checkTagName(selectedText?.anchorNode as HTMLElement) ||
        (selectedText?.anchorNode as HTMLElement).tagName === 'LI'
      )
        range.selectNodeContents(selectedText?.anchorNode as HTMLElement);
      else range.selectNodeContents(selectedText?.anchorNode?.nextSibling as HTMLElement);
      selectedText?.removeAllRanges();
      selectedText?.addRange(range);
    }
    this.closeLinkOption();
  };

  initiateAnchorLinkPopper = (eventTarget: HTMLElement): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const arrow = iframeDocument.querySelector(
        `#${CONSTANTS.LOREE_TEXT_ANCHOR_ARROW_REFERENCE_ID}`,
      ) as HTMLElement;
      const anchorTag = findUpwardsByTag(eventTarget, 'a');

      // anchorTag = this.elementPicker.getElement(anchorTag, 'initiateAnchorLinkPopper');
      if (anchorTag) {
        anchorUrl = getURLFromAnchor(anchorTag);
        if (anchorTag?.href?.includes('@')) {
          const mailTo = anchorTag.href.split(':');
          anchorUrl = mailTo[1];
        }
        fetchOpenInNewTab = anchorTag.target !== '';
      }
      if (!anchorTag?.parentElement) return;
      const anchorLinkOption = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_ANCHOR_LINK_WRAPPER}`,
      ) as HTMLElement;
      anchorLinkOption.className = 'loree-text-options-anchor-link d-flex';
      anchorLinkPopperInstance = createPopper(anchorTag, anchorLinkOption, {
        placement: 'bottom',
        modifiers: [
          {
            name: 'computeStyles',
            options: {
              gpuAcceleration: false, // true by default
            },
          },
          {
            name: 'flip',
            options: {
              fallbackPlacements: [],
            },
          },
          {
            name: 'offset',
            options: {
              offset: () => [0, 10],
            },
          },
          {
            name: 'arrow',
            options: {
              element: arrow,
            },
          },
          {
            name: 'preventOverflow',
            options: {
              mainAxis: true,
              padding: { left: 20, right: 25 },
            },
          },
          {
            name: 'keepTogether',
            options: {
              keepTogether: { enabled: false }, // true by default
            },
          },
        ],
      });
      if (iframeDocument.getElementById(CONSTANTS.LOREE_SELECTED_ANCHOR_TEXT_ELEMENT) === null) {
        const anchorLinkTag = document.createElement('a');
        anchorLinkTag.id = CONSTANTS.LOREE_SELECTED_ANCHOR_TEXT_ELEMENT;
        anchorLinkTag.href = anchorTag.href;
        anchorLinkTag.setAttribute('rel', 'noreferrer noopener');
        if (anchorTag?.href?.includes('@')) {
          const mailTo = anchorTag.href.split(':');
          anchorLinkTag.innerHTML = mailTo[1];
        } else anchorLinkTag.innerHTML = anchorTag.href;
        if (anchorTag.target) anchorLinkTag.target = anchorTag.target;
        else anchorLinkTag.onclick = (e) => e.preventDefault();
        const removeIcon = document.createElement('span');
        removeIcon.innerHTML = textOptionIcon.linkRemoverIcon;
        removeIcon.style.cursor = 'pointer';
        removeIcon.style.marginTop = '-2px';
        removeIcon.className = 'ml-2 float-right justify-content-end';
        removeIcon.id = CONSTANTS.LOREE_QUICK_LINK_REMOVE_URL;
        removeIcon.onclick = () => this.removeQuickLink(eventTarget);
        this.editCurrentLink(anchorLinkOption, anchorTag);
        anchorLinkOption.appendChild(anchorLinkTag);
        anchorLinkOption.appendChild(removeIcon); // Removal button for quicklinks
        anchorLinkOption.style.display = 'none';
      }
    }
  };

  removeQuickLink(targetValue: HTMLElement) {
    const docFrag: DocumentFragment = document.createDocumentFragment();
    // for unlink image
    const parent = targetValue.parentElement;
    if (parent?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)) {
      parent.style.maxWidth = CONSTANTS.LOREE_SPECIAL_BLOCK_ICON_IMAGE_STATIC_SIZE;
    }
    if (targetValue?.parentElement?.tagName === 'A') {
      if (parent) {
        parent.outerHTML = targetValue.outerHTML;
      }
    } else if (targetValue?.parentNode?.parentElement?.tagName === 'A') {
      if (parent?.parentElement) {
        parent.parentElement.outerHTML = parent?.outerHTML;
      }
    }
    // For unlink text
    if (targetValue.tagName === 'A') {
      while (targetValue.firstChild) {
        docFrag.appendChild(targetValue.firstChild);
      }
      (targetValue.parentNode as Node).replaceChild(docFrag, targetValue);
    }
    this.hideAnchorOption();
  }

  editCurrentLink = (anchorLinkOption: HTMLElement, anchorTag: HTMLAnchorElement) => {
    const anchorEditInput = document.createElement('input');
    anchorEditInput.value = anchorTag.href;
    anchorEditInput.id = CONSTANTS.LOREE_EDIT_ATTACHED_USER_LINK;
    anchorEditInput.className = 'loree-edit-user-attached-link';
    anchorEditInput.style.display = 'none';
    anchorEditInput.style.marginRight = '2.9rem';
    anchorLinkOption.appendChild(anchorEditInput);
  };

  urlValidatorUrl = (): [RegExp] => {
    return [
      /^(?:(?:https?|http|ftp|smtp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/,
    ];
  };

  destroyAnchorLinkPopper = (): void => {
    if (anchorLinkPopperInstance) {
      anchorLinkPopperInstance.destroy();
      anchorLinkPopperInstance = null;
      this.hidePopperInstances();
    }
  };

  hideAnchorOption = () => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const anchorLink = iframeDocument.getElementById(
        CONSTANTS.LOREE_ANCHOR_LINK_WRAPPER,
      ) as HTMLElement;
      if (anchorLink) anchorLink.style.display = 'none';
      if (anchorLink) anchorLink.className = 'loree-text-options-anchor-link d-none';
      const anchorInputLink = iframeDocument?.getElementById(
        CONSTANTS.LOREE_EDIT_ATTACHED_USER_LINK,
      ) as HTMLInputElement;
      if (anchorInputLink !== null) {
        anchorInputLink.remove();
      }
      const removeCloseIcon = iframeDocument.getElementById(CONSTANTS.LOREE_QUICK_LINK_REMOVE_URL);
      removeCloseIcon?.remove();
      iframeDocument.getElementById(CONSTANTS.LOREE_SELECTED_ANCHOR_TEXT_ELEMENT)?.remove();
      iframeDocument.getElementById(CONSTANTS.LOREE_EDIT_USER_ATTACHED_LINK_ICON)?.remove();
      iframeDocument.getElementById(CONSTANTS.LOREE_DELETE_USER_ATTACHED_LINK_ICON)?.remove();
      this.closeImageLinkOption();
    }
    this.destroyAnchorLinkPopper();
  };

  handleLinkCheckBox(checkbox: HTMLInputElement, eventType: string) {
    if (this.validateWindowSelection()) {
      openInNewTab = eventType === 'blur' ? !checkbox.checked : checkbox.checked;
      const selectedElement = this.getDocument()?.getSelection() as Selection;
      const node = selectedElement?.anchorNode as HTMLElement;
      if (openInNewTab) {
        if (node.tagName === 'A') {
          node.setAttribute('target', 'blank');
        } else if (!this.elementPicker.checkTagName(node) || node.tagName === 'LI') {
          node.childNodes.forEach((child) => {
            if ((child as HTMLElement).tagName === 'A')
              (child as HTMLElement).setAttribute('target', 'blank');
          });
        } else {
          (selectedElement?.anchorNode?.parentElement as HTMLElement).setAttribute(
            'target',
            'blank',
          );
        }
      }
      if (!openInNewTab) {
        if (node.tagName === 'A') {
          node.removeAttribute('target');
        } else if (!this.elementPicker.checkTagName(node) || node.tagName === 'LI') {
          node.childNodes.forEach((child) => {
            if ((child as HTMLElement).tagName === 'A')
              (child as HTMLElement).removeAttribute('target');
          });
        } else {
          (node.parentElement as HTMLElement).removeAttribute('target');
        }
      }
      if (node.tagName === 'A') {
        this.closeLinkOption();
      }
    }
  }

  removeAttachedLink = () => {
    if (this.validateWindowSelection()) {
      const iframeDocument = this.getDocument();
      const input = iframeDocument?.getElementById(
        CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT,
      ) as HTMLInputElement;
      input.value = '';
      input.style.borderColor = '';
      this.wordStylerObj?.removeLink();
    }
  };

  // Fetch img for sidebar design section
  fetchImageOnDesignSection = (imgUrl: string): void => {
    const imageWrapper = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_PREVIEW_WRAPPER,
    ) as HTMLElement;
    if (imageWrapper) imageWrapper.innerHTML = '';
    if (imageWrapper) {
      const imgView = document.createElement('img');
      imgView.className = 'designSectionImageView';
      imgView.id = CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_VIEW;
      imgView.src = imgUrl;
      imageWrapper.appendChild(imgView);
    }
  };

  // remove img for sidebar design section
  removeImageOnDesignSection = (): void => {
    const img = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_VIEW,
    ) as HTMLImageElement | null;
    if (img) {
      img.src = '';
    }
  };

  // Filtering Values
  sortComparision = (arg: { name: string }, arg2: { name: string }) => {
    const ProjectTitle1 = arg.name.toUpperCase();
    const ProjectTitle2 = arg2.name.toUpperCase();
    let data = 0;
    if (ProjectTitle1 > ProjectTitle2) {
      data = 1;
    } else if (ProjectTitle1 < ProjectTitle2) {
      data = -1;
    }
    return data;
  };

  uploadTarget = () => {
    const canvasImagesTab = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_NAV_LMS_IMAGE,
    ) as HTMLElement;
    const myImagesTab = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_NAV_MY_IMAGE,
    ) as HTMLElement;
    const loreeImageTab = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_NAV_LOREE_IMAGE,
    ) as HTMLElement;
    let target = 'LOREE';
    if (!lms.getAccess() && myImagesTab.classList.contains('active')) {
      target = 'STANDALONE';
      return target;
    }
    if (sessionStorage.getItem('domainName') === 'D2l' && !canvasImagesTab) {
      target = 'D2LLOREEIMAGES';
      return target;
    } else if (!canvasImagesTab) {
      return target;
    }
    if (canvasImagesTab.classList.contains('active') && lms.getAccess()) {
      target = 'CANVAS';
    }
    if (
      sessionStorage.getItem('domainName') === 'BB' &&
      loreeImageTab.classList.contains('active')
    ) {
      target = 'BBLOREEIMAGES';
      return target;
    }
    if (sessionStorage.getItem('domainName') === 'BB' && canvasImagesTab) {
      target = 'BBLMSIMAGES';
      return target;
    }
    return target;
  };

  resetVideoModalTabs = (): void => {
    const loreeVideos = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_NAV_LOREE_VIDEOS);
    if (loreeVideos) loreeVideos?.classList.add('active');
    const loreeVideosContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_LOREE_VIDEO_CONTAINER,
    );
    if (loreeVideosContainer) {
      loreeVideosContainer?.classList.add('show');
      loreeVideosContainer?.classList.add('active');
    }
  };

  resetImageModalTabs = (): void => {
    const lms = new LmsAccess();
    if (sessionStorage.getItem('domainName') !== 'D2l') {
      const loreeImages = lms.getAccess()
        ? document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_NAV_LMS_IMAGE)
        : document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_NAV_MY_IMAGE);
      if (loreeImages) loreeImages?.classList.add('active');
      const loreeImagesContainer = lms.getAccess()
        ? document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_LMS_IMAGE_CONTAINER)
        : document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_MY_IMAGE_CONTAINER);
      if (loreeImagesContainer) {
        loreeImagesContainer?.classList.add('show');
        loreeImagesContainer?.classList.add('active');
      }
    } else {
      const loreeImages = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_NAV_LOREE_IMAGE);
      if (loreeImages) loreeImages?.classList.add('active');
      const loreeImagesContainer = document.getElementById(
        CONSTANTS.LOREE_IMAGE_MODAL_LOREE_IMAGE_CONTAINER,
      );
      if (loreeImagesContainer) {
        loreeImagesContainer?.classList.add('show');
        loreeImagesContainer?.classList.add('active');
      }
    }
  };

  clearModalActiveTabs = (): void => {
    const modalTab = document.querySelectorAll('.modal-tab.active');
    if (modalTab.length > 0) {
      modalTab[0]?.classList.remove('active');
    }
    const modalTabContainer = document.querySelectorAll('.modal-tab-container.show.active');
    if (modalTabContainer.length > 0) {
      modalTabContainer[0]?.classList.remove('show');
      modalTabContainer[0]?.classList.remove('active');
    }
  };

  modalLoader = (): string => {
    return `
    <div id="modal-loader" class="m-auto justify-content-center">
      <div class="icon rotating">
      ${videoModalIcons.loader}
      </div>
      <div class="title ml-3">Loading...</div>
    </div>
  `;
  };

  initializeModalLoader = (containerType: string, activeTab: string): void => {
    let container;
    switch (containerType) {
      case 'VIDEO':
        container = document.getElementById(CONSTANTS.LOREE_VIDEO_LIST_CONTAINER) as HTMLElement;
        break;
      case 'IMAGE':
        if (activeTab === 'LOREE_IMAGES') {
          container = document.getElementById(CONSTANTS.LOREE_IMAGE_LIST_CONTAINER) as HTMLElement;
        } else if (activeTab === 'CANVAS_IMAGES') {
          container = document.getElementById(
            CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER,
          ) as HTMLElement;
        } else {
          container = document.getElementById(
            CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER,
          ) as HTMLElement;
        }
        break;
    }
    if (container) {
      container.innerHTML = '';
      container.innerHTML = this.modalLoader();
    }
  };

  resetModalTabs = (modalType: string): void => {
    this.clearModalActiveTabs();
    switch (modalType) {
      case 'VIDEO':
        this.resetVideoModalTabs();
        break;
      case 'IMAGE':
        this.resetImageModalTabs();
        break;
    }
  };

  showPreviewModal = (): void => {
    const viewCodeModalBackground = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_BACKGROUND,
    ) as HTMLElement;
    const viewCodeModal = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER,
    ) as HTMLElement;
    viewCodeModalBackground.style.display = 'block';
    viewCodeModal.style.display = 'block';
    const previewIframe: HTMLIFrameElement | null = document.getElementById(
      'loree-preview',
    ) as HTMLIFrameElement;
    if (previewIframe) {
      const previewModal = previewIframe.contentDocument;
      const previewContent = previewModal?.getElementById('previewContent') as HTMLElement;
      const html = this.getHtml();
      previewContent.innerHTML = this.removePointerEvent(html);
      const iframeDocument = this.getDocument();
      const customFontsLink = iframeDocument?.getElementById('customFontsLink') as HTMLElement;
      if (customFontsLink) {
        const clonedCustomFontsLink = customFontsLink.cloneNode(true);
        previewContent.appendChild(clonedCustomFontsLink);
      }
      const interactiveContents = previewIframe?.contentWindow?.document.getElementsByClassName(
        'loree-iframe-content-interactive-element',
      ) as HTMLCollection;
      for (const interactiveElement of interactiveContents) {
        interactiveElement.setAttribute('scrolling', 'yes');
        (interactiveElement as HTMLElement).style.height =
          '150px'; /* interactive height adjustment */
      }
      this.preventCurrentTabUrlRedirection(previewContent);
      previewContent.style.overflowX = 'hidden';
      previewContent.style.wordBreak = 'break-word';
    }
  };

  preventCurrentTabUrlRedirection = (previewContent: {
    getElementsByTagName: (arg0: string) => HTMLCollectionOf<HTMLElement>;
  }) => {
    const urlLink = previewContent.getElementsByTagName('a');
    Array.from(urlLink).forEach((link: HTMLElement) => {
      if (!link.hasAttribute('target')) {
        link.onclick = (e: MouseEvent) => e.preventDefault();
      } else {
        link.setAttribute('rel', 'noreferrer noopener');
      }
    });
  };

  hidePreviewModal = (): void => {
    const viewCodeModalBackground = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_BACKGROUND,
    ) as HTMLElement;
    const viewCodeModal = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER,
    ) as HTMLElement;
    const previewIframe: HTMLIFrameElement | null = document.getElementById(
      'loree-preview',
    ) as HTMLIFrameElement;
    if (previewIframe) {
      const previewModal = previewIframe.contentDocument;
      const previewContent = previewModal?.getElementById('previewContent') as HTMLElement;
      if (previewContent) previewContent.innerHTML = '';
    }
    viewCodeModalBackground.style.display = 'none';
    viewCodeModal.style.display = 'none';
  };

  openParagraphOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    this.hidePopperInstances();
    if (paragraphOptionPopperInstance) {
      paragraphOptionPopperInstance.destroy();
      paragraphOptionPopperInstance = null;
    }
    paragraphOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [5, 5],
          },
        },
      ],
    });
    wrapper.style.display = 'inline-flex';
  };

  closeParagraphOption = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER,
      );
      if (wrapper) wrapper.style.display = 'none';
      if (paragraphOptionPopperInstance) {
        paragraphOptionPopperInstance.destroy();
        paragraphOptionPopperInstance = null;
      }
    }
  };

  openSpacingOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    this.hidePopperInstances();
    if (spacingOptionPopperInstance) {
      spacingOptionPopperInstance.destroy();
      spacingOptionPopperInstance = null;
    }
    spacingOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [0, 5],
          },
        },
      ],
    });
    wrapper.style.display = 'inline-flex';
  };

  closeSpacingOption = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_CONTENT_WRAPPER,
      );
      if (wrapper) wrapper.style.display = 'none';
      if (spacingOptionPopperInstance) {
        spacingOptionPopperInstance.destroy();
        spacingOptionPopperInstance = null;
      }
    }
  };

  openWordOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    this.hidePopperInstances();
    if (wordOptionPopperInstance) {
      wordOptionPopperInstance.destroy();
      wordOptionPopperInstance = null;
    }
    wordOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [0, 5],
          },
        },
      ],
    });
    wrapper.style.display = 'inline-flex';
  };

  closeWordOption = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_WORD_CONTENT_WRAPPER,
      );
      if (wrapper) wrapper.style.display = 'none';
      if (wordOptionPopperInstance) {
        wordOptionPopperInstance.destroy();
        wordOptionPopperInstance = null;
      }
    }
  };

  openLanguageOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    this.hidePopperInstances();
    const selectedElement = this.getSelectedElement();
    const iframeDocument = this.getDocument();
    if (!iframeDocument || !selectedElement) return;
    if (process.env.REACT_APP_ENABLE_LOCALISATION_MINI_MENU === 'true') {
      const languageOptionsWrapper = getIFrameElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
      );
      this.hideLanguageIsoLabel();
      this.showLanguageIsoLabel(selectedElement);
      this.higlightSelectedLanguage(languageOptionsWrapper, selectedElement);
      languageOptionsWrapper.style.display = 'flex';
      languageOptionsWrapper.scrollTop = 0;
    }
    if (languageOptionPopperInstance) {
      languageOptionPopperInstance.destroy();
      languageOptionPopperInstance = null;
    }
    languageOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [0, 5],
          },
        },
      ],
    });
  };

  higlightSelectedLanguage = (
    languageOptionsWrapper: HTMLElement,
    selectedElement: HTMLElement,
  ) => {
    const lang = selectedElement.hasAttribute('lang') ? selectedElement.lang : 'en';
    const index = CONSTANTS.LOREE_MINI_MENU_LANGUAGE_LISTS.findIndex((obj) => obj.isoCode === lang);
    const clonedChildren = [...languageOptionsWrapper.children];
    const orderedLangList = clonedChildren
      .sort((a: Element, b: Element) =>
        (a.lastElementChild?.textContent ?? '')
          .toLowerCase()
          .localeCompare((b.lastElementChild?.textContent ?? '').toLocaleUpperCase()),
      )
      .splice(index, 1)[0];
    clonedChildren.unshift(orderedLangList);
    languageOptionsWrapper.replaceChildren(...clonedChildren);
    for (const langBtn of languageOptionsWrapper.children) {
      if (!selectedElement.hasAttribute('lang') && (langBtn as HTMLElement).id === 'en') {
        (langBtn as HTMLElement).classList.add('active-language-option');
      } else if ((langBtn as HTMLElement).id !== selectedElement.lang) {
        (langBtn as HTMLElement).classList.remove('active-language-option');
      } else {
        (langBtn as HTMLElement).classList.add('active-language-option');
      }
    }
  };

  closeLanguageOption = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const button = getIFrameElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON);
    if (!button) return;
    if ((button.lastChild as SVGElement).style.transform)
      (button.lastChild as SVGElement).style.removeProperty('transform');
    const wrapper = getIFrameElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
    );
    if (wrapper) wrapper.style.display = 'none';
    if (languageOptionPopperInstance) {
      languageOptionPopperInstance.destroy();
      languageOptionPopperInstance = null;
    }
  };

  updateLanguageIsoLabelPopperInstance = () => {
    languageIsoLabelPopperInstance?.update();
  };

  showLanguageIsoLabel = (selectedElement: HTMLElement): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument || !selectedElement) return;
    const isoLabel = getIFrameElementById(CONSTANTS.LOREE_LANGUAGE_ISO_LABEL_BUTTON);
    if (!isoLabel) return;
    isoLabel.innerHTML = selectedElement.hasAttribute('lang')
      ? `(${selectedElement.lang})`
      : '(en)';
    if (languageIsoLabelPopperInstance) {
      languageIsoLabelPopperInstance.destroy();
      languageIsoLabelPopperInstance = null;
    }
    languageIsoLabelPopperInstance = createPopper(selectedElement, isoLabel, {
      placement: 'right',
      modifiers: [
        {
          name: 'flip',
        },
        {
          name: 'eventListeners',
          options: { scroll: false },
        },
        {
          name: 'offset',
          options: {
            offset: [-selectedElement.offsetHeight / 2 + 12, -36],
          },
        },
      ],
    });
    isoLabel.style.display = 'inline-flex';
  };

  hideLanguageIsoLabel = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const isoLabel = getIFrameElementById(CONSTANTS.LOREE_LANGUAGE_ISO_LABEL_BUTTON);
    if (!isoLabel) return;
    isoLabel.style.display = 'none';
    if (languageIsoLabelPopperInstance) {
      languageIsoLabelPopperInstance.destroy();
      languageIsoLabelPopperInstance = null;
    }
  };

  openAlignmentOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    this.hidePopperInstances();
    if (alignmentOptionPopperInstance) {
      alignmentOptionPopperInstance.destroy();
      alignmentOptionPopperInstance = null;
    }
    alignmentOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [0, 5],
          },
        },
      ],
    });
    wrapper.style.display = 'inline-flex';
  };

  closeAlignmentOption = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_CONTENT_WRAPPER,
      );
      if (wrapper) wrapper.style.display = 'none';
      if (alignmentOptionPopperInstance) {
        alignmentOptionPopperInstance.destroy();
        alignmentOptionPopperInstance = null;
      }
    }
  };

  openImageLinkOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    openExternalinkOptionValuesCheck();
    externalLinkCheckBoxCheck(selectedElement);
    if (linkOptionPopperInstance) {
      linkOptionPopperInstance.destroy();
      linkOptionPopperInstance = null;
    }
    linkOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [0, 5],
          },
        },
      ],
    });
    wrapper.style.display = 'inline-flex';
  };

  openLinkOption = (button: HTMLElement, wrapper: HTMLElement): void => {
    this.hidePopperInstances();
    const iframeDocument = this.getDocument();
    const selectedText = iframeDocument?.getSelection();
    const anchorNode = selectedText?.anchorNode as HTMLAnchorElement;
    const anchorTag = this.wordStylerObj?.findAnchorNode(anchorNode);
    anchorUrl = anchorTag?.href ? anchorTag.href : '';
    fetchOpenInNewTab = anchorTag?.target ? anchorTag?.target.includes('blank') : false;
    const input = iframeDocument?.getElementById(
      CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT,
    ) as HTMLInputElement;
    input.style.borderColor = '';
    input.value = anchorUrl;
    input.addEventListener('keydown', (e: KeyboardEvent) => {
      if (e.keyCode === 13) e.preventDefault();
    });
    const checkBox = iframeDocument?.getElementById('link-option-tab-checkbox') as HTMLInputElement;
    checkBox.checked = fetchOpenInNewTab;
    this.disableNewTabLinkForD2L(checkBox);
    if (linkOptionPopperInstance) {
      linkOptionPopperInstance.destroy();
      linkOptionPopperInstance = null;
    }
    linkOptionPopperInstance = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: [0, 5],
          },
        },
      ],
    });
    wrapper.style.display = 'inline-flex';
  };

  /**
   *
   * @deprecated
   */
  disableNewTabLinkForD2L = (checkBox: HTMLInputElement): void => {
    if (isD2l()) {
      checkBox.checked = true;
      checkBox.disabled = true;
    }
  };

  closeLinkOption = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER,
      );
      if (wrapper) wrapper.style.display = 'none';
      if (linkOptionPopperInstance) {
        linkOptionPopperInstance.destroy();
        linkOptionPopperInstance = null;
      }
    }
  };

  closeImageLinkOption = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_IMAGE_OPTIONS_LINK_CONTENT_WRAPPER,
      );
      if (wrapper) wrapper.style.display = 'none';
      if (linkOptionPopperInstance) {
        linkOptionPopperInstance.destroy();
        linkOptionPopperInstance = null;
      }
    }
  };

  /**
   * @deprecated
   */
  urlLinkPopperEvents = (targetElement: HTMLElement) => {
    const tags = ['P', 'SPAN', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'IMG', 'LI', 'UL', 'OL'];
    const anchorNode = findUpwardsByTag(targetElement, 'a');

    if (anchorNode) {
      // this.initiateAnchorLinkPopper(targetElement);
      if (targetElement.parentElement?.parentElement) {
        showLinkInfoPopup(
          targetElement.parentElement.parentElement,
          () => {
            removeLinkFromElement(targetElement);
            hideLinkInfoPopup();
            hideLinkEditPopup();
          },
          anchorNode.href,
          getIFrameElement(),
        );
      }
    } else if (isElementOneOf(targetElement, tags)) {
      // this.resetAnchorUrl();
    }
  };

  handleContentEditable = (selectedElement: HTMLElement): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const iframeElements: HTMLCollectionOf<Element> = iframeDocument.getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT,
    );
    if (iframeElements.length > 0) {
      Array.from(iframeElements).forEach((element) => {
        if (element.tagName !== 'LI' && element.tagName !== 'OL' && element.tagName !== 'UL') {
          this.removeContentEditableAttribute(element as HTMLElement);
        }
      });
    }
    if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
      let currentElement = selectedElement;
      if (currentElement?.tagName === 'LI')
        currentElement = currentElement.parentElement as HTMLElement;
      this.addContentEditableAttribute(currentElement);
    }
  };

  hidePopperInstances = (): void => {
    this.closeForegroundColorPicker();
    this.closeBackgroundColorPicker();
    this.closeBorderRowColumnColorPicker();
    this.closeBackgroundRowColumnColorPicker();
    this.closeBorderColorPicker();
    this.closeParagraphOption();
    this.closeSpacingOption();
    this.closeWordOption();
    this.closeAlignmentOption();
    this.closeLinkOption();
    this.closeImageLinkOption();
    this.closeLanguageOption();
  };

  /**
   * User paste the content to contentWrapper.
   */
  handleContentPaste = (event: ClipboardEvent): void => {
    event.preventDefault();
    const element = event.target as HTMLElement;
    if (this.identifySourceBlock(element)) {
      this.handlePasteOnCopyPasteBlock(event);
    } else {
      if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT))
        this.pasteCopiedContent(event);
    }
    this.hideLanguageIsoLabel();
    this.showLanguageIsoLabel(this.getSelectedElement() as HTMLElement);
    this.handleSelectedContentChanges();
  };

  identifySourceBlock = (element: HTMLElement): boolean => {
    element = this.elementPicker.getElement(element, 'identifySourceBlock');
    if (element?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE)) return true;
    return false;
  };

  handlePasteOnCopyPasteBlock = (event: ClipboardEvent): void => {
    if (event.clipboardData) {
      const copiedHtml = event.clipboardData.getData('text/html');
      const wrapper = document.createElement('div');
      wrapper.innerHTML = copiedHtml;
      const metaTags = wrapper.getElementsByTagName('meta');
      Array.from(metaTags).forEach((metaTag) => {
        metaTag.remove();
      });
      if (!selectedElement) return;
      const cleanedHtml = this.convertContent(wrapper.innerHTML);
      this.removeSelectedElement();
      this.attachNewElement(selectedElement, cleanedHtml);
    }
  };

  removeSelectedElement() {
    const selected = this.getDocument()?.getSelection() as Selection;
    const range = selected.getRangeAt(0);
    const preCaretRange = range.cloneRange();
    preCaretRange.deleteContents();
  }

  attachNewElement(selectedElement: HTMLElement, cleanedHtml: string) {
    selectedElement = this.elementPicker.getElement(selectedElement, 'attachNewElement');
    selectedElement.innerHTML += cleanedHtml;
  }

  pasteCopiedContent(event: ClipboardEvent) {
    const plaintext = event?.clipboardData?.getData('text') as string;
    const selected = this.getDocument()?.getSelection() as Selection;
    const range = selected.getRangeAt(0);
    const preCaretRange = range.cloneRange();
    const copiedNode = document.createTextNode(plaintext);
    preCaretRange.deleteContents();
    preCaretRange.insertNode(copiedNode);
    this.setCursorPosition(copiedNode);
  }

  setCursorPosition(copiedNode: Node) {
    let caretRange;
    let selection;
    if (document.createRange) {
      caretRange = document.createRange();
      caretRange.selectNodeContents(copiedNode);
      caretRange.collapse(false);
      selection = this.getDocument()?.getSelection();
      selection?.removeAllRanges();
      selection?.addRange(caretRange);
    }
  }

  insertNode(preCaretRange: Range, element: Node) {
    preCaretRange.insertNode(element);
  }

  /** Undo Redo history.**/
  handleSelectedContentChanges = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const contentWrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      if (contentWrapper) {
        if (userClickAction < history.length - 1) {
          history = history.slice(0, userClickAction + 1);
        }
        history.push(this.getHtml());
        userClickAction += 1;
      }
    }
  };

  handleUndoHistory = (): void => {
    if (userClickAction > 0) {
      this.hideSelections();
      userClickAction -= 1;
      this.storeUndoRedoContent();
    }
  };

  handleRedoHistory = (): void => {
    if (userClickAction < history.length - 1) {
      this.hideSelections();
      userClickAction += 1;
      this.storeUndoRedoContent();
    }
  };

  storeUndoRedoContent = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const contentWrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      if (contentWrapper) {
        if (history[userClickAction]) {
          contentWrapper.innerHTML = history[userClickAction];
        } else {
          contentWrapper.innerHTML = history[0];
        }
      }
    }
  };

  resetRedoUndoHistory = (): void => {
    history.length = 0;
    userClickAction = 0;
  };

  setInitialHistory = (): void => {
    history[0] = this.getHtml();
  };

  viewComponentsOutline = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const contentWrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      if (contentWrapper) {
        contentWrapper.parentElement?.classList.toggle('components-outline');
      }
    }
  };

  /**
   * block options.
   */

  showBlockOptions = (isRow: boolean, isColumn: boolean, isElement: boolean): void => {
    const iframeDocument = this.getDocument();
    const isBlockSection = isRow || isColumn;
    if (iframeDocument) {
      const blockOption = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER}`,
      ) as HTMLElement;
      void this.checkBlockOptions(isRow, isColumn, isElement);
      const testBorder = iframeDocument.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER);
      testBorder?.classList.remove('row-panel-active');
      let position: string = 'top';
      let placements = ['top', 'bottom'];
      if (isRow || isColumn) {
        position = 'top-start';
        placements = [];
      }
      if (isRow || selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_MENU)) {
        const testBorder = iframeDocument.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER);
        testBorder?.classList.add('row-panel-active');
      }
      if (isColumn) {
        const testBorder = iframeDocument.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER);
        testBorder?.classList.remove('row-panel-active');
      }
      if (this.isNavigationBlock()) {
        this.blockOptionsPlacement(
          selectedElement?.closest('.row') as HTMLElement,
          blockOption,
          isBlockSection,
          position,
          placements,
        );
      } else {
        this.blockOptionsPlacement(
          selectedElement as HTMLElement,
          blockOption,
          isBlockSection,
          position,
          placements,
        );
      }
      if (blockOption !== null) {
        blockOption.style.display = 'flex';
      }
      this.checkForBlockOptionFeatures(blockOption);
    }
  };

  checkForBlockOptionFeatures = (blockOption: HTMLElement): void => {
    // hiding popper when all block options are unselected
    const optionsDisplayProp = [];
    blockOption?.childNodes.forEach((element: ChildNode) => {
      if ((element as HTMLElement).style.display !== 'none') {
        optionsDisplayProp.push(element);
      }
    });
    if (optionsDisplayProp.length < 1) {
      this.hideBlockOptions();
    }
  };

  destroyElementPopperInstance = (): void => {
    if (blockOptionsPopperInstance) {
      blockOptionsPopperInstance.destroy();
      blockOptionsPopperInstance = null;
    }
  };

  hideBlockOptions = (): void => {
    const blockOptions = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER,
    ) as HTMLElement;
    if (!blockOptions) return;
    this.destroyElementPopperInstance();
    blockOptions.style.display = 'none';
    this.closeImageLinkOption();
    this.closeLanguageOption();
  };

  checkBlockOptions = (isRow: boolean, isColumn: boolean, isElement: boolean) => {
    let topSibling = false;
    let bottomSibling = false;
    let leftSibling = false;
    let rightSibling = false;
    let navMenuItem = false;
    if (selectedElement) {
      if (selectedElement != null && this.isNavigationBar()) {
        // for special element icons.
        const specialElement = selectedElement.closest('.row') as HTMLElement;
        isColumn = false;
        isElement = true;
        if (isRow || !isColumn) {
          topSibling =
            specialElement.previousElementSibling != null &&
            specialElement.previousElementSibling?.tagName !== 'LINK';
          bottomSibling =
            specialElement.nextElementSibling != null &&
            specialElement.nextElementSibling?.tagName !== 'SCRIPT';
        }
      } else if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)) {
        const menuItemList = selectedElement?.parentElement as HTMLElement;
        navMenuItem = true;
        leftSibling = menuItemList.previousElementSibling != null;
        rightSibling = menuItemList.nextElementSibling != null;
      } else if (selectedElement?.classList.contains(CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)) {
        navMenuItem = true;
        leftSibling = false;
        rightSibling = false;
      } else {
        if (isRow || !isColumn) {
          if (
            (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) &&
              selectedElement.parentElement?.tagName === 'A') ||
            selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)
          ) {
            const imageLinkElement = selectedElement.parentElement;
            topSibling =
              imageLinkElement?.previousElementSibling != null &&
              imageLinkElement?.previousElementSibling?.tagName !== 'LINK';
            bottomSibling =
              imageLinkElement?.nextElementSibling != null &&
              imageLinkElement?.nextElementSibling?.tagName !== 'SCRIPT';
          } else {
            topSibling =
              selectedElement.previousElementSibling != null &&
              selectedElement.previousElementSibling?.tagName !== 'LINK';
            bottomSibling =
              selectedElement.nextElementSibling != null &&
              selectedElement.nextElementSibling?.tagName !== 'SCRIPT';
          }
        }
        if (isColumn && selectedElement) {
          leftSibling = selectedElement.previousElementSibling != null;
          rightSibling = selectedElement.nextElementSibling != null;
        }
      }
    }
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      /**
       * Save button
       * Check if selectedElement is row or element
       */
      const saveBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_SAVE_BUTTON}`,
      ) as HTMLElement;
      const isEditCustomBlock = isSelectionFirstElementInContent();
      if (saveBtn) {
        saveBtn.style.display = 'none';
        if (
          (isRow && featuresList?.saveascustomrow) ||
          (isColumn && featuresList?.saveascustomelement)
        ) {
          if (saveBtn) {
            saveBtn.style.display = 'inline-flex';
          }
        }
        if (isRow && featuresList?.saveascustomrow) {
          saveBtn.classList.remove('column-active-icon');
          saveBtn.setAttribute('data-original-title', 'Save Row');
        }
        if (isColumn && featuresList?.saveascustomelement) {
          saveBtn.classList.add('column-active-icon');
          saveBtn.setAttribute('data-original-title', 'Save Elements');
        }
      }
      /**
       * Duplicate button
       * Check if selectedElement is row or element
       */
      const duplicateBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_DUPLICATE_BUTTON}`,
      ) as HTMLElement;
      if (duplicateBtn) {
        duplicateBtn.style.display = 'none';
        if (
          (isRow && featuresList?.duplicaterow) ||
          (isElement && featuresList?.duplicateelement)
        ) {
          if (duplicateBtn && !navMenuItem) {
            duplicateBtn.style.display = 'inline-flex';
          }
        }
        if (
          isEditCustomBlock &&
          customBlockEditStatus &&
          customBlockType === 'Row' &&
          selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)
        ) {
          duplicateBtn.style.display = 'none';
        }
      }

      /**
       * Delete button
       */
      const deleteBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON}`,
      ) as HTMLElement;
      if (deleteBtn) {
        if (showDelete(isElement)) {
          deleteBtn.style.display = 'none';
        } else {
          deleteBtn.style.display = 'inline-flex';
        }
      }

      const externalLinkBtn = iframeDocument.getElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_EXTERNAL_LINK_BUTTON,
      ) as HTMLElement;
      if (externalLinkBtn) {
        externalLinkBtn.style.display = showLinkOption() ? 'inline-flex' : 'none';
      }

      const languageBtn = iframeDocument.getElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON,
      ) as HTMLElement;
      if (languageBtn) {
        languageBtn.style.display = showLanguageOption(this.getSelectedElement())
          ? 'inline-flex'
          : 'none';
      }

      const interactiveEditButton = iframeDocument.getElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON,
      ) as HTMLButtonElement;
      if (interactiveEditButton) {
        interactiveEditButton.style.display = isInteractiveElement() ? 'inline-flex' : 'none';
      }

      /**
       * Edit row button
       * Check if selectedElement is row
       */
      const editRowBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_ROW_EDIT_BUTTON}`,
      ) as HTMLElement;
      if (editRowBtn) {
        editRowBtn.style.display = 'none';
        if (isRow) {
          editRowBtn.style.display = 'inline-flex';
        }
      }

      /**
       * Edit column button
       * Check if selectedElement is column
       */
      const editColumnBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_COLUMN_EDIT_BUTTON}`,
      ) as HTMLElement;
      if (editColumnBtn) {
        editColumnBtn.style.display = 'none';
        if (isColumn) {
          editColumnBtn.style.display = 'inline-flex';
        }
      }

      /**
       * Move right and left button
       * Check if selectedElement is column
       */
      const moveRightBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_RIGHT_BUTTON}`,
      ) as HTMLElement;
      const moveLeftBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_LEFT_BUTTON}`,
      ) as HTMLElement;
      if (moveRightBtn && moveLeftBtn) {
        moveRightBtn.style.display = 'none';
        moveLeftBtn.style.display = 'none';
        if (
          (isRow && featuresList?.moverow) ||
          (isColumn && featuresList?.movecolumn) ||
          (isElement && featuresList?.moveelement)
        ) {
          if (isColumn || navMenuItem) {
            if (rightSibling && leftSibling) {
              moveRightBtn.style.display = 'inline-flex';
              moveLeftBtn.style.display = 'inline-flex';
            }
            if (rightSibling) {
              moveRightBtn.style.display = 'inline-flex';
            }
            if (leftSibling) {
              moveLeftBtn.style.display = 'inline-flex';
            }
          }
        }
      }

      /**
       * Move top and bottom button
       * Check if selectedElement is column
       */
      const moveTopBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON}`,
      ) as HTMLElement;
      const moveBottomBtn = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON}`,
      ) as HTMLElement;
      if (moveTopBtn && moveBottomBtn) {
        moveTopBtn.style.display = 'none';
        moveBottomBtn.style.display = 'none';
        if (!isColumn && !navMenuItem) {
          if (topSibling && bottomSibling) {
            moveTopBtn.style.display = 'inline-flex';
            moveBottomBtn.style.display = 'inline-flex';
          }
          if (topSibling) {
            moveTopBtn.style.display = 'inline-flex';
          }
          if (bottomSibling) {
            moveBottomBtn.style.display = 'inline-flex';
          }
        }
      }

      /**
       * Copy and paste button
       * Check if selectedElement is column
       */
      const copyBtn = iframeDocument.getElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_COPY_BUTTON,
      ) as HTMLElement;
      const pasteBtn = iframeDocument.getElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_PASTE_BUTTON,
      ) as HTMLElement;
      if (copyBtn && pasteBtn) {
        copyBtn.style.display = 'none';
        pasteBtn.style.display = 'none';
        if ((isColumn || isElement) && !navMenuItem) {
          if (copiedElement && !isElement) {
            pasteBtn.style.display = 'inline-flex';
          } else {
            copyBtn.style.display = 'inline-flex';
          }
        }
      }
    }
  };

  isSave = (): void => {
    if (!selectedElement) return;

    const isRow = isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
    const isElement = isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
    const elementType = isRow ? 'Row' : isElement ? 'Element' : '';
    const isEditCustomBlock = isSelectionFirstElementInContent();

    this.enableSaveCustomBLockModal(elementType);

    if (
      customBlockEditStatus &&
      isEditCustomBlock &&
      customBlockType &&
      customBlockType.includes(elementType)
    ) {
      getEditContentDetails();
      const infoText = getEditorElementById(CONSTANTS.LOREE_UPDATE_INFO_CONTENT);
      if (!infoText) return;
      infoText.classList.remove('d-none');
    }
  };

  enableSaveCustomBLockModal = (elementType: string) => {
    const blocksNameAlert = getEditorElementById(CONSTANTS.LOREE_ROW_NAME) as HTMLDivElement;
    const customBlockModalHeader = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_TITLE,
    ) as HTMLHeadElement;
    const customBlockNameLabel = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME_LABEL,
    ) as HTMLLabelElement;
    const customBlockNameInput = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME,
    ) as HTMLDivElement;
    const customBlockBackdrop = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_BACKDROP,
    ) as HTMLDivElement;
    const customBlockModal = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_WRAPPER,
    ) as HTMLDivElement;
    const saveModalSaveButton = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON,
    ) as HTMLButtonElement;

    customBlockNameInput?.setAttribute(
      'placeholder',
      'Custom ' + elementType.toLowerCase() + ' name',
    );
    customBlockBackdrop?.classList?.remove('d-none');
    customBlockModal?.classList?.remove('d-none');

    if (customBlockModalHeader && customBlockNameLabel && blocksNameAlert && saveModalSaveButton) {
      customBlockModalHeader.innerHTML = 'Save Custom ' + elementType;
      customBlockNameLabel.innerHTML = 'Custom ' + elementType + ' Name*';
      blocksNameAlert.innerHTML = `Custom ${elementType} name already exists`;
      saveModalSaveButton?.setAttribute('disabled', 'true');
      saveModalSaveButton.innerHTML = 'Save';
    }
  };

  isDuplicate = async (): Promise<void> => {
    let clonedElement: HTMLElement;
    if (selectedElement) {
      if (this.isNavigationBar()) {
        this.removeHighlightSelectedElement(selectedElement.closest('.row') as HTMLElement);
        const duplicateSpecialElements = selectedElement.closest('.row') as HTMLElement;
        clonedElement = duplicateSpecialElements.cloneNode(true) as HTMLElement;
        let nextSibling = null;
        if (selectedElement.id === CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS) {
          nextSibling = duplicateSpecialElements.nextSibling;
        }
        if (duplicateSpecialElements.parentElement) {
          duplicateSpecialElements.parentElement.insertBefore(clonedElement, nextSibling);
        }
        this.hideHamburgerIconForMenu();
      } else {
        this.removeHighlightSelectedElement(selectedElement);
        clonedElement = selectedElement.cloneNode(true) as HTMLElement;
        if (selectedElement.parentNode) {
          selectedElement.parentNode.insertBefore(clonedElement, selectedElement.nextSibling);
        }

        clonedElement?.querySelectorAll('a').forEach((e) => {
          e.onclick = disableImageLinkClick;
        });
      }
      selectedElement.click();
      await this.a11yCheckerNotification();
    }
    this.handleSelectedContentChanges();
    this.hideAnchorOption();
    this.hideBlockOptionsToSelectedElements();
    this.hideDesignSection();
    this.hideContainerDesignSection();
    this.hideMenuDesignSection();
    this.hideTableDesignSection();
    this.hideLanguageIsoLabel();
  };

  isDelete = (): void => {
    if (selectedElement) {
      // All alerts are hidden behind for UI Demo. Will later be called when respective functionalities are written.
      duplicateColumnAlert();
      successMessageContainerDisappear();
      newPageAlertMessage();
      newPageAlertContainerDisappear();
      newPageOptionsAlert();
      this.hideLanguageIsoLabel();
      hideLinkInfoPopup();
      hideLinkEditPopup();
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) {
        // delete column popup
        deleteColumnAlert();
        const deleteYes = document.getElementById('deleteYesButton') as HTMLElement;
        if (deleteYes) {
          deleteYes.onclick = (): void => void this.deleteColumn();
        }
      } else if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ROW) &&
        !selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)
      ) {
        // delete row popup
        deleteRowAlert();
        const deleteYes = document.getElementById('deleteRowYesButton') as HTMLElement;
        deleteYes.onclick = (): void => void this.deleteRow();
      } else {
        if (
          selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
          selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER) ||
          selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER) ||
          selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER) ||
          selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER)
        ) {
          this.collapseDesignSection();
          this.disableElementSection();
          this.hideSubSidebar();
          this.hideTableDesignSection();
          this.hideMenuDesignSection();
          this.hideContainerDesignSection();
          this.hideHamburgerIconForTable();
          this.hideHamburgerIconForMenu();
          this.hideHamburgerIconForContainer();
        }
        if (this.isNavigationBar()) {
          const deleteSpecialElements = selectedElement.closest('.row') as HTMLElement;
          deleteSpecialElements.remove();
          this.hideMenuDesignSection();
          this.hideHamburgerIconForMenu();
          // disable navbar selection.
        }
        // setting min-height on deleting divider
        const isChildPresent = selectedElement?.parentElement?.childElementCount;
        if (
          selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER) &&
          isChildPresent
        ) {
          if (isChildPresent === 1) {
            selectedElement?.parentElement?.setAttribute('style', 'min-height: 80px');
          }
        }
        if (selectedElement.tagName === 'LI') {
          if (
            selectedElement.previousElementSibling === null &&
            selectedElement.nextElementSibling === null
          ) {
            selectedElement = selectedElement.parentElement as HTMLElement;
          }
        }
        const elementParent = selectedElement.parentElement;
        selectedElement.remove();
        if (
          !elementParent?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ROW) &&
          !elementParent?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) &&
          elementParent?.tagName !== 'TD' &&
          elementParent?.tagName !== 'TH' &&
          elementParent?.childNodes.length === 0
        ) {
          elementParent.remove();
        }
        this.hideTextOptions();
        this.handleSelectedContentChanges();
        this.hideAddElementButtonToSelectedElement();
        this.hideBlockOptionsToSelectedElements();
        this.hideCloseElementButtonToSelectedElement();
        void this.a11yCheckerNotification();
      }
      this.hideBlockOptionsToSelectedElements();
      this.hideAddElementButtonToSelectedElement();
      this.hideCloseElementButtonToSelectedElement();
    }
    this.hideDesignSection();
    this.hideContainerDesignSection();
    this.hideAnchorOption();
  };

  a11yCheckerNotification = async () => {
    const { check } = a11yGlobalContext.value;
    if (getEditorElementById(CONSTANTS.LOREE_MAIN_A11YCHECKER)) {
      await check(false);
    }
  };

  deleteColumn = async (): Promise<void> => {
    const deleteRadioButton: HTMLElement | null = document.getElementById('deleteColumnYes');
    if (selectedElement?.parentNode) {
      if ((deleteRadioButton as HTMLInputElement).checked) {
        const rightSibling: boolean = selectedElement.nextElementSibling != null;
        const leftSibling: boolean = selectedElement.previousElementSibling != null;
        if (rightSibling || leftSibling) {
          selectedElement.remove();
          this.hideAddElementButtonToSelectedElement();
          this.hideCloseElementButtonToSelectedElement();
          this.hideBlockOptionsToSelectedElements();
        } else {
          (selectedElement.parentNode as HTMLElement).remove();
          this.hideHamburgerIconForRow();
          this.hideAddElementButtonToSelectedElement();
          this.hideCloseElementButtonToSelectedElement();
          this.hideBlockOptionsToSelectedElements();
        }
      } else {
        selectedElement.innerHTML = '';
      }
      hideDeleteColumnAlert();
      this.handleSelectedContentChanges();
    }
    await this.a11yCheckerNotification();
  };

  deleteRow = async (): Promise<void> => {
    const deleteRadioButton: HTMLElement | null = document.getElementById('deleteRowYes');
    if (selectedElement?.parentNode) {
      if ((deleteRadioButton as HTMLInputElement).checked) {
        selectedElement.remove();
        this.hideAddElementButtonToSelectedElement();
        this.hideCloseElementButtonToSelectedElement();
        this.hideBlockOptionsToSelectedElements();
        this.hideInsertRowButton();
      } else {
        for (const innerColumn of selectedElement.childNodes) {
          (innerColumn as HTMLElement).innerHTML = '';
        }
      }
      hideDeleteRowAlert();
      this.handleSelectedContentChanges();
    }
    await this.a11yCheckerNotification();
  };

  // clear entire data inside editor
  deleteContent = () => {
    document.getElementById('modal-loader')?.classList.add('d-none');
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const editor = iframeDocument.getElementById(
        CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
      ) as HTMLElement;
      editor.innerHTML = '';
      this.handleAfterDeleteContent();
    }
  };

  handleAfterDeleteContent = () => {
    this.hideSelections();
    this.handleSelectedContentChanges();
    this.hideAllElementSeciton();
    savePageAlert(translate('alert.pagecontentsdeletedsuccessfully'));
    savePageAlertContainerDisapper();
  };

  hideAllElementSeciton = () => {
    const siderBarElements = Array.from(document.getElementsByClassName('section'));
    siderBarElements.forEach((section: _Any) => {
      section.style.display = 'none';
    });
  };

  hideSelections = (): void => {
    this.hidePopperInstances();
    this.hideLanguageIsoLabel();
    this.hideTextOptions();
    this.hideAnchorOption();
    this.collapseDesignSection();
    this.disableElementSection();
    this.hideDesignSection();
    this.hideSubSidebar();
    this.hideTableDesignSection();
    this.hideMenuDesignSection();
    this.hideContainerDesignSection();
    this.hideHamburgerIconForRow();
    this.hideHamburgerIconForColumn();
    this.hideHamburgerIconForTable();
    this.hideHamburgerIconForMenu();
    this.hideHamburgerIconForContainer();
    this.hideAddElementButtonToSelectedElement();
    this.hideBlockOptionsToSelectedElements();
    this.hideCloseElementButtonToSelectedElement();
    this.hideAddRowButtonToSelectedContainer();
    this.hideCloseRowButtonToSelectedContainer();
    this.hideInsertRowButton();
    this.hideCloseInsertRowButton();
    this.showAddRowButton();
    this.hideCloseRowButton();
  };

  insertAfter = (el: HTMLElement, referenceNode: HTMLElement): void => {
    if (referenceNode.parentNode) {
      referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }
  };

  insertBefore = (el: HTMLElement, referenceNode: HTMLElement): void => {
    if (referenceNode.parentNode) {
      referenceNode.parentNode.insertBefore(el, referenceNode);
    }
  };

  moveElementLeft = (): void => {
    if (selectedElement) {
      let currentSection: HTMLElement | null = null;
      this.closeLanguageOption();
      if (languageIsoLabelPopperInstance) languageIsoLabelPopperInstance.update();
      if (this.isNavigationBar()) {
        currentSection = selectedElement.closest('.row') as HTMLElement;
        const topSibling =
          currentSection.previousElementSibling != null &&
          currentSection.previousElementSibling?.tagName !== 'LINK';
        if (!topSibling) return;
        this.moveContentLeft(currentSection);
      } else if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)) {
        currentSection = selectedElement.parentElement as HTMLElement;
        const topSibling =
          currentSection.previousElementSibling != null &&
          currentSection.previousElementSibling?.tagName !== 'LINK';
        if (!topSibling) return;
        this.moveContentLeft(currentSection);
      } else if (
        (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) &&
          selectedElement.parentElement?.tagName === 'A') ||
        selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)
      ) {
        currentSection = selectedElement.parentElement;
        const topSibling =
          currentSection?.previousElementSibling != null &&
          currentSection?.previousElementSibling?.tagName !== 'LINK';
        if (!topSibling || !currentSection) return;
        this.moveContentLeft(currentSection);
      } else {
        currentSection = selectedElement;
        this.moveContentLeft(currentSection);
      }
    }
    if (blockOptionsPopperInstance) blockOptionsPopperInstance?.update();
    if (blockEditOptionsPopperInstance) blockEditOptionsPopperInstance?.update();
    this.handleSelectedContentChanges();
  };

  moveContentLeft = (selectedElement: HTMLElement) => {
    this.insertBefore(selectedElement, selectedElement.previousElementSibling as HTMLElement);
    selectedElement.click();
    this.showElementSectionOptions();
    a11yUpdateHighlightsService.updateHighlightsIdentity();
  };

  moveElementRight = (): void => {
    if (selectedElement) {
      let currentSection: HTMLElement | null = null;
      this.closeLanguageOption();
      if (languageIsoLabelPopperInstance) languageIsoLabelPopperInstance.update();
      if (this.isNavigationBar()) {
        currentSection = selectedElement.closest('.row') as HTMLElement;
        const bottomSibling =
          currentSection.nextElementSibling != null &&
          currentSection.nextElementSibling?.tagName !== 'SCRIPT';
        if (!bottomSibling) return;
        this.moveContentRight(currentSection);
      } else if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)) {
        currentSection = selectedElement.parentElement as HTMLElement;
        const bottomSibling =
          currentSection.nextElementSibling != null &&
          currentSection.nextElementSibling?.tagName !== 'SCRIPT';
        if (!bottomSibling) return;
        this.moveContentRight(currentSection);
      } else if (
        (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) &&
          selectedElement.parentElement?.tagName === 'A') ||
        selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)
      ) {
        currentSection = selectedElement.parentElement;
        const bottomSibling =
          currentSection?.nextElementSibling != null &&
          currentSection?.nextElementSibling?.tagName !== 'SCRIPT';
        if (!bottomSibling || !currentSection) return;
        this.moveContentRight(currentSection);
      } else {
        currentSection = selectedElement;
        this.moveContentRight(currentSection);
      }
    }
    if (blockOptionsPopperInstance) blockOptionsPopperInstance.update();
    if (blockEditOptionsPopperInstance) blockEditOptionsPopperInstance.update();
    this.handleSelectedContentChanges();
  };

  moveContentRight = (selectedElement: HTMLElement) => {
    this.insertAfter(selectedElement, selectedElement.nextElementSibling as HTMLElement);
    selectedElement.click();
    this.showElementSectionOptions();
    a11yUpdateHighlightsService.updateHighlightsIdentity();
  };

  isMoveTop = (): void => {
    this.moveElementLeft();
    this.hideAnchorOption();
    this.hideTextOptions();
  };

  isMoveBottom = (): void => {
    this.moveElementRight();
    this.hideAnchorOption();
    this.hideTextOptions();
  };

  isCopied = (): void => {
    if (selectedElement) {
      this.hideAnchorOption();
      if (this.isNavigationBar()) {
        const copySpecialElements = selectedElement.closest('.row') as HTMLElement;
        copiedElement = copySpecialElements;
      } else {
        copiedElement = selectedElement;
      }
      selectedElementType === 'ELEMENT' ? (isElementCopied = true) : (isElementCopied = false);
      selectedElement.click();
      this.showElementSectionOptions();
    }
    savePageAlert(translate('alert.copiedtoclipboard'));
    savePageAlertContainerDisapper();
    this.handleSelectedContentChanges();
    this.closeLanguageOption();
  };

  isPaste = async (): Promise<void> => {
    if (selectedElement && copiedElement) {
      const pasteElement = copiedElement.cloneNode(true) as HTMLElement;
      this.updateSpecialElementProperty(pasteElement);
      if (isElementCopied) {
        selectedElement.appendChild(pasteElement);
      } else {
        selectedElement.innerHTML += pasteElement.innerHTML;
      }
    }
    selectedElement?.querySelectorAll('a').forEach((a) => (a.onclick = disableImageLinkClick));
    const currentElement = copiedElement;
    copiedElement = null; // clear copiedElement after paste
    if (currentElement) {
      currentElement.click();
      this.showElementSectionOptions();
    }
    await this.a11yCheckerNotification();
    savePageAlert(translate('alert.pastedfromclipboard'));
    savePageAlertContainerDisapper();
    this.handleSelectedContentChanges();
  };

  updateSpecialElementProperty = (copiedElement: HTMLElement): void => {
    const specialElementImages = copiedElement.getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE + ' ' + CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT,
    ) as HTMLCollectionOf<HTMLElement>;
    if (!specialElementImages) return;
    Array.from(specialElementImages).forEach(function (element) {
      if (element.tagName === 'IMG') {
        element.style.removeProperty('width');
      }
    });
  };

  updateElementValues = (input: HTMLInputElement): void => {
    if (input) {
      const styleValue: string = window
        .getComputedStyle(selectedElement as Element)
        .getPropertyValue(input.classList[0])
        .replace('px', '');
      const sum = parseFloat(styleValue);
      const value = Math.round(sum * 100) / 100;
      input.value = value.toString();
      this.handleSelectedContentChanges();
      this.enableDisableMarginForRowColumn(input);
    }
  };

  enableDisableMarginForRowColumn = (input: HTMLInputElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    let column = false;
    if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) column = true;
    input.disabled = false;
    if (column) {
      if (input.classList.contains('margin-left') || input.classList.contains('margin-right'))
        input.disabled = true;
    }
  };

  getElementValues = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const marginLeft = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_LEFT}`,
      );
      const marginRight = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_RIGHT}`,
      );
      const marginTop = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_TOP}`,
      );
      const marginBottom = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_BOTTOM}`,
      );
      const paddingLeft = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_LEFT}`,
      );
      const paddingRight = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_RIGHT}`,
      );
      const paddingTop = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_TOP}`,
      );
      const paddingBottom = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_BOTTOM}`,
      );
      const buttonSet = [
        marginLeft,
        marginRight,
        marginTop,
        marginBottom,
        paddingLeft,
        paddingRight,
        paddingTop,
        paddingBottom,
      ];
      buttonSet.map((btn: HTMLElement | null) => this.updateElementValues(btn as HTMLInputElement));
    }
  };

  checkForOptionsToShow = (elementType: string, iframewrapper: HTMLDocument): void => {
    const alignmentSection = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_SECTION_WRAPPER}`,
    );
    const colorPicker = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_SECTION_WRAPPER}`,
    );
    const sectionDivider = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_SECTION_DIVIDER}`,
    );
    const ColumnSetParent = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_PARENT}`,
    );
    const backButton = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON}`,
    );
    const twoColumnSetParent = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_PARENT}`,
    );
    const threeColumnSetParent = iframewrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_PARENT}`,
    );
    const columnAddView = [alignmentSection, colorPicker];
    const columnRemoveView = [
      sectionDivider,
      ColumnSetParent,
      backButton,
      twoColumnSetParent,
      threeColumnSetParent,
    ];
    const rowAddView = [alignmentSection, colorPicker, sectionDivider, ColumnSetParent];
    const rowRemoveView = [backButton, twoColumnSetParent, threeColumnSetParent];
    if (elementType === 'row') {
      let show;
      let hide;
      for (show of rowAddView) {
        if (show) {
          show.classList.remove('d-none');
        }
      }
      for (hide of rowRemoveView) {
        if (hide) {
          hide.classList.add('d-none');
        }
      }
    } else {
      let show;
      let hide;
      for (show of columnAddView) {
        if (show) {
          show.classList.remove('d-none');
        }
      }
      for (hide of columnRemoveView) {
        if (hide) {
          hide.classList.add('d-none');
        }
      }
    }
  };

  checkForBlockEditFeatures = (elementType: string, iframeWrapper: HTMLDocument): void => {
    // toggle background color picker feature for row/column
    const colorPicker = iframeWrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_SECTION_WRAPPER}`,
    );
    if (
      (elementType === 'row' &&
        (featuresList?.backgroundcoloroftherow === undefined ||
          !featuresList?.backgroundcoloroftherow)) ||
      (elementType === 'column' &&
        (featuresList?.backgroundcolorofthecolumn === undefined ||
          !featuresList?.backgroundcolorofthecolumn))
    ) {
      colorPicker?.classList.add('d-none');
    } else {
      colorPicker?.classList.remove('d-none');
    }

    // toggle border color picker feature for row/column
    this.borderCheckForBlockEditFeatures(elementType, iframeWrapper);

    // toggle margin padding feature for row/column
    const alignmentSection = iframeWrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_SECTION_WRAPPER}`,
    );
    if (
      (elementType === 'row' &&
        (featuresList?.marginrow === undefined || !featuresList?.marginrow)) ||
      (elementType === 'column' &&
        (featuresList?.margincolumn === undefined || !featuresList?.margincolumn))
    ) {
      alignmentSection?.children[0].classList.remove('d-flex');
      alignmentSection?.children[0].classList.add('d-none');
    } else {
      alignmentSection?.children[0].classList.add('d-flex');
    }

    // toggle padding feature for row/column
    if (
      (elementType === 'row' &&
        (featuresList?.paddingrow === undefined || !featuresList?.paddingrow)) ||
      (elementType === 'column' &&
        (featuresList?.paddingcolumn === undefined || !featuresList?.paddingcolumn))
    ) {
      alignmentSection?.children[1].classList.remove('d-flex');
      alignmentSection?.children[1].classList.add('d-none');
    } else {
      alignmentSection?.children[1].classList.add('d-flex');
    }
  };

  borderCheckForBlockEditFeatures = (elementType: string, iframeWrapper: HTMLDocument) => {
    const borderColorPicker = iframeWrapper.getElementById(
      `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_SECTION_WRAPPER}`,
    );
    if (elementType === 'row' || elementType === 'column') {
      borderColorPicker?.classList.remove('d-none');
    } else {
      borderColorPicker?.classList.add('d-none');
    }
    return borderColorPicker?.classList;
  };

  showBlockEditOptions = (elementType: string): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const editBtnId =
        elementType === 'row'
          ? CONSTANTS.LOREE_BLOCK_OPTIONS_ROW_EDIT_BUTTON
          : CONSTANTS.LOREE_BLOCK_OPTIONS_COLUMN_EDIT_BUTTON;
      this.checkForOptionsToShow(elementType, iframeDocument);
      const arrow = iframeDocument.querySelector(
        `#${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ARROW_REFERENCE_ID}`,
      ) as HTMLElement;
      const editButton = iframeDocument.querySelector(`#${editBtnId}`) as HTMLElement;
      const blockEditOption = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER}`,
      ) as HTMLElement;
      this.blockEditOptionsPlacement(arrow, editButton, blockEditOption);
      this.updateRowColumnColorPickerButton(
        CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_BUTTON,
        'backgroundColor',
      );
      this.updateRowColumnColorPickerButton(
        CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON,
        'borderColor',
      );
      blockEditOption.style.display = 'block';
      this.checkForBlockEditFeatures(elementType, iframeDocument);
    }
  };

  destroyEditPopperInstance = (): void => {
    if (blockEditOptionsPopperInstance) {
      blockEditOptionsPopperInstance.destroy();
      blockEditOptionsPopperInstance = null;
    }
  };

  hideBlockEditOptions = (blockEditOptionsWrapper: HTMLElement): void => {
    if (!blockEditOptionsWrapper) return;
    this.destroyEditPopperInstance();
    blockEditOptionsWrapper.style.display = 'none';
    this.closeBackgroundRowColumnColorPicker();
    this.closeBorderRowColumnColorPicker();
  };

  isEdit = (): void => {
    let elementType = '';
    if (selectedElement) {
      if (selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_ROW}`))
        elementType = 'row';
      if (selectedElement.classList.contains(`${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}`))
        elementType = 'column';
    }
    this.getElementValues();
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const blockEditOptionsWrapper = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER}`,
      );
      if (blockEditOptionsWrapper && blockEditOptionsWrapper.style.display !== 'none') {
        this.hideBlockEditOptions(blockEditOptionsWrapper);
      } else {
        this.showBlockEditOptions(elementType);
      }
      this.hideAnchorOption();
    }
  };

  updatePositionValues = (e: Event): void => {
    const event: _Any = e.target as HTMLInputElement;
    const selectedBlock = selectedElement;
    if (selectedBlock) {
      selectedBlock.style[event.id] = `${event.value}px`;
      blockOptionsPopperInstance?.update();
      blockEditOptionsPopperInstance?.update();
      if (addElementPopperInstance) addElementPopperInstance?.update();
    }
    this.closeBackgroundRowColumnColorPicker();
    this.closeBorderRowColumnColorPicker();
  };

  preventNonNumericalInput = (e: KeyboardEvent): void => {
    // e = e || window.event;
    const event = e.target as HTMLInputElement;
    const charCode = typeof e.which === 'undefined' ? e.keyCode : e.which;
    const charStr = String.fromCharCode(charCode);
    const inputDotArray = event.value.split('.');
    const inputArray = event.value.split('');
    const dotCount = event.value.indexOf('.');
    const caratPosition = event.selectionStart; // get the carat position

    if (charCode === 45 && !caratPosition) {
      e.preventDefault();
    }
    if (!charStr.match(/^[0-9.-]+$/)) {
      e.preventDefault();
    }
    // // just allow one dot
    if (inputDotArray.length > 1 && charCode === 46) {
      e.preventDefault();
    }
    if (
      dotCount > -1 &&
      inputDotArray[1].length > 1 &&
      caratPosition &&
      (inputArray[caratPosition] === undefined || caratPosition > dotCount)
    ) {
      e.preventDefault();
    }
  };

  onKeyUp = (e: KeyboardEvent, id: string): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const queryDocument = iframeDocument.getElementById(id);
      this.inputKeyUpValidation(e, 37, queryDocument, '-');
    }
  };

  onKeyDown = (e: KeyboardEvent): void => {
    e = e || window.event;
    const event = e.target as HTMLInputElement;
    const characterCode = typeof e.which === 'undefined' ? e.keyCode : e.which;
    // up-arrow (regular and num-pad)
    if (characterCode === 38 || characterCode === 104) {
      e.preventDefault();
      const sum = parseFloat(event.value) + 1;
      event.value = (Math.round(sum * 100) / 100).toString();
    }
    // down-arrow (regular and num-pad)
    if (characterCode === 40 || characterCode === 98) {
      e.preventDefault();
      const sum = parseFloat(event.value) - 1;
      const value = (Math.round(sum * 100) / 100).toString();
      value !== '0' ? (event.value = value) : (event.value = '0');
    }
    this.updatePositionValues(e);
  };

  onKeyDownForTextSpacing = (e: KeyboardEvent): void => {
    e = e || window.event;
    const event = e.target as HTMLInputElement;
    const characterCode = typeof e.which === 'undefined' ? e.keyCode : e.which;
    // up-arrow (regular and num-pad)
    if (characterCode === 38 || characterCode === 104) {
      e.preventDefault();
      const sum = parseFloat(event.value) + 1;
      event.value = (Math.round(sum * 100) / 100).toString();
    }
    // down-arrow (regular and num-pad)
    if (characterCode === 40 || characterCode === 98) {
      e.preventDefault();
      const sum = parseFloat(event.value) - 1;
      if (sum >= 0) {
        event.value = (Math.round(sum * 100) / 100).toString();
      }
    }
    this.updatePositionValues(e);
  };

  onInput = (e: Event): void => {
    this.updatePositionValues(e);
  };

  inputOnchangeEvent = (e: KeyboardEvent): void => {
    if (e.which < 48 || e.which > 57) {
      e.preventDefault();
    }
  };

  // Converting rgb to Hex
  rgb2hex = (rgb: string): string => {
    const color = rgb?.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    if (color !== null) {
      return (
        '#' +
        ('0' + parseInt(color[1], 10).toString(16)).slice(-2) +
        ('0' + parseInt(color[2], 10).toString(16)).slice(-2) +
        ('0' + parseInt(color[3], 10).toString(16)).slice(-2)
      );
    } else {
      return '';
    }
  };

  getHtml = (): string => {
    const iframeDocument = this.getDocument();
    let html = '';
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      if (wrapper && wrapper.children.length > 0) {
        this.hideAddElementButtonToSelectedElement();
        this.hideCloseElementButtonToSelectedElement();
        this.hideAddRowButtonToSelectedContainer();
        this.hideCloseRowButtonToSelectedContainer();
        html = wrapper.innerHTML;
        html = this.cleanHtml(html);
      }
    }
    // Replacing all rgp values to HEX
    const hexUpdatedHtml: string = html.replace(/rgb\([^)]+\)/g, (rgb) => {
      return this.rgb2hex(rgb);
    });
    return hexUpdatedHtml;
  };

  wipeOutEditorContent = () => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      if (wrapper) {
        wrapper.innerHTML = '';
      }
    }
  };

  isBlank = (str: string) => {
    return !str || /^\s*$/.test(str);
  };

  setHtml = (html: string | null | undefined): void => {
    if (html) {
      const iframeDocument = this.getDocument();
      if (iframeDocument) {
        const wrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
        if (wrapper) {
          const cleanedHtml = this.convertContent(html);
          let structuredHtml = '';
          if (!this.isBlank(cleanedHtml)) {
            structuredHtml = this.migrate.migrateElementsWithoutRowColumn(cleanedHtml);
          }
          const cleanedStructuredHtml = this.removeEmptyTags(structuredHtml);
          wrapper.innerHTML = cleanedStructuredHtml;
          history[0] = cleanedStructuredHtml;
        }
      }
    }
  };

  cleanHtml = (html: string): string => {
    const contenteditableAttribute = new RegExp('contenteditable="true"' as string, 'g');
    const rowHighlight = new RegExp(CONSTANTS.LOREE_ROW_HIGHLIGHT, 'g');
    const columnHighlight = new RegExp(CONSTANTS.LOREE_COLUMN_HIGHLIGHT, 'g');
    const elementHighlight = new RegExp(CONSTANTS.LOREE_ELEMENT_HIGHLIGHT, 'g');
    const rowHoverHighlight = new RegExp(CONSTANTS.LOREE_ROW_HOVER_HIGHLIGHT, 'g');
    const columnHoverHighlight = new RegExp(CONSTANTS.LOREE_COLUMN_HOVER_HIGHLIGHT, 'g');
    html = html.replace(rowHighlight, '');
    html = html.replace(columnHighlight, '');
    html = html.replace(elementHighlight, '');
    html = html.replace(rowHoverHighlight, '');
    html = html.replace(columnHoverHighlight, '');
    html = html.replace(contenteditableAttribute, '');
    return html;
  };

  removePointerEvent = (html: string): string => {
    const noPointer = new RegExp(CONSTANTS.LOREE_NO_POINTER, 'g');
    html = html.replace(noPointer, '');
    return html;
  };

  destroy = (): void => {
    selectedElement = null;
    copiedElement = null;
    previousSelection = null;
    addElementPopperInstance = null;
    blockEditOptionsPopperInstance = null;
    blockOptionsPopperInstance = null;
    textOptionsPopperInstance = null;
    rowHamburgerIconPopperInstance = null;
    columnHamburgerIconPopperInstance = null;
    tableHamburgerIconPopperInstance = null;
    menuHamburgerIconPopperInstance = null;
    foregroundColorPickerPopperInstance = null;
    textBackgroundColorPickerPopperInstance = null;
    backgroundColorPickerPopperInstance = null;
    borderColorPickerPopperInstance = null;
    paragraphOptionPopperInstance = null;
    spacingOptionPopperInstance = null;
    wordOptionPopperInstance = null;
    languageOptionPopperInstance = null;
    languageIsoLabelPopperInstance = null;
    alignmentOptionPopperInstance = null;
    linkOptionPopperInstance = null;
    anchorLinkPopperInstance = null;
    // imgBorderColorPickerPopperInstance = null;
  };

  inputKeyUpValidation = (
    e: KeyboardEvent,
    keyCode: number,
    queryDocument: HTMLElement | null,
    specialCharacter: string,
  ): void => {
    e = e || window.event;
    const event = e.target as HTMLInputElement;
    const characterCode = typeof e.which === 'undefined' ? e.keyCode : e.which;
    const caratPosition = event.selectionStart; // get the carat position
    const inputArray = event.value.split('');
    // check the particular special character in the input value.
    if (characterCode === keyCode) {
      const inputLength = event.value.length;
      const isMinusInput = inputArray.includes(specialCharacter);
      if (isMinusInput && caratPosition === 0) {
        const iframeDocument = this.getDocument();
        if (iframeDocument) {
          const currentElement = queryDocument as HTMLInputElement;
          if (currentElement) {
            currentElement.focus();
            currentElement.setSelectionRange(inputLength + 1, inputLength + 1);
          }
        }
      }
    }
  };

  handleLineHeight = (lineHeight: HTMLElement, lineHeightInput: HTMLInputElement): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.spacingText(lineHeightInput.value);
      const button = lineHeight.childNodes[0].childNodes[0];
      if (lineHeightInput.value === '0')
        (button as HTMLElement).classList.remove('text-options-icon-highlight');
      else (button as HTMLElement).classList.add('text-options-icon-highlight');
      lineHeightInput.focus();
    }
  };

  handleLetterSpacing = (letterSpacing: HTMLElement, wordSpaceInput: HTMLInputElement): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.wordSpacing(wordSpaceInput.value);
      const button = letterSpacing.childNodes[0].childNodes[0];
      if (wordSpaceInput.value === '0')
        (button as HTMLElement).classList.remove('text-options-icon-highlight');
      else (button as HTMLElement).classList.add('text-options-icon-highlight');
      wordSpaceInput.focus();
    }
  };

  fontSizeChange = (value: string, fontSizeInput: HTMLElement, units: string): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.fontSizeStyle(value, units);
      fontSizeInput.focus();
    }
  };

  fontFamilyStyleFeature = (select: HTMLSelectElement): void => {
    this.applyFontFamily = true;
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.fontFamilyStyle(select);
    }
  };

  textTransformFeature = (button: HTMLElement, type: string): void => {
    if (this.validateWindowSelection()) {
      const transformType = (button.childNodes[0] as HTMLElement).classList.contains(
        'text-options-icon-highlight',
      )
        ? 'revert'
        : type;
      this.wordStylerObj?.textTransformChange(transformType);
      this.wordStylerObj?.removeExistingTextCases(type);
    }
  };

  textDecorationFeature = (button: HTMLElement, type: string): void => {
    if (this.validateWindowSelection()) {
      const decorationType = (button.childNodes[0] as HTMLElement).classList.contains(
        'text-options-icon-highlight',
      )
        ? 'none'
        : type;
      this.wordStylerObj?.textDecorationChange(decorationType);
      this.wordStylerObj?.removeExistingTextDecorations(type);
    }
  };

  textVerticalAlignFeature = (button: HTMLElement, type: string): void => {
    if (this.validateWindowSelection()) {
      const verticalAlignType = (button.childNodes[0] as HTMLElement).classList.contains(
        'text-options-icon-highlight',
      )
        ? ''
        : type;
      this.wordStylerObj?.textVerticalAlignChange(verticalAlignType);
      this.wordStylerObj?.removeExistingTextDecorations(type);
    }
  };

  alignmentFeature = (button: HTMLElement, type: string): void => {
    if (this.validateWindowSelection()) {
      const alignmentType = (button.childNodes[0] as HTMLElement).classList.contains(
        'text-options-icon-highlight',
      )
        ? ''
        : type;
      this.wordStylerObj?.textAlignment(alignmentType);
      this.wordStylerObj?.removeExistingAlignments(type);
    }
  };

  boldStyleFeature = (button: HTMLElement): void => {
    if (this.validateWindowSelection()) {
      const styleValue = (button.childNodes[0] as HTMLElement).classList.contains(
        'text-options-icon-highlight',
      )
        ? 'normal'
        : 'bold';
      this.wordStylerObj?.boldStyleChange(styleValue);
      (button.childNodes[0] as HTMLElement).classList.toggle('text-options-icon-highlight');
    }
  };

  italicStyleFeature = (button: HTMLElement): void => {
    if (this.validateWindowSelection()) {
      const styleValue = (button.childNodes[0] as HTMLElement).classList.contains(
        'text-options-icon-highlight',
      )
        ? 'normal'
        : 'italic';
      this.wordStylerObj?.italicStyleChange(styleValue);
      (button.childNodes[0] as HTMLElement).classList.toggle('text-options-icon-highlight');
    }
  };

  handleUnOrderList = (button: HTMLElement): void => {
    this.hideTextOptions();
    const orderListButton = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON,
    );
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.createListElement('UL');
      if (
        isClassListContains(
          orderListButton?.childNodes[0] as HTMLElement,
          CONSTANTS.LOREE_TEXT_OPTION_ICON_HIGHLIGHT,
        )
      ) {
        removeClassFromElement(
          orderListButton?.childNodes[0] as HTMLElement,
          CONSTANTS.LOREE_TEXT_OPTION_ICON_HIGHLIGHT,
        );
      }
      (button.childNodes[0] as HTMLElement).classList.toggle(
        CONSTANTS.LOREE_TEXT_OPTION_ICON_HIGHLIGHT,
      );
      this.hideDesignSection();
    }
  };

  handleOrderList = (button: HTMLElement): void => {
    this.hideTextOptions();
    const unOrderListButton = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON,
    );
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.createListElement('OL');
      if (
        isClassListContains(
          unOrderListButton?.childNodes[0] as HTMLElement,
          CONSTANTS.LOREE_TEXT_OPTION_ICON_HIGHLIGHT,
        )
      ) {
        removeClassFromElement(
          unOrderListButton?.childNodes[0] as HTMLElement,
          CONSTANTS.LOREE_TEXT_OPTION_ICON_HIGHLIGHT,
        );
      }
      (button.childNodes[0] as HTMLElement).classList.toggle(
        CONSTANTS.LOREE_TEXT_OPTION_ICON_HIGHLIGHT,
      );
      this.hideDesignSection();
    }
  };

  paragraphFeature = (type: string, customHeader: {}): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.handleSelectedTagStyle(customHeader, type);
      this.wordStylerObj?.styleAction(type);
      this.hideTextOptions();
      this.hideDesignSection();
    }
  };

  removeAllStyle(headerData: CustomHeader): void {
    if (this.validateWindowSelection()) {
      const selected = this.wordStylerObj?.getSelectedContent();
      const anchorNode = this.elementPicker.getElement(
        selected?.anchorNode as HTMLElement,
        'blockElement',
      );
      anchorNode?.removeAttribute('style');
      const defaultStyle = this.setDefaultFontStyle(headerData, anchorNode?.tagName);
      if (anchorNode) anchorNode.innerHTML = `${anchorNode?.textContent}`;
      this.dashboardStyle.setElementStyles(anchorNode, defaultStyle);
      this.removeSelectedIcons(inlineStyleIcons);
      this.removeSelectedIcons(tagNodeIcons);
      this.handleSelectedContentChanges();
      this.hideTextOptions();
      this.hideDesignSection();
      this.updateLanguageIsoLabelPopperInstance();
    }
  }

  setDefaultFontStyle(defaultStyle: _Any, element: string) {
    let style;
    switch (element) {
      case 'H1':
        style = defaultStyle[0].h1;
        break;
      case 'H2':
        style = defaultStyle[1].h2;
        break;
      case 'H3':
        style = defaultStyle[2].h3;
        break;
      case 'H4':
        style = defaultStyle[3].h4;
        break;
      case 'H5':
        style = defaultStyle[4].h5;
        break;
      case 'H6':
        style = defaultStyle[5].h6;
        break;
      default:
        style = defaultStyle[6].paragraph;
    }
    return style;
  }

  removeListElement(htmlString: string): string {
    return htmlString
      .replace(/<ul/g, '<p')
      .replace(/<li/g, '<span')
      .replace(/<ol/g, '<p')
      .replace(/<\/ul>/g, '</p>')
      .replace(/<\/ol>/g, '</p>')
      .replace(/<br><\/li>/g, '</span><br>')
      .replace(/<\/li>/g, '</span><br>');
  }

  validateWindowSelection() {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return false;
    const selectedText = iframeDocument.getSelection() as Selection;
    this.editorSelection.validateSelection(selectedText, previousSelection, this.applyFontFamily);
    this.applyFontFamily = false;
    return true;
  }

  userLineHeight() {
    return userLineHeight;
  }

  userWordSpacing() {
    return userWordSpacing;
  }

  checkMultiSelectFontStyles = (
    anchoreNode: Array<Node>,
    appliedFontFamily: string[],
    appliedFontSize: string[],
  ) => {
    for (const node of anchoreNode) {
      const nodeContentLength = node.textContent?.trim().length
        ? node.textContent?.trim().length
        : 0;
      if (node.nodeName === '#text' && nodeContentLength > 0) {
        if (node?.parentElement) {
          const elementStyle = window.getComputedStyle(node?.parentElement);
          if (!appliedFontFamily.includes(elementStyle.fontFamily))
            appliedFontFamily.push(elementStyle.fontFamily);
          if (!appliedFontSize.includes(elementStyle.fontSize))
            appliedFontSize.push(elementStyle.fontSize);
        }
      }
      if (['SPAN', 'A', 'LI', 'P'].includes(node.nodeName)) {
        if (node) {
          const elementStyle = window.getComputedStyle(node as Element);
          if (!appliedFontFamily.includes(elementStyle.fontFamily))
            appliedFontFamily.push(elementStyle.fontFamily);
          if (!appliedFontSize.includes(elementStyle.fontSize))
            appliedFontSize.push(elementStyle.fontSize);
          if (node.childNodes.length > 0) {
            this.checkMultiSelectFontStyles(
              Array.from(node.childNodes),
              appliedFontFamily,
              appliedFontSize,
            );
          }
        }
      }
    }
    const isMultiFontApplied = appliedFontFamily.length > 0 || appliedFontSize.length > 0;
    return { isMultiFontApplied: isMultiFontApplied, font: [appliedFontFamily, appliedFontSize] };
  };

  getSelectedDetails(selected: Selection, textOptions: HTMLElement): void {
    let fontFamily = '';
    let appliedFamily = '';
    let appliedSize = '';
    const style: string[] = this.selectPreviousStyle(selected);
    const ns = getNodesInRange(selected.getRangeAt(0));
    const appliedStyles = this.checkMultiSelectFontStyles(ns, [], []);
    if (appliedStyles.isMultiFontApplied) {
      const fontFamilyList: string[] = appliedStyles.font[0];
      const fontSizeList: string[] = appliedStyles.font[1];
      appliedFamily = fontFamilyList.length === 1 ? fontFamilyList[0] : '';
      appliedSize = fontSizeList.length === 1 ? fontSizeList[0].replace('px', '') : '';
    } else {
      appliedFamily = style[0];
      appliedSize = style[1];
    }
    const boldIcon = this.getDocument()?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_BOLD_BUTTON);
    const italicIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_ITALIC_BUTTON,
    );
    const uppercaseIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_UPPERCASE_BUTTON,
    );
    const lowercaseIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_LOWERCASE_BUTTON,
    );
    const titlecaseIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_TITLECASE_BUTTON,
    );
    const underlineIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_UNDERLINE_BUTTON,
    );
    const strikethroughIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_STRIKETHROUGH_BUTTON,
    );
    const superscriptIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_SUPERSCRIPT_BUTTON,
    );
    const subscriptIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_SUBSCRIPT_BUTTON,
    );
    const centerAlignIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_ALIGN_CENTER_BUTTON,
    );
    const justifyAlignIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_ALIGN_JUSTIFY_BUTTON,
    );
    const leftAlignIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_ALIGN_LEFT_BUTTON,
    );
    const rightAlignIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_ALIGN_RIGHT_BUTTON,
    );
    const orderListIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON,
    );
    const unOrderListIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON,
    );
    const anchorIcon = this.getDocument()?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON);
    const headerH1Icon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_HEADER1_BUTTON,
    );
    const headerH2Icon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_HEADER2_BUTTON,
    );
    const headerH3Icon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_HEADER3_BUTTON,
    );
    const headerH4Icon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_HEADER4_BUTTON,
    );
    const headerH5Icon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_HEADER5_BUTTON,
    );
    const headerH6Icon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_HEADER6_BUTTON,
    );
    const normalParagraphIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_NORMAL_PARAGRAPH_BUTTON,
    );
    const lineHeightIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_LINE_HEIGHT_BUTTON,
    );
    const lineSpaceIcon = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_LINE_SPACE_BUTTON,
    );
    tagNodeIcons = [
      headerH1Icon as HTMLElement | null,
      headerH2Icon as HTMLElement | null,
      headerH3Icon as HTMLElement | null,
      headerH4Icon as HTMLElement | null,
      headerH5Icon as HTMLElement | null,
      headerH6Icon as HTMLElement | null,
      normalParagraphIcon as HTMLElement | null,
      anchorIcon as HTMLElement | null,
      orderListIcon as HTMLElement | null,
      unOrderListIcon as HTMLElement | null,
    ];
    inlineStyleIcons = [
      boldIcon as HTMLElement | null,
      italicIcon as HTMLElement | null,
      uppercaseIcon as HTMLElement | null,
      lowercaseIcon as HTMLElement | null,
      titlecaseIcon as HTMLElement | null,
      underlineIcon as HTMLElement | null,
      strikethroughIcon as HTMLElement | null,
      superscriptIcon as HTMLElement | null,
      subscriptIcon as HTMLElement | null,
      centerAlignIcon as HTMLElement | null,
      justifyAlignIcon as HTMLElement | null,
      leftAlignIcon as HTMLElement | null,
      rightAlignIcon as HTMLElement | null,
      lineHeightIcon as HTMLElement | null,
      lineSpaceIcon as HTMLElement | null,
    ];

    this.removeSelectedIcons(inlineStyleIcons);
    this.removeSelectedIcons(tagNodeIcons);
    this.addSelectedIcons(
      this.checkExistingTextInline(selected),
      inlineStyleIcons as HTMLElement[],
    );
    for (let i = 0; i < this.checkExistingTagWithStyle(selected).length; i++) {
      (tagNodeIcons[i] as HTMLElement)?.removeAttribute('disabled');
      if (this.checkExistingTagWithStyle(selected)[i]) {
        if (tagNodeIcons[i]?.childNodes[0] as HTMLElement) {
          if (!isClassListContains(tagNodeIcons[i] as HTMLElement, 'text-option-button')) {
            (tagNodeIcons[i] as HTMLElement)?.setAttribute('disabled', 'true');
            (tagNodeIcons[i]?.childNodes[0] as HTMLElement).style.color = '#000d9c';
          }
          if ((tagNodeIcons[i]?.childNodes[0] as HTMLElement).tagName === 'svg')
            (tagNodeIcons[i]?.childNodes[0] as HTMLElement).classList.add(
              'text-options-icon-highlight',
            );
        }
      }
    }
    if (style[2].length > 1) {
      userColor = rgbToHex(style[2], '#000000');
    } else {
      userColor = '#000000';
    }
    if (style[3].length > 1) {
      userBGColor = rgbToHex(style[3], '');
    } else {
      userBGColor = '';
    }
    if (style[4].length > 1) {
      userBorderColor = rgbToHex(style[4], '#000000');
    } else {
      userBorderColor = '#000000';
    }
    if (style[5].length > 1) {
      userBorderStyle = style[5];
    } else {
      userBorderStyle = '';
    }
    if (style[6].length > 0) {
      userBorderWidth = style[6];
    } else {
      userBorderWidth = '0';
    }
    if (style[7].length > 0) {
      userLineHeight = style[7];
    } else {
      userLineHeight = '1.5';
    }
    if (style[8].length > 0) {
      userWordSpacing = style[8];
    } else {
      userWordSpacing = '0';
    }
    if (appliedFamily.length > 0) {
      const exp = new RegExp('"' as string, 'g');
      fontFamily = appliedFamily.replace(exp, ' ');
    }
    baseText.selectFontFamilyOptions(textOptions, fontFamily.trim());
    const ele2: Element = textOptions.children[4];
    const ele3: Element = textOptions.children[5];
    const isPoint = (ele3 as HTMLSelectElement)?.value === 'pt';
    if (ele2) {
      (ele2 as HTMLInputElement).value = (
        isPoint
          ? Math.round((parseFloat(appliedSize) * 72.0) / 96.0)
          : Math.round(parseFloat(appliedSize))
      ).toString();
    }
  }

  addSelectedIcons = (method: Array<boolean>, type: Array<ChildNode>) => {
    for (let i = 0; i < method.length; i++) {
      if (method[i] && method.length > 0) {
        if (type[i]?.childNodes[0] as HTMLElement)
          (type[i]?.childNodes[0] as HTMLElement).classList.add('text-options-icon-highlight');
      }
    }
  };

  removeSelectedIcons = (type: Array<HTMLElement | ChildNode | null>) => {
    for (let i = 0; i < type.length; i++) {
      if (type[i]?.childNodes[0] as HTMLElement) {
        (type[i]?.childNodes[0] as HTMLElement).style.color = '#000000';
        if ((type[i]?.childNodes[0] as HTMLElement).tagName === 'svg')
          (type[i]?.childNodes[0] as HTMLElement).classList.remove('text-options-icon-highlight');
      }
    }
  };

  checkExistingTextInline = baseText.checkExistingTextInline;

  checkExistingTagWithStyle = (selected: Selection) => {
    const tagStyle: boolean[] = [];
    if (selected.anchorNode === null || selected.anchorNode === undefined) {
      return [];
    }
    let targetNode = selected.anchorNode as HTMLElement;
    const selectedElementTags: string[] = [];
    if (targetNode.childNodes[0])
      selectedElementTags.push((targetNode.childNodes[0] as HTMLElement).tagName);
    while (
      !targetNode?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) &&
      targetNode?.tagName !== 'DIV'
    ) {
      selectedElementTags.push(targetNode?.tagName);
      targetNode = targetNode?.parentElement as HTMLElement;
    }
    this.textOptionTagHandler(selectedElementTags, tagStyle);
    return tagStyle;
  };

  textOptionTagHandler = (selectedTagnames: Array<string>, tagStyle: Array<boolean>) => {
    const headerH1 = !!selectedTagnames.includes('H1');
    const headerH2 = !!selectedTagnames.includes('H2');
    const headerH3 = !!selectedTagnames.includes('H3');
    const headerH4 = !!selectedTagnames.includes('H4');
    const headerH5 = !!selectedTagnames.includes('H5');
    const headerH6 = !!selectedTagnames.includes('H6');
    const normalParagraph = !!selectedTagnames.includes('P');
    const anchorLink = !!selectedTagnames.includes('A');
    const orderedList = !!(selectedTagnames.includes('LI') && selectedTagnames.includes('OL'));
    const unorderedList = !!(selectedTagnames.includes('LI') && selectedTagnames.includes('UL'));
    tagStyle.push(
      headerH1,
      headerH2,
      headerH3,
      headerH4,
      headerH5,
      headerH6,
      normalParagraph,
      anchorLink,
      orderedList,
      unorderedList,
    );
  };

  selectPreviousStyle = (selected: Selection): string[] => {
    if (selected) {
      if (selected.anchorNode === null || selected.anchorNode === undefined) {
        return [];
      }
      let isDoubleClick = false;
      let targetNode: HTMLElement = selected.anchorNode.parentElement as HTMLElement;
      const wrapper = selected.anchorNode;
      let wrapperParent = wrapper as HTMLElement;
      if (wrapper?.nodeName !== 'LI') {
        wrapperParent = this.elementPicker.getElement(
          selected.anchorNode as HTMLElement,
          'majorParent',
        );
      }
      const range = selected.getRangeAt(0);
      const preCaretRange = range.cloneRange();
      const selectedRange = preCaretRange.cloneContents();
      if (wrapperParent.textContent === selectedRange.textContent) {
        const selectedAnchor = this.elementPicker.getElement(
          selected.anchorNode as HTMLElement,
          'selectPreviousStyle',
        );
        targetNode = selectedAnchor;
        isDoubleClick = false;
      } else isDoubleClick = true;
      if (navigator.userAgent.includes('Firefox') && this.eventDetails === 3) {
        targetNode = selected.anchorNode as HTMLElement;
      }

      const targetParent = targetNode?.parentNode as HTMLElement;
      if (!targetNode) {
        return [];
      }
      const elementStyle = window.getComputedStyle(targetNode);

      const appliedFamily = elementStyle.fontFamily;
      const appliedSize = elementStyle.fontSize.replace('px', '');
      const appliedColor = elementStyle.color;

      const appliedBGColor =
        isDoubleClick && targetNode.tagName !== 'SPAN' ? '' : targetNode.style.backgroundColor;
      const appliedBorderColor =
        isDoubleClick && targetNode.tagName !== 'SPAN' ? '' : targetNode.style.borderColor;
      const appliedBorderStyle =
        isDoubleClick && targetNode.tagName !== 'SPAN' ? '' : targetNode.style.borderStyle;
      const appliedBorderWidth =
        isDoubleClick && targetNode.tagName !== 'SPAN'
          ? ''
          : targetNode.style.borderWidth.replace('px', '');
      const appliedLineHeight =
        targetNode.style.lineHeight.length > 0
          ? targetNode?.style?.lineHeight
          : targetParent?.style?.lineHeight;
      const appliedWordSpacing = targetNode.style.wordSpacing.replace('px', '');
      return [
        appliedFamily,
        appliedSize,
        appliedColor,
        appliedBGColor,
        appliedBorderColor,
        appliedBorderStyle,
        appliedBorderWidth,
        appliedLineHeight,
        appliedWordSpacing,
      ];
    }
    return [];
  };

  addCustomLinkStyles = (
    achorTag: HTMLAnchorElement,
    customLink: { [x: string]: string; color: string; font: string },
  ) => {
    achorTag.style.color = customLink.color;
    achorTag.style.textDecoration = customLink['text-decoration'];
    achorTag.style.textDecorationStyle = customLink['text-decoration-style'];
    achorTag.style.fontFamily = customLink.font;
    customLink['text-style'] === 'Bold'
      ? (achorTag.style.fontWeight = customLink['text-style'])
      : (achorTag.style.fontStyle = customLink['text-style']);
  };

  handleCustomBlockAppendButton = (customBlockType: string) => {
    const customBlockAppendButton = document.createElement('div');
    customBlockAppendButton.id = CONSTANTS.LOREE_CUSTOM_BLOCK_ADD_BUTTON;
    customBlockAppendButton.className = 'text-center custom-block-btn-section-top';
    customBlockAppendButton.style.pointerEvents = 'auto';
    const addButton = document.createElement('button');
    addButton.className = 'custom-block-btn custom-block-add-button';
    addButton.innerHTML = 'Add';
    addButton.onclick = (): void => this.customBlockAddButton(customBlockType);
    const cancelButton = document.createElement('button');
    cancelButton.className = 'custom-block-btn custom-block-cancel-button';
    cancelButton.innerHTML = 'Cancel';
    cancelButton.onclick = (): void => this.customBlockCancelButton(customBlockType);
    customBlockAppendButton.appendChild(cancelButton);
    customBlockAppendButton.appendChild(addButton);
    this.handleLoreeEditorPointerEvents('disable', customBlockType);
    if (customBlockType === 'Row') {
      this.removeHighlightSelectedColumn(selectedElement as HTMLElement);
      selectedElement = selectedElement?.parentElement as HTMLElement;
      selectedElement.style.position = 'relative';
      selectedElement.prepend(customBlockAppendButton);
    }
    if (customBlockType === 'Elements') {
      selectedElement = selectedElement as HTMLElement;
      selectedElement.style.position = 'relative';
      selectedElement.prepend(customBlockAppendButton);
    }
    if (customBlockType === 'Template') {
      selectedElement = selectedElement as HTMLElement;
      selectedElement.style.position = 'relative';
      selectedElement?.prepend(customBlockAppendButton);
    }
  };

  handleInteractiveBlockappendButton = (type: string) => {
    const customBlockAppendButton = document.createElement('div');
    customBlockAppendButton.id = CONSTANTS.LOREE_CUSTOM_BLOCK_ADD_BUTTON;
    customBlockAppendButton.className = 'text-center custom-block-btn-section';
    customBlockAppendButton.style.pointerEvents = 'auto';
    const addButton = document.createElement('button');
    addButton.className = 'custom-block-btn custom-block-add-button';
    addButton.innerHTML = 'Add';
    addButton.onclick = (): void => this.customBlockAddButton(type);
    const cancelButton = document.createElement('button');
    cancelButton.className = 'custom-block-btn custom-block-cancel-button';
    cancelButton.innerHTML = 'Cancel';
    cancelButton.onclick = (): void => this.customBlockCancelButton(type);
    customBlockAppendButton.appendChild(cancelButton);
    customBlockAppendButton.appendChild(addButton);
    this.handleLoreeEditorPointerEvents('disable', type);
    if (type === 'Row') {
      selectedElement = selectedElement?.parentElement as HTMLElement;
      selectedElement.style.position = 'relative';
    }
    selectedElement?.prepend(customBlockAppendButton);
  };

  customBlockCancelButton = (type: string) => {
    const iFrameDocument = this.getDocument();
    if (type === 'Row') {
      if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) {
        selectedElement?.parentElement?.remove();
      } else if (selectedElement?.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)) {
        selectedElement?.remove();
      } else {
        selectedElement?.parentElement?.parentElement?.remove();
      }
    } else if (type === 'Template') {
      const customTemplate = iFrameDocument?.getElementById(CONSTANTS.LOREE_TEMPLATE_BLOCK_WRAPPER);
      if (customTemplate) customTemplate.remove();
    } else {
      const customElement = selectedElement?.parentElement?.parentElement;
      if (customElement) customElement.remove();
    }
    this.changeSelectedElement(null);
    handleAutoSaveOnCustomBlockAppend(false);
    this.hideHamburgerIconForRow();
    this.handleLoreeEditorPointerEvents('enable', type);
  };

  removeDuplicateTags = (html: string): string => {
    let text;
    text = html.replace(/<script[^>]*>(?:(?!<\/script>)[^])*<\/script>/g, '');
    text = text.replace(/<link\b[^>]*?>/gi, '');
    return text;
  };

  customBlockAddButton = (type: string) => {
    if (type === 'Row') {
      this.changeSelectedElement(selectedElement?.parentElement?.parentElement as HTMLElement);
    }
    if (type === 'Elements') {
      const addedContent = selectedElement?.parentElement?.parentElement;
      const docFrag = document.createDocumentFragment();
      while (addedContent?.firstChild) {
        const child = addedContent?.removeChild(addedContent?.firstChild);
        docFrag.appendChild(child);
      }
      addedContent?.parentElement?.replaceChild(docFrag, addedContent as Node);
      this.changeSelectedElement(null);
    }
    const addButton = this.getDocument()?.getElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_ADD_BUTTON);
    handleAutoSaveOnCustomBlockAppend(false);
    this.handleLoreeEditorPointerEvents('enable', type);
    if (addButton) {
      addButton.remove();
      if (type === 'Template') {
        const templateWrapper = this.getDocument()?.getElementById(
          CONSTANTS.LOREE_TEMPLATE_BLOCK_WRAPPER,
        );
        if (templateWrapper) {
          const html = templateWrapper.innerHTML;
          templateWrapper.remove();
          const iframeContent: HTMLElement | null | undefined = this.getDocument()?.getElementById(
            CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
          );
          iframeContent?.lastElementChild?.tagName === 'SCRIPT'
            ? iframeContent.lastElementChild.insertAdjacentHTML(
                'beforebegin',
                this.removeDuplicateTags(html),
              )
            : iframeContent?.insertAdjacentHTML('beforeend', html);
        }
        this.changeSelectedElement(null);
      }
      addButton.onclick = (): void => {
        customElementAlert(type, 'added');
        this.customElementAlertTimer();
        this.handleSelectedContentChanges();
      };
    }
  };

  customElementAlertTimer = () => {
    const alertPopup = document?.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
    if (alertPopup) alertPopup.classList.remove('d-none');
    setTimeout(() => {
      alertPopup?.remove();
    }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
  };

  handleLoreeEditorPointerEvents = (action: string, customBlockType: string) => {
    const alertText =
      customBlockType === 'Row'
        ? 'custom row'
        : customBlockType === 'Template'
        ? 'template'
        : 'custom element';
    const loreeElements = [];
    const loreeContentWrapper = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
    );
    const loreeHeader = document.getElementById(CONSTANTS.LOREE_HEADER);
    const saveTemplateIcon = document.getElementById('save-icon-position');
    const loreeSidebar = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    const loreeAddRowWrapper = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_ADD_ROW_BUTTON_WRAPPER,
    );
    const loreeAddElementButton = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_ADD_ELEMENT_BUTTON,
    );
    const loreeBlockOptions = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER,
    );
    const loreeInsertRowButton = this.getDocument()?.getElementById(
      CONSTANTS.LOREE_INSERT_ROW_BUTTON,
    );
    loreeElements.push(
      loreeContentWrapper,
      loreeHeader,
      saveTemplateIcon,
      loreeSidebar,
      loreeAddRowWrapper,
      loreeAddElementButton,
      loreeBlockOptions,
      loreeInsertRowButton,
    );
    Array.from(loreeElements).forEach((element) => {
      if (element) {
        if (action === 'disable') {
          element.style.pointerEvents = 'none';
        } else {
          element.style.pointerEvents = '';
        }
      }
    });
    if (action === 'disable') customComponentsAlert(alertText);
    else hideCustomComponentsAlert();
  };

  convertContent = (html: string): string => {
    let content = html;
    if (html && html !== '') {
      const wrapper = document.createElement('div');
      wrapper.innerHTML = content;
      this.migrate.migrateNavigationContent(wrapper);
      this.migrate.migrateDivText(wrapper);
      this.migrate.migrateBoldTag(wrapper);
      this.migrate.migrateAnchorTag(wrapper);
      this.migrateItalicTag(wrapper);
      this.migrate.migrateIncorrectImageWrappersWithLinks(wrapper);
      this.migrate.migrateImage(wrapper);

      this.migrate.migrateVideo(wrapper);
      this.migrate.migrateVideoIframe(wrapper);
      this.migrate.migrateIframeTags(wrapper);
      this.migrateLoreeInteractives(wrapper);
      this.migrate.migrateTable(wrapper);
      this.migrateList(wrapper);
      this.migrateTextTags(wrapper);
      this.migrateTextFormattingTags(wrapper);
      this.migrateHrTag(wrapper);
      this.migrate.migrateIconText(wrapper);
      this.migrate.migrateColumn(wrapper);
      this.migrate.migrateRow(wrapper);
      this.migrate.removeContainerWrapperForExistingElement(wrapper);
      let cleanedContent = wrapper.innerHTML;
      cleanedContent = this.removeEmptyTags(cleanedContent);
      content = cleanedContent;
    }
    return content;
  };

  migrateHrTag = (wrapper: HTMLElement): void => {
    const hrTag = wrapper.getElementsByTagName('hr');
    Array.from(hrTag).forEach(function (element) {
      if (!element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER)) {
        const hrWrapper = document.createElement('div');
        hrWrapper.setAttribute('style', 'max-width:100%;padding: 5px; margin:0 0 10px;');
        hrWrapper.className = 'loree-iframe-content-divider-wrapper';
        element.className = 'loree-iframe-content-divider';
        element.style.display = 'inline-flex';
        element.style.width = element.style.width ? element.style.width : '100%';
        hrWrapper.innerHTML = element.outerHTML;
        const hrParent = element.parentElement;
        if (hrParent) {
          hrParent.insertBefore(hrWrapper, element);
          element.remove();
        }
      }
    });
  };

  migrateTextFormattingTags = (wrapper: HTMLElement): void => {
    const strongTag = wrapper.getElementsByTagName('strong');
    const italicTag = wrapper.getElementsByTagName('em');
    const preTags = wrapper.getElementsByTagName('pre');
    const addressTags = wrapper.getElementsByTagName('address');
    Array.from(strongTag).forEach(function (element) {
      if (element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        element.style.fontWeight = 'bold';
      }
    });
    Array.from(italicTag).forEach(function (element) {
      if (element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        element.style.fontStyle = 'italic';
      }
    });
    Array.from(preTags).forEach(function (preTag) {
      if (preTag.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        preTag.style.whiteSpace = 'pre';
        preTag.style.padding = preTag.style.padding ? preTag.style.padding : '5px';
      }
    });
    Array.from(addressTags).forEach(function (addressTag) {
      if (addressTag.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        addressTag.style.fontStyle = 'italic';
        addressTag.style.padding = addressTag.style.padding ? addressTag.style.padding : '5px';
      }
    });
  };

  removeEmptyTags = (innerHTML: string): string => {
    let htmlStr = innerHTML;
    htmlStr = htmlStr.replace(/<strong/gim, '<span');
    htmlStr = htmlStr.replace(/<\/strong>/gim, '</span>');
    htmlStr = htmlStr.replace(/<em/gim, '<span');
    htmlStr = htmlStr.replace(/<\/em/gim, '</span');
    htmlStr = htmlStr.replace(/<pre/gim, '<p');
    htmlStr = htmlStr.replace(/<\/pre>/gim, '</p>');
    htmlStr = htmlStr.replace(/<address/gim, '<p');
    htmlStr = htmlStr.replace(/<\/address/gim, '</p');
    htmlStr = htmlStr.replace(/<p[^>]*><\/p[^>]*>/gim, '');
    htmlStr = htmlStr.replace(/<link[^>]*>/g, '');
    htmlStr = htmlStr.replace(/<script[^>]*><\/script[^>]*>/g, '');
    htmlStr = htmlStr.replace(/<sub[^>]*>/g, '');
    htmlStr = htmlStr.replace(/<\/sub>/g, '');
    htmlStr = htmlStr.replace(/<sup[^>]*>/g, '');
    htmlStr = htmlStr.replace(/<\/sup>/g, '');
    return htmlStr;
  };

  migrateList = (wrapper: HTMLElement): void => {
    const olTags = wrapper.getElementsByTagName('ol');
    Array.from(olTags).forEach(function (element) {
      if (!element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        element.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT);
      }
    });

    const ulTags = wrapper.getElementsByTagName('ul');
    Array.from(ulTags).forEach(function (element) {
      if (!element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        element.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT);
      }
    });
  };

  migrateItalicTag = (wrapper: HTMLElement): void => {
    const italicTags = wrapper.getElementsByTagName('i');
    Array.from(italicTags).forEach(function (element) {
      if (element.parentElement?.tagName === 'DIV') {
        const paragraphTag = document.createElement('p');
        paragraphTag.innerHTML = element.innerHTML;
        paragraphTag.style.cssText = `font-style: italic;${element.style.cssText}`;
        (element.parentNode as Node).replaceChild(paragraphTag, element);
      } else {
        const spanTag = document.createElement('span');
        spanTag.innerHTML = element.innerHTML;
        spanTag.style.cssText = `font-style: italic;${element.style.cssText}`;
        (element.parentNode as Node).replaceChild(spanTag, element);
      }
    });
  };

  migrateTextTags = (wrapper: HTMLElement): void => {
    const tags = [
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'header',
      'a',
      'li',
      'p',
      'span',
      'strong',
      'em',
      'pre',
      'address',
      'caption',
    ];
    tags.forEach(function (tag) {
      const identifiedTags = wrapper.getElementsByTagName(tag);
      Array.from(identifiedTags).forEach((element: Element) => {
        if (!element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
          if (tag === 'p') {
            const tagNames = ['script', 'link'];
            tagNames.forEach(function (tag) {
              if (element.getElementsByTagName(tag).length === 1) {
                element.remove();
              }
            });
            (element as HTMLInputElement).style.padding = '5px';
          }
          element.classList.add(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT);
        }
      });
    });
  };

  migrateLoreeInteractives = (wrapper: HTMLElement): void => {
    const loreeInteractiveLinks = ['loree-int', 'loree-interactive'];
    const iframeTags = wrapper.getElementsByTagName('iframe');
    Array.from(iframeTags).forEach(function (element) {
      const url = element.src;
      let isInteractive = false;
      loreeInteractiveLinks.forEach(function (link) {
        if (url.includes(link)) isInteractive = true;
      });
      if (isInteractive) {
        if (!element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT)) {
          const iframe = document.createElement('iframe');
          iframe.src = url;
          iframe.className = CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT;
          iframe.title = element.title;
          iframe.id = element.id;
          iframe.width = element.width;
          iframe.height = element.height;
          iframe.scrolling = element.scrolling;
          iframe.frameBorder = element.frameBorder;

          // wrapper
          const interactiveWrapper = document.createElement('div');
          interactiveWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER;
          interactiveWrapper.setAttribute('style', 'padding: 10px;margin:0 0 20px;');

          const parentElement = element.parentElement;
          if (parentElement) {
            parentElement.insertBefore(interactiveWrapper, element);
            interactiveWrapper.appendChild(iframe);
            element.remove();
          }
        }
      }
    });
  };

  handleAddRowButtonClickOfContainer = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      this.hideGlobalTemplate();
      this.showSidebar();
      this.showAddRowButton();
      this.hideCloseRowButton();
      this.showRowSection();
      this.enableRowSection();
      this.expandRowSection();
      this.disableElementSection();
      this.hideCustomElementSection();
      this.hideElementSection();
      this.collapseElementSection();
      this.hideAddRowButtonToSelectedContainer();
      this.appendCloseRowButtonToSelectedContainer();
      this.hideDesignSection();
      this.hideTableDesignSection();
      this.hideMenuDesignSection();
      this.hideContainerDesignSection();
      this.removeImageOnDesignSection();
      this.showItemsInSidebarHiddenForTable();
      this.hideInteractiveElementSection();
      if (
        selectedElement &&
        (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH')
      ) {
        this.hideItemsInSidebarForTable();
      }
    }
  };

  appendCloseRowButtonToSelectedContainer = (): void => {
    const iframeDocument = this.getDocument();
    const selectedElement = this.getSelectedElement();
    this.addMinHeightToSelectedElement(selectedElement);
    if (selectedElement && iframeDocument) {
      const closeAddElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER}`,
      ) as HTMLElement;
      if (!closeAddElementButton) return;
      this.elementAddButtonPlacement(selectedElement, closeAddElementButton);
      closeAddElementButton.style.display = 'inline-flex';
    }
  };

  hideAddRowButtonToSelectedContainer = (): void => {
    this.removeMinHeightFromColumnAndContainer();
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const addElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_ADD_ROW_BUTTON_FOR_CONTAINER}`,
      ) as HTMLElement;
      if (addElementButton) addElementButton.style.display = 'none';
    }
  };

  handleCloseRowButtonOfContainer = (): void => {
    const iframeDocument = this.getDocument();
    const selectedElement = this.getSelectedElement();
    if (iframeDocument) {
      this.hideSubSidebar();
      this.hideCloseRowButton();
      this.showAddRowButton();
      this.showRowSection();
      this.collapseRowSection();
      this.disableRowSection();
      this.collapseElementSection();
      this.hideCustomElementSection();
      this.disableElementSection();
      this.hideCloseRowButtonToSelectedContainer();
      this.showAddRowButtonToSelectedContainer();
      this.removeSelectedElementSection();
      this.hideDesignSection();
      this.removeImageOnDesignSection();
      this.hideInteractiveElementSection();
    }
    if (selectedElement && (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH')) {
      this.showTableDesignSection();
    }
  };

  hideCloseRowButtonToSelectedContainer = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const closeAddElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER}`,
      ) as HTMLElement;
      if (closeAddElementButton) closeAddElementButton.style.display = 'none';
    }
  };

  showAddRowButtonToSelectedContainer = (): void => {
    this.hideCloseRowButtonToSelectedContainer();
    const selectedElement = this.getSelectedElement();
    const iframeDocument = this.getDocument();
    this.addMinHeightToSelectedElement(selectedElement);
    if (selectedElement && iframeDocument) {
      const addElementButton = iframeDocument.getElementById(
        `${CONSTANTS.LOREE_ADD_ROW_BUTTON_FOR_CONTAINER}`,
      ) as HTMLElement;
      if (!addElementButton) return;
      this.elementAddButtonPlacement(selectedElement, addElementButton);
      addElementButton.style.display = 'inline-flex';
    }
  };

  checkDomainName = (): boolean => {
    const currentDomainName = sessionStorage.getItem('domainName');
    switch (currentDomainName) {
      case 'D2l':
        return true;
      case 'BB':
        return true;
      default:
        return false;
    }
  };

  wrapInvalidTags = (selectedElement: HTMLElement): HTMLElement => {
    const parentElement = getHighestChildUnderColumn(selectedElement);
    if (
      !parentElement ||
      ['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'OL', 'UL', 'LI', 'CAPTION'].includes(
        parentElement.tagName,
      ) ||
      parentElement.tagName === 'DIV' ||
      isImageWrapper(parentElement)
    ) {
      return selectedElement;
    }

    const pTag = document.createElement('p');
    pTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    wrapElement(parentElement, pTag);
    return pTag;
  };

  handleIframeMessageEvent = (): void => {
    window.addEventListener('message', (event) => {
      if (!isValidHost(event.origin)) {
        return;
      }
      if (event.data.type === 'colorPickerChange' || event.data.type === 'colorPickerSave') {
        this.handleColorPickerChange(event.data.key, event.data.color, event.data.type);
      }
      if (event.data.type === 'closeColorPicker') {
        switch (event.data.key) {
          case 'foregroundColor':
            this.closeForegroundColorPicker();
            break;
          case 'textBackgroundColor':
            this.closeBackgroundColorPicker();
            break;
          case 'borderColor':
            this.closeBorderColorPicker();
            break;
          case 'backgroundColor':
            this.closeBackgroundRowColumnColorPicker();
            break;
          case 'rowColumnBorderColor':
            this.closeBorderRowColumnColorPicker();
            break;
        }
      }
    });
  };

  // COLOR PICKER FUNCTIONS

  openIframeColorPicker = (
    pickerType: IframePopperInstance | null,
    position: [number, number],
    button: HTMLElement,
    wrapper: HTMLElement,
  ): void => {
    this.hidePopperInstances();
    const colorPickers = [
      'backgroundColorPickerPopperInstance',
      'rowColumnBorderColorPickerPopperInstance',
    ];
    if (pickerType) {
      pickerType.destroy();
      pickerType = null;
    }
    pickerType = createPopper(button, wrapper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            fallbackPlacements: ['top', 'bottom'],
          },
        },
        {
          name: 'offset',
          options: {
            offset: position,
          },
        },
      ],
    });
    wrapper.style.display = colorPickers.includes(String(pickerType)) ? 'block' : 'inline-flex';
  };

  closeForegroundColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER,
      );
      if (wrapper && wrapper.style.display !== 'none') {
        this.handleForeGroundColorPickerCancel(wrapper);
        this.handleColorPickerChange('foregroundColor', this.userForegroundColor(), '');
        this.customHexInputRemoveStyle(wrapper);
        wrapper.style.display = 'none';
      }
      if (foregroundColorPickerPopperInstance) {
        foregroundColorPickerPopperInstance.destroy();
        foregroundColorPickerPopperInstance = null;
      }
    }
  };

  foregroundColorChange = (type: string, color: string): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.fontColorStyle(type, color);
      this.customHexInputFocus(CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER);
    }
  };

  setuserForegroundColor(color: string) {
    userColor = color;
    return userColor;
  }

  userForegroundColor() {
    return userColor;
  }

  handleForeGroundColorPickerCancel = (pcrBorder: HTMLElement | null) => {
    const borderInputColor = pcrBorder?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    const userForegroundColor = this.userForegroundColor();
    if (borderInputColor?.value) {
      borderInputColor.value = userForegroundColor;
      this.setuserForegroundColor(userForegroundColor);
    }
  };

  closeBackgroundColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER,
      );
      if (wrapper && wrapper.style.display !== 'none') {
        this.handleTextBackGroundCancel(wrapper);
        this.customHexInputRemoveStyle(wrapper);
        wrapper.style.display = 'none';
      }
      if (textBackgroundColorPickerPopperInstance) {
        textBackgroundColorPickerPopperInstance.destroy();
        textBackgroundColorPickerPopperInstance = null;
      }
    }
  };

  backgroundColorChange = (color: string): void => {
    const iframeDocument = this.getDocument();
    const selectedElement: HTMLElement | null = this.getSelectedElement();
    if (!iframeDocument) return;
    const button = iframeDocument.getElementById(
      CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_BUTTON,
    );
    if (!button || !selectedElement) return;
    if (color) {
      button.style.backgroundColor = color;
      this.applyStyleToSelectedElement('background', color);
    } else {
      button.style.backgroundColor = '#ffffff';
      selectedElement.style.backgroundColor = '';
    }
    this.updateRowColumnColorPickerButton(
      CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_BUTTON,
      'backgroundColor',
    );
  };

  textBackgroundColorChange = (changeType: string, color: string, type?: string): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.bgColorStyle(changeType, color);
      if (type !== 'onHide') {
        this.customHexInputFocus(CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER);
      }
    }
  };

  userBackgroundColor() {
    return userBGColor;
  }

  handleTextBackGroundCancel = (pcrTextBackGround: HTMLElement | null) => {
    const backgroundInputColor = pcrTextBackGround?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    if (backgroundInputColor?.value) {
      backgroundInputColor.value = userBGColor;
      this.textBackgroundColorChange('', userBGColor, 'onHide');
    }
  };

  customiseTextBackgroundColorPickr = () => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const pcrTextBackGround = iframeDocument.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER,
    );
    const pcrTextBackGroundApply = pcrTextBackGround?.getElementsByClassName(
      'pcr-save',
    )[0] as HTMLElement;
    const pcrTextBackGroundClear = pcrTextBackGround?.getElementsByClassName(
      'pcr-clear',
    )[0] as HTMLElement;
    const pcrTextBackGroundCancel = pcrTextBackGround?.getElementsByClassName(
      'pcr-cancel',
    )[0] as HTMLElement;
    const bgInputColor = pcrTextBackGround?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    if (!pcrTextBackGroundApply) return;
    pcrTextBackGroundApply.onclick = () => {
      const previousBgColor = this.userBackgroundColor();
      userBGColor = validateHTMLColorHex(bgInputColor.value) ? bgInputColor.value : previousBgColor;
    };
    pcrTextBackGroundCancel.onclick = () => {
      this.handleTextBackGroundCancel(pcrTextBackGround);
    };
    pcrTextBackGroundClear.onclick = () => {
      userBGColor = '';
      bgInputColor.value = '';
    };
  };

  closeBorderColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
      );
      if (wrapper && wrapper.style.display !== 'none') {
        const previousBorderColor = this.userBorderColor();
        const previousBorderWidth = this.userBorderWidth();
        const previousBorderStyle = this.userBorderStyle();
        this.handleTextBorderColorPickerCancel(wrapper, previousBorderWidth, previousBorderStyle);
        this.handleColorPickerChange('borderColor', previousBorderColor, '');
        this.customHexInputRemoveStyle(wrapper);
        wrapper.style.display = 'none';
      }
      if (borderColorPickerPopperInstance) {
        borderColorPickerPopperInstance.destroy();
        borderColorPickerPopperInstance = null;
      }
    }
  };

  borderColorChange = (type: string, color: string): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.borderColorStyle(type, color);
      this.customHexInputFocus(CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER);
    }
  };

  handleBorderStyleChange = (option: string): void => {
    userTempBorderStyle = option;
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.borderStyleChange(option);
    }
  };

  handleBorderWidth = (borderWidthInput: HTMLInputElement, type: string): void => {
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.borderWidthChange(borderWidthInput.value);
      if (type !== 'cancel') borderWidthInput.focus();
    }
  };

  userBorderColor() {
    return userBorderColor;
  }

  userBorderStyle() {
    userBorderStyle = userBorderStyle.charAt(0).toUpperCase() + userBorderStyle.slice(1);
    if (userBorderStyle === '') {
      this.wordStylerObj?.borderStyleChange('Solid');
      userBorderStyle = 'Solid';
    }
    return userBorderStyle;
  }

  userBorderWidth() {
    if (userBorderWidth === '0') {
      this.wordStylerObj?.borderWidthChange('0');
    }
    return userBorderWidth;
  }

  handleTextBorderColorPickerCancel = (
    pcrBorder: HTMLElement | null,
    previousBorderWidth: string,
    previousBorderStyle: string,
  ) => {
    const borderInput = pcrBorder?.getElementsByClassName(
      'border-width-input',
    )[0] as HTMLInputElement;
    const borderStyle = pcrBorder?.getElementsByClassName(
      'border-style-dropdown',
    )[0] as HTMLElement;
    if (borderStyle) borderStyle.innerHTML = previousBorderStyle;
    if (this.validateWindowSelection()) {
      this.wordStylerObj?.borderStyleChange(previousBorderStyle);
    }
    userBorderWidth = previousBorderWidth;
    if (borderInput?.value) {
      borderInput.value = previousBorderWidth;
      this.handleBorderWidth(borderInput, 'cancel');
    }
  };

  customiseTextBorderColorPicker = () => {
    const iframeDocument = this.getDocument();
    const previousBorderWidth = this.userBorderWidth();
    const previousBorderColor = this.userBorderColor();
    const previousBorderStyle = this.userBorderStyle();
    if (iframeDocument) {
      const pcrBorder = iframeDocument.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
      );
      const pcrBorderCancel = pcrBorder?.getElementsByClassName('pcr-cancel')[0] as HTMLElement;
      const pcrBorderApply = pcrBorder?.getElementsByClassName('pcr-save')[0] as HTMLElement;
      const pcrBorderClear = pcrBorder?.getElementsByClassName('pcr-clear')[0] as HTMLElement;
      const borderInputColor = pcrBorder?.getElementsByClassName(
        'custom-hex-color-input',
      )[0] as HTMLInputElement;
      pcrBorderApply.onclick = () => {
        const borderInput = pcrBorder?.getElementsByClassName(
          'border-width-input',
        )[0] as HTMLInputElement;
        const previousBorderColor = this.userBorderColor();
        userBorderWidth = borderInput.value;
        userBorderStyle = userTempBorderStyle !== '' ? userTempBorderStyle : userBorderStyle;
        userTempBorderStyle = '';
        userBorderColor = validateHTMLColorHex(borderInputColor.value)
          ? borderInputColor.value
          : previousBorderColor;
      };
      pcrBorderCancel.onclick = () => {
        const borderInputColor = pcrBorder?.getElementsByClassName(
          'custom-hex-color-input',
        )[0] as HTMLInputElement;
        borderInputColor.value = previousBorderColor;
        userBorderColor = previousBorderColor;
        userBorderStyle = previousBorderStyle !== 'Solid' ? previousBorderStyle : 'Solid';
        userTempBorderStyle = '';
        this.handleTextBorderColorPickerCancel(pcrBorder, previousBorderWidth, previousBorderStyle);
      };
      pcrBorderClear.onclick = () => {
        userBorderColor = '#FFFFFF';
        borderInputColor.value = '#FFFFFF';
        userBorderWidth = '0';
        this.wordStylerObj?.borderStyleChange('Solid');
        userBorderStyle = 'Solid';
        userTempBorderStyle = '';
      };
    }
  };

  closeBackgroundRowColumnColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER,
      );
      if (wrapper && wrapper.style.display !== 'none') {
        this.handleRowColumnBackGroundCancel(wrapper);
        wrapper.style.display = 'none';
      }
      if (backgroundColorPickerPopperInstance) {
        backgroundColorPickerPopperInstance.destroy();
        backgroundColorPickerPopperInstance = null;
      }
    }
  };

  userRowBackGroundColor() {
    return userCustomStyle.row.backgroundColor;
  }

  userColumnBackGroundColor() {
    return userCustomStyle.column.backgroundColor;
  }

  updateUserRowBackGroundColor(color: string) {
    userCustomStyle.row.backgroundColor = color;
  }

  updateUserColumnBackGroundColor(color: string) {
    userCustomStyle.column.backgroundColor = color;
  }

  handleRowColumnBackGroundCancel = (pcrRowColumnBackGround: HTMLElement) => {
    const previousBackGroundColor = isRowEditOptionBlock
      ? this.userRowBackGroundColor()
      : this.userColumnBackGroundColor();
    const backgroundInputColor = getElementByClassName(
      pcrRowColumnBackGround,
      'pcr-result',
    ) as HTMLInputElement;
    if (backgroundInputColor?.value) {
      backgroundInputColor.value = previousBackGroundColor;
      this.backgroundColorChange(previousBackGroundColor);
    }
  };

  customiseRowColumnBackGroundColorPicker = () => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    isRowEditOptionBlock =
      (iframeDocument.getElementsByClassName('row-panel-active')[0] as HTMLElement) !== undefined;
    const pcrRowColumnBackGround = iframeDocument.getElementById(
      CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER,
    ) as HTMLElement;
    const pcrBackGroundApply = getElementByClassName(pcrRowColumnBackGround, 'pcr-save');
    const pcrBackGroundClear = getElementByClassName(pcrRowColumnBackGround, 'pcr-clear');
    const pcrBackGroundCancel = getElementByClassName(pcrRowColumnBackGround, 'pcr-cancel');
    const backGroundInputColor = getElementByClassName(
      pcrRowColumnBackGround,
      'pcr-result',
    ) as HTMLInputElement;
    pcrBackGroundApply.onclick = () => {
      if (isRowEditOptionBlock) {
        this.updateUserRowBackGroundColor(backGroundInputColor.value);
      } else {
        this.updateUserColumnBackGroundColor(backGroundInputColor.value);
      }
    };
    pcrBackGroundCancel.onclick = () => {
      this.handleRowColumnBackGroundCancel(pcrRowColumnBackGround);
    };
    pcrBackGroundClear.onclick = () => {
      backGroundInputColor.value = '#FFFFFF';
      if (isRowEditOptionBlock) {
        this.updateUserRowBackGroundColor(backGroundInputColor.value);
      } else {
        this.updateUserColumnBackGroundColor(backGroundInputColor.value);
      }
    };
  };

  closeBorderRowColumnColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const wrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
      );
      if (wrapper && wrapper.style.display !== 'none') {
        const previousBorderColor = isRowEditOptionBlock
          ? this.userRowBorderColor()
          : this.userColumnBorderColor();
        const previousBorderWidth = isRowEditOptionBlock
          ? this.userRowBorderWidth()
          : this.userColumnBorderWidth();
        this.handleRowColumnBorderColorPickerCancel(wrapper, previousBorderWidth);
        this.handleColorPickerChange('rowColumnBorderColor', previousBorderColor, '');
        wrapper.style.display = 'none';
      }
      if (rowColumnBorderColorPickerPopperInstance) {
        rowColumnBorderColorPickerPopperInstance.destroy();
        rowColumnBorderColorPickerPopperInstance = null;
      }
    }
  };

  updateRowColumnColorPickerButton = (colorPickerId: string, property: _Any): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const button = iframeDocument.getElementById(colorPickerId);
    if (!button) return;
    let color = '#ffffff';
    const selectedElement = this.getSelectedElement();
    if (selectedElement && selectedElement.style[property] !== '') {
      color = selectedElement.style[property];
    }
    button.style.backgroundColor = color;
  };

  handleRowColumnBorderWidth = (borderWidthInput: HTMLInputElement, type: string): void => {
    const selectedElement = this.getSelectedElements();
    if (!selectedElement) return;
    selectedElement[0].style.borderWidth = borderWidthInput.value + 'px';
    if (type !== 'cancel') borderWidthInput.focus();
  };

  rowColumnBorderColorChange = (color: string): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const button = iframeDocument.getElementById(
      CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON,
    );
    if (!button) return;
    if (color) {
      button.style.backgroundColor = color;
      this.applyStyleToSelectedElement('border-color', color);
    } else {
      button.style.backgroundColor = '#ffffff';
      this.applyStyleToSelectedElement('border-color', '');
    }
    const borderWidthWrapper = iframeDocument.getElementById(
      CONSTANTS.LOREE_ROW_COLUMN_BORDER_WIDTH_COLOR_PICKER,
    ) as HTMLInputElement;
    if (borderWidthWrapper)
      this.applyStyleToSelectedElement('border-width', borderWidthWrapper.value);
    const borderStyleWrapper = iframeDocument.getElementById(
      CONSTANTS.LOREE_ROW_COLUMN_BORDER_STYLE_COLOR_PICKER,
    ) as HTMLInputElement;
    if (borderStyleWrapper)
      this.updateRowColumnColorPickerButton(
        CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON,
        'borderColor',
      );
  };

  rowColumnBorderWidthChange = (e: Event): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const target = e.target as HTMLInputElement;
    this.applyStyleToSelectedElement('border-width', `${target.value}px`);
    if (!this.getSelectedElement()?.style.borderStyle)
      this.applyStyleToSelectedElement(
        'border-style',
        isRowEditOptionBlock ? userCustomStyle.row.borderStyle : userCustomStyle.column.borderStyle,
      );
  };

  rowColumnBorderStyleChange = (e: Event): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const target = e.target as HTMLInputElement;
    this.applyStyleToSelectedElement('border-style', target.value);
  };

  userRowBorderColor() {
    return userRowBorderColor;
  }

  updateUserRowBorderColor(color: string) {
    userRowBorderColor = color;
  }

  userRowBorderWidth() {
    return userRowBorderWidth;
  }

  getUserRowBorderStyle() {
    return userCustomStyle.row.borderStyle;
  }

  updateRowBorderStyle(style: string) {
    userCustomStyle.row.borderStyle = style;
  }

  updateUserRowBorderWidth(width: string) {
    userRowBorderWidth = width;
  }

  userColumnBorderColor() {
    return userCustomStyle.column.borderColor;
  }

  updateUserColumnBorderColor(color: string) {
    userCustomStyle.column.borderColor = color;
  }

  getColumnBorderStyle() {
    return userCustomStyle.column.borderStyle;
  }

  updateColumnBorderStyle(style: string) {
    userCustomStyle.column.borderStyle = style;
  }

  updateUserColumnBorderWidth(width: string) {
    userCustomStyle.column.borderWidth = width;
  }

  userColumnBorderWidth() {
    return userCustomStyle.column.borderWidth;
  }

  handleRowColumnBorderColorPickerCancel = (
    pcrBorder: HTMLElement | null,
    previousBorderWidth: string,
  ) => {
    this.applyStyleToSelectedElement(
      'border-style',
      isRowEditOptionBlock ? userCustomStyle.row.borderStyle : userCustomStyle.column.borderStyle,
    );
    const borderInput = pcrBorder?.getElementsByClassName(
      'rowColumnBorderInput',
    )[0] as HTMLInputElement;
    const borderStyleSelect = pcrBorder?.getElementsByClassName(
      'rowColumnBorderSelect',
    )[0] as HTMLSelectElement;
    if (borderInput?.value) {
      borderInput.value = previousBorderWidth;
      this.handleRowColumnBorderWidth(borderInput, 'cancel');
    } else {
      borderInput.value = '0';
      borderStyleSelect.selectedIndex = selectedIndexForBorderStyle(
        borderStyleSelect,
        isRowEditOptionBlock ? userCustomStyle.row.borderStyle : userCustomStyle.column.borderStyle,
      );
    }
  };

  customiseRowColumnBorderColorPicker = () => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const pcrBorder = iframeDocument.getElementById(
        CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
      ) as HTMLElement;
      isRowEditOptionBlock =
        (iframeDocument.getElementsByClassName('row-panel-active')[0] as HTMLElement) !== undefined;
      const previousBorderWidth = isRowEditOptionBlock
        ? this.userRowBorderWidth()
        : this.userColumnBorderWidth();
      const previousBorderColor = isRowEditOptionBlock
        ? this.userRowBorderColor()
        : this.userColumnBorderColor();
      const pcrBorderCancel = getElementByClassName(pcrBorder, 'pcr-cancel');
      const pcrBorderApply = getElementByClassName(pcrBorder, 'pcr-save');
      const pcrBorderClear = getElementByClassName(pcrBorder, 'pcr-clear');
      const borderInput = getElementByClassName(
        pcrBorder,
        'rowColumnBorderInput',
      ) as HTMLInputElement;
      const borderStyle = getElementByClassName(
        pcrBorder,
        'rowColumnBorderSelect',
      ) as HTMLSelectElement;
      const borderInputColor = getElementByClassName(pcrBorder, 'pcr-result') as HTMLInputElement;
      pcrBorderClear.onclick = () => {
        borderInput.value = '';
        borderStyle.selectedIndex = selectedIndexForBorderStyle(borderStyle, 'solid');
        isRowEditOptionBlock
          ? (userCustomStyle.row.borderStyle = 'solid')
          : (userCustomStyle.column.borderStyle = 'solid');
        borderInputColor.value = '#FFFFFF';
        if (isRowEditOptionBlock) {
          userRowBorderWidth = '0';
          userRowBorderColor = borderInputColor.value;
        } else {
          userCustomStyle.column.borderWidth = '0';
          userCustomStyle.column.borderColor = borderInputColor.value;
        }
      };
      pcrBorderApply.onclick = () => {
        if (isRowEditOptionBlock) {
          userRowBorderWidth = borderInput.value;
          userCustomStyle.row.borderStyle = borderStyle.innerHTML;
          userRowBorderColor = borderInputColor.value;
        } else {
          userCustomStyle.column.borderWidth = borderInput.value;
          userCustomStyle.column.borderColor = borderInputColor.value;
          userCustomStyle.column.borderStyle = borderStyle.value;
        }
      };
      pcrBorderCancel.onclick = () => {
        borderInputColor.value = previousBorderColor;
        if (isRowEditOptionBlock) {
          userRowBorderColor = previousBorderColor;
          userRowBorderWidth = previousBorderWidth;
        } else {
          userCustomStyle.column.borderColor = previousBorderColor;
          userCustomStyle.column.borderWidth = previousBorderWidth;
        }
        this.handleRowColumnBorderColorPickerCancel(pcrBorder, previousBorderWidth);
      };
    }
  };

  getSelectedBlockOptionMenu = () => {
    return isRowEditOptionBlock;
  };

  handleColorPickerChange = (key: string, color: string, type: string): void => {
    switch (key) {
      case 'foregroundColor':
        this.foregroundColorChange(type, color);
        this.customHexInput(CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER, color);
        break;
      case 'textBackgroundColor':
        this.textBackgroundColorChange(type, color, 'onOpen');
        this.customHexInput(CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER, color);
        break;
      case 'borderColor':
        this.borderColorChange(type, color);
        this.customHexInput(CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER, color);
        break;
      case 'backgroundColor':
        this.backgroundColorChange(color);
        break;
      case 'rowColumnBorderColor':
        this.rowColumnBorderColorChange(color);
        break;
    }
  };

  customHexInput = (id: string, color: string) => {
    const colorPicker = this.getDocument()?.getElementById(id);
    const customInput = colorPicker?.getElementsByClassName('pcr-interaction')[0]
      .childNodes[0] as HTMLInputElement;
    customInput.value = color;
  };

  customHexInputFocus = (id: string) => {
    const colorPicker = this.getDocument()?.getElementById(id);
    const customInput = colorPicker?.getElementsByClassName('pcr-interaction')[0]
      .childNodes[0] as HTMLInputElement;
    if (!customInput) return;
    customInput?.focus();
  };

  customHexInputRemoveStyle = (wrapper: HTMLElement) => {
    const customInput = wrapper.getElementsByClassName('pcr-interaction')[0]
      ? (wrapper.getElementsByClassName('pcr-interaction')[0].childNodes[0] as HTMLInputElement)
      : null;
    if (customInput?.classList?.contains('custom-border-danger'))
      customInput.classList.remove('custom-border-danger');
  };

  // hide color pickers

  hideColorPickersOfDesignSection = (): void => {
    let element;
    let defaultColor;
    const pickers = document.getElementsByClassName(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS,
    );
    Array.from(pickers).forEach((picker) => {
      if (picker.classList.contains('background-picker') && picker.classList.contains('visible')) {
        element = document.getElementById('imageBackgroundColorPicker');
        if (element) {
          defaultColor = this.designSec.getDefaultBackGroundColor();
          this.designSec.handleImageBackgroundColorChange(defaultColor, element);
        }
      }
      if (picker.classList.contains('border-picker') && picker.classList.contains('visible')) {
        element = document.getElementById('loree-design-section-image-border-color-picker');
        if (element) {
          defaultColor = this.designSec.getDefaultBorderColor();
          this.designSec.handleImageBorderColorChange(defaultColor, element);
        }
      }
      if (picker.classList.contains('line-picker') && picker.classList.contains('visible')) {
        element = document.getElementById('loree-design-section-line-color-picker');
        if (element) {
          defaultColor = this.designSec.getDefaultLineColor();
          this.designSec.handleLineBorderColorChange(defaultColor, element);
        }
      }
      picker.outerHTML = '';
    });
  };

  hideColorPickersOfContainerSection = (): void => {
    const pickers = Array.from(
      getElementsByClassName(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS),
    );
    for (const picker of pickers) {
      if (
        picker.classList.contains('visible') &&
        picker.classList.contains('container-background-picker')
      ) {
        const el = getElementById(CONSTANTS.LOREE_CONTAINER_BACKGROUND_COLOR_BUTTON);
        const color = this.containerDesign?.getContainerBackgroundColor() as string;
        this.containerDesign?.handleContainerBackgroundColorChange(color, el);
      }
      picker.outerHTML = '';
    }
  };
}

export default Base;
