import EditorSelection from './selection';
import {
  spanSelection,
  spanPreviousSelection,
  brSelection,
  brPreviousSelection,
  selection,
  previousSelection,
  selectionMockData,
  listSelection,
} from './selectionMockData';

describe('#Selection', () => {
  let editorSelection: EditorSelection;
  beforeEach(() => {
    jest.clearAllMocks();
    editorSelection = new EditorSelection();
    editorSelection.createRangeToSelection = jest
      .fn()
      .mockImplementation((Selection) => 'selectedText');
    editorSelection.setRangeForSelection = jest
      .fn()
      .mockImplementation((Selection) => 'selectedText');
    document.createRange = jest.fn();
  });

  test('validate the selected text', () => {
    const validateSelection = editorSelection.validateSelection(
      selectionMockData,
      spanPreviousSelection as Range,
      false,
    );
    expect(validateSelection).toBe(true);
  });

  test('check the selection contains span tag', () => {
    editorSelection.isBRtag = true;
    const spyusermodifyRangeForSelection = jest.spyOn(editorSelection, 'setRangeForSelection');
    editorSelection.validatePreviousSelection(spanSelection, spanPreviousSelection as Range);
    editorSelection.validateBRTagInSelection = jest
      .fn()
      .mockImplementation((spanSelection) => true);
    expect(editorSelection.validateBRTagInSelection(spanPreviousSelection as Range)).toBe(true);
    expect(spyusermodifyRangeForSelection).toBeCalledTimes(1);
    expect(editorSelection.isBRtag).toBe(true);
  });
  test('check the selection contains BR tag', () => {
    editorSelection.isBRtag = true;
    const spysetcreateRangeToSelection = jest.spyOn(editorSelection, 'createRangeToSelection');
    editorSelection.validatePreviousSelection(brSelection, brPreviousSelection as Range);
    editorSelection.validateRangeForSelection = jest
      .fn()
      .mockImplementation((spanSelection) => true);
    expect(editorSelection.validateRangeForSelection(spanPreviousSelection as Range)).toBe(true);
    expect(spysetcreateRangeToSelection).toBeCalledTimes(1);
    expect(editorSelection.isBRtag).toBe(false);
  });
  test('check the selection which already have styles', () => {
    const spysetRangeForSelection = jest.spyOn(editorSelection, 'setRangeForSelection');
    editorSelection.validatePreviousSelection(selection, previousSelection as Range);
    expect(spysetRangeForSelection).toBeCalledTimes(1);
    expect(editorSelection.isBRtag).toBe(false);
  });
});

describe('#modifySelectedNode', () => {
  let editorSelection: EditorSelection;
  beforeEach(() => {
    jest.clearAllMocks();
    editorSelection = new EditorSelection();
    const spyRange = {
      selectNodeContents: jest.fn(),
    };
    document.createRange = jest.fn().mockImplementation(() => spyRange);
  });

  test('#Previous selection null', async () => {
    editorSelection.modifyRangeForSelection(selectionMockData, null);
    const selection = document.getSelection();
    expect(selection?.anchorOffset).toBe(0);
    expect(selection?.focusOffset).toBe(0);
  });

  test('when it has a valid selected Range', () => {
    editorSelection.modifyRangeForSelection(spanSelection, spanPreviousSelection as Range);
    expect(spanSelection.anchorOffset).toBe(4);
    expect(spanSelection?.focusOffset).toBe(5);
  });

  test('when selection does not contain anchorNode', () => {
    editorSelection.modifyRangeForSelection(selectionMockData, spanPreviousSelection as Range);
    const selection = document.getSelection();
    expect(selection?.anchorOffset).toBe(0);
    expect(selection?.focusOffset).toBe(0);
  });
});

describe('#ListItem Selection', () => {
  let editorSelection: EditorSelection;
  beforeAll(() => {
    jest.clearAllMocks();
    editorSelection = new EditorSelection();
    const spyRange = {
      selectNodeContents: jest.fn(),
    };
    document.createRange = jest.fn().mockImplementation(() => spyRange);
  });
  test('validateRangeForSelection', () => {
    const spyValidateRangeForSelection = jest.spyOn(editorSelection, 'validatePreviousSelection');
    editorSelection.validateSelection(listSelection, previousSelection as Range, false);
    expect(listSelection.removeAllRanges).toBeCalledTimes(1);
    expect(spyValidateRangeForSelection).toBeCalledTimes(1);
  });

  test('validateRange for previous Selection', () => {
    const spyValidateRangeForSelection = jest
      .spyOn(editorSelection, 'validateRangeForSelection')
      .mockImplementation(() => true);
    const spyCreateRange = jest.spyOn(editorSelection, 'createRangeToSelection');
    editorSelection.validatePreviousSelection(listSelection, previousSelection as Range);
    expect(editorSelection.isBRtag).toBe(false);
    expect(spyValidateRangeForSelection).toHaveBeenCalledTimes(1);
    expect(spyCreateRange).toHaveBeenCalledTimes(1);
  });

  test('createRange for selection', () => {
    const spyValidateListSelection = jest
      .spyOn(editorSelection, 'validateRangeForSingleList')
      .mockImplementation(() => true);
    const spyStartContainer = jest.spyOn(editorSelection, 'getStartContainer');
    editorSelection.createRangeToSelection(listSelection, previousSelection as Range);
    expect(spyValidateListSelection).toHaveBeenCalledTimes(1);
    expect(spyStartContainer).toHaveBeenCalledTimes(3);
  });
});
