import { selectedElement } from '../base';

export default class EditorSelection {
  selectionTextChange: Range | null = null;
  isBRtag = false;

  validateSelection = (
    selectedText: Selection,
    previousSelection: Range | null,
    applyFontFamily: boolean,
  ) => {
    if (!previousSelection) {
      return;
    }
    const validateRangeForListConversion = this.validateRangeForListConversion(
      selectedText,
      previousSelection,
    );
    const validateRangeForText =
      selectedText?.type !== 'Range' || selectedText?.focusNode?.nodeName === 'LI';
    const validateOffsets = this.checkOffsetsForSlection(previousSelection);
    const checkForBRtag =
      previousSelection?.startContainer?.nextSibling?.nodeName === 'BR' && validateOffsets;
    const range = document.createRange();
    const node = this.getCommonAncestor(previousSelection);

    switch (true) {
      case !applyFontFamily && validateRangeForListConversion: // set range for paragraph to list item conversion
        selectedText?.removeAllRanges();
        range.selectNodeContents(node as Node);
        selectedText?.addRange(range);
        break;
      case validateRangeForText: // validate the range while increasing font-size for selection
        selectedText?.removeAllRanges();
        this.validatePreviousSelection(selectedText, previousSelection);
        break;
      case checkForBRtag: // set the status if the selection contains BR tag
        this.selectionTextChange = previousSelection;
        this.isBRtag = true;
        break;
    }

    return true;
  };

  getCommonAncestor = (previousSelection: Range | null) => {
    return previousSelection?.commonAncestorContainer; // get selection's parentElement
  };

  getEndContainer = (previousSelection: Range | null) => {
    return previousSelection?.endContainer; // get selection's endCotainer Element
  };

  getEndContainerParentSibling = (previousSelection: Range | null) => {
    return previousSelection?.endContainer?.parentNode?.previousSibling; // get selection's endCotainer's parentNode's sibling
  };

  getStartContainer = (previousSelection: Range | null) => {
    return previousSelection?.startContainer; // get selection's startContainer Element
  };

  checkOffsetsForSlection = (previousSelection: Range | null) => {
    return previousSelection?.startOffset !== previousSelection?.endOffset; // check the offsets for selection
  };

  validateRangeForListConversion = (selectedText: Selection, previousSelection: Range | null) => {
    const parentElement = this.getCommonAncestor(previousSelection);
    const endContainer = this.getEndContainer(previousSelection);
    const startContainerNodeName = this.getStartContainer(previousSelection);
    const nodeName = startContainerNodeName?.nextSibling?.nodeName;
    return (
      selectedText?.anchorNode?.nodeName === '#text' &&
      (selectedText?.focusOffset === 0 || selectedText.anchorOffset === 0) &&
      !endContainer?.nextSibling &&
      (nodeName === 'SPAN' || nodeName === 'A') &&
      parentElement?.nodeName !== '#text'
    );
  };

  validateBRTagInSelection = (previousSelection: Range | null) => {
    const startContainer = this.getStartContainer(previousSelection);
    const isOffsets = this.checkOffsetsForSlection(previousSelection);
    return (
      this.isBRtag &&
      this.selectionTextChange === previousSelection &&
      startContainer?.nextSibling?.nodeName === 'SPAN' &&
      isOffsets
    );
  };

  validateRangeForSelection = (previousSelection: Range | null) => {
    const validateOffset = this.checkOffsetsForSlection(previousSelection);
    const parentNode = this.getCommonAncestor(previousSelection);
    const nodeName = parentNode?.nodeName;
    return (
      !validateOffset ||
      previousSelection?.endOffset === 0 ||
      nodeName === 'UL' ||
      nodeName === 'OL'
    );
  };

  validatePreviousSelection = (selectedText: Selection, previousSelection: Range | null) => {
    const validateBRtag = this.validateBRTagInSelection(previousSelection); // set range if the selection contains BR tag
    const validateRangeForSelection = this.validateRangeForSelection(previousSelection); // create range for the partial selection
    const parentNode = this.getCommonAncestor(previousSelection); // to get the parent tag
    const nodeName = parentNode?.nodeName;
    switch (true) {
      case validateBRtag:
        this.modifyRangeForSelection(selectedText, previousSelection);
        break;
      case validateRangeForSelection:
        this.isBRtag = false;
        this.createRangeToSelection(selectedText, previousSelection);
        break;
      case nodeName !== 'UL' && nodeName !== 'OL': // set range for content that are not list items
        this.setRangeForSelection(selectedText, previousSelection);
        break;
    }
  };

  modifyRangeForSelection = (selectedText: Selection, previousSelection: Range | null) => {
    if (!previousSelection) {
      return;
    }
    const modifyRangeForSelected = previousSelection?.startContainer?.nextSibling;
    if (selectedText.anchorNode) {
      selectedText?.removeRange(previousSelection);
    }
    const range = document.createRange();
    range.selectNodeContents(modifyRangeForSelected as Node);
    selectedText?.addRange(range);
  };

  setRangeForSelection = (selectedText: Selection, previousSelection: Range | null) => {
    const startContainer = this.getStartContainer(previousSelection);
    const endContainer = this.getEndContainer(previousSelection);
    this.isBRtag = !!(
      startContainer?.nextSibling?.nodeName === 'BR' ||
      (endContainer?.nodeName !== '#text' && startContainer?.nodeName === '#text')
    );
    this.selectionTextChange = previousSelection;
    selectedText?.addRange(previousSelection as Range);
  };

  validateRangeForList = (previousSelection: Range | null) => {
    const endContainer = this.getEndContainer(previousSelection);
    const parentNode = this.getCommonAncestor(previousSelection);
    const nodeName = parentNode?.nodeName;
    return (nodeName === 'UL' || nodeName === 'OL') && endContainer?.nodeName === '#text';
  };

  validateRangeForPartialSelection = (previousSelection: Range | null) => {
    let endContainer: Node | undefined | String = this.getEndContainer(previousSelection);
    endContainer = endContainer?.nextSibling?.nodeName;
    const endContainerParentSibling = this.getEndContainerParentSibling(previousSelection);
    const nodeName = endContainerParentSibling?.nodeName;
    return (
      (nodeName === 'SPAN' && !endContainer) || (nodeName === 'SPAN' && endContainer === 'SPAN')
    );
  };

  validateRangeForText = (previousSelection: Range | null) => {
    const parentElement = this.getCommonAncestor(previousSelection);
    const startContainer = this.getStartContainer(previousSelection);
    return (
      startContainer?.nodeType !== 1 &&
      startContainer?.nextSibling &&
      parentElement?.nodeName === '#text'
    );
  };

  validateRangeForSingleList = (previousSelection: Range | null) => {
    const endContainer = this.getEndContainer(previousSelection);
    const parentNode = this.getCommonAncestor(previousSelection);
    const nodeName = parentNode?.nodeName;
    return (nodeName === 'UL' || nodeName === 'OL') && endContainer?.nodeName === 'LI';
  };

  createRangeToSelection = (selectedText: Selection, previousSelection: Range | null) => {
    const parentElement = this.getCommonAncestor(previousSelection); // set selection for whole list item
    const validateRangeforListSelection = this.validateRangeForList(previousSelection); // set selection for list
    const validateRangeforListItem = this.validateRangeForSingleList(previousSelection); // set selection for single list item
    const validateListItem = parentElement?.nodeName === 'LI'; // set selection for partial content selection in paragraph/header/list
    const validateOffset = this.checkOffsetsForSlection(previousSelection);
    const validateParentElement = parentElement && parentElement?.nodeValue !== ''; // set selection for whole paragraph/header
    const startContainer = this.getStartContainer(previousSelection);
    const createdRange = this.createRangeForPtag(previousSelection);
    let newNode;
    let newRange;

    switch (true) {
      case validateRangeforListSelection:
        newNode = parentElement as Node;
        break;
      case validateRangeforListItem:
        newNode = startContainer?.parentNode as Node;
        break;
      case validateListItem && validateOffset:
        newNode = this.createRangeForListItem(previousSelection);
        break;
      case validateListItem && !validateOffset:
        newRange = previousSelection as Range;
        break;
      case !validateListItem:
        newRange = createdRange.newRange;
        newNode = createdRange.newNode;
        break;
      case validateParentElement:
        newNode = parentElement?.parentNode as Node;
        break;
    }
    // creating range to the selected content
    if (!newRange && newNode) {
      const range = document.createRange();
      range.selectNodeContents(newNode);
      selectedText?.addRange(range);
    } else if (newRange) selectedText?.addRange(newRange);
  };

  createRangeForListItem = (previousSelection: Range | null) => {
    const validateRangeforPartialSelection =
      this.validateRangeForPartialSelection(previousSelection); // set range when partially selected
    const endContainer = this.getEndContainer(previousSelection);
    const endContainerParentSibling = this.getEndContainerParentSibling(previousSelection);
    let newNode;

    if (validateRangeforPartialSelection) newNode = endContainerParentSibling as Node;
    else newNode = endContainer?.previousSibling as Node;

    return newNode;
  };

  createRangeForPtag = (previousSelection: Range | null) => {
    const validateRangeforText = this.validateRangeForText(previousSelection); // set range if the tagname is #text
    const validateRangeforPartialSelection =
      this.validateRangeForPartialSelection(previousSelection); // set range when partially selected
    const endContainer = this.getEndContainer(previousSelection);
    const startContainer = this.getStartContainer(previousSelection);
    const endContainerParentSibling = this.getEndContainerParentSibling(previousSelection);
    let newNode;
    let newRange;

    switch (true) {
      case validateRangeforPartialSelection:
        newNode = endContainerParentSibling as Node;
        break;
      case validateRangeforText:
        newNode = startContainer?.nextSibling as Node;
        break;
      case endContainer?.nodeName === '#text':
        newNode = endContainer?.previousSibling as Node;
        break;
      default:
        newRange = previousSelection as Range;
    }
    return { newNode, newRange };
  };
}

export function getCurrentSelectedElement(): HTMLElement | null {
  return selectedElement as HTMLElement;
}

export function getFontFamiliesFromRange(range: Range) {
  return getStyleSettingsFromRange(range, 'font-family');
}

export function getStyleSettingsFromRange(range: Range, style: string): string[] {
  return [];
}

function getNextNode(node: Node) {
  if (node.firstChild) return node.firstChild;
  while (node) {
    if (node.nextSibling) return node.nextSibling;
    node = node.parentNode as Node;
  }
  return null;
}

export function getNodesInRange(range: Range) {
  const start = range.startContainer;
  const end = range.endContainer;
  const commonAncestor = range.commonAncestorContainer;
  const nodes = [];

  // walk parent nodes from start to common ancestor
  for (let node = start.parentNode; node; node = node.parentNode) {
    nodes.push(node);
    if (node === commonAncestor) break;
  }
  nodes.reverse();

  // walk children and siblings from start until end is found
  for (let node: Node | null = start; node; node = getNextNode(node)) {
    nodes.push(node);
    if (node === end) break;
  }

  return nodes;
}
