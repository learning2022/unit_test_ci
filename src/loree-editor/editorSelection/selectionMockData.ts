export const spanSelection = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  anchorOffset: 4,
  focusOffset: 5,
  removeAllRanges: jest.fn(),
  addRange: jest.fn(),
} as unknown as Selection;

export const spanPreviousSelection = {
  commonAncestorContainer: {
    nextSibling: {
      nodeName: 'LI',
    },
  },
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 8,
  endOffset: 9,
};

export const brSelection = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 2,
  endOffset: 3,
} as unknown as Selection;

export const brPreviousSelection = {
  commonAncestorContainer: {
    nodeName: 'P',
  },
  startContainer: {
    nodeName: 'UL',
    nextSibling: {
      nodeName: 'UL',
    },
  },
  endContainer: {
    nodeName: 'UL',
  },
  startOffset: 0,
  endOffset: 0,
};

export const selection = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 2,
  endOffset: 3,
} as unknown as Selection;

export const previousSelection = {
  commonAncestorContainer: {
    nextSibling: {
      nodeName: 'LI',
    },
  },
  startOffset: 5,
  endOffset: 7,
};

export const selectionMockData = {
  startContainer: {
    nextSibling: {
      nodeName: 'SPAN',
    },
  },
  startOffset: 4,
  endOffset: 5,
  removeAllRanges: jest.fn(),
  addRange: jest.fn(),
} as unknown as Selection;

export const listSelection = {
  type: 'Caret',
  focusNode: {
    nodeName: 'LI',
  },
  startContainer: {
    parentNode: (document.createElement('li').innerText = 'Insert Text Here'),
  },
  addRange: jest.fn(),
  removeAllRanges: jest.fn(),
} as unknown as Selection;
