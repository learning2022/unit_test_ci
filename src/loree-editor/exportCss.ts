import { generateRandomString } from './utils';
const usedClassNames: string[] = [];
let css = '';

export const getExternalCss = (html: HTMLElement) => {
  const elements = html.getElementsByTagName('*');
  Array.from(elements).forEach((element) => {
    const ele = element as HTMLElement;
    stripStyleTag(ele);
  });
  return true;
};

const stripStyleTag = (html: HTMLElement) => {
  const cssText = html.style.cssText;
  if (cssText !== '') {
    const className = generateUniqueClassName();
    html.classList.add(className);
    html.removeAttribute('style');
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    css += `.${className}{${cssText}} `;
  }
};

export const generateUniqueClassName = (): string => {
  const className = generateRandomString(6);
  if (usedClassNames.includes(className)) {
    return generateUniqueClassName();
  }
  usedClassNames.push(className);
  return className;
};
