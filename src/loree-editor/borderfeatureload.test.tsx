/* eslint-disable */ // Remove this line when editing this file
import Base from './base';
import CONSTANTS from './constant';

// For border process
describe('Border loading', () => {
  const currentElement = document.implementation.createHTMLDocument('New Document');
  const baseInstance = new Base();
  beforeEach(() => {
    const innerDiv = currentElement.createElement('div');
    innerDiv.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_SECTION_WRAPPER;
    currentElement.body.appendChild(innerDiv);
  });
  test('Loading feature content while editing in row', () => {
    const result: any = baseInstance.borderCheckForBlockEditFeatures('row', currentElement);
    const resultArr = Object.values(result);
    expect(expect.arrayContaining(resultArr)).toEqual(['']);
  });
  test('Loading feature content while editing in column', () => {
    const result: any = baseInstance.borderCheckForBlockEditFeatures('column', currentElement);
    const resultArr = Object.values(result);
    expect(expect.arrayContaining(resultArr)).toEqual(['']);
  });
  test('Loading feature content while editing in other element', () => {
    const result: any = baseInstance.borderCheckForBlockEditFeatures('cell', currentElement);
    const resultArr = Object.values(result);
    expect(['d-none']).toEqual(expect.arrayContaining(resultArr));
  });
});
