import validHosts from './validHosts.json';
import * as utils from './utils';
import {
  createSelectElement,
  getElementsByClassName,
  getElementByQuerySelector,
  createDiv,
  getElementsByQuerySelectorAll,
} from './common/dom';
import CONSTANTS from './constant';
import { headerMock, imageWithText, paragraphMock, tableMockData } from './baseMockData';
import { alignmentMenuMock } from './modules/sidebar/mockData';

describe('Utility functions', () => {
  test.each([
    { numberValue: 5, expected: '05' },
    { numberValue: 55, expected: '55' },
    { numberValue: 555, expected: '555' },
  ])('converting into double digit', ({ numberValue, expected }) => {
    expect(utils.convertIntoDoubleDigit(numberValue)).toBe(expected);
  });
  test('Remove the hyphen from the given word', () => {
    const testInput = 'this-word-contains-special-characters';
    expect(utils.replaceHyphenSpecialCharacter(testInput)).toBe(
      'thiswordcontainsspecialcharacters',
    );
  });
});

describe('Host validator functions', () => {
  it('returns true with validHosts', () => {
    validHosts.forEach((host) => expect(utils.isValidHost(host)).toBe(true));
  });
  it('returns false with an invalid host', () => {
    expect(utils.isValidHost('wepwn.you.com')).toBe(false);
  });
});

describe('Generate random string', () => {
  let randomLength;
  test.each([
    { randomLength: 6, expected: 6 },
    { randomLength: 4, expected: 4 },
  ])(
    'should return the random string with number of charaters sent',
    ({ randomLength, expected }) => {
      expect(utils.generateRandomString(randomLength).length).toBe(expected);
    },
  );

  it('should return the empty string', () => {
    randomLength = 0;
    const result = utils.generateRandomString(randomLength);
    expect(result).toEqual('');
  });
});

describe('Border Select options', () => {
  let selectElement: HTMLElement;
  let selectedBorderStyle: string;

  beforeAll(() => {
    selectElement = createSelectElement('loree-border-style');
    document.body.appendChild(selectElement);
  });
  test('border select is options are rendered', () => {
    selectElement.innerHTML = utils.createBorderStyleOptions();
    expect(selectElement.innerHTML).toBe(
      '<option value="dashed">Dashed</option><option value="dotted">Dotted</option><option value="double">Double</option><option value="solid">Solid</option>',
    );
  });
  test('selected border style index in option', () => {
    selectedBorderStyle = 'double';
    expect(utils.selectedIndexForBorderStyle(selectElement, selectedBorderStyle)).toBe(2);
  });
  test.each(['wavy', 'inset', 'outset'])('invalid border style index in option', (style) => {
    selectedBorderStyle = style;
    expect(utils.selectedIndexForBorderStyle(selectElement, selectedBorderStyle)).toBe(-1);
  });
  test('empty border style index in option', () => {
    selectedBorderStyle = '';
    expect(utils.selectedIndexForBorderStyle(selectElement, selectedBorderStyle)).toBe(-1);
  });
});

describe('#removeSelectedElementSection', () => {
  document.body.innerHTML = `<div class="sectionContent" id=${CONSTANTS.LOREE_SIDEBAR_ELEMENT_COLLAPSE}><div class="sidebarElementsWrapper collapse show" id="loree-sidebar-element-wrapper" data-parent="#loree-sidebar-element-section-button"><button class="sidebarElement"><div id="image" class="icon-block active">Image</div></button></div></div>`;
  test('remove active class from sidebar elements', () => {
    utils.removeSelectedElementSection();
    expect(getElementsByClassName('active').length).toEqual(0);
  });
  test('should not fail when there is no active class', () => {
    expect(getElementsByClassName('active').length).toEqual(0);
    utils.removeSelectedElementSection();
    expect(getElementsByClassName('active').length).toEqual(0);
  });
});

describe('#hideLanguageMenu', () => {
  beforeAll(() => {
    document.body.innerHTML = `<div x-placement="bottom-start" aria-labelledby="" class="loree-language-menu dropdown-menu-editor-alignment dropdown-menu show" data-popper-reference-hidden="false" data-popper-escaped="false" data-popper-placement="bottom-start" style="position: absolute; inset: 0px auto auto 0px; margin: 0px; transform: translate(-1px, 40px);"><a href="#" class="activeLanguage dropdown-item" role="button">Chinese</a><a href="#" class="dropdown-item" role="button">English</a></div>`;
  });
  test('dropdown menu toggle show on hide functionality', () => {
    expect(getElementByQuerySelector('.loree-language-menu')?.classList).toContain('show');
    utils.hideHeaderLanguageMenu();
    expect(getElementByQuerySelector('.loree-language-menu')?.classList).not.toContain('show');
  });
});

describe('#currentI18nLanguage', () => {
  test('if i18n has not been set should return default', () => {
    expect(utils.getCurrentI18nLanguage()).toBe('en');
  });
  test('current i18n should return the stored value', () => {
    localStorage.setItem('loreeLng', 'zh');
    expect(utils.getCurrentI18nLanguage()).toBe('zh');
  });
});

describe('#addDefaultLanguageToElements', () => {
  beforeAll(() => {
    localStorage.setItem('loreeLng', 'ja');
  });
  test.each([
    {
      text: headerMock,
    },
    {
      text: paragraphMock,
    },
  ])('direct textContent elements with default language', ({ text }) => {
    const wrapper = createDiv();
    wrapper.innerHTML = '';
    wrapper.innerHTML = text;
    utils.addUserLanguageAsDefaultToElement(wrapper.firstElementChild as HTMLElement);
    expect(wrapper.firstElementChild).toHaveAttribute('lang', 'ja');
  });
  test.each([
    {
      text: tableMockData,
    },
    {
      text: imageWithText,
    },
    {
      text: alignmentMenuMock,
    },
  ])('text elements with other elements should also get default lang', ({ text }) => {
    const wrapper = createDiv();
    wrapper.innerHTML = '';
    wrapper.innerHTML = text;
    utils.addUserLanguageAsDefaultToElement(wrapper.firstElementChild as HTMLElement);
    const headerElements = getElementsByQuerySelectorAll(
      'h3',
      wrapper.firstElementChild as HTMLElement,
    );
    headerElements?.forEach((element) => expect(element).toHaveAttribute('lang', 'ja'));
    const paragraphElements = getElementsByQuerySelectorAll(
      'p',
      wrapper.firstElementChild as HTMLElement,
    );
    paragraphElements?.forEach((element) => expect(element).toHaveAttribute('lang', 'ja'));
  });
});

describe('#addDefaultLangOnLangEnableStatus', () => {
  beforeAll(() => {
    process.env.REACT_APP_ENABLE_LOCALISATION = 'false';
  });
  test('when language options are disabled en should be default', () => {
    const wrapper = createDiv();
    wrapper.innerHTML = '';
    wrapper.innerHTML = '<p class="loree-iframe-content-element">Insert text here</p>';
    utils.addUserLanguageAsDefaultToElement(wrapper.firstElementChild as HTMLElement);
    expect(wrapper.firstElementChild).toHaveAttribute('lang', 'en');
  });
});

describe('#updateUserLanguageStore', () => {
  test('default selected mini menu language to be empty', () => {
    expect(utils.getUserLanguageStore().selectedMiniMenuLanguage).toBe('');
  });
  test('after update function triggered the language store should be updated', () => {
    for (const langauge of CONSTANTS.LOREE_MINI_MENU_LANGUAGE_LISTS) {
      utils.updateUserLanguageStore('selectedMiniMenuLanguage', langauge.isoCode);
      expect(utils.getUserLanguageStore().selectedMiniMenuLanguage).toBe(langauge.isoCode);
    }
  });
});
