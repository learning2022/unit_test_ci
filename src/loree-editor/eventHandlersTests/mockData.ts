/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
const editorMockData = {
  loreeIframe: 'loree-iframe',
  loreeIframeElement: 'loree-iframe-element',
  isTagExists: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'ol', 'ul', 'caption'],
  mouseEvent: {
    altKey: false,
    ctrlKey: false,
    metaKey: false,
    shiftKey: false,
    srcElement: `div.col-12.loree-iframe-content-column.column-highlight.element-highlight`,
    target: `div.col-12.loree-iframe-content-column.column-highlight.element-highlight`,
    type: 'click',
    which: 1,
    x: 473,
  } as unknown as MouseEvent,
  selection: {
    anchorNode: null,
    type: 'Range',
  } as Selection,
};

export default editorMockData;
