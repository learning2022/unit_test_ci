/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import Iframe from '../modules/iframe/iframe';
import { shallow } from 'enzyme';
import editorMockData from './mockData';
function mockIframeComponent() {
  return (
    <iframe id={editorMockData.loreeIframe} data-testid={editorMockData.loreeIframe}>
      <p className={editorMockData.loreeIframeElement}>Insert Text</p>
      <ul className={editorMockData.loreeIframeElement} id='test-unorder-list'>
        <li>Insert Text</li>
        <li>Insert Text</li>
      </ul>
    </iframe>
  );
}

describe('When user select text using Control+A', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = shallow(mockIframeComponent());
  });

  const ctrlEvent = new KeyboardEvent('keydown', { keyCode: 17 });
  const aEvent = new KeyboardEvent('keydown', { keyCode: 65 });
  const iframeElement = new Iframe();

  const selection = ({
    anchorNode: {
      nodeName: '#text',
      nextSibling: null,
      firstChild: null,
      parentElement: {
        className: 'loree-iframe-content-element element-highlight',
        innerHTML: 'Insert text here',
        nodeName: 'p',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
        },
        parentElement: {
          childElementCount: 2,
        },
      } as HTMLElement,
      getAttribute: {},
    } as HTMLElement,
    type: 'Range',
  } as unknown) as Selection;

  test('check iframe exists', () => {
    expect(wrapper).toContainElement;
  });

  test('mock the keyboard event', () => {
    wrapper.find('.loree-iframe-element').at(0).simulate('keydown', ctrlEvent);
    wrapper.find('.loree-iframe-element').at(0).simulate('keydown', aEvent);
    document.dispatchEvent(ctrlEvent);
    document.dispatchEvent(aEvent);
    expect(ctrlEvent).toBeTruthy();
    expect(ctrlEvent).toBeTruthy();
  });

  test('capture the key event', () => {
    expect(ctrlEvent.keyCode).toEqual(17);
    expect(aEvent.keyCode).toEqual(65);
  });

  test('is Select All event triggered?(ie: verify 2 keys)', () => {
    iframeElement.attachEventListeners();
    document.body.innerHTML = ` <iframe id=${editorMockData.loreeIframe} data-testid=${editorMockData.loreeIframe}><p className=${editorMockData.loreeIframeElement}>Insert Text</p></iframe>`;
    expect(wrapper.find('#loree-iframe').text()).toEqual('Insert TextInsert TextInsert Text');
    expect(iframeElement.eventDetails).toBe(2);
  });

  test('verify is text selected?', () => {
    const selectedElementTag = wrapper.find('.loree-iframe-element').at(0).getElement();
    expect(selectedElementTag).not.toBeNull();
  });

  test('modify the range of a selection if Control All pressed', () => {
    const modifyOverallSelectionRange = jest.fn();
    if (ctrlEvent.keyCode && aEvent.keyCode) modifyOverallSelectionRange();
    expect(modifyOverallSelectionRange).toHaveBeenCalledTimes(1);
  });
  test('check selected text element tag is valid', () => {
    const selectedElementTag = wrapper.find('.loree-iframe-element').at(0).getElement().type;
    const isvalidTagName = editorMockData.isTagExists.includes(selectedElementTag.toString());
    expect(isvalidTagName).toBeTruthy();
  });

  test('test the handle function called', () => {
    const handleQuicklinkInsertLinkForLists = jest.fn();
    const pTagElement = document.createElement('p');
    pTagElement.onclick = () => handleQuicklinkInsertLinkForLists();
    pTagElement.click();
    expect(handleQuicklinkInsertLinkForLists).toBeCalledTimes(1);
    expect(wrapper.find('#test-unorder-list').at(0).children().length).toBe(2);
  });

  test('check the link, and quicklink options', () => {
    const optionWrapper = document.createElement('div');
    const headerOption = document.createElement('button');
    headerOption.innerText = 'Header';
    const linkOption = document.createElement('button');
    linkOption.innerText = 'Link';
    optionWrapper.appendChild(headerOption);
    optionWrapper.appendChild(linkOption);
    const optionSWrapper = optionWrapper.getElementsByTagName('button');
    iframeElement.hideOptions(true, selection, optionSWrapper[0], optionSWrapper[1]);
    expect(optionSWrapper[0].classList.contains('d-none')).toBeTruthy;
    expect(optionSWrapper[1].classList.contains('d-none')).toBeTruthy;
  });
});

describe('When user tripple clicks', () => {
  const iframeElement = new Iframe();
  test('Check the selection has a valid anchor node', () => {
    document.createRange = jest.fn();
    const mouseSpy = jest.spyOn(iframeElement, 'handleMouseDoubleClick');
    iframeElement.handleTripleClickEvent(editorMockData.mouseEvent, editorMockData.selection);
    expect(mouseSpy).not.toHaveBeenCalled(); //returns 0 if invalid anchor node exists
  });
});
