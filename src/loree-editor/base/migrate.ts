import { elementHasClass, wrapChildElements } from '../common/dom';
import CONSTANTS from '../constant';
import { addClassToElement, isClassExist } from '../utils';
import { ElementPicker } from './elementPicker';

export class Migrate {
  elementPicker = new ElementPicker();

  public migrateRow = (wrapper: HTMLElement): void => {
    const excludedWrapper = this.excludeOuterRowColumn(wrapper);
    if (excludedWrapper) {
      wrapper = excludedWrapper;
    }
    const rows = wrapper.getElementsByClassName('row');
    for (let i = 0; i < rows.length; i++) {
      const row = rows[i];
      const nestedRow = this.isNestedRow(row as HTMLMapElement);
      if (
        (!nestedRow && !this.isClassContains(row, CONSTANTS.LOREE_IFRAME_CONTENT_MENU)) ||
        this.isClassContains(row, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)
      ) {
        this.addClass(row, CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
      } else if (nestedRow && !this.isContainer(row as HTMLElement)) {
        this.addClass(row, CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
        row.outerHTML = this.wrapRowWithContainer(row as HTMLElement);
      }
    }
    this.migrateRowWithCustomStyles(wrapper);
  };

  public wrapRowWithContainer = (rowElement: HTMLElement) => {
    const containerWrapper = document.createElement('div');
    containerWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER;
    containerWrapper.style.cssText = `display:flex; margin:0px; padding: ${
      rowElement.style?.width === '100%' ? '5px' : '0'
    }`;
    const container = document.createElement('div');
    container.className = CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER;
    container.style.cssText = `width: ${rowElement.style?.width};`;
    rowElement.style.width = '100%';
    if (rowElement.style.marginLeft === 'auto' && rowElement.style.marginRight === 'auto') {
      container.style.margin = '0px auto';
    }
    container.innerHTML = rowElement.outerHTML;
    containerWrapper.innerHTML = container.outerHTML;
    return containerWrapper.outerHTML;
  };

  public isContainer = (element: HTMLElement): boolean => {
    return Boolean(
      element?.parentElement?.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER),
    );
  };

  public isNestedRow = (row: HTMLElement): boolean => {
    const dummyRow = row;
    if (dummyRow.parentElement) {
      const dummyRowParent = dummyRow.parentElement;
      if (
        !dummyRowParent.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) &&
        dummyRowParent.id !== 'custom-block'
      ) {
        return false;
      }
      return true;
    }
    return false;
  };

  public excludeOuterRowColumn = (element: HTMLElement): HTMLElement => {
    for (let i = 0; i < element.children.length; i++) {
      const wrapper = element.children[i];
      if (this.isClassContains(wrapper, CONSTANTS.LOREE_IFRAME_CONTENT_ROW)) {
        return wrapper.children[0].children[0] as HTMLElement;
      }
    }
    return element;
  };

  public migrateBoldTag = (wrapper: HTMLElement): void => {
    const boldTags = wrapper.querySelectorAll('b, strong');
    Array.from(boldTags).forEach((element) => {
      if (element.parentElement?.tagName === 'DIV') {
        const paragraphTag = document.createElement('p');
        this.addClass(paragraphTag, 'bStyle');
        paragraphTag.innerHTML = element.innerHTML;
        paragraphTag.style.cssText = `font-weight: bold`;
        (element.parentNode as Node).replaceChild(paragraphTag, element);
      } else {
        const spanTag = document.createElement('span');
        this.addClass(spanTag, 'bStyle');
        spanTag.innerHTML = element.innerHTML;
        spanTag.style.cssText = `font-weight: bold`;
        (element.parentNode as Node).replaceChild(spanTag, element);
      }
    });
  };

  // fixImageWithLinks = (imgElement: HTMLImageElement) => {
  //   const linkTag = imgElement.parentElement;
  //   if (!linkTag) {
  //     return;
  //   }

  //   if (
  //     linkTag.parentElement &&
  //     linkTag.parentElement.tagName === 'DIV' &&
  //     linkTag.parentElement.classList.includes('ANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER')
  //   ) {
  //   }
  // };

  public migrateIncorrectImageWrappersWithLinks = (wrapper: HTMLElement) => {
    const imageWrappers = wrapper.querySelectorAll(
      `div.${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}`,
    );

    imageWrappers.forEach((imgWrapper) => {
      if (imgWrapper.parentElement?.tagName === 'A') {
        const anchorElement = imgWrapper.parentElement;
        const anchorParent = imgWrapper.parentElement.parentElement;
        if (anchorParent) {
          anchorParent.appendChild(imgWrapper);
          wrapChildElements(imgWrapper, anchorElement);
        }
      }
    });
  };

  public migrateImage = (wrapper: HTMLElement): void => {
    const images = wrapper.querySelectorAll('img');
    images.forEach((imgElement) => {
      if (elementHasClass(imgElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE)) {
        const parentElement = imgElement.parentElement;
        // if (!parentElement) return;
        if (
          parentElement &&
          parentElement.tagName === 'DIV' &&
          elementHasClass(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER)
        ) {
          parentElement.style.removeProperty('width');
        }
        return;
      }
      addClassToElement(imgElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE);

      const parentElement = imgElement.parentElement;
      if (!parentElement) return;
      if (parentElement.tagName !== 'DIV' && parentElement.tagName !== 'A') {
        const newImageWrapper = document.createElement('div');
        newImageWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
        this.setStyleForMigratedImageWrapper(newImageWrapper, imgElement);
        wrapChildElements(parentElement, newImageWrapper);
        // newImageWrapper.innerHTML = element.outerHTML;
        // if (parentTagName.includes(parentElement.tagName)) {
        //   parentElement.insertBefore(newImageWrapper, element);
        // } else {
        //   parentElement.after(newImageWrapper, element);
        // }
        // element.remove();
        return;
      }
      if (parentElement.tagName === 'A') {
        // we have one case where a linked image is coming into Loree for the first time.
        //  We need to wrap it correctly now
        if (parentElement.parentElement) {
          const newImageWrapper = document.createElement('div');
          newImageWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
          this.setStyleForMigratedImageWrapper(newImageWrapper, imgElement);
          wrapChildElements(parentElement.parentElement, newImageWrapper);
        }
        return;
      }
      this.addClass(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER);
      this.setStyleForMigratedImageWrapper(parentElement, imgElement);
      this.addClass(imgElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE);
      imgElement.style.padding = '';
      imgElement.style.margin = '';
      imgElement.style.display = '';
      imgElement.style.height = 'auto';
    });
  };

  public setStyleForMigratedImageWrapper = (
    imageWrapper: HTMLElement,
    element: HTMLElement,
  ): HTMLElement => {
    const styleConstant = `
    ${element.style.margin ? 'margin:' + element.style.margin + ';' : ''}
    ${element.style.padding ? 'padding:' + element.style.padding + ';' : ''}
    ${
      element.style.backgroundColor ? 'background-color:' + element.style.backgroundColor + ';' : ''
    }
    ${element.style.textAlign ? 'text-align:' + element.style.textAlign + ';' : ''}`;
    imageWrapper.setAttribute('style', styleConstant);
    switch (true) {
      case imageWrapper.style.marginLeft === 'auto' && imageWrapper.style.marginRight === 'auto':
        imageWrapper.style.textAlign = 'center';
        break;
      case imageWrapper.style.marginLeft === 'auto':
        imageWrapper.style.textAlign = 'right';
        break;
      case imageWrapper.style.marginRight === 'auto':
        imageWrapper.style.textAlign = 'left';
        break;
    }
    return imageWrapper;
  };

  public migrateAnchorTag = (wrapper: HTMLElement): void => {
    const anchorTags = wrapper.getElementsByTagName('a');
    Array.from(anchorTags).forEach((element: HTMLElement) => {
      element.setAttribute('onclick', 'event.preventDefault()');
      this.handleAnchorScenario(element);
    });
  };

  public handleAnchorScenario = (anchorElement: HTMLElement): void => {
    let innerText = '';
    switch (anchorElement.parentElement?.tagName) {
      case 'SUP':
        anchorElement.innerHTML = `<span style='vertical-align: super;' class='loree-iframe-content-element element-highlight'>${anchorElement.innerHTML}</span>`;
        innerText = anchorElement.outerHTML;
        break;
      case 'SUB':
        anchorElement.innerHTML = `<span style='vertical-align: sub;' class='loree-iframe-content-element element-highlight'>${anchorElement.innerHTML}</span>`;
        innerText = anchorElement.outerHTML;
        break;
      case 'SPAN':
      case 'EM':
      case 'P':
      case 'LI':
      case 'TD':
      case 'H1':
      case 'H2':
      case 'H3':
      case 'H4':
      case 'H5':
      case 'H6':
        innerText = anchorElement.outerHTML;
        break;
      case 'DIV':
        innerText = this.migrateDivTextWithAnchor(anchorElement);
    }

    if (innerText.length) {
      anchorElement.outerHTML = innerText;
    }

    if (anchorElement) {
      Array.from(anchorElement?.children).forEach((childElement) => {
        if (!childElement.isEqualNode(anchorElement)) return;
        childElement.replaceWith(anchorElement);
      });
    }
  };

  private readonly migrateDivTextWithAnchor = (anchorElement: HTMLElement) => {
    const firstChild = anchorElement.children[0];
    if (
      anchorElement.childNodes[0].nodeType === anchorElement.TEXT_NODE ||
      (!firstChild.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) &&
        firstChild.tagName !== 'IMG')
    ) {
      const paragraphTag = document.createElement('p');
      paragraphTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
      paragraphTag.innerHTML = anchorElement.outerHTML;
      return paragraphTag.outerHTML;
    } else {
      return anchorElement.outerHTML;
    }
  };

  public migrateVideo = (wrapper: HTMLElement): void => {
    const videoElements = wrapper.getElementsByTagName('video');
    Array.from(videoElements)
      .filter((videoElement) => !this.isClassContains(videoElement, CONSTANTS.LOREE_NO_POINTER))
      .forEach((videoElement: HTMLElement) => {
        this.wrapVideoElement(videoElement);
      });
  };

  public migrateVideoIframe = (wrapper: HTMLElement): void => {
    const acceptedVideoLinks = ['vimeo', 'youtu.be', 'youtube'];
    const iframeElements = wrapper.getElementsByTagName('iframe');
    Array.from(iframeElements)
      .filter(
        (iframeElement) =>
          !this.isClassContains(iframeElement, CONSTANTS.LOREE_NO_POINTER) &&
          acceptedVideoLinks.some((link) => iframeElement.src?.includes(link)),
      )
      .forEach((iframeElement) => {
        this.wrapVideoElement(iframeElement);
      });
  };

  public migrateIframeTags = (wrapper: HTMLElement): void => {
    const iframeElements = wrapper.getElementsByTagName('iframe');
    Array.from(iframeElements).forEach((iframeElement) => {
      iframeElement.style.maxWidth = '100%';
    });
  };

  public wrapVideoElement = (videoElement: HTMLElement): void => {
    const parentElement = videoElement.parentElement as HTMLElement;
    parentElement.setAttribute(
      'style',
      `${videoElement.style.float ? 'text-align:' + videoElement.style.float + ';' : ''}`,
    );
    switch (true) {
      case videoElement.style.marginLeft === 'auto' && videoElement.style.marginRight === 'auto':
        parentElement.setAttribute('style', 'text-align:center');
        break;
      case videoElement.style.marginLeft === 'auto':
        parentElement.setAttribute('style', 'text-align:right');
        break;
      case videoElement.style.marginRight === 'auto':
        parentElement.setAttribute('style', 'text-align:left');
        break;
    }
    const contentWrapper = document.createElement('div');
    contentWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO;
    contentWrapper.setAttribute(
      'style',
      `position:relative;width: ${
        videoElement.style.width === 'auto'
          ? '100%'
          : videoElement.style.width || (videoElement as HTMLIFrameElement).width
      };display: inline-flex;max-width: 100%;max-height: 100%;${
        videoElement.style.borderWidth ? 'border-width:' + videoElement.style.borderWidth + ';' : ''
      }${
        videoElement.style.borderStyle ? 'border-style:' + videoElement.style.borderStyle + ';' : ''
      }${
        videoElement.style.borderColor ? 'border-color:' + videoElement.style.borderColor + ';' : ''
      }`,
    );

    const responsiveWrapper = document.createElement('div');
    responsiveWrapper.setAttribute('style', 'padding: 42% 0px 0px;display: flex;');

    this.addClass(videoElement, CONSTANTS.LOREE_NO_POINTER);
    videoElement.setAttribute(
      'style',
      'position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width:100%;',
    );
    responsiveWrapper.innerHTML = videoElement.outerHTML;
    contentWrapper.appendChild(responsiveWrapper);
    let wrapper = parentElement;
    if (
      !parentElement?.hasAttribute('class') ||
      this.isClassContains(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)
    ) {
      const videoWrapper = document.createElement('div');
      videoWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER;
      videoWrapper.setAttribute('style', 'padding: 5px;margin:0 0 10px;');
      parentElement?.insertBefore(videoWrapper, videoElement);
      videoWrapper.appendChild(contentWrapper);
      wrapper = videoWrapper;
    } else {
      this.addClass(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER);
      parentElement?.replaceWith(parentElement);
      parentElement.prepend(contentWrapper);
    }
    if (videoElement.style.marginLeft === 'auto' && videoElement.style.marginRight === 'auto') {
      wrapper.style.textAlign = 'center';
    }
    videoElement.remove();
  };

  private readonly wrapIframeVideoElement = (iframeVideoElement: HTMLElement) => {
    const parentElement = iframeVideoElement.parentElement as HTMLElement;
    if (
      !parentElement?.hasAttribute('class') ||
      this.isClassContains(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)
    ) {
      const iframeVideoWrapper = document.createElement('div');
      iframeVideoWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER;
      iframeVideoWrapper.setAttribute(
        'style',
        'padding: 10px;margin-bottom: 20px;max-width: 100%;max-height: 100%;',
      );
      parentElement?.insertBefore(iframeVideoWrapper, iframeVideoElement);
      iframeVideoWrapper.appendChild(iframeVideoElement);
    } else {
      this.addClass(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER);
    }
  };

  public migrateTable = (wrapper: HTMLElement): void => {
    const tableDataTags = wrapper.getElementsByTagName('td');
    this.wrapTableContent(tableDataTags);
    const tableHeaderTags = wrapper.getElementsByTagName('th');
    this.wrapTableContent(tableHeaderTags);
    const tableTags = wrapper.getElementsByTagName('table');
    Array.from(tableTags).forEach((element) => {
      if (!element || this.isClassContains(element, CONSTANTS.LOREE_IFRAME_CONTENT_TABLE)) return;
      const parentElement = element.parentElement as HTMLElement;
      element.style.float = 'none';
      element.style.width = '100%';
      element.style.height = 'auto';
      element.style.border = '1px solid #a5a5a5';
      if (
        !this.isClassContains(element, CONSTANTS.LOREE_IFRAME_CONTENT_TABLE) &&
        !this.isClassContains(parentElement, 'table-responsive') &&
        element.tagName !== 'DIV'
      ) {
        const tableWrapper = document.createElement('div');
        tableWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER;
        tableWrapper.style.cssText = `display:flex;width: 100%;height:auto;overflow-x:auto;margin:0 0 10px;border-width:0;`;
        this.addClass(element, CONSTANTS.LOREE_IFRAME_CONTENT_TABLE);
        parentElement?.insertBefore(tableWrapper, element);
        tableWrapper.innerHTML = element.outerHTML;
        element.remove();
      } else {
        this.addStyleClassToTable(element, parentElement);
      }
    });
  };

  private readonly addStyleClassToTable = (element: HTMLElement, parentElement: HTMLElement) => {
    this.addClass(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER);
    element.style.width = parentElement.style.width;
    if (parentElement.style.marginLeft === 'auto' && parentElement.style.marginRight === 'auto') {
      element.style.margin = '0px auto';
    }
    parentElement.style.cssText = `display:flex;width:100%;height:auto;overflow-x:auto;padding:${parentElement.style.padding}`;
    this.addClass(element, CONSTANTS.LOREE_IFRAME_CONTENT_TABLE);
  };

  private readonly wrapTableContent = (
    tableElements: HTMLCollectionOf<HTMLTableDataCellElement>,
  ): void => {
    for (const element of tableElements) {
      if (element.childNodes.length > 0 && this.checkTextTags(element.childNodes)) {
        const tableDataWrapper = document.createElement('p');
        tableDataWrapper.style.padding = '5px';
        tableDataWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
        tableDataWrapper.innerHTML = element.innerHTML;
        element.innerHTML = tableDataWrapper.outerHTML;
      }
    }
  };

  private readonly checkTextTags = (children: NodeListOf<ChildNode>): boolean => {
    for (const child of children) {
      if (child.nodeName !== '#text' && child.nodeName !== 'SPAN') return false;
    }
    return true;
  };

  public migrateIconText = (wrapper: HTMLElement) => {
    const iconTextElements = wrapper.querySelectorAll('[id^="col_row_2_table"]');
    const iconTextLoreeElements = wrapper.querySelectorAll(
      `[id^=${CONSTANTS.LOREE_IFRAME_CONTENT_ICON}]`,
    );
    this.migrateV1SpecialElements(iconTextElements);
    this.migrateExistingV2SpecialElements(iconTextLoreeElements);
  };

  public migrateV1SpecialElements = (iconTextElements: NodeListOf<Element>) => {
    Array.from(iconTextElements).forEach((iconTextElement) => {
      iconTextElement.className = `${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT} row`;
      iconTextElement.removeAttribute('id');
      const childElemenets = iconTextElement.getElementsByTagName('*');
      Array.from(childElemenets).forEach((childElement) => {
        this.addClass(childElement, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT);
        if ((childElement as HTMLElement).id) childElement.removeAttribute('id');
      });
    });
  };

  public migrateExistingV2SpecialElements = (iconTextLoreeElements: NodeListOf<Element>) => {
    Array.from(iconTextLoreeElements).forEach((iconTextLoreeElement) => {
      this.addClass(iconTextLoreeElement, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT);
      iconTextLoreeElement.removeAttribute('id');
    });
  };

  public removeContainerWrapperForExistingElement = (wrapper: HTMLElement) => {
    const iconTextElements = wrapper.querySelectorAll(
      `.${CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT}`,
    );
    iconTextElements.forEach((element) => {
      if (
        this.isClassContains(
          element.parentNode as HTMLElement,
          CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER,
        )
      ) {
        const currentParentElement = this.elementPicker.getElement(
          element.parentElement as HTMLElement,
          'removeContainerWrapperForExistingElement',
        );
        currentParentElement.replaceWith(element);
      }
    });
  };

  public migrateRowWithCustomStyles = (wrapper: HTMLElement): void => {
    const rows = wrapper.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_ROW);
    Array.from(rows).forEach((row) => {
      if (!row.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)) {
        const rowElement = row as HTMLElement;
        const isWrap =
          rowElement.style?.width &&
          rowElement.style.width !== '100%' &&
          !rowElement.parentElement?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER);
        if (isWrap) {
          const rowWrapper = document.createElement('div');
          rowWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_ROW + ' row';
          rowWrapper.style.cssText = 'position:relative; margin: 0;';
          const columnElement = document.createElement('div');
          columnElement.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN + ' col-12';
          columnElement.innerHTML = this.wrapRowWithContainer(rowElement);
          rowWrapper.appendChild(columnElement);
          row.outerHTML = rowWrapper.outerHTML;
        }
      }
    });
  };

  public migrateColumn = (wrapper: HTMLElement): void => {
    const columns = wrapper.querySelectorAll('[class^="col-"]');
    Array.from(columns).forEach((column) => {
      if (this.isOneColumnWithHeader(column)) {
        this.migrateOneColumnWithHeader(column);
      }
      if (this.isMutipleColumnWithHeader(column)) {
        this.migrateMultipleColumnWithHeader(column);
      }
      if (
        !this.isClassContains(column, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) &&
        !this.isClassContains(column, CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)
      ) {
        this.addClass(column, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
      }
    });
    this.addRowForColumnsWithoutRow(wrapper);
  };

  private readonly addRowForColumnsWithoutRow = (wrapper: HTMLElement) => {
    const columns = wrapper.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
    Array.from(columns).forEach((column) => {
      let colParent = column.parentElement;
      while (colParent && !this.isClassContains(colParent, 'row')) {
        if (
          this.isClassContains(colParent, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) ||
          !colParent.parentElement
        ) {
          this.addClass(column.parentElement as HTMLElement, 'row');
          return;
        }
        colParent = colParent.parentElement;
      }
    });
  };

  private readonly isClassContains = (element: Element, className: string): boolean => {
    return element?.classList?.contains(className);
  };

  private readonly addClass = (element: Element, className: string) => {
    element?.classList.add(className);
  };

  private readonly isOneColumnWithHeader = (column: Element): boolean => {
    return (
      column.classList.length === 1 &&
      this.isClassContains(column, 'col-12') &&
      column.className === column.nextElementSibling?.className
    );
  };

  private readonly isMutipleColumnWithHeader = (column: Element) => {
    const multipleColClasses = ['col-md-6', 'col-lg-6', 'col-lg-4', 'col-lg-3'];
    return (
      column.classList.length > 1 &&
      (column as HTMLElement).style.display === 'table-cell' &&
      isClassExist(Array.from(column.classList), multipleColClasses)
    );
  };

  private readonly migrateOneColumnWithHeader = (column: Element) => {
    const colSibling = column.nextElementSibling;
    const docFrag = document.createDocumentFragment();
    while (colSibling?.firstChild) {
      const child = colSibling?.removeChild(colSibling.firstChild);
      docFrag.appendChild(child);
    }
    column?.appendChild(docFrag);
    colSibling?.remove();
  };

  private readonly migrateMultipleColumnWithHeader = (column: Element) => {
    const colChild = column.firstChild;
    const docFrag = document.createDocumentFragment();
    while (colChild?.firstChild) {
      const range = document.createRange();
      const fragment = range.createContextualFragment(
        (colChild.firstChild as HTMLElement).innerHTML,
      );
      docFrag.appendChild(fragment);
      colChild?.removeChild(colChild.firstChild);
    }
    (column.firstChild as HTMLElement).remove();
    column?.appendChild(docFrag);
  };

  public migrateNavigationContent = (wrapper: HTMLElement): void => {
    const navigationSection = wrapper.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_MENU);
    if (navigationSection) {
      Array.from(navigationSection)
        .filter((element) => element.classList.contains('row'))
        .forEach((navigationElement) => {
          const navigationRowElement = navigationElement as HTMLElement;
          if (navigationRowElement.style.display !== 'flex') {
            navigationRowElement.style.display = 'flex';
          }
        });
    }
  };

  public migrateDivText = (wrapper: HTMLElement): void => {
    const divTags = wrapper.getElementsByTagName('div');
    let i;
    let j;
    for (i = 0; i < Array.from(divTags).length; i++) {
      for (j = 0; j < divTags[i].childNodes.length; j++) {
        if (
          divTags[i].childNodes[j].nodeType === 3 &&
          divTags[i].childNodes[j].nodeValue?.trim() !== ''
        ) {
          const paragraphTag = document.createElement('p');
          paragraphTag.style.cssText = `margin: 0`;
          const siblingsList = this.checkSiblingElementToConcat(divTags[i].childNodes[j]);
          divTags[i].replaceChild(paragraphTag, divTags[i]?.childNodes[j]);
          siblingsList.forEach((nextEle) => {
            paragraphTag.appendChild(nextEle);
          });
        }
      }
    }
  };

  private readonly checkSiblingElementToConcat = (currentElement: Node) => {
    const inlineElements = ['A', 'SPAN'];
    const arr = [currentElement];
    let nextElement = currentElement.nextSibling;
    while (nextElement && inlineElements.includes((nextElement as HTMLElement).tagName)) {
      arr.push(nextElement);
      if (!nextElement.nextSibling) {
        return arr;
      }
      nextElement = nextElement.nextSibling;
    }
    return arr;
  };

  public migrateElementsWithoutRowColumn = (html: string) => {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    const elements = wrapper.querySelectorAll(
      ':not(.loree-iframe-content-column):not(.loree-iframe-content-row)',
    );
    Array.from(elements).forEach((element) => {
      let parentElement = element.parentElement as HTMLElement;
      while (
        !this.isClassContains(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) &&
        parentElement?.parentElement
      ) {
        parentElement = parentElement?.parentElement;
      }
      if (!this.isClassContains(parentElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) {
        const siblingList = this.trackSiblings(element);
        this.wrapIndividualRowColumn(element, siblingList);
      }
    });
    return wrapper.innerHTML;
  };

  private readonly trackSiblings = (element: Element): Array<HTMLElement> => {
    const siblingList: Array<HTMLElement> = [];
    let siblingElement = element.nextSibling as HTMLElement;
    while (
      siblingElement &&
      !this.isClassContains(siblingElement, CONSTANTS.LOREE_IFRAME_CONTENT_ROW) &&
      !this.isClassContains(siblingElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)
    ) {
      siblingList.push(siblingElement);
      if (!siblingElement.nextSibling) return siblingList;
      siblingElement = siblingElement.nextSibling as HTMLElement;
    }
    return siblingList;
  };

  private readonly wrapIndividualRowColumn = (
    element: Element,
    siblingList: Array<HTMLElement>,
  ) => {
    const rowElement = document.createElement('div');
    rowElement.className = `row ${CONSTANTS.LOREE_IFRAME_CONTENT_ROW}`;
    rowElement.setAttribute('style', 'position:relative; margin: 0px; padding: 10px');
    const columnElement = document.createElement('div');
    columnElement.className = `col-12 ${CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN}`;
    columnElement.setAttribute('style', 'padding: 10px');
    if (element) element.parentElement?.replaceChild(rowElement, element);
    columnElement.appendChild(element);
    Array.from(siblingList).forEach((sibling) => {
      columnElement.appendChild(sibling);
    });
    rowElement.appendChild(columnElement);
  };
}
