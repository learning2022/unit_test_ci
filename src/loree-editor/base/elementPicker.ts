import CONSTANTS from '../constant';

export class ElementPicker {
  excludedListTags: string[] = ['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'OL', 'UL', 'CAPTION'];
  targetParentTag: string[] = [
    'P',
    'H1',
    'H2',
    'H3',
    'H4',
    'H5',
    'H6',
    'OL',
    'UL',
    'LI',
    'CAPTION',
  ];

  getElement = (element: HTMLElement, key: string): HTMLElement => {
    while (this.validateElement(element, key)) {
      element = element.parentElement as HTMLElement;
    }
    return element;
  };

  validateElement = (element: HTMLElement, key: string): boolean => {
    if (!element) return false;
    let isValid = false;
    switch (key) {
      case 'list':
      case 'wordSpacing':
      case 'spacingText':
      case 'tripleClick':
        isValid = this.checkTagName(element);
        break;
      case 'tripleClickEventListener':
        isValid =
          element?.tagName !== 'DIV' && element?.tagName !== 'LI' && this.checkTagName(element);
        break;
      case 'selectMenuEvent':
      case 'identifySourceBlock':
        isValid = element?.tagName !== 'DIV';
        break;
      case 'AnchorLinkPopper':
      case 'anchorNode':
      case 'urlLinkPopperEvents':
      case 'initiateAnchorLinkPopper':
        isValid = element?.tagName !== 'A';
        break;
      case 'majorParent':
      case 'singleLineItem':
      case 'overAllSelection':
        isValid = element?.tagName !== 'DIV' && this.checkTagName(element);
        break;
      case 'blockElement':
        isValid = element?.tagName !== 'DIV' && this.checkParentTag(element);
        break;
      case 'LI':
        isValid = element?.tagName !== 'LI';
        break;
      case 'isList':
        isValid = element?.tagName !== 'P' && element.tagName !== 'LI' && element.tagName !== 'DIV';
        break;
      case 'paragraph':
        isValid = element?.tagName === 'SPAN';
        break;
      case 'handleClickWithShift':
        isValid = element?.tagName !== 'TBODY';
        break;
      case 'handleSelectedTableEvent':
      case 'handleSelectedTableDataEvent':
        isValid = element?.tagName !== 'TABLE';
        break;
      case 'attachNewElement':
        isValid =
          element?.tagName !== 'DIV' &&
          element?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE);
        break;
      case 'selectPreviousStyle':
      case 'handleSelectedElementEvent':
        isValid = this.checkParentTag(element);
        break;
      case 'handleClickEvent':
        isValid = element?.tagName !== 'DIV' && this.checkTagName(element);
        break;
      case 'wrapInvalidTags':
        isValid = !element?.parentElement?.classList?.contains(
          CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN,
        );
        break;
      case 'removeContainerWrapperForExistingElement':
        isValid = !element.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER);
        break;
    }
    if (
      element?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) &&
      element?.getAttribute('id') === CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER
    )
      return false;
    return isValid;
  };

  checkTagName(wrapper: HTMLElement) {
    return !this.excludedListTags.includes(wrapper?.tagName);
  }

  checkParentTag(wrapper: HTMLElement) {
    return !this.targetParentTag.includes(wrapper?.tagName);
  }
}
