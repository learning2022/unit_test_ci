import { checkExistingTextInline } from './base-text';

describe('base text element changes', () => {
  it('base text', () => {
    const selection = {
      anchorNode: {
        nodeName: '#notthetext',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
        },
        parentElement: {} as HTMLElement,
      } as HTMLElement,
    } as unknown as Selection;
    global.getComputedStyle = jest.fn().mockImplementation((thing) => ({}));
    const result = checkExistingTextInline(selection);
    expect(result[0]).toBe(true);
    expect(result[9]).toBe(false);
  });

  it('base text - parent node', () => {
    const selection = {
      anchorNode: {
        nodeName: '#text',
        parentElement: {
          style: {
            fontWeight: 'bold',
            fontStyle: 'italic',
          },
        } as HTMLElement,
      } as HTMLElement,
    } as unknown as Selection;
    global.getComputedStyle = jest.fn().mockImplementation((thing) => ({}));
    const result = checkExistingTextInline(selection);
    expect(result[0]).toBe(true);
    expect(result[9]).toBe(false);
  });
});
