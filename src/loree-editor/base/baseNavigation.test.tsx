import Base from '../base';
import CONSTANTS from '../constant';

describe('Navigation menu enhancement', () => {
  const baseInstance = new Base();

  beforeEach(() => {
    const iframeElement = document.createElement('iframe');
    const hamburgerIcon = document.createElement('div');
    const dummyInput1 = document.createElement('div');
    dummyInput1.id = 'dummyId1';
    dummyInput1.className = 'row';
    iframeElement.id = CONSTANTS.LOREE_IFRAME;
    hamburgerIcon.className = 'loree-navigation-hamburger-icon';
    hamburgerIcon.id = CONSTANTS.LOREE_NAVIGATE_HAMBURGER_ICON;
    hamburgerIcon.setAttribute('style', 'display: none');
    hamburgerIcon.innerHTML = `Icon`;
    iframeElement.appendChild(hamburgerIcon);
    document.body.appendChild(iframeElement);
    dummyInput1.innerHTML = `
      <div class=${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} style="padding: 0px; color: #000d9c;">
        <p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} style="padding: 13px 10px; margin: 0px; font-weight: bold;">Menu Item</p>
      </div>
      `;
    document.body.appendChild(dummyInput1);
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(
        () => document.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_MENU)[0],
      );
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
    baseInstance.showBlockOptions = jest.fn().mockImplementation(() => '');
    baseInstance.validateWindowSelection = jest.fn().mockImplementation(() => true);
  });
  afterEach(() => {
    document.body.innerHTML = '';
  });
  test('Clicking in Menu item without row div', () => {
    baseInstance.handleSelectedMenuEvent();
    expect(document.getElementById(CONSTANTS.LOREE_NAVIGATE_HAMBURGER_ICON)).toHaveStyle(
      'display: inline-flex',
    );
  });
});
