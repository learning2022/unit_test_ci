/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
export const checkExistingTextInline = (selected: Selection): boolean[] | [] => {
  if (selected.anchorNode === null || selected.anchorNode === undefined) {
    return [];
  }

  let targetNode = selected.anchorNode as HTMLElement;
  const targetNodeParent = selected.anchorNode.parentElement as HTMLElement;

  if (targetNode.nodeName === '#text') {
    targetNode = targetNodeParent;
  }

  const elementStyle = window.getComputedStyle(targetNode);

  return [
    targetNode.style.fontWeight === 'bold',
    targetNode.style.fontStyle === 'italic',
    targetNode.style.textTransform === 'uppercase',
    targetNode.style.textTransform === 'lowercase',
    targetNode.style.textTransform === 'capitalize',
    targetNode.style.textDecoration === 'underline',
    targetNode.style.textDecoration === 'line-through',
    targetNode.style.verticalAlign === 'sub',
    targetNode.style.verticalAlign === 'super',
    elementStyle.textAlign === 'center',
    elementStyle.textAlign === 'justify',
    elementStyle.textAlign === 'left',
    elementStyle.textAlign === 'right',
    elementStyle.lineHeight !== '0px',
    elementStyle.wordSpacing !== '0px',
  ];
};

export const selectFontFamilyOptions = (textOptions: any, fontFamily: string): string => {
  const FontFamilys = fontFamily.split(',');
  const element = textOptions.children[3];
  let selectedFontFamily = '';
  if (element) {
    for (let f = 0; f < FontFamilys.length; f++) {
      if (selectedFontFamily.length > 1) {
        break;
      }
      for (let i = 0; i < element.options.length; i++) {
        const optionValue = element.options[i].value;
        const fontValue = FontFamilys[f].trim();
        if (optionValue.match(fontValue)) {
          selectedFontFamily = fontValue;
          element.options[i].selected = true;
        } else {
          element.options[i].selected = false;
        }
      }
    }
  }
  return selectedFontFamily;
};
