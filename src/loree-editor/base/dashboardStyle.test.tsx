import DashboardStyle from './dashboardStyles';
import CONSTANTS from '../constant';
import Base from '../base';
import { textOptionIcon } from '../modules/textOptions/UI/icons';
import { specialElement } from '../elements/specialElement';
import { tableElement } from '../elements/tableElement';

describe('Navigation menu filter issue', () => {
  let dummyInput2: HTMLElement;
  let defaultStyles: any;
  const dashboardStyleInstance = new DashboardStyle();
  const iframe = document.createElement('iframe');

  function loreeWrapper() {
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    iframe.appendChild(wrapper);
    const iframeHTML = `<div id = ${CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON}></div><div id = ${CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON}></div><div id ='${CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON}'></div>`;
    iframe.innerHTML += iframeHTML;
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }

  beforeEach(() => {
    document.body.innerHTML = '';
    loreeWrapper();
    dummyInput2 = document.createElement('p');
    dummyInput2.className =
      CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS + ' ' + CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    dummyInput2.setAttribute(
      'style',
      "font-size: 22px; font-family: 'arial'; padding: 12px 20px; margin: 0px;",
    );
    dummyInput2.innerHTML = 'Menu Item';
    defaultStyles = {
      size: '20px',
      font: 'Open Sans',
    };
    document.body.append(dummyInput2);

    const parentWrapper = document.createElement('div') as HTMLElement;
    parentWrapper.id = CONSTANTS.LOREE_IFRAME;
    const orderedList = document.createElement('button');
    orderedList.id = CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON;
    const unorderedList = document.createElement('button');
    unorderedList.id = CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON;
    parentWrapper.append(orderedList);
    parentWrapper.append(unorderedList);
  });
  test('Clear format for navigation menu', () => {
    const clearFormatOutput = `<p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} loree-iframe-content-element" style="font-size: 20px; font-family: Open Sans; padding: 12px 20px; margin: 0px;">Menu Item</p>`;
    dashboardStyleInstance.setElementStyles(dummyInput2, defaultStyles);
    expect(
      document.getElementsByClassName(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)[0]?.outerHTML,
    ).toEqual(clearFormatOutput);
  });
  test('Mini Menu selection', () => {
    const baseInstance = new Base();
    baseInstance.modifyConversionTextPopupIcons('none');
    expect(
      baseInstance.getDocument()!.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON),
    ).toHaveStyle('display: none');
    expect(
      baseInstance.getDocument()!.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON),
    ).toHaveStyle('display: none');
    expect(
      baseInstance.getDocument()!.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON),
    ).toHaveStyle('display: none');
  });
});

describe('Navigation Menu selection', () => {
  const baseInstance = new Base();
  let selectedElement: HTMLElement;
  beforeAll(() => {
    document.body.innerHTML = specialElement.contents[1].innerContent[1].content;
  });
  test.each([CONSTANTS.LOREE_IFRAME_CONTENT_MENU, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS])(
    'Selecting Navigation menu',
    (position) => {
      selectedElement = document.getElementsByClassName(position)[0] as HTMLElement;
      baseInstance.getSelectedElement = jest.fn().mockImplementation(() => selectedElement);
      baseInstance.changeSelectedElement(selectedElement);
      expect(document.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_MENU)[0]).toHaveClass(
        CONSTANTS.LOREE_ELEMENT_HIGHLIGHT,
      );
    },
  );
});

describe('Table Caption styles', () => {
  const dashboardStyleInstance = new DashboardStyle();
  const baseInstance = new Base();
  let dummyInput: HTMLElement;
  let defaultStyles: _Any;
  let button: HTMLElement;
  beforeEach(() => {
    jest.clearAllMocks();
    document.body.innerHTML = tableElement.contents[1].content;
    defaultStyles = {
      size: '20px',
      font: 'Open Sans',
    };
    button = document.createElement('button');
    button.className = 'text-option-button editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', 'Unordered List');
    button.setAttribute('data-placement', 'bottom');
    button.innerHTML = textOptionIcon.orderedTextIcon;
    button.id = CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON;
    baseInstance.validateWindowSelection = jest.fn().mockImplementation(() => true);
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
  });
  test('Caption clear format', () => {
    const expectedResult =
      '<caption class="loree-iframe-content-element loree-table-caption-element" style="caption-side: bottom; border-width: 0px; border-style: solid; border-color: #000000; padding: 5px; margin: 0px; font-size: 20px; font-family: Source Sans Pro;">Insert caption here</caption>';
    dummyInput = document.getElementsByClassName(
      CONSTANTS.LOREE_TABLE_CAPTION_ELEMENT,
    )[0] as HTMLElement;
    dashboardStyleInstance.setElementStyles(dummyInput, defaultStyles);
    expect(dummyInput.outerHTML).toEqual(expectedResult);
  });
});
