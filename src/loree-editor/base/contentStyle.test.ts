/* eslint-disable */ // Remove this line when editing this file
import { checkExistingTextInline, selectFontFamilyOptions } from './contentStyle';

describe('base text element changes', () => {
  it('base text', () => {
    const selection = ({
      anchorNode: {
        nodeName: '#notthetext',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
        },
        parentElement: {} as HTMLElement,
      } as HTMLElement,
    } as unknown) as Selection;
    global.getComputedStyle = jest.fn().mockImplementation(thing => ({}));
    const result = checkExistingTextInline(selection);
    expect(result[0]).toBe(true);
    expect(result[9]).toBe(false);
  });

  it('base text - parent node', () => {
    const selection = ({
      anchorNode: {
        nodeName: '#text',
        parentElement: {
          style: {
            fontWeight: 'bold',
            fontStyle: 'italic',
          },
        } as HTMLElement,
      } as HTMLElement,
    } as unknown) as Selection;
    global.getComputedStyle = jest.fn().mockImplementation(thing => ({}));
    const result = checkExistingTextInline(selection);
    expect(result[0]).toBe(true);
    expect(result[9]).toBe(false);
  });

  it('base text - null', () => {
    const selection = ({
      anchorNode: null,
    } as unknown) as Selection;
    global.getComputedStyle = jest.fn().mockImplementation(thing => ({}));
    const result = checkExistingTextInline(selection);
    expect(result.length).toBe(0);
  });
});

describe('#FontFamilySelection', () => {
  describe('text option popper contains fontFamily input field', () => {
    let textOptions: any;
    beforeEach(() => {
      textOptions = {
        children: [
          { button: null },
          { button: null },
          { button: null },
          {
            options: [
              { value: 'Arial', selected: false },
              { value: 'Open Sans', selected: false },
              { value: 'Arial Bold', selected: false },
              { value: 'Arial Bold Italic', selected: false },
            ],
          },
        ],
      };
    });
    it('set font family of selected text in input field', () => {
      const result = selectFontFamilyOptions(textOptions, 'Arial Bold');
      expect(result).toBe('Arial Bold');
      expect(textOptions.children[3].options[2].selected).toBe(true);
      expect(textOptions.children[3].options[1].selected).toBe(false);
    });
    it('should set only one font family of selected text in input field if it contains more than one', () => {
      const result = selectFontFamilyOptions(textOptions, 'Open Sans, sans-serif');
      expect(result).toBe('Open Sans');
      expect(textOptions.children[3].options[0].selected).toBe(false);
      expect(textOptions.children[3].options[1].selected).toBe(true);
    });
    it('set font family as empty in input field if selected text doesn"t contains style', () => {
      const result = selectFontFamilyOptions(textOptions, '');
      expect(result).toBe('');
    });
    it('set the font family of selected text as empty if the style doesn"t match the values in the input field', () => {
      const result = selectFontFamilyOptions(textOptions, 'Helvetica');
      expect(result).toBe('');
      expect(textOptions.children[3].options[0].selected).toBe(false);
      expect(textOptions.children[3].options[1].selected).toBe(false);
    });
  });
  describe('text option popper doesn"t contains fontFamily input field', () => {
    let textOptions: any;
    beforeEach(() => {
      textOptions = {
        children: [],
      };
    });
    it('should return the font family as empty', () => {
      const result = selectFontFamilyOptions(textOptions, 'Arial');
      expect(result).toBe('');
    });
  });
});
