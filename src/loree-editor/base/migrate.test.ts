import { Migrate } from './migrate';
import CONSTANTS from '../constant';
import { queryByTestId } from '@testing-library/react';
import migrationMockData from './migrationMockData';
import { createDiv, createElement, getElementById, getElementsByClassName } from '../common/dom';
import { getElementByClassName } from '../utils';

const migrate = new Migrate();
describe('#Migrate container', () => {
  let domElement: HTMLElement;
  let elements: HTMLCollectionOf<Element> | HTMLElement[] = [];
  beforeAll(() => {
    domElement = document.createElement('div');
    domElement.innerHTML = `<div class="row"> <div class="column"> </div></div>
    <div class="row">
      <div class="row c6567" style="margin: 0px auto">
        <div class="col-12 col-md-6 col-lg-6 c6575">
          <p class="c6583">
            Insert Text Here
          </p>
        </div>
      </div>
    </div>`;
    elements = domElement.getElementsByClassName('row');
  });
  describe('check nested row exist', () => {
    test('when nested row exist', () => {
      const result = migrate.isNestedRow(elements[1] as HTMLElement);
      expect(result).toBe(false);
    });
    test('when nested row does not exist', () => {
      const result = migrate.isNestedRow(elements[0] as HTMLElement);
      expect(result).toEqual(false);
    });
  });

  describe('apply container for nested rows', () => {
    test('when nested depth level is one', () => {
      migrate.migrateRow(domElement);
      expect(String(domElement.outerHTML.match(CONSTANTS.LOREE_IFRAME_CONTENT_ROW))).toEqual(
        CONSTANTS.LOREE_IFRAME_CONTENT_ROW,
      );
    });
    test('when nested depth level is two', () => {
      migrate.migrateRow(domElement);
      expect(
        String(domElement.outerHTML.match(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER)),
      ).toBe('null');
      expect(domElement.outerHTML).toBe(migrationMockData.rowMigration);
    });
  });
  describe('migrating content from v1', () => {
    test('row content', () => {
      document.body.innerHTML = migrationMockData.containerMigration.rowMigration;
      const result = migrate.isNestedRow(getElementById('rowMigration'));
      expect(result).toBe(false);
    });
    test('nested row content', () => {
      document.body.innerHTML = migrationMockData.containerMigration.nestedRowMigration;
      const result = migrate.isNestedRow(getElementById('nestedRowMigration'));
      expect(result).toBe(false);
    });
  });
});

describe('#MigrateImage', () => {
  const validImageParent = ['td', 'th'];
  validImageParent.forEach((validParent) => {
    describe(`when immediate parent is ${validParent}`, () => {
      test('migrates image tag to Loree format', () => {
        const wrapper = document.createElement(validParent);
        wrapper.innerHTML = migrationMockData.imageMigration.imageTag;
        migrate.migrateImage(wrapper);
        expect(wrapper.outerHTML).toEqual(
          `<${validParent}>${migrationMockData.imageMigration.imageWrapper}</${validParent}>`,
        );
      });
    });
  });
  describe('#setStyleForMigratedImageWrapper', () => {
    let selectedElement: Element;
    test('setting image alignment as right auto using margin', () => {
      const imageWrapper = document.createElement('div');
      imageWrapper.innerHTML = migrationMockData.imageMigration.imageAlignment;
      const imageElement = createElement('img');
      imageElement.style.margin = '0px 0px 0px auto ';
      selectedElement = migrate.setStyleForMigratedImageWrapper(
        imageWrapper.getElementsByClassName(
          CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER,
        )[0] as HTMLElement,
        imageElement,
      );
      expect(selectedElement).toHaveStyle('textAlign:right');
    });
  });
  describe('when immediate parent is not a div/th/td/a', () => {
    test('migrates image tag to Loree format', () => {
      const wrapper = document.createElement('div');
      const imageWrapper = document.createElement('p');
      imageWrapper.innerHTML = migrationMockData.imageMigration.imageTag;
      wrapper.appendChild(imageWrapper);
      migrate.migrateImage(wrapper);
      expect(wrapper.innerHTML).toEqual(`<p>${migrationMockData.imageMigration.imageWrapper}</p>`);
    });
  });
  describe('migrating image wrapper content', () => {
    test('image wrapper without width style attribute', () => {
      const imageWrapper = createElement('img');
      const wrapper = createElement('div');
      wrapper.append(imageWrapper);

      migrate.migrateImage(wrapper);
      expect(wrapper).not.toHaveStyle('width: 100%');
    });
    test('image wrapper with width style attribute', () => {
      const image = createElement('img');
      const wrapper = createElement('div');
      wrapper.setAttribute('style', 'width: 100%');
      wrapper.append(image);
      migrate.migrateImage(wrapper);
      expect(wrapper).not.toHaveStyle('width: 100%');
    });
  });
  describe('when parent has a div tag', () => {
    test('do not create new wrapper element instead add wrapper class to parent', () => {
      const wrapper = document.createElement('div');
      wrapper.className = 'img-responsive';
      wrapper.innerHTML = migrationMockData.imageMigration.imageTag;
      migrate.migrateImage(wrapper);
      expect(wrapper.outerHTML).toEqual(
        `<div class="img-responsive loree-iframe-content-image-wrapper" style="margin: 10px auto 20px; padding: 20px 20px 20px 20px; text-align: center;"><img alt="" src="https://wysiwyg-page-builder.s3-ap-southeast-2.amazonaws.com/Image-block.png" title="" class="img-fluid loree-iframe-content-image" style="height: auto; max-width: 100%; min-height: auto; width: 50%;"></div>`,
      );
    });
  });

  describe('when image has an A parent', function () {
    test('image is wrapped in an image wrapper correctly', () => {
      const d1 = document.createElement('p');
      d1.innerHTML = '<a><img style="margin: 5px;padding: 6px;"></a>';

      migrate.migrateImage(d1);
      expect(d1.innerHTML).toEqual(
        `<div class="loree-iframe-content-image-wrapper" style="
    margin:5px;
    padding:6px;
    
    "><a><img style="margin: 5px;padding: 6px;" class="loree-iframe-content-image"></a></div>`,
      );
    });
  });
});

describe('#MigrateBoldTag', () => {
  describe('when immediate parent is div tag', () => {
    test('migrates bold tag with p tag', () => {
      const wrapper = document.createElement('div');
      wrapper.innerHTML = `<b>Insert text here</b>`;
      migrate.migrateBoldTag(wrapper);
      expect(wrapper.innerHTML).toEqual(
        `<p class="bStyle" style="font-weight: bold;">Insert text here</p>`,
      );
    });
    test('migrates bold tag with strong tag', () => {
      const wrapper = document.createElement('div');
      wrapper.innerHTML = `<p><strong>Insert text here</strong></p>`;
      migrate.migrateBoldTag(wrapper);
      expect(wrapper.innerHTML).toEqual(
        `<p><span class="bStyle" style="font-weight: bold;">Insert text here</span></p>`,
      );
    });
  });
  describe('when immediate parent is not a div tag', () => {
    test('migrates bold tag with span tag', () => {
      const wrapper = document.createElement('p');
      wrapper.innerHTML = `<b>Insert text here</b>`;
      migrate.migrateBoldTag(wrapper);
      expect(wrapper.innerHTML).toEqual(
        `<span class="bStyle" style="font-weight: bold;">Insert text here</span>`,
      );
    });
  });
});

describe('#anchorTagMigration', () => {
  let wrapper: HTMLElement;
  beforeEach(() => {
    wrapper = document.createElement('h3');
    wrapper.className = 'loree-iframe-content-element';
    const atag = document.createElement('a');
    atag.href = 'http://www.google.com';
    atag.className = 'loree-iframe-content-element';
    atag.innerHTML = 'Click here to visit Google';
    wrapper.appendChild(atag);
    document.body.appendChild(wrapper);
  });
  test('migrated a tag with proper wrapper', () => {
    migrate.migrateAnchorTag(wrapper);
    expect(document.body.innerHTML).toEqual(migrationMockData.anchorTagMigration.properWrapper);
  });

  test('if content migrated with SUB tag should contains span with style', () => {
    const subTag = migrationMockData.anchorTagMigration.subTagInput;
    wrapper.insertAdjacentHTML('beforeend', subTag);
    migrate.migrateAnchorTag(wrapper);
    expect(wrapper.children[1].outerHTML).toEqual(
      migrationMockData.anchorTagMigration.subTagOutput,
    );
  });

  test('if content migrated with SUP tag should contains span with style', () => {
    const supTag = migrationMockData.anchorTagMigration.supTagInput;
    wrapper.insertAdjacentHTML('beforeend', supTag);
    migrate.migrateAnchorTag(wrapper);
    expect(wrapper.children[1].outerHTML).toEqual(
      migrationMockData.anchorTagMigration.supTagOutput,
    );
  });
  test('if migrated content contains uncovered tags still A tag should be visible with text and attributes', () => {
    const unCoveredTag = migrationMockData.anchorTagMigration.tableInput;
    wrapper.insertAdjacentHTML('beforeend', unCoveredTag);
    migrate.migrateAnchorTag(wrapper);
    expect(wrapper.children[1].children[0].innerHTML).toBe(
      '<a href="http://www.savings.com" class="loree-iframe-content-element" target="blank" onclick="event.preventDefault()">Monthly savings</a>',
    );
    expect(wrapper.children[1].children[0].textContent).toBe('Monthly savings');
  });
  test('migrate anchor tag which contains image', () => {
    const imageAnchor = migrationMockData.anchorTagMigration.imageTagInput;
    wrapper.insertAdjacentHTML('beforeend', imageAnchor);
    migrate.migrateAnchorTag(wrapper);
    expect(wrapper.children[1].outerHTML).toEqual(
      migrationMockData.anchorTagMigration.imageTagOutput,
    );
  });
  describe('#anchorTagWithDiv', () => {
    let divElement: HTMLElement;
    beforeEach(() => {
      divElement = document.createElement('div');
    });
    afterEach(() => {
      divElement.innerHTML = '';
    });
    test('anchor tag direct child for div element wrapped into p tag', () => {
      divElement.innerHTML = migrationMockData.anchorTagMigration.anchorWithDivInput;
      migrate.migrateAnchorTag(divElement.childNodes[0] as HTMLElement);
      expect((divElement.childNodes[0] as HTMLElement).outerHTML).toBe(
        migrationMockData.anchorTagMigration.anchorWithDivOutput,
      );
    });
    test('image wrapped with anchor tag should not be wrapped with p tag', () => {
      divElement.innerHTML = migrationMockData.anchorTagMigration.imageWrappedWithAnchor;
      migrate.migrateAnchorTag(divElement.childNodes[0] as HTMLElement);
      expect((divElement.childNodes[0] as HTMLElement).outerHTML).toBe(
        migrationMockData.anchorTagMigration.imageWrappedWithAnchor,
      );
    });
  });
});

describe('#migrateVideo', () => {
  const wrapper = document.createElement('div');
  afterEach(() => {
    wrapper.innerHTML = '';
  });
  describe('when video element is not in Loree V2 format', () => {
    test('migrates video element without class wrapper to Loree V2 format', () => {
      wrapper.style.cssText = 'width: auto; height: auto; margin: 10px 0px 10px 0px;';
      wrapper.innerHTML = migrationMockData.videoMigration.videoTag;
      migrate.migrateVideo(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.videoMigration.wrapperLessVideoTag);
    });
    test('migrates video element with class wrapper to Loree V2 format', () => {
      wrapper.className = 'video-responsive';
      wrapper.style.cssText = 'width: auto; height: auto; margin: 10px 0px 10px 0px;';
      wrapper.innerHTML = migrationMockData.videoMigration.videoTag;
      migrate.migrateVideo(wrapper);
      expect(wrapper.outerHTML).toEqual(migrationMockData.videoMigration.wrapperedVideoTag);
    });
  });
  describe('when video element is already in Loree V2 format', () => {
    test('does not migrate', () => {
      wrapper.style.cssText = 'width: auto; height: auto; margin: 10px 0px 10px 0px;';
      wrapper.innerHTML = migrationMockData.videoMigration.wrapperedVideoTag;
      migrate.migrateVideo(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.videoMigration.wrapperedVideoTag);
    });
  });
});
describe('#migrateVideoIframe', () => {
  describe('when iframe video element is not in Loree V2 format', () => {
    test('migrates iframe video element to Loree V2 format', () => {
      const wrapper = document.createElement('div');
      wrapper.innerHTML = migrationMockData.videoMigration.videoTag;
      migrate.migrateVideoIframe(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.videoMigration.videoTag);
    });
  });
  describe('when iframe video element is already in Loree V2 format', () => {
    test('does not migrate', () => {
      const wrapper = document.createElement('div');
      wrapper.innerHTML = migrationMockData.videoMigration.wrapperedVideoTag;
      migrate.migrateVideoIframe(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.videoMigration.wrapperedVideoTag);
    });
  });
});
describe('Video alignment', () => {
  const wrapper = document.createElement('div');
  beforeEach(() => {
    document.body.innerHTML = '';
  });
  test('setting video alignment as left auto using margin', () => {
    wrapper.innerHTML = migrationMockData.videoMigration.rightAlignment;
    document.body.append(wrapper);
    const videoWrapper = getElementByClassName(wrapper, CONSTANTS.LOREE_NO_POINTER);
    migrate.wrapVideoElement(videoWrapper);
    expect(getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)[0]).toHaveStyle(
      'textAlign:right',
    );
  });
  test('setting video alignment as right auto using margin', () => {
    wrapper.innerHTML = migrationMockData.videoMigration.leftAlignment;
    document.body.append(wrapper);
    const videoWrapper = getElementByClassName(wrapper, CONSTANTS.LOREE_NO_POINTER);
    migrate.wrapVideoElement(videoWrapper);
    expect(getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)[0]).toHaveStyle(
      'textAlign:left',
    );
  });
  test('setting video alignment as left auto right auto using margin', () => {
    wrapper.innerHTML = migrationMockData.videoMigration.centerAlignment;
    document.body.append(wrapper);
    const videoWrapper = getElementByClassName(wrapper, CONSTANTS.LOREE_NO_POINTER);
    migrate.wrapVideoElement(videoWrapper);
    expect(getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)[0]).toHaveStyle(
      'textAlign:center',
    );
  });
  test('video alignment with float value', () => {
    wrapper.innerHTML = migrationMockData.videoMigration.wrapperAlignment;
    document.body.append(wrapper);
    const videoWrapper = getElementByClassName(wrapper, CONSTANTS.LOREE_NO_POINTER);
    migrate.wrapVideoElement(videoWrapper);
    expect(getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)[0]).toHaveStyle(
      'textAlign:right',
    );
  });
  test('video alignment without float value', () => {
    wrapper.innerHTML = migrationMockData.videoMigration.videoMigrationWrapperAlignment;
    document.body.append(wrapper);
    const videoWrapper = getElementByClassName(wrapper, CONSTANTS.LOREE_NO_POINTER);
    migrate.wrapVideoElement(videoWrapper);
    expect(getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)[0]).not.toHaveStyle(
      'textAlign:right',
    );
  });
});
describe('#anchorNodeAttributeDomTest', () => {
  function anchorNodeDom() {
    const div = document.createElement('div');
    div.id = 'loree-iframe-content-wrapper';
    div.innerHTML = `<div class="loree-iframe-content-row><div class="loree-iframe-content-column>
    <p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: &quot;Open Sans&quot;; font-size: 20px;"><a href="https://crystaldelta.instructure.com/courses/178/modules/26645" title="https://crystaldelta.instructure.com/courses/178/modules/26645" class="loree-iframe-content-element " style="color: #b01a8b; text-decoration: overline dotted; font-family: Arial; font-style: italic;" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/178/modules/26645" data-api-returntype="Module"><span class="loree-iframe-content-element normalStyle " style=" font-style: italic;">Lorem</span></a> Ipsum Lorem IpsumLoremIpsumLorem Ipsum any quotes here applied will be displayed</p></div></div>
    `;
    return div;
  }
  let container: HTMLElement | null;
  beforeEach(() => {
    container = anchorNodeDom() as HTMLElement;
  });

  afterEach(() => {
    container = null;
  });
  test('anchor tag with no attribute of target', () => {
    migrate.migrateAnchorTag(container as HTMLElement);
    const anchorTag = (container as HTMLElement).querySelector('a');
    expect(anchorTag?.hasAttribute('target')).toBeFalsy();
  });

  test('multiple anchor tag with target attribute and without target attribute', () => {
    (container as HTMLElement)
      .querySelector('p')
      ?.insertAdjacentHTML(
        'beforeend',
        `<a href="https://crystaldelta.instructure.com/courses/178/modules/26645" title="https://crystaldelta.instructure.com/courses/178/modules/26645" class="loree-iframe-content-element" target="_blank" data-testid="user-anchor">Text</a> `,
      );
    migrate.migrateAnchorTag(container as HTMLElement);
    expect(queryByTestId(container as HTMLElement, 'user-anchor')).toBeTruthy();
    expect(queryByTestId(container as HTMLElement, 'user-anchor')).toHaveAttribute(
      'target',
      '_blank',
    );
  });
});

describe('#IsContaier', () => {
  let containerSource = `<div class="loree-iframe-content-container-wrapper" style="display: flex; margin: 0px; padding: 5px;">
    <div class="loree-iframe-content-container" style="width: 100%;">
        <div class="row loree-iframe-content-row" style="display: flex; width: 100%; max-width: 100%; height: %; padding: 10px; margin: 0px;">
            <div class="col-12 col-md-6 col-lg-6 loree-iframe-content-column" style="display: block; width: 50%; float: left; padding: 10px 0px; font-family: Helvetica;">
                <p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: #000000; font-size: 16px;">Insert Text Here</p>
            </div>
            <div class="col-12 col-md-6 col-lg-6 loree-iframe-content-column" style="display: block; width: 50%; float: right; padding: 10px 0px; font-family: Helvetica;">
                <p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: #000000; font-size: 16px;">Insert Text Here</p>
            </div>
        </div>
    </div>
  </div>`;
  let nonContainerSource = `<div class="row " style="display: flex; width: 100%; max-width: 100%; height: %; padding: 10px; margin: 0px;">
    <div class="col-12 col-md-6 col-lg-6 " style="display: block; min-height: 50px; width: 50%; height: %; float: right; padding: 10px 0; font-family: Helvetica; font: none;">
      <p class="" style="padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 16px;">Insert Text Here</p>
      <div class="row " style="display: flex; width: 100%; max-width: 100%; height: %; padding: 10px; margin: 0px;">
        <div class="col-12 col-md-6 col-lg-6 " style="display: block; min-height: 50px; width: 50%; height: %; float: left; padding: 10px 0; font-family: Helvetica; font: none;">
            <p class="" style="padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 16px;">Insert Text Here</p>
        </div>
        <div class="col-12 col-md-6 col-lg-6 " style="display: block; min-height: 50px; width: 50%; height: %; float: right; padding: 10px 0; font-family: Helvetica; font: none;">
            <p class="" style="padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 16px;">Insert Text Here</p>
        </div>
      </div>
  </div>`;
  let containerRowElement: HTMLElement;
  let nonContainerRowElement: HTMLElement;

  function constructDOM(rawHTML: string): HTMLElement {
    const element = document.createElement('div');
    element.innerHTML = rawHTML;
    const rows = element.getElementsByClassName('row');
    return rows[0] as HTMLElement;
  }

  beforeAll(() => {
    containerRowElement = constructDOM(containerSource);
    nonContainerRowElement = constructDOM(nonContainerSource);
  });

  test('when container wrapper exist', () => {
    const isContainer = migrate.isContainer(containerRowElement);
    expect(isContainer).toBe(true);
  });

  test('when container wrapper does not exist', () => {
    const isContainer = migrate.isContainer(nonContainerRowElement);
    expect(isContainer).toBe(false);
  });

  afterAll(() => {
    containerSource = '';
    nonContainerSource = '';
  });
});

describe('#migrateIconText', () => {
  const wrapper = document.createElement('div');
  describe('when got content from V1', () => {
    test('migartes content into Loree V2 format', () => {
      wrapper.innerHTML = migrationMockData.iconTextLoreeV1Input;
      migrate.migrateIconText(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.iconTextLoreeV1Output);
    });
  });
  describe('when got Loree V2 content', () => {
    test('replaces id with class', () => {
      wrapper.innerHTML = migrationMockData.iconTextLoreeV2Input;
      migrate.migrateIconText(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.iconsTextLoreeV2Output);
    });
  });
  describe('remove container wrapper for special elements', () => {
    test('container wrapper is removed', () => {
      wrapper.innerHTML = migrationMockData.iconTextWithContainerWrapper;
      migrate.removeContainerWrapperForExistingElement(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.iconTextWithContainerWrapperOutput);
    });
  });
});
describe('#migrateRowWithCustomStyles', () => {
  const wrapper = document.createElement('div');
  beforeAll(() => {
    wrapper.innerHTML = migrationMockData.rowWithCustomStylesInput;
  });
  describe('when row does not have customised width', () => {
    test('row is not wrapped with container', () => {
      migrate.migrateRowWithCustomStyles(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.rowWithCustomStylesInput);
    });
  });
  describe('when row has customised width', () => {
    test('row wrapped with container', () => {
      (wrapper.firstChild as HTMLElement).style.width = '70%';
      migrate.migrateRowWithCustomStyles(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.rowWithCustomStylesOutput);
    });
  });
});
describe('#tableMigration', () => {
  const wrapper = document.createElement('div');
  afterEach(() => {
    wrapper.innerHTML = '';
  });
  test('create table wrapper if not wrapped', () => {
    wrapper.innerHTML = migrationMockData.tableMigration.tableTag;
    migrate.migrateTable(wrapper);
    expect(wrapper.innerHTML).toEqual(migrationMockData.tableMigration.wrappedTable);
  });
  test('table wrapped with single parent if already wrapped', () => {
    wrapper.className = 'table-responsive';
    wrapper.setAttribute('style', 'width:80%;margin:0px auto;padding:10px');
    wrapper.innerHTML = migrationMockData.tableMigration.tableTag;
    migrate.migrateTable(wrapper);
    expect(wrapper.outerHTML).toEqual(migrationMockData.tableMigration.unwrappedTable);
  });
});
describe('#migrateColumn', () => {
  const wrapper = document.createElement('div');
  describe('when content has one column with header/one column element', () => {
    test('adds row class and for one column with header merge two columns into one', () => {
      wrapper.innerHTML = migrationMockData.columnMigration.input;
      migrate.migrateColumn(wrapper);
      expect(wrapper.innerHTML).toEqual(migrationMockData.columnMigration.output);
    });
  });
});
describe('#migrateElementsWithoutRowColumn', () => {
  describe('when elements do not have row column structure', () => {
    test('wrap elements with row column', () => {
      const result = migrate.migrateElementsWithoutRowColumn(
        migrationMockData.elementsWithoutRowColumnMigration.input,
      );
      expect(result).toEqual(migrationMockData.elementsWithoutRowColumnMigration.output);
    });
  });
});
describe('#migrateTextContent', () => {
  let wrapper: HTMLElement;
  beforeEach(() => {
    wrapper = document.createElement('div');
  });
  afterEach(() => {
    wrapper.innerHTML = '';
  });
  test('plain text content inside div are properly wrapped into Paragraph tag', () => {
    wrapper.innerHTML = migrationMockData.divWithTextMigration.plainInput;
    migrate.migrateDivText(wrapper);
    expect(wrapper.innerHTML).toEqual(migrationMockData.divWithTextMigration.convertedPlainOutput);
  });
  test('nested content along with plain text still wrapped with paragrapgh tag', () => {
    wrapper.innerHTML = migrationMockData.divWithTextMigration.nestedInput;
    migrate.migrateDivText(wrapper);
    expect(wrapper.innerHTML).toEqual(migrationMockData.divWithTextMigration.convertedNestedOutput);
  });
  test('text content along with inline element wrapped in single p tag', () => {
    wrapper.innerHTML = migrationMockData.divWithTextMigration.contentWithInlineElement;
    migrate.migrateDivText(wrapper);
    expect(wrapper.innerHTML).toEqual(
      migrationMockData.divWithTextMigration.convertedContentWithInlineElement,
    );
  });
});
describe('#migrateMultipleColWithHeader', () => {
  let wrapper: HTMLElement;
  beforeEach(() => {
    wrapper = document.createElement('div');
  });
  afterEach(() => {
    wrapper.innerHTML = '';
  });
  test('2 col header with no additional wrapper div', () => {
    wrapper.innerHTML = migrationMockData.multipleColHeader.twoCol;
    migrate.migrateColumn(wrapper);
    expect(wrapper.innerHTML).toEqual(migrationMockData.multipleColHeader.twoColOutput);
  });
  test('3 col header with no additional wrapper div', () => {
    wrapper.innerHTML = migrationMockData.multipleColHeader.threeCol;
    migrate.migrateColumn(wrapper);
    expect(wrapper.innerHTML).toEqual(migrationMockData.multipleColHeader.threeColOutput);
  });
});
describe('Navigation migration', () => {
  let wrapper: HTMLElement;
  let navigationWrapper: HTMLElement;
  beforeEach(() => {
    wrapper = createDiv();
    wrapper.innerHTML = '';
    navigationWrapper = createDiv(
      'loree-iframe-content-menu',
      `row ${CONSTANTS.LOREE_IFRAME_CONTENT_MENU}`,
    );
    wrapper.append(navigationWrapper);
  });
  test('migrate the non flex display style in navigation element', () => {
    navigationWrapper.setAttribute('style', `display: inline-flex;`);
    migrate.migrateNavigationContent(wrapper);
    expect(navigationWrapper.style.display).toBe('flex');
  });
  test('migrate the flex display style in navigation element', () => {
    navigationWrapper.setAttribute('style', `display: flex;`);
    migrate.migrateNavigationContent(wrapper);
    expect(navigationWrapper.style.display).toBe('flex');
  });
});
