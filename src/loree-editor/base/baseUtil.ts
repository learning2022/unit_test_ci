import CONSTANTS from '../constant';
import Base from '../base';
import { customBlockEditStatus, customBlockType } from '../../utils/saveContent';
import { getIFrameElementById } from '../common/dom';

const baseClass = () => {
  const base = new Base();
  return base;
};

export const isSelectionFirstElementInContent = () => {
  const selectedElement = baseClass().getSelectedElement();
  const loreeWrapperContent = getIFrameElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
  return loreeWrapperContent?.firstElementChild === selectedElement;
};

export const showDelete = (isElement: boolean): boolean => {
  const selectedElement = baseClass().getSelectedElement();
  const featuresList = baseClass().getFeatureList();
  const isEditCustomBlock = isSelectionFirstElementInContent();
  return (
    (isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN) &&
      !featuresList?.deletecolumn) ||
    (isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ROW) &&
      !featuresList?.deleterow) ||
    ((isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER) ||
      isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) &&
      !featuresList?.deleteelement) ||
    (isEditCustomBlock && customBlockEditStatus && customBlockType !== 'My Template' && !isElement)
  );
};

export const showLinkOption = (): boolean => {
  const selectedElement = baseClass().getSelectedElement();
  return (
    isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
    isClassListContains(selectedElement, CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION) ||
    isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)
  );
};

export const showLanguageOption = (selectedElement: HTMLElement | null): boolean => {
  return selectedElement
    ? ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'ol', 'ul', 'caption'].includes(
        selectedElement.tagName.toLowerCase(),
      )
    : false;
};

export const isInteractiveElement = (): boolean => {
  const selectedElement = baseClass().getSelectedElement();
  return isClassListContains(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER);
};

export const isClassListContains = (element: HTMLElement | null, className: string): boolean => {
  return element ? element.classList.contains(className) : false;
};

export const addClassToElement = (element: HTMLElement, className: string) => {
  element.classList.add(className);
};

export const removeClassFromElement = (element: HTMLElement, className: string) => {
  element.classList.remove(className);
};
