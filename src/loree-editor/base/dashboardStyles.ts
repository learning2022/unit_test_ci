import CONSTANTS from '../constant';
import { isClassListContains } from '../utils';
import DEFAULTSTYLE from '../defaultElementSTyles';
class DashboardStyle {
  setElementStyles(anchorElement: HTMLElement, defaultStyle: _Any) {
    if (!anchorElement) {
      return;
    }
    anchorElement.style.fontSize = defaultStyle.size;
    anchorElement.style.fontFamily = defaultStyle.font;
    if (isClassListContains(anchorElement, CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)) {
      anchorElement.style.padding = DEFAULTSTYLE.navigationPadding;
      anchorElement.style.margin = DEFAULTSTYLE.navigationMargin;
    } else if (isClassListContains(anchorElement, CONSTANTS.LOREE_TABLE_CAPTION_ELEMENT)) {
      anchorElement.style.captionSide = DEFAULTSTYLE.tablecaptionSide;
      anchorElement.style.fontFamily = DEFAULTSTYLE.tableCaptionFontFamily;
      anchorElement.style.borderWidth = DEFAULTSTYLE.tablecaptionBorderWidth;
      anchorElement.style.borderStyle = DEFAULTSTYLE.tablecaptionBorderStyle;
      anchorElement.style.borderColor = DEFAULTSTYLE.tablecaptionBorderColor;
      anchorElement.style.padding = DEFAULTSTYLE.tablecaptionPadding;
      anchorElement.style.margin = DEFAULTSTYLE.tablecaptionMargin;
    }
  }
}
export default DashboardStyle;
