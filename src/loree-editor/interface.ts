import { State } from '@popperjs/core';
import { TabType } from '../lti/admin/customTemplate/editTemplateModal';
export interface DeviceInterface {
  desktop?: boolean;
  tablet?: boolean;
  mobile?: boolean;
}

export interface HeaderInterface {
  save?: boolean;
  redo?: boolean;
  undo?: boolean;
  title?: string;
  devices?: DeviceInterface;
  preview?: boolean;
  accessibility?: boolean;
}
export interface TextOptionInterface {
  paragraphOption?: boolean;
  boldOption?: boolean;
  italicOption?: boolean;
  fontFamilyOption?: boolean;
  fontSizeOption?: boolean;
  spacingOption?: boolean;
  fontColorOption?: boolean;
  fontHighlightColorOption?: boolean;
  unorderedListColorOption?: boolean;
  orderedListColorOption?: boolean;
  wordOption?: boolean;
  alignmentOption?: boolean;
  linkOption?: boolean;
  clearOption?: boolean;
}

export interface CustomColors {
  color: string;
}

export interface CustomFonts {
  fonts: [];
}
export interface FontListType {
  [key: string]: number | string | boolean | undefined;
}
export interface FontList extends FontListType {
  url?: string | undefined;
  name: string;
  fontFamily: string;
  selected: boolean;
}
export interface ConfigInterface {
  header?: HeaderInterface;
  features?: FeatureInterface | DynamicFeatureKey;
  textOptions?: TextOptionInterface;
  customColor?: CustomColors[];
  customFonts?: _Any;
  customHeaderStyle?: _Any;
  customLinkStyle?: _Any;
}

export interface FeatureInterface {
  fontstyles?: boolean | undefined;
  saveascustomrow?: boolean | undefined;
  saveascustomelement?: boolean | undefined;
  duplicaterow?: boolean | undefined;
  duplicateelement?: boolean | undefined;
  moverow?: boolean | undefined;
  movecolumn?: boolean | undefined;
  moveelement?: boolean | undefined;
  deletecolumn?: boolean | undefined;
  deleterow?: boolean | undefined;
  deleteelement?: boolean | undefined;
  backgroundcoloroftherow?: boolean | undefined;
  backgroundcolorofthecolumn?: boolean | undefined;
  marginrow?: boolean | undefined;
  margincolumn?: boolean | undefined;
  paddingrow?: boolean | undefined;
  paddingcolumn?: boolean | undefined;
  editorganisationh5p?: boolean | undefined;
  deleteorganisationh5p?: boolean | undefined;
  copyandpaste?: boolean | undefined;
  changerow?: boolean | undefined;
  changecolumn?: boolean | undefined;
  image?: boolean | undefined;
  saveastemplate?: boolean | undefined;
  imagedesign?: boolean | undefined;
  videodesign?: boolean | undefined;
  youtube?: boolean | undefined;
  insertbyurl?: boolean | undefined;
  vimeo?: boolean | undefined;
  uploadnewvideo?: boolean | undefined;
  dividerdesign?: boolean | undefined;
  tabledesign?: boolean | undefined;
  navigate?: boolean | undefined;
  h5p?: boolean | undefined;
  templatesfiltersearch?: boolean | undefined;
  outline?: boolean | undefined;
  undoredo?: boolean | undefined;
  codeproperties?: boolean | undefined;
  accessibilitychecker?: boolean | undefined;
  preview?: boolean | undefined;
  rowstructure?: boolean | undefined;
  customrowfiltersearch?: boolean | undefined;
  myinteractive?: boolean | undefined;
  thememanager?: boolean | undefined;
  customBlockImage?: boolean | undefined;
  video?: boolean | undefined;
  textblock?: boolean | undefined;
  divider?: boolean | undefined;
  specialblocks?: boolean | undefined;
  embedurl?: boolean | undefined;
  container?: boolean | undefined;
  table?: boolean | undefined;
  customelementsfiltersearch?: boolean | undefined;
  externalTools?: boolean | undefined;
  banner?: boolean | undefined;
  navigationMenu?: boolean | undefined;
}

export interface DynamicFeatureKey {
  [key: string]: boolean | string;
}
export interface BlockInfo {
  id: string;
  content: string;
  createdBy: string;
  ltiPlatformID: string;
  title: string;
  categoryID: string | number;
  thumbnail: string;
  isGlobal: boolean;
  updatedAt: string;
  active: boolean;
  type: string;
  category: { id: string; name: string };
  createdAt: string;
}
export interface CustomElementsProps {
  loading: boolean;
  filterValue: string;
  globalFilter: string;
  customElements: _Any;
  filteredTemplate: _Any;
  categories: Array<[]>;
  searchValue: string;
  tabType: TabType;
}

type EmailStatus = string | number;
export interface StateP {
  loading: boolean;
  customElements: _Any;
  showEditModal: boolean;
  elementName: string;
  categoryName: string;
  categoryId: EmailStatus;
  elementId: string;
  filteredTemplate: _Any;
  disableEditButton: boolean;
  showToast: boolean;
  toastMessage: string;
  global: boolean;
  index: number;
  active: boolean;
  savingLoader: boolean;
  elementNames: string;
  category: string;
  createdBy: string;
  createdAt: string;
  lastUpdated: string;
}

export interface TemplateProps {
  loading: boolean;
  filterValue: string;
  globalFilter: string;
  customTemplateLists: _Any;
  filteredTemplate: _Any;
  categories: Array<[]>;
  searchValue: string;
  tabType: TabType;
}

export interface StateT {
  loading: boolean;
  showEditModal: boolean;
  templateName: string;
  categoryName: string;
  categoryId: EmailStatus;
  templateId: string;
  customTemplateLists: _Any;
  filteredTemplate: _Any;
  disableEditButton: boolean;
  showToast: boolean;
  toastMessage: string;
  global: boolean;
  active: boolean;
  index: number;
  savingLoader: boolean;
  elementNames: string;
  category: string;
  createdBy: string;
  createdAt: string;
  lastUpdated: string;
}

export interface CustomRowProps {
  loading: boolean;
  filterValue: string;
  globalFilter: string;
  customRows: _Any;
  filteredTemplate: _Any;
  categories: Array<[]>;
  searchValue: string;
  tabType: TabType;
}
export interface StateR {
  loading: boolean;
  customElements: _Any;
  customRows: _Any;
  filteredTemplate: _Any;
  rowName: string;
  disableEditButton: boolean;
  showEditModal: boolean;
  showToast: boolean;
  toastMessage: string;
  rowId: string;
  categoryId: EmailStatus;
  global: boolean;
  index: number;
  active: boolean;
  categoryName: string;
  savingLoader: boolean;
  customRowTitle: string;
  category: string;
  createdBy: string;
  createdAt: string;
  lastUpdated: string;
}

export interface RowContentInterface {
  name: string;
  id: string;
  template: string;
  htmlString: string;
}

export interface RowsInterface {
  name: string;
  id: string;
  template: string;
  multiple?: boolean;
  htmlString?: string;
  contents?: Array<RowContentInterface>;
}

export interface ElementSubContentInterface {
  name: string;
  id: string;
  template: string;
  content: string;
}

export interface ElementContentInterface {
  name: string;
  id: string;
  template: string;
  content: string;
  innerContent: Array<ElementSubContentInterface>;
}

export interface LtiTool {
  id?: string;
  ltiPlatformID?: string | null;
  toolName?: string;
  issuerUrl?: string;
  clientId?: string;
  clientSecret?: string;
  oidcUrl?: string;
  redirectURI?: string;
  targetLinkURI?: string;
  jwksUrl?: string;
  domainName?: string;
  deploymentId?: string;
  toolDeployments?: { items: { id: string }[] };
}

export interface LtiToolLoginHint {
  id?: string;
  issuerUrl: string;
  clientId: string;
  loreeUserEmail: string | null;
  messageType?: string;
  resourceLink?: string;
}

export interface ElementInterface {
  name: string;
  id: string;
  template: string;
  multiple: boolean;
  content: string;
  contents: Array<ElementContentInterface>;
}

export interface VideoModalIconsInterface {
  addIcon: string;
  searchIcon: string;
  youtubeIcon: string;
  vimeoIcon: string;
  loader: string;
}

export interface InteractiveIconsInterface {
  interactiveloader: string;
  interactiveEditCloseIcon: string;
}
export interface ImageModalIconsInterface {
  imageModalSearchIcon: string;
}
export interface ELEMENTSECTIONICONS {
  shapeIcon: string;
  imageIcon: string;
  videoIcon: string;
  textIcon: string;
  pasetAnySource: string;
  blockQuoteIcon: string;
  dividerIcon: string;
  tableIcon: string;
  customElementIcon: string;
  interactiveIcon: string;
  h5pIcon: string;
  embedUrl: string;
}

export interface DIVIDERICONSCOLLECTION {
  verticalLineDivider: string;
  horizontalLineDivider: string;
  verticalSpaceDivider: string;
}

export interface LoreeEditorFeatures {
  [k: string]: {
    label: string;
    value: boolean;
    basicDisabled: boolean;
  };
}

export interface CategorizedFeatures {
  [Category: string]: {
    [Features: string]: string | string[];
  };
}

export interface LmsRoleAndFeatures {
  id: string | Number;
  role: string;
  label: string;
  features: LoreeEditorFeatures;
  base_role_type: string;
  created_at: string;
  last_updated_at: string;
  workflow_state: string;
  is_account_role: boolean;
  permissions: Object;
}

export interface CustomHeaderType {
  [key: string]: {
    size: string;
    font: string;
  };
}

export interface HeaderListType {
  fontFamily: string;
  name: string;
  url: string;
  selected: boolean;
}

export interface RoleListType {
  DisplayName: string;
  Identifier: number;
  nameForCourses: string;
  roleId: number;
  label: string;
  id: number;
  base_role_type: string;
}

export interface IframePopperInstance {
  destroy: () => void;
  state?: State;
  forceUpdate?: () => void;
  update?: () => Promise<Partial<State>>;
}

export interface LanguageProps {
  isAdmin?: boolean;
  updateLanguageEnableStatus?: (status: boolean) => void;
  isEditor?: boolean;
}

export interface LanguageInfo {
  id: string;
  label: string;
  type?: string;
}

export interface PlatformBaseInfo {
  ltiPlatformID: string;
  currentUserId: string;
}

export interface UserLanguageStore {
  selectedMiniMenuLanguage: string;
}

export interface Items {
  id: number;
  name: string;
  position: number;
  unlock_at: null;
  require_sequential_progress: boolean;
  publish_final_grade: boolean;
  prerequisite_module_ids: never[];
  published: boolean;
  items_count: number;
  items_url: string;
}

export interface ModuleProps {
  isModuleListUpdated: (status: boolean) => void;
}
