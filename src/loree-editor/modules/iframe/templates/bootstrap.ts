import CONSTANTS from '../../../constant';
import { elementAddRemoveIcons } from '../../../iconHolder';

const Bootstrap = `
<!doctype html>
  <html lang="en" style="height: auto">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <link rel="stylesheet" href="/stylesheets/bootstrapStyle.min.css">
      <link rel="stylesheet" href="/stylesheets/textOptionsStyle.min.css">
      <link rel="stylesheet" href="/stylesheets/blockOptionsStyle.min.css">
      <link rel="stylesheet" href="/stylesheets/blockEditOptionsStyle.min.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/nano.min.css"/>
      <style id=${CONSTANTS.LOREE_IFRAME_STYLE_WRAPPER}></style>
      <link rel="stylesheet" href="/stylesheets/externalCanvasStyles.min.css" id=${CONSTANTS.LOREE_IFRAME_EXTERNAL_CANVAS_STYLE_WRAPPER}> 
      <script async src="/scripts/axe.min.js" id="${CONSTANTS.LOREE_AXE_SCRIPT}"></script>
    </head>
      <body style="height: 100%; min-height: 100vh; border: 20px solid #e9e9e9; border-bottom-width: 1px;">
        <div class="loree-space-help-icon" id=${CONSTANTS.LOREE_SPACE_HELP_ICON}></div>
        <div>
          <div class="loreeContentWrapper"  id=${CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER}></div>
          <div class="loreeAddRowWrapper" style="display: none; user-select: none;">
            <div class="loreeAddRowButtonWrapper" id=${CONSTANTS.LOREE_ADD_ROW_BUTTON_WRAPPER}>
              <hr>
              <button class="btn rounded-pill loreeAddRowButton" id=${CONSTANTS.LOREE_ADD_ROW_BUTTON} style="display: inline-flex;">
                <span class="loreeAddRowButtonLabel">Add Row</span>
                <svg width="14" height="14" viewBox="0 0 14 14" class="btn-icon"><path id="Path_2823" data-name="Path 2823" d="M7,0A7,7,0,1,1,0,7,7,7,0,0,1,7,0Z" fill="#fff"></path><g id="Group_57" data-name="Group 57" transform="translate(3 3)"><g id="Group_56" data-name="Group 56"><path id="Path_2709" data-name="Path 2709" d="M130.847,126.967h-2.879v-2.879a.5.5,0,1,0-1,0v2.879h-2.879a.5.5,0,0,0,0,1h2.879v2.879a.5.5,0,1,0,1,0v-2.879h2.879a.5.5,0,0,0,0-1Z" transform="translate(-123.588 -123.588)" fill="#000d9c"></path></g></g></svg>
              </button>
              <button class="btn loreeCloseRowButton" id=${CONSTANTS.LOREE_CLOSE_ROW_BUTTON} style="display: none;">
                <svg width="24" height="24" viewBox="0 0 24 24" class="btn-icon"><g id="close_row" transform="translate(-668 -60)"><circle id="Ellipse_9" data-name="Ellipse 9" cx="11" cy="11" r="11" transform="translate(669 61)" fill="#fff"></circle><path id="Union_13" data-name="Union 13" d="M0,12A12,12,0,1,1,12,24,11.993,11.993,0,0,1,0,12Zm1.6,0A10.4,10.4,0,1,0,12,1.6,10.412,10.412,0,0,0,1.6,12Zm13.657,4.388L12,13.132,8.742,16.389A.8.8,0,0,1,7.61,15.258L10.868,12,7.61,8.742A.8.8,0,0,1,8.742,7.611L12,10.868l3.257-3.257a.8.8,0,0,1,1.132,1.132L13.131,12l3.257,3.258a.8.8,0,1,1-1.132,1.131Z" transform="translate(668 60)" fill="#909090"></path></g></svg>
              </button>
            </div>
          </div>

          <button id=${CONSTANTS.LOREE_ADD_ELEMENT_BUTTON} class="addElementIcon" style="display: none">
            ${elementAddRemoveIcons.addElementIcon}
          </button>

          <button id=${CONSTANTS.LOREE_CLOSE_ADD_ELEMENT_BUTTON} class="closeElementIcon" style="display: none">
            ${elementAddRemoveIcons.closeElementIcon}
          </button>

          <button id=${CONSTANTS.LOREE_ADD_ROW_BUTTON_FOR_CONTAINER} class="addElementIcon" style="display: none">
            ${elementAddRemoveIcons.addElementIcon}
          </button>

          <button id=${CONSTANTS.LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER} class="closeElementIcon" style="display: none">
            ${elementAddRemoveIcons.closeElementIcon}
          </button>

          <button id=${CONSTANTS.LOREE_INSERT_ROW_BUTTON} class="addElementIcon" style="display: none">
          ${elementAddRemoveIcons.addElementIcon}
          </button>

          <button id=${CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON} class="closeElementIcon" style="display: none">
            ${elementAddRemoveIcons.closeElementIcon}
          </button>

          <div class="loree-text-options" id=${CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER} style="visibility: hidden"></div>

          <div class="loree-block-options" id=${CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER} style="display: none"></div>

          <button class="loree-language-iso-label" id=${CONSTANTS.LOREE_LANGUAGE_ISO_LABEL_BUTTON} style="display: none"></button>

          <div class="loree-text-options-anchor-link" id=${CONSTANTS.LOREE_ANCHOR_LINK_WRAPPER} style="display: none"></div>

          <div class="loree-block-edit-options" id=${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER} style="display: none"></div>

          <div class="foreground-color-picker-wrapper" id=${CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER} style="display: none; z-index: 10">
            <div id='${CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER}'></div>
          </div>

          <div class="text-background-color-picker-wrapper" id=${CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER} style="display: none; z-index: 10">
            <div id='${CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER}'></div>
          </div>

          <div class="border-color-picker-wrapper" id=${CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER} style="display: none; z-index: 10">
            <div id='${CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER}'></div>
          </div>

          <div class="background-color-picker-wrapper" id=${CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER} style="display: none; z-index: 10">
            <div id='${CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER}'></div>
          </div>

          <div class="background-color-picker-wrapper" id=${CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER} style="display: none; z-index: 10">
            <div id='${CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER}'></div>
          </div>

          <div class="loree-row-hamburger-icon" id=${CONSTANTS.LOREE_ROW_HAMBURGER_ICON} style="display: none">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><g id="row_mini_menu" transform="translate(-182 -396.641)"><rect id="Rectangle_2854" data-name="Rectangle 2854" width="20" height="20" transform="translate(182 396.641)" fill="#000d9c"/><g id="Group_1255" data-name="Group 1255" transform="translate(186 400.641)"><rect id="Rectangle_2855" data-name="Rectangle 2855" width="12" height="2" transform="translate(0 5)" fill="#fff"/><rect id="Rectangle_2856" data-name="Rectangle 2856" width="12" height="2" fill="#fff"/><rect id="Rectangle_2857" data-name="Rectangle 2857" width="12" height="2" transform="translate(0 10)" fill="#fff"/></g></g></svg>
          </div>
          <div class="loree-column-hamburger-icon" id=${CONSTANTS.LOREE_COLUMN_HAMBURGER_ICON} style="display: none">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><g id="column_mini_menu" transform="translate(-202 -396.641)"><rect id="Rectangle_2858" data-name="Rectangle 2858" width="20" height="20" transform="translate(202 396.641)" fill="#585858"/><g id="Group_1256" data-name="Group 1256" transform="translate(218 400.641) rotate(90)"><rect id="Rectangle_2855" data-name="Rectangle 2855" width="12" height="2" transform="translate(0 5)" fill="#fff"/><rect id="Rectangle_2856" data-name="Rectangle 2856" width="12" height="2" fill="#fff"/><rect id="Rectangle_2857" data-name="Rectangle 2857" width="12" height="2" transform="translate(0 10)" fill="#fff"/></g></g></svg>
          </div>
          
          <div class="loree-table-hamburger-icon" id=${CONSTANTS.LOREE_TABLE_HAMBURGER_ICON} style="display: none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24.001"> <g id="table_choose" transform="translate(-209 -110)"> <rect id="Rectangle_2464" data-name="Rectangle 2464" width="24" height="24" transform="translate(209 110)" fill="#fff"/> <g id="Group_850" data-name="Group 850" transform="translate(-2492.704 -1382.534)"> <g id="Group_823" data-name="Group 823" transform="translate(2701.704 1492.535)"> <path id="Path_3387" data-name="Path 3387" d="M2724.277,1493.963v21.145h-21.145v-21.145h21.145m.1-1.428h-21.338a1.331,1.331,0,0,0-1.331,1.331V1515.2a1.331,1.331,0,0,0,1.331,1.331h21.338a1.331,1.331,0,0,0,1.331-1.331v-21.338a1.331,1.331,0,0,0-1.331-1.331Z" transform="translate(-2701.705 -1492.535)" fill="#585858"/> </g> <g id="Group_824" data-name="Group 824" transform="translate(2719.05 1498.69)"> <rect id="Rectangle_2451" data-name="Rectangle 2451" width="0.952" height="16.918" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_825" data-name="Group 825" transform="translate(2713.347 1498.69)"> <rect id="Rectangle_2452" data-name="Rectangle 2452" width="0.952" height="16.918" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_826" data-name="Group 826" transform="translate(2707.169 1492.86)"> <rect id="Rectangle_2453" data-name="Rectangle 2453" width="1.904" height="22.748" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_827" data-name="Group 827" transform="translate(2708.12 1509.461)"> <rect id="Rectangle_2454" data-name="Rectangle 2454" width="17.077" height="0.952" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_828" data-name="Group 828" transform="translate(2708.12 1503.758)"> <rect id="Rectangle_2455" data-name="Rectangle 2455" width="17.077" height="0.952" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_829" data-name="Group 829" transform="translate(2702.449 1497.58)"> <rect id="Rectangle_2456" data-name="Rectangle 2456" width="22.748" height="1.904" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_830" data-name="Group 830" transform="translate(2718.575 1493.143)"> <rect id="Rectangle_2457" data-name="Rectangle 2457" width="1.904" height="5.547" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_831" data-name="Group 831" transform="translate(2712.872 1493.143)"> <rect id="Rectangle_2458" data-name="Rectangle 2458" width="1.904" height="5.547" transform="translate(0 0)" fill="#585858"/> </g> <g id="Group_832" data-name="Group 832" transform="translate(2702.448 1508.984)"> <rect id="Rectangle_2459" data-name="Rectangle 2459" width="5.671" height="1.904" transform="translate(0)" fill="#585858"/> </g> <g id="Group_833" data-name="Group 833" transform="translate(2702.448 1503.281)"> <rect id="Rectangle_2460" data-name="Rectangle 2460" width="5.671" height="1.904" transform="translate(0 0)" fill="#585858"/> </g> </g> </g> </svg>
          </div>

          <div class="loree-navigation-hamburger-icon" id=${CONSTANTS.LOREE_NAVIGATE_HAMBURGER_ICON} style="display: none">
          <svg xmlns="http://www.w3.org/2000/svg" width="33" height="33" viewBox="0 0 33 33"> <g id="navigation_mini_menu" transform="translate(-182 -396.641)"> <rect id="Rectangle_2854" data-name="Rectangle 2854" width="33" height="33" rx="16.5" transform="translate(182 396.641)" fill="#000d9c"/> <g id="Group_1255" data-name="Group 1255" transform="translate(188.536 405.811)"> <rect id="Rectangle_2855" data-name="Rectangle 2855" width="16" height="3" rx="1.5" transform="translate(0.464 -0.17)" fill="#fff"/> <rect id="Rectangle_2856" data-name="Rectangle 2856" width="19" height="3" rx="1.5" transform="translate(0.464 6.83)" fill="#fff"/> <rect id="Rectangle_2857" data-name="Rectangle 2857" width="9" height="3" rx="1.5" transform="translate(0.464 12.83)" fill="#fff"/> </g> </g> </svg>
          </div>

          <div class="loree-table-hamburger-icon" id=${CONSTANTS.LOREE_CONTAINER_HAMBURGER_ICON} style="display: none">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 104 104"><g id="Layer_2" data-name="Layer 2"><g id="Layer_4" data-name="Layer 4"><path d="M98.48,0h-93A5.52,5.52,0,0,0,0,5.52V19.35a5.52,5.52,0,0,0,5.52,5.52h93A5.52,5.52,0,0,0,104,19.35V5.52A5.52,5.52,0,0,0,98.48,0ZM100,19.35a1.52,1.52,0,0,1-1.52,1.52h-93A1.52,1.52,0,0,1,4,19.35V5.52A1.52,1.52,0,0,1,5.52,4h93A1.52,1.52,0,0,1,100,5.52Z"/><path d="M98.48,79.13h-93A5.52,5.52,0,0,0,0,84.65V98.48A5.52,5.52,0,0,0,5.52,104h93A5.52,5.52,0,0,0,104,98.48V84.65A5.52,5.52,0,0,0,98.48,79.13ZM100,98.48A1.52,1.52,0,0,1,98.48,100h-93A1.52,1.52,0,0,1,4,98.48V84.65a1.52,1.52,0,0,1,1.52-1.52h93A1.52,1.52,0,0,1,100,84.65Z"/><polygon points="93.3 44.27 60.59 70.05 76.11 70.05 93.3 56.5 93.3 44.27"/><path d="M87.79,34H80.23L34.42,70.05H49.94L92.71,36.34A5.8,5.8,0,0,0,87.79,34Z"/><polygon points="10.7 59.73 43.41 33.95 27.89 33.95 10.7 47.5 10.7 59.73"/><path d="M93.3,65.71V64.9l-6.54,5.15h1C90.83,70.05,93.3,68.11,93.3,65.71Z"/><path d="M11.29,67.66a5.8,5.8,0,0,0,4.92,2.39h7.56L69.58,34H54.06Z"/><path d="M10.7,38.29v.81L17.24,34h-1C13.17,34,10.7,35.89,10.7,38.29Z"/></g></g></svg>
          </div>
        </div>
      </body>
     <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>
     <script src="/scripts/main.js"></script>
   </html>
`;

export default Bootstrap;
