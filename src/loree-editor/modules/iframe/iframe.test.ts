import { isImageWrapper } from '../../../loree-document/imageWrapper';
import Base from '../../base';
import { createDiv, createElement } from '../../common/dom';
import CONSTANTS from '../../constant';
import { appendElementToBody } from '../../utils';
import { showLinkDetails } from '../links/links';
import Iframe from './iframe';

jest.mock('../links/links', () => {
  return {
    showLinkDetails: jest.fn(),
  };
});

describe('#hideQuickLinkOption', () => {
  const IframeObject = new Iframe();
  describe('when text element is single clicked', () => {
    document.body.innerHTML = '';
    const doc = document.body;
    let selectionEvent: MouseEvent;
    const textWrapper = createElement('div', '', CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
    doc.appendChild(textWrapper);
    const textElement = createElement('p', '', CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT);
    textWrapper.appendChild(textElement);

    test('hideQuickLinkOption should be called', () => {
      textElement.addEventListener('click', function (e) {
        selectionEvent = e;
      });
      textElement.dispatchEvent(new Event('click'));
      const hideQuickLinkOption = jest.spyOn(IframeObject, 'hideQuickLinkOption');
      IframeObject.handleQuickLinkSelection(selectionEvent.target as HTMLElement);
      expect(hideQuickLinkOption).toBeCalledTimes(0);
    });
  });

  describe('#when text element is single clicked', () => {
    document.body.innerHTML = '';
    const doc = document.body;
    let selectionEvent: MouseEvent;
    const textWrapper = createElement('div', '', CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN);
    doc.appendChild(textWrapper);
    const textElementLinked = createElement('p', '', CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT);
    const anchorTagText = createElement('a') as HTMLAnchorElement;
    anchorTagText.href = '';
    textElementLinked.appendChild(anchorTagText);
    textWrapper.appendChild(textElementLinked);

    test('hideQuickLinkOption should not be called', () => {
      anchorTagText.addEventListener('click', function (e) {
        selectionEvent = e;
      });
      anchorTagText.dispatchEvent(new Event('click'));
      const hideQuickLinkOption = jest.spyOn(IframeObject, 'hideQuickLinkOption');
      IframeObject.handleQuickLinkSelection(selectionEvent.target as HTMLElement);
      expect(hideQuickLinkOption).toBeCalledTimes(1);
    });
  });
});

describe('#eventListeners', () => {
  const IframeObject = new Iframe();
  const baseObject = new Base();

  describe('span tag listeners', () => {
    const doc = document.body;
    const parentWrapper = document.createElement('div');
    parentWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN;
    doc.appendChild(parentWrapper);
    const ptag = document.createElement('p');
    ptag.id = 'test-paragraph';
    ptag.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    parentWrapper.appendChild(ptag);
    const spanTag = document.createElement('span');
    spanTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    spanTag.innerText = 'New Element';
    parentWrapper.appendChild(spanTag);

    describe('#single click event for non span element', () => {
      let event: MouseEvent;
      beforeAll(() => {
        ptag.addEventListener('click', function (e) {
          event = e;
          IframeObject.handleClickEvent(e);
        });
        ptag.dispatchEvent(new Event('click'));
      });

      beforeEach(() => {
        jest.resetAllMocks();
      });

      test('function for Url Popper is called once to close the popper', () => {
        IframeObject.handleClickEvent(event);
        expect(showLinkDetails).toHaveBeenCalled();
      });

      test('it must not be pasted content', () => {
        expect(IframeObject.checkIsPastedContent((event.target as HTMLElement).parentNode)).toEqual(
          false,
        );
      });

      test('parent must be a div tag', () => {
        expect(ptag.parentElement?.tagName).toEqual('DIV');
      });

      test('change selected element to be called', () => {
        const spyChangeSelectedElement = jest.spyOn(IframeObject, 'changeSelectedElement');
        IframeObject.handleClickEvent(event);
        expect(spyChangeSelectedElement).toHaveBeenCalled();
      });

      test('it must contains element-highlight class after change selected element', () => {
        expect(ptag).toHaveClass('loree-iframe-content-element element-highlight');
      });

      test('contenteditable attribute for tag must be null', () => {
        expect((event.target as HTMLElement).getAttribute('contentEditable')).toEqual(null);
      });

      test('handle contentEditable attribute function', () => {
        // add contentEditable attribute
        const spyChangeSelectedElement = jest.spyOn(IframeObject, 'handleContentEditable');
        IframeObject.handleClickEvent(event);
        expect(spyChangeSelectedElement).toHaveBeenCalled();
        const edit = jest.spyOn(baseObject, 'addContentEditableAttribute');
        baseObject.addContentEditableAttribute(event.target as HTMLElement);
        expect(edit).toHaveBeenCalled();
      });

      test('contenteditable attribute for tag must be true', () => {
        expect((event.target as HTMLElement).getAttribute('contentEditable')).toEqual('true');
      });
    });

    describe('#single click for span element', () => {
      let event: Event;
      beforeAll(() => {
        spanTag.addEventListener('click', function (e) {
          event = e;
          IframeObject.handleClickEvent(e);
        });
        spanTag.dispatchEvent(new Event('click'));
      });

      test('the whether tag name is SPAN', () => {
        expect((event.target as HTMLElement).tagName).toEqual('SPAN');
      });

      test('span tag converted to P tag', () => {
        jest.spyOn(baseObject, 'wrapInvalidTags');
        expect(doc.innerHTML).toEqual(
          '<div class="loree-iframe-content-column"><div id="test-paragraph" class="loree-iframe-content-image-wrapper"><img src="" class="loree-iframe-content-image"></div></div>',
        );
      });
    });
  });

  describe('#single click for img element', () => {
    const IframeObject = new Iframe();

    describe('#anchor tag append', () => {
      document.body.innerHTML = '';
      const doc = document.body;
      const parentWrapper = document.createElement('div');
      parentWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN;
      doc.appendChild(parentWrapper);
      const anchorTag = document.createElement('a');
      anchorTag.href = '';
      parentWrapper.appendChild(anchorTag);
      const divTag = document.createElement('div');
      divTag.id = 'test-wrapper';
      divTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
      anchorTag.appendChild(divTag);
      const imgTag = document.createElement('img');
      imgTag.src = '';
      imgTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE;
      divTag.appendChild(imgTag);

      beforeAll(() => {
        imgTag.addEventListener('click', function (e) {
          IframeObject.handleClickEvent(e);
        });
        imgTag.dispatchEvent(new Event('click'));
      });
      describe('incorrect image wrapper', () => {
        const div = document.createElement('div');
        const anchor = document.createElement('a');
        const img = document.createElement('img');

        beforeEach(() => {
          div.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
          div.appendChild(img);
          anchor.appendChild(div);
          document.body.appendChild(anchor);
        });
        test('when clicked it is repaired', () => {
          div.addEventListener('click', function (e) {
            IframeObject.handleClickEvent(e);
          });
          div.dispatchEvent(new Event('click'));
          expect(isImageWrapper(div)).toBeTruthy();
          expect(img.parentElement?.tagName).toEqual('A');
        });
      });
    });

    describe('#anchor tag not append', () => {
      let event: Event;

      document.body.innerHTML = '';
      const docBody = document.body;
      const parentWrapper = document.createElement('div');
      parentWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN;
      docBody.appendChild(parentWrapper);
      const divtag = document.createElement('div');
      divtag.id = 'test-paragraph';
      divtag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
      parentWrapper.appendChild(divtag);
      const imgTag = document.createElement('img');
      imgTag.src = '';
      imgTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE;
      divtag.appendChild(imgTag);

      beforeEach(() => {
        imgTag.addEventListener('click', function (e) {
          event = e;
          IframeObject.handleClickEvent(e);
        });
        imgTag.dispatchEvent(new Event('click'));
      });

      test('whether tag name is IMG', () => {
        expect((event.target as HTMLElement).tagName).toEqual('IMG');
      });

      test('whether parent tag name is not Anchor tag', () => {
        const element = event.target as HTMLElement | null;
        expect((element!.parentElement!.parentElement as HTMLElement).tagName).not.toEqual('A');
      });
    });
  });

  describe('#hideLangIsoevents', () => {
    let IframeObject = new Iframe();

    beforeEach(() => {
      IframeObject = new Iframe();
      document.body.innerHTML = '';
      const iframe = document.createElement('iframe');
      const iframeDocument = createDiv('loree-wrapper');
      iframeDocument.title = 'loree iframe';
      iframe.innerHTML = '';
      appendElementToBody(iframeDocument);
      iframe.id = CONSTANTS.LOREE_IFRAME;
      const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      wrapper.innerHTML = `<p class=${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}>Insert text here</p>`;
      iframe.appendChild(wrapper);
      document.body.appendChild(iframe);
      const iframeDoc = iframe.contentWindow?.document;
      iframeDoc?.open().write(iframe.innerHTML);
      iframeDoc?.close();
      IframeObject.getDocument = jest.fn().mockImplementation(() => iframe as HTMLElement);
      IframeObject.hideLanguageIsoLabel = jest.fn();
      IframeObject.showLanguageIsoLabel = jest.fn();
      IframeObject.handleSelectedContentChanges = jest.fn();
    });
    test('hide and show Language Iso Label on paste', () => {
      const event = {
        target: 'Text',
        preventDefault: jest.fn(),
      };
      IframeObject.getSelectedElement = jest
        .fn()
        .mockImplementation(() => IframeObject.getDocument()?.firstChild?.firstChild);
      IframeObject.handleContentPaste(event as unknown as ClipboardEvent);
      expect(IframeObject.hideLanguageIsoLabel).toBeCalledTimes(1);
      expect(IframeObject.showLanguageIsoLabel).toBeCalledTimes(1);
    });
  });
});
