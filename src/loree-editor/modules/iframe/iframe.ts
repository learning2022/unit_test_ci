import debounce from 'lodash.debounce';
import { ensure } from '../../../utils/generalUtils';
import CONSTANTS from '../../constant';
import { hideLinkEditPopup } from '../../document/linkEditPopup';
import Bootstrap from './templates/bootstrap';
import Base from '../../base';
import TextOptions from '../textOptions/UI/textOptionsUI';
import BlockOptions from '../blockOptions/blockOptions';
import BlockEditOptions from '../blockOptions/editOptions';
import EditorEvents from '../../editorEvents';
import { ImageModal } from '../imageModal/imageModal';
import { VideoModal } from '../videoModal/videoModal';
import { WordStyler } from '../textOptions/native-styler/wordStyler';
import {
  getIframedocumentElementsByClassName,
  getIframedocumentElementById,
  resizeSaveIconElement,
} from '../../utils';
import { init } from '../../a11y-client/init';
import { a11yGlobalContext } from '../../../a11y-service/context/a11y-context';
import Space from '../space/space';
import { a11yNotificationEvent } from '../../../a11y-service/context/a11y-tools';
import { getElementById } from '../../common/dom';
import ContainerDesign from '../sidebar/containerDesign';
import { showLanguageOption } from '../../base/baseUtil';
import { hideLinkInfoPopup } from '../../document/linkInfoPopup';
import { showLinkDetails } from '../links/links';
import { isImageWrapper, repairImageWrapper } from '../../../loree-document/imageWrapper';

class Iframe extends Base {
  textOptions: TextOptions;
  blockOptions: BlockOptions;
  blockEditOptions: BlockEditOptions;
  editorEvents: EditorEvents;
  space: Space;
  keyStatus = 0;
  adminConfig: _Any = {};

  constructor() {
    super();
    this.textOptions = new TextOptions();
    this.blockOptions = new BlockOptions();
    this.blockEditOptions = new BlockEditOptions();
    this.editorEvents = new EditorEvents();
    this.space = new Space();
  }

  initiate = (): void => {
    this.attachIframe();
  };

  attachIframe = (): void => {
    this.wordStylerObj = new WordStyler();
    this.containerDesign = new ContainerDesign();
    const space = document.getElementById(CONSTANTS.LOREE_SPACE);
    if (space) {
      const iframe: HTMLIFrameElement = document.createElement('iframe');
      iframe.id = CONSTANTS.LOREE_IFRAME;
      iframe.className = 'loreeIframe';
      space.appendChild(iframe);
    }
  };

  imageClass = (): _Any => {
    const imageModal = new ImageModal();
    return imageModal;
  };

  videoClass = (): _Any => {
    const videoModal = new VideoModal();
    return videoModal;
  };

  attachIframeContent = (config: _Any): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      iframeDocument.open();
      iframeDocument.write(Bootstrap);
      iframeDocument.close();
      this.textOptions.initiate(config);
      this.blockOptions.initiate(config.features);
      this.blockEditOptions.initiate(config);
      this.adminConfig = config;
      this.space.attachHelpIcon();
    }
  };

  attachCustomFonts = (config: _Any) => {
    const iframe = document.getElementById('loree-iframe') as HTMLIFrameElement;
    const iframePreViewPort = document.getElementById('loree-preview') as HTMLIFrameElement;
    const fonts = config.customFonts?.fonts;
    if (fonts && fonts.length > 0) {
      let fontApiUrl = 'https://fonts.googleapis.com/css2?';
      fonts.forEach((font: _Any) => {
        if (Object.prototype.hasOwnProperty.call(font, 'url')) {
          const api = font.url.split('=');
          fontApiUrl += `&family=${api[1]}`;
        }
      });
      const linkTag = document.createElement('link');
      linkTag.setAttribute('rel', 'stylesheet');
      linkTag.setAttribute('href', fontApiUrl);
      linkTag.setAttribute('id', 'customFontsLink');
      iframe.contentDocument?.head.appendChild(linkTag);
      const editorPreview = linkTag.cloneNode(true);
      (editorPreview as HTMLElement).removeAttribute('id');
      (editorPreview as HTMLElement).setAttribute('id', 'previewCustomFontsLink');
      iframePreViewPort.contentDocument?.head.appendChild(editorPreview);
    }
  };

  addIconClickIfPresent = (iframeDocument: _Any, elementId: string, onClickHandler: _Any) => {
    const icon = getIframedocumentElementById(iframeDocument, elementId);
    icon.onclick = () => onClickHandler();
    return icon;
  };

  attachEventListeners = (): void => {
    const iframeWindow = this.getIframeWindow();
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;

    resizeSaveIconElement(iframeWindow);

    iframeDocument.getElementById(CONSTANTS.LOREE_AXE_SCRIPT)?.addEventListener('load', () => {
      if (iframeWindow) {
        init(iframeWindow.axe);
        if (process.env.REACT_APP_A11Y_CHECKER_STATE === 'true') {
          void a11yNotificationEvent();
        }
      }
    });

    // Listen for changes to the iframe content so we can update the a11y highlights
    // todo also listen for dom changes using MutationObserver: https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
    iframeDocument.addEventListener(
      'input',
      // debounce the handler to not slow down keyboard input
      debounce(() => {
        const { updateHighlights, activeStep } = a11yGlobalContext.value;
        if (activeStep >= 0) {
          updateHighlights();
        }
      }, 50),
    );

    iframeDocument.addEventListener('keydown', (event) => {
      if (event.key === 'Enter') {
        iframeDocument.execCommand('insertHTML', false, '');
      }
      if (showLanguageOption(this.getSelectedElement())) {
        this.hideLanguageIsoLabel();
        this.showLanguageIsoLabel(this.getSelectedElement() as HTMLElement);
      }
    });

    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_ADD_ROW_BUTTON,
      this.handleAddRowButtonClick,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_CLOSE_ROW_BUTTON,
      this.handleCloseRowButton,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_ADD_ELEMENT_BUTTON,
      this.handleAddElementButtonClick,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_CLOSE_ADD_ELEMENT_BUTTON,
      this.handleCloseAddElementButtonClick,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_INSERT_ROW_BUTTON,
      this.handleInsertRowButtonClick,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON,
      this.handleCloseInsertRowButtonClick,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_ADD_ROW_BUTTON_FOR_CONTAINER,
      this.handleAddRowButtonClickOfContainer,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_CLOSE_ROW_BUTTON_FOR_CONTAINER,
      this.handleCloseRowButtonOfContainer,
    );
    this.addIconClickIfPresent(
      iframeDocument,
      CONSTANTS.LOREE_SPACE_HELP_ICON_BUTTON,
      this.hideAnchorOption,
    );

    // const contentWrapper = getIFrameElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    const contentWrapper = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
    ) as HTMLIFrameElement;
    let enableMouseMove = false;
    let movestart = false;
    if (contentWrapper) {
      contentWrapper.addEventListener('click', (event: _Any): void => {
        if (event.shiftKey) {
          // Shift-Click
          this.handleMouseDoubleClick(event);
          this.hideBlockOptions();
        }

        if (enableMouseMove) {
          enableMouseMove = false;
          return;
        }
        this.eventDetails = event.detail;
        switch (event.detail) {
          // single click event will be event.detail === 1
          case 1:
            this.handleClickEvent(event);
            break;
          // lly double click @ event.detail === 2
          case 2:
            this.handleMouseDoubleClick(event);
            this.hideBlockOptions();
            break;
          // lly triple click @ event.detail === 3
          case 3: {
            const selectedText = this.getDocument()?.getSelection() as Selection;
            this.handleTripleClickEvent(event, selectedText);
            this.hideBlockOptions();
            break;
          }
        }
      });

      contentWrapper.addEventListener('keyup', (e: _Any): void => {
        this.handleSelectionOnArrowKey(e);
        this.hideBlockOptions();
      });

      contentWrapper.addEventListener('keydown', (e: KeyboardEvent) => {
        if (e.key === 'Backspace') {
          this.onKeyBackspace();
        }

        if (e.ctrlKey || e.metaKey) {
          switch (e.key) {
            case 'b':
              e.preventDefault();
              this.onKeyControlB();
              break;
            case 'i':
              e.preventDefault();
              this.onKeyControlI();
              break;
            case 'u':
              e.preventDefault();
              this.onKeyControlU();
              break;
            case 'a':
              this.onKeyControlA();
              break;
          }
        }

        if (e.keyCode !== 13) return;
        if (!this.isListItemExist()) {
          this.editorEvents.handleKeyEvents(e);
        } else this.isListTag(e);
      });

      // iframeDocument.addEventListener('selectionchange', () => {
      //   this.onSelectionChanged();
      // });

      contentWrapper.addEventListener('paste', (event: ClipboardEvent): void => {
        this.hideTextOptions();
        this.handleContentPaste(event);
      });

      contentWrapper.addEventListener('mouseover', (e: _Any): void => {
        this.handleMouseOver(e);
      });

      contentWrapper.addEventListener('mouseout', (e: _Any): void => {
        this.handleMouseOut(e);
      });

      contentWrapper.addEventListener('mousedown', (event: _Any): void => {
        movestart = true;
        if (enableMouseMove) this.handleClickEvent(event);
      });
      contentWrapper.addEventListener('mousemove', (event: _Any): void => {
        enableMouseMove = true;
        const selectedElement = event?.target as HTMLElement;
        // show text options on drag if the selected element is an element
        if (
          movestart &&
          selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)
        ) {
          this.removeHiddenQuickLinkInsertLink();
          this.handleQuicklinkInsertLinkForLists(false);
          this.handleHeaderStyleOption();
          this.initiateTextOptionsPopper();
          this.hideBlockOptions();
        }
      });
      contentWrapper.addEventListener('mouseup', (): void => {
        movestart = false;
      });
    }
  };

  // private onSelectionChanged() {
  //   const sel = this.getDocument()?.getSelection();
  //   if (sel) {
  //     const div = getAsElementWithTag('div', sel.anchorNode);
  //   }
  //
  //   this.hideBlockOptions();
  //   if (!sel || sel.isCollapsed) {
  //     this.hideTextOptions();
  //     return;
  //   }
  //   this.removeHiddenQuickLinkInsertLink();
  //   this.handleQuicklinkInsertLinkForLists(false);
  //   this.initiateTextOptionsPopper();
  //
  //   // const range = sel.getRangeAt(0);
  //   // this.textOptions.updateTextOptionsFromRange(range);
  // }

  private onKeyControlA() {
    this.removeHiddenQuickLinkInsertLink();
    this.handleQuicklinkInsertLinkForLists(true);
    this.handleHeaderStyleOption();
    this.modifyOverallSelectionRange();
    this.initiateTextOptionsPopper();
  }

  private onKeyBackspace() {
    this.hideTextOptions();
    this.hideBlockOptions();
    const focusElement = this.getDocument()?.getSelection();
    const nodeType = focusElement?.anchorNode?.nodeName;
    const itemElement = focusElement?.anchorNode?.parentElement;
    if (nodeType === 'LI' && itemElement?.childElementCount === 1) {
      itemElement?.parentElement?.removeChild(itemElement);
    }
  }

  private onKeyControlB() {
    const iframe = this.getDocument();
    const button = ensure(iframe?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_BOLD_BUTTON));
    const styleValue = (button?.childNodes[0] as HTMLElement).classList.contains(
      'text-options-icon-highlight',
    )
      ? 'normal'
      : 'bold';
    this.wordStylerObj?.boldStyleChange(styleValue);
    (button.childNodes[0] as HTMLElement).classList.toggle('text-options-icon-highlight');
  }

  private onKeyControlI() {
    const iframe = this.getDocument();
    const button = ensure(iframe?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_ITALIC_BUTTON));
    const styleValue = (button.childNodes[0] as HTMLElement).classList.contains(
      'text-options-icon-highlight',
    )
      ? 'normal'
      : 'italic';
    this.wordStylerObj?.italicStyleChange(styleValue);
    (button.childNodes[0] as HTMLElement).classList.toggle('text-options-icon-highlight');
  }

  private onKeyControlU() {
    const iframe = this.getDocument();

    const button = ensure(iframe?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_UNDERLINE_BUTTON));
    const styleValue = (button.childNodes[0] as HTMLElement).classList.contains(
      'text-options-icon-highlight',
    )
      ? 'none'
      : 'underline';
    this.wordStylerObj?.textDecorationChange(styleValue);
    this.wordStylerObj?.removeExistingTextDecorations('underline');
  }

  handleTripleClickEvent = (event: MouseEvent, selectedText: Selection) => {
    const range = document.createRange();
    const anchorNode = this.elementPicker.getElement(
      selectedText?.anchorNode as HTMLElement,
      'tripleClickEventListener',
    );
    if (!anchorNode) return;
    range.selectNodeContents(anchorNode as Node);
    selectedText?.removeAllRanges();
    selectedText?.addRange(range);
    this.handleMouseDoubleClick(event);
  };

  modifyOverallSelectionRange = () => {
    const iframeDocument: _Any = this.getDocument();
    const selectedText: _Any = iframeDocument?.getSelection() as Selection;
    let anchorNode = selectedText?.anchorNode;
    const range = document.createRange();
    anchorNode = this.elementPicker.getElement(anchorNode, 'overAllSelection');
    range.selectNodeContents(anchorNode);
    selectedText?.removeAllRanges();
    selectedText?.addRange(range);
  };

  handleHeaderStyleOption = () => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const AstyleOption = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON,
    ) as HTMLElement;
    const isListElement = this.isListItemExist();
    if (isListElement && !this.isClassContains(AstyleOption, 'd-none')) {
      AstyleOption.classList.add('d-none');
    } else if (!isListElement && this.isClassContains(AstyleOption, 'd-none')) {
      AstyleOption.classList.remove('d-none');
    }
  };

  handleQuicklinkInsertLinkForLists = (isPartial: boolean) => {
    const iframeDocument: _Any = this.getDocument();
    const selectedText: _Any = iframeDocument?.getSelection() as Selection;
    const quickLinkWrapper = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_WRAPPER,
    ) as HTMLElement;
    const insertLinkWrapper = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON,
    ) as HTMLElement;
    const isListElement = this.isListItemExist();
    if (!isListElement) return;
    this.hideOptions(isPartial, selectedText, quickLinkWrapper, insertLinkWrapper);
  };

  hideOptions = (
    isPartial: boolean,
    selectedText: _Any,
    quickLinkWrapper: HTMLElement,
    insertLinkWrapper: HTMLElement,
  ) => {
    if (
      isPartial
        ? selectedText?.anchorNode?.parentElement?.parentElement?.childElementCount > 1 &&
          !this.isClassContains(quickLinkWrapper, 'd-none')
        : this.wordStylerObj?.isMultipleListItemSelected(selectedText) &&
          !this.isClassContains(quickLinkWrapper, 'd-none')
    ) {
      quickLinkWrapper.classList.add('d-none');
    }
    if (
      isPartial
        ? selectedText.anchorNode.parentElement.parentElement.childElementCount > 1 &&
          !this.isClassContains(insertLinkWrapper, 'd-none')
        : this.wordStylerObj?.isMultipleListItemSelected(selectedText) &&
          !this.isClassContains(insertLinkWrapper, 'd-none')
    ) {
      insertLinkWrapper.classList.add('d-none');
    }
  };

  isClassContains = (element: HTMLElement, className: string): boolean =>
    element?.classList ? element.classList.contains(className) : false;

  removeHiddenQuickLinkInsertLink = () => {
    const iframeDocument: _Any = this.getDocument();
    const quickLinkWrapper = document?.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_WRAPPER,
    ) as HTMLElement;
    const insertLinkWrapper = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON,
    ) as HTMLElement;
    quickLinkWrapper?.classList.remove('d-none');
    insertLinkWrapper?.classList.remove('d-none');
  };

  handleSelectionOnArrowKey = (e: _Any) => {
    const arrowCode: number[] = [35, 36, 37, 38, 39, 40];
    if (arrowCode.includes(e.keyCode)) {
      this.hideTextOptions();
      const range = this.getDocument()?.getSelection()?.getRangeAt(0) as Range;
      this.setPreviousSelection(range);
      this.initiateTextOptionsPopper();
    }
  };

  isListItemExist = (): boolean => {
    const selected = this.getDocument()?.getSelection();
    let element = selected?.anchorNode as HTMLElement;
    while (
      element &&
      element.tagName !== 'BODY' &&
      !element?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) &&
      this.elementPicker.checkTagName(element)
    ) {
      if (element?.tagName === 'LI') {
        return true;
      }
      if (element?.parentElement) element = element.parentElement;
    }
    return false;
  };

  isListTag = (e: _Any) => {
    const fontProperty = this.adminConfig.customHeaderStyle.customHeader[6].paragraph;
    const element = e.target as HTMLElement;
    if (element.tagName === 'UL' || element.tagName === 'OL') {
      if (element.lastElementChild?.textContent === '') {
        e.preventDefault();
        element.lastElementChild?.remove();
        element.removeAttribute('contenteditable');
        const pTag = document.createElement('p');
        pTag.setAttribute('contenteditable', 'true');
        pTag.className = 'loree-iframe-content-element';
        pTag.setAttribute(
          'style',
          `border-width: 0; border-style: solid; border-color: #000000;color: #000000; padding: 5px; margin:0 0 10px; font-family:${fontProperty.font}; font-size:${fontProperty.size}`,
        );
        pTag.innerHTML = '';
        element.parentElement?.removeAttribute('contenteditable');
        element.insertAdjacentElement('afterend', pTag);
        const nextElement: _Any = element.nextElementSibling;
        nextElement.focus();
      }
    }
    if (element.tagName === 'P') {
      if ((element.childNodes[0] as HTMLElement).lastElementChild?.textContent === '') {
        e.preventDefault();
        (element.childNodes[0] as HTMLElement).lastElementChild?.remove();
        element.removeAttribute('contenteditable');
        const span = document.createElement('span');
        span.setAttribute('contenteditable', 'true');
        span.className = 'loree-iframe-content-element d-block';
        span.innerHTML = '';
        element.parentElement?.removeAttribute('contenteditable');
        element.insertAdjacentElement('beforeend', span);
        const spanElement: _Any = element.lastElementChild;
        spanElement.focus();
      }
    }
  };

  isArrowKeyCode = (keyCode: number): boolean => {
    const arrowCode: number[] = [37, 38, 39, 40];
    return arrowCode.includes(keyCode);
  };

  checkIsPastedContent = (selectedElement: _Any) => {
    if (
      selectedElement?.parentElement?.classList.contains('loree-iframe-content-copy-paste') ||
      selectedElement?.parentElement?.classList.contains('loree-iframe-content-element')
    )
      return true;
    else if (
      selectedElement?.parentElement?.parentElement?.classList.contains(
        'loree-iframe-content-copy-paste',
      ) ||
      selectedElement?.parentElement?.parentElement?.classList.contains(
        'loree-iframe-content-element',
      )
    )
      return true;
    else if (
      selectedElement?.parentElement?.parentElement?.parentElement?.classList.contains(
        'loree-iframe-content-copy-paste',
      ) ||
      selectedElement?.parentElement?.parentElement?.parentElement?.classList.contains(
        'loree-iframe-content-element',
      )
    )
      return true;
    else return false;
  };

  processClickEvent = (
    eventTarget: HTMLElement,
    metaKey: boolean,
    ctrlKey: boolean,
    shiftKey: boolean,
  ): void => {
    let selectedElement = eventTarget;

    if (isImageWrapper(selectedElement)) {
      repairImageWrapper(selectedElement);
      selectedElement.removeAttribute('contenteditable');
    }

    selectedElement = this.wrapInvalidTags(selectedElement);
    hideLinkInfoPopup();
    hideLinkEditPopup();

    if (selectedElement) {
      showLinkDetails(selectedElement);
      const result = this.checkIsPastedContent(selectedElement.parentElement);
      if (result) this.addContentEditableAttribute(selectedElement);
      if (
        selectedElement.parentElement?.tagName === 'A' &&
        selectedElement.tagName !== 'DIV' &&
        selectedElement.tagName !== 'IMG'
      ) {
        const currentElement = this.elementPicker.getElement(selectedElement, 'handleClickEvent');
        this.addContentEditableAttribute(currentElement);
      }
    }

    if (selectedElement.tagName === 'LI' && selectedElement.hasAttribute('contenteditable')) {
      selectedElement.removeAttribute('contenteditable');
    }
    if (
      !selectedElement.hasAttribute('contenteditable') &&
      !selectedElement.parentElement?.hasAttribute('contenteditable')
    ) {
      this.changeSelectedElement(selectedElement);
      this.handleQuickLinkSelection(eventTarget);
      this.handleContentEditable(selectedElement);
    } else if (
      selectedElement.hasAttribute('contenteditable') ||
      selectedElement.parentElement?.hasAttribute('contenteditable')
    ) {
      this.hideTextOptions();
      this.changeSelectedElement(selectedElement);
      this.handleQuickLinkSelection(eventTarget);
    }
    this.handleSuperKeysClick(metaKey, ctrlKey, shiftKey);
  };

  handleClickEvent = (event: MouseEvent | null): void => {
    if (event?.target) {
      this.processClickEvent(
        event.target as HTMLElement,
        event.metaKey,
        event.ctrlKey,
        event.shiftKey,
      );
    }
    this.hideSubSidebar();
  };

  handleQuickLinkSelection = (eventTarget: HTMLElement): void => {
    const anchorNode = this.elementPicker.getElement(eventTarget, 'urlLinkPopperEvents');
    const isTextElement = anchorNode?.parentElement?.classList.contains(
      CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT,
    );
    if (!isTextElement) return;
    if (anchorNode && anchorNode.tagName === 'A') {
      this.hideQuickLinkOption();
    }
  };

  hideQuickLinkOption = (): void => {
    const quickLinkOption = getElementById(CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_HEADING);
    if (!quickLinkOption) return;
    quickLinkOption.style.display = 'none';
  };

  handleMouseDoubleClick = (event: MouseEvent): void => {
    if (event?.target) {
      const selectedElement = event.target as HTMLElement;
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        this.changeSelectedElement(selectedElement);
        this.removeHiddenQuickLinkInsertLink();
        this.handleHeaderStyleOption();
        this.initiateTextOptionsPopper();
      } else if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE)) {
        event.preventDefault();
        this.imageClass().replaceImageUploadHandler();
      } else if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO)) {
        event.preventDefault();
        this.videoClass().replaceVideoUploadHandler();
      } else if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE)) {
        event.preventDefault();
        const iframeDocument = this.getDocument();
        if (!iframeDocument) return;
        const copyPasteElements = getIframedocumentElementsByClassName(
          iframeDocument,
          CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE,
        );
        Array.from(copyPasteElements).forEach((copyPasteElement: _Any) => {
          copyPasteElement.removeAttribute('contenteditable');
        });
        this.addContentEditableAttribute(selectedElement);
      }
    }
  };

  handleMouseOver = (event: MouseEvent): void => {
    const element = event.target as HTMLElement;
    if (element) {
      let type = 'other';
      if (element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO)) {
        type = 'video';
      }
      if (element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)) {
        type = 'row';
      }
      if (element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) {
        type = 'column';
      }
      if (
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_H5P_WRAPPER) ||
        element.tagName === 'TD'
      ) {
        type = 'element';
      }
      switch (type) {
        case 'row':
          element.classList.add(CONSTANTS.LOREE_ROW_HOVER_HIGHLIGHT);
          break;
        case 'column':
          element.classList.add(CONSTANTS.LOREE_COLUMN_HOVER_HIGHLIGHT);
          break;
        case 'element':
          element.classList.add(CONSTANTS.LOREE_ELEMENT_HOVER_HIGHLIGHT);
          break;
      }
    }
  };

  handleMouseOut = (event: MouseEvent): void => {
    const element = event.target as HTMLElement;
    if (element) {
      let type = 'other';
      if (element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO)) {
        type = 'video';
      }
      if (element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_ROW)) {
        type = 'row';
      }
      if (element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN)) {
        type = 'column';
      }
      if (
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_COPY_PASTE) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER) ||
        element.className.includes(CONSTANTS.LOREE_IFRAME_CONTENT_H5P_WRAPPER) ||
        element.tagName === 'TD'
      ) {
        type = 'element';
      }
      switch (type) {
        case 'row':
          element.classList.remove(CONSTANTS.LOREE_ROW_HOVER_HIGHLIGHT);
          break;
        case 'column':
          element.classList.remove(CONSTANTS.LOREE_COLUMN_HOVER_HIGHLIGHT);
          break;
        case 'element':
          element.classList.remove(CONSTANTS.LOREE_ELEMENT_HOVER_HIGHLIGHT);
          break;
      }
    }
  };
}

export default Iframe;
