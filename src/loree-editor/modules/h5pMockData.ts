export const h5pMocks = {
  canvasCourseDetails: {
    data: {
      courseDetails: `{
        "statusCode":200,
        "body":
          {
            "account": {"id":1,
            "name": "School of Arts"}}
        }`,
    },
  },
  listUser: {
    data: {
      listHFivePUserss: {
        items: `{
          "statusCode":200,
          "body":
            {
              "account": {"id":1,
              "name": "School of Arts"}}
          }`,
      },
    },
  },
  h5PContents: {
    data: {
      getH5PContent: `{
          "statusCode":200,
          "body":{
            "current_user": "129",
            "h5pContents":[{
              "account_id": "7",
              "created_user": "129",
              "id": "223",
              "tags": "Cloud,rain,candy",
              "title": "Course Shell1",
              "updated_at": "2021-08-13 04:45:03"
            }]
          }
        }`,
    },
  },
};

export const h5pContentData = {
  data: {
    getH5PContent:
      '{"statusCode":200,"body":{"status":1,"message":"","current_user":"92","h5pContents":[{"id":"35","title":"My Accordion","created_user":"65","tags":null,"updated_at":"2020-12-01 17:12:07","account_id":null}]}}',
  },
};
