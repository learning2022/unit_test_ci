/* eslint-disable */ // Remove this line when editing this file
import { interactiveContent, interativeLibrary, interactiveContentPaginate } from '../../graphql/queries';
import { initiateInteractive, verifyInteractive } from '../../graphql/mutations';
import { API, graphqlOperation, Auth } from 'aws-amplify';
import { interactiveEncrypt } from '../../utils/interactiveEncrypt';

let interactiveLists: any;
let encryptedList: any;
let liLibraryDetail: any;
export let verifyApiData: any;
export const elementData: any[] = [];
export let lintContent: any[] = [];
export let lintPaginateContent: any[];
export let libraryData = false;
export let initiateData = false;
export let encryptData = false;
export let contentData = false;
export const contentPaginateData = false;

//get method library api
export const libraryApi = async () => {
  let libraryApi: any = await API.graphql(graphqlOperation(interativeLibrary, { userId: '' }));
  libraryApi = JSON.parse(libraryApi.data.interativeLibrary);
  liLibraryDetail = libraryApi.body;
  libraryData = true;
  if (libraryApi.statusCode === 200) {
    return liLibraryDetail;
  } else {
    console.log('error in fetching library');
    return 'error';
  }
};

//initiate interactive api
export const inititateApi = async () => {
  const user = await Auth.currentAuthenticatedUser().then(user => {
    return user;
  });
  const ltiPlatformId = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformId) {
    let inititateApi: any = await API.graphql(graphqlOperation(initiateInteractive, {type: 'CANVAS' }));
    inititateApi = JSON.parse(inititateApi.data.initiateInteractive);
    interactiveLists = inititateApi;
    return interactiveLists;
  } else {
    // For now, Organization we don't use, In future will change to dynamically
    let inititateApi: any = await API.graphql(
      graphqlOperation(initiateInteractive, {
        type: 'STANDALONE',
        name: user.attributes.name,
        email: user.attributes.email,
        organization: 'crystaldelta.instructure.com',
      }),
    );
    inititateApi = JSON.parse(inititateApi.data.initiateInteractive);
    interactiveLists = inititateApi;
    initiateData = true;
    if (inititateApi.statusCode === 200) {
      return interactiveLists;
    } else {
      console.log('error in fetching library');
      return 'error';
    }
  }
};

//encrypte api
export const encryptedApi = async () => {
  const encryptedData: any = await interactiveEncrypt(interactiveLists);
  encryptedList = encryptedData;
  encryptData = true;
  return encryptedList;
};

export const verifyApi = async () => {
  let verifyApi: any = await API.graphql(
    graphqlOperation(verifyInteractive, {
      user: encryptedList.user_encrypt,
      orgId: encryptedList.org_id_encrypt,
    }),
  );
  verifyApi = JSON.parse(verifyApi.data.verifyInteractive);
  verifyApiData = verifyApi;
  if (verifyApi.statusCode === 200) {
    return verifyApiData;
  } else {
    return 'error';
  }
};

// Get method content api
export const contentApi = async () => {
  const user = await Auth.currentAuthenticatedUser();
  const ltiPlatformId = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformId) {
    let inititateApi: any = await API.graphql(graphqlOperation(initiateInteractive, { type: 'CANVAS' }));
    inititateApi = JSON.parse(inititateApi.data.initiateInteractive);
    interactiveLists = inititateApi;
  } else {
    // For now, Organization we don't use, In future will change to dynamically
    let inititateApi: any = await API.graphql(
      graphqlOperation(initiateInteractive, {
        type: 'STANDALONE',
        name: user.attributes.name,
        email: user.attributes.email,
        organization: 'crystaldelta.instructure.com',
      }),
    );
    inititateApi = JSON.parse(inititateApi.data.initiateInteractive);
    interactiveLists = inititateApi;
  }
  const encryptedData: any = await interactiveEncrypt(interactiveLists);
  encryptedList = encryptedData;
  let verifyApi: any = await API.graphql(
    graphqlOperation(verifyInteractive, {
      user: encryptedList.user_encrypt,
      orgId: encryptedList.org_id_encrypt,
    }),
  );
  verifyApi = JSON.parse(verifyApi.data.verifyInteractive);
  verifyApiData = verifyApi;
  let contentApi: any = await API.graphql(
    graphqlOperation(interactiveContent, { userId: verifyApiData.body.user, orgId: verifyApiData.body.org_id }),
  );
  contentApi = JSON.parse(contentApi.data.interactiveContent);
  lintContent = contentApi.body;
  contentData = true;
  if (contentApi.statusCode === 200) {
    return lintContent;
  } else {
    return 'error';
  }
};

//Get method content paginate api
export const contentPaginateApi = async (number: number) => {
  const user = await Auth.currentAuthenticatedUser();
  const ltiPlatformId = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformId) {
    let inititateApi: any = await API.graphql(graphqlOperation(initiateInteractive, { type: 'CANVAS' }));
    inititateApi = JSON.parse(inititateApi.data.initiateInteractive);
    interactiveLists = inititateApi;
  } else {
    // For now, Organization we don't use, In future will change to dynamically
    let inititateApi: any = await API.graphql(
      graphqlOperation(initiateInteractive, {
        type: 'STANDALONE',
        name: user.attributes.name,
        email: user.attributes.email,
        organization: 'crystaldelta.instructure.com',
      }),
    );
    inititateApi = JSON.parse(inititateApi.data.initiateInteractive);
    interactiveLists = inititateApi;
  }
  const encryptedData: any = await interactiveEncrypt(interactiveLists);
  encryptedList = encryptedData;
  let verifyApi: any = await API.graphql(
    graphqlOperation(verifyInteractive, {
      user: encryptedList.user_encrypt,
      orgId: encryptedList.org_id_encrypt,
    }),
  );
  verifyApi = JSON.parse(verifyApi.data.verifyInteractive);
  verifyApiData = verifyApi;
  const page = number;
  const limit = 10;
  let contentPaginateApi: any = await API.graphql(
    graphqlOperation(interactiveContentPaginate, {
      userId: verifyApi.body.user,
      orgId: verifyApi.body.org_id,
      pageId: page,
      pageLimit: limit,
    }),
  );
  contentPaginateApi = JSON.parse(contentPaginateApi.data.interactiveContentPaginate);
  lintPaginateContent = contentPaginateApi.body;
  // contentPaginateData = true;
  if (contentPaginateApi.statusCode === 200) {
    return lintPaginateContent;
  } else {
    return 'error';
  }
};
