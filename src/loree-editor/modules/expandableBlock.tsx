import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Collapse } from 'react-bootstrap';

import { Loader } from '../../components/loaderV2/loader';
import { createDiv, createParagraph } from '../common/dom';
import { videoModalIcons } from './sidebar/iconHolder';
import s from './expandableBlock.module.scss';

interface ExpandableBlockProps {
  title: string;
  interactiveId: string;
  headingId: string;
  contentId: string;
  otherContentId: string;
  contentParent: string;
  initialExpand: boolean;
}
const CHAR_CODE_SPACE = 32;
export const ExpandableBlock = ({
  title,
  interactiveId,
  headingId,
  contentId,
  otherContentId,
  contentParent,
  initialExpand = false,
}: ExpandableBlockProps): JSX.Element => {
  const [expanded, setExpanded] = useState(initialExpand);

  const handleMouseEvent = () => setExpanded(!expanded);
  const handkeKeyboardEvent = (e: React.KeyboardEvent) => {
    if (e.charCode === CHAR_CODE_SPACE) {
      setExpanded(!expanded);
    }
  };

  return (
    <div id={interactiveId}>
      <div // accordionHeading
        id={headingId}
        className={`accordion-sub-text design-section-font ${s.accordionIconStructure} mb-2`}
        onClick={handleMouseEvent}
        onKeyPress={handkeKeyboardEvent}
        role='button'
        tabIndex={0}
        aria-expanded={expanded}
        aria-controls={contentId}
        data-interactiveblockheading='true'
      >
        <div>
          <span className='accordion-icon block-accordion'>{expanded ? 'I' : '+'}</span>
        </div>
        <div className={s.accordionIconTitleStructure}>
          <span>{title}</span>
        </div>
      </div>

      <Collapse in={expanded}>
        <div // accordionContent
          id={contentId}
          className='fourSideMargin-2 design-section-font custom-scrollbar'
          aria-hidden={!expanded} // TODO should this be here if we are using Collapse
          role='list'
          data-parent={contentParent}
          data-testid={contentId}
        >
          <div id={otherContentId}>
            <Loader />
          </div>
        </div>
      </Collapse>
    </div>
  );
};

export const buildInteractiveBlockV2 = (
  title: string,
  interactiveId: string,
  headingId: string,
  contentId: string,
  otherContentId: string,
  contentParent: string,
  initialExpand: boolean = false,
): HTMLElement => {
  const wrapper = createDiv(`${interactiveId}Wrapper`, 'success');
  ReactDOM.render(
    React.createElement(ExpandableBlock, {
      title,
      interactiveId,
      headingId,
      contentId,
      otherContentId,
      contentParent,
      initialExpand: initialExpand,
    }),
    wrapper,
  );
  return wrapper;
};

/** @deprecated */
export const renderLoaderElement = (): string => {
  return `
    <div id="modal-loader" class="m-auto justify-content-center">
      <div class="icon rotating">
        ${videoModalIcons.loader}
      </div>
      <div class="title ml-3">Loading...</div>
    </div>
  `;
};

/** @deprecated */
export const buildInteractiveBlock = (
  title: string,
  interactiveId: string,
  headingId: string,
  contentId: string,
  otherContentId: string,
  contentParent: string,
  expanded: boolean = false,
): HTMLElement => {
  const interactiveBlock = createDiv(interactiveId);
  const accordionHeading = createParagraph(
    headingId,
    'accordion-sub-text design-section-font mb-2',
  );
  accordionHeading.dataset.toggle = 'collapse';
  accordionHeading.dataset.target = `#${contentId}`;
  accordionHeading.dataset.interactiveBlockHeading = 'true';
  accordionHeading.addEventListener('click', handleAccordionHeadingClickOrKeyPress);

  accordionHeading.addEventListener('keypress', (e) => {
    if (e.code !== 'Space') return;
    (e.currentTarget as HTMLElement).click();
  });

  accordionHeading.setAttribute('role', 'button');
  accordionHeading.setAttribute('tabindex', '0');
  accordionHeading.setAttribute('aria-expanded', expanded.toString());
  accordionHeading.setAttribute('style', 'display: flex; flex-wrap: wrap;');
  accordionHeading.innerHTML = `
    <div>
      <span class="accordion-icon block-accordion">${expanded ? 'I' : '+'}</span>
    </div>
    <div style="margin-left: 10px; flex: 1 1 35px;">
      <span>${title}</span>
    </div>
  `;
  interactiveBlock.appendChild(accordionHeading);

  const accordionContent = createDiv(contentId);
  accordionContent.className = `${
    expanded ? 'show' : ''
  } collapse fourSideMargin-2 design-section-font custom-scrollbar`;
  accordionContent.dataset.parent = `#${contentParent}`;
  accordionContent.innerHTML = `<div id="${otherContentId}">${renderLoaderElement()}</div>`;
  interactiveBlock.appendChild(accordionContent);

  return interactiveBlock;
};

/** @deprecated */
const handleAccordionHeadingClickOrKeyPress = (e: MouseEvent | KeyboardEvent) => {
  const accordionIcons = document.getElementsByClassName('block-accordion');
  for (const icon of accordionIcons) {
    icon.innerHTML = icon.innerHTML.replace('I', '+');
  }
  const targetElementParent = e.target as HTMLElement;
  let targetElement: _Any;
  console.log(targetElementParent.tagName);
  if (targetElementParent.tagName === 'SPAN')
    targetElement = targetElementParent.parentElement?.parentElement;
  else if (targetElementParent.tagName === 'DIV')
    targetElement = targetElementParent.parentElement as HTMLElement;
  else targetElement = targetElementParent;
  console.log('TARGET', targetElement);
  if (!targetElement) return;
  const childNode = targetElement.children[0].children[0];
  setTimeout(() => {
    let validateVal;
    if (targetElement.parentElement.tagName === 'DIV')
      validateVal = targetElement?.getAttribute('aria-expanded');
    else validateVal = targetElement?.parentElement?.getAttribute('aria-expanded');
    console.log('validateVal', validateVal, childNode);

    if (validateVal === 'true') {
      childNode.innerHTML = childNode.innerHTML.replace('+', 'I');
    } else if (validateVal === 'false') {
      childNode.innerHTML = childNode.innerHTML.replace('I', '+');
    }
  }, 10);
};
