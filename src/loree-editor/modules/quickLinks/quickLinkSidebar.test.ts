import API from '@aws-amplify/api';
import Sidebar from '../sidebar/sidebar';
import Design from '../sidebar/design';
import * as quickLinkAPI from './quickLinkApi';
import { quickLinksData } from './mockData';
import { createParagraph, getElementById, getElementsByClassName } from '../../common/dom';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('As a user when opens loree editor', () => {
  describe('#QuickLinks with Data', () => {
    const sideBar = new Sidebar();
    beforeAll(() => {
      window.sessionStorage.setItem('lmsUrl', 'https://test.com');
      window.sessionStorage.setItem('domainName', 'canvas');
      window.sessionStorage.setItem('course_id', '848');
      document.body.innerHTML = `<div id='loree-wrapper'></div>`;
      sideBar.initiate({
        features: {
          dividerdesign: true,
          fontstyles: true,
        },
      });
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => quickLinksData.announcementData)
        .mockImplementationOnce(() => quickLinksData.quizzesWithData);
      const selectedPTagElement = createParagraph('', '');
      selectedPTagElement.innerHTML = 'Insert Text Here';
      new Design().attachDesignContent('element', selectedPTagElement, { fontstyles: true });
    });

    test('Should contain quicklink option in side panel', () => {
      const QuickLinkOption = getElementById('loree-design-section-quick-link-block-heading');
      expect(QuickLinkOption?.children[1].innerHTML).toEqual('design.quicklinks');
    });

    // For Announcements
    test('Should call getAnnouncements function on clicking on Announcement Accordian', () => {
      const getAnnouncementsSpy: jest.SpyInstance = jest.spyOn(quickLinkAPI, 'getAnnouncements');
      const sectionLabel = getElementsByClassName('panel') as HTMLCollectionOf<HTMLElement>;
      expect(sectionLabel[5].children[0]?.children[1].innerHTML).toEqual('design.announcements');
      const sectionLabelElement = sectionLabel[5].children[0] as HTMLElement;
      sectionLabelElement.click();
      expect(getAnnouncementsSpy).toHaveBeenCalled();
    });

    test('Should render the announcements item under the accordian', () => {
      const quickLinkContent = getElementsByClassName('quickLinkContent')[0];
      expect(quickLinkContent.innerHTML).toEqual('Announcement1');
    });

    // For Quizzes
    test('Should call getQuizzes function on clicking on Quiz Accordian', () => {
      const getQuizzesSpy: jest.SpyInstance = jest.spyOn(quickLinkAPI, 'getQuizzes');
      const sectionLabel = getElementsByClassName('panel') as HTMLCollectionOf<HTMLElement>;
      const sectionLabelElement = sectionLabel[4].children[0] as HTMLElement;
      sectionLabelElement.click();
      expect(getQuizzesSpy).toHaveBeenCalled();
    });
    test('Should render the quiz item under the accordian', () => {
      const quickLinkContent = getElementsByClassName('quickLinkContent')[0];
      expect(quickLinkContent.innerHTML).toEqual('Q-1');
    });
  });
});
