import { showMoreContent, quickLinkDataHandling } from './quickLinkUI';
import * as quicklinkapi from './quickLinkApi';
import CONSTANTS from '../../constant';
import { StorageMock } from '../../../utils/storageMock';
import { ExpandableSection } from './quickLinkAccordion';
import Design from '../sidebar/design';
import React from 'react';
import ReactDOM from 'react-dom';
import { setSelectedElementType } from '../../utils';
import { specialElement } from '../../elements/specialElement';
import Base from '../../base';
import AttachQuickLinkDesign from './quickLinkDesign';
import { createDiv, getElementsByClassName } from '../../common/dom';

global.sessionStorage = new StorageMock() as unknown as Storage;

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Quicklinks pagination process', () => {
  const mockDiv = createDiv('');
  const e = { target: 'span.ml-3.font-weight-bold' };
  const expected = 'design.showmore';

  const resultValue1 = [
    { title: 'CD SH 1', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh1' },
    { title: 'CD SH 2', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh2' },
    { title: 'CD SH 3', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh3' },
    { title: 'CD SH 4', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh4' },
    { title: 'CD SH 5', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh5' },
    { title: 'CD SH 6', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh6' },
  ];
  const resultExpected1 =
    '<div id="loree-quick-link-pages-collapse"><div class="quickLinkContent" title="CD SH 1">CD SH 1</div><div class="quickLinkContent" title="CD SH 2">CD SH 2</div><div class="quickLinkContent" title="CD SH 3">CD SH 3</div><div class="quickLinkContent" title="CD SH 4">CD SH 4</div><div class="quickLinkContent" title="CD SH 5">CD SH 5</div><div class="quickLinkContent" title="CD SH 6">CD SH 6</div></div>';
  const resultValue2 = [
    { title: 'CD SH 1', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh1' },
    { title: 'CD SH 2', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh2' },
    { title: 'CD SH 3', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh3' },
    { title: 'CD SH 4', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh4' },
    { title: 'CD SH 5', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh5' },
    { title: 'CD SH 6', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh6' },
    { title: 'CD SH 7', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh7' },
    { title: 'CD SH 8', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh8' },
    { title: 'CD SH 9', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh9' },
    {
      title: 'CD SH 10',
      html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh10',
    },
  ];
  const resultExpected2 =
    '<div id="loree-quick-link-pages-collapse"><div class="quickLinkContent" title="CD SH 1">CD SH 1</div><div class="quickLinkContent" title="CD SH 2">CD SH 2</div><div class="quickLinkContent" title="CD SH 3">CD SH 3</div><div class="quickLinkContent" title="CD SH 4">CD SH 4</div><div class="quickLinkContent" title="CD SH 5">CD SH 5</div><div class="quickLinkContent" title="CD SH 6">CD SH 6</div></div>';
  const resultValue3 = [
    { title: 'CD SH 1', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh1' },
    { title: 'CD SH 2', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh2' },
    { title: 'CD SH 3', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh3' },
    { title: 'CD SH 4', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh4' },
    { title: 'CD SH 5', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh5' },
    { title: 'CD SH 6', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh6' },
    { title: 'CD SH 7', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh7' },
    { title: 'CD SH 8', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh8' },
    { title: 'CD SH 9', html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh9' },
    {
      title: 'CD SH 10',
      html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh10',
    },
    {
      title: 'CD SH 11',
      html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh11',
    },
  ];
  const resultExpected3 =
    '<div id="loree-quick-link-pages-collapse"><div class="quickLinkContent" title="CD SH 1">CD SH 1</div><div class="quickLinkContent" title="CD SH 2">CD SH 2</div><div class="quickLinkContent" title="CD SH 3">CD SH 3</div><div class="quickLinkContent" title="CD SH 4">CD SH 4</div><div class="quickLinkContent" title="CD SH 5">CD SH 5</div><div class="quickLinkContent" title="CD SH 6">CD SH 6</div><div class="quickLinkContent" title="CD SH 1">CD SH 1</div><div class="quickLinkContent" title="CD SH 2">CD SH 2</div><div class="quickLinkContent" title="CD SH 3">CD SH 3</div><div class="quickLinkContent" title="CD SH 4">CD SH 4</div><div class="quickLinkContent" title="CD SH 5">CD SH 5</div><div class="quickLinkContent" title="CD SH 6">CD SH 6</div><div class="quickLinkContent" title="CD SH 7">CD SH 7</div><div class="quickLinkContent" title="CD SH 8">CD SH 8</div><div class="quickLinkContent" title="CD SH 9">CD SH 9</div><div class="quickLinkContent" title="CD SH 10">CD SH 10</div><div class="quickLinkContent" title="CD SH 11">CD SH 11</div><div class="morecontent_quicklink">design.showmore</div></div>';
  const resultValue4: [] = [];
  const resultExpected4 =
    '<div id="loree-quick-link-pages-collapse"><div class="quickLinkContent" title="CD SH 1">CD SH 1</div><div class="quickLinkContent" title="CD SH 2">CD SH 2</div><div class="quickLinkContent" title="CD SH 3">CD SH 3</div><div class="quickLinkContent" title="CD SH 4">CD SH 4</div><div class="quickLinkContent" title="CD SH 5">CD SH 5</div><div class="quickLinkContent" title="CD SH 6">CD SH 6</div><div class="quickLinkContent" title="CD SH 1">CD SH 1</div><div class="quickLinkContent" title="CD SH 2">CD SH 2</div><div class="quickLinkContent" title="CD SH 3">CD SH 3</div><div class="quickLinkContent" title="CD SH 4">CD SH 4</div><div class="quickLinkContent" title="CD SH 5">CD SH 5</div><div class="quickLinkContent" title="CD SH 6">CD SH 6</div><div class="quickLinkContent" title="CD SH 7">CD SH 7</div><div class="quickLinkContent" title="CD SH 8">CD SH 8</div><div class="quickLinkContent" title="CD SH 9">CD SH 9</div><div class="quickLinkContent" title="CD SH 10">CD SH 10</div><div class="quickLinkContent" title="CD SH 11">CD SH 11</div><div class="morecontent_quicklink">design.showmore</div></div>';
  beforeEach(() => {
    mockDiv.innerHTML = '';
    mockDiv.id = CONSTANTS.LOREE_QUICK_LINK_MODULES_COLLAPSE;
    document.body.innerHTML =
      '<div id="' + CONSTANTS.LOREE_QUICK_LINK_PAGES_COLLAPSE + '">' + '</div>';
  });
  test('For modules', () => {
    const moduleOutput = showMoreContent('modules', 'font', mockDiv);
    expect(moduleOutput).toEqual(expected);
  });
  test('For pages', () => {
    const moduleOutput = showMoreContent('pages', 'font', mockDiv);
    expect(moduleOutput).toEqual(expected);
  });
  test('For discussion', () => {
    const moduleOutput = showMoreContent('discussions', 'font', mockDiv);
    expect(moduleOutput).toEqual(expected);
  });
  test('For announcement', () => {
    const moduleOutput = showMoreContent('announcements', 'font', mockDiv);
    expect(moduleOutput).toEqual(expected);
  });
  test('For quizzes', () => {
    const moduleOutput = showMoreContent('quizzes', 'font', mockDiv);
    expect(moduleOutput).toEqual(expected);
  });
  test('For files', () => {
    const moduleOutput = showMoreContent('files', 'font', mockDiv);
    expect(moduleOutput).toEqual(expected);
  });
  test('Results within limit', async () => {
    jest.spyOn(quicklinkapi, 'getPages').mockImplementation(async () => resultValue1);
    await quickLinkDataHandling(e, 'pages', 'font', false);
    expect(document.body.innerHTML).toEqual(resultExpected1);
  });
  test('Results matching limit', async () => {
    jest.spyOn(quicklinkapi, 'getPages').mockImplementation(async () => resultValue2);
    await quickLinkDataHandling(e, 'pages', 'font', false);
    expect(document.body.innerHTML).toEqual(resultExpected2);
  });
  test('Results one more than limit', async () => {
    jest.spyOn(quicklinkapi, 'getPages').mockImplementation(async () => resultValue3);
    await quickLinkDataHandling(e, 'pages', 'font', false);
    await quickLinkDataHandling(e, 'pages', 'font', true);
    expect(document.body.innerHTML).toEqual(resultExpected3);
  });
  test('No Result', async () => {
    jest.spyOn(quicklinkapi, 'getPages').mockImplementation(async () => resultValue4);
    await quickLinkDataHandling(e, 'pages', 'font', false);
    expect(document.body.innerHTML).toEqual(resultExpected4);
  });
});

describe('#getQuickLinkUrl', () => {
  const expandableSection = new ExpandableSection();
  describe('when page/discussion/quiz/announcement is selected', () => {
    test('returns selected page/discussion/quiz/announcement link', () => {
      const pageData = {
        title: 'CD SH 1',
        html_url: 'https://crystaldelta.instructure.com/courses/875/pages/cd-sh1',
      };
      expect(expandableSection.getQuickLinkUrl(pageData, 'html_url')).toEqual(
        'https://crystaldelta.instructure.com/courses/875/pages/cd-sh1',
      );
    });
  });

  describe('when file is selected', () => {
    test('returns selected file link', () => {
      const fileData = {
        title: 'CD SH 1',
        url: 'https://crystaldelta.instructure.com/files/176894/download?download_frd=1&verifier=XvqkWIVugMTgJEkYyEGxfc3iTCttauZHUnNjkjaz',
      };
      expect(expandableSection.getQuickLinkUrl(fileData, 'url')).toEqual(
        'https://crystaldelta.instructure.com/files/176894/download?download_frd=1&verifier=XvqkWIVugMTgJEkYyEGxfc3iTCttauZHUnNjkjaz',
      );
    });
  });

  describe('when module is selected', () => {
    beforeEach(() => {
      sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
      sessionStorage.setItem('course_id', '834');
      expandableSection.contentType = 'modules';
    });
    test('returns selected module link', () => {
      const moduleData = { id: 17039, name: 'Introduction' };
      expect(expandableSection.getQuickLinkUrl(moduleData, 'id')).toEqual(
        'https://crystaldelta.instructure.com/courses/834/modules/17039',
      );
    });
  });
});

describe('Quicklinks for Navigation', () => {
  let designInstance: Design;
  let quickLinkDesignInstance: AttachQuickLinkDesign;
  const DUMMY_URL = 'http://www.google.com';
  const domDiv = createDiv('');
  beforeEach(() => {
    document.body.innerHTML = '';
    designInstance = new Design();
    quickLinkDesignInstance = new AttachQuickLinkDesign();
    ReactDOM.render(
      <div className='loree-navigation-image-section'>
        <img
          className='loree-iframe-content-image loree-navigation-image-section'
          src='https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg'
          alt='navigation_logo'
        />
      </div>,
      domDiv,
    );

    document.body.innerHTML = specialElement.contents[1].innerContent[1].content;
  });

  test('Quicklinks for Navigation menu image', () => {
    const featuresList = {
      imagedesign: true,
    };
    const baseInstance = new Base();
    const imageToAddQuickLink = domDiv?.getElementsByClassName(
      CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION,
    )[0] as HTMLElement;
    designInstance.attachDesignContent('image', imageToAddQuickLink, featuresList);
    setSelectedElementType('image');
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => imageToAddQuickLink);
    quickLinkDesignInstance.baseClass = jest.fn().mockImplementation(() => baseInstance);
    sessionStorage.setItem('domainName', 'D2l');
    quickLinkDesignInstance.attachQuickLinkToSelectedElement(DUMMY_URL, 'Google', 'image');
    const anchorOutput = domDiv?.getElementsByTagName('a')[0];
    expect(anchorOutput).toHaveClass(CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION);
    expect(anchorOutput).toHaveAttribute('href', DUMMY_URL);
    expect(anchorOutput).toHaveAttribute('target', '_blank');
  });

  test('Quicklinks for Navigation menu text', () => {
    const featuresList = {
      dividerdesign: true,
    };
    const baseInstance = new Base();

    const range = document.createRange();
    range.setStart(getElementsByClassName(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)[0], 0);
    range.setEnd(getElementsByClassName(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)[0], 1);
    document.getSelection()?.addRange(range);

    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
    baseInstance.getSelectedText = jest.fn().mockImplementation(() => document.getSelection());
    designInstance.baseClass = jest.fn().mockImplementation(() => baseInstance);
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() => getElementsByClassName(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)[0]);
    quickLinkDesignInstance.baseClass = jest.fn().mockImplementation(() => baseInstance);

    const textSection = getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS,
    )[0] as HTMLElement;

    designInstance.attachDesignContent('line', textSection, featuresList);
    sessionStorage.setItem('domainName', 'canvas');
    quickLinkDesignInstance.attachQuickLinkToSelectedElement(DUMMY_URL, 'Google', 'line');

    const anchorOutput = document.getElementsByTagName('a')[0];
    expect(anchorOutput?.classList.contains(CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)).toBe(false);
    expect(anchorOutput).toHaveAttribute('href', DUMMY_URL);
    expect(anchorOutput).not.toHaveAttribute('target', '_blank');
  });
});

describe('#QuickLinks', () => {
  let designInstance: Design;
  let quickLinkDesignInstance: AttachQuickLinkDesign;
  const url = 'https://www.google.com';
  const domDiv = createDiv('');
  beforeEach(() => {
    jest.clearAllMocks();
    document.body.innerHTML = '';
    quickLinkDesignInstance = new AttachQuickLinkDesign();
    designInstance = new Design();
    ReactDOM.render(
      <div className='loree-iframe-content-element element-highlight'>
        <img
          src='https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg'
          alt='sampleImage'
        />
      </div>,
      domDiv,
    );
    document.body.innerHTML = `<p class="loree-iframe-content-element element-highlight" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px
      10px; font-family: &quot;Open Sans&quot;; font-size: 16px;" contenteditable="true">Insert text here</p>`;
  });

  test('insert link to the selected Image', () => {
    const featuresList = {
      imagedesign: true,
    };
    const baseInstance = new Base();
    const imageToAddQuickLink = domDiv?.getElementsByClassName(
      'loree-iframe-content-element element-highlight',
    )[0] as HTMLElement;
    designInstance.attachDesignContent('image', imageToAddQuickLink, featuresList);
    setSelectedElementType('image');
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => imageToAddQuickLink);
    quickLinkDesignInstance.baseClass = jest.fn().mockImplementation(() => baseInstance);
    sessionStorage.setItem('domainName', 'D2l');
    quickLinkDesignInstance.attachQuickLinkToSelectedElement(url, 'Google', 'image');
    const anchorOutput = domDiv?.getElementsByTagName('a')[0];
    expect(anchorOutput).toHaveAttribute('href', url);
    expect(anchorOutput).toHaveAttribute('target', '_blank');
  });

  test('insert link to the selected text', () => {
    const featuresList = {
      fontstyles: true,
    };
    const baseInstance = new Base();
    setSelectedElementType('font');
    const range = document.createRange();
    range.setStart(getElementsByClassName('loree-iframe-content-element')[0], 0);
    range.setEnd(getElementsByClassName('loree-iframe-content-element')[0], 1);
    document.getSelection()?.addRange(range);

    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
    baseInstance.getSelectedText = jest.fn().mockImplementation(() => document.getSelection());
    designInstance.baseClass = jest.fn().mockImplementation(() => baseInstance);
    baseInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() => getElementsByClassName(CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS)[0]);
    quickLinkDesignInstance.baseClass = jest.fn().mockImplementation(() => baseInstance);

    const textSection = getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS,
    )[0] as HTMLElement;

    designInstance.attachDesignContent('line', textSection, featuresList);
    sessionStorage.setItem('domainName', 'canvas');
    quickLinkDesignInstance.attachQuickLinkToSelectedElement(url, 'Google', 'font');

    const anchorOutput = document.getElementsByTagName('a')[0];
    expect(anchorOutput).toHaveAttribute('href', url);
    expect(anchorOutput).not.toHaveAttribute('target', '_blank');
  });
});
