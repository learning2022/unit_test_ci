export const quickLinksData = {
  quizzesWithData: {
    data: {
      canvasQuizzes: `{"statusCode":200,"body":[{"id":9322,"title":"Q-1","html_url":"https://crystaldelta.instructure.com/courses/1012/quizzes/9322","mobile_url":"https://crystaldelta.instructure.com/courses/1012/quizzes/9322?force_user=1&persist_headless=1","description":"","quiz_type":"assignment","time_limit":null,"timer_autosubmit_disabled":false,"shuffle_answers":false,"show_correct_answers":true,"scoring_policy":"keep_highest","allowed_attempts":1,"one_question_at_a_time":false,"question_count":0,"points_possible":null,"cant_go_back":false,"access_code":null,"ip_filter":null,"due_at":null,"lock_at":null,"unlock_at":null,"published":false,"unpublishable":true,"locked_for_user":false,"hide_results":null,"show_correct_answers_at":null,"hide_correct_answers_at":null,"all_dates":[{"due_at":null,"unlock_at":null,"lock_at":null,"base":true}],"can_unpublish":true,"can_update":true,"require_lockdown_browser":false,"require_lockdown_browser_for_results":false,"require_lockdown_browser_monitor":false,"lockdown_browser_monitor_data":null,"speed_grader_url":null,"permissions":{"manage":true,"read":true,"create":true,"update":true,"submit":true,"preview":true,"delete":true,"read_statistics":true,"grade":true,"review_grades":true,"view_answer_audits":true},"quiz_reports_url":"https://crystaldelta.instructure.com/api/v1/courses/1012/quizzes/9322/reports","quiz_statistics_url":"https://crystaldelta.instructure.com/api/v1/courses/1012/quizzes/9322/statistics","message_students_url":"https://crystaldelta.instructure.com/api/v1/courses/1012/quizzes/9322/submission_users/message","section_count":1,"important_dates":false,"quiz_submission_versions_html_url":"https://crystaldelta.instructure.com/courses/1012/quizzes/9322/submission_versions","assignment_id":17339,"one_time_results":false,"only_visible_to_overrides":false,"assignment_group_id":3158,"show_correct_answers_last_attempt":false,"version_number":1,"has_access_code":false,"post_to_sis":false,"migration_id":null}]}`,
    },
  },
  quizzesWithoutData: {
    data: {
      canvasQuizzes: `{"statusCode":200,"body":[]}`,
    },
  },
  announcementData: {
    data: {
      canvasAnnouncements: `{"statusCode":200,"body":[{"id":9322,"title":"Announcement1","html_url":"https://crystaldelta.instructure.com/courses/1012/quizzes/9322","mobile_url":"https://crystaldelta.instructure.com/courses/1012/quizzes/9322?force_user=1&persist_headless=1","description":"","quiz_type":"assignment","time_limit":null,"timer_autosubmit_disabled":false,"shuffle_answers":false,"show_correct_answers":true,"scoring_policy":"keep_highest","allowed_attempts":1,"one_question_at_a_time":false,"question_count":0,"points_possible":null,"cant_go_back":false,"access_code":null,"ip_filter":null,"due_at":null,"lock_at":null,"unlock_at":null,"published":false,"unpublishable":true,"locked_for_user":false,"hide_results":null,"show_correct_answers_at":null,"hide_correct_answers_at":null,"all_dates":[{"due_at":null,"unlock_at":null,"lock_at":null,"base":true}],"can_unpublish":true,"can_update":true,"require_lockdown_browser":false,"require_lockdown_browser_for_results":false,"require_lockdown_browser_monitor":false,"lockdown_browser_monitor_data":null,"speed_grader_url":null,"permissions":{"manage":true,"read":true,"create":true,"update":true,"submit":true,"preview":true,"delete":true,"read_statistics":true,"grade":true,"review_grades":true,"view_answer_audits":true},"quiz_reports_url":"https://crystaldelta.instructure.com/api/v1/courses/1012/quizzes/9322/reports","quiz_statistics_url":"https://crystaldelta.instructure.com/api/v1/courses/1012/quizzes/9322/statistics","message_students_url":"https://crystaldelta.instructure.com/api/v1/courses/1012/quizzes/9322/submission_users/message","section_count":1,"important_dates":false,"quiz_submission_versions_html_url":"https://crystaldelta.instructure.com/courses/1012/quizzes/9322/submission_versions","assignment_id":17339,"one_time_results":false,"only_visible_to_overrides":false,"assignment_group_id":3158,"show_correct_answers_last_attempt":false,"version_number":1,"has_access_code":false,"post_to_sis":false,"migration_id":null}]}`,
    },
  },
  announcementWithoutData: {
    data: {
      canvasAnnouncements: `{"statusCode":200,"body":[]}`,
    },
  },
};
