import { isD2l } from '../../../lmsConfig';
import {
  addLinkToImageInsideWrapper,
  getImageLinkFromWrapper,
  wrapImage,
} from '../../../loree-document/imageWrapper';
import { addLinkToElement } from '../../../loree-document/links';
import Base from '../../base';
import { createDiv, getElementById } from '../../common/dom';
import CONSTANTS from '../../constant';
import { hideLinkInfoPopup } from '../../document/linkInfoPopup';
import { getSelectedElementType } from '../../utils';
import { specialBlockImageSizeAdjustment } from '../externalLinkOption';
import { translate } from '../../../i18n/translate';
import { showLinkDetails } from '../links/links';

let selectedElement: HTMLElement | null = null;
let customLink: _Any;

export default class AttachQuickLinkDesign {
  attachQuickLinkToSelectedElement = (
    quickLinkUrl: string,
    quickLinkTitle: string,
    type: string,
  ): void => {
    selectedElement = this.baseClass().getSelectedElement();
    const config = this.baseClass().getConfigList();
    this.appendCustomStyles(config);
    const appendElement = selectedElement as HTMLElement;
    type = getSelectedElementType();
    if (type === 'image' && selectedElement) {
      // validation for existing anchor element
      const link = getImageLinkFromWrapper(selectedElement);
      if (link) {
        const el = selectedElement;
        this.showConfirmationModal('replace', 'image', selectedElement, quickLinkUrl, () => {
          const wrapper = wrapImage(el);
          console.log(wrapper);
          const anchor = addLinkToImageInsideWrapper(wrapper);
          hideLinkInfoPopup();
          anchor.href = quickLinkUrl;
          showLinkDetails(getImageLinkFromWrapper(wrapper));
        });
        const closeButton = getElementById('loree-custom-block-delete-close-icon');
        if (closeButton) closeButton.onclick = () => this.deletePopup();
      } else {
        const anchor = addLinkToElement(selectedElement, quickLinkUrl, false);
        if (isD2l()) anchor.setAttribute('target', '_blank');
        anchor.onclick = (e) => e.preventDefault();
        anchor.rel = 'noreferrer noopener';

        hideLinkInfoPopup();

        if (appendElement?.classList?.contains(CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION)) {
          anchor.className = CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION;
        }
        specialBlockImageSizeAdjustment(anchor, appendElement);
      }
    } else {
      this.setRangeForSelection(quickLinkUrl, quickLinkTitle);
    }
  };

  createNewTabForLink = (element: HTMLElement) => {
    if (isD2l()) element.setAttribute('target', '_blank');
  };

  appendCustomStyles = (config: _Any) => {
    customLink = config?.customLinkStyle?.customLink ? config?.customLinkStyle.customLink[0] : '';
  };

  baseClass = (): _Any => new Base();

  createAnchorTag = (quickLinkUrl: string) => {
    const anchor = document.createElement('a');
    anchor.setAttribute('href', quickLinkUrl);
    anchor.setAttribute('title', quickLinkUrl);
    this.createNewTabForLink(anchor);
    anchor.rel = 'noreferrer noopener';
    if (customLink) this.baseClass().addCustomLinkStyles(anchor, customLink);
    anchor.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    return anchor;
  };

  getRange = (text: _Any) => {
    const anchorNode = text.anchorNode as HTMLElement;
    let range;
    switch (true) {
      case anchorNode?.tagName === 'DIV' && text.type === 'Caret':
        range = this.baseClass().getPreviousSelectedElement();
        break;
      case text.anchorNode && text.anchorNode?.id !== 'loree-text-options-wrapper':
        range = text.getRangeAt(0);
        break;
      default:
        range = this.baseClass().getPreviousSelectedElement();
        break;
    }
    return range;
  };

  setRangeForSelection = (quickLinkUrl: string, quickLinkTitle: string) => {
    const text = this.baseClass().getSelectedText();
    const range = this.getRange(text);
    let isAnchorExist = this.checkAnchorTag(range);
    const preCaretRange = range?.cloneRange();
    const anchor = this.createAnchorTag(quickLinkUrl);
    const selectedRange = preCaretRange?.cloneContents();
    const selectedText = document.createTextNode(selectedRange.textContent);
    const array = Array.from(selectedRange.childNodes);
    array.map((innerTag: _Any) => {
      if (innerTag.nodeName === 'A') {
        isAnchorExist = true;
      }
      return isAnchorExist;
    });

    switch (true) {
      case (text.anchorNode?.tagName === 'DIV' || text.type === 'None') &&
        selectedRange.textContent === '':
        this.setSelection(range, quickLinkUrl, quickLinkTitle);
        break;
      case isAnchorExist:
        this.showConfirmationModal(anchor, selectedText, preCaretRange, range, () => {
          this.appendQuickLinkToSelectedText(anchor, selectedText, preCaretRange, range, 'replace');
        });
        break;
      case selectedText.length === 0 && text.type !== 'None':
        this.insertLinkToSelection(anchor, quickLinkTitle, range);
        break;
      default:
        this.appendQuickLinkToSelectedText(anchor, selectedText, preCaretRange, range, 'add');
        break;
    }

    const closeButton = getElementById('loree-custom-block-delete-close-icon');
    if (closeButton) closeButton.onclick = () => this.deletePopup();
  };

  insertLinkToSelection = (anchor: HTMLAnchorElement, quickLinkTitle: string, range: Range) => {
    anchor.innerHTML = quickLinkTitle;
    const selection = this.baseClass().getDocument().getSelection();
    let anchorRange = selection.getRangeAt(0);
    anchorRange.insertNode(anchor);
    anchorRange = range.cloneRange();
    anchorRange.collapse(false);
    selection.removeAllRanges();
    selection.addRange(anchorRange);
    selectedElement?.focus();
  };

  setSelection = (range: Range, url: string, quickLinkTitle: string) => {
    const node = this.getNode(range);
    const selection = this.baseClass().getDocument().getSelection();
    const newRange = document.createRange();
    selection?.removeAllRanges();
    newRange.selectNodeContents(node);
    selection?.addRange(newRange);
    this.setRangeForSelection(url, quickLinkTitle);
  };

  getNode = (selectedRange: Range) => {
    const startContainer = selectedRange.startContainer as HTMLElement;
    let node;
    switch (true) {
      case !startContainer.tagName:
        node = startContainer.nextSibling
          ? (startContainer.nextSibling as Node)
          : (startContainer.parentNode as Node);
        break;
      default:
        node = startContainer;
        break;
    }
    return node;
  };

  checkAnchorTag = (selectedRange: Range) => {
    const endContainer = selectedRange.endContainer as HTMLElement;
    const startContainer = selectedRange.startContainer as HTMLElement;
    const endContainerParentElement = endContainer.parentNode as HTMLElement;
    const startContainerParentElement = startContainer.parentNode as HTMLElement;
    const endContainerElement = endContainerParentElement.parentNode as HTMLElement;
    const startContainerElement = startContainerParentElement.parentNode as HTMLElement;
    return (
      endContainer.tagName === 'A' ||
      endContainerParentElement?.tagName === 'A' ||
      endContainerElement?.tagName === 'A' ||
      startContainer.tagName === 'A' ||
      startContainerParentElement?.tagName === 'A' ||
      startContainerElement?.tagName === 'A'
    );
  };

  // appendQuickLinkToSelectedImage = (selectedElement: HTMLElement, quickLinkUrl: string) => {
  //   const wrapper = wrapImage(selectedElement);
  //   const anchor = addLinkToImageInsideWrapper(wrapper);
  //   anchor.href = quickLinkUrl;
  // };

  appendQuickLinkToSelectedText = (
    anchor: HTMLAnchorElement,
    selectedText: _Any,
    preCaretRange: _Any,
    range: _Any,
    type: string,
  ) => {
    anchor.appendChild(selectedText);
    if (type === 'replace') {
      this.checkParentTag(range, anchor.innerText);
    }
    preCaretRange?.deleteContents();
    preCaretRange?.insertNode(anchor);
    this.baseClass().handleSelectedContentChanges();
    const selection = this.baseClass().getDocument().getSelection();
    const anchorNode = selection?.anchorNode as HTMLElement;
    const newRange = document.createRange();
    selection?.removeAllRanges();
    newRange.selectNodeContents(anchor as Node);
    selection?.addRange(newRange);
    if (anchorNode?.textContent === selectedText.textContent) {
      this.baseClass().setPreviousSelection(newRange);
    }
  };

  // To check the anchor tag if the selected content already has link
  checkParentTag = (range: _Any, selectedText: string) => {
    const endContainer = range.endContainer.parentNode;
    const startContainer = range.startContainer.parentNode;
    switch (true) {
      case endContainer.tagName === 'A':
        this.removeParentTag(range.endContainer.parentNode, selectedText);
        break;
      case endContainer.parentNode.tagName === 'A':
        this.removeParentTag(range.endContainer.parentNode.parentNode, selectedText);
        break;
      case startContainer.tagName === 'A':
        this.removeParentTag(range.startContainer.parentNode, selectedText);
        break;
      case startContainer.parentNode.tagName === 'A':
        this.removeParentTag(range.startContainer.parentNode.parentNode, selectedText);
        break;
    }
  };

  // To remove the anchor tag if the selected content already has link
  removeParentTag = (range: _Any, selectedText: string) => {
    const anchorTag = range.textContent.includes(selectedText); // to check the anchor tag is partially selected
    const sameTag = range.textContent === selectedText;
    if (!anchorTag || sameTag) range.remove();
  };

  showConfirmationModal = (
    anchor: _Any,
    selectedText: _Any,
    preCaretRange: _Any,
    range: _Any,
    onAccept: () => void,
  ) => {
    const editorParentWrapper = getElementById(CONSTANTS.LOREE_WRAPPER);
    const quickLinkBlockBackdrop = createDiv(
      'loree-custom-block-delete-backdrop',
      'loree-custom-block-backdrop',
    );
    editorParentWrapper.appendChild(quickLinkBlockBackdrop);
    const quickLinkBlockPopup = createDiv('loree-custom-block-delete-modal-wrapper', 'modal');
    const quickLinkModalContent = createDiv(
      'loree-custom-block-delete-modal-content',
      'modal-dialog modal-dialog-centered',
    );
    quickLinkModalContent.appendChild(
      this.menuModalContainer(anchor, selectedText, preCaretRange, range, onAccept),
    );
    quickLinkBlockPopup.appendChild(quickLinkModalContent);
    editorParentWrapper.appendChild(quickLinkBlockPopup);
  };

  menuModalContainer = (
    anchor: _Any,
    selectedText: _Any,
    preCaretRange: _Any,
    range: _Any,
    onAccept: () => void,
  ) => {
    const quickLinkModalContainer = createDiv(
      'loree-custom-block-delete-modal-container',
      'modal-content',
    );
    let innerHTML = `<div class="modal-header p-0 mx-0" id=loree-custom-block-delete-modal-header>
  <h5 class="modal-title text-primary" id="loree-custom-block-delete-modal-title"><span>${translate(
    'modal.warning',
  )}</span></h5>
  <button type="button" id="loree-custom-block-delete-close-icon" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div><span class ="loree-custom-block-section-divider" ></span>`;
    innerHTML += `<div class="modal-body p-0 "><p>${translate(
      'modal.selectedelementisalreadylinked',
    )}</p></div>
    `;
    const modalButtonWrapper = createDiv(
      'loree-custom-block-delete-modal-footer',
      'd-flex flex-row flex-nowrap align-items-center',
    );
    const modalButtonChildWrapper = createDiv('', 'm-auto');
    const cancelBtn = document.createElement('button');
    cancelBtn.id = `loree-custom-block-delete-modal-footer-cancel-button`;
    cancelBtn.innerHTML = translate('global.cancel');
    cancelBtn.onclick = () => this.deletePopup();
    const confirmBtn = document.createElement('button');
    confirmBtn.className = 'ok-button';
    confirmBtn.onclick = () => {
      onAccept();
      // anchor !== 'replace'
      //   ? this.appendQuickLinkToSelectedText(anchor, selectedText, preCaretRange, range, 'replace')
      //   : this.appendQuickLinkToSelectedImage(preCaretRange, range);
      this.deletePopup();
    };
    confirmBtn.innerHTML = translate('global.yes');
    modalButtonChildWrapper.appendChild(cancelBtn);
    modalButtonChildWrapper.appendChild(confirmBtn);
    modalButtonWrapper.appendChild(modalButtonChildWrapper);
    quickLinkModalContainer.innerHTML = innerHTML;
    quickLinkModalContainer.appendChild(modalButtonWrapper);
    return quickLinkModalContainer;
  };

  deletePopup = () => {
    const quickLinkBlockBackdrop = getElementById(`loree-custom-block-delete-backdrop`);
    quickLinkBlockBackdrop?.remove();
    const customBlockModalPopup = getElementById(`loree-custom-block-delete-modal-wrapper`);
    customBlockModalPopup?.remove();
    const base = new Base();
    base.hideCloseRowButton();
    base.showAddRowButton();
    base.hideAddElementButtonToSelectedElement();
  };
}
