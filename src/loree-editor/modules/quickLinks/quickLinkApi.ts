import {
  canvasPages,
  canvasModules,
  canvasDiscussions,
  canvasAssignments,
  canvasQuizzes,
  canvasFiles,
  canvasCourseNavigation,
  canvasAnnouncements,
  d2lModules,
  viewBbContents,
} from '../../../graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import CONSTANTS from '../../../loree-editor/constant';
import {
  CanvasAnnouncementsQuery,
  CanvasAssignmentsQuery,
  CanvasCourseNavigationQuery,
  CanvasDiscussionsQuery,
  CanvasFilesQuery,
  CanvasModulesQuery,
  CanvasPagesQuery,
  CanvasQuizzesQuery,
  D2lModulesQuery,
  ViewBbContentsQuery,
} from '../../../API';

export const getPages = async (pageIndex: number) => {
  const courseId = sessionStorage.getItem('course_id');
  const pageResponse = await API.graphql<CanvasPagesQuery>(
    graphqlOperation(canvasPages, {
      courseId: courseId,
      pageIndex: pageIndex,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(pageResponse?.data?.canvasPages as string).statusCode === 200
    ? JSON.parse(pageResponse?.data?.canvasPages as string).body
    : 'error';
};

export const getModules = async (pageIndex: number) => {
  const courseId = sessionStorage.getItem('course_id');
  const modulerResponse = await API.graphql<CanvasModulesQuery>(
    graphqlOperation(canvasModules, {
      courseId: courseId,
      pageIndex: pageIndex,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(modulerResponse?.data?.canvasModules as string).statusCode === 200
    ? JSON.parse(modulerResponse?.data?.canvasModules as string).body
    : 'error';
};

export const getDiscussions = async (pageIndex: number) => {
  const courseId = sessionStorage.getItem('course_id');
  const discussionResponse = await API.graphql<CanvasDiscussionsQuery>(
    graphqlOperation(canvasDiscussions, {
      courseId: courseId,
      pageIndex: pageIndex,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(discussionResponse?.data?.canvasDiscussions as string).statusCode === 200
    ? JSON.parse(discussionResponse?.data?.canvasDiscussions as string).body
    : 'error';
};

export const getAssignments = async () => {
  const courseId = sessionStorage.getItem('course_id');
  const assignmentResponse = await API.graphql<CanvasAssignmentsQuery>(
    graphqlOperation(canvasAssignments, {
      courseId: courseId,
    }),
  );
  const assignmentDataResponse = JSON.parse(assignmentResponse?.data?.canvasAssignments as string);
  if (assignmentDataResponse.statusCode === 200) {
    const assignmentData = [];
    for (const responseData of assignmentDataResponse.body) {
      for (const data of responseData.assignments) {
        assignmentData.push(data);
      }
    }
    return assignmentData;
  } else {
    return 'error';
  }
};

export const getQuizzes = async (pageIndex: number) => {
  const courseId = sessionStorage.getItem('course_id');
  const quizResponse = await API.graphql<CanvasQuizzesQuery>(
    graphqlOperation(canvasQuizzes, {
      courseId: courseId,
      pageIndex: pageIndex,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(quizResponse?.data?.canvasQuizzes as string).statusCode === 200
    ? JSON.parse(quizResponse?.data?.canvasQuizzes as string).body
    : 'error';
};

export const getFiles = async (pageIndex: number) => {
  const courseId = sessionStorage.getItem('course_id');
  const fileResponse = await API.graphql<CanvasFilesQuery>(
    graphqlOperation(canvasFiles, {
      courseId: courseId,
      pageIndex: pageIndex,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(fileResponse?.data?.canvasFiles as string).statusCode === 200
    ? JSON.parse(fileResponse?.data?.canvasFiles as string).body
    : 'error';
};

export const getCourseNavigation = async () => {
  const courseId = sessionStorage.getItem('course_id');
  const courseNavResponse = await API.graphql<CanvasCourseNavigationQuery>(
    graphqlOperation(canvasCourseNavigation, {
      courseId: courseId,
      pageIndex: 1,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(courseNavResponse?.data?.canvasCourseNavigation as string).statusCode === 200
    ? JSON.parse(courseNavResponse?.data?.canvasCourseNavigation as string).body
    : 'error';
};

export const getAnnouncements = async (pageIndex: number) => {
  const courseId = sessionStorage.getItem('course_id');
  const announcementResponse = await API.graphql<CanvasAnnouncementsQuery>(
    graphqlOperation(canvasAnnouncements, {
      courseId: courseId,
      pageIndex: pageIndex,
      pageItems: CONSTANTS.LOREE_QUICKLINK_MIN_ITEM,
    }),
  );
  return JSON.parse(announcementResponse?.data?.canvasAnnouncements as string).statusCode === 200
    ? JSON.parse(announcementResponse?.data?.canvasAnnouncements as string).body
    : 'error';
};

export const getD2lQuickLinkData = async () => {
  const courseId = sessionStorage.getItem('course_id');
  const modulesResponse = await API.graphql<D2lModulesQuery>(
    graphqlOperation(d2lModules, { courseId: courseId }),
  );
  const modulesData = JSON.parse(modulesResponse?.data?.d2lModules as string);
  return modulesData.statusCode === 200 ? modulesData.body : 'error';
};

export const getBbQuickLinkData = async () => {
  const courseId = sessionStorage.getItem('course_id');
  const bbContentResponse = await API.graphql<ViewBbContentsQuery>(
    graphqlOperation(viewBbContents, {
      courseId: courseId,
    }),
  );
  const contentData = JSON.parse(bbContentResponse?.data?.viewBbContents as string).body.results;
  return JSON.parse(bbContentResponse.data?.viewBbContents as string).statusCode === 200
    ? contentData
    : 'error';
};
