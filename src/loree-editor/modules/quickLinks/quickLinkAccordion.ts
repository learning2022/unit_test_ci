import AttachQuickLinkDesign from './quickLinkDesign';
import { resultNotFoundElement, showMoreContent } from './quickLinkUI';
export class ExpandableSection {
  quickLinkData: _Any[] = [];
  resultNotFoundElementParam = '';
  accordionBodyElementID = '';
  contentType = '';
  appendType = '';
  quickLinkObject: _Any;
  init = (
    quickLinkData: _Any[],
    resultNotFoundElementParam: string,
    accordionBodyElementID: string,
    contentType: string,
    appendType: string,
  ): void => {
    this.quickLinkData = quickLinkData;
    this.resultNotFoundElementParam = resultNotFoundElementParam;
    this.accordionBodyElementID = accordionBodyElementID;
    this.contentType = contentType;
    this.appendType = appendType;
  };

  appendContentToElement = (
    quickLinkData: _Any[],
    optionHtmlUrlIndex: string,
    titleValue: string,
    isMore: boolean,
  ): void => {
    this.quickLinkData = quickLinkData;
    const accordionBody = document.getElementById(this.accordionBodyElementID) as HTMLElement;
    accordionBody.innerHTML = '';
    if (this.quickLinkData.length === 0) {
      const listDiv = resultNotFoundElement(this.resultNotFoundElementParam);
      accordionBody.appendChild(listDiv);
    } else {
      for (const data of this.quickLinkData) {
        const listDiv = document.createElement('div');
        listDiv.className = 'quickLinkContent';
        listDiv.title = data[titleValue];
        listDiv.innerHTML = data[titleValue];
        const optionHtmlUrl = this.getQuickLinkUrl(data, optionHtmlUrlIndex);
        this.quickLinkObject = new AttachQuickLinkDesign();
        listDiv.onclick = () =>
          this.quickLinkObject.attachQuickLinkToSelectedElement(
            optionHtmlUrl,
            data[titleValue],
            this.appendType,
          );
        accordionBody.appendChild(listDiv);
      }
      if (isMore) showMoreContent(this.contentType, this.appendType, accordionBody);
    }
  };

  getQuickLinkUrl = (data: _Any, optionHtmlUrlIndex: string): string => {
    let optionHtmlUrl = data[optionHtmlUrlIndex];
    if (this.contentType === 'modules')
      optionHtmlUrl = `${sessionStorage.getItem('lmsUrl')}/courses/${sessionStorage.getItem(
        'course_id',
      )}/modules/${optionHtmlUrl}`;
    return optionHtmlUrl;
  };
}
