/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
import CONSTANTS from '../../constant';
import {
  getPages,
  getDiscussions,
  getModules,
  getAssignments,
  getFiles,
  getQuizzes,
  getAnnouncements,
  getCourseNavigation,
  getD2lQuickLinkData,
  getBbQuickLinkData,
} from './quickLinkApi';
import Design from '../sidebar/design';
import { videoModalIcons } from '../sidebar/iconHolder';
import Base from '../../base';
import { viewBbContentsById } from '../../../graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import { ExpandableSection } from './quickLinkAccordion';
import AttachQuickLinkDesign from './quickLinkDesign';
import { translate } from '../../../i18n/translate';

let quickLinkModulesData: any = [];
let isMoreModule = false;
let moduleIndex = 1;
let quickLinkPagesData: any = [];
let isMorePage = false;
let pageIndex = 1;
let quickLinkDiscussionsData: any = [];
let isMoreDiscussion = false;
let discussionIndex = 1;
let quickLinkAssignmentsData: any = [];
let quickLinkQuizzesData: any = [];
let isMoreQuizze = false;
let quizzeIndex = 1;
let quickLinkAnnouncementsData: any = [];
let isMoreAnnouncement = false;
let announcementIndex = 1;
let quickLinkCourseNavigationData: any = [];
let quickLinkFilesData: any = [];
let isMoreFile = false;
let fileIndex = 1;

let moduleObject: any;
let pageObject: any;
let discussionObject: any;
let announcementObject: any;
let quizObject: any;
let fileObject: any;
let quickLinkObject: AttachQuickLinkDesign;

let quickLinkType: string;
let quickLinkId: string;
let quickLinkName: string;
let quickLinkUrl: string;
let quickLinkLoading = false;

let checkModulesData = false;
let checkPagesData = false;
let checkAssignmentsData = false;
let checkAnnouncementsData = false;
let checkDiscussionsData = false;
let checkCourseNavigationData = false;
let checkQuizzesData = false;
let checkFilesData = false;

const designClass = (): any => {
  const design = new Design();
  return design;
};

const baseClass = (): any => {
  const base = new Base();
  return base;
};

export const quickLinkBlock = (type: string) => {
  const quickLinkContent = document.createElement('div');
  quickLinkContent.className = '';
  quickLinkContent.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_WRAPPER;
  //accordion heading
  const accordionHeading = document.createElement('div');
  accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_HEADING;
  accordionHeading.className = 'accordion-sub-text design-section-font';
  accordionHeading.dataset.toggle = 'collapse';
  accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_COLLAPSE}`;
  accordionHeading.onclick = (e: any): void => designClass().accordionIconClick(e);
  accordionHeading.setAttribute('aria-labeledbody', 'true');
  accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>+</span>
   <span>${translate('design.quicklinks')}</span>`;
  quickLinkContent.appendChild(accordionHeading);
  //accordion content
  const accordionContentDiv = document.createElement('div');
  accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
  accordionContentDiv.style.overflowX = 'hidden';
  accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_COLLAPSE;
  accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
  accordionContentDiv.appendChild(quickLinkAccordion(type));
  quickLinkContent.appendChild(accordionContentDiv);
  return quickLinkContent;
};

const quickLinkAccordion = (type: string) => {
  const quickLinkOptions = document.createElement('div');
  quickLinkOptions.className = 'quickLinkLabel accordion';
  quickLinkOptions.id = 'loree-quick-link-accordion-wrapper';
  quickLinkOptions.appendChild(modulesBlock(type));
  quickLinkOptions.appendChild(pagesBlock(type));
  quickLinkOptions.appendChild(assignmentsBlock(type));
  quickLinkOptions.appendChild(discussionsBlock(type));
  quickLinkOptions.appendChild(quizzesBlock(type));
  quickLinkOptions.appendChild(announcementBlock(type));
  if (!baseClass().checkDomainName()) quickLinkOptions.appendChild(courseNavigationBlock(type));
  if (!baseClass().checkDomainName()) quickLinkOptions.appendChild(FilesBlock(type));
  return quickLinkOptions;
};

const loaderElement = (elementId: string) => {
  const loader = `<div id="modal-loader" class="m-auto justify-content-center">
    <div class="icon rotating">
    ${videoModalIcons.loader}
    </div>
  </div>`;
  const element: any = document.getElementById(elementId);
  element.innerHTML = loader;
  return element;
};

const appendLoaderToElement = (elementId: string) => {
  const loader = `<div id="modal-loader" class="m-auto justify-content-center">
    <div class="icon rotating">
    ${videoModalIcons.loader}
    </div>
  </div>`;
  const element: any = document.getElementById(elementId);
  element.innerHTML += loader;
  return element;
};

export const resultNotFoundElement = (resultType: string) => {
  const listDiv = document.createElement('div');
  listDiv.className = 'font-italic';
  listDiv.style.fontSize = '12px';
  listDiv.innerHTML = `No ${resultType} found`;
  return listDiv;
};
export const quickLinkDataHandling = async (e: any, type: any, appendType: any, showMore: boolean) => {
  switch (type) {
    case 'modules':
      if (!checkModulesData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_MODULES_COLLAPSE);
        quickLinkModulesData = await getModules(moduleIndex);
        checkModulesData = true;
        isMoreModule = quickLinkModulesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        moduleObject = new ExpandableSection();
        moduleObject.init(quickLinkModulesData, 'Modules', CONSTANTS.LOREE_QUICK_LINK_MODULES_COLLAPSE, 'modules', appendType);
        // Pass data, Modules, ID, modules, type,
      }
      if (showMore) {
        moduleIndex++;
        appendLoaderToElement(CONSTANTS.LOREE_QUICK_LINK_MODULES_COLLAPSE);
        const return_quickLinkModulesData = await getModules(moduleIndex);
        isMoreModule = return_quickLinkModulesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quickLinkModulesData = quickLinkModulesData.concat(return_quickLinkModulesData);
      }
      moduleObject.appendContentToElement(quickLinkModulesData, 'id', 'name', isMoreModule);
      break;
    case 'pages':
      if (!checkPagesData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_PAGES_COLLAPSE);
        quickLinkPagesData = await getPages(pageIndex);
        checkPagesData = true;
        isMorePage = quickLinkPagesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        pageObject = new ExpandableSection();
        pageObject.init(quickLinkPagesData, 'Pages', CONSTANTS.LOREE_QUICK_LINK_PAGES_COLLAPSE, 'pages', appendType);
      }
      if (showMore) {
        pageIndex++;
        appendLoaderToElement(CONSTANTS.LOREE_QUICK_LINK_PAGES_COLLAPSE);
        const return_quickLinkPagesData = await getPages(pageIndex);
        isMorePage = return_quickLinkPagesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quickLinkPagesData = quickLinkPagesData.concat(return_quickLinkPagesData);
      }
      pageObject.appendContentToElement(quickLinkPagesData, 'html_url', 'title', isMorePage);
      break;
    case 'discussions':
      if (!checkDiscussionsData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_DISCUSSION_COLLAPSE);
        quickLinkDiscussionsData = await getDiscussions(discussionIndex);
        checkDiscussionsData = true;
        isMoreDiscussion = quickLinkDiscussionsData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        discussionObject = new ExpandableSection();
        discussionObject.init(
          quickLinkDiscussionsData,
          'Discussions',
          CONSTANTS.LOREE_QUICK_LINK_DISCUSSION_COLLAPSE,
          'discussions',
          appendType,
        );
      }
      if (showMore) {
        discussionIndex++;
        appendLoaderToElement(CONSTANTS.LOREE_QUICK_LINK_DISCUSSION_COLLAPSE);
        const return_quickLinkDiscussionsData = await getDiscussions(discussionIndex);
        isMoreDiscussion = return_quickLinkDiscussionsData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quickLinkDiscussionsData = quickLinkDiscussionsData.concat(return_quickLinkDiscussionsData);
      }
      discussionObject.appendContentToElement(quickLinkDiscussionsData, 'html_url', 'title', isMoreDiscussion);
      break;
    case 'assignments':
      if (!checkAssignmentsData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_ASSIGNMENT_COLLAPSE);
        quickLinkAssignmentsData = await getAssignments();
        checkAssignmentsData = true;
      }
      appendAssignmentsList(appendType);
      break;
    case 'quizzes':
      if (!checkQuizzesData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_QUIZ_COLLAPSE);
        quickLinkQuizzesData = await getQuizzes(quizzeIndex);
        checkQuizzesData = true;
        isMoreQuizze = quickLinkQuizzesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quizObject = new ExpandableSection();
        quizObject.init(quickLinkQuizzesData, 'Quizzes', CONSTANTS.LOREE_QUICK_LINK_QUIZ_COLLAPSE, 'quizzes', appendType);
      }
      if (showMore) {
        quizzeIndex++;
        appendLoaderToElement(CONSTANTS.LOREE_QUICK_LINK_QUIZ_COLLAPSE);
        const return_quickLinkQuizzesData = await getQuizzes(quizzeIndex);
        isMoreQuizze = return_quickLinkQuizzesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quickLinkQuizzesData = quickLinkQuizzesData.concat(return_quickLinkQuizzesData);
      }
      quizObject.appendContentToElement(quickLinkQuizzesData, 'html_url', 'title', isMoreQuizze);
      break;
    case 'announcements':
      if (!checkAnnouncementsData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_ANNOUNCEMENT_COLLAPSE);
        quickLinkAnnouncementsData = await getAnnouncements(announcementIndex);
        checkAnnouncementsData = true;
        isMoreAnnouncement = quickLinkAnnouncementsData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        announcementObject = new ExpandableSection();
        announcementObject.init(
          quickLinkQuizzesData,
          'Announcements',
          CONSTANTS.LOREE_QUICK_LINK_ANNOUNCEMENT_COLLAPSE,
          'announcements',
          appendType,
        );
      }
      if (showMore) {
        announcementIndex++;
        appendLoaderToElement(CONSTANTS.LOREE_QUICK_LINK_ANNOUNCEMENT_COLLAPSE);
        const return_quickLinkAnnouncementsData = await getAnnouncements(announcementIndex);
        isMoreAnnouncement = return_quickLinkAnnouncementsData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quickLinkAnnouncementsData = quickLinkAnnouncementsData.concat(return_quickLinkAnnouncementsData);
      }
      announcementObject.appendContentToElement(quickLinkAnnouncementsData, 'html_url', 'title', isMoreAnnouncement);
      break;
    case 'courseNavigation':
      if (!checkCourseNavigationData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_COURSENAVIGATION_COLLAPSE);
        quickLinkCourseNavigationData = await getCourseNavigation();
        checkCourseNavigationData = true;
      }
      appendCourseNavigationList(appendType);
      break;
    case 'files':
      if (!checkFilesData) {
        loaderElement(CONSTANTS.LOREE_QUICK_LINK_FILES_COLLAPSE);
        quickLinkFilesData = await getFiles(fileIndex);
        checkFilesData = true;
        isMoreFile = quickLinkFilesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        fileObject = new ExpandableSection();
        fileObject.init(quickLinkFilesData, 'Files', CONSTANTS.LOREE_QUICK_LINK_FILES_COLLAPSE, 'files', appendType);
      }
      if (showMore) {
        fileIndex++;
        appendLoaderToElement(CONSTANTS.LOREE_QUICK_LINK_FILES_COLLAPSE);
        const return_quickLinkFilesData = await getFiles(fileIndex);
        isMoreFile = return_quickLinkFilesData.length >= CONSTANTS.LOREE_QUICKLINK_MIN_ITEM;
        quickLinkFilesData = quickLinkFilesData.concat(return_quickLinkFilesData);
      }
      fileObject.appendContentToElement(quickLinkFilesData, 'url', 'display_name', isMoreFile);
      break;
  }
};

const modulesBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_MODULES_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  // quick-link-icon-right-arrow';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span>
<span class="ml-3 font-weight-bold">${translate('global.modules')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'modules', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_MODULES_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const pagesBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_PAGES_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed'; // quick-link-icon-right-arrow';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('design.pages')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'pages', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_PAGES_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse  ml-3 mb-3 ';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const discussionsBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_DISCUSSION_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('global.discussions')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'discussions', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_DISCUSSION_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const assignmentsBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_ASSIGNMENT_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('design.assignments')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'assignments', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_ASSIGNMENT_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const quizzesBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_QUIZ_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('design.quizzes')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'quizzes', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_QUIZ_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const announcementBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_ANNOUNCEMENT_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('design.announcements')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'announcements', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_ANNOUNCEMENT_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const courseNavigationBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_COURSENAVIGATION_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('design.coursenavigation')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'courseNavigation', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_COURSENAVIGATION_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

const FilesBlock = (type: string) => {
  const quickLinkAccordion = document.createElement('div');
  quickLinkAccordion.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${CONSTANTS.LOREE_QUICK_LINK_FILES_COLLAPSE}`;
  headerText.className = 'sectionLabel d-block collapsed';
  headerText.innerHTML = `<span class="image">
  <svg viewBox="0 0 8 14">
    <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
  </svg>
</span><span class="ml-3 font-weight-bold">${translate('design.files')}</span>`;
  headerText.onclick = e => quickLinkDataHandling(e, 'files', type, false);
  const accordionBody = document.createElement('div');
  accordionBody.id = CONSTANTS.LOREE_QUICK_LINK_FILES_COLLAPSE;
  accordionBody.dataset.parent = '#loree-quick-link-accordion-wrapper';
  accordionBody.className = 'collapse ml-3 mb-3';
  quickLinkAccordion.appendChild(headerText);
  quickLinkAccordion.appendChild(accordionBody);
  return quickLinkAccordion;
};

// Show more process
export const showMoreContent = (optionFor: any, appendType: any, accordionBody: HTMLElement) => {
  const showMore = document.createElement('div');
  showMore.className = 'morecontent_quicklink';
  showMore.innerHTML = translate('design.showmore');
  showMore.onclick = e => quickLinkDataHandling(e, optionFor, appendType, true);
  accordionBody!.appendChild(showMore);
  return showMore.innerHTML;
};

// Assignment and Course navigation Secion

const appendCourseNavigationList = (appendType: any) => {
  const accordionBody = document.getElementById(CONSTANTS.LOREE_QUICK_LINK_COURSENAVIGATION_COLLAPSE) as HTMLElement;
  accordionBody.innerHTML = '';
  const courseNavList = [
    translate('design.syllabus'),
    translate('design.announcements'),
    translate('global.modules'),
    translate('global.discussions'),
    translate('design.assignments'),
    translate('design.quizzes'),
    translate('design.grades'),
    translate('design.pages'),
    translate('design.people'),
    translate('design.files'),
  ];
  if (quickLinkCourseNavigationData.length === 0) {
    const listDiv = resultNotFoundElement('Course navigations');
    accordionBody!.appendChild(listDiv);
  } else {
    for (const data of quickLinkCourseNavigationData) {
      for (const navigation of courseNavList) {
        if (data.label === navigation) {
          const listDiv = document.createElement('div');
          listDiv.className = 'quickLinkContent';
          listDiv.title = data.label;
          listDiv.innerHTML = data.label;
          quickLinkObject = new AttachQuickLinkDesign();
          listDiv.onclick = () => quickLinkObject.attachQuickLinkToSelectedElement(data.full_url, data.label, appendType);
          accordionBody.appendChild(listDiv);
        }
      }
    }
  }
};

const appendAssignmentsList = (appendType: any) => {
  const accordionBody = document.getElementById(CONSTANTS.LOREE_QUICK_LINK_ASSIGNMENT_COLLAPSE) as HTMLElement;
  accordionBody!.innerHTML = '';
  if (quickLinkAssignmentsData.length === 0) {
    const listDiv = resultNotFoundElement('Assignments');
    accordionBody!.appendChild(listDiv);
  } else {
    for (const data of quickLinkAssignmentsData) {
      const listDiv = document.createElement('div');
      listDiv.className = 'quickLinkContent';
      listDiv.title = data.name;
      listDiv.innerHTML = data.name;
      quickLinkObject = new AttachQuickLinkDesign();
      listDiv.onclick = () => quickLinkObject.attachQuickLinkToSelectedElement(data.html_url, data.name, appendType);
      accordionBody.appendChild(listDiv);
    }
  }
};

export const d2lQuickLinkBlock = (type: string) => {
  const quickLinkContent = document.createElement('div');
  quickLinkContent.className = '';
  quickLinkContent.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_WRAPPER;
  const accordionHeading = document.createElement('div');
  accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_HEADING;
  accordionHeading.className = 'accordion-sub-text design-section-font position-relative';
  accordionHeading.onclick = () => d2lQuickLinkClick(type);
  accordionHeading.innerHTML = `<span class='d2l-quick-link-heading'>${translate('design.quicklinks')}</span>`;
  accordionHeading.insertAdjacentHTML('beforeend', `<span id=${CONSTANTS.LOREE_D2L_QUICK_LINK_POP_UP} class='quick-link-popup'></span>`);
  quickLinkContent.appendChild(accordionHeading);
  return quickLinkContent;
};

const d2lQuickLinkClick = async (type: string) => {
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const quickLinkBackdrop = document.createElement('div');
  quickLinkBackdrop.id = 'd2l-quick-link-backdrop';
  quickLinkBackdrop.className = 'd2l-quick-link-backdrop d-block';
  editorParentWrapper!.appendChild(quickLinkBackdrop);
  const quickLinkModalWrapper = document.createElement('div');
  quickLinkModalWrapper.id = 'd2l-quick-link-modal-wrapper';
  quickLinkModalWrapper.className = 'modal d-block';
  const quickLinkContent = document.createElement('div');
  quickLinkContent.id = 'd2l-quick-link-modal-content';
  quickLinkContent.className = 'modal-dialog modal-dialog-centered';
  const quickLinkModal = document.createElement('div');
  quickLinkModal.id = 'd2l-quick-link-modal-container';
  quickLinkModal.className = 'modal-content';
  const quickLinkBody = document.createElement('div');
  quickLinkBody.innerHTML = baseClass().modalLoader();
  quickLinkModal.appendChild(appendQuickLinkHeader());
  quickLinkModal.appendChild(quickLinkBody);
  quickLinkContent.appendChild(quickLinkModal);
  quickLinkModalWrapper.appendChild(quickLinkContent);
  editorParentWrapper.appendChild(quickLinkModalWrapper);
  const d2lQuickLinkData = await getD2lQuickLinkData();
  quickLinkBody.innerHTML = '';
  quickLinkBodyRow(quickLinkBody, d2lQuickLinkData);
  quickLinkModal.appendChild(appendQuickLinkFooter(type));
};

const quickLinkBodyRow = (quickLinkBody: HTMLElement, d2lQuickLinkData: any) => {
  const wrapper = document.createElement('div');
  const unitHeader = document.createElement('p');
  unitHeader.className = 'd2l-units-header font-weight-bold m-0 active';
  unitHeader.innerText = 'Units';
  const quickLinkRow = document.createElement('div');
  quickLinkRow.className = 'row d2l-quick-link-row mb-2';
  d2lQuickLinkColumn(quickLinkRow, d2lQuickLinkData);
  wrapper.appendChild(unitHeader);
  wrapper.appendChild(quickLinkRow);
  quickLinkBody.appendChild(wrapper);
};

const d2lQuickLinkColumn = (quickLinkRow: HTMLElement, d2lQuickLinkData: any) => {
  const quickLinkCol = document.createElement('div');
  quickLinkCol.className = 'col-3 p-1';
  const quickLinkColContainer = document.createElement('div');
  quickLinkColContainer.className = 'shadow p-2 d2l-quick-link-column position-relative';
  if (d2lQuickLinkData.Modules) appendQuickLinkContent(d2lQuickLinkData.Modules, quickLinkRow, quickLinkColContainer, true);
  if (d2lQuickLinkData.Topics) appendQuickLinkContent(d2lQuickLinkData.Topics, quickLinkRow, quickLinkColContainer, false);
  quickLinkCol.appendChild(quickLinkColContainer);
  quickLinkRow.appendChild(quickLinkCol);
};

const handleD2lQuickLinkClick = (event: any, moduleId: any, moduleTitle: string, moduleList: any, quickLinkRow: HTMLElement) => {
  quickLinkId = moduleId;
  quickLinkType = moduleList.length > 0 ? 'units' : 'topics';
  quickLinkName = moduleTitle;
  const targetModule = document.getElementById(`d2l-module-item-${moduleId}`) as HTMLInputElement;
  if (targetModule) targetModule.checked = true;
  let targetColumn = event.target.parentElement;
  while (!targetColumn.classList.contains('col-3')) {
    targetColumn = targetColumn.parentElement;
  }
  while (targetColumn.nextSibling !== null) {
    targetColumn.nextSibling.remove();
  }
  for (let i = 0; i < quickLinkRow.childNodes.length; i++) {
    (quickLinkRow.childNodes[i].firstChild as HTMLElement).classList.remove('bg-light');
  }
  const applyButton = document.getElementById('loree-quick-link-apply-button') as HTMLButtonElement;
  if (applyButton) applyButton.disabled = false;
  targetColumn.firstChild.classList.add('bg-light');
  if ((moduleList.Modules && moduleList.Modules.length > 0) || (moduleList.Topics && moduleList.Topics.length > 0))
    d2lQuickLinkColumn(quickLinkRow, moduleList);
};

const handleD2lQuickLinkApply = (type: string) => {
  const quickLinkUrl = `${sessionStorage.getItem('lmsUrl')}/d2l/le/lessons/${sessionStorage.getItem(
    'course_id',
  )}/${quickLinkType}/${quickLinkId}`;
  quickLinkObject = new AttachQuickLinkDesign();
  quickLinkObject.attachQuickLinkToSelectedElement(quickLinkUrl, quickLinkName, type);
  deleteQuickLinkModal('d2l');
};

const deleteQuickLinkModal = (type: string) => {
  const quickLinkModal = document.getElementById(`${type}-quick-link-modal-wrapper`);
  const quickLinkModalBackdrop = document.getElementById(`${type}-quick-link-backdrop`);
  if (quickLinkModal && quickLinkModalBackdrop) {
    quickLinkModal.remove();
    quickLinkModalBackdrop.remove();
  }
};
const appendQuickLinkHeader = () => {
  const quickLinkModalHeader = document.createElement('div');
  quickLinkModalHeader.className = 'modal-header p-0 m-0';
  quickLinkModalHeader.innerHTML = `<h5 class='quick-link-header-text text-primary'>${translate('design.quicklinks')}</h5>`;
  const closeIcon = document.createElement('button');
  closeIcon.className = 'close';
  const closeIconSpan = document.createElement('span');
  closeIconSpan.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="7.586" height="7.587" viewBox="0 0 7.586 7.587">
  <path id="close" d="M-5641.125-5116.2l-2.672-2.673-2.673,2.673a.657.657,0,0,1-.929,0,.657.657,0,0,1,0-.929l2.673-2.673-2.673-2.673a.656.656,0,0,1,0-.928.659.659,0,0,1,.929,0l2.673,2.673,2.672-2.673a.658.658,0,0,1,.928,0,.655.655,0,0,1,0,.928l-2.673,2.673,2.673,2.673a.656.656,0,0,1,0,.929.654.654,0,0,1-.464.192A.656.656,0,0,1-5641.125-5116.2Z" transform="translate(5647.591 5123.591)"/>
</svg>`;
  closeIcon.onclick = () => deleteQuickLinkModal(sessionStorage.getItem('domainName') === 'D2l' ? 'd2l' : 'bb');
  closeIcon.appendChild(closeIconSpan);
  quickLinkModalHeader.appendChild(closeIcon);
  return quickLinkModalHeader;
};

const appendQuickLinkFooter = (type: string) => {
  const quickLinkModalFooter = document.createElement('div');
  quickLinkModalFooter.className = 'd-flex m-auto align-items-center';
  const cancelButton = document.createElement('button');
  cancelButton.className = 'loree-quick-link-cancel-button';
  cancelButton.onclick = () => deleteQuickLinkModal(sessionStorage.getItem('domainName') === 'D2l' ? 'd2l' : 'bb');
  cancelButton.innerText = 'Cancel';
  const applyButton = document.createElement('button');
  applyButton.id = 'loree-quick-link-apply-button';
  applyButton.disabled = true;
  applyButton.className = 'loree-quick-link-apply-button';
  applyButton.onclick = () =>
    sessionStorage.getItem('domainName') === 'D2l' ? handleD2lQuickLinkApply(type) : handleBbQuickLinkApply(type);
  applyButton.innerText = 'Apply';
  quickLinkModalFooter.appendChild(cancelButton);
  quickLinkModalFooter.appendChild(applyButton);
  return quickLinkModalFooter;
};

const appendQuickLinkContent = (
  d2lQuickLinkData: any,
  quickLinkRow: HTMLElement,
  quickLinkColContainer: HTMLElement,
  isModule: boolean,
) => {
  const quickLinkHierarchyLevel = quickLinkRow.childNodes.length;
  d2lQuickLinkData.forEach((module: any) => {
    const dataId = isModule ? module.ModuleId : module.TopicId;
    const moduleItemWrapper = document.createElement('div');
    const moduleItem = document.createElement('input');
    moduleItem.type = 'radio';
    moduleItem.className = 'form-check-input ml-1';
    moduleItem.name = `d2l-module-${quickLinkHierarchyLevel}`;
    moduleItem.id = `d2l-module-item-${dataId}`;
    const moduleItemLabel = document.createElement('label');
    moduleItemLabel.className = 'form-check-label d2l-module-item-label ml-4 text-truncate';
    moduleItemLabel.htmlFor = `d2l-module-item-${dataId}`;
    moduleItemLabel.innerHTML = module.Title;
    moduleItemLabel.title = module.Title;
    if (isModule && (module.Modules.length > 0 || module.Topics.length > 0))
      moduleItemWrapper.className += ' d2l-quick-link-icon-right-arrow';
    moduleItemWrapper.appendChild(moduleItem);
    moduleItemWrapper.appendChild(moduleItemLabel);
    moduleItemWrapper.onclick = e => handleD2lQuickLinkClick(e, dataId, module.Title, module, quickLinkRow);
    quickLinkColContainer.appendChild(moduleItemWrapper);
  });
};

//BB QuickLinks

const handleBbQuickLinkApply = (type: string) => {
  const url = `${sessionStorage.getItem('lmsUrl')}${quickLinkUrl}`;
  quickLinkObject = new AttachQuickLinkDesign();
  quickLinkObject.attachQuickLinkToSelectedElement(url, quickLinkName, type);
  deleteQuickLinkModal('bb');
};

const handleBbQuickLink = (event: any, quickLinkRow: HTMLElement, moduleId: any, hasChildren: boolean, linkUrl: any, title: any) => {
  quickLinkName = title;
  quickLinkUrl = linkUrl;
  const targetModule = document.getElementById(`bb-module-item-${moduleId}`) as HTMLInputElement;
  if (targetModule) targetModule.checked = true;
  let targetColumn = event.target.parentElement;
  while (!targetColumn.classList.contains('card-parent')) {
    targetColumn = targetColumn.parentElement;
  }
  while (targetColumn.nextSibling !== null) {
    targetColumn.nextSibling.remove();
  }
  for (let i = 0; i < quickLinkRow.childNodes.length; i++) {
    (quickLinkRow.childNodes[i].firstChild as HTMLElement).classList.remove('bg-light');
  }
  const applyButton = document.getElementById('loree-quick-link-apply-button') as HTMLButtonElement;
  if (applyButton) applyButton.disabled = false;
  targetColumn.firstChild.classList.add('bg-light');
  if (hasChildren && !quickLinkLoading) {
    quickLinkLoading = true;
    handleBbQuickLinkColumn(quickLinkRow, moduleId, 'column');
  }
};

export const handleBbQuickLinkBlock = (type: string) => {
  const quickLinkContent = document.createElement('div');
  quickLinkContent.className = '';
  quickLinkContent.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_WRAPPER;
  const accordionHeading = document.createElement('div');
  accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_HEADING;
  accordionHeading.className = 'accordion-sub-text design-section-font position-relative';
  accordionHeading.onclick = () => handleBbQuickLinkClick(type);
  accordionHeading.innerHTML = `<span class='bb-quick-link-heading'>Quick Links</span>`;
  accordionHeading.insertAdjacentHTML('beforeend', `<span id=${CONSTANTS.LOREE_BB_QUICK_LINK_POP_UP} class='quick-link-popup'></span>`);
  quickLinkContent.appendChild(accordionHeading);
  return quickLinkContent;
};

const handleNavQuickLink = (unitHeader: any, quickLinkRow: any, id: any) => {
  const header = document.getElementsByClassName('bb-units-header');
  Array.from(header).forEach(head => {
    head.classList.remove('active');
  });
  if (quickLinkRow) quickLinkRow.innerHTML = baseClass().modalLoader();
  const applyButton = document.getElementById('loree-quick-link-apply-button') as HTMLButtonElement;
  applyButton.disabled = true;
  unitHeader.classList.add('active');
  handleBbQuickLinkColumn(quickLinkRow, id, 'row');
};

const handleBbQuickLinkBodyRow = (quickLinkBody: HTMLElement, bBQuickLinkData: any) => {
  const wrapper = document.createElement('div');
  let quickLinkRow: any;
  const lindId = bBQuickLinkData[0] ? bBQuickLinkData[0].id : '';
  bBQuickLinkData.forEach((element: any, index: any) => {
    quickLinkRow = document.createElement('div');
    quickLinkRow.className = 'd-flex bb-quick-link-row mb-2 overflow-auto';
    const unitHeader = document.createElement('p');
    unitHeader.className = `bb-units-header font-weight-bold mr-3 d-inline-block w-auto ${index === 0 ? 'active' : ''}`;
    unitHeader.innerText = element.title;
    unitHeader.onclick = e => handleNavQuickLink(unitHeader, quickLinkRow, element.id);
    wrapper.appendChild(unitHeader);
  });
  handleBbQuickLinkColumn(quickLinkRow, lindId, 'row');
  wrapper.appendChild(quickLinkRow);
  quickLinkBody.appendChild(wrapper);
};

const handleBbQuickLinkColumn = async (quickLinkRow: HTMLElement, lindId: any, type: string) => {
  const quickLinkCol = document.createElement('div');
  quickLinkCol.className = 'card-parent p-1';
  const quickLinkColContainer = document.createElement('div');
  quickLinkColContainer.className = 'card-width shadow p-2 bb-quick-link-column position-relative';
  const course_id = sessionStorage.getItem('course_id');
  const response: any = await API.graphql(
    graphqlOperation(viewBbContentsById, {
      courseId: course_id,
      contentId: lindId,
    }),
  );
  const contentData = JSON.parse(response.data.viewBbContentsById).body.results;
  const linkData = JSON.parse(response.data.viewBbContentsById).statusCode === 200 ? contentData : 'error';
  if (type === 'row') quickLinkRow.innerHTML = '';
  if (linkData) appendBbQuickLinkContent(linkData, quickLinkRow, quickLinkColContainer);
  quickLinkCol.appendChild(quickLinkColContainer);
  quickLinkRow.appendChild(quickLinkCol);
  quickLinkLoading = false;
};

const handleBbQuickLinkClick = async (type: string) => {
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const quickLinkBackdrop = document.createElement('div');
  quickLinkBackdrop.id = 'bb-quick-link-backdrop';
  quickLinkBackdrop.className = 'bb-quick-link-backdrop d-block';
  editorParentWrapper!.appendChild(quickLinkBackdrop);
  const quickLinkModalWrapper = document.createElement('div');
  quickLinkModalWrapper.id = 'bb-quick-link-modal-wrapper';
  quickLinkModalWrapper.className = 'modal d-block';
  const quickLinkContent = document.createElement('div');
  quickLinkContent.id = 'bb-quick-link-modal-content';
  quickLinkContent.className = 'modal-dialog modal-dialog-centered';
  const quickLinkModal = document.createElement('div');
  quickLinkModal.id = 'bb-quick-link-modal-container';
  quickLinkModal.className = 'modal-content';
  const quickLinkBody = document.createElement('div');
  quickLinkBody.innerHTML = baseClass().modalLoader();
  quickLinkModal.appendChild(appendQuickLinkHeader());
  quickLinkModal.appendChild(quickLinkBody);
  quickLinkContent.appendChild(quickLinkModal);
  quickLinkModalWrapper.appendChild(quickLinkContent);
  editorParentWrapper.appendChild(quickLinkModalWrapper);
  const bBQuickLinkData = await getBbQuickLinkData();
  quickLinkBody.innerHTML = '';
  handleBbQuickLinkBodyRow(quickLinkBody, bBQuickLinkData);
  quickLinkModal.appendChild(appendQuickLinkFooter(type));
};

const appendBbQuickLinkContent = (linkData: any, quickLinkRow: HTMLElement, quickLinkColContainer: HTMLElement) => {
  const quickLinkHierarchyLevel = quickLinkRow.childNodes.length;
  if (linkData.length > 0) {
    linkData.forEach((module: any) => {
      const dataId = module.id;
      const linkurl = module.links[0].href;
      const moduleItemWrapper = document.createElement('div');
      const moduleItem = document.createElement('input');
      moduleItem.type = 'radio';
      moduleItem.className = 'form-check-input ml-1';
      moduleItem.name = `bb-module-${quickLinkHierarchyLevel}`;
      moduleItem.id = `bb-module-item-${dataId}`;
      const moduleItemLabel = document.createElement('label');
      moduleItemLabel.className = 'form-check-label bb-module-item-label ml-4 text-truncate';
      moduleItemLabel.htmlFor = `bb-module-item-${dataId}`;
      moduleItemLabel.innerHTML = module.title;
      moduleItemLabel.title = module.title;
      if (module.hasChildren) moduleItemWrapper.className += ' bb-quick-link-icon-right-arrow';
      moduleItemWrapper.appendChild(moduleItem);
      moduleItemWrapper.appendChild(moduleItemLabel);
      moduleItem.onclick = e => handleBbQuickLink(e, quickLinkRow, module.id, module.hasChildren, linkurl, module.title);
      quickLinkColContainer.appendChild(moduleItemWrapper);
    });
  } else {
    const moduleItemWrapper = document.createElement('p');
    moduleItemWrapper.innerHTML = 'no items found';
    quickLinkColContainer.appendChild(moduleItemWrapper);
  }
};
