import Amplify, { Auth } from 'aws-amplify';
import CONSTANTS from '../../constant';
import {
  videModalForD2l,
  videoModalForBBAndCanvas,
  videoModalRightContainer,
  videoModalRightContainerBBAndCanvas,
} from './videoModalMockData';
import {
  appendModalContentBody,
  appendVideoModalFilterContainer,
  appendVideoModalRightContainer,
} from './videoModalUI';
import { VideoModal } from './videoModal';
import awsconfig from './../../../../src/aws-exports';
import { LmsAccess } from '../../../utils/lmsAccess';
import * as videoModalUtils from './videoModalUtils';
let videoModalInstance: VideoModal;

describe('#videoModalUI', () => {
  beforeEach(() => {
    videoModalInstance = new VideoModal();
    const videoModalBody = document.createElement('div');
    videoModalBody.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY;
    videoModalBody.className = 'd-flex flex-row flex-nowrap';
    sessionStorage.setItem('domainName', 'BB');
    sessionStorage.setItem('lmsUrl', 'https://crystaldelt.instructure.com');
  });
  test('renders a videoModal with autofocus set', () => {
    const myFilterContainer = appendVideoModalFilterContainer();
    const input = myFilterContainer.children.item(0);
    expect(input?.getAttribute('autofocus')).toBe('');
  });
  test('videoModal for canvas and BB', () => {
    const videoModal = appendModalContentBody(
      {
        youtube: true,
        vimeo: true,
        uploadnewvideo: false,
      },
      true,
    );
    expect(videoModal.innerHTML).toEqual(videoModalForBBAndCanvas);
  });
});

describe('video modal for D2l', () => {
  beforeEach(() => {
    sessionStorage.setItem('domainName', 'D2l');
  });
  test('renders a videoModal body', () => {
    const videoModal = appendModalContentBody(
      {
        youtube: true,
        vimeo: true,
        uploadnewvideo: false,
      },
      true,
    );
    expect(videoModal.innerHTML).toEqual(videModalForD2l);
  });
});
describe('video modal right container for BB and canvas', () => {
  const lms = new LmsAccess();
  Amplify.configure(awsconfig);
  beforeEach(() => {
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'veni',
        sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    sessionStorage.setItem('domainName', 'BB');
  });

  test('renders a videoModal body right side', async () => {
    const rightContainer = appendVideoModalRightContainer(
      lms,
      {
        youtube: true,
        vimeo: true,
        uploadnewvideo: true,
      },
      true,
    );
    expect(rightContainer.innerHTML).toEqual(videoModalRightContainerBBAndCanvas);
  });
});
describe('video modal right container for D2l', () => {
  const lms = new LmsAccess();
  beforeEach(() => {
    sessionStorage.setItem('domainName', 'D2l');
  });

  test('renders a videoModal body right side', async () => {
    const rightContainer = appendVideoModalRightContainer(
      lms,
      {
        youtube: true,
        vimeo: true,
        uploadnewvideo: true,
      },
      true,
    );
    expect(rightContainer.innerHTML).toEqual(videoModalRightContainer);
  });
});

describe('video modal get youtube video in D2l', () => {
  const eventMock = {
    key: 'Enter',
    code: 13,
    charCode: 13,
    target: true,
  };
  beforeEach(() => {
    videoModalInstance = new VideoModal();
    sessionStorage.setItem('domainName', 'D2l');
    sessionStorage.setItem('lmsUrl', 'https://crystaldelt.instructure.com');
  });

  test('get youtube input', async () => {
    videoModalInstance.insertYoutubeVideo = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.getYoutubeVideo(eventMock as unknown as KeyboardEvent);
    expect(videoModalInstance.resetStandaloneVideoPreview).toBeCalled();
  });

  test('get vimeo video input', async () => {
    videoModalInstance.insertVimeoVideo = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.getVimeoVideo(eventMock as unknown as KeyboardEvent);
    expect(videoModalInstance.resetStandaloneVideoPreview).toBeCalled();
  });

  test('get video by url', async () => {
    videoModalInstance.insertVideoByUrl = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.getVideoByUrl(eventMock as unknown as KeyboardEvent);
    expect(videoModalInstance.resetStandaloneVideoPreview).toBeCalled();
  });
  test('appendVideoToPreviewScreen in D2l', async () => {
    videoModalInstance.checkVideoInputType = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.appendVideoToPreviewScreen();
    expect(videoModalInstance.resetStandaloneVideoPreview).toBeCalled();
  });
  test('backbutton handler in D2l', async () => {
    videoModalInstance.checkVideoModalScreenVisiblity = jest.fn().mockImplementation(() => {});
    const videModalMock = jest.spyOn(videoModalUtils, 'resetStandaloneVideoModalSize');
    videoModalInstance.backButtonHandler();
    expect(videModalMock).toHaveBeenCalled();
  });
});

describe('video modal get youtube video in BB/CANVAS', () => {
  const eventMock = {
    key: 'Enter',
    code: 13,
    charCode: 13,
    target: true,
  };
  beforeEach(() => {
    videoModalInstance = new VideoModal();
    sessionStorage.setItem('domainName', 'BB');
  });

  test('get youtube input', async () => {
    videoModalInstance.insertYoutubeVideo = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.getYoutubeVideo(eventMock as unknown as KeyboardEvent);
    expect(videoModalInstance.resetStandaloneVideoPreview).not.toHaveBeenCalled();
  });

  test('get vimeo video input', async () => {
    videoModalInstance.insertVimeoVideo = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.getVimeoVideo(eventMock as unknown as KeyboardEvent);
    expect(videoModalInstance.resetStandaloneVideoPreview).not.toHaveBeenCalled();
  });

  test('get video by url', async () => {
    videoModalInstance.insertVideoByUrl = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.getVideoByUrl(eventMock as unknown as KeyboardEvent);
    expect(videoModalInstance.resetStandaloneVideoPreview).not.toHaveBeenCalled();
  });
  test('appendVideoToPreviewScreen in BB/canvas', async () => {
    videoModalInstance.checkVideoInputType = jest.fn().mockImplementation(() => {});
    videoModalInstance.resetStandaloneVideoPreview = jest.fn();
    videoModalInstance.appendVideoToPreviewScreen();
    expect(videoModalInstance.resetStandaloneVideoPreview).not.toHaveBeenCalled();
  });
});
