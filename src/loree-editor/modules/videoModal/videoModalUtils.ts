/* eslint-disable no-case-declarations */
import { API, graphqlOperation } from 'aws-amplify';
import CONSTANTS from '../../constant';
import Base from '../../base';
import { listFiles, listKalturaConfigs } from '../../../graphql/queries';
import { LmsAccess } from '../../../utils/lmsAccess';
import { listCanvasVideos } from '../../../utils/videoUpload';
import { translate } from '../../../i18n/translate';
import { DropdownTranslationKeys, DropdownValues } from '../imageModal/imageModalUtils';
import { getCurrentPlatform, isD2l } from '../../../lmsConfig';
import { captureErrorLog } from '../../utils';
import { createDiv } from '../../common/dom';

let videoInsertionType: string;
let videoBlockInnerHtml: string;

const lms = new LmsAccess();

const baseClass = (): _Any => {
  const base = new Base();
  return base;
};

export const getVideoBlockInnerHtml = (): string => {
  return videoBlockInnerHtml;
};

export const getVideoBlockInsertionType = (): string => {
  return videoInsertionType;
};

export const clearVideoUrlInputFieldValue = (): void => {
  const videoUploaderInput = document.getElementById(
    CONSTANTS.LOREE_VIDEO_UPLOADER_INPUT,
  ) as HTMLInputElement;
  const youtubeInput = document.getElementById(
    CONSTANTS.LOREE_VIDEO_YOUTUBE_INPUT,
  ) as HTMLInputElement;
  const vimeoInput = document.getElementById(CONSTANTS.LOREE_VIDEO_VIMEO_INPUT) as HTMLInputElement;
  const videoUrlInput = document.getElementById(
    CONSTANTS.LOREE_VIDEO_BY_URL_INPUT,
  ) as HTMLInputElement;
  if (videoUploaderInput && youtubeInput && vimeoInput && videoUrlInput) {
    videoUploaderInput.value = '';
    youtubeInput.value = '';
    vimeoInput.value = '';
    videoUrlInput.value = '';
  }
};

export const removeVideoDisableClassForInputField = (): void => {
  const localUploadInput = document.getElementById(CONSTANTS.LOREE_VIDEO_LOCAL_UPLOAD_INPUT);
  const youtubeInput = document.getElementById(
    CONSTANTS.LOREE_VIDEO_YOUTUBE_INPUT,
  ) as HTMLInputElement;
  const vimeoInput = document.getElementById(CONSTANTS.LOREE_VIDEO_VIMEO_INPUT) as HTMLInputElement;
  const videoUrlInput = document.getElementById(
    CONSTANTS.LOREE_VIDEO_BY_URL_INPUT,
  ) as HTMLInputElement;
  if (localUploadInput && youtubeInput && vimeoInput && videoUrlInput) {
    localUploadInput?.classList.remove('active');
    if (
      localUploadInput?.parentElement &&
      youtubeInput?.parentElement &&
      vimeoInput?.parentElement &&
      videoUrlInput?.parentElement
    ) {
      localUploadInput?.parentElement?.classList.remove('disable');
      youtubeInput?.parentElement?.classList.remove('disable');
      vimeoInput?.parentElement?.classList.remove('disable');
      videoUrlInput?.parentElement?.classList.remove('disable');
    }
  }
};

const resetVideoDesignSection = (): void => {
  const videoElement = document.getElementById('video');
  videoElement?.classList.remove('active');
};

export const resetVideoModalDetailScreen = (): void => {
  const videoUploadScreen = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY,
  ) as HTMLElement;
  const addButton = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  const modalLeftContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY_LEFT_CONTAINER,
  ) as HTMLElement;
  const videoPreview = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_PREVIEW,
  ) as HTMLElement;
  const videoPreviewScreen = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY,
  ) as HTMLElement;
  const thumbnailBlock = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_THUMBNAIL_BLOCK,
  );
  const videoTitle = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT,
  ) as HTMLInputElement;
  const thumbnailInput = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_INPUT,
  ) as HTMLInputElement;
  const thumbnailPreview = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE,
  ) as HTMLImageElement;
  const thumbnailLabel = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_LABEL,
  ) as HTMLInputElement;
  const videoModalBackBtn = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_BACK_BUTTON,
  ) as HTMLButtonElement;
  const videoThumbnail = document.querySelectorAll('.video-thumbnail.active');
  const removeThumbnailLabelWrapper = document.getElementById(
    CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL_WRAPPER,
  ) as HTMLLabelElement;
  removeThumbnailLabelWrapper?.classList.add('d-none');
  videoUploadScreen?.classList.remove('d-none');
  modalLeftContainer?.classList.remove('disable');
  videoPreviewScreen?.classList.add('d-none');
  videoModalBackBtn.classList.remove('d-inline-block');
  videoModalBackBtn.classList.add('d-none');
  thumbnailBlock?.classList.add('d-none');
  if (videoThumbnail.length > 0) {
    videoThumbnail[0]?.classList.remove('active');
  }
  if (addButton && videoPreview && thumbnailInput && videoTitle && thumbnailLabel) {
    addButton.disabled = true;
    videoPreview.innerHTML = '';
    videoTitle.value = '';
    thumbnailInput.value = '';
    thumbnailLabel.innerHTML = translate('modal.addthumbnail');
  }
  if (thumbnailPreview) thumbnailPreview.removeAttribute('src');
};

const clearVideoListContainer = (): void => {
  const loreeVideoListContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeVideoListContainer) loreeVideoListContainer.innerHTML = '';
};

// Filtering Values
const sortVideoComparision = (arg: _Any, arg2: _Any): _Any => {
  const ProjectTitle1 = arg.title.toUpperCase();
  const ProjectTitle2 = arg2.title.toUpperCase();
  let data = 0;
  if (ProjectTitle1 > ProjectTitle2) {
    data = 1;
  } else if (ProjectTitle1 < ProjectTitle2) {
    data = -1;
  }
  return data;
};

const resetVideoSearchBar = (): void => {
  const videoModalCloseButtom = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_FILTER_SEARCH_ICON,
  ) as HTMLElement;
  if (videoModalCloseButtom?.classList.contains('active')) {
    videoModalCloseButtom.click();
  }
};

const resetVideoFilterDropdown = (): void => {
  const videoModalFilterDropdown = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_FILTER_BUTTON,
  ) as HTMLElement;
  if (videoModalFilterDropdown) {
    videoModalFilterDropdown.innerHTML = translate('modal.recent');
  }
};

export const resetStandaloneVideoModalSize = (): void => {
  const videoModal = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_DIALOG);
  const videoModalDivider = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_DIVIDER);
  videoModal?.classList.add('standalone-modal');
  videoModalDivider?.classList.add('standalone-modal-divider');
};

const setVideoModalDisplay = (): void => {
  const videoModalBackground = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_BACKGROUND,
  ) as HTMLElement;
  const videoModal = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_WRAPPER) as HTMLElement;
  videoModalBackground.style.display = 'block';
  videoModal.style.display = 'block';
};

const resetVideoModalDisplay = (): void => {
  const videoModalBackground = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_BACKGROUND,
  ) as HTMLElement;
  const videoModal = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_WRAPPER) as HTMLElement;
  videoModalBackground.style.display = 'none';
  videoModal.style.display = 'none';
};

const selectedVideoHighlight = (selectedElement: HTMLElement) => {
  const loreeVideoListContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeVideoListContainer.childNodes) {
    const videoLists = Array.prototype.slice.call(loreeVideoListContainer.childNodes);
    videoLists.forEach((videoList) => {
      videoList.classList.remove('active');
    });
  }
  selectedElement.classList.add('active');
  const okButton = document.getElementById(
    CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  if (okButton) {
    okButton.disabled = false;
  }
};

const appendVideoThumbail = (src: _Any, name: _Any, thumbnailObjectUrl: _Any): string => {
  return `
    <video ${
      thumbnailObjectUrl ? `poster="${thumbnailObjectUrl}"` : ''
    }onclick="this.paused ? this.play() : this.pause();" src="${src}" title="${name}" class="loree-video-list-video-thumbnail"></video>
    <div class="loree-video-list-video-title text-truncate" data-toggle="tooltip" data-placement="top" title="${name}">${name}</div>
  `;
};

const appendCanvasVideoListToContainer = async (uploadUrl: _Any) => {
  const loreeVideoListContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
  ) as HTMLElement;
  const videoThumbnail = document.createElement('div');
  videoThumbnail.className = 'd-flex flex-column video-thumbnail';
  videoThumbnail.onclick = () => selectedVideoHighlight(videoThumbnail);
  videoThumbnail.innerHTML = appendVideoThumbail(
    uploadUrl.objectUrl,
    uploadUrl.name,
    uploadUrl.thumbnailObjectUrl,
  );
  loreeVideoListContainer.appendChild(videoThumbnail);
};

const listCanvasFilteredVideos = (videoFilesList: _Any) => {
  const videosList: _Any = [];
  videoFilesList.map((item: _Any) =>
    videosList.push({
      id: item.id,
      name: item.title,
      objectUrl: item.dataUrl,
      thumbnailObjectUrl: item.thumbnailUrl,
    }),
  );
  const loreeVideoListContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeVideoListContainer) {
    loreeVideoListContainer.innerHTML = '';
    if (videosList.length > 0) {
      videosList.forEach((videoList: _Any): void => {
        void appendCanvasVideoListToContainer(videoList);
      });
    } else {
      const loreeVideoListContainer = document.getElementById(
        CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
      ) as HTMLElement;
      const videoThumbnail = document.createElement('div');
      videoThumbnail.className = 'm-auto';
      videoThumbnail.innerHTML = translate('global.noresultsfound');
      loreeVideoListContainer.appendChild(videoThumbnail);
    }
  }
};

export const handleCanvasVideoSort: _Any = async (sortDropdownValue: string) => {
  let videoFileList: _Any = [];
  videoFileList = await listCanvasVideos();
  const FilterDataArray = [];
  if (videoFileList.length > 0) {
    for (const typeOfMedia of videoFileList) {
      FilterDataArray.push(typeOfMedia);
      switch (sortDropdownValue) {
        case DropdownTranslationKeys.ASCENDING:
        case DropdownValues.ASCENDING:
          FilterDataArray.sort(sortVideoComparision);
          listCanvasFilteredVideos(FilterDataArray);
          break;
        case DropdownTranslationKeys.DESCENDING:
        case DropdownValues.DESCENDING:
          const sorting = FilterDataArray.sort(sortVideoComparision);
          const sortDescent = sorting.reverse();
          listCanvasFilteredVideos(sortDescent);
          break;
        case DropdownTranslationKeys.RECENT:
        case DropdownValues.RECENT:
          FilterDataArray.sort((arg: _Any, arg2: _Any) => {
            const dateOfArg: _Any = new Date(arg.createdAt);
            const dateOfArg2: _Any = new Date(arg2.createdAt);
            return dateOfArg2 - dateOfArg;
          });
          listCanvasFilteredVideos(FilterDataArray);
          break;
        case DropdownTranslationKeys.SIZE:
        case DropdownValues.SIZE:
          FilterDataArray.sort((arg: _Any, arg2: _Any) => {
            const sizeOfArg: _Any = arg.size;
            const sizeOfArg2: _Any = arg2.size;
            return sizeOfArg2 - sizeOfArg;
          });
          listCanvasFilteredVideos(FilterDataArray);
          break;
      }
    }
  } else {
    const loreeVideoListContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
    ) as HTMLElement;
    if (loreeVideoListContainer) {
      loreeVideoListContainer.innerHTML = '';
      const videoThumbnail = createDiv();
      videoThumbnail.className = 'm-auto';
      videoThumbnail.innerHTML = translate('global.noresultsfound');
      loreeVideoListContainer.appendChild(videoThumbnail);
    }
  }
};

const appendVideoListToContainer = async (uploadUrl: _Any) => {
  const loreeVideoListContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
  ) as HTMLElement;
  const videoThumbnail = document.createElement('div');
  videoThumbnail.className = 'd-flex flex-column video-thumbnail';
  videoThumbnail.onclick = () => selectedVideoHighlight(videoThumbnail);
  videoThumbnail.innerHTML = appendVideoThumbail(
    uploadUrl.objectUrl,
    uploadUrl.name,
    uploadUrl.thumbnailObjectUrl,
  );
  loreeVideoListContainer.appendChild(videoThumbnail);
};

export const listFilteredVideos = (videoFilesList: _Any) => {
  const videosList: _Any = [];
  videoFilesList.map((item: _Any) =>
    videosList.push({
      id: item.id,
      name: item.name,
      objectUrl: item.location.key,
      thumbnailObjectUrl: item.thumbnail?.key ? item.thumbnail.key : '', // need to update
    }),
  );
  const loreeVideoListContainer = document.getElementById(
    CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeVideoListContainer) {
    loreeVideoListContainer.innerHTML = '';
    if (videosList.length > 0) {
      videosList.forEach((videoList: _Any): void => {
        void appendVideoListToContainer(videoList);
      });
    } else {
      const loreeVideoListContainer = document.getElementById(
        CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
      ) as HTMLElement;
      const videoThumbnail = document.createElement('div');
      videoThumbnail.className = 'm-auto';
      videoThumbnail.innerHTML = translate('global.noresultsfound');
      loreeVideoListContainer.appendChild(videoThumbnail);
    }
  }
};

export const handleVideoSort: _Any = async (sortDropdownValue: string) => {
  let videoFileList: _Any = [];
  videoFileList = await API.graphql(graphqlOperation(listFiles));
  const FilterDataArray = [];
  for (const typeOfMedia of videoFileList.data.listFiles.items) {
    if (typeOfMedia.type === 'VIDEO') {
      FilterDataArray.push(typeOfMedia);
      switch (sortDropdownValue) {
        case DropdownTranslationKeys.ASCENDING:
        case DropdownValues.ASCENDING:
          FilterDataArray.sort(baseClass().sortComparision);
          listFilteredVideos(FilterDataArray);
          break;
        case DropdownTranslationKeys.DESCENDING:
        case DropdownValues.DESCENDING:
          const sorting = FilterDataArray.sort(baseClass().sortComparision);
          const sortDescent = sorting.reverse();
          listFilteredVideos(sortDescent);
          break;
        case DropdownTranslationKeys.RECENT:
        case DropdownValues.RECENT:
          FilterDataArray.sort((arg: _Any, arg2: _Any) => {
            const dateOfArg: _Any = new Date(arg.createdAt);
            const dateOfArg2: _Any = new Date(arg2.createdAt);
            return dateOfArg2 - dateOfArg;
          });
          listFilteredVideos(FilterDataArray);
          break;
        case DropdownTranslationKeys.SIZE:
        case DropdownValues.SIZE:
          FilterDataArray.sort((arg: _Any, arg2: _Any) => {
            const sizeOfArg: _Any = arg.size;
            const sizeOfArg2: _Any = arg2.size;
            return sizeOfArg2 - sizeOfArg;
          });
          listFilteredVideos(FilterDataArray);
          break;
      }
    }
  }
};

export const checkIsKalturaConfigured = async () => {
  const { ltiPlatformID } = await getCurrentPlatform();
  try {
    const list: _Any = await API.graphql(
      graphqlOperation(listKalturaConfigs, {
        filter: {
          ltiPlatformID: {
            eq: ltiPlatformID,
          },
        },
        limit: 1,
      }),
    );
    return list?.data?.listKalturaConfigs?.items?.length > 0;
  } catch (err) {
    captureErrorLog('Fetching Kaltura configuration', err);
  }
};

export const getAllVideos = async () => {
  const sortingName = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_FILTER_BUTTON);
  baseClass().initializeModalLoader('VIDEO', 'MY_VIDEOS');
  if (lms.getAccess() && !isD2l()) {
    handleCanvasVideoSort(sortingName?.innerHTML);
  } else {
    handleVideoSort(sortingName?.innerHTML);
  }
};

const handleSearchCanvasVideo = async (searchText: string) => {
  baseClass().initializeModalLoader('VIDEO', 'MY_VIDEOS');
  if (searchText !== '') {
    const videoFileList: _Any = await listCanvasVideos();
    let items: _Any[] = [];
    if (videoFileList.length > 0) {
      items = videoFileList;
      const filteredVideos = items.filter((data: _Any) => {
        return data.title.toLowerCase().indexOf(searchText.toLowerCase().substr(0, 20)) !== -1;
      });
      if (filteredVideos) {
        listCanvasFilteredVideos(filteredVideos);
      }
    }
  } else {
    void getAllVideos();
  }
};

const handleSearchLoreeVideo = async (searchText: string) => {
  if (searchText !== '') {
    const videos: _Any = await API.graphql(graphqlOperation(listFiles));
    let items: _Any[] = [];
    if (videos.data.listFiles.items.length > 0) {
      items = videos.data.listFiles.items;
      const filteredVideos = items.filter((data: _Any) => {
        return data.name.toLowerCase().indexOf(searchText.toLowerCase().substr(0, 20)) !== -1;
      });
      if (filteredVideos) {
        listFilteredVideos(filteredVideos);
      }
    }
  } else {
    void getAllVideos();
  }
};

export const handleSearchVideo = async (searchText: string) => {
  if (lms.getAccess()) {
    void handleSearchCanvasVideo(searchText);
  } else {
    void handleSearchLoreeVideo(searchText);
  }
};

// globally set video details
export const setVideoDetails = (type: string, htmlText: string): void => {
  videoInsertionType = type;
  videoBlockInnerHtml = htmlText;
};

// show video upload/insertion modal popup
export const showVideoModal = (): void => {
  clearVideoUrlInputFieldValue();
  removeVideoDisableClassForInputField();
  resetVideoDesignSection();
  resetVideoModalDetailScreen();
  clearVideoListContainer();
  resetVideoSearchBar();
  setVideoModalDisplay();
  baseClass().resetModalTabs('VIDEO');
  void getAllVideos();
};

// hide video upload/insertion modal popup
export const hideVideoModal = (): void => {
  clearVideoUrlInputFieldValue();
  removeVideoDisableClassForInputField();
  resetVideoModalDetailScreen();
  resetVideoModalDisplay();
  clearVideoListContainer();
  resetVideoSearchBar();
  resetVideoFilterDropdown();
  if (!lms.getAccess() || isD2l()) resetStandaloneVideoModalSize();
};
