import Base from '../../base';
import CONSTANTS from '../../constant';
import { API, graphqlOperation } from 'aws-amplify';
import { createFile } from '../../../graphql/mutations';
import awsExports from '../../../aws-exports';
import { showFileUploadConfirmAlert, showFileTypeAlert } from '../../../utils/alert';
import { handleUploadImage } from '../../../utils/imageUpload';
import { LmsAccess } from '../../../utils/lmsAccess';
import { handleUploadVideo, handleCanvasUploadVideo } from '../../../utils/videoUpload';
import {
  appendModalContentHeader,
  appendModalContentHeaderDivider,
  appendModalContentBody,
  appenVideoDetailModal,
  appendModelContentFooter,
} from './videoModalUI';
import {
  showVideoModal,
  hideVideoModal,
  handleCanvasVideoSort,
  handleVideoSort,
  clearVideoUrlInputFieldValue,
  removeVideoDisableClassForInputField,
  resetVideoModalDetailScreen,
  handleSearchVideo,
  getVideoBlockInnerHtml,
  getVideoBlockInsertionType,
  setVideoDetails,
  resetStandaloneVideoModalSize,
} from './videoModalUtils';
import { getSelectedVideoElementStyle } from '../../modules/sidebar/designStyle';
import { addUserLanguageAsDefaultToElement } from '../../utils';
import { FeatureInterface } from '../../interface';
import { isD2l } from '../../../lmsConfig';

const lms = new LmsAccess();
export class VideoModal {
  initiate = (
    featuresList: FeatureInterface | undefined,
    isKalturaConfigured: Boolean | undefined,
  ): void => {
    const videoModalDialog = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_DIALOG);
    const videoModalContentTemplate: HTMLElement = document.createElement('div');
    videoModalContentTemplate.className = 'modal-content';
    videoModalContentTemplate.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT;
    videoModalContentTemplate.appendChild(appendModalContentHeader());
    videoModalContentTemplate.appendChild(appendModalContentHeaderDivider());
    videoModalContentTemplate.appendChild(
      appendModalContentBody(featuresList as FeatureInterface, isKalturaConfigured),
    );
    videoModalContentTemplate.appendChild(appenVideoDetailModal());
    videoModalContentTemplate.appendChild(appendModelContentFooter());
    if (videoModalDialog) {
      videoModalDialog.appendChild(videoModalContentTemplate);
    }
  };

  baseClass = (): Base => {
    const base = new Base();
    return base;
  };

  videoModalEventHandlers = (): void => {
    const localUploadInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_LOCAL_UPLOAD_INPUT,
    ) as HTMLInputElement;
    const youtubeInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_YOUTUBE_INPUT,
    ) as HTMLInputElement;
    const vimeoInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_VIMEO_INPUT,
    ) as HTMLInputElement;
    const videoUrlInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_BY_URL_INPUT,
    ) as HTMLInputElement;
    const modalLeftContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY_LEFT_CONTAINER,
    ) as HTMLElement;
    const cancelButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_CANCEL_BUTTON,
    ) as HTMLButtonElement;
    const backButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_BACK_BUTTON,
    ) as HTMLButtonElement;
    const addButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    const videoTitle = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    const videoModalSearchBox = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_FILTER_SEARCH_BOX,
    ) as HTMLInputElement;
    const videoModalSearchIcon = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_FILTER_SEARCH_ICON,
    ) as HTMLElement;
    const videoModalFilterDropdown = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_FILTER_DROPDOWN,
    ) as HTMLElement;
    const videoModalFilterButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_FILTER_BUTTON,
    ) as HTMLButtonElement;
    const videoModalCloseButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_WRAPPER_CLOSE,
    ) as HTMLButtonElement;
    const thumbnailLabel = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_LABEL,
    ) as HTMLLabelElement;
    const thumbnailInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_INPUT,
    ) as HTMLInputElement;
    const thumbnailRemoveLabel = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL,
    ) as HTMLLabelElement;
    const searchBoxIcon = document.getElementById('video-search-icon') as HTMLElement;
    if (
      localUploadInput ||
      youtubeInput ||
      vimeoInput ||
      videoUrlInput ||
      modalLeftContainer ||
      cancelButton ||
      backButton ||
      addButton ||
      videoTitle ||
      videoModalSearchBox ||
      videoModalSearchIcon ||
      videoModalFilterDropdown ||
      videoModalFilterButton ||
      videoModalCloseButton ||
      thumbnailLabel ||
      thumbnailInput ||
      thumbnailRemoveLabel ||
      searchBoxIcon
    ) {
      this.videoModalSearchIconHandler(videoModalSearchIcon, videoModalSearchBox, searchBoxIcon);
      this.videoModalSearchBoxHandler(videoModalSearchBox);
      this.videoModalFilterDropdownHandler(videoModalFilterDropdown, videoModalFilterButton);
      this.videoModalIframeVideoInputHandler(
        localUploadInput,
        youtubeInput,
        vimeoInput,
        videoUrlInput,
        modalLeftContainer,
        addButton,
      );
      this.videoModalAddButtonHandler(addButton);
      this.videoModalVideoTitleInputHandler(videoTitle);
      this.videoModalLeftContainerHandler(
        localUploadInput,
        youtubeInput,
        vimeoInput,
        videoUrlInput,
        modalLeftContainer,
        addButton,
      );
      this.videoModalBackButtonHandler(backButton);
      this.videoModalCancelButtonHandler(cancelButton, videoModalCloseButton);
      this.videoModalFileInputHandler(
        localUploadInput,
        youtubeInput,
        vimeoInput,
        videoUrlInput,
        modalLeftContainer,
        addButton,
      );
      this.videoModalThumbnailChange(thumbnailLabel, thumbnailInput);
      this.videoModalThumbnailRemoveHandler(thumbnailRemoveLabel);
    }
  };

  videoModalSearchIconHandler = (
    videoModalSearchIcon: HTMLElement,
    videoModalSearchBox: HTMLInputElement,
    searchBoxIcon: HTMLElement,
  ): void => {
    if (videoModalSearchIcon) {
      videoModalSearchIcon.onclick = () =>
        this.videoSearchBox(videoModalSearchIcon, videoModalSearchBox, searchBoxIcon);
    }
  };

  videoSearchBox = (
    videoModalSearchIcon: HTMLElement,
    videoModalSearchBox: HTMLInputElement,
    searchBoxIcon: HTMLElement,
  ): void => {
    videoModalSearchBox.value = '';
    if (videoModalSearchIcon.classList.contains('active')) {
      videoModalSearchBox.style.display = 'none';
      videoModalSearchIcon.classList.remove('active');
      searchBoxIcon.setAttribute('width', '24px');
      searchBoxIcon.setAttribute('height', '20px');
    } else {
      videoModalSearchBox.style.display = 'block';
      videoModalSearchIcon.classList.add('active');
      searchBoxIcon.setAttribute('width', '15px');
      searchBoxIcon.setAttribute('height', '12px');
    }
  };

  videoModalSearchBoxHandler = (videoModalSearchBox: HTMLInputElement): void => {
    if (videoModalSearchBox)
      videoModalSearchBox.onkeyup = async () => await handleSearchVideo(videoModalSearchBox.value);
  };

  videoModalFilterDropdownHandler = (
    videoModalFilterDropdown: HTMLElement,
    videoModalFilterButton: HTMLButtonElement,
  ): void => {
    if (videoModalFilterDropdown)
      videoModalFilterDropdown.onclick = (event: MouseEvent) =>
        this.onFilterApply(event, videoModalFilterButton);
  };

  onFilterApply = (event: MouseEvent, videoModalFilterButton: HTMLButtonElement): void => {
    const selectedFilter = (event.target as HTMLElement).innerText;
    videoModalFilterButton.innerText = selectedFilter;
    if (lms.getAccess() || !isD2l()) {
      this.baseClass().initializeModalLoader('VIDEO', 'MY_VIDEOS');
      handleCanvasVideoSort(selectedFilter);
    } else {
      handleVideoSort(selectedFilter);
    }
  };

  videoModalIframeVideoInputHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (youtubeInput)
      youtubeInput.onclick = () =>
        this.videoModalYoutubeInputHandler(
          localUploadInput,
          youtubeInput,
          vimeoInput,
          videoUrlInput,
          modalLeftContainer,
          addButton,
        );
    if (vimeoInput)
      vimeoInput.onclick = () =>
        this.videoModalVimeoInputHandler(
          localUploadInput,
          youtubeInput,
          vimeoInput,
          videoUrlInput,
          modalLeftContainer,
          addButton,
        );
    if (videoUrlInput)
      videoUrlInput.onclick = () =>
        this.videoModalVideoByUrlInputHandler(
          localUploadInput,
          youtubeInput,
          vimeoInput,
          videoUrlInput,
          modalLeftContainer,
          addButton,
        );
  };

  videoModalYoutubeInputHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (
      localUploadInput?.parentElement &&
      youtubeInput?.parentElement &&
      vimeoInput?.parentElement &&
      videoUrlInput?.parentElement
    ) {
      localUploadInput?.parentElement.classList.add('disable');
      youtubeInput?.parentElement.classList.remove('disable');
      vimeoInput?.parentElement.classList.add('disable');
      videoUrlInput?.parentElement.classList.add('disable');
      localUploadInput?.classList.remove('active');
    }
    modalLeftContainer?.classList.add('disable');
    addButton.disabled = true;
    youtubeInput.onkeypress = (e: KeyboardEvent): void => this.getYoutubeVideo(e);
    youtubeInput.oninput = (e: Event): void => this.checkInputFieldValue(e);
    youtubeInput.onfocus = (): void => clearVideoUrlInputFieldValue();
  };

  videoModalVimeoInputHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (
      localUploadInput?.parentElement &&
      youtubeInput?.parentElement &&
      vimeoInput?.parentElement &&
      videoUrlInput?.parentElement
    ) {
      localUploadInput?.parentElement.classList.add('disable');
      youtubeInput?.parentElement.classList.add('disable');
      vimeoInput?.parentElement.classList.remove('disable');
      videoUrlInput?.parentElement.classList.add('disable');
      localUploadInput?.classList.remove('active');
    }
    modalLeftContainer?.classList.add('disable');
    addButton.disabled = true;
    vimeoInput.onkeypress = (e: KeyboardEvent): void => this.getVimeoVideo(e);
    vimeoInput.oninput = (e: Event): void => this.checkInputFieldValue(e);
    vimeoInput.onfocus = (): void => clearVideoUrlInputFieldValue();
  };

  videoModalVideoByUrlInputHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (
      localUploadInput?.parentElement &&
      youtubeInput?.parentElement &&
      vimeoInput?.parentElement &&
      videoUrlInput?.parentElement
    ) {
      localUploadInput?.parentElement.classList.add('disable');
      youtubeInput?.parentElement.classList.add('disable');
      vimeoInput?.parentElement.classList.add('disable');
      videoUrlInput?.parentElement.classList.remove('disable');
      localUploadInput?.classList.remove('active');
    }
    modalLeftContainer?.classList.add('disable');
    addButton.disabled = true;
    videoUrlInput.onkeypress = (e: KeyboardEvent): void => this.getVideoByUrl(e);
    videoUrlInput.oninput = (e: Event): void => this.checkInputFieldValue(e);
    videoUrlInput.onfocus = (): void => clearVideoUrlInputFieldValue();
  };

  videoModalAddButtonHandler = (addButton: HTMLButtonElement): void => {
    addButton.onclick = () => this.addButtonOnClickHandler();
  };

  videoModalVideoTitleInputHandler = (videoTitle: HTMLInputElement): void => {
    videoTitle.oninput = (e: Event): void => this.checkInputFieldValue(e);
  };

  videoModalLeftContainerHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (modalLeftContainer)
      modalLeftContainer.onclick = () =>
        this.leftContainerHandler(
          localUploadInput,
          youtubeInput,
          vimeoInput,
          videoUrlInput,
          modalLeftContainer,
          addButton,
        );
  };

  leftContainerHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (modalLeftContainer?.classList.contains('disable')) {
      if (
        localUploadInput?.parentElement &&
        youtubeInput?.parentElement &&
        vimeoInput?.parentElement &&
        videoUrlInput?.parentElement
      ) {
        localUploadInput?.parentElement?.classList.remove('disable');
        youtubeInput?.parentElement?.classList.remove('disable');
        vimeoInput?.parentElement?.classList.remove('disable');
        videoUrlInput?.parentElement?.classList.remove('disable');
        youtubeInput.value = '';
        vimeoInput.value = '';
        videoUrlInput.value = '';
        localUploadInput?.classList.remove('active');
      }
      modalLeftContainer?.classList.remove('disable');
      addButton.disabled = false;
    }
  };

  videoModalBackButtonHandler = (backButton: HTMLButtonElement): void => {
    backButton.onclick = () => this.backButtonHandler();
  };

  videoModalCancelButtonHandler = (
    cancelButton: HTMLButtonElement,
    videoModalCloseButton: HTMLButtonElement,
  ): void => {
    cancelButton.onclick = () => hideVideoModal();
    videoModalCloseButton.onclick = () => hideVideoModal();
  };

  videoModalFileInputHandler = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (localUploadInput)
      localUploadInput.onclick = () =>
        this.uploadVideo(
          localUploadInput,
          youtubeInput,
          vimeoInput,
          videoUrlInput,
          modalLeftContainer,
          addButton,
        );
  };

  videoModalThumbnailRemoveHandler = (thumbnailRemoveLabel: HTMLLabelElement): void => {
    thumbnailRemoveLabel.onclick = () => this.removeThumbnail();
  };

  videoModalThumbnailChange = (
    thumbnailLabel: HTMLLabelElement,
    thumbnailInput: HTMLInputElement,
  ): void => {
    thumbnailLabel.onclick = () => this.changeThumbnail(thumbnailLabel, thumbnailInput);
  };

  createVideoTag = (
    selectedContainer: HTMLElement,
    objectUrl: string,
    thumbnail: string | null,
  ): void => {
    selectedContainer.innerHTML = '';
    const videoTag = document.createElement('video');
    videoTag.title = '';
    videoTag.className = CONSTANTS.LOREE_NO_POINTER;
    videoTag.style.cssText = `position: absolute;top:0;left:0;width: 100%;height: 100%;border-width:0;border-style:solid;border-color:#000;max-width:100%;`;
    videoTag.controls = true;
    videoTag.src = objectUrl;
    if (thumbnail !== null) {
      videoTag.poster = thumbnail;
    }
    selectedContainer.prepend(videoTag);
  };

  videoFileUpload = async (
    innerHTML: string,
    videoTitle: string,
    videoFile: File,
    thumbnailImageFile: File | null,
    selectedElement: HTMLElement | null,
  ): Promise<void> => {
    if (videoFile) {
      let thumbnailObjectUrl;
      let videoDetails;
      const ext = videoFile.name.substr(videoFile.name.lastIndexOf('.') + 1);
      const fileName = `${videoTitle}.${ext}`;
      if (lms.getAccess()) {
        videoDetails = await handleCanvasUploadVideo(videoFile, fileName);
      } else {
        videoDetails = await handleUploadVideo(videoFile, fileName);
      }
      if (videoDetails) {
        const videoObjectUrl = videoDetails[1];
        if (thumbnailImageFile !== null) {
          const target: string = this.baseClass().uploadTarget();
          const thumbnailObject = await handleUploadImage(
            thumbnailImageFile,
            thumbnailImageFile.name,
            target,
          );
          if (thumbnailObject) thumbnailObjectUrl = thumbnailObject[1];
          if (target === 'LOREE')
            void this.saveVideoDetails(videoDetails[0], videoObjectUrl, thumbnailObjectUrl);
        } else if (thumbnailImageFile === null && lms.getAccess()) {
          thumbnailObjectUrl = videoDetails[0].thumbnailUrl;
        } else {
          thumbnailObjectUrl = null;
        }
        this.appendVideoSrcToSelectedInnerHtml(
          innerHTML,
          videoTitle,
          videoObjectUrl,
          thumbnailObjectUrl,
          selectedElement,
        );
      }
    }
  };

  videoFileUploadPreview = (
    fileName: string,
    objectUrl: string,
    thumbnail: string | null,
    fileInsertionType: string,
  ): void => {
    const videoUploadScreen = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY,
    ) as HTMLElement;
    const videoPreviewScreen = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY,
    ) as HTMLElement;
    const videoPreviewContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_PREVIEW,
    ) as HTMLElement;
    const videoTitle = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    const videoModalBackBtn = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_BACK_BUTTON,
    ) as HTMLButtonElement;
    if (fileInsertionType === 'UPLOAD') {
      const thumbnailBlock = document.getElementById(
        CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_THUMBNAIL_BLOCK,
      );
      thumbnailBlock?.classList.remove('d-none');
    }
    videoTitle.value = fileName;
    const imageWrapper = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE_WRAPPER,
    ) as HTMLElement;
    this.createVideoTag(videoPreviewContainer, objectUrl, thumbnail);
    if (thumbnail !== null) {
      imageWrapper.innerHTML = '';
      const image = document.createElement('img');
      image.id = CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE;
      image.className = 'w-100';
      image.src = thumbnail;
      imageWrapper.appendChild(image);
      const thumbnailLabel = document.getElementById(
        CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_LABEL,
      ) as HTMLLabelElement;
      const removeThumbnailLabelWrapper = document.getElementById(
        CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL_WRAPPER,
      ) as HTMLLabelElement;
      thumbnailLabel.innerHTML = 'Update';
      removeThumbnailLabelWrapper?.classList.remove('d-none');
    }
    videoUploadScreen?.classList.add('d-none');
    videoPreviewScreen?.classList.remove('d-none');
    videoModalBackBtn.classList.add('d-inline-block');
    videoModalBackBtn.classList.remove('d-none');
    const okButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if (videoTitle.value === '') {
      okButton.disabled = true;
    } else {
      okButton.disabled = false;
    }
  };

  saveVideoDetails = async (
    videoDetails: { key: string; title: string; size: string | number },
    videoObjectUrl: string,
    thumbnailObjectUrl: string,
  ): Promise<void> => {
    const name = videoDetails.key ? videoDetails.key.slice(12, -4) : videoDetails.title;
    const size = videoDetails.size ? videoDetails.size : 0;
    let videoInput;
    if (thumbnailObjectUrl !== null) {
      videoInput = {
        name: name,
        size: size,
        location: {
          bucket: awsExports.aws_user_files_s3_bucket,
          key: videoObjectUrl,
          region: awsExports.aws_appsync_region,
        },
        thumbnail: {
          bucket: awsExports.aws_user_files_s3_bucket,
          key: thumbnailObjectUrl,
          region: awsExports.aws_appsync_region,
        },
        type: 'VIDEO',
      };
    } else {
      videoInput = {
        name: name,
        size: size,
        location: {
          bucket: awsExports.aws_user_files_s3_bucket,
          key: videoObjectUrl,
          region: awsExports.aws_appsync_region,
        },
        type: 'VIDEO',
      };
    }
    await API.graphql(graphqlOperation(createFile, { input: videoInput }));
  };

  appendVideoToSelectedColumn = (html: string, selectedElement: HTMLElement | null): void => {
    if (selectedElement) {
      const temp = document.createElement('div');
      temp.innerHTML = html;
      const content = temp.childNodes[0] as HTMLElement;
      selectedElement.appendChild(content);
      addUserLanguageAsDefaultToElement(content);
      this.baseClass().changeSelectedElement(content);
      this.baseClass().collapseElementSection();
      this.baseClass().disableElementSection();
      this.baseClass().hideCloseElementButtonToSelectedElement();
      const selectedVideo = content.getElementsByTagName('video')[0];
      if (selectedVideo) {
        getSelectedVideoElementStyle(selectedVideo as HTMLElement);
        this.baseClass().getVideoControls(selectedVideo, 'video');
      }
      this.baseClass().handleSelectedContentChanges();
    }
  };

  appendIframeSrcToSelectedInnerHtml = (
    innerHTML: string,
    objecturl: string,
    title: string,
    videoType: string | undefined,
    selectedElement: HTMLElement | null,
  ): void => {
    if (selectedElement) {
      const temp = document.createElement('div');
      temp.innerHTML = innerHTML;
      const content = temp.childNodes[0] as HTMLElement;
      content.getElementsByTagName('video')[0].remove();
      const iframeTag = content.getElementsByTagName('iframe')[0];
      iframeTag.src = objecturl;
      iframeTag.title = title;
      const iframeTagParent = iframeTag.parentElement as HTMLElement;
      if (iframeTagParent) {
        if (videoType === 'YOUTUBE') {
          iframeTagParent.style.padding = '55% 0 0 0';
        }
      }
      selectedElement.appendChild(content);
      addUserLanguageAsDefaultToElement(content);
      this.baseClass().changeSelectedElement(content);
      this.baseClass().collapseElementSection();
      this.baseClass().disableElementSection();
      this.baseClass().hideCloseElementButtonToSelectedElement();
      if (iframeTag) {
        getSelectedVideoElementStyle(iframeTag as HTMLElement);
        this.baseClass().getVideoControls(iframeTag, 'iframe');
      }
      this.baseClass().handleSelectedContentChanges();
    }
  };

  appendVideoSrcToSelectedInnerHtml = (
    innerHTML: string,
    videoTitle: string,
    videoObjectUrl: string,
    thumbnailObjectUrl: string | null,
    selectedElement: HTMLElement | null,
  ) => {
    const videoTagDiv = document.createElement('div');
    videoTagDiv.innerHTML = innerHTML;
    videoTagDiv.getElementsByTagName('iframe')[0].remove();
    const videoTag = videoTagDiv.getElementsByTagName('video')[0];
    videoTag.src = videoObjectUrl;
    videoTag.title = videoTitle;
    if (thumbnailObjectUrl !== null) {
      videoTag.poster = thumbnailObjectUrl;
    }
    this.appendVideoToSelectedColumn(videoTagDiv.innerHTML, selectedElement);
  };

  videoUploadOnChangeHandler = async (event: Event) => {
    const fileTarget = event.target as HTMLInputElement | null;
    if (fileTarget) {
      const files = fileTarget.files as FileList;
      if (files.length > 0) {
        const file: File = files[0];
        const objectUrl: string = URL.createObjectURL(file);
        const fileName = file.name.split('.').slice(0, -1).join('.');
        if (!/\.(mp4|mov)$/i.test(file.name)) {
          showFileTypeAlert('video');
        } else if (file.size > CONSTANTS.LOREE_VIDEO_UPLOADER_VALIDATION_SIZE) {
          showFileUploadConfirmAlert('video', (upload) => {
            if (upload) {
              showVideoModal();
            }
          });
        } else {
          this.videoFileUploadPreview(fileName, objectUrl, null, 'UPLOAD');
        }
      }
    }
  };

  uploadVideo = (
    localUploadInput: HTMLInputElement,
    youtubeInput: HTMLInputElement,
    vimeoInput: HTMLInputElement,
    videoUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (
      localUploadInput &&
      youtubeInput &&
      vimeoInput &&
      videoUrlInput &&
      modalLeftContainer &&
      addButton
    ) {
      if (
        localUploadInput?.parentElement &&
        youtubeInput?.parentElement &&
        vimeoInput?.parentElement &&
        videoUrlInput?.parentElement
      ) {
        localUploadInput?.parentElement.classList.remove('disable');
        youtubeInput?.parentElement.classList.add('disable');
        vimeoInput?.parentElement.classList.add('disable');
        videoUrlInput?.parentElement.classList.add('disable');
        localUploadInput?.classList.add('active');
      }
      modalLeftContainer?.classList.add('disable');
      addButton.disabled = true;
    }
    const videoUploader = document.getElementById(CONSTANTS.LOREE_VIDEO_UPLOADER_INPUT);
    if (videoUploader) {
      videoUploader.click();
      videoUploader.onchange = async (event) => await this.videoUploadOnChangeHandler(event);
    }
  };

  removeThumbnail = (): void => {
    const thumbnailInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_INPUT,
    ) as HTMLInputElement;
    const thumbnailPreview = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE,
    ) as HTMLImageElement;
    const thumbnailLabel = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_LABEL,
    ) as HTMLInputElement;
    if (thumbnailInput && thumbnailPreview && thumbnailLabel) {
      thumbnailInput.value = '';
      thumbnailPreview?.remove();
      thumbnailLabel.innerHTML = 'Add Thumbnail';
    }
    const removeThumbnailLabelWrapper = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL_WRAPPER,
    ) as HTMLLabelElement;
    removeThumbnailLabelWrapper?.classList.add('d-none');
  };

  changeThumbnail = (thumbnailLabel: HTMLLabelElement, thumbnailInput: HTMLInputElement): void => {
    if (thumbnailInput) {
      thumbnailInput.click();
      thumbnailInput.onchange = (event: Event) => this.thumbnailImageChange(event, thumbnailLabel);
    }
  };

  createIframe = (selectedContainer: HTMLElement, objectUrl: string): void => {
    selectedContainer.innerHTML = '';
    const iframe = document.createElement('iframe');
    iframe.frameBorder = '0';
    iframe.src = objectUrl;
    iframe.className = CONSTANTS.LOREE_NO_POINTER;
    iframe.title = '';
    iframe.style.cssText = `position: absolute;top:0;left:0;width: 100%;height: 100%;border-width:0;border-style:solid;border-color:#000;max-width:100%;`;
    selectedContainer.prepend(iframe);
  };

  enableBackButton = (): void => {
    const videoModalBackBtn = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_BACK_BUTTON,
    ) as HTMLButtonElement;
    videoModalBackBtn.classList.add('d-inline-block');
    videoModalBackBtn.classList.remove('d-none');
  };

  getYouTubeVideoID = (url: string) => {
    const videoUrl = url.split(/(vi\/|v%3D|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    return undefined !== videoUrl[2] ? videoUrl[2].split(/[^0-9a-z_\\-]/i)[0] : videoUrl[0];
  };

  insertYoutubeVideo = (): void => {
    const youtubeInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_YOUTUBE_INPUT,
    ) as HTMLInputElement;
    const videoID = this.getYouTubeVideoID(youtubeInput.value);
    const videoUploadScreen = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY);
    const videoPreviewScreen = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY,
    );
    const videoPreviewContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_PREVIEW,
    ) as HTMLElement;
    const thumbnailBlock = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_THUMBNAIL_BLOCK,
    );
    const videoTitle = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    videoTitle.value = '';
    const objectUrl = `https://www.youtube.com/embed/${videoID}`;
    this.createIframe(videoPreviewContainer, objectUrl);
    this.enableBackButton();
    videoUploadScreen?.classList.add('d-none');
    videoPreviewScreen?.classList.remove('d-none');
    thumbnailBlock?.classList.add('d-none');
    const okButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if (okButton) {
      okButton.disabled = true;
    }
  };

  getYoutubeVideo = (e: KeyboardEvent): void => {
    if (e.target && e.key === 'Enter') {
      if (!lms.getAccess() || isD2l()) this.resetStandaloneVideoPreview();
      this.insertYoutubeVideo();
    }
  };

  getVimeoVideoID = (url: string) => {
    const videoUrl = url.split(/video\/|https?:\/\/vimeo\.com\//);
    return undefined !== videoUrl[1] ? videoUrl[1].split(/[?&]/)[0] : videoUrl[0];
  };

  insertVimeoVideo = (): void => {
    const vimeoInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_VIMEO_INPUT,
    ) as HTMLInputElement;
    const videoID = this.getVimeoVideoID(vimeoInput.value);
    const videoUploadScreen = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY);
    const videoPreviewScreen = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY,
    );
    const videoPreviewContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_PREVIEW,
    ) as HTMLElement;
    const thumbnailBlock = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_THUMBNAIL_BLOCK,
    );
    const videoTitle = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    videoTitle.value = '';
    const objectUrl = `https://player.vimeo.com/video/${videoID}`;
    this.createIframe(videoPreviewContainer, objectUrl);
    this.enableBackButton();
    videoUploadScreen?.classList.add('d-none');
    videoPreviewScreen?.classList.remove('d-none');
    thumbnailBlock?.classList.add('d-none');
    const okButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if (okButton) {
      okButton.disabled = true;
    }
  };

  getVimeoVideo = (e: KeyboardEvent): void => {
    if (e.target && e.key === 'Enter') {
      if (!lms.getAccess() || isD2l()) this.resetStandaloneVideoPreview();
      this.insertVimeoVideo();
    }
  };

  insertVideoByUrl = (): void => {
    const videoUrlInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_BY_URL_INPUT,
    ) as HTMLInputElement;
    if (videoUrlInput) this.videoFileUploadPreview('', videoUrlInput.value, null, 'BY_URL');
  };

  getVideoByUrl = (e: KeyboardEvent): void => {
    if (e.target && e.key === 'Enter') {
      if (!lms.getAccess() || isD2l()) this.resetStandaloneVideoPreview();
      this.insertVideoByUrl();
    }
  };

  checkInputFieldValue = (e: Event): void => {
    const okButton = document.getElementById(
      CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if ((e.target as HTMLInputElement).value === '') {
      okButton.disabled = true;
    } else {
      okButton.disabled = false;
    }
  };

  checkVideoModalScreenVisiblity = () => {
    let screenType;
    const videoUploadScreen = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY);
    const videoPreviewScreen = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY,
    ) as HTMLElement;
    if (!videoUploadScreen?.classList.contains('d-none')) {
      screenType = 'videoInputScreen';
    } else if (!videoPreviewScreen?.classList.contains('d-none')) {
      screenType = 'videoPreviewScreen';
    }
    return screenType;
  };

  backButtonHandler = (): void => {
    const screenType = this.checkVideoModalScreenVisiblity();
    if (!lms.getAccess() || isD2l()) resetStandaloneVideoModalSize();
    switch (screenType) {
      case 'videoInputScreen':
        hideVideoModal();
        break;
      case 'videoPreviewScreen':
        clearVideoUrlInputFieldValue();
        removeVideoDisableClassForInputField();
        resetVideoModalDetailScreen();
        break;
    }
  };

  checkVideoInputType = () => {
    let inputType;
    const youtubeInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_YOUTUBE_INPUT,
    ) as HTMLInputElement;
    const vimeoInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_VIMEO_INPUT,
    ) as HTMLInputElement;
    const videoUrlInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_BY_URL_INPUT,
    ) as HTMLInputElement;
    if (youtubeInput.value !== '') {
      inputType = 'YOUTUBE';
    } else if (vimeoInput.value !== '') {
      inputType = 'VIMEO';
    } else if (videoUrlInput.value !== '') {
      inputType = 'BY_URL';
    } else {
      inputType = 'LOCAL';
    }
    return inputType;
  };

  insertLocalVideo = (): void => {
    const loreeVideoListContainer = document.getElementById(
      CONSTANTS.LOREE_VIDEO_LIST_CONTAINER,
    ) as HTMLElement;
    const videoThumbnailList = Array.prototype.slice.call(loreeVideoListContainer.childNodes);
    videoThumbnailList.forEach((videoThumbnail) => {
      if (videoThumbnail.classList.contains('active')) {
        let thumbnail;
        const objectUrl = videoThumbnail.children[0].src;
        const fileName = videoThumbnail.children[0].title;
        if (videoThumbnail.children[0].hasAttribute('poster')) {
          thumbnail = videoThumbnail.children[0].poster;
        } else {
          thumbnail = null;
        }
        this.videoFileUploadPreview(fileName, objectUrl, thumbnail, 'UPLOAD');
      }
    });
  };

  resetStandaloneVideoPreview = (): void => {
    const videoModal = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_DIALOG);
    videoModal?.classList.remove('standalone-modal');
    const videoModalDivider = document.getElementById(CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_DIVIDER);
    videoModalDivider?.classList.remove('standalone-modal-divider');
  };

  appendVideoToPreviewScreen = (): void => {
    const videoInputType = this.checkVideoInputType();
    if (!lms.getAccess() || isD2l()) this.resetStandaloneVideoPreview();
    switch (videoInputType) {
      case 'YOUTUBE':
        this.insertYoutubeVideo();
        break;
      case 'VIMEO':
        this.insertVimeoVideo();
        break;
      case 'BY_URL':
        this.insertVideoByUrl();
        break;
      case 'LOCAL':
        this.insertLocalVideo();
        break;
    }
  };

  checkAppendVideoType = (videoPreviewScreen: HTMLElement) => {
    let videoType;
    const previewVideo = videoPreviewScreen?.getElementsByTagName('video')[0];
    const previewIframe = videoPreviewScreen?.getElementsByTagName('iframe')[0];
    if (previewVideo) {
      videoType = 'VIDEO';
    } else if (previewIframe) {
      videoType = 'IFRAME';
    } else {
      videoType = 'PREVIEW_ERROR';
    }
    return videoType;
  };

  appendVideoFileToSelectedBlock = (videoPreviewScreen: HTMLElement, videoTitle: string): void => {
    const insertType = getVideoBlockInsertionType();
    const innerHTML = getVideoBlockInnerHtml();
    const previewVideo = videoPreviewScreen?.getElementsByTagName('video')[0];
    const thumbnailImage = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_INPUT,
    ) as HTMLInputElement;
    const thumbnailImageSrc = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE,
    ) as HTMLImageElement;
    const selectedElement = this.baseClass().getSelectedElement();
    const videoUploaderInput = document.getElementById(
      CONSTANTS.LOREE_VIDEO_UPLOADER_INPUT,
    ) as HTMLInputElement;
    const videoFiles = videoUploaderInput.files as FileList;
    if (videoFiles.length > 0) {
      // if new video file choosed
      const videoFile: File = videoFiles[0];
      const thumbnailImageFiles = thumbnailImage.files as FileList;
      if (thumbnailImageFiles.length > 0) {
        // if thumbnail choosed
        const thumbnailImageFile: File = thumbnailImageFiles[0];
        if (insertType === 'NEW') {
          void this.videoFileUpload(
            innerHTML,
            videoTitle,
            videoFile,
            thumbnailImageFile,
            selectedElement,
          );
        } else if (insertType === 'REPLACE') {
          void this.replaceVideofileUpload(
            videoTitle,
            videoFile,
            thumbnailImageFile,
            selectedElement,
          );
        }
      } else {
        // if thumbnail not choosed
        if (insertType === 'NEW') {
          void this.videoFileUpload(innerHTML, videoTitle, videoFile, null, selectedElement);
        } else if (insertType === 'REPLACE') {
          void this.replaceVideofileUpload(videoTitle, videoFile, null, selectedElement);
        }
      }
      // clear video input and thumbnail image input
      videoUploaderInput.value = '';
      thumbnailImage.value = '';
    } else {
      // if existing video file choosed
      const thumbnailImageFiles = thumbnailImage.files as FileList;
      if (thumbnailImageFiles.length > 0) {
        // if thumbnail changed
        const thumbnailImageFile: File = thumbnailImageFiles[0];
        if (insertType === 'NEW') {
          void this.appendNewThumbnailSrcToVideo(
            innerHTML,
            videoTitle,
            previewVideo.src,
            thumbnailImageFile,
            selectedElement,
          );
        } else if (insertType === 'REPLACE') {
          void this.appendNewThumbnailSrcToReplaceVideo(
            'video',
            videoTitle,
            previewVideo.src,
            thumbnailImageFile,
            selectedElement,
          );
        }
      } else {
        // if thumbnail not changed
        if (insertType === 'NEW') {
          this.appendVideoSrcToSelectedInnerHtml(
            innerHTML,
            videoTitle,
            previewVideo.src,
            thumbnailImageSrc && thumbnailImageSrc.getAttribute('src') !== ''
              ? thumbnailImageSrc.getAttribute('src')
              : null,
            selectedElement,
          );
        } else if (insertType === 'REPLACE') {
          this.appendReplaceVideoSrcToSelectedVideo(
            'video',
            videoTitle,
            previewVideo.src,
            thumbnailImageSrc && thumbnailImageSrc.getAttribute('src') !== ''
              ? thumbnailImageSrc.getAttribute('src')
              : null,
            selectedElement,
          );
        }
      }
      // clear thumbnail image input
      thumbnailImage.value = '';
    }
    this.baseClass().removeSelectedElementSection();
    hideVideoModal();
  };

  checkIframeVideoType = (iframeVideoUrl: string) => {
    let iframeVideoType;
    const url = iframeVideoUrl;
    if (url) {
      if (url.includes('youtube.com')) {
        iframeVideoType = 'YOUTUBE';
      } else if (url.includes('vimeo.com')) {
        iframeVideoType = 'VIMEO';
      } else {
        iframeVideoType = 'OTHER';
      }
    }
    return iframeVideoType;
  };

  appendIframeVideoFileToSelectedBlock = (
    videoPreviewScreen: HTMLElement,
    videoTitle: string,
  ): void => {
    const insertType = getVideoBlockInsertionType();
    const innerHTML = getVideoBlockInnerHtml();
    const previewIframe = videoPreviewScreen?.getElementsByTagName('iframe')[0];
    const iframeVideoType = this.checkIframeVideoType(previewIframe.src);
    const selectedElement = this.baseClass().getSelectedElement();
    if (insertType === 'NEW') {
      this.appendIframeSrcToSelectedInnerHtml(
        innerHTML,
        previewIframe.src,
        videoTitle,
        iframeVideoType,
        selectedElement,
      );
    } else if (insertType === 'REPLACE') {
      this.appendReplaceVideoSrcToSelectedVideo(
        'iframe',
        videoTitle,
        previewIframe.src,
        '',
        selectedElement,
      );
    }
    hideVideoModal();
  };

  appendVideoToSelectedBlock = (): void => {
    const videoPreviewScreen = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY,
    ) as HTMLElement;
    const videoTitle = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    if (videoTitle.value !== '') {
      const appendVideoType = this.checkAppendVideoType(videoPreviewScreen);
      switch (appendVideoType) {
        case 'VIDEO':
          this.appendVideoFileToSelectedBlock(videoPreviewScreen, videoTitle.value);
          break;
        case 'IFRAME':
          this.appendIframeVideoFileToSelectedBlock(videoPreviewScreen, videoTitle.value);
          break;
      }
    }
  };

  replaceVideofileUpload = async (
    titleName: string,
    videoFile: File,
    thumbnailImageFile: File | null,
    selectedElement: HTMLElement | null,
  ): Promise<void> => {
    let thumbnailObjectUrl;
    let videoDetails;
    const ext = videoFile.name.substr(videoFile.name.lastIndexOf('.') + 1);
    const fileName = `${titleName}.${ext}`;
    const uploadingLabel = document.getElementById(CONSTANTS.LOREE_UPLOADING_ITEM_LABEL);
    if (uploadingLabel) uploadingLabel.innerHTML = fileName;
    if (lms.getAccess()) {
      videoDetails = await handleCanvasUploadVideo(videoFile, fileName);
    } else {
      videoDetails = await handleUploadVideo(videoFile, fileName);
    }
    if (videoDetails) {
      const videoObjectUrl = videoDetails[1];
      if (thumbnailImageFile !== null) {
        const target: string = this.baseClass().uploadTarget();
        const thumbnailObject = await handleUploadImage(
          thumbnailImageFile,
          thumbnailImageFile.name,
          target,
        );
        if (thumbnailObject) thumbnailObjectUrl = thumbnailObject[1];
        if (target === 'LOREE')
          void this.saveVideoDetails(videoDetails[0], videoObjectUrl, thumbnailObjectUrl);
      } else if (thumbnailImageFile === null && lms.getAccess()) {
        thumbnailObjectUrl = videoDetails[0].thumbnailUrl;
      } else {
        thumbnailObjectUrl = null;
      }
      this.appendReplaceVideoSrcToSelectedVideo(
        'video',
        titleName,
        videoObjectUrl,
        thumbnailObjectUrl,
        selectedElement,
      );
    }
  };

  addButtonOnClickHandler = () => {
    const screenType = this.checkVideoModalScreenVisiblity();
    switch (screenType) {
      case 'videoInputScreen':
        this.appendVideoToPreviewScreen();
        break;
      case 'videoPreviewScreen':
        this.appendVideoToSelectedBlock();
        break;
    }
  };

  thumbnailImageChange = (e: Event, thumbnailLabel: HTMLElement): void => {
    const imageWrapper = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE_WRAPPER,
    ) as HTMLElement;
    const imageTarget = e.target as HTMLInputElement | null;
    if (imageTarget) {
      const files = imageTarget.files as FileList;
      if (files.length > 0) {
        imageWrapper.innerHTML = '';
        const image = document.createElement('img');
        image.id = CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE;
        image.className = 'w-100';
        image.src = window.URL.createObjectURL(files[0]);
        imageWrapper.appendChild(image);
        thumbnailLabel.innerHTML = 'Update';
      }
    }
    const removeThumbnailLabelWrapper = document.getElementById(
      CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL_WRAPPER,
    ) as HTMLLabelElement;
    removeThumbnailLabelWrapper?.classList.remove('d-none');
  };

  appendNewThumbnailSrcToVideo = async (
    innerHTML: string,
    videoTitle: string,
    videoObjectUrl: string,
    thumbnailImageFile: File,
    selectedElement: HTMLElement | null,
  ) => {
    let thumbnailObjectUrl;
    const target: string = this.baseClass().uploadTarget();
    const thumbnailObject = await handleUploadImage(
      thumbnailImageFile,
      thumbnailImageFile.name,
      target,
    );
    if (thumbnailObject) thumbnailObjectUrl = thumbnailObject[1];
    this.appendVideoSrcToSelectedInnerHtml(
      innerHTML,
      videoTitle,
      videoObjectUrl,
      thumbnailObjectUrl,
      selectedElement,
    );
  };

  swapVideoFileToVideoFile = (
    title: string,
    videoObjectUrl: string,
    thumbnailObjectUrl: string,
    selectedVideo: HTMLVideoElement,
  ): void => {
    selectedVideo.title = title;
    selectedVideo.src = videoObjectUrl;
    if (thumbnailObjectUrl !== null) {
      selectedVideo.poster = thumbnailObjectUrl;
    } else {
      selectedVideo.removeAttribute('poster');
    }
  };

  swapIframeFileToIframeFile = (
    title: string,
    videoUrlType: string,
    videoObjectUrl: string,
    selectedIframe: HTMLIFrameElement,
  ): void => {
    selectedIframe.title = title;
    selectedIframe.src = videoObjectUrl;
    const superParent = selectedIframe.parentNode as HTMLElement;
    if (
      videoUrlType === 'YOUTUBE' &&
      selectedIframe.style.width === '100%' &&
      selectedIframe.style.height === '100%'
    ) {
      superParent.style.padding = '55% 0 0 0';
      selectedIframe.style.width = '100%';
      selectedIframe.style.height = '100%';
    } else if (
      videoUrlType === 'VIMEO' &&
      selectedIframe.style.width === '100%' &&
      selectedIframe.style.height === '100%'
    ) {
      superParent.style.padding = '42% 0 0 0';
      selectedIframe.style.width = '100%';
      selectedIframe.style.height = '100%';
    }
  };

  getPreviousTagStyle = (selectedTag: HTMLElement) => {
    const borderWidth = window
      .getComputedStyle(selectedTag)
      .getPropertyValue('border-block-start-width')
      .replace('px', '');
    const borderStyle = window
      .getComputedStyle(selectedTag)
      .getPropertyValue('border-block-start-style');
    const borderColor = window
      .getComputedStyle(selectedTag)
      .getPropertyValue('border-block-start-color');
    const widthStyle = window
      .getComputedStyle(selectedTag)
      .getPropertyValue('width')
      .replace('px', '');
    const heightStyle = window
      .getComputedStyle(selectedTag)
      .getPropertyValue('height')
      .replace('px', '');
    return [borderWidth, borderStyle, borderColor, widthStyle, heightStyle];
  };

  swapVideoFileToIframeFile = (
    title: string,
    videoUrlType: string,
    videoObjectUrl: string,
    selectedVideo: HTMLVideoElement,
  ): void => {
    const previousTagStyles = this.getPreviousTagStyle(selectedVideo);
    const superParent = selectedVideo.parentNode as HTMLElement;
    const iframe = document.createElement('iframe');
    iframe.frameBorder = '0';
    iframe.className = CONSTANTS.LOREE_NO_POINTER;
    iframe.src = videoObjectUrl;
    iframe.title = title;
    iframe.style.cssText = `position: absolute;top:0;left:0;width: ${previousTagStyles[3]}px;height: ${previousTagStyles[4]}px;border-width: ${previousTagStyles[0]}px;border-style:${previousTagStyles[1]};border-color: ${previousTagStyles[2]};max-width:100%;`;
    if (superParent) {
      if (
        videoUrlType === 'YOUTUBE' &&
        selectedVideo.style.width === '100%' &&
        selectedVideo.style.height === '100%'
      ) {
        superParent.style.padding = '55% 0 0 0';
        iframe.style.width = '100%';
        iframe.style.height = '100%';
      } else if (videoUrlType === 'VIMEO') {
        superParent.style.padding = '42% 0 0 0';
        iframe.style.width = '100%';
        iframe.style.height = '100%';
      }
      superParent.prepend(iframe);
    }
    selectedVideo.remove();
  };

  swapIframeFileToVideoFile = (
    title: string,
    videoObjectUrl: string,
    thumbnailObjectUrl: string,
    selectedIframe: HTMLIFrameElement,
  ): void => {
    const previousTagStyles = this.getPreviousTagStyle(selectedIframe);
    const superParent = selectedIframe.parentNode as HTMLElement;
    const video = document.createElement('video');
    video.className = CONSTANTS.LOREE_NO_POINTER;
    video.controls = true;
    video.src = videoObjectUrl;
    video.title = title;
    if (thumbnailObjectUrl !== null) {
      video.poster = thumbnailObjectUrl;
    }
    video.style.cssText = `position: absolute;top:0;left:0;width: ${previousTagStyles[3]}px;height: ${previousTagStyles[4]}px;border-width: ${previousTagStyles[0]}px;border-style:${previousTagStyles[1]};border-color: ${previousTagStyles[2]};max-width:100%;`;
    if (superParent) {
      if (selectedIframe.style.width === '100%' && selectedIframe.style.height === '100%') {
        superParent.style.padding = '42% 0 0 0';
        video.style.width = '100%';
        video.style.height = '100%';
      }
      superParent.prepend(video);
    }
    selectedIframe.remove();
  };

  checkVideoSwapType = (
    selectedVideo: HTMLVideoElement,
    selectedIframe: HTMLIFrameElement,
    videoType: string,
  ) => {
    let videoSwapType;
    if (selectedVideo && videoType === 'video') {
      videoSwapType = 'VIDEO_FILE_TO_VIDEO_FILE';
    } else if (selectedIframe && videoType === 'iframe') {
      videoSwapType = 'IFRAME_FILE_TO_IFRAME_FILE';
    } else if (selectedVideo && videoType === 'iframe') {
      videoSwapType = 'VIDEO_FILE_TO_IFRAME_FILE';
    } else if (selectedIframe && videoType === 'video') {
      videoSwapType = 'IFRAME_FILE_TO_VIDEO_FILE';
    }
    return videoSwapType;
  };

  appendReplaceVideoSrcToSelectedVideo = (
    videoType: string,
    title: string,
    videoObjectUrl: string,
    thumbnailObjectUrl: string | null,
    selectedElement: HTMLElement | null,
  ) => {
    const selectedVideo = selectedElement?.getElementsByTagName('video')[0] as HTMLVideoElement;
    const selectedIframe = selectedElement?.getElementsByTagName('iframe')[0] as HTMLIFrameElement;
    const videoUrlType = this.checkIframeVideoType(videoObjectUrl) as string;
    const videoSwapType = this.checkVideoSwapType(selectedVideo, selectedIframe, videoType);
    switch (videoSwapType) {
      case 'VIDEO_FILE_TO_VIDEO_FILE':
        this.swapVideoFileToVideoFile(
          title,
          videoObjectUrl,
          thumbnailObjectUrl ?? '',
          selectedVideo,
        );
        break;
      case 'IFRAME_FILE_TO_IFRAME_FILE':
        this.swapIframeFileToIframeFile(title, videoUrlType, videoObjectUrl, selectedIframe);
        break;
      case 'VIDEO_FILE_TO_IFRAME_FILE':
        this.swapVideoFileToIframeFile(title, videoUrlType, videoObjectUrl, selectedVideo);
        break;
      case 'IFRAME_FILE_TO_VIDEO_FILE':
        this.swapIframeFileToVideoFile(
          title,
          videoObjectUrl,
          thumbnailObjectUrl ?? '',
          selectedIframe,
        );
        break;
    }
    this.baseClass().changeSelectedElement(selectedElement);
  };

  appendNewThumbnailSrcToReplaceVideo = async (
    videoType: string,
    videoTitle: string,
    videoObjectUrl: string,
    thumbnailImageFile: File,
    selectedElement: HTMLElement | null,
  ) => {
    let thumbnailObjectUrl;
    const target: string = this.baseClass().uploadTarget();
    const thumbnailObject = await handleUploadImage(
      thumbnailImageFile,
      thumbnailImageFile.name,
      target,
    );
    if (thumbnailObject) thumbnailObjectUrl = thumbnailObject[1];
    this.appendReplaceVideoSrcToSelectedVideo(
      videoType,
      videoTitle,
      videoObjectUrl,
      thumbnailObjectUrl,
      selectedElement,
    );
  };

  // insert new video into the editor
  videoUploadHandler = (innerHTML: string): void => {
    this.baseClass().hideSubSidebar();
    this.baseClass().hideCloseElementButtonToSelectedElement();
    this.baseClass().collapseElementSection();
    this.baseClass().disableElementSection();
    setVideoDetails('NEW', innerHTML);
    showVideoModal();
  };

  // replace a existing video into the editor
  replaceVideoUploadHandler = () => {
    setVideoDetails('REPLACE', '');
    showVideoModal();
  };
}
