import CONSTANTS from '../../constant';
import { closeIcon, videoModalIcons } from '../sidebar/iconHolder';
import { LmsAccess } from '../../../utils/lmsAccess';
import { translate } from '../../../i18n/translate';
import { isD2l } from '../../../lmsConfig';
import { FeatureInterface } from '../../interface';

const lms = new LmsAccess();

export const appendModalContentHeader = (): HTMLElement => {
  const header = document.createElement('div');
  header.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_HEADER;
  header.className = 'd-flex flex-row flex-wrap justify-content-between align-items-center';
  const headerText = document.createElement('p');
  headerText.innerHTML = translate('global.videos');
  headerText.className = 'modal-title';
  const headerIcon = document.createElement('button');
  headerIcon.id = CONSTANTS.LOREE_VIDEO_MODAL_WRAPPER_CLOSE;
  headerIcon.type = 'button';
  headerIcon.className = 'close';
  headerIcon.innerHTML = closeIcon;
  header.appendChild(headerText);
  header.appendChild(headerIcon);
  return header;
};

export const appendModalContentHeaderDivider = (): HTMLElement => {
  const divider = document.createElement('div');
  divider.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_DIVIDER;
  divider.className = lms.getAccess() && !isD2l() ? '' : 'standalone-modal-divider';
  return divider;
};

export const appendVideoModalFilterContainer = (): HTMLElement => {
  const videoModalHeaderFilter = document.createElement('div');
  videoModalHeaderFilter.className = 'd-flex flex-row justify-content-between';
  videoModalHeaderFilter.id = CONSTANTS.LOREE_VIDEO_MODAL_FILTER_CONTAINER;
  // search bar
  const searchInput = document.createElement('input');
  searchInput.id = CONSTANTS.LOREE_VIDEO_MODAL_FILTER_SEARCH_BOX;
  searchInput.className = 'video-search-box';
  searchInput.name = 'search';
  searchInput.type = 'text';
  searchInput.autocomplete = 'off';
  searchInput.placeholder = translate('global.search');
  // @ts-expect-error
  searchInput.autofocus = true;
  const searchIconDiv = document.createElement('div');
  searchIconDiv.id = CONSTANTS.LOREE_VIDEO_MODAL_FILTER_SEARCH_ICON;
  searchIconDiv.className = 'loree-video-modal-filter-search-icon-style';
  searchIconDiv.innerHTML = videoModalIcons.searchIcon;
  // dropdown
  const filterDropDown = document.createElement('div');
  filterDropDown.className = 'dropdown dropdown-layout';
  const filterDropDownButton = document.createElement('button');
  filterDropDownButton.id = CONSTANTS.LOREE_VIDEO_MODAL_FILTER_BUTTON;
  filterDropDownButton.className =
    'btn btn-primary dropdown-toggle d-flex justify-content-between align-items-center';
  filterDropDownButton.type = 'button';
  filterDropDownButton.setAttribute('data-toggle', 'dropdown');
  filterDropDownButton.innerHTML = translate('modal.recent');
  const dropDownItem = document.createElement('div');
  dropDownItem.className = 'dropdown-menu';
  dropDownItem.id = CONSTANTS.LOREE_VIDEO_MODAL_FILTER_DROPDOWN;
  dropDownItem.setAttribute('aria-labelledby', CONSTANTS.LOREE_VIDEO_MODAL_FILTER_BUTTON);
  const dropDownButton = document.createElement('div');
  dropDownButton.className = 'dropdown-item';
  dropDownButton.innerHTML = translate('modal.recent');
  const dropDownAscendingButton = document.createElement('div');
  dropDownAscendingButton.className = 'dropdown-item';
  dropDownAscendingButton.innerHTML = translate('modal.ascending');
  const dropDownDescedingButton = document.createElement('div');
  dropDownDescedingButton.className = 'dropdown-item';
  dropDownDescedingButton.innerHTML = translate('modal.descending');
  const dropDownSizeButton = document.createElement('div');
  dropDownSizeButton.className = !lms.getAccess() ? 'dropdown-item' : 'd-none';
  dropDownSizeButton.innerHTML = translate('global.size');
  dropDownItem.appendChild(dropDownButton);
  dropDownItem.appendChild(dropDownAscendingButton);
  dropDownItem.appendChild(dropDownDescedingButton);
  dropDownItem.appendChild(dropDownSizeButton);
  filterDropDown.appendChild(filterDropDownButton);
  filterDropDown.appendChild(dropDownItem);
  videoModalHeaderFilter.appendChild(searchInput);
  videoModalHeaderFilter.appendChild(searchIconDiv);
  videoModalHeaderFilter.appendChild(filterDropDown);
  return videoModalHeaderFilter;
};

const appendModalLeftContentHeader = (): HTMLElement => {
  const videoModalLeftContainerHeader = document.createElement('div');
  videoModalLeftContainerHeader.className = 'd-flex flex-row left-container-header';
  const videoModalTitleHeader = document.createElement('div');
  videoModalTitleHeader.className = 'left-container-header-title';
  const titleHeaderNav = document.createElement('nav');
  const titleHeaderNavChild = document.createElement('div');
  titleHeaderNavChild.className = 'nav nav-tabs';
  titleHeaderNavChild.setAttribute('role', 'tablist');
  const titleHeaderNavAnchor = document.createElement('a');
  titleHeaderNavAnchor.className = 'nav-item nav-link modal-tab';
  titleHeaderNavAnchor.id = CONSTANTS.LOREE_VIDEO_MODAL_NAV_LOREE_VIDEOS;
  titleHeaderNavAnchor.setAttribute('data-toggle', 'tab');
  titleHeaderNavAnchor.href = `#${CONSTANTS.LOREE_VIDEO_MODAL_LOREE_VIDEO_CONTAINER}`;
  titleHeaderNavAnchor.innerHTML = translate('modal.kalturavideos');
  titleHeaderNavChild.appendChild(titleHeaderNavAnchor);
  titleHeaderNav.appendChild(titleHeaderNavChild);
  videoModalTitleHeader.appendChild(titleHeaderNav);
  videoModalLeftContainerHeader.appendChild(videoModalTitleHeader);
  videoModalLeftContainerHeader.appendChild(appendVideoModalFilterContainer());
  return videoModalLeftContainerHeader;
};

export const appendVideoModalRightContainer = (
  lms: { getAccess(): boolean | undefined },
  featuresList: FeatureInterface,
  isKalturaConfigured: Boolean | undefined,
): HTMLElement => {
  const rightContainer = document.createElement('div');
  rightContainer.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY_RIGHT_CONTAINER;
  rightContainer.className = 'd-flex align-items-center';
  const rightContainerLayout = document.createElement('div');
  rightContainerLayout.className = `right-container-layout`;
  // Local uploader
  const localUploadBody = document.createElement('div');
  localUploadBody.style.marginBottom = '12px';
  const uploaderInput = document.createElement('div');
  uploaderInput.className = 'upload-add-icon';
  if (lms.getAccess() && !isD2l() && isKalturaConfigured) {
    if (featuresList.uploadnewvideo) {
      const uploadButton = document.createElement('button');
      uploadButton.id = CONSTANTS.LOREE_VIDEO_LOCAL_UPLOAD_INPUT;
      uploadButton.innerHTML = `<span>${videoModalIcons.addIcon}</span>
    Upload New Video`;
      uploaderInput.appendChild(uploadButton);
      const uploadMessage = document.createElement('div');
      uploadMessage.id = CONSTANTS.LOREE_VIDEO_LOCAL_UPLOAD_INPUT_MESSAGE;
      uploadMessage.innerHTML = `<p>${translate('modal.supportedfiles')} - .mp4, .mov.</p>
    <p>${translate('modal.morethan30mbinsizewilltaketimetoload')}</p>`;
      localUploadBody.appendChild(uploaderInput);
      localUploadBody.appendChild(uploadMessage);
      rightContainerLayout.appendChild(localUploadBody);
    }
  }
  // youtube uploader
  if (featuresList.youtube) {
    const youTubeUploader = document.createElement('div');
    youTubeUploader.style.marginBottom = '12px';
    const youTubeIcon = document.createElement('span');
    youTubeIcon.innerHTML = videoModalIcons.youtubeIcon;
    const youTubeInput = document.createElement('input');
    youTubeInput.id = CONSTANTS.LOREE_VIDEO_YOUTUBE_INPUT;
    youTubeInput.type = 'text';
    youTubeInput.placeholder = translate('global.insertlink');
    youTubeInput.autocomplete = 'off';
    youTubeUploader.appendChild(youTubeIcon);
    youTubeUploader.appendChild(youTubeInput);
    rightContainerLayout.appendChild(youTubeUploader);
  }
  // vimeo uploader
  if (featuresList.vimeo) {
    const vimeoUploader = document.createElement('div');
    vimeoUploader.style.marginBottom = '12px';
    const vimeoIcon = document.createElement('span');
    vimeoIcon.innerHTML = videoModalIcons.vimeoIcon;
    const vimeoInput = document.createElement('input');
    vimeoInput.id = CONSTANTS.LOREE_VIDEO_VIMEO_INPUT;
    vimeoInput.type = 'text';
    vimeoInput.placeholder = translate('global.insertlink');
    vimeoInput.autocomplete = 'off';
    vimeoUploader.appendChild(vimeoIcon);
    vimeoUploader.appendChild(vimeoInput);
    rightContainerLayout.appendChild(vimeoUploader);
  }
  // byurl uploader
  if (featuresList.insertbyurl) {
    const byUrlUploader = document.createElement('div');
    byUrlUploader.style.marginBottom = '12px';
    const byUrlIcon = document.createElement('span');
    byUrlIcon.innerHTML = videoModalIcons.addIcon;
    const byUrlInput = document.createElement('input');
    byUrlInput.id = CONSTANTS.LOREE_VIDEO_BY_URL_INPUT;
    byUrlInput.type = 'text';
    byUrlInput.placeholder = translate('global.insertlink');
    byUrlInput.autocomplete = 'off';
    byUrlUploader.appendChild(byUrlIcon);
    byUrlUploader.appendChild(byUrlInput);
    // appended all uploader
    rightContainerLayout.appendChild(byUrlUploader);
  }
  rightContainer.appendChild(rightContainerLayout);
  return rightContainer;
};

const appendLeftContainer = (videoModalBody: HTMLDivElement): void => {
  const videoModalBodyLeftContainerParent = document.createElement('div');
  videoModalBodyLeftContainerParent.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY_LEFT_CONTAINER;
  videoModalBodyLeftContainerParent.className = 'd-flex flex-column';
  videoModalBodyLeftContainerParent.appendChild(appendModalLeftContentHeader());
  const tabContent = document.createElement('div');
  tabContent.id = 'nav-tabContent';
  tabContent.innerHTML = `<div class="tab-pane fade modal-tab-container" id=${CONSTANTS.LOREE_VIDEO_MODAL_LOREE_VIDEO_CONTAINER} role="tabpanel" aria-labelledby=${CONSTANTS.LOREE_VIDEO_MODAL_NAV_LOREE_VIDEOS}>
    <div id="${CONSTANTS.LOREE_VIDEO_LIST_CONTAINER}" class="d-flex flex-row flex-wrap"></div>
    </div>`;
  videoModalBodyLeftContainerParent.appendChild(tabContent);
  videoModalBody.appendChild(videoModalBodyLeftContainerParent);
};

const appendMiddleContainer = (
  videoModalBody: HTMLDivElement,
  featuresList: FeatureInterface,
): void => {
  const middleContainer = document.createElement('div');
  middleContainer.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY_MIDDLE_CONTAINER;
  middleContainer.className = 'd-flex align-items-center';
  if (
    featuresList.youtube ||
    featuresList.vimeo ||
    featuresList.uploadnewvideo ||
    featuresList.insertbyurl
  ) {
    middleContainer.innerHTML = `<div>(${translate('global.or')})</div>`;
  }
  videoModalBody.appendChild(middleContainer);
};

export const appendModalContentBody = (
  featuresList: FeatureInterface,
  isKalturaConfigured: Boolean | undefined,
): HTMLElement => {
  const videoModalBody = document.createElement('div');
  videoModalBody.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_BODY;
  videoModalBody.className = 'd-flex flex-row flex-nowrap';
  if (lms.getAccess() && !isD2l() && isKalturaConfigured) {
    appendLeftContainer(videoModalBody); // left container
    appendMiddleContainer(videoModalBody, featuresList); // middle container
  }
  videoModalBody.appendChild(
    appendVideoModalRightContainer(lms, featuresList, isKalturaConfigured),
  ); // right container
  return videoModalBody;
};

export const appenVideoDetailModal = (): HTMLElement => {
  const detailModalContainer = document.createElement('div');
  detailModalContainer.id = CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_CONTENT_BODY;
  detailModalContainer.className =
    'd-none d-flex flex-column flex-md-row video-details-modal-wrapper';
  detailModalContainer.innerHTML = `<div class="col-12 col-md-6 pl-0" style="width: 450px;">
    <div id=${CONSTANTS.LOREE_VIDEO_DETAILS_PREVIEW} class="w-100 d-flex video-preview-block justify-content-center align-items-center"></div>
  </div>`;
  const videoModalRightContainer = document.createElement('div');
  videoModalRightContainer.className = 'col-12 col-md-6 pr-0';
  videoModalRightContainer.style.width = '450px';
  const videoSpecificationPreview = document.createElement('div');
  videoSpecificationPreview.className = 'video-details-specifications d-flex flex-column';
  videoSpecificationPreview.innerHTML = `  <div class="d-flex flex-column">
    <label class="pb-1 mb-0">Title name*</label>
    <input id="${
      CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_TITLE_INPUT
    }" type="text" required class="p-1" autocomplete="off" />
  </div>
  <div id="${
    CONSTANTS.LOREE_VIDEO_DETAILS_MODAL_THUMBNAIL_BLOCK
  }" class="d-none d-flex flex-row mt-3">
    <div>
      <label class="pb-1 mb-0">${translate('global.thumbnailimage')}</label>
      <div class="video-thumbnail-image position-relative d-flex justify-content-end flex-column">
        <div id="${
          CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_IMAGE_WRAPPER
        }" style="min-height: 80px; max-height: 80px"></div>
        <div class="d-flex flex-row">
          <div class="w-100">
            <label id="${
              CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_LABEL
            }" class="video-thumbnail-btn mb-0 w-100 text-center p-1">${translate(
    'modal.addthumbnail',
  )}</label>
            <input id="${
              CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_INPUT
            }" type="file" size="60" class="d-none" accept="image/*" /></div>
          <div id="${
            CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL_WRAPPER
          }" class="w-100 d-none">
            <label id="${
              CONSTANTS.LOREE_VIDEO_DETAILS_THUMBNAIL_REMOVE_LABEL
            }" class="video-thumbnail-btn mb-0 w-100 text-center p-1" style="border-left: 1px solid #909090">${translate(
    'global.remove',
  )}</label>
          </div>
        </div>
      </div>
    </div>
  </div>`;
  videoModalRightContainer.appendChild(videoSpecificationPreview);
  detailModalContainer.appendChild(videoModalRightContainer);
  return detailModalContainer;
};

export const appendModelContentFooter = (): HTMLElement => {
  const contentFooter = document.createElement('div');
  contentFooter.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER;
  contentFooter.className = 'd-flex flex-row flex-nowrap align-items-center';
  const footerButton = document.createElement('div');
  footerButton.style.margin = 'auto';
  const backButton = document.createElement('button');
  backButton.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_BACK_BUTTON;
  backButton.innerHTML = translate('global.back');
  backButton.className = 'mx-1';
  const cancelButton = document.createElement('button');
  cancelButton.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_CANCEL_BUTTON;
  cancelButton.innerHTML = translate('global.cancel');
  cancelButton.className = 'mx-1';
  const addButton = document.createElement('button');
  addButton.id = CONSTANTS.LOREE_VIDEO_MODAL_CONTENT_FOOTER_ADD_BUTTON;
  addButton.innerHTML = translate('global.add');
  addButton.className = 'editor-btn-primary mx-1';
  footerButton.appendChild(backButton);
  footerButton.appendChild(cancelButton);
  footerButton.appendChild(addButton);
  contentFooter.appendChild(footerButton);
  return contentFooter;
};
