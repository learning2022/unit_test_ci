import Base, {
  backgroundColorPickerPopperInstance,
  rowColumnBorderColorPickerPopperInstance,
} from '../../base';
import CONSTANTS from '../../constant';
import { blockOptionsIcon } from '../../iconHolder';
import {
  marginSet,
  paddingSet,
  columnSet,
  twoColumnSet,
  threeColumnSet,
} from './editOptionsColumnSet';
import { ColumnPropsAlert, columnPropsAlert } from '../../alert';
import { ConfigInterface } from '../../interface';
import {
  removeClassToElement,
  addClassToElement,
  getEditorElementById,
  getIframedocumentElementById,
  createBorderStyleOptions,
  selectedIndexForBorderStyle,
} from '../../utils';
import { a11yUpdateHighlightsService } from '../../../views/editor/observerService';
import { rgbToHex } from '../colorPicker/utils';

const customColors: string[] = [];

class EditOptions extends Base {
  initiate = (config: ConfigInterface): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const blockEditOptionsWrapper = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER,
      );
      if (blockEditOptionsWrapper) {
        this.appendCustomStyles(config);
        this.appendBlockEditArrow(blockEditOptionsWrapper);
        this.appendAlignmentEditSection(blockEditOptionsWrapper);
        this.appendMarginInputFields();
        this.appendPaddingInputFields();
        if (
          config.features?.rowstructure !== undefined ||
          config.features?.rowstructure !== false
        ) {
          this.appendDivider(blockEditOptionsWrapper);
          this.appendBackButton(blockEditOptionsWrapper);
          this.appendColumnSet(blockEditOptionsWrapper);
          this.appendTwoColumnSet(blockEditOptionsWrapper);
          this.appendThreeColumnSet(blockEditOptionsWrapper);
        }
      }
      this.initializeEditColumn();
    }
  };

  addTwoCol = (addView: (HTMLElement | null)[], removeView: (HTMLElement | null)[]): void => {
    let show;
    let hide;
    for (show of addView) {
      if (show) {
        removeClassToElement(show, 'd-none');
      }
    }
    for (hide of removeView) {
      if (hide) {
        addClassToElement(hide, 'd-none');
      }
    }
  };

  removeTwoCol = (addView: (HTMLElement | null)[], removeView: (HTMLElement | null)[]): void => {
    let show;
    let hide;
    for (show of addView) {
      if (show) {
        addClassToElement(show, 'd-none');
      }
    }
    for (hide of removeView) {
      if (hide) {
        removeClassToElement(hide, 'd-none');
      }
    }
  };

  addThreeCol = (
    addView: (HTMLElement | null)[],
    removeView: (HTMLElement | null)[],
  ): (HTMLElement | null)[][] => {
    let show;
    let hide;
    for (show of addView) {
      if (show) this.addaddView(show);
    }
    for (hide of removeView) {
      if (hide) this.addremoveView(hide);
    }
    const arrayElement = [addView, removeView];
    return arrayElement;
  };

  addaddView = (addView: HTMLElement): HTMLElement => {
    removeClassToElement(addView, 'd-none');
    return addView;
  };

  addremoveView = (removeView: HTMLElement): HTMLElement => {
    addClassToElement(removeView, 'd-none');
    return removeView;
  };

  removeThreeCol = (addView: (HTMLElement | null)[], removeView: (HTMLElement | null)[]): void => {
    let show;
    let hide;
    for (show of addView) {
      if (show) this.removeaddView(show);
    }
    for (hide of removeView) {
      if (hide) this.removeremoveView(hide);
    }
  };

  removeaddView = (addView: HTMLElement): HTMLElement => {
    addClassToElement(addView, 'd-none');
    return addView;
  };

  removeremoveView = (removeView: HTMLElement): HTMLElement => {
    removeClassToElement(removeView, 'd-none');
    return removeView;
  };

  initializeEditColumn = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const twoColEvent = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_TWO}`,
      );
      const threeColEvent = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_THREE}`,
      );
      const backOptionEvent = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON}`,
      );
      const backthreeOptionEvent = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON}`,
      );
      const sectionSpaceBlock = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_SECTION_WRAPPER}`,
      );
      const pickerSection = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_SECTION_WRAPPER}`,
      );
      const borderColorSection = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_SECTION_WRAPPER}`,
      );
      const sectionDivider = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_SECTION_DIVIDER}`,
      );
      const columnSetParent = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_PARENT}`,
      );
      const twoCol = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_PARENT}`,
      );
      const threeCol = getIframedocumentElementById(
        iframeDocument,
        `${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_PARENT}`,
      );
      const addView = [backOptionEvent, twoCol];
      const addThreeView = [backOptionEvent, threeCol];
      const removeView = [
        sectionSpaceBlock,
        pickerSection,
        borderColorSection,
        sectionDivider,
        columnSetParent,
      ];
      if (twoColEvent !== null) {
        twoColEvent.addEventListener('click', () => this.addTwoCol(addView, removeView));
      }
      if (threeColEvent !== null) {
        threeColEvent.addEventListener('click', () => this.addThreeCol(addThreeView, removeView));
      }
      if (backOptionEvent !== null) {
        backOptionEvent.addEventListener('click', () => this.removeTwoCol(addView, removeView));
      }
      if (backthreeOptionEvent !== null) {
        backthreeOptionEvent.addEventListener('click', () =>
          this.removeThreeCol(addThreeView, removeView),
        );
      }
    }
  };

  appendCustomStyles = (config: _Any) => {
    const colors = config.customColor?.colors;
    if (colors?.length) {
      colors.forEach((clr: { color: string }) => {
        customColors.push(clr.color);
      });
    }
  };

  appendBlockEditArrow = (wrapper: HTMLElement): void => {
    const arrowPointer = document.createElement('div');
    arrowPointer.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ARROW;
    arrowPointer.innerHTML = `
    <div id="${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ARROW_REFERENCE_ID}" data-popper-arrow>
      <div class="editOptionArrowWrapper top">
          <div class="editOptionArrow top"></div>
      </div>
    </div>`;
    wrapper.appendChild(arrowPointer);
  };

  appendAlignmentEditSection = (wrapper: HTMLElement): void => {
    const alignmentEditSection = document.createElement('div');
    alignmentEditSection.className = 'd-flex flex-row flex-nowrap';
    alignmentEditSection.innerHTML = `
    <div id="${
      CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_SECTION_WRAPPER
    }" class="section-space-block">
        <div class="d-flex flex-row flex-nowrap justify-content-between" style="margin-bottom: 13px;">
            <div class="label-style">Margin (px)</div>
    ${marginSet
      .map(
        (marginSetValue: { className: string }, index: number) =>
          `<div id="${marginSetValue.className}" class="${marginSetValue.className}" key=${index}></div>`,
      )
      .join('')}
        </div>
        <div class="d-flex flex-row flex-nowrap justify-content-between">
            <div class="label-style">Padding (px)</div>
    ${paddingSet
      .map(
        (paddingSetValue: { className: string }, index: number) =>
          `<div id="${paddingSetValue.className}" class="${paddingSetValue.className}" key=${index}>
            </div>
        `,
      )
      .join('')}
        </div>
    </div>`;
    const colorpickersWrapper = document.createElement('div');
    colorpickersWrapper.className = 'picker-section';
    const backgroundColorPicker = this.rowAndColumnBackgroundColorPicker();
    colorpickersWrapper.appendChild(backgroundColorPicker);
    const borderColorPicker = this.rowAndColumnBorderColorPicker();
    colorpickersWrapper.appendChild(borderColorPicker);
    alignmentEditSection.appendChild(colorpickersWrapper);
    wrapper.appendChild(alignmentEditSection);
  };

  rowAndColumnBackgroundColorPicker = (): HTMLElement => {
    // wrapper
    const bgColorPicker = document.createElement('div');
    bgColorPicker.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_SECTION_WRAPPER;
    bgColorPicker.className = 'rowColumnBackgroundColorPicker';

    // label
    const label = document.createElement('div');
    label.className = 'label-style';
    label.innerHTML = 'Background';
    bgColorPicker.appendChild(label);

    // color picker
    const bgColorPickerButton = document.createElement('button');
    bgColorPickerButton.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_BUTTON;
    bgColorPickerButton.className = 'btn color-picker color-picker-row-column';
    bgColorPickerButton.onclick = this.handleBackgroundColorPicker;
    bgColorPicker.appendChild(bgColorPickerButton);
    return bgColorPicker;
  };

  appendDivider = (wrapper: HTMLElement): void => {
    const divider = document.createElement('div');
    divider.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_SECTION_DIVIDER;
    divider.className = 'divider';
    wrapper.appendChild(divider);
  };

  appendBackButton = (wrapper: HTMLElement): void => {
    const backButton = document.createElement('div');
    backButton.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON;
    backButton.className = 'mt-n2 mb-2 ml-n2 back-to-editOption d-none';
    backButton.innerHTML = `${blockOptionsIcon.leftarrowElementIcon}`;
    wrapper.appendChild(backButton);
  };

  appendColumnSet = (wrapper: HTMLElement): void => {
    const editColumnSet = document.createElement('div');
    editColumnSet.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_PARENT;
    editColumnSet.innerHTML = `
    <div class="d-flex flex-row flex-wrap">
    ${columnSet
      .map(
        (columnSetValue: { id: string; className: string; numberOfSet: string[] }, index: number) =>
          `<div id="${columnSetValue.id}" class="d-flex flex-row justify-content-between ${
            columnSetValue.className
          }" key=${index} onClick={}>
    ${columnSetValue.numberOfSet
      .map(
        (setCount: string, setCountIndex: number) =>
          `<div class=${setCount} key=${setCountIndex}></div>`,
      )
      .join('')}
            </div>
        `,
      )
      .join('')}
    </div>`;
    editColumnSet.onclick = (e: Event): void => {
      this.replaceCol(e);
    };
    wrapper.appendChild(editColumnSet);
  };

  appendTwoColumnSet = (wrapper: HTMLElement): void => {
    const editTwoColumnSet = document.createElement('div');
    editTwoColumnSet.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_PARENT;
    editTwoColumnSet.className = 'd-none';
    editTwoColumnSet.innerHTML = `
    <div class="d-flex flex-row flex-wrap">
    ${twoColumnSet
      .map(
        (columnSetValue: { id: string; className: string; columnType: string[] }, index: number) =>
          `<div id="${
            columnSetValue.id
          }" identity="two-column" class="d-flex flex-row justify-content-between ${
            columnSetValue.className
          }" key=${index}>
    ${columnSetValue.columnType
      .map(
        (setCount: string, setCountIndex: number) =>
          `<div class=${setCount} key=${setCountIndex}></div>`,
      )
      .join('')}
            </div>
        `,
      )
      .join('')}
    </div>`;
    editTwoColumnSet.onclick = (e: Event): void => {
      this.replaceCol(e);
    };
    wrapper.appendChild(editTwoColumnSet);
  };

  appendThreeColumnSet = (wrapper: HTMLElement): void => {
    const editThreeColumnSet = document.createElement('div');
    editThreeColumnSet.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_PARENT;
    editThreeColumnSet.className = 'd-none';
    editThreeColumnSet.innerHTML = `
    <div class="d-flex flex-row flex-wrap">
    ${threeColumnSet
      .map(
        (columnSetValue: { id: string; className: string; columnType: string[] }, index: number) =>
          `<div id="${
            columnSetValue.id
          }" identity="three-column" class="d-flex flex-row justify-content-between ${
            columnSetValue.className
          }" key=${index}>
    ${columnSetValue.columnType
      .map(
        (setCount: string, setCountIndex: number) =>
          `<div class=${setCount} key=${setCountIndex}></div>`,
      )
      .join('')}
            </div>
        `,
      )
      .join('')}
    </div>`;
    editThreeColumnSet.onclick = (e: Event): void => {
      this.replaceCol(e);
    };
    wrapper.appendChild(editThreeColumnSet);
  };

  ChangeColClassName = (htmlClass: string[], element: HTMLElement | null, count: number): void => {
    if (!element) return;
    for (let j = 0; j < count; j++) {
      element.children[j].setAttribute(
        'class',
        [htmlClass[j], CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN].join(' '),
      );
      (element.children[j] as HTMLElement).style.padding += '10px';
    }
    this.changeSelectedElement(element.children[0] as HTMLElement);
    a11yUpdateHighlightsService.updateHighlightsIdentity();
  };

  replaceCol = (e: Event): void => {
    this.closeBackgroundRowColumnColorPicker();
    const targetParent = (e.target as HTMLElement)?.parentElement;
    let colSet;
    if (
      targetParent?.id === 'one-col-set' ||
      targetParent?.id === 'four-col-set' ||
      targetParent?.id === 'five-col-set' ||
      targetParent?.id === 'six-col-set'
    ) {
      colSet = columnSet;
    } else if (targetParent?.getAttribute('identity') === 'three-column') {
      colSet = threeColumnSet;
    } else {
      colSet = twoColumnSet;
    }
    for (const col of colSet) {
      if (targetParent?.id === col.id) {
        const selectedElement: HTMLElement | null = this.getSelectedElement();
        if (selectedElement) {
          const addElement = col.htmlClass.length - selectedElement.childElementCount;
          if (addElement === 0) {
            this.ChangeColClassName(
              col.htmlClass,
              selectedElement,
              selectedElement.childElementCount,
            );
          } else if (addElement > 0) {
            const element = document.createElement('div');
            for (let i = 0; i < addElement; i++) {
              selectedElement.appendChild(element.cloneNode(true));
            }
            this.ChangeColClassName(
              col.htmlClass,
              selectedElement,
              selectedElement.childElementCount,
            );
            this.handleSelectedContentChanges();
          } else if (addElement < 0) {
            ColumnPropsAlert();
            const columnPropsYesBtn = getEditorElementById('deleteColumnProps');
            if (columnPropsYesBtn) {
              columnPropsYesBtn.onclick = (): void =>
                this.deleteColumnProp(addElement, selectedElement, col);
            }
          }
        }
      }
    }
  };

  deleteColumnProp = (
    addElement: number,
    selectedElement: HTMLElement | null,
    col: { htmlClass: string[] },
  ): void => {
    if (!selectedElement) return;
    const placePreviousCol: HTMLInputElement = getEditorElementById(
      'placeInPreviousCol',
    ) as HTMLInputElement;
    if (placePreviousCol?.checked) {
      if (selectedElement.lastElementChild) {
        for (let i = 0; i > addElement; i--) {
          const deletedDiv = selectedElement.lastElementChild as HTMLElement;
          selectedElement.lastElementChild.remove();
          const tempArray: ChildNode[] = [];
          deletedDiv.childNodes.forEach((element) => {
            tempArray.push(element);
          });
          for (let i = 0; i < tempArray.length; i++) {
            selectedElement.lastElementChild?.appendChild(tempArray[i]);
          }
        }
        this.ChangeColClassName(col.htmlClass, selectedElement, selectedElement.childElementCount);
      }
    } else {
      for (let i = 0; i > addElement; i--) {
        if (selectedElement.lastElementChild) {
          selectedElement.lastElementChild.remove();
        }
      }
      this.ChangeColClassName(col.htmlClass, selectedElement, selectedElement.childElementCount);
    }
    this.handleSelectedContentChanges();
    columnPropsAlert();
  };

  appendMarginInputFields = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      marginSet
        .map(
          (
            marginSetValue: { id: string; className: string; btnClassName: string },
            index: number,
          ) => {
            const wrapper = getIframedocumentElementById(
              iframeDocument,
              `${marginSetValue.className}`,
            );
            const inputElement = document.createElement('input');

            inputElement.id = marginSetValue.id;
            inputElement.className = `${marginSetValue.btnClassName} ${index} rowColumnAlignmentOption`;
            inputElement.type = 'number';
            inputElement.autocomplete = 'off';
            inputElement.oninput = (e: Event): void => this.onInput(e);
            inputElement.onkeydown = (e: KeyboardEvent): void => this.onKeyDown(e);
            inputElement.onkeypress = (e): void => this.inputOnchangeEvent(e);
            inputElement.name = marginSetValue.id;
            if (wrapper) {
              wrapper.appendChild(inputElement);
            }
            return null;
          },
        )
        .join('');
    }
  };

  appendPaddingInputFields = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      paddingSet
        .map(
          (
            paddingSetValue: { id: string; className: string; btnClassName: string },
            index: number,
          ) => {
            const wrapper = getIframedocumentElementById(
              iframeDocument,
              `${paddingSetValue.className}`,
            );
            const inputElement = document.createElement('input');
            inputElement.id = paddingSetValue.id;
            inputElement.className = `${paddingSetValue.btnClassName} ${index} rowColumnAlignmentOption`;
            inputElement.type = 'number';
            inputElement.autocomplete = 'off';
            inputElement.oninput = (e: Event): void => this.onInput(e);
            inputElement.onkeydown = (e: KeyboardEvent): void => this.onKeyDown(e);
            inputElement.onkeypress = (e): void => this.inputOnchangeEvent(e);
            inputElement.name = paddingSetValue.id;
            if (wrapper) {
              wrapper.appendChild(inputElement);
            }
            return null;
          },
        )
        .join('');
    }
  };

  handleBackgroundColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const button = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLOR_PICKER_BUTTON,
      );
      const wrapper = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_ROW_COLUMN_BACKGROUND_COLOR_PICKER_WRAPPER,
      );
      if (button && wrapper) {
        if (wrapper.style.display === 'none') {
          this.setBackgroundRowColumnColorPicker();
          this.openIframeColorPicker(backgroundColorPickerPopperInstance, [50, 5], button, wrapper);
          this.customiseRowColumnBackGroundColorPicker();
        } else {
          this.closeBackgroundRowColumnColorPicker();
        }
      }
    }
  };

  setBackgroundRowColumnColorPicker = (): void => {
    const iframe = getEditorElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
    if (!iframe || !iframe.contentWindow) return;
    const selectedElement = this.getSelectedElement();
    let color: string = '';
    if (selectedElement && selectedElement.style.backgroundColor !== '') {
      color = selectedElement.style.backgroundColor;
    } else {
      color = 'rgb(255, 255, 255)';
    }
    const data = {
      key: 'setRowColumnBackgroundColor',
      value: rgbToHex(color, ''),
      swatches: customColors,
    };
    this.getSelectedBlockOptionMenu()
      ? this.updateUserRowBackGroundColor(color)
      : this.updateUserColumnBackGroundColor(color);
    iframe.contentWindow.postMessage(data, window.location.origin);
  };

  rowAndColumnBorderColorPicker = (): HTMLElement => {
    // wrapper
    const borderColorPicker = document.createElement('div');
    borderColorPicker.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_SECTION_WRAPPER;
    borderColorPicker.className = 'rowColumnBorderColorPicker';

    // label
    const borderLabel = document.createElement('div');
    borderLabel.className = 'label-style';
    borderLabel.innerHTML = 'Border';
    borderColorPicker.appendChild(borderLabel);

    // color picker
    const borderColorPickerButton = document.createElement('button');
    borderColorPickerButton.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON;
    borderColorPickerButton.className = 'btn color-picker color-picker-row-column';
    borderColorPickerButton.onclick = this.handleBorderColorPicker;
    borderColorPicker.appendChild(borderColorPickerButton);
    return borderColorPicker;
  };

  handleBorderColorPicker = (): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const button = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON,
      );
      const wrapper = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
      );
      if (button && wrapper) {
        if (wrapper.style.display === 'none') {
          this.setBorderRowColumnColorPicker();
          this.openIframeColorPicker(
            rowColumnBorderColorPickerPopperInstance,
            [50, 5],
            button,
            wrapper,
          );
          this.setBorderWidthAndStyle();
          this.appendBorderOptionForRowColumn();
          this.customiseRowColumnBorderColorPicker();
        } else {
          this.closeBorderRowColumnColorPicker();
        }
      }
    }
  };

  setBorderRowColumnColorPicker = (): void => {
    const iframe = getEditorElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
    if (!iframe || !iframe.contentWindow) return;
    const selectedElement = this.getSelectedElement();
    let color = '#ffffff';
    if (selectedElement && selectedElement.style.borderColor !== '') {
      color = selectedElement.style.borderColor;
    }
    const data = {
      key: 'setRowColumnBorderColor',
      value: rgbToHex(color, ''),
      swatches: customColors,
    };
    !this.getSelectedBlockOptionMenu()
      ? this.updateUserColumnBorderColor(color)
      : this.updateUserRowBorderColor(color);
    iframe.contentWindow.postMessage(data, window.location.origin);
  };

  setBorderWidthAndStyle = (): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    let borderWidth = '0';
    if (selectedElement.style.borderWidth) {
      borderWidth = selectedElement.style.borderWidth.split('px')[0];
    }
    !this.getSelectedBlockOptionMenu()
      ? this.updateUserColumnBorderWidth(borderWidth)
      : this.updateUserRowBorderWidth(borderWidth);
    let borderStyle: string = 'solid';
    if (selectedElement.style.borderStyle) {
      borderStyle = selectedElement.style.borderStyle;
    }
    !this.getSelectedBlockOptionMenu()
      ? this.updateColumnBorderStyle(borderStyle)
      : this.updateRowBorderStyle(borderStyle);
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const input = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_ROW_COLUMN_BORDER_WIDTH_COLOR_PICKER,
    ) as HTMLInputElement;
    if (input) input.value = borderWidth;
    const select = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_ROW_COLUMN_BORDER_STYLE_COLOR_PICKER,
    ) as HTMLSelectElement;
    if (select) {
      select.selectedIndex = selectedIndexForBorderStyle(select, borderStyle);
    }
  };

  appendBorderOptionForRowColumn = (): void => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const wrapper = getIframedocumentElementById(
      iframeDocument,
      CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
    );
    if (!wrapper) return;
    const borderOptionWrapper = wrapper.getElementsByClassName('rowColumnBorderOptionWrapper');
    if (borderOptionWrapper.length <= 0) {
      const swatchesWrapper = wrapper.getElementsByClassName('pcr-swatches')[0] as HTMLElement;
      if (!swatchesWrapper) return;
      this.attachBorderOptionWrapper(swatchesWrapper);
    }
  };

  attachBorderOptionWrapper = (wrapper: HTMLElement): void => {
    const borderOptionWrapper = document.createElement('div');
    borderOptionWrapper.className = 'rowColumnBorderOptionWrapper';

    // label
    const label = this.getRowColumnBorderLabel();
    borderOptionWrapper.appendChild(label);

    // Input
    const input = this.getRowColumnBorderInput();
    borderOptionWrapper.appendChild(input);

    // Input
    const select = this.getRowColumnBorderSelect();
    borderOptionWrapper.appendChild(select);

    if (!wrapper.parentNode) return;
    wrapper.parentNode.insertBefore(borderOptionWrapper, wrapper.nextSibling);
  };

  getRowColumnBorderLabel = (): HTMLElement => {
    const div = document.createElement('div');
    div.className = 'rowColumnBorderLabel';
    div.innerHTML = 'Width';
    return div;
  };

  getRowColumnBorderInput = (): HTMLElement => {
    const input = document.createElement('input');
    input.className = 'rowColumnBorderInput';
    input.name = 'rowColumnBorderInput';
    input.id = CONSTANTS.LOREE_ROW_COLUMN_BORDER_WIDTH_COLOR_PICKER;
    input.type = 'number';
    input.value = '0';
    input.autocomplete = 'off';
    input.onchange = (e) => this.rowColumnBorderWidthChange(e);
    return input;
  };

  getRowColumnBorderSelect = (): HTMLElement => {
    const select = document.createElement('select');
    select.className = 'rowColumnBorderSelect';
    select.name = 'rowColumnBorderSelect';
    select.value = 'solid';
    select.id = CONSTANTS.LOREE_ROW_COLUMN_BORDER_STYLE_COLOR_PICKER;
    select.innerHTML = createBorderStyleOptions();
    select.selectedIndex = 3;
    select.onchange = (e) => this.rowColumnBorderStyleChange(e);
    return select;
  };
}

export default EditOptions;
