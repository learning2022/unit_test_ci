import { fireEvent } from '@testing-library/react';
import { getElementById } from '../../common/dom';
import CONSTANTS from '../../constant';
import BlockOption from './blockOptions';
import * as interactive from '../interactive';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#blockOptions', () => {
  const blockOptions = new BlockOption();
  let element: HTMLDivElement;
  beforeEach(() => {
    element = document.createElement('iframe');
    element.className = 'loree-block-options';
    element.id = 'loree-block-options-wrapper';
    document.body.append(element);
  });
  test('appendExternalLinkOption', () => {
    blockOptions.appendExternalOption(element);
    expect(
      document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_EXTERNAL_LINK_BUTTON),
    ).toHaveAttribute('title', 'global.insertlink');
  });
  test('appendSaveButton', () => {
    blockOptions.appendSaveOption(element);
    expect(document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_SAVE_BUTTON)).toHaveAttribute(
      'id',
      CONSTANTS.LOREE_BLOCK_OPTIONS_SAVE_BUTTON,
    );
  });
  test('appendMoveTopOption', () => {
    blockOptions.appendTopOption(element);
    expect(document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON)).toHaveAttribute(
      'title',
      'blockoption.movetop',
    );
  });
  test('appendBottomOption', () => {
    blockOptions.appendBottomOption(element);
    expect(
      document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON),
    ).toHaveAttribute('title', 'blockoption.movebottom');
  });
  test('appendLeftOption', () => {
    blockOptions.appendLeftOption(element);
    expect(document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_LEFT_BUTTON)).toHaveAttribute(
      'title',
      'blockoption.moveleft',
    );
  });
  test('appendRightOption', () => {
    blockOptions.appendRightOption(element);
    expect(
      document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_RIGHT_BUTTON),
    ).toHaveAttribute('title', 'blockoption.moveright');
  });
  test('appendDeleteOption', () => {
    blockOptions.appendDeleteOption(element);
    expect(document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON)).toHaveAttribute(
      'title',
      'global.delete',
    );
  });
  test('appendDuplicateOption', () => {
    blockOptions.appendDuplicateOption(element);
    expect(document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_DUPLICATE_BUTTON)).toHaveAttribute(
      'title',
      'global.duplicate',
    );
  });
  test('appendInteractiveEditButtom', () => {
    blockOptions.appendInteractiveEditOption(element);
    expect(getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON)).toHaveAttribute(
      'aria-label',
      'editInteractive',
    );
  });
  test('edit interactive using block option', () => {
    blockOptions.appendInteractiveEditOption(element);
    blockOptions.getSelectedElement = jest.fn().mockImplementation(() => element);
    const editMock = jest.spyOn(interactive, 'editInteractiveUsingBlockOption');
    const editButton = getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON);
    fireEvent.click(editButton);
    expect(editMock).toBeCalledWith(element);
  });
  test('appendLanguageOption', () => {
    blockOptions.appendLanguagesOption(element);
    expect(getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON)).toHaveAttribute(
      'title',
      'blockoption.languages',
    );
  });
});
