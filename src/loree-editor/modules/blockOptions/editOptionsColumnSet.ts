import CONSTANTS from '../../constant';
interface MarginSet {
  id: string;
  className: string;
  btnClassName: string;
}
interface PaddingSet {
  id: string;
  className: string;
  btnClassName: string;
}
interface ColumnSet {
  id: string;
  numberOfSet: string[];
  htmlClass: string[];
  className: string;
}
interface TwoColumnContainerSet {
  id: string;
  className: string;
  htmlClass: string[];
  columnType: string[];
}
interface ThreeColumnContainerSet {
  id: string;
  className: string;
  htmlClass: string[];
  columnType: string[];
}

export const marginSet: MarginSet[] = [
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_LEFT,
    className: 'margin-left-border field-style',
    btnClassName: 'margin-left',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_RIGHT,
    className: 'margin-right-border field-style',
    btnClassName: 'margin-right',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_TOP,
    className: 'margin-top-border field-style',
    btnClassName: 'margin-top',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_MARGIN_BOTTOM,
    className: 'margin-bottom-border field-style',
    btnClassName: 'margin-bottom',
  },
];

export const paddingSet: PaddingSet[] = [
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_LEFT,
    className: 'padding-left-border field-style',
    btnClassName: 'padding-left',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_RIGHT,
    className: 'padding-right-border field-style',
    btnClassName: 'padding-right',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_TOP,
    className: 'padding-top-border field-style',
    btnClassName: 'padding-top',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_ALIGNMENT_PADDING_BOTTOM,
    className: 'padding-bottom-border field-style',
    btnClassName: 'padding-bottom',
  },
];

export const columnSet: ColumnSet[] = [
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_ONE,
    numberOfSet: ['one'],
    htmlClass: ['col-lg-12 col-md-12 col-sm-12 col-xs-12 '],
    className: 'set-one set-block',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_TWO,
    numberOfSet: ['one', 'two'],
    className: 'set-two set-block',
    htmlClass: ['col-lg-6 col-md-6 col-sm-12 col-xs-12', 'col-lg-6 col-md-6 col-sm-12 col-xs-12'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_THREE,
    numberOfSet: ['one', 'two', 'three'],
    className: 'set-three set-block',
    htmlClass: [
      'col-lg-4 col-md-4 col-sm-12 col-xs-12',
      'col-lg-4 col-md-4 col-sm-12 col-xs-12',
      'col-lg-4 col-md-4 col-sm-12 col-xs-12',
    ],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_FOUR,
    numberOfSet: ['one', 'two', 'three', 'four'],
    htmlClass: [
      'col-lg-3 col-md-6 col-sm-12 col-xs-12',
      'col-lg-3 col-md-6 col-sm-12 col-xs-12',
      'col-lg-3 col-md-6 col-sm-12 col-xs-12',
      'col-lg-3 col-md-6 col-sm-12 col-xs-12',
    ],
    className: 'set-four set-block',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_FIVE,
    numberOfSet: ['one', 'two', 'three', 'four', 'five'],
    htmlClass: [
      'col-12 col-md-6 col-xl-5col',
      'col-12 col-md-6 col-xl-5col',
      'col-12 col-md-6 col-xl-5col',
      'col-12 col-md-6 col-xl-5col',
      'col-12 col-md-6 col-xl-5col',
    ],
    className: 'set-five set-block',
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_COLUMN_SET_SIX,
    numberOfSet: ['one', 'two', 'three', 'four', 'five', 'six'],
    htmlClass: [
      'col-lg-2 col-md-4 col-sm-12 col-xs-12',
      'col-lg-2 col-md-4 col-sm-12 col-xs-12',
      'col-lg-2 col-md-4 col-sm-12 col-xs-12',
      'col-lg-2 col-md-4 col-sm-12 col-xs-12',
      'col-lg-2 col-md-4 col-sm-12 col-xs-12',
      'col-lg-2 col-md-4 col-sm-12 col-xs-12',
    ],
    className: 'set-six set-block',
  },
];
export const twoColumnSet: TwoColumnContainerSet[] = [
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_FIFTY_FIFTY,
    className: 'set-block fiftyFifty-block',
    htmlClass: ['col-lg-6 col-md-6 col-sm-12 col-xs-12', 'col-lg-6 col-md-6 col-sm-12 col-xs-12'],
    columnType: ['50', '50'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_SIXTY_FOURTY,
    className: 'set-block sixtyFourty-block',
    htmlClass: ['col-lg-7 col-md-7 col-sm-12 col-xs-12', 'col-lg-5 col-md-5 col-sm-12 col-xs-12'],
    columnType: ['60', '40'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_FOURTY_SIXTY,
    className: 'set-block fourtySixty-block',
    htmlClass: ['col-lg-5 col-md-5 col-sm-12 col-xs-12', 'col-lg-7 col-md-7 col-sm-12 col-xs-12'],
    columnType: ['40', '60'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_SEVENTY_THIRTY,
    className: 'set-block seventyThirty-block',
    htmlClass: ['col-lg-8 col-md-8 col-sm-12 col-xs-12', 'col-lg-4 col-md-4 col-sm-12 col-xs-12'],
    columnType: ['70', '30'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_THIRTY_SEVENTY,
    className: 'set-block thirtySeventy-block',
    htmlClass: ['col-lg-4 col-md-4 col-sm-12 col-xs-12', 'col-lg-8 col-md-8 col-sm-12 col-xs-12'],
    columnType: ['30', '70'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_EIGHTY_TWENTY,
    className: 'set-block eightyTwenty-block',
    htmlClass: ['col-lg-9 col-md-9 col-sm-12 col-xs-12', 'col-lg-3 col-md-3 col-sm-12 col-xs-12'],
    columnType: ['80', '20'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_TWENTY_EIGHTY,
    className: 'set-block twentyEighty-block',
    htmlClass: ['col-lg-3 col-md-3 col-sm-12 col-xs-12', 'col-lg-9 col-md-9 col-sm-12 col-xs-12'],
    columnType: ['20', '80'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_NINTY_TEN,
    className: 'set-block nintyTen-block',
    htmlClass: ['col-lg-10 col-md-10 col-sm-12 col-xs-12', 'col-lg-2 col-md-2 col-sm-12 col-xs-12'],
    columnType: ['90', '10'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_TWO_COLUMN_SET_TEN_NINTY,
    className: 'set-block tenNinty-block',
    htmlClass: ['col-lg-2 col-md-2 col-sm-12 col-xs-12', 'col-lg-10 col-md-10 col-sm-12 col-xs-12'],
    columnType: ['10', '90'],
  },
];

export const threeColumnSet: ThreeColumnContainerSet[] = [
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_THRITYTHREE_THRITYTHREE_THRITYTHREE,
    className: 'set-block thirtythreeThirtythreeThirtythree-block',
    htmlClass: [
      'col-lg-4 col-md-4 col-sm-12 col-xs-12',
      'col-lg-4 col-md-4 col-sm-12 col-xs-12',
      'col-lg-4 col-md-4 col-sm-12 col-xs-12',
    ],
    columnType: ['33', '33', '33'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_TWENTY_SIXTY_TWENTY,
    className: 'set-block twentySixtyTwenty-block',
    htmlClass: [
      'col-lg-2 col-md-2 col-sm-12 col-xs-12',
      'col-lg-8 col-md-8 col-sm-12 col-xs-12',
      'col-lg-2 col-md-2 col-sm-12 col-xs-12',
    ],
    columnType: ['20', '60', '20'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_FOURTY_FOURTY_TWENTY,
    className: 'set-block fourtyFourtyTwenty-block',
    htmlClass: [
      'col-lg-5 col-md-5 col-sm-12 col-xs-12',
      'col-lg-5 col-md-5 col-sm-12 col-xs-12',
      'col-lg-2 col-md-2 col-sm-12 col-xs-12',
    ],
    columnType: ['40', '40', '20'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_TWENTY_FOURTY_FOURTY,
    className: 'set-block twentyFourtyFourty-block',
    htmlClass: [
      'col-lg-2 col-md-2 col-sm-12 col-xs-12',
      'col-lg-5 col-md-5 col-sm-12 col-xs-12',
      'col-lg-5 col-md-5 col-sm-12 col-xs-12',
    ],
    columnType: ['20', '40', '40'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_THIRTY_THIRTY_FOURTY,
    className: 'set-block thirtyThirtyFourty-block',
    htmlClass: [
      'col-lg-3 col-md-3 col-sm-12 col-xs-12',
      'col-lg-3 col-md-3 col-sm-12 col-xs-12',
      'col-lg-6 col-md-6 col-sm-12 col-xs-12',
    ],
    columnType: ['30', '30', '40'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_FOURTY_THIRTY_THIRTY,
    className: 'set-block fourtyThirtyThirty-block',
    htmlClass: [
      'col-lg-6 col-md-6 col-sm-12 col-xs-12',
      'col-lg-3 col-md-3 col-sm-12 col-xs-12',
      'col-lg-3 col-md-3 col-sm-12 col-xs-12',
    ],
    columnType: ['40', '30', '30'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_TEN_THIRTY_SIXTY,
    className: 'set-block tenThirtySixty-block',
    htmlClass: [
      'col-lg-1 col-md-1 col-sm-12 col-xs-12',
      'col-lg-3 col-md-3 col-sm-12 col-xs-12',
      'col-lg-8 col-md-8 col-sm-12 col-xs-12',
    ],
    columnType: ['10', '30', '60'],
  },
  {
    id: CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_THREE_COLUMN_SET_SIXTY_THIRTY_TEN,
    className: 'set-block sixtyThirtyTen-block',
    htmlClass: [
      'col-lg-8 col-md-8 col-sm-12 col-xs-12',
      'col-lg-3 col-md-3 col-sm-12 col-xs-12',
      'col-lg-1 col-md-1 col-sm-12 col-xs-12',
    ],
    columnType: ['60', '30', '10'],
  },
];
