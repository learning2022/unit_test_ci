// import { translate } from '../../../i18n/translate';
import Base from '../../base';
import { createButton, createElement, createTooltipButton } from '../../common/dom';
import CONSTANTS from '../../constant';
import { blockOptionsIcon } from '../../iconHolder';
import { FeatureInterface } from '../../interface';
import { appendExternalLinkOptionWrapper, handleImageLinkOption } from '../externalLinkOption';
import { editInteractiveUsingBlockOption } from '../interactive';
import { translate } from '../../../i18n/translate';
import { updateUserLanguageStore } from '../../utils';

class BlockOptions extends Base {
  initiate = (featuresList: FeatureInterface): void => {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const blockOptionsWrapper = iframeDocument.getElementById(
        CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER,
      );
      if (blockOptionsWrapper) {
        this.appendSaveOption(blockOptionsWrapper);
        this.appendDuplicateOption(blockOptionsWrapper);
        this.appendDeleteOption(blockOptionsWrapper);
        if (featuresList.copyandpaste) {
          this.appendCopyOption(blockOptionsWrapper);
          this.appendPasteOption(blockOptionsWrapper);
        }
        if (featuresList.changerow) this.appendEditRowOption(blockOptionsWrapper);
        if (featuresList.changecolumn) this.appendEditColumnOption(blockOptionsWrapper);
        if (process.env.REACT_APP_ENABLE_LOCALISATION_MINI_MENU === 'true') {
          this.appendLanguagesOption(blockOptionsWrapper);
        }
        this.appendInteractiveEditOption(blockOptionsWrapper);
        this.appendLeftOption(blockOptionsWrapper);
        this.appendRightOption(blockOptionsWrapper);
        this.appendTopOption(blockOptionsWrapper);
        this.appendBottomOption(blockOptionsWrapper);
        this.appendExternalOption(blockOptionsWrapper);
      }
    }
  };

  appendSaveOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_SAVE_BUTTON;
    button.className = 'btn blockOptionButton  editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isSave();
    button.innerHTML = `<span>${blockOptionsIcon.saveRowIcon}</span><span>${blockOptionsIcon.saveColumnIcon}</span>`;
    wrapper.appendChild(button);
  };

  appendInteractiveEditOption = (wrapper: HTMLElement) => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_EDIT_BUTTON;
    button.className = 'btn blockOptionButton  editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('data-placement', 'bottom');
    button.setAttribute('aria-label', 'editInteractive');
    button.setAttribute('title', translate('blockoption.editinteractive'));
    button.onclick = async () => {
      const selectedElement = this.getSelectedElement() as HTMLElement;
      await editInteractiveUsingBlockOption(selectedElement);
    };
    button.innerHTML = `<span>${blockOptionsIcon.interactiveEditIcon}</span>`;
    wrapper.appendChild(button);
  };

  appendDuplicateOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_DUPLICATE_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('global.duplicate'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = async (): Promise<void> => await this.isDuplicate();
    button.innerHTML = `${blockOptionsIcon.duplicateElementIcon}`;
    wrapper.appendChild(button);
  };

  appendDeleteOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('global.delete'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isDelete();
    button.innerHTML = `${blockOptionsIcon.deleteElementIcon}`;
    wrapper.appendChild(button);
  };

  appendCopyOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_COPY_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.copy'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isCopied();
    button.innerHTML = `${blockOptionsIcon.copyElementIcon}`;
    wrapper.appendChild(button);
  };

  appendPasteOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_PASTE_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.paste'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = async (): Promise<void> => await this.isPaste();
    button.innerHTML = `${blockOptionsIcon.pasteElementIcon}`;
    wrapper.appendChild(button);
  };

  appendEditRowOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_ROW_EDIT_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('global.editrow'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isEdit();
    button.innerHTML = `${blockOptionsIcon.rowElementIcon}`;
    wrapper.appendChild(button);
  };

  appendEditColumnOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_COLUMN_EDIT_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.editcolumn'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isEdit();
    button.innerHTML = `${blockOptionsIcon.columnElementIcon}`;
    wrapper.appendChild(button);
  };

  appendLeftOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_LEFT_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.moveleft'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.moveElementLeft();
    button.innerHTML = `${blockOptionsIcon.leftarrowElementIcon}`;
    wrapper.appendChild(button);
  };

  appendRightOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_RIGHT_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.moveright'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.moveElementRight();
    button.innerHTML = `${blockOptionsIcon.rightarrowElementIcon}`;
    wrapper.appendChild(button);
  };

  appendTopOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_TOP_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.movetop'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isMoveTop();
    button.innerHTML = `${blockOptionsIcon.toparrowElementIcon}`;
    wrapper.appendChild(button);
  };

  appendBottomOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_BOTTOM_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('blockoption.movebottom'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => this.isMoveBottom();
    button.innerHTML = `${blockOptionsIcon.downarrowElementIcon}`;
    wrapper.appendChild(button);
  };

  appendExternalOption = (wrapper: HTMLElement): void => {
    const button = document.createElement('button');
    button.id = CONSTANTS.LOREE_BLOCK_OPTIONS_EXTERNAL_LINK_BUTTON;
    button.className = 'btn blockOptionButton editor-tooltip';
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', translate('global.insertlink'));
    button.setAttribute('data-placement', 'bottom');
    button.onclick = (): void => handleImageLinkOption(button);
    button.innerHTML = `${blockOptionsIcon.linkTextIcon}`;
    wrapper.appendChild(button);
    appendExternalLinkOptionWrapper();
  };

  appendLanguagesOption = (wrapper: HTMLElement): void => {
    const button = createTooltipButton(
      translate('blockoption.languages'),
      'bottom',
      CONSTANTS.LOREE_BLOCK_OPTIONS_LANGUAGE_BUTTON,
      'btn blockOptionButton editor-tooltip',
    );
    button.innerHTML = `${blockOptionsIcon.languageIcon} <svg viewBox="0 0 8 14"><path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path></svg>`;
    wrapper.appendChild(button);
    const tooltip = this.appendLanguageOptionsLists();
    button.onclick = () => {
      const arrowIcon = button.lastChild as SVGElement;
      if (tooltip?.style.display === 'none') {
        this.openLanguageOption(button, tooltip);
        arrowIcon.style.transform = 'rotate(90deg)';
      } else {
        this.closeLanguageOption();
        arrowIcon.style.removeProperty('transform');
      }
    };
  };

  appendLanguageOptionsLists = (): HTMLElement | null => {
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return null;
    const tooltip = createElement(
      'div',
      CONSTANTS.LOREE_BLOCK_OPTIONS_TOOLTIP_LANGUAGE_OPTIONS_WRAPPER,
      'loree-block-option-language-wrapper',
    );
    tooltip.style.display = 'none';
    for (const langauge of CONSTANTS.LOREE_MINI_MENU_LANGUAGE_LISTS) {
      const tooltipOption = createButton(langauge.isoCode);
      tooltipOption.className = 'd-flex';
      tooltipOption.innerHTML = `<span style="flex:1;margin-right: -5px">(${langauge.isoCode})</span> <span style="flex:2">${langauge.language}</span>`;
      tooltipOption.onclick = () => {
        const selectedElement = this.getSelectedElement() as HTMLElement;
        if (!selectedElement?.lang) {
          selectedElement?.setAttribute('lang', langauge.isoCode);
        } else {
          selectedElement.lang = langauge.isoCode;
        }
        updateUserLanguageStore('selectedMiniMenuLanguage', langauge.isoCode);
        this.hideLanguageIsoLabel();
        this.showLanguageIsoLabel(selectedElement);
        this.closeLanguageOption();
      };
      tooltip.appendChild(tooltipOption);
    }
    iframeDocument.body.appendChild(tooltip);
    return tooltip;
  };
}
export default BlockOptions;
