import EditOptions from './editOptions';
import { columnSet, twoColumnSet, threeColumnSet } from './editOptionsColumnSet';
import { editOptionsMockData } from './editOptionsMockData';
import CONSTANTS from '../../constant';
import Base from '../../base';
import BlockOptions from './blockOptions';
import {
  getEditorElementById,
  getIframedocumentElementById,
  getIframedocumentElementsByClassName,
} from '../../utils';

describe('Three column process', () => {
  describe('Adding 3 column', () => {
    test('Showing necessary element', () => {
      const editoptionObject = new EditOptions();
      const element1 = document.createElement('div');
      element1.className = 'd-none';
      const result = editoptionObject.addaddView(element1);
      expect(result).toHaveAttribute('class', '');
    });
    test('Removing hide elements', () => {
      const editoptionObject = new EditOptions();
      const element1 = document.createElement('div');
      const result = editoptionObject.addremoveView(element1);
      expect(result).toHaveClass('d-none');
    });
  });
  describe('Removing 3 column', () => {
    test('Showing necessary element', () => {
      const editoptionObject = new EditOptions();
      const element1 = document.createElement('div');
      const result = editoptionObject.removeaddView(element1);
      expect(result).toHaveClass('d-none');
    });
    test('Removing hide elements', () => {
      const editoptionObject = new EditOptions();
      const element1 = document.createElement('div');
      element1.className = 'd-none';
      const result = editoptionObject.removeremoveView(element1);
      expect(result).toHaveAttribute('class', '');
    });
  });
  describe('Editing row with all the bootrap classes', () => {
    const editoptionObject = new EditOptions();
    let colSet;
    let selectedElement: HTMLElement | null;
    const element1 = document.createElement('div');
    beforeEach(() => {
      document.body.innerHTML = '';
      element1.id = 'selectedElement';
      element1.className = '';
      document.body.appendChild(element1);
      selectedElement = getEditorElementById('selectedElement');
    });
    test('One column editing', async () => {
      getEditorElementById('selectedElement')!.innerHTML = `<div class=''></div>`;
      colSet = columnSet; // For one column
      editoptionObject.ChangeColClassName(
        colSet[0].htmlClass,
        selectedElement,
        selectedElement!.childElementCount,
      );
      expect(document.body.innerHTML).toEqual(editOptionsMockData.oneColumnOoutput);
    });
    test('Two column editing', () => {
      getEditorElementById(
        'selectedElement',
      )!.innerHTML = `<div class=''></div><div class= ''></div>`;
      colSet = twoColumnSet; // For two column
      const selectedElement: HTMLElement | null = getEditorElementById('selectedElement');
      editoptionObject.ChangeColClassName(
        colSet[0].htmlClass,
        selectedElement,
        selectedElement!.childElementCount,
      );
      expect(document.body.innerHTML).toEqual(editOptionsMockData.twoColumnOoutput);
    });
    test('Three column editing', () => {
      getEditorElementById(
        'selectedElement',
      )!.innerHTML = `<div class=''></div><div class=''></div><div class=''></div>`;
      colSet = threeColumnSet; // For three column
      const selectedElement: HTMLElement | null = getEditorElementById('selectedElement');
      editoptionObject.ChangeColClassName(
        colSet[0].htmlClass,
        selectedElement,
        selectedElement!.childElementCount,
      );
      expect(document.body.innerHTML).toEqual(editOptionsMockData.threeColumnOoutput);
    });
    test('Four column editing', () => {
      getEditorElementById(
        'selectedElement',
      )!.innerHTML = `<div class=''></div><div class=''></div><div class=''></div><div class=''></div>`;
      colSet = columnSet; // For four column
      const selectedElement: HTMLElement | null = getEditorElementById('selectedElement');
      editoptionObject.ChangeColClassName(
        colSet[3].htmlClass,
        selectedElement,
        selectedElement!.childElementCount,
      );
      expect(document.body.innerHTML).toEqual(editOptionsMockData.fourColumnOoutput);
    });
    test('six column editing', () => {
      getEditorElementById(
        'selectedElement',
      )!.innerHTML = `<div class=''></div><div class=''></div><div class=''></div><div class=''></div><div class=''></div><div class=''></div>`;
      colSet = columnSet; // For six column
      const selectedElement: HTMLElement | null = getEditorElementById('selectedElement');
      editoptionObject.ChangeColClassName(
        colSet[5].htmlClass,
        selectedElement,
        selectedElement!.childElementCount,
      );
      expect(document.body.innerHTML).toEqual(editOptionsMockData.sixColumnOoutput);
    });
    afterAll(() => {
      document.body.innerHTML = '';
    });
  });
});

describe('#editOptions', () => {
  const iframe = document.createElement('iframe');
  let blockOptions: BlockOptions;
  let editOptions: EditOptions;
  let baseInstance: Base;
  let blockOptionsElement: HTMLElement;
  let editOptionsElement: HTMLElement;
  let firstRow: HTMLDivElement;
  let secondRow: HTMLDivElement;
  let firstCol: HTMLDivElement;
  let secondCol: HTMLDivElement;
  function loreeWrapper() {
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    iframe.appendChild(wrapper);
    iframe.innerHTML += editOptionsMockData.iframeInnerHtml;
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }

  beforeEach(() => {
    loreeWrapper();
    baseInstance = new Base();
    jest.spyOn(baseInstance, 'updateFeaturesList');
    baseInstance.updateFeaturesList({
      features: { saveascustomrow: true, deleterow: true, changerow: true },
    });
    blockOptions = new BlockOptions();
    blockOptions.initiate({ changerow: true });
    editOptions = new EditOptions();
    editOptions.initiate({});
    blockOptionsElement = getIframedocumentElementById(
      iframe.contentDocument,
      CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER,
    ) as HTMLElement;
    editOptionsElement = getIframedocumentElementById(
      iframe.contentDocument,
      CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER,
    ) as HTMLElement;
  });

  test('click on row shows block options', () => {
    const rowElement = getIframedocumentElementsByClassName(
      iframe.contentDocument,
      CONSTANTS.LOREE_IFRAME_CONTENT_ROW,
    )[0] as HTMLElement;
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => rowElement);
    baseInstance.changeSelectedElement(rowElement);
    expect(blockOptionsElement?.style.display).toEqual('flex');
  });

  test('click on edit row opens popup', () => {
    (blockOptionsElement?.childNodes[3] as HTMLButtonElement).click();
    expect(editOptionsElement?.style.display).toEqual('block');
  });

  test('click on 5 column row changes 1 column to 5 column', async () => {
    const fiveColRow = editOptionsElement.querySelector('#five-col-set > .one') as HTMLElement;
    fiveColRow.click();
    expect(baseInstance.getSelectedElement()?.outerHTML).toEqual(
      editOptionsMockData.selectedElementOutput,
    );
  });
  test('click on 6 column row changes 1 column to 6 column', async () => {
    const sixColRow = editOptionsElement.querySelector('#six-col-set > .one') as HTMLElement;
    sixColRow.click();
    expect(baseInstance.getSelectedElement()?.outerHTML).toEqual(
      editOptionsMockData.ConvertedElement,
    );
  });
  test('click on border should open color picker', () => {
    baseInstance.handleRowColumnBorderColorPickerCancel = jest.fn();
    editOptions.setBorderRowColumnColorPicker = jest.fn();
    secondRow = getIframedocumentElementsByClassName(
      iframe.contentDocument,
      CONSTANTS.LOREE_IFRAME_CONTENT_ROW,
    )[1] as HTMLDivElement;
    const borderButton = editOptionsElement.querySelector(
      `#${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BORDER_COLOR_PICKER_BUTTON}`,
    ) as HTMLElement;
    borderButton?.click();
    expect(
      getIframedocumentElementById(
        iframe.contentDocument,
        CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
      )?.style.display,
    ).toEqual('inline-flex');

    // close color picker when click on other content
    baseInstance.changeSelectedElement(secondRow);
    expect(
      getIframedocumentElementById(
        iframe.contentDocument,
        CONSTANTS.LOREE_ROW_COLUMN_BORDER_COLOR_PICKER_WRAPPER,
      )?.style.display,
    ).toEqual('none');
  });
  test('border row is updated with default color on initial', () => {
    editOptions.getSelectedBlockOptionMenu = jest.fn().mockImplementation(() => true);
    editOptions.setBorderRowColumnColorPicker();
    expect(editOptions.userRowBorderColor()).toBe('#ffffff');
  });
  test('border row color picker should be updated with selected element styles', () => {
    editOptions.getSelectedBlockOptionMenu = jest.fn().mockImplementation(() => true);
    firstRow = getIframedocumentElementsByClassName(
      iframe.contentDocument,
      CONSTANTS.LOREE_IFRAME_CONTENT_ROW,
    )[0] as HTMLDivElement;
    editOptions.changeSelectedElement(firstRow);
    editOptions.applyStyleToSelectedElement('border-color', 'rgb(255, 0, 0)');
    editOptions.applyStyleToSelectedElement('border-width', '16px');
    editOptions.applyStyleToSelectedElement('border-style', 'double');
    editOptions.setBorderRowColumnColorPicker();
    editOptions.setBorderWidthAndStyle();
    expect(editOptions.userRowBorderColor()).toBe('rgb(255, 0, 0)');
    expect(editOptions.getUserRowBorderStyle()).toBe('double');
    expect(editOptions.userRowBorderWidth()).toBe('16');
  });
  test('new row selection should bring default values not previous values', () => {
    editOptions.changeSelectedElement(secondRow);
    editOptions.getSelectedBlockOptionMenu = jest.fn().mockImplementation(() => true);
    editOptions.setBorderRowColumnColorPicker();
    editOptions.setBorderWidthAndStyle();
    expect(editOptions.userRowBorderColor()).toBe('#ffffff');
    expect(editOptions.getUserRowBorderStyle()).toBe('solid');
    expect(editOptions.userRowBorderWidth()).toBe('0');
  });
  test('border column is updated with default color on initial', () => {
    editOptions.getSelectedBlockOptionMenu = jest.fn().mockImplementation(() => false);
    editOptions.setBorderRowColumnColorPicker();
    expect(editOptions.userColumnBorderColor()).toBe('#ffffff');
  });
  test('border column color picker should be updated with selected element styles', () => {
    editOptions.getSelectedBlockOptionMenu = jest.fn().mockImplementation(() => false);
    firstCol = getIframedocumentElementsByClassName(
      iframe.contentDocument,
      CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN,
    )[0] as HTMLDivElement;
    editOptions.changeSelectedElement(firstCol);
    editOptions.applyStyleToSelectedElement('border-color', 'rgb(165, 165, 165)');
    editOptions.applyStyleToSelectedElement('border-width', '11px');
    editOptions.applyStyleToSelectedElement('border-style', 'dashed');
    editOptions.setBorderRowColumnColorPicker();
    editOptions.setBorderWidthAndStyle();
    expect(editOptions.userColumnBorderColor()).toBe('rgb(165, 165, 165)');
    expect(editOptions.userColumnBorderWidth()).toBe('11');
  });
  test('new column selection should bring default values not previous values', () => {
    secondCol = getIframedocumentElementsByClassName(
      iframe.contentDocument,
      CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN,
    )[1] as HTMLDivElement;
    editOptions.changeSelectedElement(secondCol);
    editOptions.getSelectedBlockOptionMenu = jest.fn().mockImplementation(() => false);
    editOptions.setBorderRowColumnColorPicker();
    editOptions.setBorderWidthAndStyle();
    expect(editOptions.userColumnBorderColor()).toBe('#ffffff');
    expect(editOptions.userColumnBorderWidth()).toBe('0');
  });
  afterEach(() => {
    iframe.innerHTML = '';
  });
});
