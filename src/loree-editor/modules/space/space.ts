import CONSTANTS from '../../constant';
import { isBB, isCanvas, lmsUserManual } from '../../../lmsConfig';
import { getIFrame } from '../../common/dom';

import s from './space.module.scss';

const editorHelpIcon =
  '<svg aria-hidden="true" width="12" height="15" focusable="false" data-prefix="fas" data-icon="question-circle" class="svg-inline--fa fa-question-circle fa-w-16 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg>';

class Space {
  initiate = (): void => {
    this.attachSpace();
  };

  getHelpLink = (optionType: string): string => {
    switch (optionType) {
      case 'user-guide':
        return lmsUserManual();
      case 'loree-support':
        return CONSTANTS.LOREE_CANVAS_SUPPORT_PORTAL;
      case 'whats-new':
        return CONSTANTS.LOREE_CANVAS_WHATS_NEW;
      case 'bb-whats-new':
        return CONSTANTS.LOREE_BB_WHATS_NEW;
      default:
        return '';
    }
  };

  helpLink = (optionType: string): void => {
    window.open(this.getHelpLink(optionType));
  };

  attachSpace = (): void => {
    // these should be passed in or something should orchestrate -
    // parent and child is tightly coupled now
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const spaceWrapper = document.createElement('div');
      spaceWrapper.id = CONSTANTS.LOREE_SPACE;
      spaceWrapper.className = s.loreeSpace;
      spaceWrapper.style.left = '0px';
      spaceWrapper.style.width = '100%';

      const a11yCheckerContainer = document.createElement('div');
      a11yCheckerContainer.id = CONSTANTS.LOREE_SPACE_A11Y_CHECKER;
      a11yCheckerContainer.className = s.a11yChecker;
      spaceWrapper.appendChild(a11yCheckerContainer);
      editorWrapper.appendChild(spaceWrapper);
    }
  };

  attachHelpIcon = () => {
    const iframeDocument = getIFrame();
    const helpIconWrapper = iframeDocument?.getElementById(CONSTANTS.LOREE_SPACE_HELP_ICON);
    if (!helpIconWrapper) return;
    const helpDropdown = document.createElement('div');
    const helpDropdownButton = document.createElement('button');
    helpDropdownButton.className =
      'no-caret dropdown-toggle btn ' + CONSTANTS.LOREE_SPACE_HELP_ICON_BUTTON;
    helpDropdownButton.id = CONSTANTS.LOREE_SPACE_HELP_ICON_BUTTON;
    helpDropdownButton.setAttribute(
      'style',
      `padding:0;
      font-size: 0rem;
      position: fixed;
      right: 3px;
      top: 1px;`,
    );
    helpDropdownButton.title = 'Help';
    helpDropdownButton.type = 'button';
    helpDropdownButton.setAttribute('data-toggle', 'dropdown');
    helpDropdownButton.innerHTML = editorHelpIcon;
    const dropDownItem = document.createElement('div');
    dropDownItem.className = 'dropdown-menu loree-space-help-icon-dropdown';
    dropDownItem.setAttribute('style', `display:none; `);
    const dropDownUserGuideButton = document.createElement('div');
    dropDownUserGuideButton.className = 'dropdown-item';
    dropDownUserGuideButton.innerHTML = 'User guide';
    dropDownUserGuideButton.onclick = (): void => this.helpLink('user-guide');

    const dropDownSupportButton = document.createElement('div');
    dropDownSupportButton.className = 'dropdown-item';
    dropDownSupportButton.innerHTML = 'Loree support';
    dropDownSupportButton.onclick = (): void => this.helpLink('loree-support');

    const dropDownWhatsNewButton = document.createElement('div');
    dropDownWhatsNewButton.className = 'dropdown-item';
    dropDownWhatsNewButton.innerHTML = `What's new`;

    dropDownItem.appendChild(dropDownUserGuideButton);
    dropDownItem.appendChild(dropDownSupportButton);
    if (isCanvas()) {
      dropDownWhatsNewButton.onclick = (): void => this.helpLink('whats-new');
    } else if (isBB()) {
      dropDownWhatsNewButton.onclick = (): void => this.helpLink('bb-whats-new');
    }
    dropDownItem.appendChild(dropDownWhatsNewButton);
    helpDropdown.appendChild(helpDropdownButton);
    helpDropdown.appendChild(dropDownItem);

    helpIconWrapper.appendChild(helpDropdown);
  };
}
export default Space;
