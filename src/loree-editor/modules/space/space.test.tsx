import Space from './space';
import { createDiv, getIFrame } from '../../common/dom';

import CONSTANTS from '../../constant';

describe('OnBoardHelpsection', () => {
  const spaceInstance = new Space();
  const iframe = document.createElement('iframe');
  const iframeDocument = createDiv('div');

  function loreeWrapper() {
    iframeDocument.id = 'loree-wrapper';
    document.body.append(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createDiv('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    iframe.appendChild(wrapper);
    const helpSection = createDiv('div');
    helpSection.id = CONSTANTS.LOREE_SPACE_HELP_ICON;
    iframe.appendChild(helpSection);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  beforeEach(() => {
    document.body.innerHTML = '';
    loreeWrapper();
  });
  test('on load help icon', () => {
    spaceInstance.attachHelpIcon();
    const iframeDocument = getIFrame();
    const helpIconWrapper = iframeDocument?.getElementsByClassName(
      CONSTANTS.LOREE_SPACE_HELP_ICON_BUTTON,
    )[0] as HTMLElement;
    expect(helpIconWrapper.style.right).toBe('3px');
    expect(helpIconWrapper.style.top).toBe('1px');
  });
  test('on load help icon inner options', () => {
    spaceInstance.attachHelpIcon();
    const iframeDocument = getIFrame();
    const helpIconWrapper = iframeDocument?.getElementsByClassName(
      'loree-space-help-icon-dropdown',
    )[0] as HTMLElement;
    expect(helpIconWrapper.style.display).toBe('none');
  });
});
describe('help section display', () => {
  let spaceInstance: Space;
  beforeEach(() => {
    window.open = jest.fn();
    spaceInstance = new Space();
  });
  test.each([
    { helperOption: 'user-guide' },
    { helperOption: 'loree-support' },
    { helperOption: 'whats-new' },
    { helperOption: 'bb-whats-new' },
  ])('black board whats new option', ({ helperOption }) => {
    spaceInstance.helpLink(helperOption);
    expect(window.open).toHaveBeenCalled();
  });
});
