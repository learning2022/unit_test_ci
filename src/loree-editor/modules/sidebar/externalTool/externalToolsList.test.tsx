import React from 'react';
import CONSTANTS from '../../../constant';
import Sidebar from '../sidebar';
import { API } from 'aws-amplify';
import { render, fireEvent, waitFor, RenderResult } from '@testing-library/react';
import { ExternalToolsList } from './externalToolsList';

const listExternalTools = {
  data: {
    listLtiTools: {
      items: [
        {
          id: 'dda27045-8f55-4a2c-9911-ab2163df2da3',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          toolName: 'ExternalToolsTesting - H5P2',
          issuerUrl: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
          clientId: '1000000000001',
          clientSecret: '1000000000001==',
          oidcUrl: 'https://limon.h5p.com/lti/login',
          redirectURI: 'https://limon.h5p.com/lti/launch',
          targetLinkURI: 'https://limon.h5p.com/lti/launch',
          jwksUrl: 'https://limon.h5p.com/lti/jwks/1291484793007326309.json',
          createdAt: '2022-02-15T02:29:45.934Z',
          updatedAt: '2022-02-18T03:23:28.394Z',
        },
        {
          id: 'a5bf499a-226a-45c6-b7c9-857fb7215f2a',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          toolName: 'ExternalToolsTesting - H5P3',
          issuerUrl: 'result',
          clientId: 'result',
          clientSecret: 'result',
          oidcUrl: 'result',
          redirectURI: 'result',
          targetLinkURI: 'result',
          jwksUrl: 'result',
          createdAt: '2022-02-15T02:27:19.764Z',
          updatedAt: '2022-02-17T02:59:45.595Z',
        },
      ],
      nextToken: null,
    },
  },
};
const listExternalToolsEmpty = {
  data: {
    listLtiTools: {
      items: [],
      nextToken: null,
    },
  },
};

describe('when external tools API failed', () => {
  beforeAll(() => {
    API.graphql = jest.fn().mockImplementation(() => {});
  });
  test('should call console error', async () => {
    const { queryByText } = render(<ExternalToolsList />);
    await waitFor(() => queryByText(/Something went wrong!/i));
    expect(queryByText(/Something went wrong!/i)).toBeDefined();
  });
});

describe('#External tools', () => {
  let externalTool: HTMLElement;
  let wrapper: RenderResult;
  let subSidebar: HTMLElement;
  beforeEach(() => {
    API.graphql = jest.fn().mockImplementation(() => listExternalTools);
    sessionStorage.setItem('domainName', 'canvas');
    const sideBar = new Sidebar();
    wrapper = render(<div id={CONSTANTS.LOREE_WRAPPER} />);
    sideBar.initiate({ features: {} });
    externalTool = wrapper.container.querySelector(
      `#${CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT}`,
    ) as HTMLElement;
    subSidebar = wrapper.container.querySelector(`#${CONSTANTS.LOREE_SUB_SIDEBAR}`) as HTMLElement;
  });
  test('external tool element is listed in sidebar', () => {
    expect(externalTool).toBeDefined();
  });
  test.skip('click on external tool element should list tools in sidebar', async () => {
    fireEvent.click(externalTool);
    expect(subSidebar).toBeVisible();
    await waitFor(() => wrapper.getAllByText('ExternalToolsTesting', { exact: false }));
    expect(wrapper.getAllByText('ExternalToolsTesting', { exact: false }).length).not.toEqual(0);
  });
  describe.skip('when there is no external tools', () => {
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(() => listExternalToolsEmpty);
    });
    test('should show no results found in sub sidebar', async () => {
      fireEvent.click(externalTool);
      expect(subSidebar).toBeVisible();
      await waitFor(() => wrapper.getByText(/No results/i));
      expect(wrapper.getByText(/No results/i)).toBeDefined();
    });
  });
});
describe('when LMS is BB/d2l', () => {
  let wrapper: RenderResult;
  beforeEach(() => {
    sessionStorage.setItem('domainName', 'bb');
    const sideBar = new Sidebar();
    // eslint-disable-next-line react/react-in-jsx-scope
    wrapper = render(<div id={CONSTANTS.LOREE_WRAPPER} />);
    sideBar.initiate({ features: {} });
  });
  test('should not display external tools element', () => {
    const externalTool = wrapper.container.querySelector(
      `#${CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT}`,
    ) as HTMLElement;
    expect(externalTool).toBeNull();
  });
});
