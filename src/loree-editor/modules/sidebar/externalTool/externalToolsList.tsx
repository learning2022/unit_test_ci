import React, { useState, useEffect } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { ListLtiToolsQuery } from '../../../../API';
import { listLtiTools } from '../../../../graphql/queries';
import { captureErrorLog } from '../../../utils';
import { Loader } from '../../../../components/loaderV2/loader';
import { ReactComponent as H5PIcon } from '../../../../assets/Icons/elements/h5p.svg';
import { translate } from '../../../../i18n/translate';
import ExternalTool, { LoginHintProps } from './externalTool';

export const ExternalToolsList = (props: {}) => {
  const [ltiTools, setLtiTools] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [externalTool, setExternalTool] = useState(false);
  const [externalToolModalConfig, setExternalToolModalConfig] = useState<_Any>();

  const showExternalToolModal = (externalTool: LoginHintProps) => {
    setExternalTool(true);
    setExternalToolModalConfig(externalTool);
  };

  const hideExternalToolModal = () => {
    setExternalTool(false);
    setExternalToolModalConfig('');
  };

  useEffect(() => {
    void (async function () {
      try {
        const list: _Any = await API.graphql<ListLtiToolsQuery>(graphqlOperation(listLtiTools, {}));
        setLtiTools(list?.data?.listLtiTools?.items);
        setIsLoading(false);
      } catch (error) {
        captureErrorLog('fetching external tools', error);
        setIsLoading(false);
        setIsError(true);
      }
    })();
  }, []);

  return (
    <>
      <div className='subSidebarElementWrapper'>
        {isError ? (
          <p className='loree-error-message'>{translate('externaltools.error')}</p>
        ) : isLoading ? (
          <Loader />
        ) : ltiTools.length > 0 ? (
          ltiTools.map((tool: LoginHintProps, index: number) => (
            <div className='subSidebarElements' key={index}>
              <div
                className='external-tool-hover'
                onClick={async () => showExternalToolModal(tool)}
                key={`external-tool-hover-${index}`}
                aria-hidden='true'
              >
                <div className='px-5 block-height' key={`external-tool-block-${index}`}>
                  <H5PIcon key={`external-tool-icon-${index}`} />
                </div>
                <p className='text-center external-tool-name' key={`external-tool-name-${index}`}>
                  {tool.toolName}
                </p>
              </div>
            </div>
          ))
        ) : (
          <p className='loree-no-results-message text-center mx-4'>
            {translate('global.noresultsfound')}
          </p>
        )}
      </div>
      {externalTool && (
        <ExternalTool externalToolConfig={externalToolModalConfig} onHide={hideExternalToolModal} />
      )}
    </>
  );
};
