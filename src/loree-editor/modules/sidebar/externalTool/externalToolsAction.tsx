import { API, Auth, graphqlOperation } from 'aws-amplify';
import { CreateLtiToolLoginHintMutation } from '../../../../API';
import { createLtiToolLoginHint } from '../../../../graphql/mutations';
import { LtiToolLoginHint } from '../../../interface';
import { ExternalToolIframeProps } from './externalToolIframe';

export const createLtiLoginHint = async (externalTool: ExternalToolIframeProps) => {
  const currentAuthenticatedUser = await Auth.currentAuthenticatedUser();
  const email = currentAuthenticatedUser.attributes.email;
  const ltiToolLoginHintPayload: LtiToolLoginHint = {
    issuerUrl: externalTool.issuerUrl,
    clientId: externalTool.clientId,
    loreeUserEmail: email,
    messageType: externalTool.messageType,
    resourceLink: externalTool.targetLinkURI,
  };
  const ltiToolLoginHint = await API.graphql<CreateLtiToolLoginHintMutation>(
    graphqlOperation(createLtiToolLoginHint, { input: ltiToolLoginHintPayload }),
  );
  return ltiToolLoginHint;
};
