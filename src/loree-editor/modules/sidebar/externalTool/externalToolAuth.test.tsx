import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import ExternalToolAuth from './externalToolAuth';

const externalToolAuthProps = {
  pathname: '/lti/tool/auth',
  location: {
    search: `?idToken=12345&state=some_state&redirectUri=https://andibox.com`,
  },
};

describe('#ExternalToolAuth', () => {
  let externalToolAuth: RenderResult;
  beforeEach(() => {
    // @ts-expect-error:
    externalToolAuth = render(<ExternalToolAuth {...externalToolAuthProps} />);
  });

  test('form and loader rendered', () => {
    expect(externalToolAuth.getAllByTitle('Loading, please wait')).toBeDefined();
    expect(externalToolAuth.getByRole('form')).toBeDefined();
  });
});
