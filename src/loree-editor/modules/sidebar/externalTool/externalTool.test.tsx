import React from 'react';
import { render, RenderResult, screen } from '@testing-library/react';
import ExternalTool from './externalTool';
import axe from '../../../../axeHelper';

const externalToolLoginProps = {
  externalToolConfig: {
    toolName: 'H5P',
    clientId: '123000000000123',
    issuerUrl: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
    oidcUrl: 'https://limon.h5p.com/lti/login',
    targetLinkURI: 'https://limon.h5p.com/lti/launch',
  },
  onHide: jest.fn(),
};

describe('#ExternalToolContent', () => {
  let externalToolModal: RenderResult;
  beforeEach(() => {
    externalToolModal = render(<ExternalTool {...externalToolLoginProps} />);
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(externalToolModal.container);
    expect(results).toHaveNoViolations();
  });

  test('renders external tool modal contents', () => {
    expect(screen.getByText('H5P')).toBeInTheDocument();
    expect(screen.getByText('Close')).toBeInTheDocument();
  });
});
