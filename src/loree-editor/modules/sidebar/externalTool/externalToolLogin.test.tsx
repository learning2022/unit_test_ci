import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import ExternalToolLogin from './externalToolLogin';
import { API, Auth } from 'aws-amplify';

const externalToolLoginProps = {
  pathname: '/lti/tool/auth',
  location: {
    search: `?launchParams={"clientId":"123000000000123","issuerUrl":"https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool","oidcUrl":"https://limon.h5p.com/lti/login","targetLinkURI":"https://limon.h5p.com/content/1291590289572747719"}`,
  },
};

const createLtiLoginHintMock = {
  data: {
    createLtiToolLoginHint: {
      id: '3cf8925e-57cf-43fd-a88d-1617c531a1d9',
      issuerUrl: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
      clientId: '123000000000123',
      loreeUserEmail: 'canvas_123670000000000245@example.com',
      messageType: 'LtiResourceLinkRequest',
      resourceLink: 'https://limon.h5p.com/content/1291590289572747719',
      createdAt: '2022-03-30T03:55:08.108Z',
      updatedAt: '2022-03-30T03:55:08.108Z',
    },
  },
};

describe('#ExternalToolLogin', () => {
  let externalToolLogin: RenderResult;
  beforeEach(() => {
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'krishnaveni',
        sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    API.graphql = jest.fn().mockImplementation(() => createLtiLoginHintMock);
    // @ts-expect-error:
    externalToolLogin = render(<ExternalToolLogin {...externalToolLoginProps} />);
  });

  test('form is rendered', async () => {
    expect(externalToolLogin.getByTitle('loree-external-tool')).toBeDefined();
  });
});
