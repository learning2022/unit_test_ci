import React, { useEffect, useRef, useState } from 'react';
import { createLtiLoginHint } from './externalToolsAction';
import { captureErrorLog } from '../../../utils';
import CONSTANTS from '../../../constant';
export interface ExternalToolIframeProps {
  issuerUrl: string;
  clientId: string;
  oidcUrl: string;
  targetLinkURI: string;
  messageType: string;
}
const ExternalToolIframe = (props: ExternalToolIframeProps) => {
  const [loginHintId, setLoginHintId] = useState('');
  const externalToolForm = useRef<HTMLFormElement>(null);
  useEffect(() => {
    void (async function () {
      try {
        const ltiToolLoginHint = await createLtiLoginHint(props);
        if (!ltiToolLoginHint.data?.createLtiToolLoginHint) return;
        setLoginHintId(ltiToolLoginHint?.data?.createLtiToolLoginHint.id);
        externalToolForm.current?.submit();
      } catch (error) {
        captureErrorLog('creating lti login hint', error);
      }
    })();
  }, [props]);

  return loginHintId.length > 0 ? (
    <>
      {props.messageType === 'LtiResourceLinkRequest' && (
        <form
          id='form-external-tool'
          action={props.oidcUrl}
          ref={externalToolForm}
          method='post'
          target='_self'
          title='loree-external-tool'
          name='form1test'
        >
          <input type='hidden' name='iss' value={props.issuerUrl} />
          <input type='hidden' name='login_hint' value={loginHintId} />
          <input type='hidden' name='client_id' value={props.clientId} />
          <input type='hidden' name='target_link_uri' value={props.targetLinkURI} />
        </form>
      )}
      {props.messageType === 'LtiDeepLinkingRequest' && (
        <iframe
          id={CONSTANTS.LOREE_IFRAME_LTI_CREATE_MODAL}
          className='loree-external-tool-iframe'
          title='loree-external-tool'
          src={`data:text/html;charset=utf-8, ${encodeURI(`<body onload="setTimeout(function() { document.frm1.submit() }, 100)">
              <form name="frm1" action=${props.oidcUrl} method="post">
              <input type='hidden' name='iss' value=${props.issuerUrl}>
              <input type='hidden' name='login_hint' value=${loginHintId}>
              <input type='hidden' name='client_id' value=${props.clientId}>
              <input type='hidden' name='target_link_uri' value=${props.targetLinkURI}>
              </form>
            </body>`)}`}
        />
      )}
    </>
  ) : (
    <></>
  );
};
export default ExternalToolIframe;
