import React, { useEffect, useRef } from 'react';
import { Form } from 'react-bootstrap';
import History from 'history';
import Loading from '../../../../components/loader/loading';

interface ExternalToolAuthProps {
  location: History.Location;
}

const ExternalToolAuth = (props: ExternalToolAuthProps) => {
  let userQuery;
  if (props.location.search) {
    userQuery = new URLSearchParams(props.location.search);
  }
  const redirectUri = userQuery?.get('redirectUri') as string;
  const externalToolForm = useRef<HTMLFormElement>(null);

  useEffect(() => {
    externalToolForm.current?.submit();
  }, [externalToolForm]);

  return (
    <>
      <Loading />
      <Form
        method='POST'
        ref={externalToolForm}
        action={redirectUri}
        id='external-tool-auth-form'
        role='form'
      >
        <Form.Control type='hidden' name='id_token' value={userQuery?.get('idToken') as string} />
        <Form.Control type='hidden' name='state' value={userQuery?.get('state') as string} />
      </Form>
    </>
  );
};

export default ExternalToolAuth;
