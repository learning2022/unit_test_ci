import React, { useEffect, useState } from 'react';
import CustomModal from '../../../../components/customModal/customModal';
import { translate } from '../../../../i18n/translate';
import CONSTANTS from '../../../constant';
import { isValidHost } from '../../../utils';
import { baseClass } from '../../customBlocks/customBlockHandler';
import ExternalToolIframe from './externalToolIframe';

interface ExternalToolProps {
  externalToolConfig: LoginHintProps;
  onHide(): void;
}

export interface LoginHintProps {
  toolName: string;
  issuerUrl: string;
  clientId: string;
  oidcUrl: string;
  targetLinkURI: string;
}

function ExternalTool(props: ExternalToolProps) {
  const [externalToolTitle, setExternalToolTitle] = useState('');
  const [externalToolLoadingStatus, setExternalToolLoadingStatus] = useState(true);

  useEffect(() => {
    const appendExternalToolElementToEditor = (event: MessageEvent) => {
      if (event.data.key === 'setExternalToolSrc') {
        props.onHide();
        baseClass().hideSidebar();
        baseClass().appendElementToSelectedColumn(externalToolContent(event.data.value));
      }
    };

    const externalToolContent = (externalToolSrc: string) => {
      return `<div style="padding:10px; margin-bottom: 20px;" class ='${CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_WRAPPER}'>
                <iframe 
                  class=${CONSTANTS.LOREE_IFRAME_CONTENT_EXTERNAL_TOOL_ELEMENT}
                  frameBorder="0"
                  width ="100%"
                  height=400
                  src="${externalToolSrc}">
                </iframe>
              </div>`;
    };

    // event listener for external tool modal
    const externalToolIframeListener = function (event: MessageEvent) {
      if (!isValidHost(event.origin)) return;
      appendExternalToolElementToEditor(event);
      window.removeEventListener('message', externalToolIframeListener, false);
    };
    window.addEventListener('message', externalToolIframeListener);
    setExternalToolTitle(props.externalToolConfig.toolName);
    setExternalToolLoadingStatus(false);

    return () => {
      window.removeEventListener('message', externalToolIframeListener, false);
    };
  }, [props]);

  return (
    <CustomModal
      show
      hide={props.onHide}
      loadingStatus={externalToolLoadingStatus}
      modalTitle={externalToolTitle}
      addOrContinueText={translate('global.add')}
      showAddOrContinueButton={false}
      showCancelButton={false}
      modalBody={
        <ExternalToolIframe
          issuerUrl={props.externalToolConfig.issuerUrl}
          clientId={props.externalToolConfig.clientId}
          oidcUrl={props.externalToolConfig.oidcUrl}
          targetLinkURI={props.externalToolConfig.targetLinkURI}
          messageType='LtiDeepLinkingRequest'
        />
      }
    />
  );
}

export default ExternalTool;
