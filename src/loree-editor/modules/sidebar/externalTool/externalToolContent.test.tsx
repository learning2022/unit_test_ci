import React from 'react';
import { RenderResult, render } from '@testing-library/react';
import ExternalToolContent from './externalToolContent';
import History from 'history';
import axe from '../../../../axeHelper';

const params = {
  search: `?launchParams={"contentUrl":"https://limon.h5p.com/content/1291590289572747719"}`,
} as History.Location;

const externalToolLoginProps = {
  location: params,
};

describe('#ExternalToolContent', () => {
  let externalToolLogin: RenderResult;
  beforeEach(() => {
    externalToolLogin = render(<ExternalToolContent {...externalToolLoginProps} />);
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(externalToolLogin.container);
    expect(results).toHaveNoViolations();
  });

  test('renders loader', () => {
    expect(externalToolLogin.getByText('Loading', { exact: false })).toBeDefined();
  });
});
