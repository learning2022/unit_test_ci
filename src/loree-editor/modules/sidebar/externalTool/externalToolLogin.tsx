import React, { useState, useEffect } from 'react';
import History from 'history';
import ExternalToolIframe, { ExternalToolIframeProps } from './externalToolIframe';

interface ExternalToolLoginProps {
  location: History.Location;
}
const ExternalToolLogin = (props: ExternalToolLoginProps) => {
  const [launchParams, setLaunchParams] = useState<ExternalToolIframeProps>();

  useEffect(() => {
    if (props.location.search) {
      const userQuery = new URLSearchParams(props.location.search);
      setLaunchParams(JSON.parse(userQuery?.get('launchParams') as string));
    }
  }, [props.location.search]);

  return launchParams ? (
    <ExternalToolIframe
      issuerUrl={launchParams.issuerUrl}
      clientId={launchParams.clientId}
      oidcUrl={launchParams.oidcUrl}
      targetLinkURI={launchParams.targetLinkURI}
      messageType='LtiResourceLinkRequest'
    />
  ) : (
    <></>
  );
};

export default ExternalToolLogin;
