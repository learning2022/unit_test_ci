import React, { useEffect } from 'react';
import History from 'history';
import Loading from '../../../../components/loader/loading';

interface ExternalToolContentProps {
  location: History.Location;
}

const ExternalToolContent = (props: ExternalToolContentProps) => {
  useEffect(() => {
    if (props.location.search) {
      const userQuery = new URLSearchParams(props.location.search);
      const contentUrl = userQuery?.get('contentUrl') as string;

      const data = {
        key: 'setExternalToolSrc',
        value: contentUrl,
      };

      window.parent.postMessage(data, window.location.origin);
    }
  }, [props.location.search]);

  return <Loading />;
};

export default ExternalToolContent;
