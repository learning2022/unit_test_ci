import React from 'react';
import ReactDOM from 'react-dom';
import CONSTANTS from '../../../constant';
import { ReactComponent as ExternalToolIcon } from '../../../../assets/Icons/elements/external_tools.svg';
import { removeSelectedElementSection, addClassToElement } from '../../../utils';
import { getElementById } from '../../../common/dom';
import { ExternalToolsList } from './externalToolsList';
import { translate } from '../../../../i18n/translate';

const ExternalTools = (props: {}) => {
  const handleExternalToolsClick = () => {
    const subSidebar = getElementById(CONSTANTS.LOREE_SUB_SIDEBAR);
    const externalToolElement = getElementById(CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT);
    if (!subSidebar || !externalToolElement) {
      return;
    }
    removeSelectedElementSection();
    addClassToElement(externalToolElement, 'active');
    subSidebar.style.display = 'block';
    ReactDOM.render(<ExternalToolsList />, subSidebar);
  };

  return (
    <>
      <div
        role='presentation'
        id={CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT}
        className='icon-block'
        onClick={() => {
          handleExternalToolsClick();
        }}
        onKeyPress={() => {
          handleExternalToolsClick();
        }}
      >
        <ExternalToolIcon key='external-tool' />
      </div>
      <div className='label'>{translate('externaltools.externaltools')}</div>
    </>
  );
};

export const attachExternalTool = (wrapper: HTMLElement) => {
  ReactDOM.render(<ExternalTools />, wrapper);
};

export const removeExternalTool = (wrapper: HTMLElement) => {
  ReactDOM.unmountComponentAtNode(wrapper);
};
