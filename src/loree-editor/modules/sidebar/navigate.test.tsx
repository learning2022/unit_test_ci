import Base from '../../base';
import CONSTANTS from '../../constant';
import Navigate from './navigate';
import Bootstrap from '../iframe/templates/bootstrap';
import BlockOptions from '../../modules/blockOptions/blockOptions';
import { customBlockMockData } from '../customBlocks/customBlockMockData';
import { alignmentMenuMock, navigationMenuMock } from './mockData';
import { getEditorElementsByClassName } from '../../utils';

describe('Menu sidebar customization', () => {
  const navInstance = new Navigate();
  const baseInstance = new Base();
  const dummyInput1 = document.createElement('div');
  const inputField = document.createElement('input');
  inputField.type = '10';
  inputField.className = 'inputField';
  beforeEach(() => {
    dummyInput1.id = 'dummyId1';
    dummyInput1.className = 'dummyClass row';
    dummyInput1.setAttribute('style', 'background-color: #fff');
    dummyInput1.innerHTML = alignmentMenuMock;
    dummyInput1.innerHTML += navigationMenuMock;
    document.body.appendChild(dummyInput1);
    navInstance.getSelectedElement = jest.fn().mockImplementation(() => dummyInput1);
    baseInstance.getSelectedElement = jest.fn().mockImplementation(() => dummyInput1);
    const selectedElements = new Array(dummyInput1);
    navInstance.getSelectedElements = jest.fn().mockImplementation(() => selectedElements);
  });
  test('Space property', () => {
    baseInstance.applyStyleToSelectedElement('margin-top', '10px');
    const style = window.getComputedStyle(document.getElementsByClassName('row')[0]);
    expect(style.marginTop).toBe('10px');
  });
  // Alignment Tests
  test('alignment for menus 1', () => {
    navInstance.applyAlignmentToMenu('start');
    const style = window.getComputedStyle(
      document.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_MENU)[0],
    );
    const leftAlignmentButton = getEditorElementsByClassName(
      'navAlignmentButton',
    )[0] as HTMLButtonElement;
    expect(style.marginRight).toBe('auto');
    expect(style.marginLeft).toBe('0px');
    expect(style.justifyContent).toBe('flex-start');
    expect(leftAlignmentButton.style.stroke).toBe('blue');
  });
  test('alignment for menus 2', () => {
    navInstance.applyAlignmentToMenu('center');
    const style = window.getComputedStyle(
      document.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_MENU)[0] as HTMLButtonElement,
    );
    const centerAlignmentButton = getEditorElementsByClassName(
      'navAlignmentButton',
    )[1] as HTMLElement;
    expect(style.marginRight).toBe('auto');
    expect(style.marginLeft).toBe('auto');
    expect(style.justifyContent).toBe('center');
    expect(centerAlignmentButton.style.stroke).toBe('blue');
  });
  test('alignment for menus 3', () => {
    navInstance.applyAlignmentToMenu('end');
    const style = window.getComputedStyle(
      document.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_MENU)[0] as HTMLButtonElement,
    );
    const rightAlignmentButton = getEditorElementsByClassName(
      'navAlignmentButton',
    )[2] as HTMLElement;

    expect(style.marginLeft).toBe('auto');
    expect(style.marginRight).toBe('0px');
    expect(style.justifyContent).toBe('flex-end');
    expect(rightAlignmentButton.style.stroke).toBe('blue');
  });
  test('color for menu section', () => {
    const element = document.getElementsByClassName('row')[0] as HTMLElement;
    const returnValue = navInstance.getBackgroundColorOfContainer(element);
    const style = window.getComputedStyle(document.getElementsByClassName('row')[0]);
    expect(style.backgroundColor).toBe(returnValue);
  });
  test('border for navigationbar', () => {
    const widthInput = document.createElement('input');
    widthInput.value = '30';
    widthInput.className = 'border-width';
    document.body.append(widthInput);
    navInstance.handleNavigateBorderWidthChange();
    expect(document.getElementById('dummyId1')?.style.borderWidth).toBe('30px');
  });
  test('border style', () => {
    const borderStyleInput = document.createElement('div');
    borderStyleInput.setAttribute('style', 'borderStyle: none');
    borderStyleInput.className = 'bordertypes';
    document.body.append(borderStyleInput);
    navInstance.handleNavigateBorderStyleChange();
    expect(document.getElementById('dummyId1')?.style.borderStyle).toBe('');
  });
  test('border color', () => {
    const colorInput = document.createElement('input');
    const element = document.getElementById('dummyId1') as HTMLElement;
    colorInput.value = '#ffffff';
    colorInput.className = 'color';
    document.body.append(colorInput);
    navInstance.handleNavigateBorderColorChange('#ffffff', element);
    expect(document.getElementById('dummyId1')?.style.borderColor).toBe('#ffffff');
  });
  describe('Navigation Mini menu', () => {
    const baseInstance = new Base();
    const dummyInput1 = document.createElement('div');
    const iframe = document.createElement('iframe');
    const iframeDocument = document.createElement('div');

    function loreeWrapper() {
      iframeDocument.id = 'loree-wrapper';
      document.body.append(iframeDocument);
      iframe.id = CONSTANTS.LOREE_IFRAME;
      const wrapper = document.createElement('div');
      wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
      iframe.appendChild(wrapper);
      iframe.innerHTML += customBlockMockData.iframeInnerHtml;
      document.body.appendChild(iframe);
      const iframeDoc = iframe.contentWindow?.document;
      iframeDoc?.open().write(iframe.innerHTML);
      iframeDoc?.close();
    }

    beforeAll(() => {
      document.body.innerHTML = Bootstrap;
      loreeWrapper();
      const blockOptions = new BlockOptions();
      jest.spyOn(baseInstance, 'updateFeaturesList');
      baseInstance.updateFeaturesList({
        features: { saveascustomrow: true, deleteelement: true, moveelement: true },
      });
      blockOptions.initiate({});
      dummyInput1.id = 'dummyId1';
      dummyInput1.className = 'dummyClass row';
      dummyInput1.setAttribute('style', 'background-color: #fff');
      dummyInput1.innerHTML = `<nav class="navbar navbar-expand-sm navbar-light" style="padding: 0px; width: 100%;"><div class=${CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION}><img class="loree-iframe-content-image  ${CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO}" src='https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg' alt="navigation_logo" style="height: auto; width: 80px;" /></div><ul class="navbar-nav ${CONSTANTS.LOREE_IFRAME_CONTENT_MENU}" style="margin: 0px 0px 0px 0px; list-style: none; padding: 0px; "><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 12px 20px; margin: 0px;">Menu Item 1</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 12px 20px; margin: 0px;">Menu Item 2</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 12px 20px; margin: 0px;"> Menu Item</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 12px 20px; margin: 0px;"> Menu Item</p></li><li class="${CONSTANTS.LOREE_IFRAME_CONTENT_MENU} nav-item" style="padding: 0px;"><p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}" style="padding: 12px 20px; margin: 0px;"> Menu Item</p></li></ul> </nav>`;
      document.body.appendChild(dummyInput1);
      baseInstance.cleanUp = jest.fn().mockImplementation(() => '');
      baseInstance.showElementSectionOptions = jest.fn().mockImplementation(() => '');
    });

    test('Menu item to Move Left', () => {
      const selectedElement = document.getElementsByTagName('ul')[0].children[1] as HTMLElement;
      baseInstance.moveContentLeft(selectedElement);
      expect(document.getElementsByTagName('li')[0].children[0].innerHTML).toBe('Menu Item 2');
    });
    test('Menu item to Move right', () => {
      const selectedElement = document.getElementsByTagName('ul')[0].children[0] as HTMLElement;
      baseInstance.moveContentRight(selectedElement);
      expect(document.getElementsByTagName('li')[0].children[0].innerHTML).toBe('Menu Item 1');
    });
    test('For Check Block Options for navigation menu', () => {
      const selectedElement = document
        .getElementsByTagName('ul')[0]
        .getElementsByTagName('p')[0] as HTMLElement;
      baseInstance.changeSelectedElement(selectedElement);
      baseInstance.showBlockOptions(false, false, true);
      const deleteBtn = baseInstance
        .getDocument()
        ?.getElementById(`${CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON}`) as HTMLElement;
      const moveRightBtn = baseInstance
        .getDocument()
        ?.getElementById(`${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_RIGHT_BUTTON}`) as HTMLElement;
      const moveLeftBtn = baseInstance
        .getDocument()
        ?.getElementById(`${CONSTANTS.LOREE_BLOCK_OPTIONS_MOVE_LEFT_BUTTON}`) as HTMLElement;
      expect(deleteBtn.style.display).toBe('inline-flex');
      expect(moveRightBtn.style.display).toBe('inline-flex');
      expect(moveLeftBtn.style.display).toBe('none');
    });
    test('Navigation image mini menus', () => {
      const selectedElement = document.getElementsByClassName(
        CONSTANTS.LOREE_NAVIGATION_IMAGE_SECTION,
      )[0] as HTMLElement;
      baseInstance.changeSelectedElement(selectedElement);
      baseInstance.showBlockOptions(false, false, true);
      const externalBtn = baseInstance
        .getDocument()
        ?.getElementById(`${CONSTANTS.LOREE_BLOCK_OPTIONS_EXTERNAL_LINK_BUTTON}`) as HTMLElement;
      expect(externalBtn.style.display).toBe('inline-flex');
    });
  });
});
