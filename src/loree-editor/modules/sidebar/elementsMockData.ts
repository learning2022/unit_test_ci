import CONSTANTS from '../../constant';

const specialElementContent = [
  {
    name: 'navigationBlocl',
    id: 'loree-navigate-option',
    template: `<div id=${CONSTANTS.LOREE_IFRAME_NAVIGATE_ID}></div>`,
    content: '<div>test</div>',
    innerContent: [
      {
        id: 'loree-navigate-option',
        name: 'navigation 1',
        template: '<div></div>',
        content: '',
      },
    ],
  },
];

export const specialElementData = {
  name: 'Special Elements',
  id: 'specialblock',
  template: '',
  multiple: true,
  content: '',
  contents: specialElementContent,
};

export const subSidebarMock = `<div id=${CONSTANTS.LOREE_SUB_SIDEBAR}></div>`;
