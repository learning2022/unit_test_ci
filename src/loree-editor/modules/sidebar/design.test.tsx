import ReactDOM from 'react-dom';
import React from 'react';
import { createDiv, getElementById } from '../../common/dom';
import CONSTANTS from '../../constant';
import Design from './design';
import {
  designSectionBorderMock,
  expectedResultborder,
  expectedResultwidth,
  mockParaHtml,
  selectedElement,
  mockVideoHtml,
} from './designMockdata';
import ReactHtmlParser from 'react-html-parser';
import {
  getEditorElementsByClassName,
  getElementByClassName,
  getElementByTagName,
  getElementsByTagName,
} from '../../utils';
import { specialElement } from '../../elements/specialElement';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('test case for imageBordercolorpicker', () => {
  let DesignInstance: Design;
  beforeEach(() => {
    DesignInstance = new Design();
  });
  test('calling the function getImageVideoElementBorderColor()  in the textborderclick while opening the color picker', () => {
    const spycheckupdates = jest.spyOn(DesignInstance, 'getImageVideoElementBorderColor');
    DesignInstance.borderWidth();
    expect(spycheckupdates).toBeCalledTimes(2);
  });
});

describe('test case for applyBorderStyleToSelectedElememt', () => {
  const value = 'solid';
  const property = 'border-style';
  const selection = new DOMParser().parseFromString(selectedElement, 'text/xml');
  const newBorderColor = '#abcdef';
  const newBorderWidth = '30px';
  let appliedStyle: _Any;
  function iFrameWrapper() {
    const iframe = document.createElement('iframe');
    iframe.className = 'loree-iframe-content-element';
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    wrapper.setAttribute('style', "borderColor:'' ");
    wrapper.append(selection.firstChild as HTMLElement);
    wrapper.setAttribute('style', "borderColor:'' ");
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(wrapper.outerHTML);
    iframeDoc?.close();
    return iframe;
  }

  let DesignInstance: Design;
  beforeEach(() => {
    DesignInstance = new Design();
    DesignInstance.getSelectedElement = jest.fn().mockImplementation(() => iFrameWrapper());

    const style = document.createElement('div');
    style.setAttribute('style', "border-color:'' ");
    style.setAttribute('style', "border-width:'' ");
    const styleVal = style;
    appliedStyle = styleVal.style;
  });
  test('calling the function applyStyleToSelectedImageVideoElement while apply border width', () => {
    DesignInstance.applyStyleToSelectedImageVideoElement(property, value);
    appliedStyle.borderWidth = newBorderWidth;
    expect(appliedStyle.borderWidth).toBe('30px');
  });
  test('calling the function applyStyleToSelectedImageVideoElement while apply border color', () => {
    DesignInstance.applyStyleToSelectedImageVideoElement(property, value);
    appliedStyle.borderColor = newBorderColor;
    expect(appliedStyle.borderColor).toBe('#abcdef');
  });
  test('calling the function appliedStylePropertyBorderColor while apply style', () => {
    const spyappliedStylePropertyBorderColor = jest.spyOn(
      DesignInstance,
      'appliedStylePropertyBorderColor',
    );
    DesignInstance.applyBorderStyleToSelectedElememt(property, value);
    expect(spyappliedStylePropertyBorderColor).toBeCalledTimes(1);
  });
  test('calling the function appliedStylePropertyBorderWidth while apply style', () => {
    const spyappliedStylePropertyBorderWidth = jest.spyOn(
      DesignInstance,
      'appliedStylePropertyBorderWidth',
    );
    DesignInstance.applyBorderStyleToSelectedElememt(property, value);
    expect(spyappliedStylePropertyBorderWidth).toBeCalledTimes(1);
  });

  test('calling the function appliedStylePropertyBordercolor while apply style', () => {
    DesignInstance.applyBorderStyleToSelectedElememt(property, value);
    const result = DesignInstance.appliedStylePropertyBorderColor(property, appliedStyle);
    const actualresult = JSON.stringify(result);
    expect(actualresult).toBe(expectedResultborder);
  });

  test('calling the function appliedStylePropertyWidth while apply style', () => {
    DesignInstance.applyBorderStyleToSelectedElememt(property, value);
    const result = DesignInstance.appliedStylePropertyBorderWidth(property, appliedStyle);
    const actualresult = JSON.stringify(result);
    expect(actualresult).toBe(expectedResultwidth);
  });
  test('Calling Border Style without applying color', () => {
    DesignInstance.appliedStylePropertyBorderColor(property, appliedStyle);
    expect(appliedStyle?.borderColor).toBe('black');
  });
  test('Calling Border Style with applying color', () => {
    appliedStyle.borderColor = newBorderColor;
    DesignInstance.appliedStylePropertyBorderColor(property, appliedStyle);
    expect(appliedStyle.borderColor).toBe('#abcdef');
  });
  test('Calling Border Style without applying width', () => {
    DesignInstance.appliedStylePropertyBorderWidth(property, appliedStyle);
    expect(appliedStyle.borderWidth).toBe('0px');
  });
  test('Calling Border Style with applying width', () => {
    appliedStyle.borderWidth = newBorderWidth;
    DesignInstance.appliedStylePropertyBorderWidth(property, appliedStyle);
    expect(appliedStyle.borderWidth).toBe('30px');
  });
});

describe('design section image sidebar', () => {
  let designInstance: Design;
  beforeEach(() => {
    designInstance = new Design();
    const divElement = createDiv('div');
    const selectedImageElement = document.createElement('img');
    selectedImageElement.alt = '';
    divElement.innerHTML = '<img alt=" " id="imageElement" >';
    document.body.append(divElement);
    designInstance.getSelectedElement = jest.fn().mockImplementation(() => divElement);

    const sideBar = designInstance.imageDetailsSection();

    document.body.append(sideBar);
  });

  test('alttext', () => {
    expect(getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_VIDEO_THUMBNAIL_HEADER)?.innerHTML).toBe(
      'design.alttext',
    );
  });
  test('alt text on change event', () => {
    const altInputBox = getElementById(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_SELECTED_IMAGE_NAME,
    ) as HTMLInputElement;
    altInputBox.value = 'alt';
    altInputBox.dispatchEvent(new Event('blur'));
    expect(getElementById('imageElement')?.getAttribute('alt')).toBe('alt');
  });
});
describe('test case for checking element width checkbox', () => {
  let DesignInstance: Design;
  const domDiv = createDiv('');

  beforeEach(() => {
    document.body.innerHTML = '';
    DesignInstance = new Design();
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
    const featuresList = {
      imagedesign: true,
    };
    ReactDOM.render(
      <div className='loree-image-section'>
        <img
          className='loree-iframe-content-icon-text'
          src='https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg'
          alt='navigation_logo'
        />
      </div>,
      domDiv,
    );
    DesignInstance.initiate({ features: { image: true } });
    const element = domDiv?.getElementsByClassName('loree-image-section')[0] as HTMLElement;
    DesignInstance.attachDesignContent('image', element, featuresList);
  });

  test('calling function fullWidthOnChange() to check if width input textbox is disabled when checked', () => {
    const widthCheckBox = document.getElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH);
    widthCheckBox?.click();
    DesignInstance.fullWidthOnChange();
    const widthInput = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
    ) as HTMLElement;
    expect(widthInput).toBeDisabled();
  });

  test('calling function fullWidthOnChange() to check if width input textbox is enabled when not checked', () => {
    DesignInstance.fullWidthOnChange();
    const widthInput = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
    ) as HTMLElement;
    expect(widthInput).toBeEnabled();
  });
});

describe('test case for setting full width to an element', () => {
  let DesignInstance: Design;
  const domDiv = createDiv('');

  beforeEach(() => {
    document.body.innerHTML = '';
    DesignInstance = new Design();
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
  });

  test('calling the function setElemetWidth() to check if image element is set to full width', () => {
    const featuresList = {
      imagedesign: true,
    };
    ReactDOM.render(
      <div className='loree-image-section'>
        <img
          className='loree-iframe-content-image'
          src='https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg'
          alt='navigation_logo'
        />
      </div>,
      domDiv,
    );

    DesignInstance.initiate({ features: { image: true } });
    const element = domDiv?.getElementsByClassName('loree-image-section')[0] as HTMLElement;
    DesignInstance.attachDesignContent('image', element, featuresList);
    const widthCheckBox = document.getElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH);
    widthCheckBox?.click();
    DesignInstance.fullWidthOnChange();
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    const elementValue = selectedElement?.getElementsByTagName('img')[0] as HTMLElement;
    expect(elementValue.style.width).toBe('100%');
  });
  test('calling the function setElemetWidth() to check if video element is set to full width', () => {
    const featuresList = {
      videodesign: true,
    };
    const mockVideo = ReactHtmlParser(mockVideoHtml);
    ReactDOM.render(mockVideo, domDiv);

    DesignInstance.initiate({ features: { videodesign: true } });
    const element = domDiv?.getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER,
    )[0] as HTMLElement;
    DesignInstance.attachDesignContent('video', element, featuresList);
    const widthCheckBox = getElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH);
    widthCheckBox?.click();
    DesignInstance.fullWidthOnChange();
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    const elementValue = selectedElement?.getElementsByTagName('iframe')[0] as HTMLElement;
    expect(elementValue.style.width).toBe('100%');
  });
  test('calling the function setElemetWidth() to check if video element is set to updated width', () => {
    const featuresList = {
      videodesign: true,
    };
    const mockVideo = ReactHtmlParser(mockVideoHtml);
    ReactDOM.render(mockVideo, domDiv);

    DesignInstance.initiate({ features: { videodesign: true } });
    const element = domDiv?.getElementsByClassName(
      CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER,
    )[0] as HTMLElement;
    DesignInstance.attachDesignContent('video', element, featuresList);
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    const elementValue = selectedElement?.getElementsByTagName('iframe')[0] as HTMLElement;
    const event: any = { target: { value: 600 } };
    DesignInstance.widthOnChange(event);
    expect(elementValue.parentElement?.parentElement?.style.width).toBe('600px');
  });

  test('calling the function setElemetWidth() to check if divider line element is set to full width', () => {
    const featuresList = {
      dividerdesign: true,
    };
    ReactDOM.render(
      <div className='loree-divider-section'>
        <hr className='loree-iframe-content-divider' />
      </div>,
      domDiv,
    );

    DesignInstance.initiate({ features: { dividerdesign: true } });
    const element = domDiv?.getElementsByClassName('loree-divider-section')[0] as HTMLElement;
    DesignInstance.attachDesignContent('line', element, featuresList);
    const widthCheckBox = document.getElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH);
    widthCheckBox?.click();
    DesignInstance.fullWidthOnChange();
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    const elementValue = selectedElement?.getElementsByTagName('hr')[0] as HTMLElement;
    expect(elementValue.style.width).toBe('100%');
  });

  test('calling the function setElemetWidth() to check if divider space element is set to full width', () => {
    const featuresList = {
      dividerdesign: true,
    };
    ReactDOM.render(
      <div className='loree-divider-section'>
        <div className='loree-iframe-content-divider' />
      </div>,
      domDiv,
    );

    DesignInstance.initiate({ features: { dividerdesign: true } });
    const element = domDiv?.getElementsByClassName('loree-divider-section')[0] as HTMLElement;
    DesignInstance.attachDesignContent('space', element, featuresList);
    const widthCheckBox = document.getElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH);
    widthCheckBox?.click();
    DesignInstance.fullWidthOnChange();
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    const elementValue = selectedElement?.getElementsByClassName(
      'loree-iframe-content-divider',
    )[0] as HTMLElement;
    expect(elementValue.style.width).toBe('100%');
  });

  test('calling the function setElemetWidth() to check if divider video element is set to full width', () => {
    const featuresList = {
      videodesign: true,
    };
    ReactDOM.render(
      <div className='loree-video-section'>
        <div className='loree-iframe-content-video-wrapper'>
          <div className='loree-iframe-video-content'>
            <video>
              <track
                src='https://www.youtube.com/embed/EngW7tLk6R8?&amp;controls=0&amp;showinfo=0'
                kind='captions'
              />
            </video>
          </div>
        </div>
      </div>,
      domDiv,
    );

    DesignInstance.initiate({ features: { videodesign: true } });
    const element = domDiv?.getElementsByClassName('loree-video-section')[0] as HTMLElement;
    DesignInstance.attachDesignContent('video', element, featuresList);
    const widthCheckBox = document.getElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH);
    widthCheckBox?.click();
    DesignInstance.fullWidthOnChange();
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    const elementValue = selectedElement?.getElementsByTagName('video')[0] as HTMLElement;
    expect(elementValue.style.width).toBe('100%');
  });
});

describe('test case for width functionality for special element', () => {
  let DesignInstance: Design;
  const selectedSpecialElement = createDiv();

  beforeEach(() => {
    document.body.innerHTML = '';
    DesignInstance = new Design();
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
    const featuresList = {
      imagedesign: true,
    };
    selectedSpecialElement.innerHTML = specialElement.contents[0].innerContent[0].content;
    const element = getElementByClassName(
      selectedSpecialElement,
      CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER,
    );
    DesignInstance.initiate({ features: { image: true } });
    DesignInstance.attachDesignContent('specialElement', element, featuresList);
    DesignInstance.getSelectedElement = jest.fn().mockImplementation(() => selectedSpecialElement);
  });

  test('calling function specialElementWidthOnChange() to check if width is set right', () => {
    const event: any = { target: { value: 50 } };
    DesignInstance.widthOnChange(event);
    const selectedElement = selectedSpecialElement;
    const elementValue = getElementByTagName(selectedElement, 'img');
    expect(elementValue.style.width).toBe('50px');
  });

  test('calling function specialElementWidthOnChange() to check if method returns null when there selected element is null', () => {
    DesignInstance.getSelectedElement = jest.fn().mockImplementation(() => null);
    const event: any = { target: { value: 13 } };
    DesignInstance.widthOnChange(event);
    const selectedElement = DesignInstance.getSelectedElement();
    expect(selectedElement).toBeNull();
  });

  test('calling function specialElementWidthOnChange() to check if method returns null when width value is null', () => {
    const event: any = { target: null };
    DesignInstance.widthOnChange(event);
    expect(event.target).toBeNull();
  });
});

describe('test case for attaching design section for special element', () => {
  let DesignInstance: Design;
  const selectedSpecialElement = createDiv();

  beforeEach(() => {
    document.body.innerHTML = '';
    DesignInstance = new Design();
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
    const featuresList = {
      imagedesign: true,
    };
    selectedSpecialElement.innerHTML = specialElement.contents[0].innerContent[0].content;
    const element = getElementByClassName(
      selectedSpecialElement,
      CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER,
    );
    DesignInstance.initiate({ features: { image: true } });
    DesignInstance.attachDesignContent('specialElement', element, featuresList);
    DesignInstance.getSelectedElement = jest.fn().mockImplementation(() => selectedSpecialElement);
    window.sessionStorage.setItem('lmsUrl', 'https://test.com');
  });

  test('calling function attachSpecialElementDesign() to check design section for LMS BB', () => {
    const designEditOptions = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION);
    sessionStorage.setItem('domainName', 'BB');
    DesignInstance.attachSpecialElementDesign(designEditOptions);
    const quickLinkBBPopUp = getElementById(CONSTANTS.LOREE_BB_QUICK_LINK_POP_UP);
    expect(quickLinkBBPopUp).not.toBeNull();
  });

  test('calling function attachSpecialElementDesign() to check design section for LMS D2L', () => {
    const designEditOptions = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION);
    sessionStorage.setItem('domainName', 'D2l');
    DesignInstance.attachSpecialElementDesign(designEditOptions);
    const quickLinkD2lPopUp = getElementById(CONSTANTS.LOREE_D2L_QUICK_LINK_POP_UP);
    expect(quickLinkD2lPopUp).not.toBeNull();
  });

  test('calling function attachSpecialElementDesign() to check design section for Canvas', () => {
    const designEditOptions = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION);
    sessionStorage.setItem('domainName', 'canvas');
    DesignInstance.attachSpecialElementDesign(designEditOptions);
    const quickLinkCanvasPopUp = getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_QUICK_LINK_BLOCK_COLLAPSE,
    );
    expect(quickLinkCanvasPopUp).not.toBeNull();
  });
});

describe('test case for accordion UI functionality in design section', () => {
  let DesignInstance: Design;

  beforeEach(() => {
    document.body.innerHTML = '';
    DesignInstance = new Design();
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
    DesignInstance.initiate({ features: { image: true } });
  });

  test('render design section options UI', () => {
    const designOptions = getElementById('loree-sidebar-content-wrapper');
    designOptions.appendChild(DesignInstance.sizeBlock());
    const sizeDesignSection = getElementById('loree-design-section-size-block-wrapper');
    expect(sizeDesignSection.getElementsByClassName('design-accordion')).not.toBeNull();

    designOptions.appendChild(DesignInstance.backgroundBlock());
    const backgroundDesignSection = getElementById('loree-design-section-background-block-wrapper');
    expect(backgroundDesignSection.getElementsByClassName('design-accordion')).not.toBeNull();

    designOptions.appendChild(DesignInstance.borderBlock());
    const borderDesignSection = getElementById('loree-design-section-border-block-wrapper');
    expect(borderDesignSection.getElementsByClassName('design-accordion')).not.toBeNull();

    designOptions.appendChild(DesignInstance.videoControl());
    const videoControlDesignSection = getElementById(
      'loree-design-section-video-control-block-wrapper',
    );
    expect(videoControlDesignSection.getElementsByClassName('design-accordion')).not.toBeNull();

    DesignInstance.accordionIconClick = jest.fn();
    (sizeDesignSection.firstChild as HTMLElement).click();
    expect(DesignInstance.accordionIconClick).toBeCalledTimes(1);
  });
});

describe('#BorderDesignSection', () => {
  let borderColorPicker: HTMLElement;
  let DesignInstance: Design;
  beforeAll(() => {
    DesignInstance = new Design();
    document.body.innerHTML = designSectionBorderMock;
    document.body.innerHTML += mockParaHtml;
  });
  afterAll(() => {
    jest.clearAllMocks();
  });
  test('border clear picker renders', () => {
    const btn = getEditorElementsByClassName('imageBorderColorPicker')[0] as HTMLButtonElement;
    DesignInstance.attachImageBorderColorPicker(btn);
    btn.click();
    expect(
      getEditorElementsByClassName('pcr-app loree-design-color-pickers border-picker visible')[0],
    ).toBeInTheDocument();
  });
  test('clear button exist in border color picker', () => {
    borderColorPicker = getEditorElementsByClassName(
      'loree-design-color-pickers border-picker',
    )[0] as HTMLElement;
    expect(getElementByClassName(borderColorPicker, 'pcr-clear')).toBeInTheDocument();
  });
  test('clear function reset table to default values', () => {
    const paraTag = getElementsByTagName('p')[0];
    DesignInstance.getSelectedElement = jest.fn().mockImplementation(() => paraTag as HTMLElement);
    const selectedElement = DesignInstance.getSelectedElement() as HTMLElement;
    selectedElement.style.setProperty('border', '11px dotted #ff0000');
    expect(selectedElement.style.borderWidth).toBe('11px');
    expect(selectedElement.style.borderStyle).toBe('dotted');
    const clearBtn = getElementByClassName(borderColorPicker, 'pcr-clear');
    clearBtn.dispatchEvent(new Event('click'));
    DesignInstance.clearBorderStyles(selectedElement);
    expect(selectedElement.style.borderWidth).toBe('0px');
    expect(selectedElement.style.borderColor).toBe('#ffffff');
  });
});
