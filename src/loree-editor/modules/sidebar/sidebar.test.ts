import Sidebar from '../sidebar/sidebar';

describe('As a Loree user', () => {
  const sideBar = new Sidebar();
  beforeAll(() => {
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
  });
  describe('When loaded and entered into an editor', () => {
    // Test the entire sibar items initated and rendered
    test('the Side bar elements initiated', () => {
      const attachSidebar = jest.spyOn(sideBar, 'attachSidebar');
      const attachSubSidebar = jest.spyOn(sideBar, 'attachSubSidebar');
      const attachSidebarOpenButton = jest.spyOn(sideBar, 'attachSidebarOpenButton');
      const attachSidebarContentWrapper = jest.spyOn(sideBar, 'attachSidebarContentWrapper');
      const attachSidebarRowContent = jest.spyOn(sideBar, 'attachSidebarRowContent');
      const attachSidebarElementContent = jest.spyOn(sideBar, 'attachSidebarElementContent');
      const attachSidebarDesignContent = jest.spyOn(sideBar, 'attachSidebarDesignContent');
      const attachSidebarTableDesignContent = jest.spyOn(
        sideBar,
        'attachSidebarTableDesignContent',
      );
      const attachSidebarMenuContent = jest.spyOn(sideBar, 'attachSidebarMenuContent');
      const attachSidebarContainerDesignContent = jest.spyOn(
        sideBar,
        'attachSidebarContainerDesignContent',
      );
      sideBar.initiate({ features: { image: true } });
      expect(attachSidebar).toHaveBeenCalledTimes(1);
      expect(attachSubSidebar).toHaveBeenCalledTimes(1);
      expect(attachSidebarOpenButton).toHaveBeenCalledTimes(1);
      expect(attachSidebarContentWrapper).toHaveBeenCalledTimes(1);
      expect(attachSidebarRowContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarElementContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarDesignContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarTableDesignContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarMenuContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarContainerDesignContent).toHaveBeenCalledTimes(1);
    });
    test('the Sidebar rendered', () => {
      const loreeSidebar = document.getElementById('loree-sidebar');
      expect(loreeSidebar).not.toBeNull();
    });
    test('Check the SubSidebar rendered', () => {
      const loreeSubSidebar = document.getElementById('loree-sub-sidebar');
      expect(loreeSubSidebar).not.toBeNull();
    });
    test('Verify the SideBar open button present', () => {
      const sideBarButton = document.getElementById('loree-sidebar-open-button');
      expect(sideBarButton).not.toBeNull();
      expect(sideBarButton?.tagName).toEqual('BUTTON');
    });
    test('Check the Row sidebar section rendered', () => {
      const rowSidebarSection = document.getElementById('loree-sidebar-row-section');
      expect(rowSidebarSection).not.toBeNull();
    });
    test('Check the Element Section rendered', () => {
      const elementsSidebar = document.getElementById('loree-sidebar-content-wrapper');
      expect(elementsSidebar).not.toBeNull();
    });
    test('Check the Design section rendered', () => {
      const designSection = document.getElementById('loree-sidebar-design-section');
      expect(designSection).not.toBeNull();
    });
    test('Verify the table sidebar section rendered', () => {
      const tableSection = document.getElementById('loree-sidebar-table-design-section');
      expect(tableSection).not.toBeNull();
    });
    test('Verify the container is rendered into dom', () => {
      const containerSection = document.getElementById('loree-sidebar-container-design-section');
      expect(containerSection).not.toBeNull();
    });
  });
});
