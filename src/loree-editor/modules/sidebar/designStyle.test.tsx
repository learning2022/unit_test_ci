import { createDiv } from '../../common/dom';
import CONSTANTS from '../../constant';
import { specialElement } from '../../elements/specialElement';
import { getElementByClassName, getElementByTagName } from '../../utils';
import Design from './design';
import {
  getAlignmentValues,
  checkHasAlignment,
  getImageSizeValuesForSpecialElement,
  highlightSelectedContainerAlignment,
} from './designStyle';

describe('When check the alignment', () => {
  const element = document.createElement('div');
  test('Should align center', () => {
    const centerAlign = CONSTANTS.LOREE_DESIGN_SECTION_CENTER_ALIGN_BUTTON;
    element.id = centerAlign;
    getAlignmentValues(element);
    expect(element.id).toBe(centerAlign);
  });

  test('Should align right', () => {
    element.id = CONSTANTS.LOREE_DESIGN_SECTION_RIGHT_ALIGN_BUTTON;
    getAlignmentValues(element);
    expect(element.id).toBe(CONSTANTS.LOREE_DESIGN_SECTION_RIGHT_ALIGN_BUTTON);
  });

  test('Should align left', () => {
    const leftAlign = CONSTANTS.LOREE_DESIGN_SECTION_LEFT_ALIGN_BUTTON;
    element.id = leftAlign;
    getAlignmentValues(element);
    expect(element.id).toBe(leftAlign);
  });

  test('Should align style stroke', () => {
    checkHasAlignment(element);
    expect(element.style.stroke).toBe(`blue`);
  });
});

describe('Verifying the width of special element', () => {
  let DesignInstance: Design;
  const selectedSpecialElement = createDiv();

  beforeEach(() => {
    document.body.innerHTML = '';
    DesignInstance = new Design();
    document.body.innerHTML = `<div id='loree-sidebar-content-wrapper'></div>`;
    const featuresList = {
      imagedesign: true,
    };
    selectedSpecialElement.innerHTML = specialElement.contents[0].innerContent[0].content;
    const element = getElementByClassName(
      selectedSpecialElement,
      CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER,
    );
    DesignInstance.initiate({ features: { image: true } });
    DesignInstance.attachDesignContent('specialElement', element, featuresList);
    DesignInstance.getSelectedElement = jest.fn().mockImplementation(() => selectedSpecialElement);
  });
  test('should get the right width', () => {
    const imageElement = getElementByTagName(selectedSpecialElement, 'img') as HTMLImageElement;
    const event: any = { target: { value: 50 } };
    DesignInstance.widthOnChange(event);
    getImageSizeValuesForSpecialElement(imageElement);
    expect(imageElement.style.width).toBe('50px');
  });
});

describe('highlightSelectedContainerAlignment', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
  });

  test('should not break with null input', () => {
    highlightSelectedContainerAlignment(null);
  });

  test('should not break with missing controls', () => {
    const el = createDiv('test-id');

    highlightSelectedContainerAlignment(el);
  });

  test('should not break with node child', () => {
    const el = createDiv('test-id');
    el.appendChild(document.createTextNode('Testing'));

    highlightSelectedContainerAlignment(el);
  });

  describe('for tables', () => {
    let selectedElement: HTMLElement;
    let childElement: HTMLElement;

    beforeEach(() => {
      selectedElement = document.createElement('div');
      childElement = document.createElement('table');
      selectedElement.appendChild(childElement);
    });

    test('missing controls should return safely', () => {
      highlightSelectedContainerAlignment(selectedElement);
    });

    describe('with controls existing', () => {
      beforeEach(() => {
        document.body.appendChild(createDiv(CONSTANTS.LOREE_TABLE_DESIGN_CENTER_ALIGNMENT));
        document.body.appendChild(createDiv(CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT));
        document.body.appendChild(createDiv(CONSTANTS.LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT));
      });

      test.each([
        ['0px', '10px', CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT],
        ['10px', '10px', CONSTANTS.LOREE_TABLE_DESIGN_CENTER_ALIGNMENT],
        ['10px', '0', CONSTANTS.LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT],
        ['0', '0', CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT],
      ])('table selected(%s, %s, %s)', (left, right, id) => {
        childElement.style.marginLeft = left;
        childElement.style.marginRight = right;

        highlightSelectedContainerAlignment(selectedElement);

        expect(document.getElementById(id)?.style.stroke).toBe('blue');
      });
    });
  });

  describe('for non-tables', () => {
    let selectedElement: HTMLElement;
    let childElement: HTMLElement;

    beforeEach(() => {
      selectedElement = document.createElement('div');
      childElement = document.createElement('P');
      selectedElement.appendChild(childElement);
    });

    test('missing controls should return', () => {
      highlightSelectedContainerAlignment(selectedElement);
    });

    describe('with controls existing', () => {
      beforeEach(() => {
        document.body.appendChild(createDiv(CONSTANTS.LOREE_CONTAINER_DESIGN_CENTER_ALIGNMENT));
        document.body.appendChild(createDiv(CONSTANTS.LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT));
        document.body.appendChild(createDiv(CONSTANTS.LOREE_CONTAINER_DESIGN_RIGHT_ALIGNMENT));
      });

      test.each([
        ['0px', '10px', CONSTANTS.LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT],
        ['10px', '10px', CONSTANTS.LOREE_CONTAINER_DESIGN_CENTER_ALIGNMENT],
        ['10px', '0', CONSTANTS.LOREE_CONTAINER_DESIGN_RIGHT_ALIGNMENT],
        ['0', '0', CONSTANTS.LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT],
      ])('container selected(%s, %s, %s)', (left, right, id) => {
        childElement.style.marginLeft = left;
        childElement.style.marginRight = right;

        highlightSelectedContainerAlignment(selectedElement);

        expect(document.getElementById(id)?.style.stroke).toBe('blue');
      });
    });
  });
});
