import Base, { selectedElement } from '../../base';
import CONSTANTS from '../../constant';
import ELEMENTS from '../../element';
import {
  ElementInterface,
  ElementContentInterface,
  ConfigInterface,
  FeatureInterface,
} from '../../interface';
import {
  categoryWrapper,
  handleBlockCategoryApplyButton,
  preventDropdownClosing,
  elementsBlock,
  fetchCategory,
  refreshCategoryDropdown,
  retrieveGlobalCustomBlockElement,
  updateCustomBlockThumbnail,
  retrieveCustomBlockElement,
} from '../customBlocks/customBlockEvents';
import { customBlockSearchWrapper } from '../customBlocks/customBlocksUI';
import {
  categoryInteractiveWrapper,
  handleInteractiveCategoryApplyButton,
  preventInteractiveDropdownClosing,
  interactiveBlockThumbnail,
  retriveInteractiveBlockElement,
  handleInteractiveSearchBlocks,
  searchInteractiveWrapper,
  newInteractiveWrapper,
  refreshInteractiveCategoryDropdown,
  refreshInteractiveCheckBox,
  liContent,
} from '../interactive';
import { appendH5PElement } from '../h5p';
import { interactiveIcons, videoModalIcons } from './iconHolder';
import { ImageModal } from '../imageModal/imageModal';
import { VideoModal } from '../videoModal/videoModal';
import { libraryApi, contentApi, libraryData, contentData } from '../interactiveapi';
import EmbedURL from './embedUrlElement/index';
import { isCanvasAndBB, isCanvas } from '../../../lmsConfig';
import { attachExternalTool, removeExternalTool } from './externalTool/index';
import { createButton, createDiv, getElementsByClassName } from '../../common/dom';
import { translate } from '../../../i18n/translate';
import { getEditorElementById } from '../../utils';
import { setCustomBlockEditOptions } from '../customBlocks/customBlockHandler';
let contentDetails: _Any = [];
let liLibraryDetail: _Any;

class Element extends Base {
  initiate = (config: ConfigInterface): void => {
    const sidebarContentWrapper = document.getElementById(CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER);
    if (!sidebarContentWrapper) return;
    // Wrapper
    const elementSection = document.createElement('div');
    elementSection.id = CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION;
    elementSection.className = 'section';
    elementSection.style.display = 'none';
    this.attachLabel(elementSection);
    this.attachContent(elementSection, config);
    if (config.features?.myinteractive !== undefined && config.features.myinteractive) {
      void this.appendInteractiveElement(elementSection, config.features);
    }
    sidebarContentWrapper.appendChild(elementSection);
  };

  imageClass = (): ImageModal => {
    const imageModal = new ImageModal();
    return imageModal;
  };

  videoClass = (): VideoModal => {
    const videoModal = new VideoModal();
    return videoModal;
  };

  embedUrlClass = (): EmbedURL => {
    const embedUrlModal = new EmbedURL();
    return embedUrlModal;
  };

  attachLabel = (elementSection: HTMLElement): void => {
    // Label
    const sectionLabel = document.createElement('button');
    sectionLabel.className = 'sectionLabel';
    sectionLabel.id = CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION_BUTTON;
    sectionLabel.onclick = () => {
      this.hideCustomElementSection();
      this.hideInteractiveElementSection();
      this.hideH5PElementSection();
      document
        .getElementsByClassName(CONSTANTS.LOREE_ELEMENTS_API_LOADER)[0]
        ?.classList.add('d-none');
    };
    sectionLabel.dataset.toggle = 'collapse';
    sectionLabel.dataset.target = `#loree-sidebar-element-wrapper`;
    sectionLabel.setAttribute('aria-expanded', 'false');
    sectionLabel.innerHTML = `
      <span class="label">${translate('global.element')}</span>
      <span class="image">
        <svg viewBox="0 0 8 14">
          <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
        </svg>
      </span>
    `;
    elementSection.appendChild(sectionLabel);
  };

  attachContent = (elementSection: HTMLElement, config: ConfigInterface): void => {
    // Content
    const featureList = config.features as FeatureInterface;
    const sectionContent = document.createElement('div');
    sectionContent.className = 'sectionContent';
    sectionContent.id = `${CONSTANTS.LOREE_SIDEBAR_ELEMENT_COLLAPSE}`;
    sectionContent.innerHTML = '';
    const sidebarElementsWrapper = document.createElement('div');
    sidebarElementsWrapper.className = 'sidebarElementsWrapper collapse';
    sidebarElementsWrapper.id = 'loree-sidebar-element-wrapper';
    sidebarElementsWrapper.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION_BUTTON}`;
    ELEMENTS.forEach((element): void => {
      if (this.showElementFeatures(element.id, featureList)) {
        const sidebarElements = document.createElement('button');
        sidebarElements.className = 'sidebarElement';
        const parentWrapper = document.createElement('div');
        parentWrapper.id = element.id;
        parentWrapper.className = 'icon-block';
        parentWrapper.innerHTML = element.template;
        if (!element.multiple) {
          sidebarElements.onclick = (): void =>
            this.handleSingleElementItem(element.content, element.name);
        }
        if (element.multiple) {
          sidebarElements.onclick = (): void =>
            this.handleMultipleElementItems(element, featureList);
        }
        if (element.id === CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT) {
          sidebarElements.onclick = async () => await this.handleCustomElements();
        }
        if (element.id === CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT) {
          sidebarElements.onclick = async () => {
            void this.handleInteractiveElements();
          };
        }
        if (element.id === CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT) {
          sidebarElements.onclick = async () => {
            appendH5PElement(elementSection);
          };
        }
        const label = document.createElement('div');
        label.className = 'label';
        label.innerHTML = element.name;
        sidebarElements.appendChild(parentWrapper);
        sidebarElements.appendChild(label);
        sidebarElementsWrapper.appendChild(sidebarElements);
      }
    });
    if (
      this.showElementFeatures(CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT, featureList) &&
      process.env.REACT_APP_ENABLE_EXTERNAL_LTI_TOOL === 'true' &&
      isCanvas()
    ) {
      const externalToolButton = createButton('', 'sidebarElement');
      attachExternalTool(externalToolButton);
      sidebarElementsWrapper.appendChild(externalToolButton);
    }
    sectionContent.appendChild(sidebarElementsWrapper);
    elementSection.appendChild(sectionContent);
  };

  interactivemodalLoader = (): string => {
    return `
      <div id="interactive-dropdown-loader" class=" loader m-auto justify-content-center">
        <div class="icon rotating">
        ${interactiveIcons.interactiveloader}
        </div>
        <div class="title ml-3">${translate('global.loading')}</div>
      </div>
    `;
  };

  showElementFeatures = (element: string, featureList: FeatureInterface): boolean => {
    if (
      (element === 'image' && (featureList.image === undefined || !featureList.image)) ||
      (element === 'video' && (featureList.video === undefined || !featureList.video)) ||
      (element === 'Text' && (featureList.textblock === undefined || !featureList.textblock)) ||
      (element === 'Divider' && (featureList.divider === undefined || !featureList.divider)) ||
      (element === 'specialblock' &&
        (featureList.specialblocks === undefined || !featureList.specialblocks)) ||
      (element === CONSTANTS.LOREE_IFRAME_EMBED_URL_ELEMENT &&
        (featureList.embedurl === undefined || !featureList.embedurl)) ||
      (element === CONSTANTS.LOREE_DESIGN_SECTION_CONTAINER_ELEMENT &&
        (featureList.container === undefined || !featureList.container)) ||
      (element === CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_ELEMENT &&
        (featureList.table === undefined || !featureList.table)) ||
      (element === CONSTANTS.LOREE_IFRAME_CONTENT_CUSTOM_ELEMENT &&
        (featureList.customelementsfiltersearch === undefined ||
          !featureList.customelementsfiltersearch)) ||
      (element === CONSTANTS.LOREE_DESIGN_SECTION_INTERACTIVE_ELEMENT &&
        (featureList.myinteractive === undefined || !featureList.myinteractive)) ||
      (element === CONSTANTS.LOREE_DESIGN_SECTION_H5P_ELEMENT &&
        (featureList.h5p === undefined || !featureList.h5p)) ||
      (element === CONSTANTS.LOREE_IFRAME_EXTERNAL_TOOL_ELEMENT &&
        (featureList.externalTools === undefined || !featureList.externalTools))
    ) {
      return false;
    } else {
      return true;
    }
  };

  // Banner
  appendBannerImgBlock = (): HTMLElement => {
    const bannerImgParentDiv = document.createElement('div');
    bannerImgParentDiv.innerHTML = `<div class='block-hover' id='bannerImg'>
      <div class='p-4 block-height image-background'></div>
      <p class='text-center block-textName'>${translate('element.banner')}</p>
    </div>`;
    bannerImgParentDiv.onclick = (): void => this.imageClass().imageUploadHandler('BANNER');
    return bannerImgParentDiv;
  };

  handleMultipleElementItems = (element: ElementInterface, featureList: FeatureInterface): void => {
    this.removeSelectedElementSection();
    const subSidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SUB_SIDEBAR);

    const activeElement: Array<HTMLElement> | HTMLElement | null = document.getElementById(
      element.id,
    );
    if (activeElement) activeElement.classList.add('active');
    if (!subSidebar) return;
    removeExternalTool(subSidebar);
    subSidebar.innerHTML = '';

    const subSidebarElementWrapper = createDiv();
    subSidebarElementWrapper.className = 'subSidebarElementWrapper';
    if (element.id === 'image' && featureList.banner !== false) {
      subSidebarElementWrapper.appendChild(this.appendBannerImgBlock());
    }

    element.contents.forEach((content: ElementContentInterface) => {
      const subSidebarElements = createDiv();
      subSidebarElements.className = 'subSidebarElements';
      let contentTemplate = content.template;
      switch (content.id) {
        case CONSTANTS.LOREE_IFRAME_IMAGE_ID:
          subSidebarElements.onclick = (): void =>
            this.handleSingleImageElementItem(content.content);
          break;
        case CONSTANTS.LOREE_IFRAME_VIDEO_ID:
          subSidebarElements.onclick = (): void => this.handleVideoUploadModal(content.content);
          break;
        case CONSTANTS.LOREE_IFRAME_LINE_ID:
        case CONSTANTS.LOREE_IFRAME_SPACE_ID:
          subSidebarElements.onclick = (): void => this.handleLineElementItem(content.content);
          break;
        case CONSTANTS.LOREE_IFRAME_ICON_PRAGRAPH_ID:
          subSidebarElements.onclick = (): void => this.handleSubMenuSelection(content);
          break;
        case CONSTANTS.LOREE_IFRAME_NAVIGATE_ID:
          if (
            featureList.navigationMenu === undefined ||
            !featureList.navigationMenu ||
            selectedElement?.tagName === 'TD' ||
            selectedElement?.tagName === 'TH'
          ) {
            contentTemplate = '';
          } else {
            subSidebarElements.onclick = (): void => this.handleSubMenuSelection(content);
          }
          break;
        default:
          subSidebarElements.onclick = (): void => this.handleSingleElementItem(content.content);
      }
      subSidebarElements.innerHTML = contentTemplate;
      subSidebarElementWrapper.appendChild(subSidebarElements);
    });
    subSidebar.appendChild(subSidebarElementWrapper);
    if (this.showSubSidebar) this.showSubSidebar();
  };

  handleSubMenuSelection = (subContent: ElementContentInterface): void => {
    this.removeSelectedElementSection();
    const subSidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SUB_SIDEBAR);

    const subSidebarElement: HTMLElement | null = document.querySelector<HTMLElement>(
      '.subSidebarElementWrapper',
    );
    const hideSubOption = Array.from(document.querySelectorAll('.subSidebarElements'));
    if (subSidebar && subSidebarElement) {
      subSidebar.innerHTML = '';
      this.appendSpecialELementBackButton(subSidebarElement);
      let hide;
      for (hide of hideSubOption) {
        if (hide) {
          hide.classList.add('d-none');
        }
      }
      subContent.innerContent.forEach((content) => {
        const subSidebarElements = document.createElement('div');
        subSidebarElements.className = 'subInnerSidebarElements';
        subSidebarElements.innerHTML = content.template;
        switch (content.id) {
          case CONSTANTS.LOREE_IFRAME_IMAGE_ID:
            subSidebarElements.onclick = (): void =>
              this.handleSingleImageElementItem(content.content);
            break;
          case CONSTANTS.LOREE_IFRAME_VIDEO_ID:
            subSidebarElements.onclick = (): void => this.handleVideoUploadModal(content.content);
            break;
          case CONSTANTS.LOREE_IFRAME_LINE_ID:
          case CONSTANTS.LOREE_IFRAME_SPACE_ID:
            subSidebarElements.onclick = (): void => this.handleLineElementItem(content.content);
            break;
          default:
            subSidebarElements.onclick = (): void => this.handleSingleElementItem(content.content);
        }
        subSidebarElement.appendChild(subSidebarElements);
      });
      subSidebar.appendChild(subSidebarElement);
      if (this.showSubSidebar) this.showSubSidebar();
    }
  };

  appendSpecialELementBackButton = (wrapper: HTMLElement): void => {
    const backButton = document.createElement('div');
    backButton.id = CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON;
    backButton.className = 'mt-n2 mb-2 ml-n2 back-to-editOption';
    backButton.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="8.158" height="13" viewBox="0 0 8.158 13"> <path id="back" d="M2747.584,141.8l3.934-3.934a1.21,1.21,0,1,0-1.711-1.711l-4.74,4.74a1.342,1.342,0,0,0,0,1.812l4.74,4.74a1.21,1.21,0,1,0,1.711-1.711Z" transform="translate(-2744.215 -135.3)" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"/> </svg> <span>${translate(
      'global.back',
    )}</span>`;
    backButton.onclick = (): void => this.showSpecialELements();
    wrapper.appendChild(backButton);
  };

  showSpecialELements = (): void => {
    const showSubOption = Array.from(document.querySelectorAll('.subSidebarElements'));
    let show;
    for (show of showSubOption) {
      if (show) show.classList.remove('d-none');
    }
    const hideSubOption = Array.from(document.querySelectorAll('.subInnerSidebarElements'));
    let hide;
    for (hide of hideSubOption) {
      if (hide) hide.remove();
    }
    const backButton = document.getElementById(CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_BACK_BUTTON);
    backButton?.remove();
  };

  handleSingleImageElementItem = (innerHTML: string): void => {
    const customHtml = this.customizeElement(innerHTML);
    this.imageClass().imageUploadHandler(customHtml);
  };

  // embed URL element
  attachEmbedUrlModalBackground = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const embedUrlModalBackground = document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_MODAL_BACKGROUND,
      ) as HTMLElement;
      embedUrlModalBackground.style.display = '';
    }
  };

  attachEmbedUrlModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const embedUrlModal = document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_MODAL_WRAPPER,
      ) as HTMLElement;
      embedUrlModal.style.display = 'block';
    }
  };

  handleSingleElementItem = (innerHTML: string, elementType?: string): void => {
    if (elementType === 'Embed URL') {
      this.hideSubSidebar();
      this.removeSelectedElementSection();
      this.attachEmbedUrlModalBackground();
      this.attachEmbedUrlModal();
      return;
    }
    this.appendElementToSelectedColumn(innerHTML);
    this.removeSelectedElementSection();
    this.hideSubSidebar();
  };

  handleLineElementItem = (innerHTML: string): void => {
    this.appendLineElementToSelectedColumn(innerHTML);
  };

  handleVideoUploadModal = (innerHTML: string): void => {
    const html = this.customizeElement(innerHTML);
    this.videoClass().videoUploadHandler(html);
  };

  customizeElement(html: string): string {
    const temp = document.createElement('div');
    temp.innerHTML = html;
    Array.from(temp.children).forEach((child) =>
      this.attachCustomizeStyleToElement(child as HTMLElement),
    );
    return temp.innerHTML;
  }

  handleCustomElements = async () => {
    this.hideSubSidebar();
    this.removeSelectedElementSection();
    const sidebarElementWrapper = getElementsByClassName('sidebarElementsWrapper')[0];

    if (sidebarElementWrapper) sidebarElementWrapper.classList.remove('show');
    this.hideCustomElementSection();

    const sidebar = getEditorElementById(CONSTANTS.LOREE_SIDEBAR);
    const loaderElement = createDiv('div');
    loaderElement.id = 'modal-loader';
    loaderElement.className = 'sidebarElementsLoader m-auto justify-content-center';
    loaderElement.innerHTML = `<div class="icon rotating">
      ${videoModalIcons.loader}
      </div>
      <div class="title ml-3">${translate('global.loading')}</div>`;
    sidebar?.appendChild(loaderElement);
    const elementSection = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION,
    ) as HTMLElement;
    await this.appendCustomElement(elementSection);
    this.refreshCustomBlockSearch('elements');
    this.attachCustomElementEvents();
  };

  handleInteractiveElements = async () => {
    this.hideSubSidebar();
    this.removeSelectedElementSection();
    const interactiveWrapper = document.createElement('div');
    interactiveWrapper.id = 'interactive-dropdown-loader';
    const space = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    space?.insertBefore(interactiveWrapper, null);
    if (interactiveWrapper) {
      interactiveWrapper.innerHTML = this.interactivemodalLoader();
    }
    if (!contentData) {
      contentDetails = await contentApi();
    }
    if (contentDetails) {
      const content = liContent.length === 0 ? contentDetails : liContent;
      const removedLoader = document.getElementById('interactive-dropdown-loader');
      removedLoader?.remove();
      void retriveInteractiveBlockElement(true, content, 0);
      void refreshInteractiveCheckBox('elements', true);
      const sidebarElementWrapper = document.getElementsByClassName('sidebarElementsWrapper')[0];
      if (sidebarElementWrapper) sidebarElementWrapper.classList.remove('show');
      const interactiveElementWrapper = document.getElementById(
        'loree-sidebar-interactive-element-input-wrapper',
      );
      if (interactiveElementWrapper) {
        interactiveElementWrapper.classList.remove('d-none');
        interactiveElementWrapper.classList.add('d-block');
      }
      this.attachInteractiveElementEvents();
    }
  };

  appendCustomElement = async (elementSection: HTMLElement) => {
    !isCanvasAndBB()
      ? await retrieveCustomBlockElement()
      : await retrieveGlobalCustomBlockElement();
    await fetchCategory();
    const sidebarElementsLoader = getElementsByClassName('sidebarElementsLoader')[0];
    if (!sidebarElementsLoader) {
      return;
    }
    sidebarElementsLoader.classList.add('d-none');
    const customRowInputWrapper = document.createElement('div');
    customRowInputWrapper.id = 'loree-sidebar-custom-element-input-wrapper';
    customRowInputWrapper.innerHTML = `<div class = "custom-block-title-name">${translate(
      'element.customelements',
    )}</div>`;
    customRowInputWrapper.appendChild(customBlockSearchWrapper('elements'));
    customRowInputWrapper.innerHTML += categoryWrapper('elements');
    customRowInputWrapper.appendChild(elementsBlock());
    sidebarElementsLoader?.remove();
    elementSection?.childNodes[1]?.appendChild(customRowInputWrapper);
    setCustomBlockEditOptions('My Element');
  };

  appendInteractiveElement = async (elementSection: HTMLElement, featureList: FeatureInterface) => {
    const removedLoader = document.getElementById('interactive-modal-loader');
    removedLoader?.remove();
    if (!libraryData) {
      liLibraryDetail = await libraryApi();
    }
    const interactiveRowInputWrapper = document.createElement('div');
    interactiveRowInputWrapper.id = 'loree-sidebar-interactive-element-input-wrapper';
    interactiveRowInputWrapper.className = 'd-none';
    let innerHTML = `<div class = "interactive-block-title-name">${translate(
      'element.loreeinteractives',
    )} </div>`;
    innerHTML += searchInteractiveWrapper('element');
    innerHTML += await categoryInteractiveWrapper('elements', liLibraryDetail);
    interactiveRowInputWrapper.innerHTML = innerHTML;
    if (featureList.myinteractive !== false)
      (interactiveRowInputWrapper.childNodes[1] as HTMLElement).insertAdjacentElement(
        'beforebegin',
        newInteractiveWrapper('elements'),
      );
    void interactiveBlockThumbnail('Elements', interactiveRowInputWrapper, 0);
    elementSection.childNodes[1]?.appendChild(interactiveRowInputWrapper);
  };

  attachCustomElementEvents = () => {
    const dropDownApplyBtn = document.getElementById(
      'loree-sidebar-custom-elements-category-button-apply',
    );
    if (dropDownApplyBtn) {
      dropDownApplyBtn.onclick = () => handleBlockCategoryApplyButton('elements');
    }
    const dropdownList = document.getElementById(
      'loree-sidebar-custom-elements-category-dropdown-menu',
    );
    if (dropdownList) {
      dropdownList.onclick = (e) => preventDropdownClosing('elements', e);
    }
    const selectedListWrapper = document.getElementById('elements-category-selected-list-wrapper');
    if (selectedListWrapper) {
      selectedListWrapper.onclick = (e) => preventDropdownClosing('elements', e);
    }
    const dropdownContainer = document.getElementById(
      'loree-sidebar-custom-elements-category-dropdown',
    );
    if (dropdownContainer) {
      dropdownContainer.onclick = () => refreshCategoryDropdown('elements');
    }
    const searchBox = document.getElementById('loree-sidebar-custom-elements-search-input-value');
    if (searchBox) searchBox.oninput = () => updateCustomBlockThumbnail('elements');
  };

  attachInteractiveElementEvents = () => {
    const dropDownApplyBtn = document.getElementById(
      'loree-sidebar-interactive-elements-category-button-apply',
    );
    if (dropDownApplyBtn) {
      dropDownApplyBtn.onclick = async () =>
        await handleInteractiveCategoryApplyButton('elements', false);
    }
    const dropdownList = document.getElementById(
      'loree-sidebar-interactive-elements-category-dropdown-menu',
    );
    if (dropdownList) {
      dropdownList.onclick = (e) => preventInteractiveDropdownClosing('elements', e);
    }
    const selectedListWrapper = document.getElementById('elements-category-selected-list-wrapper');
    if (selectedListWrapper) {
      selectedListWrapper.onclick = (e) => preventInteractiveDropdownClosing('elements', e);
    }
    const searchText = document.getElementById(
      'loree-sidebar-interactive-element-search-input-value',
    ) as HTMLInputElement | null;
    if (searchText) {
      searchText.oninput = async () => await handleInteractiveSearchBlocks();
    }
    const dropdownContainer = document.getElementById(
      'loree-sidebar-interactive-elements-category-dropdown',
    );
    if (dropdownContainer) {
      dropdownContainer.onclick = async () => await refreshInteractiveCategoryDropdown('elements');
    }
  };
}

export default Element;
