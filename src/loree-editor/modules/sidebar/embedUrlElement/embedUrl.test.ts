/* eslint-disable */ // Remove this line when editing this file
import EmbedURL from './index';
import Sidebar from '../sidebar';
import {
  appendModalContentBody,
  appendModalContentHeader,
  appendModalContentHeaderDivider,
  appendModelContentFooter,
} from './embedUrlModalUI';

jest.mock('./index');

beforeEach(() => {
  (EmbedURL as any).mockClear();
});

it('Embed url is should not be called without sidebar initiate', () => {
  expect(EmbedURL).not.toHaveBeenCalled();
});

it('Check whether embed url is called', () => {
  const sidebar = new Sidebar();
  expect(EmbedURL).toHaveBeenCalledTimes(1);
});

it('should have a modal header', () => {
  expect(appendModalContentHeader()).toBeInstanceOf(HTMLElement);
});

it('should have a modal divider', () => {
  expect(appendModalContentHeaderDivider()).toBeInstanceOf(HTMLElement);
});

it('should have a modal body', () => {
  expect(appendModalContentBody()).toBeInstanceOf(HTMLElement);
});

it('should have a modal footer', () => {
  expect(appendModelContentFooter()).toBeInstanceOf(HTMLElement);
});
