import { translate } from '../../../../i18n/translate';
import CONSTANTS from '../../../constant';
import { closeIcon } from '../iconHolder';

export const appendModalContentHeader = (): HTMLElement => {
  const header = document.createElement('div');
  header.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_HEADER;
  header.className = 'd-flex flex-row flex-wrap justify-content-between align-items-center';
  const headerText = document.createElement('p');
  headerText.innerHTML = `${translate('embedurl.embedurl')}`;
  headerText.className = 'modal-title';
  const headerIcon = document.createElement('button');
  headerIcon.id = CONSTANTS.LOREE_EMBED_URL_MODAL_WRAPPER_CLOSE;
  headerIcon.type = 'button';
  headerIcon.className = 'close';
  headerIcon.innerHTML = closeIcon;
  header.appendChild(headerText);
  header.appendChild(headerIcon);
  return header;
};

export const appendModalContentHeaderDivider = (): HTMLElement => {
  const divider = document.createElement('div');
  divider.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_DIVIDER;
  return divider;
};

export const appendModalContentBody = (): HTMLElement => {
  const embedUrlModalBody = document.createElement('div');
  embedUrlModalBody.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_BODY;
  const urlContentWrapper = document.createElement('div');
  urlContentWrapper.id = CONSTANTS.LOREE_EMBED_URL_USER_INPUT_WRAPPER;
  const iframeInput = document.createElement('textarea');
  iframeInput.id = CONSTANTS.LOREE_EMBED_URL_IFRAME_INPUT;
  iframeInput.placeholder = `${translate('embedurl.insertplaceholder')}`;
  iframeInput.className = 'form-control';
  const iframeInfo = document.createElement('p');
  iframeInfo.className = 'text-secondary pt-1 font-italic';
  iframeInfo.id = CONSTANTS.LOREE_EMBED_URL_IFRAME_INFO;
  iframeInfo.innerText = `${translate('embedurl.iframeinfo')}: <iframe src=""></iframe>`;
  urlContentWrapper.appendChild(iframeInput);
  urlContentWrapper.appendChild(iframeInfo);
  urlContentWrapper.appendChild(iframeCustomisationSection());
  embedUrlModalBody.appendChild(urlContentWrapper);
  return embedUrlModalBody;
};

const iframeCustomisationSection = (): HTMLElement => {
  const wrapper = document.createElement('div');
  wrapper.className = 'row';
  const iframeTitleSectionWrapper = document.createElement('div');
  iframeTitleSectionWrapper.className = 'col-12 col-md-6';
  iframeTitleSectionWrapper.appendChild(iframeTitle());
  const iframeDimensionsSectionWrapper = document.createElement('div');
  iframeDimensionsSectionWrapper.className = 'col-12 col-md-6';
  iframeDimensionsSectionWrapper.appendChild(iframeDimensionsInput());
  wrapper.appendChild(iframeTitleSectionWrapper);
  wrapper.appendChild(iframeDimensionsSectionWrapper);
  return wrapper;
};

const iframeTitle = (): HTMLElement => {
  const iframeTitleSection = document.createElement('div');
  iframeTitleSection.className = 'form-group';
  iframeTitleSection.id = CONSTANTS.LOREE_EMBED_URL_IFRAME_TITLE;
  const iframeTitleLabel = document.createElement('label');
  iframeTitleLabel.innerText = `${translate('embedurl.title')}`;
  const iframeTitleInput = document.createElement('input');
  iframeTitleInput.className = 'form-control';
  iframeTitleInput.placeholder = `${translate('embedurl.title')}`;
  iframeTitleSection.appendChild(iframeTitleLabel);
  iframeTitleSection.appendChild(iframeTitleInput);
  return iframeTitleSection;
};

const iframeDimensionsInput = (): HTMLElement => {
  const iframeDimensionsSection = document.createElement('div');
  iframeDimensionsSection.className = 'row';
  iframeDimensionsSection.id = CONSTANTS.LOREE_EMBED_URL_DIMENSIONS_INPUT;
  const iframeWidthSection = document.createElement('div');
  iframeWidthSection.className = 'form-group w-50 px-3';
  iframeWidthSection.innerHTML = `<label>${translate(
    'design.widthpixel',
  )}</label><input class="form-control px-2"  min="0" type="number" value="1200" /><div class="form-group form-check pt-1">
  <input type="checkbox" class="form-check-input" id=${CONSTANTS.LOREE_EMBED_URL_FULL_WIDTH} />
  <label class="form-check-label" for="loree-embed-url-full-width">${translate(
    'embedurl.fullwidth',
  )}</label>
  </div>`;
  const iframeHeightSection = document.createElement('div');
  iframeHeightSection.className = 'form-group w-50 px-3';
  iframeHeightSection.innerHTML = `<label>${translate(
    'design.heightpixel',
  )}</label><input class="form-control px-2"  min="0" type="number" value="700" />`;
  iframeDimensionsSection.appendChild(iframeWidthSection);
  iframeDimensionsSection.appendChild(iframeHeightSection);
  return iframeDimensionsSection;
};

export const appendModelContentFooter = (): HTMLElement => {
  const contentFooter = document.createElement('div');
  contentFooter.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_FOOTER;
  contentFooter.className = 'd-flex flex-row flex-nowrap align-items-center';
  const footerButton = document.createElement('div');
  footerButton.style.margin = 'auto';
  const cancelButton = document.createElement('button');
  cancelButton.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_FOOTER_CANCEL_BUTTON;
  cancelButton.className = 'mx-1';
  cancelButton.innerHTML = `${translate('global.cancel')}`;
  const embedButton = document.createElement('button');
  embedButton.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_FOOTER_ADD_BUTTON;
  embedButton.innerHTML = `${translate('global.embed')}`;
  embedButton.className = 'editor-btn-primary mx-1';
  embedButton.setAttribute('disabled', '');
  footerButton.appendChild(cancelButton);
  footerButton.appendChild(embedButton);
  contentFooter.appendChild(footerButton);
  return contentFooter;
};
