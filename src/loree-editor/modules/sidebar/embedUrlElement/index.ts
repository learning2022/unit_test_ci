import Base from '../../../base';
import CONSTANTS from '../../../constant';
import {
  appendModalContentBody,
  appendModalContentHeader,
  appendModalContentHeaderDivider,
  appendModelContentFooter,
} from './embedUrlModalUI';

export default class EmbedURL {
  initiate = (): void => {
    const embedUrlModalDialog = document.getElementById(CONSTANTS.LOREE_EMBED_URL_MODAL_DIALOG);
    const embedUrlModalContentTemplate: HTMLElement = document.createElement('div');
    embedUrlModalContentTemplate.className = 'modal-content';
    embedUrlModalContentTemplate.id = CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT;
    embedUrlModalContentTemplate.appendChild(appendModalContentHeader());
    embedUrlModalContentTemplate.appendChild(appendModalContentHeaderDivider());
    embedUrlModalContentTemplate.appendChild(appendModalContentBody());
    embedUrlModalContentTemplate.appendChild(appendModelContentFooter());
    if (embedUrlModalDialog) {
      embedUrlModalDialog.appendChild(embedUrlModalContentTemplate);
    }
  };

  baseClass = () => {
    const base = new Base();
    return base;
  };

  getModalElements = () => {
    const modal = {
      closeButton: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_MODAL_WRAPPER_CLOSE,
      ) as HTMLButtonElement,
      embedButton: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_FOOTER_ADD_BUTTON,
      ) as HTMLButtonElement,
      cancelButton: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_MODAL_CONTENT_FOOTER_CANCEL_BUTTON,
      ) as HTMLButtonElement,
      fullWidthCheckBox: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_FULL_WIDTH,
      ) as HTMLInputElement,
      dimensionInputWrapper: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_DIMENSIONS_INPUT,
      ) as HTMLInputElement,
      iframeInput: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_IFRAME_INPUT,
      ) as HTMLTextAreaElement,
      iframeInfo: document.getElementById(CONSTANTS.LOREE_EMBED_URL_IFRAME_INFO) as HTMLElement,
      iframeTitle: document.getElementById(
        CONSTANTS.LOREE_EMBED_URL_IFRAME_TITLE,
      ) as HTMLInputElement,
    };
    return modal;
  };

  embedUrlEventHandlers = () => {
    if (
      this.getModalElements().closeButton ||
      this.getModalElements().embedButton ||
      this.getModalElements().cancelButton ||
      this.getModalElements().fullWidthCheckBox ||
      this.getModalElements().iframeInput
    ) {
      this.handleAddButton(
        this.getModalElements().embedButton,
        this.getModalElements().iframeInput,
      );
      this.closeEmbedUrlModal(
        this.getModalElements().closeButton,
        this.getModalElements().cancelButton,
      );
      this.fullWidthStatusHandler(
        this.getModalElements().fullWidthCheckBox,
        this.getModalElements().dimensionInputWrapper,
      );
      this.getModalElements().iframeInput.addEventListener('paste', (event: ClipboardEvent) => {
        event.preventDefault();
        this.resetIframeInfo();
        this.resetIframeInput();
        const text = this.removeAttributes(event.clipboardData?.getData('Text') as string);
        this.getModalElements().iframeInput.value = text;
        this.getModalElements().embedButton.removeAttribute('disabled');
      });
      this.getModalElements().iframeInput.addEventListener('input', () => {
        const iframeInput = this.getModalElements().iframeInput;
        const embedButton = this.getModalElements().embedButton;
        if (iframeInput?.classList?.contains('border-danger'))
          iframeInput.classList.remove('border-danger');
        if (embedButton) embedButton.removeAttribute('disabled');
        if (iframeInput.value === '') embedButton.setAttribute('disabled', '');
        this.resetIframeInfo();
      });
    }
  };

  removeAttributes = (value: string) => {
    if (/^ *$/.test(value)) return '';
    const div = document.createElement('div');
    div.innerHTML = value;
    let output: string = '';
    div.childNodes?.forEach((child: ChildNode) => {
      if ((child as HTMLElement).tagName === 'IFRAME') {
        output = (child as HTMLElement).outerHTML;
      }
    });
    div.innerHTML = output;
    const element = div.childNodes[0] as HTMLIFrameElement;
    if (element?.tagName !== undefined && element?.tagName === 'IFRAME') {
      element.removeAttribute('style');
      return element.outerHTML;
    } else return '';
  };

  handleAddButton = (embedButton: HTMLButtonElement, iframeInput: HTMLTextAreaElement) => {
    embedButton.onclick = () => {
      const value = this.removeAttributes(iframeInput.value);
      iframeInput.value = value;
      this.validateUrlTextInput(iframeInput);
    };
  };

  validateUrlTextInput = (iframeInput: HTMLTextAreaElement) => {
    if (iframeInput.value === '') {
      this.embedUrlErrorhandler(iframeInput);
      return;
    }
    if (this.validatedIframeUrl(iframeInput.value)) {
      const iframeWrapper = document.createElement('div');
      iframeWrapper.id = CONSTANTS.LOREE_EMBED_URL_IFRAME_WRAPPER;
      this.iframeWrapperDefaultStyles(iframeWrapper);
      iframeWrapper.classList?.add(
        CONSTANTS.LOREE_IFRAME_CONTENT_EMBED_URL_WRAPPER,
        'element-highlight',
      );
      iframeWrapper.innerHTML = iframeInput.value.trim();
      (iframeWrapper.childNodes[0] as HTMLIFrameElement).style.maxWidth = '100%';
      (iframeWrapper.childNodes[0] as HTMLIFrameElement).style.maxHeight = '100%';
      this.baseClass().appendElementToSelectedColumn(this.customiseIframe(iframeWrapper));
      this.hideEmbedUrlModal();
    } else this.embedUrlErrorhandler(iframeInput);
  };

  iframeWrapperDefaultStyles = (iframeWrapper: HTMLElement): HTMLElement => {
    iframeWrapper.style.padding = '10px';
    iframeWrapper.style.marginBottom = '20px';
    iframeWrapper.style.maxWidth = '100%';
    iframeWrapper.style.maxHeight = '100%';
    return iframeWrapper;
  };

  embedUrlErrorhandler = (iframeInput: HTMLTextAreaElement) => {
    this.getModalElements().iframeInfo?.classList?.remove('text-secondary');
    this.getModalElements().iframeInfo?.classList?.add('text-danger');
    iframeInput.classList.add('border-danger');
  };

  validatedIframeUrl = (input: string): boolean => {
    if (
      input.includes('<iframe') &&
      input.includes('src="https://') &&
      (input.includes('</iframe>') || input.includes('/>'))
    ) {
      return true;
    } else return false;
  };

  customiseIframe = (iframeWrapper: HTMLElement): string => {
    const iframeElement = iframeWrapper.children[0] as HTMLIFrameElement;
    const widthInput = this.getModalElements().dimensionInputWrapper.children[0]
      .children[1] as HTMLInputElement;
    const heightInput = this.getModalElements().dimensionInputWrapper.children[1]
      .children[1] as HTMLInputElement;
    const widthCheckBox = this.getModalElements().fullWidthCheckBox;
    const iframeTitleInput = this.getModalElements().iframeTitle?.children[1] as HTMLInputElement;
    iframeWrapper.style.width = widthInput.value !== '0' ? `${widthInput.value}px` : '1200px';
    iframeWrapper.style.height = heightInput.value !== '0' ? 'auto' : '700px';
    iframeElement.setAttribute('width', widthInput.value !== '0' ? widthInput.value : '1200');
    iframeElement.setAttribute('height', heightInput.value !== '0' ? heightInput.value : '700');
    if (iframeTitleInput.value !== '') iframeElement.setAttribute('title', iframeTitleInput.value);
    if (widthCheckBox.checked) {
      iframeWrapper.style.width = '100%';
      iframeElement.setAttribute('width', '100%');
    }
    return iframeWrapper.outerHTML;
  };

  fullWidthStatusHandler = (checkBox: HTMLInputElement, inputField: HTMLInputElement): void => {
    checkBox.onclick = (e: MouseEvent) => {
      const selectedColumn = this.baseClass().getSelectedElement() as HTMLElement;
      const colFulWidth = window
        .getComputedStyle(selectedColumn)
        .getPropertyValue('width')
        .replace('px', '');
      const widthCheckBox = inputField.children[0]?.children[1] as HTMLInputElement;
      if (!(e.target as HTMLInputElement).checked) widthCheckBox.removeAttribute('readonly');
      if ((e.target as HTMLInputElement).checked) {
        widthCheckBox.setAttribute('readonly', '');
        widthCheckBox.value = colFulWidth;
      }
    };
  };

  closeEmbedUrlModal = (
    closeButton: HTMLButtonElement,
    footerCancelButton: HTMLButtonElement,
  ): void => {
    closeButton.onclick = () => this.hideEmbedUrlModal();
    footerCancelButton.onclick = () => this.hideEmbedUrlModal();
  };

  hideEmbedUrlModal = (): void => {
    this.disableEmbedButton();
    this.resetIframeInput();
    this.resetIframeInfo();
    this.resetIframeTitle();
    this.resetDimensionInput();
    this.hideModalWrapper();
  };

  disableEmbedButton = (): void => {
    const embedButton = this.getModalElements().embedButton;
    if (embedButton) embedButton.setAttribute('disabled', '');
  };

  resetDimensionInput = (): void => {
    const widthInput = this.getModalElements().dimensionInputWrapper?.children[0]
      ?.children[1] as HTMLInputElement;
    const fullWidthCheckBox = this.getModalElements().fullWidthCheckBox;
    const heightInput = this.getModalElements().dimensionInputWrapper?.children[1]
      ?.children[1] as HTMLInputElement;
    widthInput.value = '1200';
    if (fullWidthCheckBox.checked) fullWidthCheckBox.checked = false;
    if (widthInput.hasAttribute('readonly')) widthInput.removeAttribute('readonly');
    heightInput.value = '700';
  };

  resetIframeInput = (): void => {
    const iframeInput = this.getModalElements().iframeInput;
    iframeInput.value = '';
    if (iframeInput?.classList?.contains('border-danger'))
      iframeInput.classList.remove('border-danger');
  };

  resetIframeInfo = (): void => {
    const iframeInfo = this.getModalElements().iframeInfo;
    if (iframeInfo?.classList?.contains('text-danger')) {
      iframeInfo.classList.remove('text-danger');
      iframeInfo.classList.add('text-secondary');
    }
  };

  resetIframeTitle = (): void => {
    const iframeTitle = this.getModalElements().iframeTitle?.children[1] as HTMLInputElement;
    iframeTitle.value = '';
  };

  hideModalWrapper = (): void => {
    const embedUrlModalBackground = document.getElementById(
      CONSTANTS.LOREE_EMBED_URL_MODAL_BACKGROUND,
    ) as HTMLElement;
    const embedUrlModal = document.getElementById(
      CONSTANTS.LOREE_EMBED_URL_MODAL_WRAPPER,
    ) as HTMLElement;
    embedUrlModalBackground.style.display = 'none';
    embedUrlModal.style.display = 'none';
  };
}
