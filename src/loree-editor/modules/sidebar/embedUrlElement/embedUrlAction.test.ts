import EmbedURL from './index';

describe('#remove attributes from embeded iframe tag', () => {
  let EmbedUrlInstance: EmbedURL;
  beforeEach(() => {
    EmbedUrlInstance = new EmbedURL();
  });
  test('style attribute is present in Iframe tag', () => {
    const iframecontent = EmbedUrlInstance.removeAttributes(
      '<iframe frameborder="0" width="1200" height="700" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://view.genial.ly/60de3ac3578a120d59115011" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe>',
    );
    expect(iframecontent).toEqual(
      '<iframe frameborder="0" width="1200" height="700" src="https://view.genial.ly/60de3ac3578a120d59115011" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe>',
    );
  });
  test('style attribute is not present in Iframe tag', () => {
    const iframecontent = EmbedUrlInstance.removeAttributes(
      '<iframe frameborder="0" width="1200" height="700" src="https://view.genial.ly/60de3ac3578a120d59115011" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe>',
    );
    expect(iframecontent).toEqual(
      '<iframe frameborder="0" width="1200" height="700" src="https://view.genial.ly/60de3ac3578a120d59115011" type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all"></iframe>',
    );
  });
  test('getModalElements', () => {
    expect(EmbedUrlInstance.getModalElements()).toBeInstanceOf(Object);
  });
});
