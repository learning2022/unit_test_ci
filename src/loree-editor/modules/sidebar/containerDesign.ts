import Pickr from '@simonwep/pickr';
import Base from '../../base';
import CONSTANTS from '../../constant';
import ICONS from './icons';
import { ConfigInterface } from '../../interface';
import { highlightSelectedContainerAlignment } from './designStyle';
import '@simonwep/pickr/dist/themes/nano.min.css';
import { translate } from '../../../i18n/translate';

let borderColorPicker: Pickr;
let backgroundColorPicker: Pickr;
const customColors: string[] = [];
let lastsavedColor: string;
export const colorPickerConstants = {
  selectedBackgroundColor: '#ffffff',
};

interface ColorPickerInterface extends Pickr.Options {
  i18n: {
    'btn:save': string;
    'btn:cancel': string;
  };
}

class ContainerDesign extends Base {
  initiate = (config: ConfigInterface): void => {
    const sidebarContentWrapper = document.getElementById(CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER);
    if (sidebarContentWrapper) {
      this.appendCustomStyles(config);
      const containerDesignSection = document.createElement('div');
      containerDesignSection.id = CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION;
      containerDesignSection.className = 'section tableDesignSection';
      containerDesignSection.style.display = 'none';
      this.attachLabel(containerDesignSection);
      this.attachOptions(containerDesignSection);
      sidebarContentWrapper.appendChild(containerDesignSection);
      this.updateContainerSectionUpdateMethod(this.refreshContainerDesignSection);
    }
  };

  appendCustomStyles = (config: _Any) => {
    const colors = config.customColor?.colors;
    if (colors?.length) {
      colors.forEach((clr: { color: string }) => {
        customColors.push(clr.color);
      });
    }
  };

  refreshContainerDesignSection = (): void => {
    this.hideColorPickersOfContainerSection();
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_CONTAINER_WRAPPER)) {
        this.attachContainerOptions();
      }
    }
  };

  attachLabel = (wrapper: HTMLElement): void => {
    const sectionLabel = document.createElement('button');
    sectionLabel.className = 'sectionLabel ';
    sectionLabel.dataset.toggle = 'collapse';
    sectionLabel.dataset.target = `#${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    sectionLabel.setAttribute('aria-expanded', 'true');
    sectionLabel.innerHTML = `
      <span class="label">${translate('global.design')}</span>
      <span class="image">
        <svg viewBox="0 0 8 14">
          <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
        </svg>
      </span>
    `;
    wrapper.appendChild(sectionLabel);
  };

  attachContainerOptions = (): void => {
    const accordion = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (accordion) {
      accordion.innerHTML = '';
      this.attachSizeOption(accordion);
      this.attachSpaceOption(accordion);
      this.attachAlignmentOption(accordion);
      this.attachBorderOption(accordion);
      this.attachColorOption(accordion);
    }
  };

  attachOptions = (wrapper: HTMLElement): void => {
    const accordion = document.createElement('div');
    accordion.className = 'tableDesignAccordionWrapper accordion';
    accordion.id = CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER;
    wrapper.appendChild(accordion);
  };

  attachAlignmentOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#container-design-section-alignment`;
    accordionButton.innerHTML = `
      <span class='icon'>+</span>
      <span class="label">${translate('global.alignment')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'container-design-section-alignment';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const leftIcon = document.createElement('button');
    leftIcon.className = 'alignmentButton';
    leftIcon.innerHTML = ICONS.LEFT_ALIGNMENT_ICON;
    leftIcon.id = CONSTANTS.LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT;
    leftIcon.onclick = (): void => this.handleContainerAlignment('start');
    accordionContent.appendChild(leftIcon);
    const centerIcon = document.createElement('button');
    centerIcon.className = 'alignmentButton';
    centerIcon.id = CONSTANTS.LOREE_CONTAINER_DESIGN_CENTER_ALIGNMENT;
    centerIcon.innerHTML = ICONS.CENTER_ALIGNMENT_ICON;
    centerIcon.onclick = (): void => this.handleContainerAlignment('center');
    accordionContent.appendChild(centerIcon);
    const rightIcon = document.createElement('button');
    rightIcon.className = 'alignmentButton';
    rightIcon.id = CONSTANTS.LOREE_CONTAINER_DESIGN_RIGHT_ALIGNMENT;
    rightIcon.innerHTML = ICONS.RIGHT_ALIGNMENT_ICON;
    rightIcon.onclick = (): void => this.handleContainerAlignment('end');
    accordionContent.appendChild(rightIcon);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
    highlightSelectedContainerAlignment(this.getSelectedElement());
  };

  handleContainerAlignment = (position: string): void => {
    const selectedElement = this.getSelectedElement();
    const alignCenter = document.getElementById(
      CONSTANTS.LOREE_CONTAINER_DESIGN_CENTER_ALIGNMENT,
    ) as HTMLElement;
    const alignLeft = document.getElementById(
      CONSTANTS.LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT,
    ) as HTMLElement;
    const alignRight = document.getElementById(
      CONSTANTS.LOREE_CONTAINER_DESIGN_RIGHT_ALIGNMENT,
    ) as HTMLElement;
    if (!selectedElement) return;
    const child = selectedElement.children[0] as HTMLElement;
    if (!child) return;
    child.style.removeProperty('margin');
    switch (position) {
      case 'start':
        alignLeft.style.stroke = 'blue';
        alignRight.style.stroke = '';
        alignCenter.style.stroke = '';
        child.style.margin = '0';
        break;
      case 'center':
        alignLeft.style.stroke = '';
        alignRight.style.stroke = '';
        alignCenter.style.stroke = 'blue';
        child.style.margin = '0 auto';
        break;
      case 'end':
        alignLeft.style.stroke = '';
        alignRight.style.stroke = 'blue';
        alignCenter.style.stroke = '';
        child.style.margin = '0 0 0 auto';
        break;
    }
    this.showAddRowButtonToSelectedContainer();
  };

  handleAccordionButtonClick = (e: Event): void => {
    const wrapper = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (wrapper) {
      const element = e.target as HTMLElement;
      let label = '';
      if (element.tagName === 'SPAN') {
        const parent = element.parentElement;
        if (parent) {
          const icon = parent.getElementsByClassName('icon')[0];
          label = parent.getElementsByClassName('label')[0].innerHTML.trim();
          if (icon) {
            if (icon.innerHTML === '+') {
              icon.innerHTML = 'I';
            } else {
              icon.innerHTML = '+';
            }
          }
        }
      } else {
        const icon = element.getElementsByClassName('icon')[0];
        label = element.getElementsByClassName('label')[0].innerHTML.trim();
        if (icon) {
          if (icon.innerHTML === '+') {
            icon.innerHTML = 'I';
          } else {
            icon.innerHTML = '+';
          }
        }
      }
      const accordionButton = wrapper.getElementsByClassName('accordionButton');
      Array.from(accordionButton).forEach((button) => {
        const icon = button.getElementsByClassName('icon')[0];
        const buttonLabel = button.getElementsByClassName('label')[0].innerHTML.trim();
        if (icon) {
          if (buttonLabel !== label) icon.innerHTML = '+';
        }
      });
    }
  };

  attachSizeOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#container-design-section-size`;
    accordionButton.innerHTML = `
      <span class='icon'>+</span>
      <span class="label">Size</span>
    `;
    alignmentWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'container-design-section-size';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const widthOption = document.createElement('div');
    widthOption.className = 'widthOption';

    // Width label
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = 'Width (%)';
    widthOption.appendChild(label);

    // Width input box
    const input = document.createElement('input');
    input.className = 'input form-control-design';
    input.type = 'number';
    input.value = this.getContainerWidth();
    input.onchange = this.handleContainerWidthChange;
    widthOption.appendChild(input);

    accordionContent.appendChild(widthOption);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
  };

  getContainerWidth = (): string => {
    let value = '100';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const container = selectedElement.children[0] as HTMLElement;
    if (!container) return value;
    const width = container.style.width.split('%')[0];
    if (width) value = width;
    return value;
  };

  handleContainerWidthChange = (e: Event) => {
    e.preventDefault();
    this.hideAddRowButtonToSelectedContainer();
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    const container = selectedElement.children[0] as HTMLElement;
    if (!container) return;
    const element = e.target as HTMLInputElement;
    const value = parseInt(element.value);
    if (value >= 0 && value <= 100) {
      element.value = `${value}`;
      container.style.width = `${value}%`;
    } else {
      if (value < 0) element.value = '0';
      if (value > 100) element.value = '100';
    }
    this.showAddRowButtonToSelectedContainer();
  };

  attachSpaceOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#container-design-section-space`;
    accordionButton.innerHTML = `
      <span class='icon'>+</span>
      <span class="label">${translate('element.space')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);

    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'container-design-section-space';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const containerPaddingSpaceWrapper = document.createElement('div');
    containerPaddingSpaceWrapper.className = 'tablePaddingSpaceWrapper';
    const paddingLabel = document.createElement('div');
    paddingLabel.className =
      'label d-flex justify-content-between mb-2 editor-design-margin-heading';
    paddingLabel.innerHTML = '<span>Padding</span><span><i>px</i></span>';
    containerPaddingSpaceWrapper.appendChild(paddingLabel);
    const paddingOptions = document.createElement('div');
    paddingOptions.className = 'options';

    this.leftPaddingOption(paddingOptions);
    this.topPaddingOption(paddingOptions);
    this.rightPaddingOption(paddingOptions);
    this.bottomPaddingOption(paddingOptions);
    containerPaddingSpaceWrapper.appendChild(paddingOptions);
    accordionContent.appendChild(containerPaddingSpaceWrapper);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
  };

  leftMarginOption = (wrapper: HTMLElement): void => {
    const leftOptionMargin = document.createElement('div');
    leftOptionMargin.className = 'option';
    const leftOptionMarginChild = document.createElement('div');
    leftOptionMarginChild.className = 'leftOptionMargin';
    const leftOptionMarginInput = document.createElement('input');
    leftOptionMarginInput.type = 'number';
    leftOptionMarginInput.className = 'form-control-design';
    leftOptionMarginInput.value = this.getContainerMargin('left');
    leftOptionMarginInput.onchange = (e: Event): void =>
      this.handleContainerMarginChange(e, 'left');
    leftOptionMarginChild.appendChild(leftOptionMarginInput);
    leftOptionMargin.appendChild(leftOptionMarginChild);
    wrapper.appendChild(leftOptionMargin);
  };

  topMarginOption = (wrapper: HTMLElement): void => {
    const topOptionMargin = document.createElement('div');
    topOptionMargin.className = 'option';
    const topOptionMarginChild = document.createElement('div');
    topOptionMarginChild.className = 'topOptionMargin';
    const topOptionMarginInput = document.createElement('input');
    topOptionMarginInput.type = 'number';
    topOptionMarginInput.className = 'form-control-design';
    topOptionMarginInput.value = this.getContainerMargin('top');
    topOptionMarginInput.onchange = (e: Event): void => this.handleContainerMarginChange(e, 'top');
    topOptionMarginChild.appendChild(topOptionMarginInput);
    topOptionMargin.appendChild(topOptionMarginChild);
    wrapper.appendChild(topOptionMargin);
  };

  rightMarginOption = (wrapper: HTMLElement): void => {
    const rightOptionMargin = document.createElement('div');
    rightOptionMargin.className = 'option';
    const rightOptionMarginChild = document.createElement('div');
    rightOptionMarginChild.className = 'rightOptionMargin';
    const rightOptionMarginInput = document.createElement('input');
    rightOptionMarginInput.type = 'number';
    rightOptionMarginInput.className = 'form-control-design';
    rightOptionMarginInput.value = this.getContainerMargin('right');
    rightOptionMarginInput.onchange = (e: Event): void =>
      this.handleContainerMarginChange(e, 'right');
    rightOptionMarginChild.appendChild(rightOptionMarginInput);
    rightOptionMargin.appendChild(rightOptionMarginChild);
    wrapper.appendChild(rightOptionMargin);
  };

  bottomMarginOption = (wrapper: HTMLElement): void => {
    const bottomOptionMargin = document.createElement('div');
    bottomOptionMargin.className = 'option';
    const bottomOptionMarginChild = document.createElement('div');
    bottomOptionMarginChild.className = 'bottomOptionMargin';
    const bottomOptionMarginInput = document.createElement('input');
    bottomOptionMarginInput.type = 'number';
    bottomOptionMarginInput.className = 'form-control-design';
    bottomOptionMarginInput.value = this.getContainerMargin('bottom');
    bottomOptionMarginInput.onchange = (e: Event): void =>
      this.handleContainerMarginChange(e, 'bottom');
    bottomOptionMarginChild.appendChild(bottomOptionMarginInput);
    bottomOptionMargin.appendChild(bottomOptionMarginChild);
    wrapper.appendChild(bottomOptionMargin);
  };

  getContainerMargin = (position: string): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    switch (position) {
      case 'top':
        value = selectedElement.style.marginTop.replace('px', '');
        if (!value) value = '0';
        break;
      case 'bottom':
        value = selectedElement.style.marginBottom.replace('px', '');
        if (!value) value = '0';
        break;
      case 'left':
        value = selectedElement.style.marginLeft.replace('px', '');
        if (!value) value = '0';
        break;
      case 'right':
        value = selectedElement.style.marginRight.replace('px', '');
        if (!value) value = '0';
        break;
    }
    return value;
  };

  handleContainerMarginChange = (e: Event, position: string): void => {
    const element = e.target as HTMLInputElement;
    switch (position) {
      case 'top':
        this.applyStyleToSelectedElement('margin-top', `${element.value}px`);
        break;
      case 'bottom':
        this.applyStyleToSelectedElement('margin-bottom', `${element.value}px`);
        break;
      case 'left':
        this.applyStyleToSelectedElement('margin-left', `${element.value}px`);
        break;
      case 'right':
        this.applyStyleToSelectedElement('margin-right', `${element.value}px`);
        break;
    }
  };

  leftPaddingOption = (wrapper: HTMLElement): void => {
    const leftOptionMargin = document.createElement('div');
    leftOptionMargin.className = 'option';
    const leftOptionMarginChild = document.createElement('div');
    leftOptionMarginChild.className = 'leftOptionPadding';
    const leftOptionMarginInput = document.createElement('input');
    leftOptionMarginInput.type = 'number';
    leftOptionMarginInput.className = 'form-control-design';
    leftOptionMarginInput.value = this.getContainerPadding('left');
    leftOptionMarginInput.onchange = (e: Event): void =>
      this.handleContainerPaddingChange(e, 'left');
    leftOptionMarginChild.appendChild(leftOptionMarginInput);
    leftOptionMargin.appendChild(leftOptionMarginChild);
    wrapper.appendChild(leftOptionMargin);
  };

  topPaddingOption = (wrapper: HTMLElement): void => {
    const topOptionMargin = document.createElement('div');
    topOptionMargin.className = 'option';
    const topOptionMarginChild = document.createElement('div');
    topOptionMarginChild.className = 'topOptionPadding';
    const topOptionMarginInput = document.createElement('input');
    topOptionMarginInput.type = 'number';
    topOptionMarginInput.className = 'form-control-design';
    topOptionMarginInput.value = this.getContainerPadding('top');
    topOptionMarginInput.onchange = (e: Event): void => this.handleContainerPaddingChange(e, 'top');
    topOptionMarginChild.appendChild(topOptionMarginInput);
    topOptionMargin.appendChild(topOptionMarginChild);
    wrapper.appendChild(topOptionMargin);
  };

  rightPaddingOption = (wrapper: HTMLElement): void => {
    const rightOptionMargin = document.createElement('div');
    rightOptionMargin.className = 'option';
    const rightOptionMarginChild = document.createElement('div');
    rightOptionMarginChild.className = 'rightOptionPadding';
    const rightOptionMarginInput = document.createElement('input');
    rightOptionMarginInput.type = 'number';
    rightOptionMarginInput.className = 'form-control-design';
    rightOptionMarginInput.value = this.getContainerPadding('right');
    rightOptionMarginInput.onchange = (e: Event): void =>
      this.handleContainerPaddingChange(e, 'right');
    rightOptionMarginChild.appendChild(rightOptionMarginInput);
    rightOptionMargin.appendChild(rightOptionMarginChild);
    wrapper.appendChild(rightOptionMargin);
  };

  bottomPaddingOption = (wrapper: HTMLElement): void => {
    const bottomOptionMargin = document.createElement('div');
    bottomOptionMargin.className = 'option';
    const bottomOptionMarginChild = document.createElement('div');
    bottomOptionMarginChild.className = 'bottomOptionPadding';
    const bottomOptionMarginInput = document.createElement('input');
    bottomOptionMarginInput.type = 'number';
    bottomOptionMarginInput.className = 'form-control-design';
    bottomOptionMarginInput.value = this.getContainerPadding('bottom');
    bottomOptionMarginInput.onchange = (e: Event): void =>
      this.handleContainerPaddingChange(e, 'bottom');
    bottomOptionMarginChild.appendChild(bottomOptionMarginInput);
    bottomOptionMargin.appendChild(bottomOptionMarginChild);
    wrapper.appendChild(bottomOptionMargin);
  };

  getContainerPadding = (position: string): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    switch (position) {
      case 'top':
        value = selectedElement.style.paddingTop.replace('px', '');
        if (!value) value = '0';
        break;
      case 'bottom':
        value = selectedElement.style.paddingBottom.replace('px', '');
        if (!value) value = '0';
        break;
      case 'left':
        value = selectedElement.style.paddingLeft.replace('px', '');
        if (!value) value = '0';
        break;
      case 'right':
        value = selectedElement.style.paddingRight.replace('px', '');
        if (!value) value = '0';
        break;
    }
    return value;
  };

  handleContainerPaddingChange = (e: Event, position: string): void => {
    const element = e.target as HTMLInputElement;
    switch (position) {
      case 'top':
        this.applyStyleToSelectedElement('padding-top', `${element.value}px`);
        break;
      case 'bottom':
        this.applyStyleToSelectedElement('padding-bottom', `${element.value}px`);
        break;
      case 'left':
        this.applyStyleToSelectedElement('padding-left', `${element.value}px`);
        break;
      case 'right':
        this.applyStyleToSelectedElement('padding-right', `${element.value}px`);
        break;
    }
  };

  attachBorderOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#container-design-section-border`;
    accordionButton.innerHTML = `
      <span class='icon'>+</span>
      <span class="label">${translate('global.border')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'container-design-section-border';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const borderOption = document.createElement('div');
    borderOption.className = 'borderOption';

    const borderWidth = document.createElement('div');
    borderWidth.className = 'borderWidth d-flex justify-content-between';

    const labelWrapper = document.createElement('div');
    labelWrapper.className = 'd-flex py-1';
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = 'Width (px)';
    borderWidth.appendChild(label);

    const selectedElement = this.getSelectedElement();
    const input = document.createElement('input');
    input.className = 'input form-control-design';
    input.type = 'number';
    input.value = this.getBorderWidthOfContainer();
    input.onchange = (e: Event): void => this.handleContainerBorderWidthChange(e);
    labelWrapper.appendChild(input);

    const color = document.createElement('button');
    color.className = 'color';
    color.style.backgroundColor = this.getBorderColorOfContainer(selectedElement);
    labelWrapper.appendChild(color);
    this.attachBorderColorPicker(color, selectedElement);
    borderWidth.appendChild(labelWrapper);
    borderOption.appendChild(borderWidth);

    const borderType = document.createElement('div');
    borderType.className = 'borderType';

    const select = document.createElement('select');
    select.innerHTML = `
      <option value="solid">${translate('design.solid')}</option>
      <option value="dashed">${translate('design.dashed')}</option>
      <option value="dotted">${translate('design.dotted')}</option>
      <option value="double">${translate('design.double')}</option>
      <option value="none">${translate('design.none')}</option>
    `;
    select.value = this.getBorderStyleOfContainer(selectedElement);
    select.id = CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_BORDER_STYLE;
    select.onchange = (e: Event): void => this.handleContainerBorderStyleChange(e);
    borderType.appendChild(select);

    borderOption.appendChild(borderType);
    accordionContent.appendChild(borderOption);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
    this.fixColorPickerBehaviour();
  };

  getBorderWidthOfContainer = (): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const borderWidth = selectedElement.style.borderWidth;
    if (!borderWidth) return value;
    value = borderWidth.replace('px', '');
    return value;
  };

  handleContainerBorderWidthChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    const tableBorderStyle = this.getBorderStyleOfContainer(selectedElement);
    this.applyStyleToSelectedElement('border-width', `${element.value}px`);
    this.applyStyleToSelectedElement('border-style', tableBorderStyle);
  };

  getBorderStyleOfContainer = (selectedElement: HTMLElement | null): string => {
    let value = 'solid';
    if (!selectedElement) return value;
    const borderStyle = selectedElement.style.borderStyle;
    if (!borderStyle) return value;
    value = borderStyle;
    return value;
  };

  handleContainerBorderStyleChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedElement('border-style', element.value);
  };

  getBorderColorOfContainer = (selectedElement: HTMLElement | null): string => {
    let value = '#a5a5a5';
    if (!selectedElement) return value;
    const borderColor = selectedElement.style.borderColor;
    if (!borderColor) return value;
    value = borderColor;
    return value;
  };

  attachBorderColorPicker = (element: HTMLElement, selectedElement: HTMLElement | null): void => {
    const defaultColor = this.getBorderColorOfContainer(selectedElement);
    if (element) {
      const options: ColorPickerInterface = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS,
        default: '#FFFFFF',
        defaultRepresentation: 'HEXA',
        useAsButton: true,
        lockOpacity: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            input: true,
            rgba: false,
            hex: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      borderColorPicker = new Pickr(options);
      borderColorPicker.on('change', (color: { toHEXA: Function }) => {
        const hexValue = color.toHEXA();
        this.handleContainerBorderColorChange(hexValue, element);
      });

      borderColorPicker.on('save', (color: { toHEXA: Function }) => {
        if (color) {
          const hexValue = color.toHEXA();
          this.handleContainerBorderColorChange(hexValue, element);
          borderColorPicker?.hide();
          lastsavedColor = hexValue;
        }
      });

      borderColorPicker.on('cancel', () => {
        if (lastsavedColor) {
          this.handleContainerBorderColorChange(lastsavedColor, element);
        } else {
          this.handleContainerBorderColorCancel(defaultColor, element);
        }
        borderColorPicker?.hide();
      });

      borderColorPicker.on('clear', () => {
        this.handleContainerBorderColorCancel(defaultColor, element);
        lastsavedColor = '';
        backgroundColorPicker?.hide();
      });
    }
  };

  handleContainerBorderColorChange = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedElement('border-color', color.toString());
    element.style.backgroundColor = color;
  };

  handleContainerBorderColorCancel = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedElement('border-width', color.toString());
    this.applyStyleToSelectedElement('border-color', color.toString());
    this.applyStyleToSelectedElement('border-style', color.toString());
    element.style.backgroundColor = color.toString();
  };

  attachColorOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#container-design-section-color`;
    accordionButton.innerHTML = `
      <span class='icon'>+</span>
      <span class="label">${translate('global.color')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);

    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'container-design-section-color';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const colorOption = document.createElement('div');
    colorOption.className = 'colorOption';

    const backgroundColorOption = document.createElement('div');
    backgroundColorOption.className = 'backgroundColorOption';

    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = translate('design.background');
    backgroundColorOption.appendChild(label);

    const selectedElement = this.getSelectedElement();
    const color = document.createElement('button');
    color.className = 'color';
    color.id = CONSTANTS.LOREE_CONTAINER_BACKGROUND_COLOR_BUTTON;
    color.style.backgroundColor = this.getBackgroundColorOfContainer(selectedElement);
    backgroundColorOption.appendChild(color);
    colorOption.appendChild(backgroundColorOption);
    this.attachBackgroundColorPicker(color);
    accordionContent.appendChild(colorOption);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
    this.fixColorPickerBehaviour();
  };

  getBackgroundColorOfContainer = (selectedElement: HTMLElement | null): string => {
    let value: string | undefined = '#ffffff';
    if (!selectedElement) return value;
    const backgroundColor = selectedElement.style.backgroundColor;
    if (!backgroundColor) {
      const columnParentElement: HTMLElement | null = selectedElement.parentElement;
      const rowParentElement = columnParentElement?.parentElement;
      if (columnParentElement?.style.backgroundColor !== '') {
        value = columnParentElement?.style.backgroundColor;
      } else if (rowParentElement?.style.backgroundColor !== '') {
        value = rowParentElement?.style.backgroundColor;
      }
      return value as string;
    }
    value = backgroundColor;
    return value;
  };

  getContainerBackgroundColor = () => {
    return colorPickerConstants.selectedBackgroundColor;
  };

  setContainerBackgroundColor = (color: string) => {
    colorPickerConstants.selectedBackgroundColor = color;
  };

  attachBackgroundColorPicker = (element: HTMLElement): void => {
    colorPickerConstants.selectedBackgroundColor = this.getBackgroundColorOfContainer(element);
    if (element) {
      const options: ColorPickerInterface = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: `${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS} container-background-picker`,
        default: colorPickerConstants.selectedBackgroundColor,
        defaultRepresentation: 'HEXA',
        useAsButton: true,
        lockOpacity: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            hex: true,
            rgba: false,
            input: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      backgroundColorPicker = Pickr.create(options);
      backgroundColorPicker.on('change', (color: { toHEXA: Function }) => {
        const hexValue = color.toHEXA();
        this.handleContainerBackgroundColorChange(hexValue, element);
      });

      backgroundColorPicker.on('save', (color: { toHEXA: Function }) => {
        if (color) {
          const hexValue = color.toHEXA();
          this.setContainerBackgroundColor(hexValue);
          this.handleContainerBackgroundColorChange(hexValue, element);
          backgroundColorPicker?.hide();
        }
      });

      backgroundColorPicker.on('cancel', () => {
        this.handleContainerBackgroundColorChange(
          colorPickerConstants.selectedBackgroundColor,
          element,
        );
        backgroundColorPicker?.hide();
      });

      backgroundColorPicker.on('clear', () => {
        this.setContainerBackgroundColor('#ffffff');
        backgroundColorPicker?.hide();
      });

      backgroundColorPicker.on('hide', () => {
        void Promise.resolve().then(() =>
          this.handleContainerBackgroundColorChange(
            colorPickerConstants.selectedBackgroundColor,
            element,
          ),
        );
      });

      backgroundColorPicker.on('show', () => {
        backgroundColorPicker?.setColor(colorPickerConstants.selectedBackgroundColor, true);
      });
    }
  };

  handleContainerBackgroundColorChange = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    const appliedColor = color !== '' ? color.toString() : '#ffffff';
    this.applyStyleToSelectedElement('background-color', appliedColor);
    element.style.backgroundColor = appliedColor;
  };

  fixColorPickerBehaviour = (): void => {
    const pcrSelections = document.querySelectorAll('.pcr-selection');
    const pcrResult = document.querySelectorAll('.pcr-result');
    pcrSelections.forEach((selection) => {
      selection.addEventListener('click', () => {
        pcrResult.forEach((input) => {
          const inputField = input as HTMLInputElement;
          inputField.blur();
        });
      });
    });
  };
}

export default ContainerDesign;
