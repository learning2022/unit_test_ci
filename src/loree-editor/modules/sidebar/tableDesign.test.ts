import TableDesign from './tableDesign';
import Base from '../../base';
import CONSTANTS from '../../constant';
import { templateConfig } from '../header/templateMockData';
import { getElementById, createElement } from '../../common/dom';
import { getEditorElementsByClassName, getElementByClassName } from '../../utils';
import { fireEvent } from '@testing-library/react';
import { tableDesignBorderSectionMock, tableMockData } from './designMockdata';

const tableDesign = new TableDesign();
const baseInstance = new Base();
const tableWrapper = createElement('div');
tableWrapper.className = `${CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER}`;
const table = document.createElement('table');
table.className = `${CONSTANTS.LOREE_IFRAME_CONTENT_TABLE}`;
const tbody = createElement('tbody');
const trElement = document.createElement('tr');
const tdElement = document.createElement('td');
tdElement.setAttribute(
  'style',
  'height: 50px; padding: 5px 10px 15px 20px; margin: 20px 15px 10px 5px',
);
tdElement.innerHTML = `<p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 16px 0px;">Insert Text Here.</p>`;
trElement.appendChild(tdElement);
tbody.appendChild(trElement);
table.appendChild(tbody);
tableWrapper.appendChild(table);
describe('#getTableMargin', () => {
  beforeAll(() => {
    const base: Base = new Base();
    jest.spyOn(base, 'changeSelectedElement');
    base.changeSelectedElement(tdElement);
  });

  describe('when margin top is needed', () => {
    test('returns margin top value', () => {
      const result = tableDesign.getTableMargin('top');
      expect(result).toEqual('20');
    });
  });
  describe('when margin bottom is needed', () => {
    test('returns margin bottom value', () => {
      const result = tableDesign.getTableMargin('bottom');
      expect(result).toEqual('10');
    });
  });
  describe('when margin left is needed', () => {
    test('returns margin left value', () => {
      const result = tableDesign.getTableMargin('left');
      expect(result).toEqual('5');
    });
  });
  describe('when margin right is needed', () => {
    test('returns margin right value', () => {
      const result = tableDesign.getTableMargin('right');
      expect(result).toEqual('15');
    });
  });
});

describe('#getTablePadding', () => {
  beforeAll(() => {
    const base: Base = new Base();
    jest.spyOn(base, 'changeSelectedElement');
    base.changeSelectedElement(tdElement);
  });
  describe('when padding top is needed', () => {
    test('returns padding top value', () => {
      const result = tableDesign.getTablePadding('top');
      expect(result).toEqual('5');
    });
  });
  describe('when padding bottom is needed', () => {
    test('returns padding bottom value', () => {
      const result = tableDesign.getTablePadding('bottom');
      expect(result).toEqual('15');
    });
  });
  describe('when padding left is needed', () => {
    test('returns padding left value', () => {
      const result = tableDesign.getTablePadding('left');
      expect(result).toEqual('20');
    });
  });
  describe('when padding right is needed', () => {
    test('returns padding right value', () => {
      const result = tableDesign.getTablePadding('right');
      expect(result).toEqual('10');
    });
  });
});

describe('#handleTableBackgroundColorChange', () => {
  const doc = document.body;
  const element = document.createElement('button');
  let selected: NodeListOf<HTMLTableDataCellElement>;
  const mockedColorStyles: [] = [];
  beforeEach(() => {
    const table = tableMockData;
    doc.insertAdjacentHTML('beforeend', table);
    doc.appendChild(element);
    selected = doc.querySelectorAll('td');
    tableDesign.getPreviousElementColorStyles = jest
      .fn()
      .mockImplementation(() => mockedColorStyles);
  });

  test('on save store element style method should be called', () => {
    tableDesign.storePreviousElementColorStyles = jest.fn();
    tableDesign.handleTableBackgroundColorChange('rgb(215, 215, 0)', element, 'save');
    expect(tableDesign.storePreviousElementColorStyles).toBeCalledTimes(1);
  });

  test('change colour on an a cell with no colour and apply', () => {
    tableDesign.getSelectedElements = jest.fn().mockImplementation(() => [selected[0]]);
    expect(selected[0].style.backgroundColor).toEqual('');
    tableDesign.handleTableBackgroundColorChange('rgb(215, 215, 0)', element, 'change');
    expect(selected[0].style.backgroundColor).toEqual('rgb(215, 215, 0)');
  });

  test('change color on a cell with no colour but then cancel', () => {
    tableDesign.getSelectedElements = jest.fn().mockImplementation(() => [selected[1]]);
    tableDesign.getPreviousElementColorStyles = jest.fn().mockImplementation(() => ['']);
    tableDesign.handleTableBackgroundColorChange('rgb(215, 215, 0)', element, 'change');
    expect(selected[1].style.backgroundColor).toEqual('rgb(215, 215, 0)');
    tableDesign.handleTableBackgroundColorChange('rgb(215, 215, 0)', element, 'cancel');
    expect(selected[1].style.backgroundColor).toEqual('');
  });

  test('change colour on a cell with colour but then cancel', () => {
    tableDesign.getSelectedElements = jest.fn().mockImplementation(() => [selected[2]]);
    tableDesign.getPreviousElementColorStyles = jest
      .fn()
      .mockImplementation(() => ['rgb(223, 58, 58)']);
    tableDesign.handleTableBackgroundColorChange('rgb(255, 255, 0)', element, 'change');
    expect(selected[2].style.backgroundColor).toEqual('rgb(255, 255, 0)');
    tableDesign.handleTableBackgroundColorChange('rgb(255, 255, 0)', element, 'cancel');
    expect(selected[2].style.backgroundColor).toEqual('rgb(223, 58, 58)');
  });

  test('change colour on a row with a mix of cells with colour and without and then hit apply', () => {
    tableDesign.getSelectedElements = jest
      .fn()
      .mockImplementation(() => [selected[4], selected[5]]);
    tableDesign.handleTableBackgroundColorChange('rgb(214, 214, 0)', element, 'change');
    expect(selected[4].style.backgroundColor).toEqual('rgb(214, 214, 0)');
    expect(selected[5].style.backgroundColor).toEqual('rgb(214, 214, 0)');
    tableDesign.handleTableBackgroundColorChange('rgb(214, 214, 0)', element, 'save');
    tableDesign.storePreviousElementColorStyles([selected[4], selected[5]]);
    jest.spyOn(tableDesign, 'getPreviousElementColorStyles');
    expect(selected[4].style.backgroundColor).toEqual('rgb(214, 214, 0)');
    expect(selected[5].style.backgroundColor).toEqual('rgb(214, 214, 0)');
  });

  test('change colour on a entire row with a mix of cells with colour and without and then hit cancel', () => {
    tableDesign.getSelectedElements = jest
      .fn()
      .mockImplementation(() => [selected[6], selected[7]]);
    tableDesign.getPreviousElementColorStyles = jest
      .fn()
      .mockImplementation(() => ['', 'rgb(165, 76, 76)']);
    tableDesign.handleTableBackgroundColorChange('rgb(214, 214, 0)', element, 'change');
    expect(selected[6].style.backgroundColor).toEqual('rgb(214, 214, 0)');
    expect(selected[7].style.backgroundColor).toEqual('rgb(214, 214, 0)');
    tableDesign.handleTableBackgroundColorChange('rgb(214, 214, 0)', element, 'cancel');
    expect(selected[6].style.backgroundColor).toEqual('');
    expect(selected[7].style.backgroundColor).toEqual('rgb(165, 76, 76)');
  });

  test('change colour on a entire row without colour and then hit cancel', () => {
    tableDesign.getSelectedElements = jest
      .fn()
      .mockImplementation(() => [selected[8], selected[9]]);
    tableDesign.getPreviousElementColorStyles = jest.fn().mockImplementation(() => ['', '']);
    tableDesign.handleTableBackgroundColorChange('rgb(0, 0, 0)', element, 'change');
    expect(selected[8].style.backgroundColor).toEqual('rgb(0, 0, 0)');
    expect(selected[9].style.backgroundColor).toEqual('rgb(0, 0, 0)');
    tableDesign.handleTableBackgroundColorChange('rgb(0, 0, 0)', element, 'cancel');
    expect(selected[8].style.backgroundColor).toEqual('');
    expect(selected[9].style.backgroundColor).toEqual('');
  });

  test('change colour on a column with a mix of cells with colour and without and then hit apply', () => {
    tableDesign.getSelectedElements = jest
      .fn()
      .mockImplementation(() => [selected[3], selected[4]]);
    tableDesign.handleTableBackgroundColorChange('rgb(0, 0, 0)', element, 'change');
    expect(selected[3].style.backgroundColor).toEqual('rgb(0, 0, 0)');
    expect(selected[4].style.backgroundColor).toEqual('rgb(0, 0, 0)');
    tableDesign.handleTableBackgroundColorChange('rgb(0, 0, 0)', element, 'save');
    tableDesign.storePreviousElementColorStyles([selected[3], selected[4]]);
    jest.spyOn(tableDesign, 'getPreviousElementColorStyles');
    expect(selected[3].style.backgroundColor).toEqual('rgb(0, 0, 0)');
    expect(selected[4].style.backgroundColor).toEqual('rgb(0, 0, 0)');
  });
});
describe('#disableTableMargin for the TD selected tags', () => {
  const doc: HTMLElement = document.body;
  const table = document.createElement('table');
  doc.append(table);

  afterEach(() => {
    document.body.innerHTML = '';
  });

  test('tableMarginSpaceWrapper should not contain d-none while selecedElement contain TD tag', () => {
    const wrapper = document.createElement('div');
    wrapper.className = 'tableMarginSpaceWrapper';
    const tableDesign = new TableDesign();
    tableDesign.getSelectedElement = jest.fn().mockImplementation(() => wrapper);
    const attachSpaceOptionWrapper = tableDesign.attachSpaceOption(doc);
    const actaulResult =
      attachSpaceOptionWrapper.getElementsByClassName('tableMarginSpaceWrapper')[0].className;
    const expectedResult = 'tableMarginSpaceWrapper';
    expect(actaulResult).toBe(expectedResult);
  });

  test('tableMarginSpaceWrapper should contain d-none while selecedElement not contain TD tag', () => {
    const wrapper = document.createElement('TD');
    wrapper.className = 'tableMarginSpaceWrapper';
    const tableDesign = new TableDesign();
    const attachSpaceOptionWrapper = tableDesign.attachSpaceOption(doc);
    tableDesign.getSelectedElement = jest.fn().mockImplementation(() => wrapper);
    const actaulResult =
      attachSpaceOptionWrapper.getElementsByClassName('tableMarginSpaceWrapper')[0].className;
    const expectedResult = 'tableMarginSpaceWrapper d-none';
    expect(actaulResult).toBe(expectedResult);
  });

  test('tableMarginSpaceWrapper should contain d-none while selecedElement not contain TH tag', () => {
    const wrapper = document.createElement('TH');
    wrapper.className = 'tableMarginSpaceWrapper';
    const tableDesign = new TableDesign();
    const attachSpaceOptionWrapper = tableDesign.attachSpaceOption(doc);
    tableDesign.getSelectedElement = jest.fn().mockImplementation(() => wrapper);
    const actaulResult =
      attachSpaceOptionWrapper.getElementsByClassName('tableMarginSpaceWrapper')[0].className;
    const expectedResult = 'tableMarginSpaceWrapper d-none';
    expect(actaulResult).toBe(expectedResult);
  });
});

describe('table design', () => {
  const alignmentButtons = [
    { id: CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT, style: `margin-right: auto` },
    {
      id: CONSTANTS.LOREE_TABLE_DESIGN_CENTER_ALIGNMENT,
      style: `margin-right: auto; margin-left: auto`,
    },
    { id: CONSTANTS.LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT, style: `margin-left: auto` },
  ];
  beforeAll(() => {
    document.body.innerHTML = `<div id=${CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER}></div>`;
  });
  test('renders table design section', () => {
    tableDesign.initiate(templateConfig);
    expect(getElementById(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION)).toBeInTheDOM();
  });
  test('selects table element should show table design section', () => {
    expect(getElementById(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION).style.display).toEqual(
      'none',
    );
    baseInstance.changeSelectedElement(table);
    expect(getElementById(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION).style.display).toEqual(
      'block',
    );
    expect(
      getElementById(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION).childNodes[1].childNodes.length,
    ).toEqual(8);
  });
  test('change table border color', () => {
    tableDesign.getSelectedElements = jest.fn().mockImplementation(() => [tableWrapper]);
    tableDesign.handleTableBorderColorChange('#000d9c', table);
    expect(table.style.borderColor).toEqual('#000d9c');
  });
  test('change no of rows for table', () => {
    const noOfRowsInput = getEditorElementsByClassName('tableRowOptionWrapper')[0]
      .childNodes[1] as HTMLInputElement;
    fireEvent.change(noOfRowsInput, { target: { value: 3 } });
    expect(table.getElementsByTagName('TR').length).toEqual(3);
  });
  test('change no of columns for table', () => {
    const noOfColsInput = getEditorElementsByClassName('tableColumnOptionWrapper')[0]
      .childNodes[1] as HTMLInputElement;
    fireEvent.change(noOfColsInput, { target: { value: 3 } });
    expect(table.getElementsByTagName('TD').length).toEqual(9);
  });
  test('change table width', () => {
    const widthInput = getEditorElementsByClassName('widthOption')[0]
      .childNodes[1] as HTMLInputElement;
    fireEvent.change(widthInput, { target: { value: 70 } });
    expect(table.style.width).toEqual('70%');
  });
  test('change left margin', () => {
    const marginLeftInput = getEditorElementsByClassName('leftOptionMargin')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(marginLeftInput, { target: { value: 20 } });
    expect(tableWrapper.style.marginLeft).toEqual('20px');
  });
  test('change top margin', () => {
    const marginTopInput = getEditorElementsByClassName('topOptionMargin')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(marginTopInput, { target: { value: 10 } });
    expect(tableWrapper.style.marginTop).toEqual('10px');
  });
  test('change right margin', () => {
    const marginRightInput = getEditorElementsByClassName('rightOptionMargin')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(marginRightInput, { target: { value: 25 } });
    expect(tableWrapper.style.marginRight).toEqual('25px');
  });
  test('change bottom margin', () => {
    const marginBottomInput = getEditorElementsByClassName('bottomOptionMargin')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(marginBottomInput, { target: { value: 15 } });
    expect(tableWrapper.style.marginBottom).toEqual('15px');
  });
  test('change padding left', () => {
    const paddingLeftInput = getEditorElementsByClassName('leftOptionPadding')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(paddingLeftInput, { target: { value: 10 } });
    expect(tableWrapper.style.paddingLeft).toEqual('10px');
  });
  test('change top padding', () => {
    const paddingTopInput = getEditorElementsByClassName('topOptionPadding')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(paddingTopInput, { target: { value: 30 } });
    expect(tableWrapper.style.paddingTop).toEqual('30px');
  });
  test('change right padding', () => {
    const paddingRightInput = getEditorElementsByClassName('rightOptionPadding')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(paddingRightInput, { target: { value: 20 } });
    expect(tableWrapper.style.paddingRight).toEqual('20px');
  });
  test('change bottom padding', () => {
    const paddingBottomInput = getEditorElementsByClassName('bottomOptionPadding')[0]
      .childNodes[0] as HTMLInputElement;
    fireEvent.change(paddingBottomInput, { target: { value: 25 } });
    expect(tableWrapper.style.paddingBottom).toEqual('25px');
  });
  test.each(alignmentButtons)('change alignment of table', (alignment) => {
    const alignmentOption = getElementById(alignment.id);
    alignmentOption.click();
    expect(table.getAttribute('style')?.includes(alignment.style)).toBeTruthy();
  });
  test('selects table data element should show table data design section', () => {
    baseInstance.changeSelectedElement(tdElement);
    expect(getElementById(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION).style.display).toEqual(
      'block',
    );
    expect(
      getElementById(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION).childNodes[1].childNodes.length,
    ).toEqual(4);
  });
});

describe('#BorderDesignSection', () => {
  let borderColorPicker: HTMLElement;
  let selectedElement: Element;
  beforeEach(() => {
    document.body.innerHTML = tableDesignBorderSectionMock;
    document.body.innerHTML += tableMockData;
    selectedElement = getEditorElementsByClassName('loree-iframe-content-table')[0];
    tableDesign.getSelectedElement = jest
      .fn()
      .mockImplementation(() => selectedElement.parentElement);
    tableDesign.getSelectedElements = jest
      .fn()
      .mockImplementation(() => [selectedElement.parentElement]);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('border clear picker renders', () => {
    tableDesign.getBorderColorOfTable = jest.fn().mockImplementation(() => '#a5a5a5');
    const btn = getEditorElementsByClassName('color')[0] as HTMLButtonElement;
    tableDesign.attachBorderColorPicker(btn, selectedElement as HTMLTableElement);
    btn.click();
    expect(
      getEditorElementsByClassName('loree-table-design-color-pickers visible')[0],
    ).toBeInTheDocument();
  });
  test('clear button exist in border color picker', () => {
    tableDesign.getBorderColorOfTable = jest.fn().mockImplementation(() => '#a5a5a5');
    const btn = getEditorElementsByClassName('color')[0] as HTMLButtonElement;
    tableDesign.attachBorderColorPicker(btn, selectedElement as HTMLTableElement);
    btn.click();
    borderColorPicker = getEditorElementsByClassName(
      'loree-table-design-color-pickers visible',
    )[0] as HTMLElement;
    expect(getElementByClassName(borderColorPicker, 'pcr-clear')).toBeInTheDocument();
  });
  test('clear function reset table to default values', () => {
    const selectedTable = selectedElement as HTMLTableElement;
    selectedTable.style.setProperty('border-width', '11px');
    selectedTable.style.setProperty('border-style', 'Double');
    expect(selectedTable.style.borderWidth).toBe('11px');
    const clearBtn = getElementByClassName(borderColorPicker, 'pcr-clear');
    clearBtn.dispatchEvent(new Event('click'));
    tableDesign.clearBorderStyles(selectedTable);
    expect(selectedTable.style.borderWidth).toBe('1px');
    expect(selectedTable.style.borderColor).toBe('#a5a5a5');
  });
  test('verify border color save function', async () => {
    tableDesign.saveBorderStyles('#000d9c', selectedElement.parentElement as HTMLElement);
    const selectedTable = selectedElement as HTMLElement;
    expect(selectedTable.style.borderColor).toBe('#000d9c');
  });
});
describe('BackgroundDesignSection', () => {
  let backgroundColorPicker: HTMLElement;
  let selectedElement: Element;
  afterEach(() => {
    jest.clearAllMocks();
  });
  beforeEach(() => {
    document.body.innerHTML = tableDesignBorderSectionMock;
    document.body.innerHTML += tableMockData;
    selectedElement = getEditorElementsByClassName('loree-iframe-content-table')[0];
    tableDesign.getSelectedElement = jest
      .fn()
      .mockImplementation(() => selectedElement.parentElement);
    tableDesign.getSelectedElements = jest
      .fn()
      .mockImplementation(() => [selectedElement.parentElement]);
  });
  test('background clear picker renders', () => {
    tableDesign.fetchColorPickerBtnColor = jest.fn().mockImplementation(() => '#ffffff');
    const btn = getEditorElementsByClassName('color')[0] as HTMLButtonElement;
    tableDesign.attachBackgroundColorPicker(btn);
    btn.click();
    expect(
      getEditorElementsByClassName('loree-table-design-color-pickers visible')[0],
    ).toBeInTheDocument();
  });
  test('clear button exist in background color picker', () => {
    tableDesign.fetchColorPickerBtnColor = jest.fn().mockImplementation(() => '#ffffff');
    const btn = getEditorElementsByClassName('color')[0] as HTMLButtonElement;
    tableDesign.attachBackgroundColorPicker(btn);
    btn.click();
    backgroundColorPicker = getEditorElementsByClassName(
      'loree-table-design-color-pickers visible',
    )[0] as HTMLElement;
    expect(getElementByClassName(backgroundColorPicker, 'pcr-clear')).toBeInTheDocument();
  });
});
