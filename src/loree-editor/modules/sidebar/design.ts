import Pickr from '@simonwep/pickr';
import CONSTANTS from '../../constant';
import { editOptionsIcon } from '../../iconHolder';
import ICONS from './icons';
import Base from '../../base';
import { ImageModal } from '../imageModal/imageModal';
import { VideoModal } from '../videoModal/videoModal';
import { LmsAccess } from '../../../utils/lmsAccess';
import {
  quickLinkBlock,
  d2lQuickLinkBlock,
  handleBbQuickLinkBlock,
} from '../quickLinks/quickLinkUI';
import { ConfigInterface, FeatureInterface } from '../../interface';
import { createDiv, createElement, getElementById, getInputElementById } from '../../common/dom';
import { getEditorElementById, getElementByClassName, getElementByTagName } from '../../utils';
import { translate } from '../../../i18n/translate';

let scaleValueX: number;
let scaleValueY: number;
let selectedElement: HTMLElement | null = null;
const horizontalTransform = `<svg xmlns="http://www.w3.org/2000/svg" width="19.021" height="17.002" viewBox="0 0 19.021 17.002">
<path id="left_right" d="M9.242,16.736v-.76A.261.261,0,0,1,9.5,15.71a.251.251,0,0,1,.182.079.262.262,0,0,1,.08.187v.76A.267.267,0,0,1,9.5,17,.264.264,0,0,1,9.242,16.736Zm9.346.212-7.793-5.832a.267.267,0,0,1,0-.428l7.793-5.836a.257.257,0,0,1,.271-.021.264.264,0,0,1,.133.175l.029-.021v12l-.09-.066a.254.254,0,0,1-.072.058.257.257,0,0,1-.117.029A.251.251,0,0,1,18.588,16.948ZM.141,16.973A.265.265,0,0,1,0,16.736V5.067a.265.265,0,0,1,.141-.237.26.26,0,0,1,.273.021l7.793,5.836a.27.27,0,0,1,0,.428L.414,16.948A.251.251,0,0,1,.26,17,.264.264,0,0,1,.141,16.973Zm.379-.76L7.613,10.9.52,5.591Zm8.723-1.782V12.881a.264.264,0,0,1,.258-.266.267.267,0,0,1,.262.266v1.549A.267.267,0,0,1,9.5,14.7.264.264,0,0,1,9.242,14.431Zm0-3.095V9.786A.261.261,0,0,1,9.5,9.52a.251.251,0,0,1,.182.079.262.262,0,0,1,.08.187v1.549A.264.264,0,0,1,9.5,11.6.26.26,0,0,1,9.242,11.336Zm0-3.1V6.692A.274.274,0,0,1,9.316,6.5.261.261,0,0,1,9.5,6.426a.247.247,0,0,1,.182.075.271.271,0,0,1,.08.191V8.237A.264.264,0,0,1,9.5,8.5.261.261,0,0,1,9.242,8.237Zm0-3.095v-.76A.262.262,0,0,1,9.5,4.116a.251.251,0,0,1,.182.079.262.262,0,0,1,.08.187v.76a.267.267,0,0,1-.262.266A.264.264,0,0,1,9.242,5.142Zm6,.037L12.611,4.041a.26.26,0,0,1-.158-.216.268.268,0,0,1,.107-.245l.563-.411A5.339,5.339,0,0,0,5.1,4.05a.255.255,0,0,1-.168.108.243.243,0,0,1-.193-.046L3.775,3.4a.275.275,0,0,1-.1-.174.267.267,0,0,1,.045-.2,7.029,7.029,0,0,1,10.818-.9l.66-.482a.253.253,0,0,1,.27-.021.265.265,0,0,1,.141.237v3.07a.274.274,0,0,1-.115.22.249.249,0,0,1-.145.045A.276.276,0,0,1,15.244,5.18ZM13.713,3.011a.274.274,0,0,1,.074.208.264.264,0,0,1-.107.2l-.441.32,1.85.8V2.38l-.434.32a.256.256,0,0,1-.34-.037,6.51,6.51,0,0,0-10.02.465l.545.4a5.885,5.885,0,0,1,4.318-2.3c.115-.007.229-.01.342-.01A5.858,5.858,0,0,1,13.713,3.011Z"/>
</svg>`;
const verticalTransform = `<svg xmlns="http://www.w3.org/2000/svg" width="15.002" height="17.023" viewBox="0 0 15.002 17.023">
<path id="top_bottom" d="M8.27,14.767V14.1a.235.235,0,0,1,.066-.165.243.243,0,0,1,.164-.07.239.239,0,0,1,.234.235v.671a.232.232,0,1,1-.465,0Zm8.365.187L9.66,9.808a.231.231,0,0,1,0-.378l6.975-5.15A.23.23,0,0,1,17,4.416l.027-.019V14.984l-.08-.058a.24.24,0,0,1-.064.051.233.233,0,0,1-.105.026A.23.23,0,0,1,16.635,14.954Zm-16.51.022A.233.233,0,0,1,0,14.767V4.471a.233.233,0,0,1,.125-.209.238.238,0,0,1,.246.018l6.973,5.15a.237.237,0,0,1,0,.378L.371,14.954A.23.23,0,0,1,.232,15,.241.241,0,0,1,.125,14.976Zm.34-.671L6.813,9.618.465,4.933Zm7.8-1.572V11.366a.232.232,0,1,1,.465,0v1.367a.232.232,0,1,1-.465,0ZM8.27,10V8.635a.235.235,0,0,1,.066-.165A.243.243,0,0,1,8.5,8.4a.239.239,0,0,1,.234.235V10a.232.232,0,0,1-.465,0Zm0-2.734V5.9a.243.243,0,0,1,.066-.169A.239.239,0,0,1,8.5,5.67a.226.226,0,0,1,.164.066.239.239,0,0,1,.07.169V7.268a.232.232,0,1,1-.465,0Zm0-2.731V3.866A.234.234,0,0,1,8.336,3.7a.243.243,0,0,1,.164-.07.239.239,0,0,1,.234.235v.671a.232.232,0,1,1-.465,0Zm5.373.033-2.355-1a.233.233,0,0,1-.143-.191.238.238,0,0,1,.1-.216l.5-.363a4.823,4.823,0,0,0-7.18.777.226.226,0,0,1-.15.1.217.217,0,0,1-.172-.04L3.379,3a.249.249,0,0,1-.094-.154.235.235,0,0,1,.041-.176,6.346,6.346,0,0,1,9.682-.792l.592-.425a.228.228,0,0,1,.24-.018.231.231,0,0,1,.127.209V4.354a.242.242,0,0,1-.1.194.226.226,0,0,1-.129.04A.255.255,0,0,1,13.643,4.57ZM12.273,2.657a.247.247,0,0,1,.066.183.234.234,0,0,1-.1.172l-.395.282L13.5,4V2.1l-.389.282a.232.232,0,0,1-.3-.033,5.879,5.879,0,0,0-8.967.411l.486.352A5.292,5.292,0,0,1,8.2,1.081q.155-.009.3-.009A5.287,5.287,0,0,1,12.273,2.657Z" transform="translate(15.002) rotate(90)"/>
</svg>`;

// Color picker instances
let imageBorderColorPicker: Pickr | null = null;
let imageBackgroundColorPicker: Pickr | null = null;
let lineBorderColorPicker: Pickr | null = null;
const borderOption = [
  {
    label: translate('design.none'),
    value: 'none',
  },
  {
    label: translate('design.dashed'),
    value: 'dashed',
  },
  {
    label: translate('design.dotted'),
    value: 'dotted',
  },
  {
    label: translate('design.double'),
    value: 'double',
  },
  {
    label: translate('design.solid'),
    value: 'solid',
  },
];
interface COLORPICKERINTERFACE extends Pickr.Options {
  i18n: {
    'btn:save': string;
    'btn:cancel': string;
  };
}

const customColors: _Any = [];
let defaultBackgroundColor: string;
let defaultBorderColor: string;
let defaultLineColor: string;

export default class Design {
  initiate = (config: ConfigInterface): void => {
    this.appendCustomStyles(config);
    const sidebarContentWrapper = document.getElementById(CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER);
    if (!sidebarContentWrapper) return;
    const designSection = document.createElement('div');
    designSection.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION;
    designSection.className = 'section';
    designSection.style.display = 'none';
    this.attachLabel(designSection);
    sidebarContentWrapper.appendChild(designSection);
  };

  appendCustomStyles = (config: _Any) => {
    const colors = config.customColor?.colors;
    if (colors?.length) {
      colors.forEach((clr: { color: string }) => {
        customColors.push(clr.color);
      });
    }
  };

  getSelectedElement = (): HTMLElement | null => selectedElement;

  baseClass = (): _Any => new Base();

  imageClass = (): ImageModal => new ImageModal();

  videoClass = (): VideoModal => new VideoModal();

  attachLabel = (designSection: HTMLElement): void => {
    const sectionLabel = document.createElement('button');
    sectionLabel.className = 'sectionLabel ';
    sectionLabel.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_BUTTON;
    sectionLabel.dataset.toggle = 'collapse';
    sectionLabel.dataset.target = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_COLLAPSE}`;
    sectionLabel.setAttribute('aria-expanded', 'true');
    sectionLabel.innerHTML = `
      <span class="label">${translate('global.design')}</span>
      <span class="image">
        <svg viewBox="0 0 8 14">
          <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
        </svg>
      </span>
    `;
    designSection.appendChild(sectionLabel);
    designSection.appendChild(this.designSectionWrapper());
  };

  designSectionWrapper = (): HTMLElement => {
    const sectionContent = document.createElement('div');
    sectionContent.className = 'sectionContent collapse show';
    sectionContent.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_COLLAPSE;
    sectionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER}`;
    sectionContent.innerHTML = '';
    const sidebarDesign = document.createElement('div');
    sidebarDesign.className = 'sidebarDesign';
    const designEditOptions = document.createElement('div');
    designEditOptions.className = 'accordion';
    designEditOptions.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION;
    const elementDetails = document.createElement('div');
    elementDetails.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS;
    sidebarDesign.appendChild(elementDetails);
    sidebarDesign.appendChild(designEditOptions);
    sectionContent.appendChild(sidebarDesign);
    return sectionContent;
  };

  accordionIconClick = (e: Event) => {
    const accordionIcons = document.getElementsByClassName('design-accordion');
    for (const icon of accordionIcons) {
      icon.innerHTML = icon.innerHTML.replace('I', '+');
      icon.classList.remove('accordion-min-icon-position');
    }
    const targetElementParent = e.target as HTMLElement;
    let targetElement = targetElementParent.parentElement as HTMLElement;
    if (targetElement?.children[0].tagName === 'DIV') {
      targetElement = targetElement.childNodes[0] as HTMLElement;
    }
    if (targetElement) {
      setTimeout(() => {
        const validateVal = targetElement?.getAttribute('aria-expanded');
        const childNode = targetElement?.children[0];
        if (validateVal === 'false' || validateVal === null) {
          childNode.innerHTML = childNode?.innerHTML.replace('I', '+');
          childNode.classList.remove('accordion-min-icon-position');
        } else if (validateVal === 'true') {
          targetElement.children[0].innerHTML = childNode?.innerHTML.replace('+', 'I');
          childNode.classList.add('accordion-min-icon-position');
        }
      });
    }
  };

  attachDesignContent = (
    elementType: string,
    element: HTMLElement,
    featuresList: FeatureInterface | null | undefined,
  ): void => {
    selectedElement = element;
    const elementDetails = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS);
    const designEditOptions = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION);
    if (!designEditOptions || !elementDetails) return;
    designEditOptions.innerHTML = '';
    elementDetails.innerHTML = '';
    switch (elementType) {
      case 'image':
        if (featuresList?.imagedesign) this.attachImageDesign(designEditOptions);
        break;
      case 'specialElement':
        if (featuresList?.imagedesign) this.attachSpecialElementDesign(designEditOptions);
        break;
      case 'video':
        if (featuresList?.videodesign) this.attachVideoDesign(designEditOptions);
        break;
      case 'line':
        if (featuresList?.dividerdesign) this.attachLineDesign(designEditOptions);
        break;
      case 'space':
        if (featuresList?.dividerdesign) this.attachSpaceDesign(designEditOptions);
        break;
      case 'element':
        if (featuresList?.fontstyles) this.attachElementDesign(designEditOptions);
        break;
    }
  };

  applyStyleToSelectedImageVideoElement = (property: string, value: string): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    let element = null;
    if (
      selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
      selectedElement.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)
    ) {
      element = selectedElement.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE)
        ? selectedElement.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE)[0]
        : selectedElement.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)[0];
    }
    if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)) {
      element = selectedElement.getElementsByClassName(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO)[0];
    }
    if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
      element = selectedElement;
    }
    if (element) {
      const styleVal = element as HTMLElement;
      const appliedStyle = styleVal.style;
      appliedStyle.setProperty(property, value);
    }
  };

  attachImageDesign = (designEditOptions: HTMLElement) => {
    const lms = new LmsAccess();
    const elementDetails = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS,
    );
    if (!elementDetails) return;
    elementDetails.appendChild(this.imageDetailsSection());
    designEditOptions.appendChild(this.sizeBlock());
    designEditOptions.appendChild(this.spaceBlock());
    designEditOptions.appendChild(this.alignmentBlock());
    if (!lms.getAccess()) designEditOptions.appendChild(this.transformBlock());
    designEditOptions.appendChild(this.backgroundBlock());
    designEditOptions.appendChild(this.borderBlock());
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'canvas') {
      designEditOptions.appendChild(quickLinkBlock('image'));
    }
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'D2l') {
      designEditOptions.appendChild(d2lQuickLinkBlock('image'));
    }
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'BB') {
      designEditOptions.appendChild(handleBbQuickLinkBlock('image'));
    }
  };

  attachSpecialElementDesign = (designEditOptions: HTMLElement) => {
    const lms = new LmsAccess();
    const elementDetails = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS);
    if (!elementDetails) return;
    elementDetails.appendChild(this.imageDetailsSection());
    designEditOptions.appendChild(this.sizeBlock());
    designEditOptions.appendChild(this.spaceBlock());
    if (!lms.getAccess()) designEditOptions.appendChild(this.transformBlock());
    designEditOptions.appendChild(this.backgroundBlock());
    designEditOptions.appendChild(this.borderBlock());
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'canvas') {
      designEditOptions.appendChild(quickLinkBlock('image'));
    }
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'D2l') {
      designEditOptions.appendChild(d2lQuickLinkBlock('image'));
    }
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'BB') {
      designEditOptions.appendChild(handleBbQuickLinkBlock('image'));
    }
  };

  attachVideoDesign = (designEditOptions: HTMLElement): void => {
    const elementDetails = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS,
    );
    if (!elementDetails) return;
    elementDetails.appendChild(this.videoDetailsSection());
    designEditOptions.appendChild(this.videoControl());
    designEditOptions.appendChild(this.sizeBlock());
    designEditOptions.appendChild(this.spaceBlock());
    designEditOptions.appendChild(this.alignmentBlock());
    designEditOptions.appendChild(this.backgroundBlock());
    designEditOptions.appendChild(this.borderBlock());
  };

  attachLineDesign = (designEditOptions: HTMLElement): void => {
    designEditOptions.appendChild(this.sizeBlock());
    designEditOptions.appendChild(this.spaceBlock());
    designEditOptions.appendChild(this.alignmentBlock());
    designEditOptions.appendChild(this.lineStyleBlock());
    designEditOptions.appendChild(this.backgroundBlock());
  };

  attachSpaceDesign = (designEditOptions: HTMLElement): void => {
    designEditOptions.appendChild(this.sizeBlock());
    designEditOptions.appendChild(this.spaceBlock());
    designEditOptions.appendChild(this.backgroundBlock());
  };

  attachElementDesign = (designEditOptions: HTMLElement) => {
    let targetElement = selectedElement?.parentElement;
    while (targetElement && targetElement.tagName !== 'TABLE' && targetElement.tagName !== 'BODY') {
      targetElement = targetElement.parentElement;
    }
    if (targetElement && targetElement.tagName === 'TABLE' && targetElement.parentElement)
      this.baseClass().showHamburgerIconForTable(targetElement.parentElement);
    const lms = new LmsAccess();
    designEditOptions.appendChild(this.spaceBlock());
    designEditOptions.appendChild(this.backgroundBlock());
    designEditOptions.appendChild(this.borderBlock());
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'canvas') {
      designEditOptions.appendChild(quickLinkBlock('font'));
    }
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'D2l') {
      designEditOptions.appendChild(d2lQuickLinkBlock('font'));
    }
    if (lms.getAccess() && sessionStorage.getItem('domainName') === 'BB') {
      designEditOptions.appendChild(handleBbQuickLinkBlock('font'));
    }
  };

  imageDetailsSection = (): HTMLElement => {
    const selectedImageElement = this.getSelectedElement();
    const imageDetails = createDiv('div');
    imageDetails.id = 'loree-sidebar-image-details';
    const thumbnailHeader = createElement('label');
    thumbnailHeader.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_VIDEO_THUMBNAIL_HEADER;
    thumbnailHeader.setAttribute('for', CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_SELECTED_IMAGE_NAME);
    thumbnailHeader.innerHTML = `${translate('design.alttext')}`;
    imageDetails.appendChild(thumbnailHeader);
    imageDetails.appendChild(this.attachElementName());
    // For image preview
    const imgWrapper = createDiv('div');
    imgWrapper.className = 'design-section-img-wrap';
    const imgPreview = createDiv('div');
    imgPreview.id = CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_PREVIEW_WRAPPER;
    imgWrapper.appendChild(imgPreview);
    imgWrapper.appendChild(this.replaceImageButton());
    selectedImageElement?.id === CONSTANTS.LOREE_BANNER_IMAGE_IDENTITY &&
      imgWrapper.appendChild(this.editButton());
    imageDetails.appendChild(imgWrapper);
    return imageDetails;
  };

  videoDetailsSection = (): HTMLElement => {
    const videoDetails = document.createElement('div');
    videoDetails.id = 'loree-sidebar-video-details';
    // replace video button
    const videoWrapper = document.createElement('div');
    videoWrapper.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_VIDEO_REPLACE_BUTTON_WRAPPER;
    videoDetails.appendChild(this.replaceVideoButton());
    videoDetails.appendChild(this.attachElementName());
    const selectedElement = this.getSelectedElement();
    const selectedVideo = selectedElement?.getElementsByTagName('video')[0];
    if (selectedVideo) {
      // thumbnail header
      const thumbnailHeader = document.createElement('div');
      thumbnailHeader.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_VIDEO_THUMBNAIL_HEADER;
      thumbnailHeader.innerHTML = `${translate('global.thumbnailimage')}`;
      videoDetails.appendChild(thumbnailHeader);
      // For video thumbnai preview
      const imgWrapper = document.createElement('div');
      imgWrapper.className = 'design-section-img-wrap';
      const imgPreview = document.createElement('div');
      imgPreview.id = CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_PREVIEW_WRAPPER;
      imgWrapper.appendChild(imgPreview);
      imgWrapper.appendChild(this.replaceVideoThumbnailButton(selectedVideo));
      imgWrapper.appendChild(this.removeVideoThumbnailButton(selectedVideo));
      videoDetails.appendChild(imgWrapper);
    }
    return videoDetails;
  };

  attachElementName = (): HTMLElement => {
    const inputElement = document.createElement('input');
    inputElement.type = 'text';
    inputElement.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_SELECTED_IMAGE_NAME;
    inputElement.className = 'design-section-image-name-block';
    inputElement.onblur = (e: FocusEvent): void => this.altTextOnChangeHandler(e);
    inputElement.autocomplete = 'off';
    return inputElement;
  };

  altTextOnChangeHandler = (e: Event): void => {
    const target = e.target as HTMLInputElement | null;
    const selectedElement = this.getSelectedElement();
    const selectedImage = selectedElement?.getElementsByTagName('img')[0];
    const selectedVideo = selectedElement?.getElementsByTagName('video')[0];
    const selectedIframe = selectedElement?.getElementsByTagName('iframe')[0];
    if (selectedImage) {
      selectedImage.alt = target?.value as string;
      selectedImage.removeAttribute('title');
    } else if (selectedVideo) {
      selectedVideo.title = target?.value as string;
    } else if (selectedIframe) {
      selectedIframe.title = target?.value as string;
    }
  };

  replaceVideoButton = (): HTMLElement => {
    const replaceButton = document.createElement('button');
    replaceButton.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_VIDEO_REPLACE_BUTTON;
    replaceButton.className = 'replace-btn';
    replaceButton.innerHTML = `${translate('global.replace')}`;
    replaceButton.onclick = (): void => this.videoClass().replaceVideoUploadHandler();
    return replaceButton;
  };

  replaceImageButton = (): HTMLElement => {
    const replaceButton = document.createElement('button');
    replaceButton.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_REPLACE_BUTTON;
    replaceButton.className = 'design-image-btn';
    replaceButton.innerHTML = `${translate('global.replace')}`;
    replaceButton.onclick = (): void => this.imageClass().replaceImageUploadHandler();
    return replaceButton;
  };

  replaceVideoThumbnailButton = (selectedVideo: HTMLElement): HTMLElement => {
    const replaceButton = document.createElement('button');
    replaceButton.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_REPLACE_BUTTON;
    replaceButton.className = 'design-image-btn';
    replaceButton.innerHTML = `${translate('global.update')}`;
    if (!selectedVideo?.hasAttribute('poster')) {
      replaceButton.innerHTML = `${translate('design.addimage')}`;
    }
    replaceButton.onclick = (): void => this.imageClass().replaceImageUploadHandler();
    return replaceButton;
  };

  removeVideoThumbnailButton = (selectedVideo: HTMLElement): HTMLElement => {
    const removeButton = document.createElement('button');
    removeButton.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_REMOVE_BUTTON;
    removeButton.className = 'design-image-btn';
    removeButton.innerHTML = `${translate('global.remove')}`;
    removeButton.onclick = (): void => this.removeThumbnailImage(removeButton);
    if (!selectedVideo?.hasAttribute('poster')) {
      removeButton.style.display = 'none';
    }
    return removeButton;
  };

  removeThumbnailImage = (removeButton: HTMLButtonElement): void => {
    const selectedElement = this.getSelectedElement();
    const image = document.getElementById(CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_VIEW);
    const imageReplaceButton = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_DESIGN_REPLACE_BUTTON,
    ) as HTMLButtonElement;
    image?.remove();
    if (selectedElement?.getElementsByTagName('video')[0].hasAttribute('poster')) {
      selectedElement?.getElementsByTagName('video')[0].removeAttribute('poster');
      const video = selectedElement?.getElementsByTagName('video')[0];
      video.load();
    }
    removeButton.style.display = 'none';
    if (imageReplaceButton) imageReplaceButton.innerHTML = `${translate('design.addimage')}`;
  };

  editButton = (): HTMLElement => {
    const editButton = document.createElement('button');
    editButton.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_EDIT_BUTTON;
    editButton.className = 'design-image-btn';
    editButton.innerHTML = `${translate('global.edit')}`;
    editButton.onclick = (): void => this.imageClass().handleImageEditButtonClick();
    return editButton;
  };

  /* Requirement change so removed shape option, once shape option is want,
     need to append this line
     designEditOptions.appendChild(this.shapeBlock());  in attachContent function before space block append.
    */
  // Shape block
  shapeBlock = (): HTMLElement => {
    const shapeContent = document.createElement('div');
    shapeContent.id = CONSTANTS.LOREE_DESIGN_SECTION_SHAPE_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_SHAPE_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_SHAPE_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('design.shape')}</span>`;
    shapeContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse display-Flex';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_SHAPE_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    accordionContentDiv.appendChild(this.squareShape()); // square shape
    accordionContentDiv.appendChild(this.circleShape()); // circle shape
    accordionContentDiv.appendChild(this.triangleShape()); // triangle shape
    shapeContent.appendChild(accordionContentDiv);
    return shapeContent;
  };

  squareShape = (): HTMLElement => {
    const squareShape = document.createElement('button');
    squareShape.className = 'square shapeBlockSpace';
    squareShape.id = CONSTANTS.LOREE_DESIGN_SECTION_SQUARE_SHAPE_BLOCK;
    return squareShape;
  };

  circleShape = (): HTMLElement => {
    const circleShape = document.createElement('button');
    circleShape.className = 'circle shapeBlockSpace';
    circleShape.id = CONSTANTS.LOREE_DESIGN_SECTION_CIRCLE_SHAPE_BLOCK;
    return circleShape;
  };

  triangleShape = (): HTMLElement => {
    const triangleShape = document.createElement('div');
    triangleShape.className = 'triangle shapeBlockSpace';
    triangleShape.id = CONSTANTS.LOREE_DESIGN_SECTION_TRIANGLE_SHAPE_BLOCK;
    const innerDiv = document.createElement('div');
    innerDiv.className = 'empty';
    triangleShape.appendChild(innerDiv);
    return triangleShape;
  };

  // video control
  videoControl = (): HTMLElement => {
    const videoControl = document.createElement('div');
    videoControl.id = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('design.videocontrol')}</span>`;
    videoControl.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    accordionContentDiv.appendChild(this.autoPlay());
    accordionContentDiv.appendChild(this.loopVideo());
    accordionContentDiv.appendChild(this.allowFullScreen());
    videoControl.appendChild(accordionContentDiv);
    return videoControl;
  };

  autoPlay = (): HTMLElement => {
    const autoPlayDiv = document.createElement('div');
    autoPlayDiv.className = 'form-check marginTopBottom-2';
    const autoPlayInput = document.createElement('input');
    autoPlayInput.type = 'checkbox';
    autoPlayInput.className = 'form-check-input';
    autoPlayInput.id = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_AUTO_PLAY;
    autoPlayInput.onchange = (): void => this.autoPlayOnChange();
    const autoPlayLabel = document.createElement('label');
    autoPlayLabel.className = 'form-check-label';
    autoPlayLabel.htmlFor = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_AUTO_PLAY;
    autoPlayLabel.innerText = translate('design.autoplay');
    autoPlayDiv.appendChild(autoPlayInput);
    autoPlayDiv.appendChild(autoPlayLabel);
    return autoPlayDiv;
  };

  autoPlayOnChange = (): void => {
    const checkBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_AUTO_PLAY,
    );
    const selectedElement = this.getSelectedElement() as HTMLElement;
    const iframeTag = selectedElement.getElementsByTagName('iframe')[0];
    const videoTag = selectedElement.getElementsByTagName('video')[0];
    const regFlag = 'g';
    const regExp1 = new RegExp('\\?', regFlag);
    const regExp2 = new RegExp('\\?autoplay', regFlag);
    if ((checkBoxVal as HTMLInputElement)?.checked) {
      if (videoTag) {
        videoTag.autoplay = true;
        videoTag.load();
      } else {
        const appendAutoPlay = iframeTag.src.search(regExp1) > 0 ? '&autoplay=1' : '?autoplay=1';
        iframeTag.src += appendAutoPlay;
      }
    } else {
      if (videoTag) {
        videoTag.autoplay = false;
        videoTag.load();
      } else {
        const removeAutoPlay = iframeTag.src.search(regExp2) > 0;
        if (removeAutoPlay) {
          const replaceText = iframeTag.src.replace('?autoplay=1', '');
          iframeTag.src = replaceText.replace('&', '?');
        } else {
          iframeTag.src = iframeTag.src.replace('&autoplay=1', '');
        }
      }
    }
    this.baseClass().handleSelectedContentChanges();
  };

  loopVideo = (): HTMLElement => {
    const loopVideoDiv = document.createElement('div');
    loopVideoDiv.className = 'form-check marginTopBottom-2';
    const loopVideoInput = document.createElement('input');
    loopVideoInput.type = 'checkbox';
    loopVideoInput.className = 'form-check-input';
    loopVideoInput.id = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_LOOP_VIDEO;
    loopVideoInput.onchange = (): void => this.loopVideoOnChange();
    const loopVideoLabel = document.createElement('label');
    loopVideoLabel.className = 'form-check-label';
    loopVideoLabel.htmlFor = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_LOOP_VIDEO;
    loopVideoLabel.innerText = `${translate('design.loopvideo')}`;
    loopVideoDiv.appendChild(loopVideoInput);
    loopVideoDiv.appendChild(loopVideoLabel);
    return loopVideoDiv;
  };

  loopVideoOnChange = (): void => {
    const checkBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_LOOP_VIDEO,
    );
    const selectedElement = this.getSelectedElement() as HTMLElement;
    const iframeTag = selectedElement.getElementsByTagName('iframe')[0];
    const videoTag = selectedElement.getElementsByTagName('video')[0];
    const regFlag = 'g';
    const regExp1 = new RegExp('\\?', regFlag);
    const regExp2 = new RegExp('\\&', regFlag);
    if ((checkBoxVal as HTMLInputElement).checked) {
      if (videoTag) {
        videoTag.loop = true;
        videoTag.load();
      } else if (iframeTag.src.search('youtube.com') > 0) {
        const id = this.videoClass().getYouTubeVideoID(iframeTag.src);
        if (iframeTag.src.search(regExp1) > 0) {
          const replaceText = iframeTag.src.slice(0, -11);
          iframeTag.src = replaceText + '?playlist=' + id + '&loop=1&autoplay=1';
        } else {
          iframeTag.src = iframeTag.src + '?playlist=' + id + '&loop=1';
        }
      } else {
        const replaceText = iframeTag.src.search(regExp1) > 0 ? '&loop=1' : '?loop=1';
        iframeTag.src += replaceText;
      }
    } else {
      if (videoTag) {
        videoTag.loop = false;
        videoTag.load();
      } else if (iframeTag.src.search('youtube.com') > 0) {
        const id = this.videoClass().getYouTubeVideoID(iframeTag.src);
        const replaceText = iframeTag.src.replace('?playlist=' + id + '&loop=1', '');
        iframeTag.src =
          replaceText.search(regExp2) > 0 ? replaceText.replace('&', '?') : replaceText;
      } else {
        if (iframeTag.src.search('&loop=1') > 0) {
          iframeTag.src = iframeTag.src.replace('&loop=1', '');
        } else {
          const replaceText = iframeTag.src.replace('?loop=1', '');
          iframeTag.src =
            replaceText.search(regExp2) > 0 ? replaceText.replace('&', '?') : replaceText;
        }
      }
    }
    this.baseClass().handleSelectedContentChanges();
  };

  allowFullScreen = (): HTMLElement => {
    const allowFullScreenDiv = document.createElement('div');
    allowFullScreenDiv.className = 'form-check marginTopBottom-2';
    const allowFullScreenInput = document.createElement('input');
    allowFullScreenInput.type = 'checkbox';
    allowFullScreenInput.className = 'form-check-input';
    allowFullScreenInput.id = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_ALLOW_FULL_SCREEN;
    allowFullScreenInput.onchange = (): void => this.allowFullScreenOnChange();
    const allowFullScreenLabel = document.createElement('label');
    allowFullScreenLabel.className = 'form-check-label';
    allowFullScreenLabel.htmlFor = CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_ALLOW_FULL_SCREEN;
    allowFullScreenLabel.innerText = `${translate('design.allowfullscreen')}`;
    allowFullScreenDiv.appendChild(allowFullScreenInput);
    allowFullScreenDiv.appendChild(allowFullScreenLabel);
    return allowFullScreenDiv;
  };

  allowFullScreenOnChange = (): void => {
    const checkBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_VIDEO_CONTROL_ALLOW_FULL_SCREEN,
    );
    const selectedElement = this.getSelectedElement() as HTMLElement;
    const iframeTag = selectedElement.getElementsByTagName('iframe')[0];
    const toReload = selectedElement.getElementsByTagName('iframe')[0];
    const videoTag = selectedElement.getElementsByTagName('video')[0];
    if (videoTag) {
      if ((checkBoxVal as HTMLInputElement).checked) {
        videoTag.classList.add('allow-fullscreen');
        videoTag.load();
      } else {
        videoTag.classList.remove('allow-fullscreen');
        videoTag.load();
      }
    } else {
      if ((checkBoxVal as HTMLInputElement).checked) {
        iframeTag.removeAttribute('donotallowfullscreen');
        iframeTag.setAttribute('allowfullscreen', '');
        iframeTag.src = toReload.src;
      } else {
        iframeTag.removeAttribute('allowfullscreen');
        iframeTag.setAttribute('donotallowfullscreen', '');
        iframeTag.src = toReload.src;
      }
    }
    this.baseClass().handleSelectedContentChanges();
  };

  // size block
  sizeBlock = (): HTMLElement => {
    const sizeContent = document.createElement('div');
    sizeContent.className = '';
    sizeContent.id = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_SIZE_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('global.size')}</span>`;
    sizeContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    accordionContentDiv.appendChild(this.fullWidth());
    accordionContentDiv.appendChild(this.widthInput());
    accordionContentDiv.appendChild(this.heightInput());
    sizeContent.appendChild(accordionContentDiv);
    return sizeContent;
  };

  fullWidth = (): HTMLElement => {
    const fullWidthDiv = document.createElement('div');
    fullWidthDiv.className = 'form-check marginTopBottom-2 mt-0';
    const fullWidthInput = document.createElement('input');
    fullWidthInput.type = 'checkbox';
    fullWidthInput.className = 'form-check-input';
    fullWidthInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH;
    fullWidthInput.onchange = (): void => this.fullWidthOnChange();
    const labelFullWidth = document.createElement('label');
    labelFullWidth.className = 'form-check-label';
    labelFullWidth.htmlFor = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH;
    labelFullWidth.innerText = `${translate('design.fullwidth')}`;
    fullWidthDiv.appendChild(fullWidthInput);
    fullWidthDiv.appendChild(labelFullWidth);
    return fullWidthDiv;
  };

  updatePopperPosition = (): void => {
    const popper = this.baseClass().getBlockOptionPopper();
    if (popper) popper.update();
  };

  fullWidthOnChange = (): void => {
    const checkBoxVal: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH,
    );
    const widthInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT);
    const selectedElement: HTMLElement | null = this.getSelectedElement();
    if (!selectedElement) return;
    if ((checkBoxVal as HTMLInputElement).checked) {
      this.setElementWidth(selectedElement);
    } else {
      widthInput.disabled = false;
    }
  };

  setElementWidth = (selectedElement: HTMLElement): void => {
    const widthInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT);
    const isImageSelected = getElementByTagName(selectedElement, 'img');
    const isLineSelected = getElementByTagName(selectedElement, 'hr');
    const isIframeSelected = getElementByTagName(selectedElement, 'iframe');
    const isVideoTag = getElementByTagName(selectedElement, 'video');
    const isSpaceSelected = getElementByClassName(
      selectedElement,
      CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER,
    );
    let elementValue: HTMLElement;

    if (isImageSelected) {
      elementValue = isImageSelected;
      this.setWidthAndHeightForElements(elementValue);
    } else if (isLineSelected) {
      elementValue = isLineSelected;
      this.setWidthForDividerLine(elementValue);
    } else if (isSpaceSelected) {
      elementValue = isSpaceSelected;
      this.setWidthAndHeightForElements(elementValue);
    } else if (isIframeSelected) {
      elementValue = getElementByClassName(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO);
      isIframeSelected.style.width = '100%';
      this.setWidthAndHeightForElements(elementValue);
    } else if (isVideoTag) {
      elementValue = getElementByClassName(selectedElement, CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO);
      isVideoTag.style.width = '100%';
      this.setWidthAndHeightForElements(elementValue);
    }
    widthInput.disabled = true;
  };

  setWidthForDividerLine = (elementValue: HTMLElement): void => {
    const widthInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT);
    const heightInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT);
    elementValue.style.width = '100%';
    const widthValue = this.getComputedValue(elementValue, 'width');
    const heightValue = this.getComputedValue(elementValue, 'border-block-start-width');
    widthInput.value = widthValue;
    heightInput.value = heightValue;
    this.updateWidthChanges();
  };

  // for elements with height: auto
  setWidthAndHeightForElements = (elementValue: HTMLElement): void => {
    const widthInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT);
    const heightInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT);
    elementValue.style.width = '100%';
    const widthValue = this.getComputedValue(elementValue, 'width');
    const heightValue = this.getComputedValue(elementValue, 'height');
    widthInput.value = widthValue;
    heightInput.value = heightValue;
    this.updateWidthChanges();
  };

  getComputedValue = (elementValue: HTMLElement, property: string): string => {
    const value = window
      .getComputedStyle(elementValue)
      .getPropertyValue(property)
      .replace('px', '');
    return value;
  };

  updateWidthChanges = (): void => {
    this.baseClass().handleSelectedContentChanges();
    this.updatePopperPosition();
  };

  widthInput = (): HTMLElement => {
    const widthInputDiv = document.createElement('div');
    const widthLabel = document.createElement('label');
    widthLabel.className = 'design-section-size-label';
    widthLabel.innerText = translate('design.width');
    const widthInputBox = document.createElement('input');
    widthInputBox.type = 'number';
    widthInputBox.className = 'form-control-design marginLeft';
    widthInputBox.id = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT;
    widthInputBox.name = 'widthInput';
    widthInputBox.onchange = (e): void => this.widthOnChange(e);
    widthInputBox.min = '0';
    const widthMetrics = document.createElement('span');
    widthMetrics.className = 'pl-2';
    widthMetrics.innerHTML = `<i>${translate('design.pixelsuffix')}</i>`;
    widthInputDiv.appendChild(widthLabel);
    widthInputDiv.appendChild(widthInputBox);
    widthInputDiv.appendChild(widthMetrics);
    return widthInputDiv;
  };

  widthOnChange = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      const selectedElement = this.getSelectedElement();
      if (selectedElement) {
        const isImageSelected = selectedElement.getElementsByTagName('img')[0];
        const isLineSelected = selectedElement.getElementsByTagName('hr')[0];
        const isIframeSelected = selectedElement.getElementsByTagName('iframe')[0];
        const isVideoTag = selectedElement.getElementsByTagName('video')[0];
        const isSpaceSelected = selectedElement.getElementsByClassName(
          CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER,
        )[0] as HTMLDivElement;
        let elementValue;
        if (isImageSelected) {
          elementValue = isImageSelected;
        } else if (isLineSelected) {
          elementValue = isLineSelected;
          if (elementValue) elementValue.style.width = value + 'px';
          return;
        } else if (isSpaceSelected) {
          elementValue = isSpaceSelected;
          if (elementValue) elementValue.style.width = value + 'px';
          return;
        } else {
          elementValue = getElementByClassName(
            selectedElement,
            CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO,
          );
        }
        if (elementValue) {
          elementValue.style.width = value + 'px';
          if (isVideoTag || isImageSelected) {
            if (isIframeSelected) isIframeSelected.style.width = value + 'px';
            if (isVideoTag) isVideoTag.style.width = value + 'px';
          }
        }
        const selectedElementHeight = this.getComputedValue(elementValue, 'height');
        const heightInput = getInputElementById(CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT);
        heightInput.value = selectedElementHeight;
      }
    }
    this.updateWidthChanges();
  };

  lockInputSize = (): HTMLElement => {
    const lockInput = document.createElement('div');
    lockInput.className = 'lock-design-section-sizeInput';
    lockInput.innerHTML = `${editOptionsIcon[0]}`;
    return lockInput;
  };

  heightInput = (): HTMLElement => {
    const selectedElement = this.getSelectedElement();
    const isImageSelected = selectedElement?.getElementsByTagName('img')[0];
    const isVideoSelected = selectedElement?.getElementsByTagName('video')[0];
    const isIframeSelected = selectedElement?.getElementsByTagName('iframe')[0];
    const heightInputDiv = document.createElement('div');
    heightInputDiv.className = 'loree-disabled-block';
    const heightLabel = document.createElement('label');
    heightLabel.className = 'design-section-size-label mb-0';
    heightLabel.innerText = `${translate('design.height')}`;
    const heightInputBox = document.createElement('input');
    heightInputBox.type =
      isImageSelected || isVideoSelected || isIframeSelected ? 'text' : 'number';
    heightInputBox.className = 'form-control-design marginLeft';
    heightInputBox.id = CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT;
    heightInputBox.name = 'heightInput';
    heightInputBox.readOnly = !!(isImageSelected || isVideoSelected || isIframeSelected);
    heightInputBox.style.background = isImageSelected ? 'transparent' : '';
    heightInputBox.onchange = (e): void => this.heightOnChange(e);
    heightInputBox.min = '0';
    heightInputDiv.appendChild(heightLabel);
    heightInputDiv.appendChild(heightInputBox);
    return heightInputDiv;
  };

  heightOnChange = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      const selectedElement = this.getSelectedElement();
      if (selectedElement) {
        const isImageSelected = selectedElement.getElementsByTagName('img')[0];
        const isLineSelected = selectedElement.getElementsByTagName('hr')[0];
        const isVideoSelected = selectedElement.getElementsByTagName('video')[0];
        const isIframeSelected = selectedElement.getElementsByTagName('iframe')[0];
        const isSpaceSelected = selectedElement.getElementsByClassName(
          CONSTANTS.LOREE_IFRAME_CONTENT_DIVIDER,
        )[0] as HTMLDivElement;
        let elementValue;
        let elementParent;
        if (isImageSelected) {
          elementValue = isImageSelected;
          elementValue.style.height = value + 'px';
        } else if (isLineSelected) {
          elementValue = isLineSelected;
          elementValue.style.borderTopWidth = value + 'px';
        } else if (isVideoSelected || isIframeSelected) {
          elementValue = selectedElement.childNodes[0].childNodes[0].childNodes[0] as HTMLElement;
          elementParent = selectedElement.childNodes[0].childNodes[0] as HTMLElement;
          elementValue.style.height = value + 'px';
        } else {
          elementValue = isSpaceSelected;
          elementValue.style.height = value + 'px';
        }
        if (elementValue) {
          if ((isVideoSelected || isIframeSelected) && elementParent) {
            elementParent.style.paddingTop = value + 'px';
          }
        }
      }
    }
    this.baseClass().handleSelectedContentChanges();
  };

  // border block
  borderBlock = (): HTMLElement => {
    const borderContent = document.createElement('div');
    borderContent.className = '';
    borderContent.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('global.border')}</span>`;
    borderContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    accordionContentDiv.appendChild(this.borderWidth());
    accordionContentDiv.appendChild(this.borderDropDown());
    borderContent.appendChild(accordionContentDiv);
    return borderContent;
  };

  borderWidth = (): HTMLElement => {
    const borderWidthInputDiv = document.createElement('div');
    borderWidthInputDiv.className = 'design-section-image-border-option';
    const widthLabel = document.createElement('label');
    widthLabel.innerText = `${translate('design.widthpixel')}`;
    const widthInputBox = document.createElement('input');
    widthInputBox.type = 'number';
    widthInputBox.className = 'form-control-design';
    widthInputBox.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_WIDTH_INPUT;
    widthInputBox.name = 'widthInput';
    widthInputBox.min = '0';
    widthInputBox.oninput = (e): void => this.handleImgBorderWidth(e);
    const colorPicker = document.createElement('button');
    colorPicker.id = CONSTANTS.LOREE_DESIGN_SECTION_IMAGE_BORDER_COLOR_PICKER;
    colorPicker.className = 'imageBorderColorPicker';
    colorPicker.style.backgroundColor = this.getImageVideoElementBorderColor();
    const invalidChars = ['-', '+', 'e'];
    widthInputBox.addEventListener('keydown', function (e) {
      if (invalidChars.includes(e.key)) {
        e.preventDefault();
      }
    });
    borderWidthInputDiv.appendChild(widthLabel);
    borderWidthInputDiv.appendChild(widthInputBox);
    borderWidthInputDiv.appendChild(colorPicker);
    this.attachImageBorderColorPicker(colorPicker);
    this.fixColorPickerBehaviour();
    return borderWidthInputDiv;
  };

  handleDesignBorderCancel = (defaultBorderColor: string, element: HTMLElement) => {
    if (defaultBorderColor) {
      this.handleImageBorderColorChange(defaultBorderColor, element);
    } else {
      this.handleImageBorderColorClear('', element);
    }
  };

  // Image border color picker
  attachImageBorderColorPicker = (element: HTMLElement): void => {
    defaultBorderColor = this.getImageVideoElementBorderColor();
    if (element) {
      const options: COLORPICKERINTERFACE = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: `${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS} border-picker`,
        default: defaultBorderColor,
        defaultRepresentation: 'HEXA',
        lockOpacity: true,
        useAsButton: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            rgba: false,
            hex: true,
            input: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      imageBorderColorPicker = Pickr.create(options);
      imageBorderColorPicker.on('change', (color: _Any) => {
        const hexValue = color.toHEXA();
        this.handleImageBorderColorChange(hexValue, element);
      });

      imageBorderColorPicker.on('save', (color: _Any) => {
        if (color) {
          const hexValue = color.toHEXA();
          this.handleImageBorderColorChange(hexValue, element);
          imageBorderColorPicker?.hide();
          this.baseClass().handleSelectedContentChanges();
          defaultBorderColor = hexValue;
        }
      });

      imageBorderColorPicker.on('clear', () => {
        this.clearBorderStyles(element);
      });

      imageBorderColorPicker.on('cancel', () => {
        this.handleDesignBorderCancel(defaultBorderColor, element);
        imageBorderColorPicker?.hide();
      });

      imageBorderColorPicker.on('hide', () => {
        void Promise.resolve().then(() =>
          this.handleDesignBorderCancel(defaultBorderColor, element),
        );
      });

      imageBorderColorPicker.on('show', () => {
        imageBorderColorPicker?.setColor(defaultBorderColor, true);
      });
    }
  };

  clearBorderStyles = (element: HTMLElement) => {
    this.handleImageBorderColorClear('', element);
    const borderWidthInput = getEditorElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_BORDER_WIDTH_INPUT,
    ) as HTMLInputElement;
    const borderStyle = getEditorElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_BORDER_STYLE_INPUT,
    ) as HTMLSelectElement;
    defaultBorderColor = '#000000';
    borderWidthInput.value = '0';
    borderStyle.selectedIndex = borderOption.findIndex((option) => option.value === 'solid');
    this.applyStyleToSelectedImageVideoElement('border-width', '0' + 'px');
    this.applyBorderStyleToSelectedElememt('border-style', 'Solid');
    imageBorderColorPicker?.hide();
  };

  handleImageBorderColorChange = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedImageVideoElement('border-color', color.toString());
    element.style.backgroundColor = color;
  };

  handleImageBorderColorClear = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedImageVideoElement('border-color', '#FFFFFF');
    element.style.backgroundColor = '#FFFFFF';
  };

  getImageVideoElementBorderColor = (): string => {
    let borderColor = '#000000';
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_TEXT)
      ) {
        const element = selectedElement.getElementsByClassName(
          CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE,
        )[0] as HTMLElement;
        if (element?.style?.borderColor !== '') borderColor = element.style.borderColor;
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO_WRAPPER)) {
        const element = selectedElement.getElementsByClassName(
          CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO,
        )[0] as HTMLElement;
        if (element?.style?.borderColor !== '') borderColor = element.style.borderColor;
      }
      if (selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT)) {
        const element = selectedElement;
        if (element.style.borderColor !== '' && element.style.borderWidth !== '0px')
          borderColor = element.style.borderColor;
      }
    }
    return borderColor;
  };

  fixColorPickerBehaviour = (): void => {
    const pcrSelections = document.querySelectorAll('.pcr-selection');
    const pcrResult = document.querySelectorAll('.pcr-result');
    pcrSelections.forEach((selection) => {
      selection.addEventListener('click', () => {
        pcrResult.forEach((input) => {
          const inputField = input as HTMLInputElement;
          inputField.blur();
        });
      });
    });
  };
  // Image border color picker

  handleImgBorderWidth = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.applyStyleToSelectedImageVideoElement('border-width', value + 'px');
      this.baseClass().handleSelectedContentChanges();
    }
  };

  borderDropDown = (): HTMLElement => {
    const borderDropDown = document.createElement('select');
    borderDropDown.className = 'border-dropdown';
    borderDropDown.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_STYLE_INPUT;
    borderOption.forEach((option) => {
      const options = document.createElement('option');
      options.value = option.value;
      options.innerHTML = option.label;
      borderDropDown.appendChild(options);
    });
    borderDropDown.onclick = (): void => this.baseClass().hidePopperInstances();
    borderDropDown.onchange = (event): void => this.handleBorderStyle(event);
    return borderDropDown;
  };

  handleBorderStyle = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.applyBorderStyleToSelectedElememt('border-style', value);
    }
  };

  applyBorderStyleToSelectedElememt = (property: string, value: string): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      const isImageSelected = selectedElement.getElementsByTagName('img')[0];
      const isLineSelected = selectedElement.getElementsByTagName('hr')[0];
      const isElementSelected = selectedElement.classList.contains('loree-iframe-content-element');
      const isVideoSelected =
        selectedElement.getElementsByTagName('video')[0] ||
        selectedElement.getElementsByTagName('iframe')[0];
      let elementValue;
      if (isImageSelected) {
        elementValue = isImageSelected;
      } else if (isLineSelected) {
        elementValue = isLineSelected;
      } else if (isElementSelected) {
        elementValue = selectedElement;
      } else if (isVideoSelected) {
        elementValue = selectedElement.childNodes[0];
      } else {
        elementValue = selectedElement.childNodes[0].childNodes[0].childNodes[0];
      }
      if (property && value) {
        const styleVal = elementValue as HTMLElement;
        const appliedStyle = styleVal.style;
        this.appliedStylePropertyBorderColor(property, appliedStyle);
        this.appliedStylePropertyBorderWidth(property, appliedStyle);
        appliedStyle.setProperty(property, value);
      }
      this.baseClass().handleSelectedContentChanges();
    }
  };

  appliedStylePropertyBorderColor = (property: string, appliedStyle: CSSStyleDeclaration) => {
    if (property === 'border-style' && appliedStyle?.borderColor?.length === 0) {
      appliedStyle.setProperty('border-color', CONSTANTS.Default_AppliedStyleProperty_BorderColor);
    }
    return appliedStyle;
  };

  appliedStylePropertyBorderWidth = (property: string, appliedStyle: CSSStyleDeclaration) => {
    if (property === 'border-style' && appliedStyle?.borderWidth?.length === 0) {
      appliedStyle.setProperty('border-width', CONSTANTS.Default_AppliedStyleProperty_BorderWidth);
    }
    return appliedStyle;
  };

  // line style block
  lineStyleBlock = (): HTMLElement => {
    const lineContent = document.createElement('div');
    lineContent.className = '';
    lineContent.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('design.linestyle')}</span>`;
    lineContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    accordionContentDiv.appendChild(this.lineStyle());
    accordionContentDiv.appendChild(this.borderDropDown());
    lineContent.appendChild(accordionContentDiv);
    return lineContent;
  };

  lineStyle = (): HTMLElement => {
    const lineColorInputDiv = document.createElement('div');
    lineColorInputDiv.className = 'design-section-line-border-option';
    const colorLabel = document.createElement('label');
    colorLabel.innerText = `${translate('global.color')}`;
    const colorPicker = document.createElement('button');
    colorPicker.className = 'lineColorPicker';
    colorPicker.id = 'loree-design-section-line-color-picker';
    colorPicker.style.backgroundColor = this.getLineBorderColor();
    lineColorInputDiv.appendChild(colorLabel);
    lineColorInputDiv.appendChild(colorPicker);
    this.attachLineBorderColorPicker(colorPicker);
    this.fixColorPickerBehaviour();
    return lineColorInputDiv;
  };

  getLineBorderColor = (): string => {
    let color = '#000000';
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      const hrElement = selectedElement.getElementsByTagName('hr')[0] as HTMLElement;
      if (hrElement) {
        if (getComputedStyle(hrElement).borderColor !== '')
          color = getComputedStyle(hrElement).borderColor;
      }
    }
    return color;
  };

  attachLineBorderColorPicker = (element: HTMLElement): void => {
    defaultLineColor = this.getLineBorderColor();
    if (element) {
      const options: COLORPICKERINTERFACE = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: `${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS} line-picker`,
        default: defaultLineColor,
        defaultRepresentation: 'HEXA',
        lockOpacity: true,
        useAsButton: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            rgba: false,
            hex: true,
            input: true,
            clear: false,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      lineBorderColorPicker = Pickr.create(options);
      lineBorderColorPicker.on('change', (color: _Any) => {
        const hexValue = color.toHEXA();
        this.handleLineBorderColorChange(hexValue, element);
      });
      lineBorderColorPicker.on('save', (color: _Any) => {
        const hexValue = color.toHEXA();
        this.handleLineBorderColorChange(hexValue, element);
        defaultLineColor = hexValue;
        lineBorderColorPicker?.hide();
        this.baseClass().handleSelectedContentChanges();
      });
      lineBorderColorPicker.on('cancel', () => {
        this.handleLineBorderColorChange(defaultLineColor, element);
        lineBorderColorPicker?.hide();
      });
      lineBorderColorPicker.on('hide', () => {
        void Promise.resolve().then(() =>
          this.handleLineBorderColorChange(defaultLineColor, element),
        );
      });
      lineBorderColorPicker.on('show', () => {
        lineBorderColorPicker?.setColor(defaultLineColor, true);
      });
    }
  };

  handleLineBorderColorChange = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    const hrElement = selectedElement.getElementsByTagName('hr')[0] as HTMLElement;
    if (!hrElement) return;
    hrElement.style.borderColor = color.toString();
    element.style.backgroundColor = color;
  };

  backgroundStyle = (): HTMLElement => {
    const lineColorInputDiv = document.createElement('div');
    lineColorInputDiv.className = 'display-Flex';
    const colorLabel = document.createElement('label');
    colorLabel.className = 'display-inlineFlex';
    colorLabel.innerText = `${translate('design.background')}`;
    const colorPicker = document.createElement('div');
    colorPicker.id = CONSTANTS.LOREE_DESIGN_SECTION_BORDER_COLOR_PICKER;
    colorPicker.className = 'color-picker mt-1';
    lineColorInputDiv.appendChild(colorLabel);
    lineColorInputDiv.appendChild(colorPicker);
    return lineColorInputDiv;
  };

  backgroundBlock = (): HTMLElement => {
    const backgroundColorContent = document.createElement('div');
    backgroundColorContent.className = 'accordion';
    backgroundColorContent.id = CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('global.color')}</span>`;
    backgroundColorContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    accordionContentDiv.appendChild(this.backgroundColorBlock());
    backgroundColorContent.appendChild(accordionContentDiv);
    return backgroundColorContent;
  };

  // Image background color picker
  backgroundColorBlock = (): HTMLElement => {
    const backgroundColorOption = document.createElement('div');
    backgroundColorOption.className = 'design-section-image-background-option';
    const label = document.createElement('label');
    label.innerText = `${translate('design.background')}`;
    const colorPicker = document.createElement('button');
    colorPicker.id = 'imageBackgroundColorPicker';
    colorPicker.className = 'imageBackgroundColorPicker';
    const defaultColor = this.getImageBackgroundColor();
    colorPicker.style.backgroundColor = defaultColor || '#ffffff';
    backgroundColorOption.appendChild(label);
    backgroundColorOption.appendChild(colorPicker);
    this.attachImageBackgroundColorPicker(colorPicker);
    this.fixColorPickerBehaviour();
    return backgroundColorOption;
  };

  getImageBackgroundColor = (): string => {
    let color = '';
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      color = selectedElement.style.backgroundColor;
    }
    return color;
  };

  getDefaultBackGroundColor() {
    return defaultBackgroundColor;
  }

  getDefaultBorderColor() {
    return defaultBorderColor;
  }

  getDefaultLineColor() {
    return defaultLineColor;
  }

  attachImageBackgroundColorPicker = (element: HTMLElement): void => {
    defaultBackgroundColor = this.getImageBackgroundColor();
    if (element) {
      const options: COLORPICKERINTERFACE = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: `${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS} background-picker`,
        default: defaultBackgroundColor || '#ffffff',
        defaultRepresentation: 'HEXA',
        lockOpacity: true,
        useAsButton: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            rgba: false,
            hex: true,
            input: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };

      imageBackgroundColorPicker = Pickr.create(options);
      imageBackgroundColorPicker.on('change', (color: _Any) => {
        const hexValue = color.toHEXA();
        this.handleImageBackgroundColorChange(hexValue, element);
      });

      imageBackgroundColorPicker.on('clear', () => {
        this.handleImageBackgroundColorChange('', element);
        defaultBackgroundColor = '';
      });

      imageBackgroundColorPicker.on('save', (color: _Any) => {
        if (color) {
          const hexValue = color.toHEXA();
          this.handleImageBackgroundColorChange(hexValue, element);
          imageBackgroundColorPicker?.hide();
          defaultBackgroundColor = this.getImageBackgroundColor();
          this.baseClass().handleSelectedContentChanges();
        }
      });

      imageBackgroundColorPicker.on('cancel', () => {
        this.handleImageBackgroundColorChange(defaultBackgroundColor, element);
        imageBackgroundColorPicker?.hide();
      });

      imageBackgroundColorPicker.on('hide', () => {
        void Promise.resolve().then(() =>
          this.handleImageBackgroundColorChange(defaultBackgroundColor, element),
        );
      });

      imageBackgroundColorPicker.on('show', () => {
        imageBackgroundColorPicker?.setColor(defaultBackgroundColor, true);
      });
    }
  };

  handleImageBackgroundColorChange = (color: string, element: HTMLElement): void => {
    const selectedElement: HTMLElement | null = this.getSelectedElement();
    if (!selectedElement) return;
    if (color) {
      this.baseClass().applyStyleToSelectedElement('background-color', color.toString());
      element.style.backgroundColor = color;
    } else {
      selectedElement.style.backgroundColor = '';
      element.style.backgroundColor = '#ffffff';
    }
  };

  // Image background color picker

  alignmentBlock = (): HTMLElement => {
    const alignmentContent = document.createElement('div');
    alignmentContent.className = '';
    alignmentContent.id = CONSTANTS.LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('global.alignment')}</span>`;
    alignmentContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_ALIGNMENT_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    // First row content
    const accordionFirstRowInnerDiv = document.createElement('div');
    accordionFirstRowInnerDiv.className = 'display-Flex';
    accordionFirstRowInnerDiv.appendChild(this.leftAlignContent());
    accordionFirstRowInnerDiv.appendChild(this.centerAlignContent());
    accordionFirstRowInnerDiv.appendChild(this.rightAlignContent());
    accordionContentDiv.appendChild(accordionFirstRowInnerDiv);
    alignmentContent.appendChild(accordionContentDiv);
    return alignmentContent;
  };

  leftAlignContent = (): HTMLElement => {
    const leftAlign = document.createElement('div');
    leftAlign.className = 'design-section-alignment-button';
    leftAlign.id = CONSTANTS.LOREE_DESIGN_SECTION_LEFT_ALIGN_BUTTON;
    leftAlign.innerHTML = ICONS.LEFT_ALIGNMENT_ICON;
    leftAlign.onclick = (): void => this.applyAlignmentForImage('start');
    return leftAlign;
  };

  rightAlignContent = (): HTMLElement => {
    const rightAlign = document.createElement('div');
    rightAlign.className = 'design-section-alignment-button';
    rightAlign.id = CONSTANTS.LOREE_DESIGN_SECTION_RIGHT_ALIGN_BUTTON;
    rightAlign.innerHTML = ICONS.RIGHT_ALIGNMENT_ICON;
    rightAlign.onclick = (): void => this.applyAlignmentForImage('end');
    return rightAlign;
  };

  centerAlignContent = (): HTMLElement => {
    const centerAlign = document.createElement('div');
    centerAlign.className = 'design-section-alignment-button';
    centerAlign.id = CONSTANTS.LOREE_DESIGN_SECTION_CENTER_ALIGN_BUTTON;
    centerAlign.innerHTML = ICONS.CENTER_ALIGNMENT_ICON;
    centerAlign.onclick = (): void => this.applyAlignmentForImage('center');
    return centerAlign;
  };

  applyAlignmentForImage = (position: string): void => {
    const selectedItem = this.getSelectedElement();
    const isImageSelected = selectedItem?.getElementsByTagName('img')[0];
    const isLineSelected = selectedItem?.getElementsByTagName('hr')[0];
    const isTableSelected = selectedItem?.getElementsByTagName('table')[0];
    const alignCenter = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_CENTER_ALIGN_BUTTON,
    ) as HTMLElement;
    const alignLeft = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_LEFT_ALIGN_BUTTON,
    ) as HTMLElement;
    const alignRight = document.getElementById(
      CONSTANTS.LOREE_DESIGN_SECTION_RIGHT_ALIGN_BUTTON,
    ) as HTMLElement;
    const addStyle = selectedItem?.parentElement;
    const imageID = isImageSelected?.id;
    let elementValue;
    if (isImageSelected) {
      elementValue = isImageSelected;
    } else if (isLineSelected) {
      elementValue = isLineSelected;
    } else if (isTableSelected) {
      elementValue = isTableSelected;
    } else {
      elementValue = getElementByClassName(
        selectedItem as HTMLElement,
        CONSTANTS.LOREE_IFRAME_CONTENT_VIDEO,
      );
    }
    if (selectedItem && elementValue && addStyle) {
      switch (position) {
        case 'start':
          if (imageID === 'ImageWithText') {
            addStyle.style.textAlign = 'left';
            alignLeft.style.stroke = 'blue';
            alignRight.style.stroke = '';
            alignCenter.style.stroke = '';
            selectedItem.style.display = 'inline-flex';
          } else {
            selectedItem.style.display = '';
            selectedItem.style.textAlign = 'left';
            alignLeft.style.stroke = 'blue';
            alignRight.style.stroke = '';
            alignCenter.style.stroke = '';
          }
          break;
        case 'end':
          if (imageID === 'ImageWithText') {
            addStyle.style.textAlign = 'right';
            selectedItem.style.display = 'inline-flex';
            alignRight.style.stroke = 'blue';
            alignLeft.style.stroke = '';
            alignCenter.style.stroke = '';
          } else {
            selectedItem.style.display = '';
            selectedItem.style.textAlign = 'right';
            alignRight.style.stroke = 'blue';
            alignLeft.style.stroke = '';
            alignCenter.style.stroke = '';
          }
          break;
        case 'center':
          if (imageID === 'ImageWithText') {
            addStyle.style.textAlign = 'center';
            selectedItem.style.display = 'inline-flex';
            alignCenter.style.stroke = 'blue';
            alignLeft.style.stroke = '';
            alignRight.style.stroke = '';
          } else {
            selectedItem.style.display = '';
            selectedItem.style.textAlign = 'center';
            alignCenter.style.stroke = 'blue';
            alignLeft.style.stroke = '';
            alignRight.style.stroke = '';
          }
          break;
        case 'top':
          selectedItem.style.display = 'flex';
          selectedItem.style.alignItems = 'flex-start';
          elementValue.style.display = 'inline-flex';
          break;
        case 'bottom':
          selectedItem.style.display = 'flex';
          selectedItem.style.alignItems = 'flex-end';
          elementValue.style.display = 'inline-flex';
          break;
        case 'middle':
          selectedItem.style.display = 'flex';
          selectedItem.style.alignItems = 'center';
          elementValue.style.display = 'inline-flex';
          break;
      }
    }
    this.baseClass().handleSelectedContentChanges();
  };

  // Transform block
  transformBlock = (): HTMLElement => {
    const transformContent = document.createElement('div');
    transformContent.id = CONSTANTS.LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('design.transform')}</span>`;
    transformContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font ml-3';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_TRANSFORM_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    const accordionInnerDiv = document.createElement('div');
    accordionInnerDiv.className = 'display-Flex';
    accordionInnerDiv.appendChild(this.topTransform());
    accordionInnerDiv.appendChild(this.leftTransform());
    accordionContentDiv.appendChild(accordionInnerDiv);
    transformContent.appendChild(accordionContentDiv);
    return transformContent;
  };

  leftTransform = (): HTMLElement => {
    const leftTranformParentDiv = document.createElement('div');
    leftTranformParentDiv.style.flex = '0.3';
    const leftTranform = document.createElement('div');
    const leftTransformInput = document.createElement('button');
    leftTransformInput.style.border = 'transparent';
    leftTransformInput.innerHTML = horizontalTransform;
    leftTransformInput.value = 'true';
    leftTransformInput.id = CONSTANTS.LOREE_IMAGE_HORIZONTAL_TRANSFORM;
    leftTransformInput.onclick = (e): void => this.horizontalTransform(e);
    leftTranform.appendChild(leftTransformInput);
    leftTranformParentDiv.appendChild(leftTranform);
    return leftTranformParentDiv;
  };

  horizontalTransform = (event: Event): void => {
    const leftTransform = document.getElementById(
      CONSTANTS.LOREE_IMAGE_HORIZONTAL_TRANSFORM,
    ) as HTMLInputElement;
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const parentElement = target.parentElement as HTMLInputElement;
      if (parentElement) {
        if (parentElement.value === 'true') {
          if (leftTransform) {
            leftTransform.value = 'false';
            scaleValueX = -1;
          }
        } else {
          if (leftTransform) {
            leftTransform.value = 'true';
            scaleValueX = 1;
          }
        }
      }
    }
    leftTransform.style.transform = `scaleX(${scaleValueX})`;
    this.applyStyleToSelectedImageVideoElement('transform', `scaleX(${scaleValueX})`);
    this.baseClass().handleSelectedContentChanges();
  };

  topTransform = (): HTMLElement => {
    const topTranformParentDiv = document.createElement('div');
    topTranformParentDiv.style.flex = '0.3';
    const topTranform = document.createElement('div');
    const topTransformInput = document.createElement('button');
    topTransformInput.style.border = 'transparent';
    topTransformInput.innerHTML = verticalTransform;
    topTransformInput.id = CONSTANTS.LOREE_IMAGE_VERTICAL_TRANSFORM;
    topTransformInput.onclick = (e): void => this.verticalTransform(e);
    topTransformInput.value = 'true';
    topTranform.appendChild(topTransformInput);
    topTranformParentDiv.appendChild(topTranform);
    return topTranformParentDiv;
  };

  verticalTransform = (event: Event): void => {
    const topTransform = document.getElementById(
      CONSTANTS.LOREE_IMAGE_VERTICAL_TRANSFORM,
    ) as HTMLInputElement;
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const parentElement = target.parentElement as HTMLInputElement;
      if (parentElement.value === 'true') {
        if (topTransform) {
          topTransform.value = 'false';
          scaleValueY = -1;
        }
      } else {
        if (topTransform) {
          topTransform.value = 'true';
          scaleValueY = 1;
        }
      }
    }
    topTransform.style.transform = `scaleY(${scaleValueY})`;
    this.baseClass().applyStyleToSelectedElement('transform', `scaleY(${scaleValueY})`);
    this.baseClass().handleSelectedContentChanges();
  };

  spaceBlock = (): HTMLElement => {
    const spaceContent = document.createElement('div');
    spaceContent.className = '';
    spaceContent.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_WRAPPER;
    // accordion heading
    const accordionHeading = document.createElement('div');
    accordionHeading.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_HEADING;
    accordionHeading.className = 'accordion-sub-text design-section-font';
    accordionHeading.dataset.toggle = 'collapse';
    accordionHeading.dataset.target = `#${CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_COLLAPSE}`;
    accordionHeading.onclick = (e): void => this.accordionIconClick(e);
    accordionHeading.setAttribute('aria-labeledbody', 'true');
    accordionHeading.innerHTML = `<span class='accordion-icon design-accordion'>${translate(
      'design.plus',
    )}</span>
    <span>${translate('design.space')}</span>`;
    spaceContent.appendChild(accordionHeading);
    // accordion content
    const accordionContentDiv = document.createElement('div');
    accordionContentDiv.className = 'collapse fourSideMargin-2 design-section-font';
    accordionContentDiv.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_COLLAPSE;
    accordionContentDiv.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_EDIT_OPTION}`;
    // margin Block
    const marginBlock = document.createElement('div');
    const marginHeading = document.createElement('div');
    marginHeading.className = 'd-flex justify-content-between mb-2 editor-design-margin-heading';
    marginHeading.innerHTML = `<b>${translate('design.margin')}</b><span><i>${translate(
      'design.pixelsuffix',
    )}</i></span>`;
    const marginFirstRowInnerDiv = document.createElement('div');
    //  space-block
    marginFirstRowInnerDiv.className = 'display-Flex';
    marginFirstRowInnerDiv.appendChild(this.leftMarginBlock());
    marginFirstRowInnerDiv.appendChild(this.topMarginBlock());
    marginBlock.appendChild(marginHeading);
    marginBlock.appendChild(marginFirstRowInnerDiv);
    const marginSeconRowInnerDiv = document.createElement('div');
    marginSeconRowInnerDiv.className = 'display-Flex';
    marginSeconRowInnerDiv.appendChild(this.rightMarginBlock());
    marginSeconRowInnerDiv.appendChild(this.bottomMarginBlock());
    marginBlock.appendChild(marginSeconRowInnerDiv);

    // padding block
    const paddingBlock = document.createElement('div');
    const paddingHeading = document.createElement('div');
    paddingHeading.className = 'd-flex justify-content-between mb-2 editor-design-margin-heading';
    paddingHeading.innerHTML = `<b>${translate('design.padding')}</b><span><i>${translate(
      'design.pixelsuffix',
    )}</i></span>`;
    const paddingFirstRowInnerDiv = document.createElement('div');
    paddingFirstRowInnerDiv.className = 'display-Flex';
    paddingFirstRowInnerDiv.appendChild(this.leftSpaceBlock());
    paddingFirstRowInnerDiv.appendChild(this.topSpaceBlock());
    paddingBlock.appendChild(paddingHeading);
    paddingBlock.appendChild(paddingFirstRowInnerDiv);
    const paddingSeconRowInnerDiv = document.createElement('div');
    paddingSeconRowInnerDiv.className = 'display-Flex';
    paddingSeconRowInnerDiv.appendChild(this.rightSpaceBlock());
    paddingSeconRowInnerDiv.appendChild(this.bottomSpaceBlock());
    paddingBlock.appendChild(paddingSeconRowInnerDiv);

    accordionContentDiv.appendChild(marginBlock);
    accordionContentDiv.appendChild(paddingBlock);
    spaceContent.appendChild(accordionContentDiv);
    return spaceContent;
  };

  leftSpaceBlock = (): HTMLElement => {
    const leftSpaceBlockParentDiv = document.createElement('div');
    leftSpaceBlockParentDiv.style.flex = '1';
    const leftSpaceBlock = document.createElement('div');
    leftSpaceBlock.className = 'left-pad';
    const leftSpaceInput = document.createElement('input');
    leftSpaceInput.type = 'number';
    leftSpaceInput.name = 'left-padding-input';
    leftSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_LEFT_PADDING_INPUT;
    leftSpaceInput.className = 'form-control-design';
    leftSpaceInput.min = '0';
    leftSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    leftSpaceInput.oninput = (event): void => this.leftSpaceBlockInputVal(event);
    leftSpaceBlock.appendChild(leftSpaceInput);
    leftSpaceBlockParentDiv.appendChild(leftSpaceBlock);
    return leftSpaceBlockParentDiv;
  };

  leftSpaceBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('padding-left', value + 'px');
      this.baseClass().handleSelectedContentChanges();
    }
  };

  topSpaceBlock = (): HTMLElement => {
    const topSpaceBlockParentDiv = document.createElement('div');
    topSpaceBlockParentDiv.style.flex = '1';
    const topSpaceBlock = document.createElement('div');
    topSpaceBlock.className = 'top-pad';
    const topSpaceInput = document.createElement('input');
    topSpaceInput.type = 'number';
    topSpaceInput.name = 'top-padding-input';
    topSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_TOP_PADDING_INPUT;
    topSpaceInput.min = '0';
    topSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    topSpaceInput.className = 'form-control-design';
    topSpaceInput.oninput = (event): void => this.topSpaceBlockInputVal(event);
    topSpaceBlock.appendChild(topSpaceInput);
    topSpaceBlockParentDiv.appendChild(topSpaceBlock);
    return topSpaceBlockParentDiv;
  };

  topSpaceBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('padding-top', value + 'px');
      this.baseClass().handleSelectedContentChanges(); // end
    }
  };

  rightSpaceBlock = (): HTMLElement => {
    const rightSpaceBlockParentDiv = document.createElement('div');
    rightSpaceBlockParentDiv.style.flex = '1';
    const rightSpaceBlock = document.createElement('div');
    rightSpaceBlock.className = 'right-pad mb-0';
    const rightSpaceInput = document.createElement('input');
    rightSpaceInput.type = 'number';
    rightSpaceInput.name = 'right-padding-input';
    rightSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_RIGHT_PADDING_INPUT;
    rightSpaceInput.min = '0';
    rightSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    rightSpaceInput.className = 'form-control-design';
    rightSpaceInput.oninput = (event): void => this.rightSpaceBlockInputVal(event);
    rightSpaceBlock.appendChild(rightSpaceInput);
    rightSpaceBlockParentDiv.appendChild(rightSpaceBlock);
    return rightSpaceBlockParentDiv;
  };

  rightSpaceBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('padding-right', value + 'px');
      this.baseClass().handleSelectedContentChanges();
    }
  };

  bottomSpaceBlock = (): HTMLElement => {
    const bottomSpaceBlockParentDiv = document.createElement('div');
    bottomSpaceBlockParentDiv.style.flex = '1';
    const bottomSpaceBlock = document.createElement('div');
    bottomSpaceBlock.className = 'bottom-pad mb-0';
    const bottomSpaceInput = document.createElement('input');
    bottomSpaceInput.name = 'bottom-padding-input';
    bottomSpaceInput.type = 'number';
    bottomSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BOTTOM_PADDING_INPUT;
    bottomSpaceInput.min = '0';
    bottomSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    bottomSpaceInput.className = 'form-control-design';
    bottomSpaceInput.oninput = (event): void => this.bottomSpaceBlockInputVal(event);
    bottomSpaceBlock.appendChild(bottomSpaceInput);
    bottomSpaceBlockParentDiv.appendChild(bottomSpaceBlock);
    return bottomSpaceBlockParentDiv;
  };

  bottomSpaceBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('padding-bottom', value + 'px');
      this.baseClass().handleSelectedContentChanges();
    }
  };

  // Margin block
  leftMarginBlock = (): HTMLElement => {
    const leftSpaceBlockParentDiv = document.createElement('div');
    leftSpaceBlockParentDiv.style.flex = '1';
    const leftSpaceBlock = document.createElement('div');
    leftSpaceBlock.className = 'left-pad';
    const leftSpaceInput = document.createElement('input');
    leftSpaceInput.type = 'number';
    leftSpaceInput.name = 'left-margin-input';
    leftSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_LEFT_MARGIN_INPUT;
    leftSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    leftSpaceInput.className = 'form-control-design';
    leftSpaceInput.min = '0';
    leftSpaceInput.oninput = (event): void => this.leftMarginBlockInputVal(event);
    leftSpaceBlock.appendChild(leftSpaceInput);
    leftSpaceBlockParentDiv.appendChild(leftSpaceBlock);
    return leftSpaceBlockParentDiv;
  };

  leftMarginBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('margin-left', value + 'px');
      this.baseClass().updateLanguageIsoLabelPopperInstance();
      this.baseClass().handleSelectedContentChanges();
    }
  };

  topMarginBlock = (): HTMLElement => {
    const topSpaceBlockParentDiv = document.createElement('div');
    topSpaceBlockParentDiv.style.flex = '1';
    const topSpaceBlock = document.createElement('div');
    topSpaceBlock.className = 'top-pad';
    const topSpaceInput = document.createElement('input');
    topSpaceInput.type = 'number';
    topSpaceInput.name = 'top-margin-input';
    topSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_TOP_MARGIN_INPUT;
    topSpaceInput.min = '0';
    topSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    topSpaceInput.className = 'form-control-design';
    topSpaceInput.oninput = (event): void => this.topMarginBlockInputVal(event);
    topSpaceBlock.appendChild(topSpaceInput);
    topSpaceBlockParentDiv.appendChild(topSpaceBlock);
    return topSpaceBlockParentDiv;
  };

  topMarginBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('margin-top', value + 'px');
      this.baseClass().updateLanguageIsoLabelPopperInstance();
      this.baseClass().handleSelectedContentChanges();
    }
  };

  rightMarginBlock = (): HTMLElement => {
    const rightSpaceBlockParentDiv = document.createElement('div');
    rightSpaceBlockParentDiv.style.flex = '1';
    const rightSpaceBlock = document.createElement('div');
    rightSpaceBlock.className = 'right-pad';
    const rightSpaceInput = document.createElement('input');
    rightSpaceInput.type = 'number';
    rightSpaceInput.name = 'right-margin-input';
    rightSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_RIGHT_MARGIN_INPUT;
    rightSpaceInput.min = '0';
    rightSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    rightSpaceInput.className = 'form-control-design';
    rightSpaceInput.oninput = (event): void => this.rightMarginBlockInputVal(event);
    rightSpaceBlock.appendChild(rightSpaceInput);
    rightSpaceBlockParentDiv.appendChild(rightSpaceBlock);
    return rightSpaceBlockParentDiv;
  };

  rightMarginBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('margin-right', value + 'px');
      this.baseClass().updateLanguageIsoLabelPopperInstance();
      this.baseClass().handleSelectedContentChanges();
    }
  };

  bottomMarginBlock = (): HTMLElement => {
    const bottomSpaceBlockParentDiv = document.createElement('div');
    bottomSpaceBlockParentDiv.style.flex = '1';
    const bottomSpaceBlock = document.createElement('div');
    bottomSpaceBlock.className = 'bottom-pad';
    const bottomSpaceInput = document.createElement('input');
    bottomSpaceInput.name = 'bottom-margin-input';
    bottomSpaceInput.type = 'number';
    bottomSpaceInput.id = CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BOTTOM_MARGIN_INPUT;
    bottomSpaceInput.min = '0';
    bottomSpaceInput.onkeypress = (event): void => this.baseClass().preventNonNumericalInput(event);
    bottomSpaceInput.className = 'form-control-design';
    bottomSpaceInput.oninput = (event): void => this.bottomMarginBlockInputVal(event);
    bottomSpaceBlock.appendChild(bottomSpaceInput);
    bottomSpaceBlockParentDiv.appendChild(bottomSpaceBlock);
    return bottomSpaceBlockParentDiv;
  };

  bottomMarginBlockInputVal = (event: Event): void => {
    const target: HTMLInputElement | null = event.target as HTMLInputElement | null;
    if (target) {
      const value = target.value;
      this.baseClass().applyStyleToSelectedElement('margin-bottom', value + 'px');
      this.baseClass().handleSelectedContentChanges();
    }
  };
}
