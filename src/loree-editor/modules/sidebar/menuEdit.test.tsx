import CONSTANTS from '../../constant';
import Navigate from './navigate';
import { specialElement } from '../../elements/specialElement';
import { getElementsByClassName } from '../../common/dom';
// For Adding and deleting menu items.
describe('Adding and removing Menus', () => {
  const navInstance = new Navigate();
  const events: any = {};
  const wrapper = document.createElement('div');
  const dummyInput1 = document.createElement('div');
  dummyInput1.id = 'dummyId1';

  beforeEach(() => {
    dummyInput1.innerHTML = specialElement.contents[1].innerContent[0].content;
    navInstance.countElements = jest.fn().mockImplementation(() => 4);
    document.addEventListener = jest.fn((event, callback) => {
      events[event] = callback;
    });
  });

  test('Adding menu items', () => {
    navInstance.getSelectedElement = jest.fn().mockImplementation(() => dummyInput1);
    navInstance.getSelectedMenuRows = jest.fn().mockImplementation(() => 5);
    navInstance.attachCustomizeStyleToElement = jest
      .fn()
      .mockImplementation((mockValue) => mockValue);

    navInstance.attachNoOfLinks(wrapper);
    document.body.innerHTML = '';
    document.body.appendChild(wrapper);
    document.body.appendChild(dummyInput1);
    const sel = document.getElementById('menuLinkNo');
    sel!.setAttribute('value', '6');

    const e = new Event('change');
    const element = document.querySelector('#menuLinkNo');
    element!.dispatchEvent(e);
    expect(document.getElementsByTagName('ul')[0].childElementCount).toBe(6);
    expect(
      getElementsByClassName('nav-item')[5].classList.contains(
        CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT,
      ),
    ).toBeTruthy();
  });
});
describe('Removing process from the navigation menu', () => {
  const navInstance = new Navigate();
  const events: any = {};
  const wrapper = document.createElement('div');
  const dummyInput2 = document.createElement('div');

  beforeEach(() => {
    document.body.innerHTML = '';
    dummyInput2.id = 'dummyId2';
    dummyInput2.innerHTML = specialElement.contents[1].innerContent[0].content;

    const loreeWrapper = document.createElement('div');
    loreeWrapper.id = 'loree-wrapper';
    loreeWrapper.className = 'wrapper';
    navInstance.countElements = jest.fn().mockImplementation(() => 4);
    document.addEventListener = jest.fn((event, callback) => {
      events[event] = callback;
    });
    navInstance.getSelectedElement = jest.fn().mockImplementation(() => dummyInput2);
    navInstance.getSelectedMenuRows = jest.fn().mockImplementation(() => 3);
    navInstance.attachNoOfLinks(wrapper);
    document.body.innerHTML = '';
    document.body.appendChild(wrapper);
    document.body.appendChild(dummyInput2);
    document.body.appendChild(loreeWrapper);
  });

  test('Removing menu items by confirmed', () => {
    const sel = document.getElementById('menuLinkNo');
    sel!.setAttribute('value', '4');
    const e = new Event('change');
    const element = document.querySelector('#menuLinkNo');
    element!.dispatchEvent(e);

    const deleteMenuYesButton = document.getElementById('deleteMenuProps');
    deleteMenuYesButton!.dispatchEvent(new Event('click'));
    expect(document.getElementsByTagName('ul')[0].childElementCount).toBe(4);
  });
  test('Try to remove and cancel menu item', () => {
    const sel = document.getElementById('menuLinkNo');
    sel!.setAttribute('value', '3');
    const e = new Event('change');
    const element = document.querySelector('#menuLinkNo');
    element!.dispatchEvent(e);

    const deleteMenuNoButton = document.getElementById('deleteMenuPropsStop');
    deleteMenuNoButton!.dispatchEvent(new Event('click'));
    expect(document.getElementsByTagName('ul')[0].childElementCount).toBe(5);
  });
});
