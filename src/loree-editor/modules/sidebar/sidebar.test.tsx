import React from 'react';
import ReactDOM from 'react-dom';
import Sidebar from './sidebar';
import CONSTANTS from '../../constant';
import TextOptions from '../textOptions/UI/textOptionsUI';
import { fireEvent } from '@testing-library/react';
import { hideTextOptionSubElements } from '../../utils';
import { createDiv, getElementById } from '../../common/dom';
import { API } from 'aws-amplify';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('As a Loree user', () => {
  const sideBar = new Sidebar();
  beforeAll(() => {
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
  });
  describe('When loaded and entered into an editor', () => {
    // Test the entire sibar items initated and rendered
    test('the Side bar elements initiated', () => {
      const attachSidebar = jest.spyOn(sideBar, 'attachSidebar');
      const attachSubSidebar = jest.spyOn(sideBar, 'attachSubSidebar');
      const attachSidebarOpenButton = jest.spyOn(sideBar, 'attachSidebarOpenButton');
      const attachSidebarContentWrapper = jest.spyOn(sideBar, 'attachSidebarContentWrapper');
      const attachSidebarRowContent = jest.spyOn(sideBar, 'attachSidebarRowContent');
      const attachSidebarElementContent = jest.spyOn(sideBar, 'attachSidebarElementContent');
      const attachSidebarDesignContent = jest.spyOn(sideBar, 'attachSidebarDesignContent');
      const attachSidebarTableDesignContent = jest.spyOn(
        sideBar,
        'attachSidebarTableDesignContent',
      );
      const attachSidebarMenuContent = jest.spyOn(sideBar, 'attachSidebarMenuContent');
      const attachSidebarContainerDesignContent = jest.spyOn(
        sideBar,
        'attachSidebarContainerDesignContent',
      );
      sideBar.initiate({ features: { image: true } });
      expect(attachSidebar).toHaveBeenCalledTimes(1);
      expect(attachSubSidebar).toHaveBeenCalledTimes(1);
      expect(attachSidebarOpenButton).toHaveBeenCalledTimes(1);
      expect(attachSidebarContentWrapper).toHaveBeenCalledTimes(1);
      expect(attachSidebarRowContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarElementContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarDesignContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarTableDesignContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarMenuContent).toHaveBeenCalledTimes(1);
      expect(attachSidebarContainerDesignContent).toHaveBeenCalledTimes(1);
    });
    test('the Sidebar rendered', () => {
      const loreeSidebar = getElementById('loree-sidebar');
      expect(loreeSidebar).not.toBeNull();
    });
    test('Check the SubSidebar rendered', () => {
      const loreeSubSidebar = getElementById('loree-sub-sidebar');
      expect(loreeSubSidebar).not.toBeNull();
    });
    test('Verify the SideBar open button present', () => {
      const sideBarButton = getElementById('loree-sidebar-open-button');
      expect(sideBarButton).not.toBeNull();
      expect(sideBarButton?.tagName).toEqual('BUTTON');
    });
    test('Check the Row sidebar section rendered', () => {
      const rowSidebarSection = getElementById('loree-sidebar-row-section');
      expect(rowSidebarSection).not.toBeNull();
    });
    test('Check the Element Section rendered', () => {
      const elementsSidebar = getElementById('loree-sidebar-content-wrapper');
      expect(elementsSidebar).not.toBeNull();
    });
    test('Check the Design section rendered', () => {
      const designSection = getElementById('loree-sidebar-design-section');
      expect(designSection).not.toBeNull();
    });
    test('Verify the table sidebar section rendered', () => {
      const tableSection = getElementById('loree-sidebar-table-design-section');
      expect(tableSection).not.toBeNull();
    });
    test('Verify the container is rendered into dom', () => {
      const containerSection = getElementById('loree-sidebar-container-design-section');
      expect(containerSection).not.toBeNull();
    });
    test('hide template wrapper when customRow is invoked', async () => {
      API.graphql = jest.fn();
      const createRowSection = getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION);
      createRowSection.style.display = 'block';
      await sideBar.attachGlobalTemplateHandler();
      expect(getElementById(CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_WRAPPER)).toBeNull();
    });
  });
});

describe('#Sidebar', () => {
  const sideBarInstance = new Sidebar();
  const textOptionsInstance = new TextOptions();
  let iframeDocument: Document;
  let textOptionSubELement: HTMLElement;
  beforeAll(() => {
    document.body.innerHTML = '';
    const domDiv = createDiv();
    ReactDOM.render(
      <div>
        <iframe title={CONSTANTS.LOREE_IFRAME} id={CONSTANTS.LOREE_IFRAME} />
        <div id={CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER} />
        <div id={CONSTANTS.LOREE_WRAPPER} />
      </div>,
      domDiv,
    );
    document.body.append(domDiv);
    sideBarInstance.attachSidebar();
    sideBarInstance.getDocument = jest.fn().mockImplementation(() => document);
  });
  test('rendering the subelement of the text option poper', () => {
    const wrapper = getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER);
    textOptionsInstance.appendParagraphOption(wrapper);
    iframeDocument = (getElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement)
      ?.contentDocument as Document;
    expect(
      iframeDocument.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER)?.style
        .display,
    ).toBe('none');
    textOptionSubELement = iframeDocument.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER,
    ) as HTMLElement;
    if (textOptionSubELement) textOptionSubELement.style.display = 'block';
  });

  test('should close the text option popper when click on the side bar', () => {
    const sideBarElement = getElementById(CONSTANTS.LOREE_SIDEBAR);
    fireEvent.click(
      sideBarElement,
      void hideTextOptionSubElements(iframeDocument, sideBarInstance.textOptionSubElements),
    );
    expect(textOptionSubELement?.style.display).toBe('none');
  });

  test('should call hideAnchorOption function', () => {
    const sideBarElement = getElementById(CONSTANTS.LOREE_SIDEBAR);
    const spysetRangeForSelection = jest.spyOn(sideBarInstance, 'hideAnchorOption');
    sideBarElement.click();
    expect(spysetRangeForSelection).toBeCalledTimes(1);
  });
});
