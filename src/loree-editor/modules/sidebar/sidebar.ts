import CONSTANTS from '../../constant';
import STYLE_CONSTANTS from '../../styleConstant';
import Base from '../../base';
import Row from './row';
import Design from './design';
import TableDesign from './tableDesign';
import Navigate from './navigate';
import ContainerDesign from './containerDesign';
import Element from './element';
import { VideoModal } from '../videoModal/videoModal';
import { closeIcon } from './iconHolder';
import { ImageModal } from '../imageModal/imageModal';
import { ViewCodeModal } from '../viewCodeModal/viewCodeModal';
import {
  categoryWrapper,
  fetchCategory,
  retrieveCustomGlobalTemplate,
  templatesBlock,
  preventDropdownClosing,
  handleTemplateCategoryApplyButton,
  refreshCategoryDropdown,
  refreshCheckBox,
  retrieveCustomTemplate,
} from '../customBlocks/customTemplateEvents';
import { searchWrapper } from '../customBlocks/customBlocksUI';
import { PreviewModal } from '../previewModal/previewModal';
import EmbedURL from './embedUrlElement';
import { LmsAccess } from '../../../utils/lmsAccess';
import { isCanvasAndBB, isD2l } from '../../../lmsConfig';
import { ConfigInterface, FeatureInterface } from '../../interface';
import { getEditorElementById, hideTextOptionSubElements } from '../../../loree-editor/utils';
import { translate } from '../../../i18n/translate';
import { createDiv, getElementsByClassName } from '../../common/dom';
import { setCustomBlockEditOptions } from '../customBlocks/customBlockHandler';
import { checkIsKalturaConfigured } from '../videoModal/videoModalUtils';
export const isTemplateClicked = false;

class Sidebar extends Base {
  row: Row;
  element: Element;
  design: Design;
  tableDesign: TableDesign;
  navigate: Navigate;
  containerDesign: ContainerDesign;
  videoModal: VideoModal;
  imageModal: ImageModal;
  embedUrl: EmbedURL;
  viewCodeModal: ViewCodeModal;
  previewModal: PreviewModal;
  sidebarOpenButton: boolean;
  categoryList: [];
  textOptionSubElements: string[] = [
    CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_CONTENT_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_WORD_CONTENT_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_CONTENT_WRAPPER,
    CONSTANTS.LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER,
  ];

  constructor() {
    super();
    this.row = new Row();
    this.element = new Element();
    this.design = new Design();
    this.tableDesign = new TableDesign();
    this.navigate = new Navigate();
    this.containerDesign = new ContainerDesign();
    this.videoModal = new VideoModal();
    this.imageModal = new ImageModal();
    this.embedUrl = new EmbedURL();
    this.viewCodeModal = new ViewCodeModal();
    this.previewModal = new PreviewModal();
    this.sidebarOpenButton = false;
    this.categoryList = [];
  }

  initiate = (config: ConfigInterface): void => {
    this.attachSidebar();
    this.attachSubSidebar();
    this.attachSidebarOpenButton();
    this.attachSidebarContentWrapper();
    this.attachSidebarRowContent(config);
    this.attachSidebarElementContent(config);
    this.attachSidebarDesignContent(config);
    this.attachSidebarTableDesignContent(config);
    this.attachSidebarMenuContent(config);
    this.attachSidebarContainerDesignContent(config);
    this.imageUploaderInput();
    this.attachImageModalBackground();
    this.attachImageModal();
    this.attachImageModalContent(config.features);
    this.videoUploaderInput();
    this.attachVideoModalBackground();
    this.attachVideoModal();
    void this.attachVideoModalContent(config.features as FeatureInterface);
    this.attachEmbedUrlModalBackground();
    this.attachEmbedUrlModal();
    this.attachEmbedUrlContent();
    this.attachViewCodeModalBackground();
    this.attachViewCodeModal();
    this.attachViewCodeModalContent();
    this.attachBannerImageModal();
    this.attachPreviewModalBackground();
    this.attachPreviewModal();
    this.attachPreviewModalContent();
  };

  attachSidebar = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const sidebar: HTMLElement = document.createElement('div');
      sidebar.id = CONSTANTS.LOREE_SIDEBAR;
      sidebar.className = 'loreeSidebar custom-scrollbar sidebar-scroll';
      sidebar.style.display = 'none';
      sidebar.style.width = `${STYLE_CONSTANTS.SIDEBAR_WIDTH}px`;
      sidebar.addEventListener('click', () => {
        hideTextOptionSubElements(this.getDocument(), this.textOptionSubElements);
        this.hideAnchorOption();
      });
      editorWrapper.appendChild(sidebar);
      this.attachSidebarCloseButton();
    }
  };

  attachSubSidebar = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const subSideBar: HTMLDivElement = document.createElement('div');
      subSideBar.className = 'loreeSubSidebar';
      subSideBar.id = CONSTANTS.LOREE_SUB_SIDEBAR;
      subSideBar.style.display = 'none';
      subSideBar.style.width = `${STYLE_CONSTANTS.SUB_SIDEBAR_WIDTH}px`;
      subSideBar.style.left = `${STYLE_CONSTANTS.SIDEBAR_WIDTH}px`;
      editorWrapper.appendChild(subSideBar);
    }
  };

  sidebarCloseButtonHandler = (): void => {
    this.hideSidebar();
    const elementSectionButton = document.getElementById(
      'loree-sidebar-element-section-button',
    ) as HTMLButtonElement;
    if (!elementSectionButton?.disabled) {
      this.hideCloseElementButtonToSelectedElement();
      this.showAddElementButtonToSelectedElement();
    }
  };

  attachSidebarCloseButton = (): void => {
    const sidebarWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    if (sidebarWrapper) {
      const sidebarCloseButtonWrapper = document.createElement('div');
      sidebarCloseButtonWrapper.className = 'sidebarCloseButtonWrapper';
      const button: HTMLButtonElement = document.createElement('button');
      button.className = 'sidebarCloseButton';
      button.id = CONSTANTS.LOREE_SIDEBAR_CLOSE_BUTTON;
      button.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 98.156 90.25"><defs><style>.a{stroke:rgba(0,0,0,0);stroke-miterlimit:10;}</style></defs><path class="a" d="M81.79,86.615,46.539,51.364a8.964,8.964,0,0,1-2.618-6.738,8.962,8.962,0,0,1,2.618-6.738L81.79,2.636A9,9,0,0,1,94.518,15.364L65.257,44.626,94.518,73.887A9,9,0,1,1,81.79,86.615Zm-43.913,0L2.626,51.364A8.964,8.964,0,0,1,.008,44.626a8.962,8.962,0,0,1,2.618-6.738L37.877,2.636A9,9,0,0,1,50.605,15.364L21.344,44.626,50.605,73.887A9,9,0,1,1,37.877,86.615Z" transform="translate(0.501 0.499)"/></svg>`;
      button.onclick = () => this.sidebarCloseButtonHandler();
      sidebarCloseButtonWrapper.appendChild(button);
      sidebarWrapper.appendChild(sidebarCloseButtonWrapper);
    }
  };

  sidebarOpenButtonHandler = (): void => {
    this.showSidebar();
    const elementSectionButton = document.getElementById(
      'loree-sidebar-element-section-button',
    ) as HTMLButtonElement;
    if (!elementSectionButton?.disabled) {
      this.expandElementSection();
      this.hideAddElementButtonToSelectedElement();
      this.appendCloseAddElementButtonToSelectedElement();
    }
  };

  attachSidebarOpenButton = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const button: HTMLButtonElement = document.createElement('button');
      button.className = 'sidebarOpenButton';
      button.id = CONSTANTS.LOREE_SIDEBAR_OPEN_BUTTON;
      button.style.display = 'none';
      button.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 81.555 75"><defs><style>.a{stroke:#fff ;stroke-miterlimit:10; fill: #fff;}</style></defs><path class="a" d="M67.815,71.815,38.587,42.587A7.432,7.432,0,0,1,36.416,37a7.431,7.431,0,0,1,2.171-5.587L67.815,2.186A7.462,7.462,0,0,1,78.368,12.739L54.107,37,78.368,61.262A7.462,7.462,0,1,1,67.815,71.815Zm-36.409,0L2.177,42.587A7.432,7.432,0,0,1,.007,37a7.431,7.431,0,0,1,2.171-5.587L31.405,2.186A7.462,7.462,0,0,1,41.958,12.739L17.7,37,41.958,61.262A7.462,7.462,0,1,1,31.405,71.815Z" transform="translate(0.501 0.499)"/></svg>`;
      button.onclick = () => this.sidebarOpenButtonHandler();
      editorWrapper.appendChild(button);
    }
  };

  attachSidebarContentWrapper = (): void => {
    const sidebarWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    if (sidebarWrapper) {
      const sidebarContentWrapper = document.createElement('div');
      sidebarContentWrapper.id = CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER;
      sidebarContentWrapper.className = 'accordion';
      sidebarWrapper.appendChild(sidebarContentWrapper);
    }
  };

  attachSidebarRowContent = (config: ConfigInterface): void => {
    this.row.initiate(config);
  };

  attachSidebarElementContent = (config: ConfigInterface): void => {
    this.element.initiate(config);
  };

  attachSidebarDesignContent = (config: ConfigInterface): void => {
    this.design.initiate(config);
  };

  attachSidebarTableDesignContent = (config: ConfigInterface): void => {
    const featureList = config.features;
    if (featureList?.tabledesign !== undefined || featureList?.tabledesign !== false)
      this.tableDesign.initiate(config);
  };

  attachSidebarMenuContent = (config: ConfigInterface): void => {
    const featureList = config.features;
    if (featureList?.navigate !== undefined || featureList?.navigate !== false)
      this.navigate.initiate(config);
  };

  attachSidebarContainerDesignContent = (config = {}): void => {
    this.containerDesign.initiate(config);
  };

  // Common function for image uploader
  imageUploaderInput = (): void => {
    const sideBar = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    if (sideBar) {
      const uploader = document.createElement('input');
      uploader.id = CONSTANTS.LOREE_IMAGE_UPLOADER_INPUT;
      uploader.type = 'file';
      uploader.className = 'd-none';
      uploader.accept = 'image/x-png,image/jpeg,image/jpg,image/gif';
      sideBar.appendChild(uploader);
    }
  };

  attachImageModalBackground = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const imageModalBackground = document.createElement('div');
      imageModalBackground.id = CONSTANTS.LOREE_IMAGE_MODAL_BACKGROUND;
      imageModalBackground.style.display = 'none';
      imageModalBackground.innerHTML = ``;
      editorWrapper.appendChild(imageModalBackground);
    }
  };

  attachImageModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const imageModal = document.createElement('div');
      imageModal.id = CONSTANTS.LOREE_IMAGE_MODAL_WRAPPER;
      imageModal.className = 'modal';
      imageModal.style.display = 'none';
      imageModal.innerHTML = `<div id=${CONSTANTS.LOREE_IMAGE_MODAL_DIALOG} class="modal-dialog-centered"></div>`;
      editorWrapper.appendChild(imageModal);
    }
  };

  attachImageModalContent = (features: FeatureInterface | undefined): void => {
    this.imageModal.initiate(features);
    this.imageModal.imageModalEventHandlers();
  };

  videoUploaderInput = (): void => {
    const sideBar = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    if (sideBar) {
      const uploader = document.createElement('input');
      uploader.id = CONSTANTS.LOREE_VIDEO_UPLOADER_INPUT;
      uploader.type = 'file';
      uploader.className = 'd-none';
      uploader.accept = '.mp4,.mov';
      sideBar.appendChild(uploader);
    }
  };

  attachVideoModalBackground = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const videoModalBackground = document.createElement('div');
      videoModalBackground.id = CONSTANTS.LOREE_VIDEO_MODAL_BACKGROUND;
      videoModalBackground.style.display = 'none';
      videoModalBackground.innerHTML = ``;
      editorWrapper.appendChild(videoModalBackground);
    }
  };

  attachVideoModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    const lms = new LmsAccess();
    if (editorWrapper) {
      const videoModal = document.createElement('div');
      videoModal.id = CONSTANTS.LOREE_VIDEO_MODAL_WRAPPER;
      videoModal.className = 'modal';
      videoModal.style.display = 'none';
      videoModal.innerHTML = `<div id=${
        CONSTANTS.LOREE_VIDEO_MODAL_DIALOG
      } class="modal-dialog-centered d-flex justify-content-center align-items-center ${
        lms.getAccess() && !isD2l() ? '' : 'standalone-modal'
      }"></div>`;
      editorWrapper.appendChild(videoModal);
    }
  };

  attachVideoModalContent = async (featuresList: FeatureInterface) => {
    const isKalturaConfigured: Boolean | undefined = await checkIsKalturaConfigured();
    void this.videoModal.initiate(featuresList, isKalturaConfigured);
    this.videoModal.videoModalEventHandlers();
  };

  attachViewCodeModalBackground = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const viewCodeModalBackground = document.createElement('div');
      viewCodeModalBackground.id = CONSTANTS.LOREE_VIEW_CODE_MODAL_BACKGROUND;
      viewCodeModalBackground.style.display = 'none';
      viewCodeModalBackground.innerHTML = ``;
      editorWrapper.appendChild(viewCodeModalBackground);
    }
  };

  attachViewCodeModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const viewCodeModal = document.createElement('div');
      viewCodeModal.id = CONSTANTS.LOREE_VIEW_CODE_MODAL_WRAPPER;
      viewCodeModal.className = 'modal';
      viewCodeModal.style.display = 'none';
      viewCodeModal.innerHTML = `<div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_DIALOG} class="modal-dialog-centered"></div>`;
      editorWrapper.appendChild(viewCodeModal);
    }
  };

  attachViewCodeModalContent = (): void => {
    this.viewCodeModal.initiate();
    this.viewCodeModal.viewCodeModalEventHandlers();
  };

  attachPreviewModalBackground = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const previewModalBackground = document.createElement('div');
      previewModalBackground.id = CONSTANTS.LOREE_PREVIEW_MODAL_BACKGROUND;
      previewModalBackground.style.display = 'none';
      previewModalBackground.innerHTML = ``;
      editorWrapper.appendChild(previewModalBackground);
    }
  };

  attachPreviewModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const previewModal = document.createElement('div');
      previewModal.id = CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER;
      previewModal.className = 'modal';
      previewModal.style.display = 'none';
      previewModal.innerHTML = `<div id=${CONSTANTS.LOREE_PREVIEW_MODAL_DIALOG} class="modal-dialog-centered"></div>`;
      editorWrapper.appendChild(previewModal);
    }
  };

  attachPreviewModalContent = (): void => {
    this.previewModal.initiate();
    this.previewModal.previewModalEventHandlers();
  };

  // embed URL element
  attachEmbedUrlModalBackground = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const embedUrlModalBackground = document.createElement('div');
      embedUrlModalBackground.id = CONSTANTS.LOREE_EMBED_URL_MODAL_BACKGROUND;
      embedUrlModalBackground.style.display = 'none';
      embedUrlModalBackground.innerHTML = ``;
      editorWrapper.appendChild(embedUrlModalBackground);
    }
  };

  attachEmbedUrlModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const embedUrlModal = document.createElement('div');
      embedUrlModal.id = CONSTANTS.LOREE_EMBED_URL_MODAL_WRAPPER;
      embedUrlModal.className = 'modal';
      embedUrlModal.style.display = 'none';
      embedUrlModal.innerHTML = `<div id=${CONSTANTS.LOREE_EMBED_URL_MODAL_DIALOG} class="modal-dialog-centered"></div>`;
      editorWrapper.appendChild(embedUrlModal);
    }
  };

  attachEmbedUrlContent = (): void => {
    this.embedUrl.initiate();
    this.embedUrl.embedUrlEventHandlers();
  };

  // Banner
  attachBannerImageModal = (): void => {
    const editorWrapper: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const wrapper = document.createElement('div');
      wrapper.style.display = 'none';
      wrapper.className = 'banner-image-modal-wrapper';
      wrapper.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_WRAPPER;
      this.attachBannerImageModalContent(wrapper);
      editorWrapper.appendChild(wrapper);
    }
  };

  attachBannerImageModalContent = (wrapper: HTMLElement): void => {
    const content = document.createElement('div');
    content.className = 'col-lg-5 banner-image-modal-content';
    this.attachBannerImageModalHeader(content);
    this.attachBannerImageModalBody(content);
    this.attachBannerImageModalFooter(content);
    wrapper.appendChild(content);
  };

  attachBannerImageModalHeader = (wrapper: HTMLElement): void => {
    const header = document.createElement('div');
    header.className = 'banner-image-modal-content-header';
    const title = document.createElement('span');
    title.className = 'banner-image-modal-content-header-title';
    title.innerHTML = `${translate('sidebar.bannerediting')}`;
    header.appendChild(title);
    const close = document.createElement('button');
    close.className = 'banner-image-modal-content-header-close';
    close.innerHTML = closeIcon;
    close.onclick = (): void => this.imageModal.hideBannerImageModal();
    header.appendChild(close);
    wrapper.appendChild(header);
  };

  attachBannerImageModalBody = (wrapper: HTMLElement): void => {
    const body = document.createElement('div');
    body.className = 'banner-image-modal-content-body';
    const message = document.createElement('div');
    message.className = 'banner-image-modal-content-body-message';
    message.innerHTML = `${translate('sidebar.bannerimagewarning')}`;
    body.appendChild(message);
    const bodyContent = document.createElement('div');
    bodyContent.className = 'row';
    const imageSection = document.createElement('div');
    imageSection.className = 'col-10 pt-1';
    this.attachBannerImageSection(imageSection);
    bodyContent.appendChild(imageSection);
    const optionSection = document.createElement('div');
    optionSection.className = 'col-2 pt-1';
    this.attachBannerOptionSection(optionSection);
    bodyContent.appendChild(optionSection);
    body.appendChild(bodyContent);
    wrapper.appendChild(body);
  };

  attachBannerImageSection = (wrapper: HTMLElement): void => {
    const imageSection = document.createElement('div');
    imageSection.className = 'banner-image-modal-content-body-image-section';
    imageSection.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION;
    const imageTag = document.createElement('img');
    imageTag.className = 'banner-modal-placeholder-image';
    imageTag.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE;
    imageTag.alt = 'placeholder image';
    imageTag.src = '';
    imageSection.appendChild(imageTag);
    const highlighter = document.createElement('div');
    highlighter.className = 'banner-image-modal-highlighter';
    highlighter.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_HIGHLIGHTER;
    imageSection.appendChild(highlighter);
    wrapper.appendChild(imageSection);
  };

  attachBannerOptionSection = (wrapper: HTMLElement): void => {
    const optionSection = document.createElement('div');
    optionSection.className = 'banner-image-modal-content-body-option-section';
    optionSection.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_SECTION;
    const topButton = document.createElement('button');
    topButton.className = 'option active';
    topButton.innerHTML = `${translate('sidebar.top')}`;
    topButton.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_TOP_BUTTON;
    topButton.onclick = (): void => this.imageModal.handleBannerImageCropPositionChange('top');
    optionSection.appendChild(topButton);
    const centerButton = document.createElement('button');
    centerButton.className = 'option';
    centerButton.innerHTML = `${translate('sidebar.center')}`;
    centerButton.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_CENTER_BUTTON;
    centerButton.onclick = (): void =>
      this.imageModal.handleBannerImageCropPositionChange('center');
    optionSection.appendChild(centerButton);
    const bottomButton = document.createElement('button');
    bottomButton.className = 'option';
    bottomButton.innerHTML = `${translate('sidebar.bottom')}`;
    bottomButton.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_BOTTOM_BUTTON;
    bottomButton.onclick = (): void =>
      this.imageModal.handleBannerImageCropPositionChange('bottom');
    optionSection.appendChild(bottomButton);
    wrapper.appendChild(optionSection);
  };

  attachBannerImageModalFooter = (wrapper: HTMLElement): void => {
    const footer = document.createElement('div');
    footer.className = 'banner-image-modal-content-footer';
    // const backButton = document.createElement('button');
    // backButton.className = 'option primary';
    // backButton.innerHTML = 'Back';
    // footer.appendChild(backButton);
    const cancelButton = document.createElement('button');
    cancelButton.className = 'option primary';
    cancelButton.innerHTML = `${translate('global.cancel')}`;
    cancelButton.onclick = (): void => this.imageModal.hideBannerImageModal();
    footer.appendChild(cancelButton);
    const applyButton = document.createElement('button');
    applyButton.className = 'option secondary';
    applyButton.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_APPLY_BUTTON;
    applyButton.innerHTML = `${translate('global.apply')}`;
    footer.appendChild(applyButton);
    wrapper.appendChild(footer);
  };

  // Banner
  /*
   * attach template search and filter wrapper
   */
  attachGlobalTemplateHandler = async () => {
    const sideBar: HTMLElement | null = getEditorElementById(CONSTANTS.LOREE_SIDEBAR);
    if (!sideBar) return;

    !isCanvasAndBB() ? await retrieveCustomTemplate() : await retrieveCustomGlobalTemplate();
    await fetchCategory();
    const sidebarTemplateLoader = getElementsByClassName('sidebarTemplateLoader')[0];
    if (!sidebarTemplateLoader) {
      return;
    }

    const globalTemplateWrapper = createDiv('div');
    globalTemplateWrapper.id = CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_WRAPPER;
    globalTemplateWrapper.appendChild(searchWrapper());
    globalTemplateWrapper.insertAdjacentHTML('beforeend', categoryWrapper());
    globalTemplateWrapper.appendChild(templatesBlock());
    sidebarTemplateLoader?.remove();
    sideBar.appendChild(globalTemplateWrapper);
    void refreshCheckBox();

    const dropDownApplyBtn = getEditorElementById(
      'loree-sidebar-custom-global-template-category-button-apply',
    );
    if (dropDownApplyBtn) {
      dropDownApplyBtn.onclick = async () => handleTemplateCategoryApplyButton();
    }
    const dropdownList = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_DROPDOWN_MENU,
    ) as HTMLElement;
    if (dropdownList) {
      dropdownList.onclick = (e) => preventDropdownClosing(e);
    }
    const selectedListWrapper = getEditorElementById('template-category-selected-list-wrapper');
    if (selectedListWrapper) {
      selectedListWrapper.onclick = (e) => preventDropdownClosing(e);
    }
    const dropdownContainer = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_DROPDOWN,
    );
    if (dropdownContainer) {
      dropdownContainer.onclick = (): void => {
        refreshCategoryDropdown();
      };
    }
    setCustomBlockEditOptions('My Template');
  };
}

export default Sidebar;
