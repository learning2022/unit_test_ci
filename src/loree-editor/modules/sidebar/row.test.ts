import { API } from 'aws-amplify';
import { createDiv } from '../../common/dom';
import CONSTANTS from '../../constant';
import Row from './row';
describe('#Row in Sidebar', () => {
  const rowInstance = new Row();
  beforeAll(() => {
    const containerDesign = createDiv('div');
    API.graphql = jest.fn().mockReturnValue({
      data: { listCustomBlocks: { items: [] }, categoryByPlatform: { items: [] } },
    });
    containerDesign.id = CONSTANTS.LOREE_SIDEBAR;
    document.body.appendChild(containerDesign);
    rowInstance.attachContent(containerDesign, {});
  });
  test('should show the loader to fetch the templates and should refresh the template list', async () => {
    rowInstance.refreshCustomBlockSearch = jest.fn();
    await rowInstance.handleCustomRows();
    expect(rowInstance.refreshCustomBlockSearch).toBeCalledWith('row');
  });
});
