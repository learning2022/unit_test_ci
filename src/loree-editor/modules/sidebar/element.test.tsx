import Element from './element';
import CONSTANTS from '../../constant';
import { specialElement } from '../../elements/specialElement';

import { API } from 'aws-amplify';
import { createDiv, getElementById, getNElementByClassName } from '../../common/dom';
import { specialElementData, subSidebarMock } from './elementsMockData';

describe('Navigation Menu Process', () => {
  const elementInstance = new Element();
  beforeEach(() => {
    document.body.innerHTML =
      '<div id = "' +
      CONSTANTS.LOREE_SUB_SIDEBAR +
      '">' +
      '<div class="subSidebarElementWrapper">' +
      '<div class="subSidebarElements"></div>' +
      '' +
      '</div>' +
      '</div>';
  });

  test('Selecting Icon with text option', () => {
    elementInstance.handleSubMenuSelection(specialElement.contents[0]);
    const addedContent = document.getElementsByClassName('subInnerSidebarElements');
    expect(addedContent[0].innerHTML).toEqual(specialElement.contents[0].innerContent[0].template);
  });
  test('Selecting Navigate option', () => {
    elementInstance.handleSubMenuSelection(specialElement.contents[1]);
    const addedContent = document.getElementsByClassName('subInnerSidebarElements');
    expect(addedContent[0].innerHTML).toEqual(specialElement.contents[1].innerContent[0].template);
  });
  test.each([0, 1, 2, 3])('Navigation presets', (position) => {
    elementInstance.handleSubMenuSelection(specialElement.contents[1]);
    const addedContent = document.getElementsByClassName('subInnerSidebarElements');
    expect(addedContent[position].innerHTML).toEqual(
      specialElement.contents[1].innerContent[position].template,
    );
  });

  afterAll(() => {
    document.body.innerHTML = '';
  });
});

describe('#Row in Sidebar', () => {
  const elementInstance = new Element();
  beforeAll(() => {
    const containerDesign = createDiv('div');
    API.graphql = jest.fn().mockReturnValue({
      data: { listCustomBlocks: { items: [] }, categoryByPlatform: { items: [] } },
    });
    containerDesign.id = CONSTANTS.LOREE_SIDEBAR;
    document.body.appendChild(containerDesign);
    elementInstance.attachContent(containerDesign, { features: {} });
  });
  test('should show the loader to fetch the templates and should refresh the template list', async () => {
    elementInstance.refreshCustomBlockSearch = jest.fn();
    await elementInstance.handleCustomElements();
    expect(elementInstance.refreshCustomBlockSearch).toBeCalledWith('elements');
  });
});

describe('#handleMultipleElementItems', () => {
  let elementInstance: Element;

  beforeEach(() => {
    elementInstance = new Element();
    const subSiderbar = subSidebarMock;
    document.body.innerHTML = subSiderbar;
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('verify whether navigation element is enabled', () => {
    const spy = jest.spyOn(elementInstance, 'handleSubMenuSelection');

    const elementData = specialElementData;
    elementInstance.handleMultipleElementItems(elementData, { navigationMenu: true });
    expect(getElementById(CONSTANTS.LOREE_IFRAME_NAVIGATE_ID)).toBeInTheDocument();

    const subsideBarElements = getNElementByClassName('subSidebarElements', 0);
    subsideBarElements.click();
    expect(spy).toHaveBeenCalled();
  });

  test('verify whether navigation element is disabled', () => {
    const elementData = specialElementData;
    elementInstance.handleMultipleElementItems(elementData, { navigationMenu: false });
    expect(getElementById(CONSTANTS.LOREE_IFRAME_NAVIGATE_ID)).not.toBeInTheDocument();
  });
});
