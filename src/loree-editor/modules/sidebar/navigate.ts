import Base from '../../base';
import CONSTANTS from '../../constant';
import ICONS from './icons';
import '@simonwep/pickr/dist/themes/nano.min.css';
import Pickr from '@simonwep/pickr';
import { navigationRemoveAlert, closeMenuAlert } from '../../alert';
import {
  getNElementByClassName,
  createDiv,
  createinputElement,
  createElement,
} from '../../common/dom';
import { translate } from '../../../i18n/translate';
import {
  getCurrentI18nLanguage,
  getEditorElementsByClassName,
  getUserLanguageStore,
  setColorNavAlignment,
} from '../../utils';

/* eslint-disable  @typescript-eslint/no-explicit-any */
let borderColorPicker: Pickr;
let backgroundColorPicker: Pickr;
const customColors: string[] = [];
let lastsavedColor: string;

interface ColorPickerInterface extends Pickr.Options {
  i18n: {
    'btn:save': string;
    'btn:cancel': string;
  };
}

class Navigate extends Base {
  initiate = (config: any): void => {
    const sidebarContentWrapper = document.getElementById(CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER);
    if (sidebarContentWrapper) {
      this.appendCustomStyles(config);
      const menuSection = createDiv('div', 'accordionItem');
      menuSection.id = CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION;
      menuSection.className = 'section tableDesignSection';
      menuSection.style.display = 'none';
      this.attachLabel(menuSection);
      this.attachOptions(menuSection);
      sidebarContentWrapper.appendChild(menuSection);
      this.updateMenuSectionUpdateMethod(this.overMenuDesignSectionMethod);
    }
  };

  appendCustomStyles = (config: any) => {
    const colors = config.customColor?.colors;
    if (colors?.length) {
      colors.forEach((clr: any) => {
        customColors.push(clr.color);
      });
    }
  };

  overMenuDesignSectionMethod = (): void => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_MENU) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER) ||
        selectedElement.tagName === 'P'
      ) {
        this.attachMenuOptions();
      }
    }
  };

  attachLabel = (wrapper: HTMLElement): void => {
    const sectionLabel = document.createElement('button');
    sectionLabel.className = 'sectionLabel ';
    sectionLabel.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_BUTTON;
    sectionLabel.dataset.toggle = 'collapse';
    sectionLabel.dataset.target = `#${CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    sectionLabel.setAttribute('aria-expanded', 'true');
    sectionLabel.innerHTML = `<span class="label">${translate(
      'global.design',
    )}</span> <span class="image"> <svg viewBox="0 0 8 14"> <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path> </svg> </span>`;
    wrapper.appendChild(sectionLabel);
  };

  attachMenuOptions = (): void => {
    const accordion = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (accordion) {
      accordion.innerHTML = '';
      this.attachNoOfLinks(accordion);
      this.attachSpaceOption(accordion);
      this.attachAlignmentOption(accordion);
      this.attachColorOption(accordion);
      this.attachBorderOption(accordion);
    }
  };

  attachOptions = (wrapper: HTMLElement): void => {
    const accordion = createDiv('div', 'accordionItem');
    accordion.className = 'menuAccordionWrapper accordion';
    accordion.id = CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER;
    wrapper.appendChild(accordion);
  };

  attachBorderOption = (wrapper: HTMLElement): void => {
    const navigateWrapper = createDiv('div', 'accordionItem');
    navigateWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#menu-design-section-border`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.border')}</span>
    `;
    navigateWrapper.appendChild(accordionButton);
    const accordionContent = createDiv('div', 'accordionItem');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'menu-design-section-border';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const borderOption = createDiv('div', 'accordionItem');
    borderOption.className = 'borderOption';

    const borderWidth = createDiv('div', 'accordionItem');
    borderWidth.className = 'borderWidth d-flex justify-content-between';

    const labelWrapper = createDiv('div', 'accordionItem');
    labelWrapper.className = 'd-flex py-1';
    const label = createDiv('div', 'accordionItem');
    label.className = 'label';
    label.innerHTML = `${translate('design.widthpixel')}`;
    borderWidth.appendChild(label);

    const selectedElement = this.getSelectedElement();
    const input = createinputElement('input');
    input.className = 'input form-control-design border-width';
    input.type = 'number';
    input.value = this.getBorderWidthOfContainer();
    input.onchange = (): void => this.handleNavigateBorderWidthChange();
    labelWrapper.appendChild(input);

    const color = createElement('button');
    color.className = 'color';
    color.style.backgroundColor = this.getBorderColorOfContainer(selectedElement);
    labelWrapper.appendChild(color);
    this.attachBorderColorPicker(color, selectedElement);
    borderWidth.appendChild(labelWrapper);
    borderOption.appendChild(borderWidth);

    const borderType = createDiv('div', 'accordionItem');
    borderType.className = 'borderType';

    const select = document.createElement('select');
    select.className = 'bordertypes';
    select.innerHTML = `<option value="solid">${translate('design.solid')}</option>
    <option value="dashed">${translate('design.dashed')}</option>
    <option value="dotted">${translate('design.dotted')}</option>
    <option value="double">${translate('design.double')}</option>
    <option value="none">${translate('design.none')}</option>`;
    select.value = this.getBorderStyleOfContainer(selectedElement);
    select.id = CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_BORDER_STYLE;
    select.onchange = (): void => this.handleNavigateBorderStyleChange();
    borderType.appendChild(select);

    borderOption.appendChild(borderType);
    accordionContent.appendChild(borderOption);
    navigateWrapper.appendChild(accordionContent);
    wrapper.appendChild(navigateWrapper);
    this.fixColorPickerBehaviour();
  };

  getBorderWidthOfContainer = (): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const borderWidth = selectedElement.style.borderWidth;
    if (!borderWidth) return value;
    value = borderWidth.replace('px', '');
    return value;
  };

  handleNavigateBorderWidthChange = (): void => {
    const element = getNElementByClassName('border-width') as HTMLInputElement;
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    const tableBorderStyle = this.getBorderStyleOfContainer(selectedElement);
    this.applyStyleToSelectedElement('border-width', `${element.value}px`);
    this.applyStyleToSelectedElement('border-style', tableBorderStyle);
  };

  getBorderStyleOfContainer = (selectedElement: HTMLElement | null): string => {
    let value = 'solid';
    if (!selectedElement) return value;
    const borderStyle = selectedElement.style.borderStyle;
    if (!borderStyle) return value;
    value = borderStyle;
    return value;
  };

  handleNavigateBorderStyleChange = (): void => {
    const element = getNElementByClassName('bordertypes') as HTMLInputElement;
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) {
      return;
    }
    this.applyStyleToSelectedElement('border-style', element.value);
  };

  getBorderColorOfContainer = (selectedElement: HTMLElement | null): string => {
    let value = '#000000';
    if (!selectedElement) {
      return value;
    }
    const borderColor = selectedElement.style.borderColor;
    if (!borderColor) {
      return value;
    }
    value = borderColor;
    return value;
  };

  attachBorderColorPicker = (element: HTMLElement, selectedElement: HTMLElement | null): void => {
    const defaultColor = this.getBorderColorOfContainer(selectedElement);
    if (!element) {
      return;
    }
    const options: ColorPickerInterface = {
      el: element,
      theme: 'nano',
      swatches: customColors,
      appClass: CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS,
      default: defaultColor,
      defaultRepresentation: 'HEXA',
      useAsButton: true,
      lockOpacity: true,
      components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
          input: true,
          rgba: false,
          hex: true,
          clear: true,
          cancel: true,
          save: true,
        },
      },
      i18n: {
        'btn:save': translate('global.apply'),
        'btn:cancel': translate('global.cancel'),
      },
    };
    borderColorPicker = new Pickr(options);
    borderColorPicker.on('change', (color: { toHEXA: Function }) => {
      const hexValue = color.toHEXA();
      this.handleNavigateBorderColorChange(hexValue, element);
    });

    borderColorPicker.on('save', (color: { toHEXA: Function }) => {
      if (color) {
        const hexValue = color.toHEXA();
        this.handleNavigateBorderColorChange(hexValue, element);
        borderColorPicker?.hide();
        lastsavedColor = hexValue;
      }
    });

    borderColorPicker.on('cancel', () => {
      if (lastsavedColor) {
        this.handleNavigateBorderColorChange(lastsavedColor, element);
      } else {
        this.handleNavigateBorderColorCancel(defaultColor, element);
      }
      borderColorPicker?.hide();
    });

    borderColorPicker.on('clear', () => {
      this.handleNavigateBorderColorClear('#ffffff', element);
      lastsavedColor = '#ffffff';

      backgroundColorPicker?.hide();
    });
  };

  handleNavigateBorderColorChange = (color: string, element: HTMLElement): void => {
    if (!this.getSelectedElement()) {
      return;
    }
    this.applyStyleToSelectedElement('border-color', color.toString());
    element.style.backgroundColor = color;
  };

  handleNavigateBorderColorCancel = (color: string, element: HTMLElement): void => {
    if (!this.getSelectedElement()) {
      return;
    }
    this.applyStyleToSelectedElement('border-width', '0px');
    this.applyStyleToSelectedElement('border-color', '#ffffff');
    this.applyStyleToSelectedElement('border-style', 'none');
    element.style.backgroundColor = color.toString();
  };

  handleNavigateBorderColorClear = (color: string, element: HTMLElement): void => {
    if (!this.getSelectedElement()) {
      return;
    }
    this.applyStyleToSelectedElement('border-color', '#ffffff');
    this.getBorderStyleOfContainer(element);
    this.handleNavigateBorderStyleChange();
    element.style.backgroundColor = color.toString();
  };

  attachNoOfLinks = (wrapper: HTMLElement): void => {
    const rowOptionWrapper = createDiv('div', 'accordionItem');
    rowOptionWrapper.className = 'tableRowOptionWrapper';
    const title = createDiv('div', 'accordionItem');
    title.className = 'tableRowOption title';
    title.innerHTML = `${translate('element.linksnum')}`;
    rowOptionWrapper.appendChild(title);
    const linkInput = document.createElement('select');
    let linkRange = 3;
    while (linkRange < 11) {
      const input = document.createElement('option');
      input.text = '' + linkRange;
      input.value = '' + linkRange;
      linkInput.add(input, linkInput[linkRange - 3]);
      linkRange++;
    }
    linkInput.value = this.getSelectedMenuRows();
    linkInput.onchange = this.handleNavigateRowOnChange;
    linkInput.id = 'menuLinkNo';
    linkInput.className = 'tableRowOption input form-control-design menuLinkNumber';
    rowOptionWrapper.appendChild(linkInput);
    wrapper.appendChild(rowOptionWrapper);
  };

  getSelectedMenuRows = (): string => {
    let value = '3';
    const selectedElement = this.getSelectedElement()?.getElementsByTagName('ul')[0];
    const childrenCounts = selectedElement ? this.countElements(selectedElement) : '3';
    value = '' + childrenCounts;
    return value;
  };

  handleNavigateRowOnChange = (e: Event): void => {
    e.preventDefault();
    const element = e.target as HTMLInputElement;
    if (element) {
      const value = parseInt(element.value);
      const selectedElement = this.getSelectedElement()?.getElementsByTagName('ul')[0];
      const totalChildElement = selectedElement ? this.countElements(selectedElement) : 0;
      if (totalChildElement < value) {
        this.addMenuItem(value, totalChildElement, selectedElement);
      } else if (totalChildElement >= value) {
        this.confirmToDelete(value, totalChildElement, selectedElement);
      }
    }
  };

  countElements = (selectedHtmlElement: HTMLElement) => {
    const children = selectedHtmlElement?.childNodes;
    let count = 0;
    for (let i = 0, m = children.length; i < m; i++) {
      if (children[i].nodeType === document.ELEMENT_NODE) {
        count++;
      }
    }
    return count;
  };

  addMenuItem = (value: number, totalChildElement: number, selectedElement: any) => {
    const addingChild = value - totalChildElement;
    for (let j = 0; j < addingChild; j++) {
      const newElement = document.createElement('li');
      newElement.className =
        CONSTANTS.LOREE_IFRAME_CONTENT_MENU + ' nav-item ' + CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
      newElement.setAttribute('style', 'padding: 0px;');
      newElement.innerHTML = `<p class="${CONSTANTS.LOREE_IFRAME_NAVIGATE_ITEMS} ${
        CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
      }" style="padding: 12px 20px; margin: 0px; border-width: 0; border-style: solid; border-color: #FFFFFF;" contenteditable="true" lang= ${
        getUserLanguageStore().selectedMiniMenuLanguage !== ''
          ? getUserLanguageStore().selectedMiniMenuLanguage
          : getCurrentI18nLanguage()
      }> ${translate('element.menuitem')}</p>`;
      const newMenuElement = this.attachCustomizeStyleToElement(newElement);
      selectedElement?.appendChild(newMenuElement);
      this.removeSelectedElementSection();
      this.hideSubSidebar();
    }
  };

  confirmToDelete = (value: number, totalChildElement: any, selectedElement: any) => {
    navigationRemoveAlert();
    const deleteMenuYesButton = document.getElementById('deleteMenuProps');
    const deleteMenuNoButton = document.getElementById('deleteMenuPropsStop');
    if (deleteMenuYesButton) {
      deleteMenuYesButton.onclick = (): void =>
        this.removeMenuItem(value, totalChildElement, selectedElement);
    }
    if (deleteMenuNoButton) {
      deleteMenuNoButton.onclick = (): void => this.revertMenuItem(totalChildElement);
    }
  };

  removeMenuItem = (value: number, totalChildElement: number, selectedElement: any) => {
    const removeChild = totalChildElement - value;
    for (let j = 1; j <= removeChild; j++) {
      const lastNode = selectedElement?.lastChild;
      if (lastNode) selectedElement?.removeChild(lastNode);
    }
    closeMenuAlert();
  };

  revertMenuItem = (totalChildElement: any) => {
    const linkNoUpdate = document.getElementById('menuLinkNo') as HTMLInputElement;
    linkNoUpdate.value = totalChildElement;
    closeMenuAlert();
  };

  attachAlignmentOption = (wrapper: HTMLElement): void => {
    const navigateWrapper = createDiv('div', 'accordionItem');
    navigateWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#nav-design-section-alignment`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.alignment')}</span>
    `;
    navigateWrapper.appendChild(accordionButton);
    const accordionContent = createDiv('div', 'accordionItem');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'nav-design-section-alignment';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER}`;

    this.createAlignmentElement(accordionContent, ICONS.LEFT_ALIGNMENT_ICON, 'start');
    this.createAlignmentElement(accordionContent, ICONS.CENTER_ALIGNMENT_ICON, 'center');
    this.createAlignmentElement(accordionContent, ICONS.RIGHT_ALIGNMENT_ICON, 'end');

    navigateWrapper.appendChild(accordionContent);
    wrapper.appendChild(navigateWrapper);
    this.highlightSelectedNavbarAlignment(this.getSelectedElement());
  };

  getAlignmentControlForElement(
    element: HTMLUListElement,
  ): [HTMLButtonElement, HTMLButtonElement, HTMLButtonElement] {
    return [
      getEditorElementsByClassName('navAlignmentButton')[0] as HTMLButtonElement,
      getEditorElementsByClassName('navAlignmentButton')[1] as HTMLButtonElement,
      getEditorElementsByClassName('navAlignmentButton')[2] as HTMLButtonElement,
    ];
  }

  handleHighlightToAppliedAlignment = (
    navElement: HTMLUListElement,
    element: HTMLButtonElement,
  ) => {
    const alignmentButtons = this.getAlignmentControlForElement(navElement);
    Array.from(alignmentButtons).forEach((button) => {
      if (button === element) {
        setColorNavAlignment(button, 'blue');
      } else {
        setColorNavAlignment(button, '');
      }
    });
  };

  applyAlignmentToMenu = (position: string): void => {
    const navElement: HTMLUListElement | null | undefined =
      this.getSelectedElement()?.getElementsByTagName('ul')[0];
    if (!navElement) return;
    const navElementStyle = navElement.style;
    const [alignLeft, alignCenter, alignRight] = this.getAlignmentControlForElement(navElement);

    let leftValue = '';
    let rightValue = '';
    let contentJustify = '';

    switch (position) {
      case 'start':
        this.handleHighlightToAppliedAlignment(navElement, alignLeft);
        leftValue = '0px';
        rightValue = 'auto';
        contentJustify = 'flex-start';
        break;
      case 'center':
        this.handleHighlightToAppliedAlignment(navElement, alignCenter);
        leftValue = 'auto';
        rightValue = 'auto';
        contentJustify = 'center';
        break;
      case 'end':
        this.handleHighlightToAppliedAlignment(navElement, alignRight);
        leftValue = 'auto';
        rightValue = '0px';
        contentJustify = 'flex-end';
        break;
    }
    navElementStyle.setProperty('margin-left', leftValue);
    navElementStyle.setProperty('margin-right', rightValue);
    navElementStyle.setProperty('justify-content', contentJustify);
  };

  highlightSelectedNavbarAlignment = (selectedElement: HTMLElement | null) => {
    const navElement: HTMLUListElement | null | undefined =
      selectedElement?.getElementsByTagName('ul')[0];

    if (!navElement) return;

    const [alignLeft, alignCenter, alignRight] = this.getAlignmentControlForElement(navElement);

    if (!alignLeft || !alignRight || !alignCenter) {
      return;
    }

    switch (navElement.style.justifyContent) {
      case 'flex-start':
        setColorNavAlignment(alignLeft, 'blue');
        break;
      case 'center':
        setColorNavAlignment(alignCenter, 'blue');
        break;
      case 'flex-end':
        setColorNavAlignment(alignRight, 'blue');
        break;
    }
  };

  createAlignmentElement = (
    accordionContent: HTMLElement,
    content: string,
    alignmentType: string,
  ): void => {
    const alignIcon = document.createElement('button');
    alignIcon.className = 'navAlignmentButton';
    alignIcon.innerHTML = content;
    alignIcon.onclick = (): void => this.applyAlignmentToMenu(alignmentType);
    accordionContent.appendChild(alignIcon);
  };

  handleAccordionButtonClick = (e: Event): void => {
    const wrapper = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (wrapper) {
      const element = e.target as HTMLElement;
      let label = '';
      if (element.tagName === 'SPAN') {
        const parent = element.parentElement;
        if (parent) {
          const icon = parent.getElementsByClassName('icon')[0];
          label = parent.getElementsByClassName('label')[0].innerHTML.trim();
          if (icon) {
            if (icon.innerHTML === '+') {
              icon.innerHTML = 'I';
            } else {
              icon.innerHTML = '+';
            }
          }
        }
      } else {
        const icon = element.getElementsByClassName('icon')[0];
        label = element.getElementsByClassName('label')[0].innerHTML.trim();
        if (icon) {
          if (icon.innerHTML === '+') {
            icon.innerHTML = 'I';
          } else {
            icon.innerHTML = '+';
          }
        }
      }
      const accordionButton = wrapper.getElementsByClassName('accordionButton');
      Array.from(accordionButton).forEach((button) => {
        const icon = button.getElementsByClassName('icon')[0];
        const buttonLabel = button.getElementsByClassName('label')[0].innerHTML.trim();
        if (icon) {
          if (buttonLabel !== label) icon.innerHTML = '+';
        }
      });
    }
  };

  attachSpaceOption = (wrapper: HTMLElement): void => {
    const navigateWrapper = createDiv('div', 'accordionItem');
    navigateWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-space`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('design.space')}</span>
    `;
    navigateWrapper.appendChild(accordionButton);

    const accordionContent = createDiv('div', 'accordionItem');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-space';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER}`;

    const tablePaddingSpaceWrapper = createDiv('div', 'accordionItem');
    tablePaddingSpaceWrapper.className = 'tablePaddingSpaceWrapper';
    const paddingLabel = createDiv('div', 'accordionItem');
    paddingLabel.className =
      'label d-flex justify-content-between mb-2 editor-design-margin-heading';
    paddingLabel.innerHTML = `<b>${translate('design.padding')}</b><span><i>${translate(
      'design.pixelsuffix',
    )}</i></span>`;
    tablePaddingSpaceWrapper.appendChild(paddingLabel);
    const paddingOptions = createDiv('div', 'accordionItem');
    paddingOptions.className = 'options';

    this.paddingOption(paddingOptions, 'leftOptionPadding', 'left');
    this.paddingOption(paddingOptions, 'topOptionPadding', 'top');
    this.paddingOption(paddingOptions, 'rightOptionPadding', 'right');
    this.paddingOption(paddingOptions, 'bottomOptionPadding', 'bottom');
    tablePaddingSpaceWrapper.appendChild(paddingOptions);

    accordionContent.appendChild(tablePaddingSpaceWrapper);
    navigateWrapper.appendChild(accordionContent);
    wrapper.appendChild(navigateWrapper);
  };

  paddingOption = (wrapper: HTMLElement, className: string, paddingType: string): void => {
    const leftOptionMargin = createDiv('div', 'accordionItem');
    leftOptionMargin.className = 'option';
    const leftOptionMarginChild = createDiv('div', 'accordionItem');
    leftOptionMarginChild.className = className;
    const leftOptionMarginInput = createinputElement('input');
    leftOptionMarginInput.type = 'number';
    leftOptionMarginInput.min = '0';
    leftOptionMarginInput.className = 'form-control-design';
    leftOptionMarginInput.value = this.getMenuPadding(paddingType);
    leftOptionMarginInput.onchange = (e: Event): void =>
      this.handleMenuPaddingChange(e, paddingType);
    leftOptionMarginChild.appendChild(leftOptionMarginInput);
    leftOptionMargin.appendChild(leftOptionMarginChild);
    wrapper.appendChild(leftOptionMargin);
  };

  getMenuPadding = (position: string): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    switch (position) {
      case 'top':
        value = getComputedStyle(selectedElement).paddingTop.replace('px', '');
        break;
      case 'bottom':
        value = getComputedStyle(selectedElement).paddingBottom.replace('px', '');
        break;
      case 'left':
        value = getComputedStyle(selectedElement).paddingLeft.replace('px', '');
        break;
      case 'right':
        value = getComputedStyle(selectedElement).paddingRight.replace('px', '');
        break;
    }
    if (!value) value = '0';
    return value;
  };

  handleMenuPaddingChange = (e: Event, position: string): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (selectedElements.length < 2) {
      switch (position) {
        case 'top':
          this.applyStyleToSelectedElement('padding-top', `${element.value}px`);
          break;
        case 'bottom':
          this.applyStyleToSelectedElement('padding-bottom', `${element.value}px`);
          break;
        case 'left':
          this.applyStyleToSelectedElement('padding-left', `${element.value}px`);
          break;
        case 'right':
          this.applyStyleToSelectedElement('padding-right', `${element.value}px`);
          break;
      }
    } else {
      selectedElements.forEach((tdElement) => {
        switch (position) {
          case 'top':
            tdElement.style.paddingTop = `${element.value}px`;
            break;
          case 'bottom':
            tdElement.style.paddingBottom = `${element.value}px`;
            break;
          case 'left':
            tdElement.style.paddingLeft = `${element.value}px`;
            break;
          case 'right':
            tdElement.style.paddingRight = `${element.value}px`;
            break;
        }
      });
    }
  };

  attachColorOption = (wrapper: HTMLElement): void => {
    const navigateWrapper = createDiv('div', 'accordionItem');
    navigateWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#menu-design-section-color`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.color')}</span>
    `;
    navigateWrapper.appendChild(accordionButton);

    const accordionContent = createDiv('div', 'accordionItem');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'menu-design-section-color';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_MENU_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const colorOption = createDiv('div', 'accordionItem');
    colorOption.className = 'colorOption';

    const backgroundColorOption = createDiv('div', 'accordionItem');
    backgroundColorOption.className = 'backgroundColorOption';

    const label = createDiv('div', 'accordionItem');
    label.className = 'label';
    label.innerHTML = `${translate('design.background')}`;
    backgroundColorOption.appendChild(label);

    const selectedElement = this.getSelectedElement();
    const color = document.createElement('button');
    color.className = 'color';
    color.style.backgroundColor = this.getBackgroundColorOfContainer(selectedElement);
    backgroundColorOption.appendChild(color);
    colorOption.appendChild(backgroundColorOption);
    this.attachBackgroundColorPicker(color);
    accordionContent.appendChild(colorOption);
    navigateWrapper.appendChild(accordionContent);
    wrapper.appendChild(navigateWrapper);
    this.fixColorPickerBehaviour();
  };

  getBackgroundColorOfContainer = (selectedElement: HTMLElement | null): string => {
    let value: string | undefined = '#ffffff';
    if (!selectedElement) return value;
    const backgroundColor = selectedElement.style.backgroundColor;
    if (backgroundColor) {
      return backgroundColor;
    }
    const columnParentElement: HTMLElement | null = selectedElement.parentElement;
    const rowParentElement = columnParentElement?.parentElement;
    if (columnParentElement?.style.backgroundColor !== '') {
      value = columnParentElement?.style.backgroundColor;
    } else if (rowParentElement?.style.backgroundColor !== '') {
      value = rowParentElement?.style.backgroundColor;
    }
    return value as string;
  };

  attachBackgroundColorPicker = (element: HTMLElement): void => {
    const defaultColor = this.getBackgroundColorOfContainer(element);
    let ClickCancelColor = false;
    if (element) {
      const options: ColorPickerInterface = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS,
        default: defaultColor,
        defaultRepresentation: 'HEXA',
        useAsButton: true,
        lockOpacity: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            hex: true,
            rgba: false,
            input: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      backgroundColorPicker = Pickr.create(options);
      backgroundColorPicker.on('change', (color: { toHEXA: Function }) => {
        const hexValue = color.toHEXA();
        this.handleNavigateBackgroundColorChange(hexValue, element);
      });

      backgroundColorPicker.on('save', (color: { toHEXA: Function }) => {
        if (color) {
          const hexValue = color.toHEXA();
          this.handleNavigateBackgroundColorChange(hexValue, element);
          backgroundColorPicker?.hide();
          lastsavedColor = hexValue;
          ClickCancelColor = true;
        }
      });

      backgroundColorPicker.on('cancel', () => {
        if (lastsavedColor && ClickCancelColor) {
          this.handleNavigateBackgroundColorChange(lastsavedColor, element);
        } else {
          this.handleNavigateBackgroundColorCancel(defaultColor, element);
        }
        backgroundColorPicker?.hide();
      });

      backgroundColorPicker.on('clear', () => {
        this.handleNavigateBackgroundColorCancel(defaultColor, element);
        backgroundColorPicker?.hide();
        ClickCancelColor = false;
      });
    }
  };

  handleNavigateBackgroundColorChange = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedElement('background-color', color.toString());
    element.style.backgroundColor = color;
  };

  handleNavigateBackgroundColorCancel = (color: string, element: HTMLElement): void => {
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;
    this.applyStyleToSelectedElement('background-color', color.toString());
    element.style.backgroundColor = color;
  };

  fixColorPickerBehaviour = (): void => {
    const pcrSelections = document.querySelectorAll('.pcr-selection');
    const pcrResult = document.querySelectorAll('.pcr-result');
    pcrSelections.forEach((selection) => {
      selection.addEventListener('click', () => {
        pcrResult.forEach((input) => {
          const inputField = input as HTMLInputElement;
          inputField.blur();
        });
      });
    });
  };

  attachGutterOption = (wrapper: HTMLElement): void => {
    const gutterWrapper = createDiv('div', 'accordionItem');
    gutterWrapper.className = 'tableRowOptionWrapper';
    gutterWrapper.setAttribute('style', 'margin-top: 15px;');
    const title = document.createElement('b');
    title.className = 'tableRowOption title';
    title.innerHTML = `${translate('design.gutter')}`;
    gutterWrapper.appendChild(title);
    const RangeInput = document.createElement('select');
    let rangeCount = 0;
    while (rangeCount < 6) {
      const input = document.createElement('option');
      input.text = '' + rangeCount;
      input.value = '' + rangeCount;
      RangeInput.add(input, RangeInput[rangeCount]);
      rangeCount++;
    }
    RangeInput.value = this.getSelectedGutterValue();
    RangeInput.onchange = this.handleTextGutterChange;
    RangeInput.id = 'gutterOption';
    RangeInput.className = 'tableRowOption input form-control-design menuLinkNumber';
    gutterWrapper.appendChild(RangeInput);
    wrapper.appendChild(gutterWrapper);

    const gutterDivContent = createDiv('div', 'accordionItem');
    gutterDivContent.className = 'gutterContent';
    const sliderInput = createinputElement('input');
    sliderInput.type = 'range';
    sliderInput.setAttribute('min', '0');
    sliderInput.setAttribute('max', '5');
    sliderInput.setAttribute('value', this.getSelectedGutterValue());
    sliderInput.id = 'gutterSlider';
    sliderInput.className = 'gutterSlider';
    sliderInput.onchange = this.handleRangeGutterChange;
    gutterDivContent.appendChild(sliderInput);
    wrapper.appendChild(gutterDivContent);
  };

  getSelectedGutterValue = (): string => {
    let gutterRange = '0';
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      const selectedChild = selectedElement?.children[0] as HTMLElement;
      const gutterValue: string = selectedChild.style.marginLeft;
      gutterRange = gutterValue !== '' ? gutterValue.replace('px', '') : '0';
    }
    return gutterRange;
  };

  handleTextGutterChange = (e: Event): any => {
    e.preventDefault();
    const element = e.target as HTMLInputElement;
    if (element) {
      const value = parseInt(element.value);
      this.addGutter(value);
      const gutterSlider = document.getElementById('gutterSlider');
      gutterSlider?.setAttribute('value', element.value);
    }
  };

  handleRangeGutterChange = (e: Event): any => {
    e.preventDefault();
    const element = e.target as HTMLInputElement;
    if (element) {
      const value = parseInt(element.value);
      this.addGutter(value);
      const gutterText = document.getElementById('gutterOption');
      gutterText?.setAttribute('value', element.value);
    }
  };

  addGutter = (value: number): any => {
    const selectedElement = this.getSelectedElement();
    if (selectedElement) {
      const totalChildElement = selectedElement ? this.countElements(selectedElement) : 0;
      for (let child = 0; child < totalChildElement; child++) {
        const selectedChild = selectedElement?.children[child] as HTMLElement;
        selectedChild.style.setProperty('margin-left', value + 'px');
        selectedChild.style.setProperty('margin-right', value + 'px');
      }
    }
  };
}
export default Navigate;
