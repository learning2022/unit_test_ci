import Base from '../../base';
import CONSTANTS from '../../constant';
import ROWS from '../../row';
import { RowsInterface } from '../../interface';
import {
  categoryWrapper,
  handleBlockCategoryApplyButton,
  preventDropdownClosing,
  rowsBlock,
  retrieveGlobalCustomBlockRow,
  fetchCategory,
  refreshCategoryDropdown,
  updateCustomBlockThumbnail,
  retrieveCustomBlockRow,
} from '../customBlocks/customBlockEvents';
import { customBlockSearchWrapper } from '../customBlocks/customBlocksUI';
import { videoModalIcons } from '../../modules/sidebar/iconHolder';
import { isCanvasAndBB } from '../../../lmsConfig';
import { translate } from '../../../i18n/translate';
import { ensure } from '../../../utils/generalUtils';
import { createDiv, getElementById } from '../../common/dom';
import { getEditorElementById } from '../../utils';
import { setCustomBlockEditOptions } from '../customBlocks/customBlockHandler';
class Row extends Base {
  selectedSidebarRow: string | null;

  constructor() {
    super();
    this.selectedSidebarRow = null;
  }

  initiate = (config: _Any): void => {
    const sidebarContentWrapper = document.getElementById(CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER);
    if (!sidebarContentWrapper) return;
    // Wrapper
    const rowSection = document.createElement('div');
    rowSection.id = CONSTANTS.LOREE_SIDEBAR_ROW_SECTION;
    rowSection.className = 'section';
    rowSection.style.display = 'none';
    this.attachLabel(rowSection);
    this.attachContent(rowSection, config.features);
    sidebarContentWrapper.appendChild(rowSection);
  };

  attachLabel = (rowSection: HTMLElement): void => {
    // Label
    const sectionLabel = document.createElement('button');
    sectionLabel.className = 'sectionLabel ';
    sectionLabel.id = CONSTANTS.LOREE_SIDEBAR_ROW_SECTION_BUTTON;
    sectionLabel.dataset.toggle = 'collapse';
    sectionLabel.dataset.target = `#${CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE}`;
    sectionLabel.setAttribute('aria-expanded', 'false');
    sectionLabel.innerHTML = `
      <span class="label">${translate('global.row')}</span>
      <span class="image">
        <svg viewBox="0 0 8 14">
          <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
        </svg>
      </span>
    `;
    rowSection.appendChild(sectionLabel);
  };

  attachContent = (rowSection: HTMLElement, featuresList: _Any): void => {
    // Content
    const sectionContent = document.createElement('div');
    sectionContent.className = 'sectionContent collapse show';
    sectionContent.id = CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE;
    sectionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER}`;
    sectionContent.innerHTML = '';
    ROWS.forEach((row): void => {
      if (
        (row.id === 'customRows' && featuresList.customrowfiltersearch) ||
        row.id !== 'customRows'
      ) {
        const sidebarRows = document.createElement('div');
        sidebarRows.className = 'sidebarRows';
        sidebarRows.innerHTML = row.template;
        sidebarRows.id = row.id;
        if (!row.multiple && row.id !== 'customRows') {
          sidebarRows.onclick = (): void =>
            this.handleSingleRowItem(ensure<string>(row.htmlString));
        }
        if (row.multiple && row.id !== 'customRows') {
          sidebarRows.onclick = (): void => this.handleMultipleRowItems(row);
        }
        if (row.id === 'customRows')
          sidebarRows.onclick = async () => await this.handleCustomRows();
        sectionContent.appendChild(sidebarRows);
      }
    });
    rowSection.appendChild(sectionContent);
  };

  appendCustomRow = async (rowSection: HTMLElement) => {
    !isCanvasAndBB() ? await retrieveCustomBlockRow() : await retrieveGlobalCustomBlockRow();
    await fetchCategory();
    const sidebarRowsLoader = document.getElementsByClassName('sidebarRowsLoader')[0];
    if (!sidebarRowsLoader) return;

    const customRowInputWrapper = document.createElement('div');
    customRowInputWrapper.id = CONSTANTS.LOREE_SIDEBAR_CUSTOM_ROW_INPUT_WRAPPER;
    customRowInputWrapper.appendChild(customBlockSearchWrapper('row'));
    customRowInputWrapper.innerHTML += categoryWrapper('row');
    customRowInputWrapper.appendChild(rowsBlock());
    sidebarRowsLoader?.remove();
    rowSection?.childNodes[1]?.appendChild(customRowInputWrapper);
    setCustomBlockEditOptions('My Row');
  };

  handleCustomRows = async () => {
    const row = getEditorElementById(CONSTANTS.LOREE_SIDEBAR_ROW_COLLAPSE);
    row?.childNodes.forEach((child) => {
      if ((child as HTMLElement).id !== 'customRows') {
        (child as HTMLElement).classList.toggle('d-none');
      }
    });
    this.hideCustomRowSection();
    if (!getElementById('oneColumn')?.classList.contains('d-none')) return;
    const sidebar: _Any = getEditorElementById('loree-sidebar');
    const loaderElement = createDiv('div');
    loaderElement.id = 'modal-loader';
    loaderElement.className = 'sidebarRowsLoader m-auto justify-content-center';
    loaderElement.innerHTML = `<div class="icon rotating">
      ${videoModalIcons.loader}
      </div>
      <div class="title ml-3">${translate('global.loading')}</div>`;
    sidebar.appendChild(loaderElement);
    const rowSection: _Any = getEditorElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION);
    await this.appendCustomRow(rowSection);
    this.refreshCustomBlockSearch('row');
    this.attachCustomRowEvents();
  };

  attachCustomRowEvents = () => {
    const dropDownApplyBtn = document.getElementById(
      'loree-sidebar-custom-row-category-button-apply',
    );
    if (dropDownApplyBtn) {
      dropDownApplyBtn.onclick = () => handleBlockCategoryApplyButton('row');
    }
    const dropdownList = document.getElementById('loree-sidebar-custom-row-category-dropdown-menu');
    if (dropdownList) {
      dropdownList.onclick = (e) => preventDropdownClosing('row', e);
    }
    const dropdownContainer = document.getElementById('loree-sidebar-custom-row-category-dropdown');
    if (dropdownContainer) {
      dropdownContainer.onclick = () => refreshCategoryDropdown('row');
    }
    const searchBox = document.getElementById('loree-sidebar-custom-row-search-input-value');
    if (searchBox) searchBox.oninput = () => updateCustomBlockThumbnail('row');
  };

  handleMultipleRowItems = (row: RowsInterface): void => {
    const subSidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SUB_SIDEBAR);
    if (this.selectedSidebarRow) {
      this.removeActiveFromSelectedRow(this.selectedSidebarRow);
      this.addActiveToSelectedRow(row.id);
    } else {
      this.selectedSidebarRow = row.id;
      this.addActiveToSelectedRow(row.id);
    }
    if (subSidebar) {
      subSidebar.innerHTML = '';
      const subSidebarRowsWrapper = document.createElement('div');
      subSidebarRowsWrapper.className = 'subSidebarRowsWrapper';
      ensure(row.contents).forEach((content) => {
        const subSidebarRows = document.createElement('div');
        subSidebarRows.className = 'subSidebarRows';
        subSidebarRows.innerHTML = content.template;
        subSidebarRows.onclick = (): void => this.handleSingleRowItem(content.htmlString);
        subSidebarRowsWrapper.appendChild(subSidebarRows);
      });
      subSidebar.appendChild(subSidebarRowsWrapper);
      this.showSubSidebar();
    }
  };

  addActiveToSelectedRow = (id: string): void => {
    const addActiveRow: Array<HTMLElement> | HTMLElement | null = document.getElementById(id);
    if (addActiveRow) {
      addActiveRow.classList.add('active');
    }
  };

  removeActiveFromSelectedRow = (id: string): void => {
    const removeActiveRow = document.getElementById(id);
    removeActiveRow?.classList.remove('active');
  };

  handleSingleRowItem = (innerHTML: string): void => {
    this.appendContentToIframeContentWrapper(innerHTML);
    this.collapseRowSection();
    this.disableRowSection();
    this.hideSubSidebar();
    this.showAddRowButton();
    this.hideCloseRowButton();
    this.removeSelectedRowSection();
  };
}

export default Row;
