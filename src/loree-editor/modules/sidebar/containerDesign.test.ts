import ContainerDesign from './containerDesign';
import CONSTANTS from '../../constant';
import Base from '../../base';
import BlockOptions from '../blockOptions/blockOptions';
import { createDiv, getElementById, getElementsByClassName } from '../../common/dom';
import { getElementByClassName } from '../../utils';
import { hex2rgb, mockColorPickerInstance } from '../colorPicker/utils';
import Pickr from '@simonwep/pickr';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

const containerMockData = `<div style="loree-iframe-content-container-wrapper element-hover-highlight coloumn-minHeight element-highlight"><div class="loree-iframe-content-container" style="width:100%;"></div></div>`;

describe('#Container Design section', () => {
  const designInstance = new ContainerDesign();
  const baseInstance = new Base();
  const blockOptions = new BlockOptions();
  let containerBgColorPicker: Pickr;
  let containerBackgroundPickerWrapper: HTMLElement;
  let containerBgButton: HTMLButtonElement;
  let applyButton: HTMLElement;
  let cancelButton: HTMLElement;
  let clearButton: HTMLElement;
  afterAll(() => {
    containerBgColorPicker.destroy();
  });
  test('should render with collapse', () => {
    designInstance.attachLabel(document.body);
    const element = document.getElementsByTagName('button')[0];
    expect(element.childElementCount).toBe(2);
    expect(element.childNodes[1].textContent).toBe('global.design');
  });
  test('attach container design section', () => {
    const container = createDiv();
    container.innerHTML = `<div style="display:flex; margin:0px; padding: 5px;" class="loree-iframe-content-container-wrapper coloumn-minHeight element-highlight element-hover-highlight"><div class="loree-iframe-content-container" style="width:100%;"></div></div>`;
    designInstance.getSelectedElement = jest
      .fn()
      .mockImplementation(() => container.childNodes[0] as HTMLElement);
    document.body.innerHTML = `<div id=${CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER}></div>`;
    designInstance.initiate({});
    designInstance.attachContainerOptions();
    expect(
      getElementById('loree-sidebar-container-design-section-accordion-wrapper')?.children,
    ).toHaveLength(5);
  });
  test('delete container', () => {
    document.body.innerHTML += `<div id=${CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER}></div>`;
    blockOptions.appendDeleteOption(getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER));
    const containerDesign = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);

    baseInstance.showContainerDesignSection();
    expect(containerDesign?.style.display).toEqual('block');
    document.getElementById(CONSTANTS.LOREE_BLOCK_OPTIONS_DELETE_BUTTON)?.click();
    expect(containerDesign?.style.display).toEqual('none');
  });
  test('background color picker button exists', () => {
    containerBgButton = getElementById(
      CONSTANTS.LOREE_CONTAINER_BACKGROUND_COLOR_BUTTON,
    ) as HTMLButtonElement;
    expect(containerBgButton).toBeInTheDocument();
  });
  test('color picker render on button click', () => {
    containerBgColorPicker = mockColorPickerInstance(
      containerBgButton,
      `${CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION_COLOR_PICKERS} container-background-picker`,
      '#FFFFFF',
    );
    containerBgButton.click();
    containerBackgroundPickerWrapper = getElementsByClassName(
      'container-background-picker',
    )[0] as HTMLElement;
    expect(containerBgColorPicker).toBeInstanceOf(Pickr);
    expect(containerBackgroundPickerWrapper).toBeInTheDocument();
  });
  test('default color is not undefined', () => {
    expect(designInstance.getContainerBackgroundColor()).toBe('#ffffff');
  });
  test('color pciker button events', () => {
    designInstance.getSelectedElement = jest.fn().mockImplementation(() => containerMockData);
    designInstance.applyStyleToSelectedElement = jest.fn();
    clearButton = getElementByClassName(containerBackgroundPickerWrapper, 'pcr-clear');
    clearButton.click = jest
      .fn()
      .mockImplementation(() => designInstance.setContainerBackgroundColor('#ffffff'));
    applyButton = getElementByClassName(containerBackgroundPickerWrapper, 'pcr-save');
    applyButton.click = jest
      .fn()
      .mockImplementation(() => designInstance.setContainerBackgroundColor('#ff0000'));
    applyButton.click();
    expect(designInstance.getContainerBackgroundColor()).toBe('#ff0000');
    cancelButton = getElementByClassName(containerBackgroundPickerWrapper, 'pcr-cancel');
    containerBgColorPicker.on('cancel', () => {
      designInstance.handleContainerBackgroundColorChange(
        designInstance.getContainerBackgroundColor(),
        containerBgButton,
      );
      expect(designInstance.applyStyleToSelectedElement).toHaveBeenCalledWith(
        'background-color',
        designInstance.getContainerBackgroundColor(),
      );
    });
    cancelButton.click();
    expect(containerBgButton.style.backgroundColor).toBe(
      hex2rgb(designInstance.getContainerBackgroundColor()),
    );
  });
});
