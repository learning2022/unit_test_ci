import { getElementById } from '../../common/dom';
import CONSTANTS from '../../constant';

export const getSelectedImageElementDetails = (selectedElement: HTMLImageElement): void => {
  const designSectionImageName = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_SELECTED_IMAGE_NAME,
  ) as HTMLInputElement;
  if (designSectionImageName) {
    designSectionImageName.value = selectedElement.alt;
  }
};

export const getSelectedVideoElementDetails = (selectedElement: HTMLElement): void => {
  // setting video name
  const designSectionImageName = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_SELECTED_IMAGE_NAME,
  ) as HTMLInputElement;
  if (designSectionImageName) {
    designSectionImageName.value = selectedElement.title;
  }
};

export const getSelectedImageElementStyle = (selectedElement: HTMLElement): void => {
  const selectedElementParent = selectedElement.parentElement as HTMLElement;
  if (selectedElementParent) {
    checkFullWidth(selectedElement);
    getSpaceValues(selectedElementParent);
    getAlignmentValues(selectedElementParent);
    selectedElementParent.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)
      ? getImageSizeValuesForSpecialElement(selectedElement as HTMLImageElement)
      : getImageSizeValues(selectedElement as HTMLImageElement);
    getBorderValues(selectedElement);
  }
};

export const getSelectedLineElementStyle = (selectedElement: HTMLElement): void => {
  const selectedElementParent = selectedElement.parentElement as HTMLElement;
  if (selectedElementParent) {
    checkFullWidth(selectedElement);
    getSpaceValues(selectedElementParent);
    getAlignmentValues(selectedElementParent);
    getLineStyleValues(selectedElement);
  }
};

export const getSelectedVideoElementStyle = (selectedElement: HTMLElement): void => {
  const videoSuperParent = selectedElement.parentElement?.parentElement
    ?.parentElement as HTMLElement;
  const videoParent = selectedElement.parentElement?.parentElement as HTMLElement;
  if (videoSuperParent) {
    checkFullWidth(videoParent);
    getSpaceValues(videoSuperParent);
    getAlignmentValues(videoSuperParent);
    getSizeValues(videoParent);
    getBorderValues(videoParent);
  }
};

export const getSelectedSpaceElementStyle = (selectedElement: HTMLElement): void => {
  const selectedElementParent = selectedElement.parentElement as HTMLElement;
  if (selectedElementParent) {
    checkFullWidth(selectedElement);
    getSpaceValues(selectedElementParent);
    getAlignmentValues(selectedElementParent);
    getSizeValues(selectedElement);
  }
};

export const getSelectedElementStyle = (selectedElement: HTMLElement): void => {
  if (selectedElement) {
    getSpaceValues(selectedElement);
    getBorderValues(selectedElement);
  }
};

const checkFullWidth = (selectedElement: HTMLElement): void => {
  const fullWidthCheckBox = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_FULL_WIDTH,
  ) as HTMLInputElement;
  const widthInputBox = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
  ) as HTMLInputElement;
  if (!fullWidthCheckBox) return;
  if (selectedElement.style.width === '100%') {
    fullWidthCheckBox.checked = true;
    widthInputBox.disabled = true;
  } else {
    fullWidthCheckBox.checked = false;
    widthInputBox.disabled = false;
  }
};

export const getAlignmentValues = (element: HTMLElement): void => {
  const alignmentValue = window.getComputedStyle(element).getPropertyValue('text-align');
  const alignCenter = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_CENTER_ALIGN_BUTTON,
  ) as HTMLElement;
  const alignLeft = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_LEFT_ALIGN_BUTTON,
  ) as HTMLElement;
  const alignRight = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_RIGHT_ALIGN_BUTTON,
  ) as HTMLElement;
  switch (alignmentValue) {
    case 'center':
      checkHasAlignment(alignCenter);
      break;
    case 'left':
      checkHasAlignment(alignLeft);
      break;
    case 'right':
      checkHasAlignment(alignRight);
      break;
  }
};

export const checkHasAlignment = (alignment: HTMLElement | null) => {
  if (alignment?.style) {
    alignment.style.stroke = 'blue';
  }
};

function getAlignmentControlsForElement(
  element: Element,
): [HTMLElement | null, HTMLElement | null, HTMLElement | null] {
  if (element.tagName === 'TABLE') {
    return [
      document.getElementById(CONSTANTS.LOREE_TABLE_DESIGN_CENTER_ALIGNMENT),
      document.getElementById(CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT),
      document.getElementById(CONSTANTS.LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT),
    ];
  }
  return [
    document.getElementById(CONSTANTS.LOREE_CONTAINER_DESIGN_CENTER_ALIGNMENT),
    document.getElementById(CONSTANTS.LOREE_CONTAINER_DESIGN_LEFT_ALIGNMENT),
    document.getElementById(CONSTANTS.LOREE_CONTAINER_DESIGN_RIGHT_ALIGNMENT),
  ];
}

export const highlightSelectedContainerAlignment = (selectedElement: HTMLElement | null) => {
  const firstChild = selectedElement?.firstElementChild;
  if (!firstChild) {
    return;
  }

  const [alignCenter, alignLeft, alignRight] = getAlignmentControlsForElement(firstChild);

  if (!alignLeft || !alignRight || !alignCenter) {
    return;
  }

  const right = window.getComputedStyle(firstChild).marginRight.replace('px', '');
  const left = window.getComputedStyle(firstChild).marginLeft.replace('px', '');

  if (right === '0' && left === '0') {
    alignLeft.style.stroke = 'blue';
    return;
  }
  if (left !== '0' && right !== '0') {
    alignCenter.style.stroke = 'blue';
    return;
  }
  if (left !== '0') {
    alignRight.style.stroke = 'blue';
    return;
  }
  alignLeft.style.stroke = 'blue';
};

const getSpaceValues = (element: HTMLElement): void => {
  const paddingTopValue = window
    .getComputedStyle(element)
    .getPropertyValue('padding-top')
    .replace('px', '');
  const paddingBottomValue = window
    .getComputedStyle(element)
    .getPropertyValue('padding-bottom')
    .replace('px', '');
  const paddingLeftValue = window
    .getComputedStyle(element)
    .getPropertyValue('padding-left')
    .replace('px', '');
  const paddingRightValue = window
    .getComputedStyle(element)
    .getPropertyValue('padding-right')
    .replace('px', '');
  const marginTopValue = window
    .getComputedStyle(element)
    .getPropertyValue('margin-top')
    .replace('px', '');
  const marginBottomValue = window
    .getComputedStyle(element)
    .getPropertyValue('margin-bottom')
    .replace('px', '');
  const marginLeftValue = window
    .getComputedStyle(element)
    .getPropertyValue('margin-left')
    .replace('px', '');
  const marginRightValue = window
    .getComputedStyle(element)
    .getPropertyValue('margin-right')
    .replace('px', '');
  const paddingTopInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_TOP_PADDING_INPUT,
  ) as HTMLInputElement;
  const paddingBottomInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BOTTOM_PADDING_INPUT,
  ) as HTMLInputElement;
  const paddingLeftInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_LEFT_PADDING_INPUT,
  ) as HTMLInputElement;
  const paddingRightInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_RIGHT_PADDING_INPUT,
  ) as HTMLInputElement;
  const marginTopInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_TOP_MARGIN_INPUT,
  ) as HTMLInputElement;
  const marginBottomInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BOTTOM_MARGIN_INPUT,
  ) as HTMLInputElement;
  const marginLeftInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_LEFT_MARGIN_INPUT,
  ) as HTMLInputElement;
  const marginRightInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_RIGHT_MARGIN_INPUT,
  ) as HTMLInputElement;
  if (
    paddingTopInput &&
    paddingBottomInput &&
    paddingLeftValue &&
    paddingRightInput &&
    marginTopInput &&
    marginBottomInput &&
    marginLeftInput &&
    marginRightInput
  ) {
    paddingTopInput.value = paddingTopValue;
    paddingBottomInput.value = paddingBottomValue;
    paddingLeftInput.value = paddingLeftValue;
    paddingRightInput.value = paddingRightValue;
    marginTopInput.value = marginTopValue;
    marginBottomInput.value = marginBottomValue;
    marginLeftInput.value = marginLeftValue;
    marginRightInput.value = marginRightValue;
  }
};

const getSizeValues = (element: HTMLElement): void => {
  const widthValue = window.getComputedStyle(element).getPropertyValue('width').replace('px', '');
  const heightValue = window.getComputedStyle(element).getPropertyValue('height').replace('px', '');
  const widthInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
  ) as HTMLInputElement;
  const heightInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT,
  ) as HTMLInputElement;
  let parentWidthValue = widthValue;
  if (element.parentElement)
    parentWidthValue = window
      .getComputedStyle(element.parentElement)
      .getPropertyValue('width')
      .replace('px', '');
  if (widthValue && heightInput) {
    widthInput.value = widthValue;
    widthInput.max = parentWidthValue;
    heightInput.value = heightValue;
  }
};

const getImageSizeValues = (element: HTMLImageElement): void => {
  const imageElement = element;
  if (imageElement.complete) {
    getHeightAndWidthOfImage(imageElement);
  } else {
    imageElement.onload = (): void => getHeightAndWidthOfImage(imageElement);
  }
};

export const getImageSizeValuesForSpecialElement = (element: HTMLImageElement): void => {
  const imageElement = element;
  if (imageElement.complete) {
    getWidthOfSpecialElement(imageElement);
  } else {
    imageElement.onload = (): void => getWidthOfSpecialElement(imageElement);
  }
};

const getWidthOfSpecialElement = (imageElement: HTMLImageElement): void => {
  let widthValue;
  if (imageElement.style.width && imageElement.style.width !== '100%') {
    widthValue = imageElement.style.width.replace('px', '');
  } else {
    widthValue = getComputedValue(imageElement, 'width');
  }
  const heightValue = getComputedValue(imageElement, 'height');
  const widthInput = getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
  ) as HTMLInputElement;
  const heightInput = getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT,
  ) as HTMLInputElement;
  if (widthValue && heightInput) {
    widthInput.value = widthValue;
    heightInput.value = heightValue;
  }
};

const getHeightAndWidthOfImage = (imageElement: HTMLImageElement): void => {
  const widthValue = window
    .getComputedStyle(imageElement)
    .getPropertyValue('width')
    .replace('px', '');
  const heightValue = window
    .getComputedStyle(imageElement)
    .getPropertyValue('height')
    .replace('px', '');
  const widthInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
  ) as HTMLInputElement;
  const heightInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT,
  ) as HTMLInputElement;
  let parentWidthValue = widthValue;
  if (imageElement.parentElement)
    parentWidthValue = window
      .getComputedStyle(imageElement.parentElement)
      .getPropertyValue('width')
      .replace('px', '');
  if (widthValue && heightInput) {
    widthInput.value = widthValue;
    if (!imageElement.classList.contains(CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO)) {
      widthInput.max = parentWidthValue;
    }
    heightInput.value = heightValue;
  }
};

const getLineStyleValues = (element: HTMLElement): void => {
  const lineStyle = window.getComputedStyle(element).getPropertyValue('border-block-start-style');
  const widthValue = window.getComputedStyle(element).getPropertyValue('width').replace('px', '');
  const heightValue = window
    .getComputedStyle(element)
    .getPropertyValue('border-block-start-width')
    .replace('px', '');
  const widthInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_WIDTH_INPUT,
  ) as HTMLInputElement;
  const heightInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_SIZE_HEIGHT_INPUT,
  ) as HTMLInputElement;
  const lineStyleInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_BORDER_STYLE_INPUT,
  ) as HTMLInputElement;
  if (lineStyleInput && widthInput && heightInput) {
    lineStyleInput.value = lineStyle;
    widthInput.value = widthValue;
    heightInput.value = heightValue;
  }
};

const getBorderValues = (element: HTMLElement): void => {
  const borderWidth = window
    .getComputedStyle(element)
    .getPropertyValue('border-block-start-width')
    .replace('px', '');
  const borderStyle = window.getComputedStyle(element).getPropertyValue('border-block-start-style');
  const borderWidthInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_BORDER_WIDTH_INPUT,
  ) as HTMLInputElement;
  const borderStyleInput = document.getElementById(
    CONSTANTS.LOREE_DESIGN_SECTION_BORDER_STYLE_INPUT,
  ) as HTMLInputElement;
  if (borderWidthInput && borderStyleInput) {
    borderWidthInput.value = borderWidth;
    borderStyleInput.value = borderStyle;
  }
};

const getComputedValue = (elementValue: HTMLElement, property: string): string =>
  window.getComputedStyle(elementValue).getPropertyValue(property).replace('px', '');
