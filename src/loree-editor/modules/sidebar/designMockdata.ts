import { translate } from '../../../i18n/translate';
export const expectedResultborder =
  '{"0":"border-color","_values":{"border-color":"black","border-top-color":"black","border-right-color":"black","border-bottom-color":"black","border-left-color":"black"},"_importants":{"border-width":""},"_length":1}';
export const expectedResultwidth =
  '{"0":"border-width","_values":{"border-width":"0px","border-top-width":"0px","border-right-width":"0px","border-bottom-width":"0px","border-left-width":"0px"},"_importants":{},"_length":1}';

export const selectedElement = `<div style="display:flex; border-color:""; margin:0px; padding: 5px;" class="loree-iframe-content-elements"><div class="loree-iframe-content-container" style="width:100% border-color:"";;"></div></div>`;

export const tableMockData = `<div class="col-12 loree-iframe-content-column loree-style-yFIuz1">
<div class="loree-iframe-content-table-wrapper loree-style-77xKL3"><table class="loree-iframe-content-table loree-style-wk4bJQ" style="width: 100%; height: auto;">
    <caption class="loree-iframe-content-element loree-style-8WFIfF">${translate(
      'designmock.insertcaptionhere',
    )}</caption>
    <tbody>
      <tr class="loree-style-0w7iIO">
        <td class="loree-style-VG6lnx">
          <p class="loree-iframe-content-element loree-style-3ytMKq">${translate(
            'designmock.inserttexthere',
          )}</p>
        </td>
        <td class="loree-style-MJe5QK">
          <p class="loree-iframe-content-element loree-style-Q0nCUR">${translate(
            'designmock.inserttexthere',
          )}</p>
        </td>
      </tr>
      <tr class="loree-style-Okz0Cy">
        <td class="loree-style-DoQ7YI" style="background-color: rgb(223, 58, 58)">
          <p class="loree-iframe-content-element loree-style-jKeo3d">${translate(
            'designmock.inserttexthere',
          )}</p>
        </td>
        <td class="loree-style-D9tO9E" style="background-color: rgb(223, 58, 58)">
          <p class="loree-iframe-content-element loree-style-lh4Mqs">${translate(
            'designmock.inserttexthere',
          )}</p>
        </td>
      </tr>
      <tr class="loree-style-Okz0Cy">
        <td class="loree-style-DoQ7YI">
          <p class="loree-iframe-content-element loree-style-jKeo3d">${translate(
            'designmock.inserttabledata',
          )}</p>
        </td>
        <td class="loree-style-D9tO9E" style="background-color: rgb(165, 76, 76)">
          <p class="loree-iframe-content-element loree-style-lh4Mqs">${translate(
            'designmock.inserttabledata',
          )}</p>
        </td>
      </tr>
      <tr class="loree-style-Okz0Cy">
        <td class="loree-style-DoQ7YI">
          <p class="loree-iframe-content-element loree-style-jKeo3d">${translate(
            'designmock.inserttabledata',
          )}</p>
        </td>
        <td class="loree-style-D9tO9E" style="background-color: rgb(165, 76, 76)">
          <p class="loree-iframe-content-element loree-style-lh4Mqs">${translate(
            'designmock.inserttabledata',
          )}</p>
        </td>
      </tr>
      <tr class="loree-style-Okz0Cy">
      <td class="loree-style-DoQ7YI">
        <p class="loree-iframe-content-element loree-style-jKeo3d">${translate(
          'designmock.inserttabledata',
        )}</p>
      </td>
      <td class="loree-style-D9tO9E">
        <p class="loree-iframe-content-element loree-style-lh4Mqs">${translate(
          'designmock.inserttabledata',
        )}</p>
      </td>
    </tr>
    <tr class="loree-style-Okz0Cy">
      <td class="loree-style-DoQ7YI">
        <p class="loree-iframe-content-element loree-style-jKeo3d">${translate(
          'designmock.inserttabledata',
        )}</p>
      </td>
      <td class="loree-style-D9tO9E">
        <p class="loree-iframe-content-element loree-style-lh4Mqs">${translate(
          'designmock.inserttabledata',
        )}</p>
      </td>
    </tr>
    </tbody>
  </table>
</div>
</div>`;

export const tableDesignBorderSectionMock = `<div class="accordionContent collapse show" id="table-design-section-border" data-parent="#loree-sidebar-table-design-section-accordion-wrapper" style="">
<div class="borderOption">
<div class="borderWidth d-flex justify-content-between">
<div class="label">${translate('design.widthpixel')}</div>
<div class="d-flex py-1"><input class="input form-control-design" type="number">
<button class="color" role="button" aria-label=${translate(
  'designmock.colorpickerdialog_aria',
)} style="background-color: rgb(165, 165, 165);"></button></div></div>
<div class="borderType">
<select id="loree-sidebar-table-design-section-border-style">
<option value="solid">${translate('design.solid')}</option>
<option value="dashed">${translate('design.dashed')}</option>
<option value="dotted">${translate('design.dotted')}</option>
<option value="double">${translate('design.double')}</option>
<option value="none">${translate('design.none')}</option>
</select></div></div></div>`;

export const designSectionBorderMock = `<div class="fourSideMargin-2 design-section-font collapse show" id="loree-design-section-border-block-collapse" data-parent="#loree-sidebar-designEditOptions" style="">
<div class="design-section-image-border-option"><label>${translate('design.widthpixel')}</label>
<input type="number" class="form-control-design" id="loree-design-section-border-width-input" name="widthInput" min="0">
<button id="loree-design-section-image-border-color-picker" class="imageBorderColorPicker" role="button" aria-label=${translate(
  'designmock.colorpickerdialog_aria',
)} style="background-color: rgb(0, 0, 0);"></button></div>
<select class="border-dropdown" id="loree-design-section-border-style-input">
<option value="none">${translate('design.none')}</option>
<option value="dashed">${translate('design.dashed')}</option>
<option value="dotted">${translate('design.dotted')}</option>
<option value="double">${translate('design.double')}</option>
<option value="solid">${translate('design.solid')}</option></select></div>`;

export const mockParaHtml = `<p class="loree-iframe-content-element element-highlight" style="padding: 5px; margin: 0px 0px 10px; font-family: ABeeZee; font-size: 20px;" contenteditable="true">${translate(
  'designmock.inserttexthere',
)}</p>`;

export const mockVideoHtml =
  '<div class="loree-iframe-content-video-wrapper element-highlight" style="display: flex;padding: 5px;margin: 0 0 10px;"> <div class="loree-iframe-video-content" style="position:relative;width: 100%;display: inline-flex;max-width: 100%;max-height: 100%;border-width: 0;border-style: solid;border-color: #000000"> <div style="padding: 55% 0px 0px; display: flex;"> <iframe class="loree-no-pointer" src="https://www.youtube.com/embed/Ke90Tje7VS0" frameborder="0" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;max-width: 100%;" title=" "></iframe> </div> </div> </div>';
