import Pickr from '@simonwep/pickr';
import Base from '../../base';
import CONSTANTS from '../../constant';
import ICONS from './icons';
import { ConfigInterface } from '../../interface';
import '@simonwep/pickr/dist/themes/nano.min.css';
import { highlightSelectedContainerAlignment } from './designStyle';
import { getCurrentI18nLanguage, getEditorElementById, getUserLanguageStore } from '../../utils';
import hideColorPickerofElementSection from '../colorPicker/utils';
import { translate } from '../../../i18n/translate';

let borderColorPicker: Pickr | null = null;
let backgroundColorPicker: Pickr | null = null;
const customColors: _Any = [];
let editorConfig: _Any = {};
let previousElementColorStyles: string[] = [];
interface COLORPICKERINTERFACE extends Pickr.Options {
  i18n: {
    'btn:save': string;
    'btn:cancel': string;
  };
}

class TableDesign extends Base {
  initiate = (config: ConfigInterface): void => {
    editorConfig = config;
    const sidebarContentWrapper = document.getElementById(CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER);
    if (sidebarContentWrapper) {
      this.appendCustomStyles(config);
      const tableDesignSection = document.createElement('div');
      tableDesignSection.id = CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION;
      tableDesignSection.className = 'section tableDesignSection';
      tableDesignSection.style.display = 'none';
      this.attachLabel(tableDesignSection);
      this.attachOptions(tableDesignSection);
      sidebarContentWrapper.appendChild(tableDesignSection);
      this.updateTableSectionUpdateMethod(this.overTableDesignSectionMethod);
    }
  };

  appendCustomStyles = (config: _Any) => {
    const colors = config.customColor?.colors;
    if (colors?.length) {
      colors.forEach((clr: { color: string }) => {
        customColors.push(clr.color);
      });
    }
  };

  overTableDesignSectionMethod = (): void => {
    hideColorPickerofElementSection(CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_COLOR_PICKERS);
    const selectedElement = this.getSelectedElement();
    this.storePreviousElementColorStyles([selectedElement]);
    if (selectedElement) {
      if (
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE_WRAPPER) ||
        selectedElement.classList.contains(CONSTANTS.LOREE_IFRAME_CONTENT_TABLE) ||
        selectedElement.tagName === 'TBODY' ||
        selectedElement.tagName === 'TR'
      ) {
        this.attachTableOptions();
      }
      if (selectedElement.tagName === 'TD' || selectedElement.tagName === 'TH') {
        this.attachTableDataOptions();
      }
    }
  };

  attachLabel = (wrapper: HTMLElement): void => {
    const sectionLabel = document.createElement('button');
    sectionLabel.className = 'sectionLabel ';
    sectionLabel.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_BUTTON;
    sectionLabel.dataset.toggle = 'collapse';
    sectionLabel.dataset.target = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    sectionLabel.setAttribute('aria-expanded', 'true');
    sectionLabel.innerHTML = `
      <span class="label">${translate('global.design')}</span>
      <span class="image">
        <svg viewBox="0 0 8 14">
          <path id="dropdown_normal" d="M7,0l7,8H0Z" transform="translate(8) rotate(90)"></path>
        </svg>
      </span>
    `;
    wrapper.appendChild(sectionLabel);
  };

  attachTableOptions = (): void => {
    const accordion = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (accordion) {
      accordion.innerHTML = '';
      this.attachRowColumnLabel(accordion);
      this.attachNoOfRowsOption(accordion);
      this.attachNoOfColumnsOption(accordion);
      this.attachSizeOption(accordion);
      this.attachSpaceOption(accordion);
      this.attachAlignmentOption(accordion);
      this.attachColorOption(accordion);
      this.attachBorderOption(accordion);
    }
  };

  attachTableDataOptions = (): void => {
    const accordion = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (accordion) {
      accordion.innerHTML = '';
      this.attachRowColumnSelectionOption(accordion);
      this.attachSpaceOption(accordion);
      this.attachColorOption(accordion);
      this.attachFontOption(accordion);
    }
  };

  attachRowColumnLabel = (wrapper: HTMLElement): void => {
    const label = document.createElement('label');
    label.innerHTML = 'Number of:';
    label.className = 'sectionContent';
    wrapper.appendChild(label);
  };

  attachOptions = (wrapper: HTMLElement): void => {
    const accordion = document.createElement('div');
    accordion.className = 'tableDesignAccordionWrapper accordion';
    accordion.id = CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER;
    wrapper.appendChild(accordion);
  };

  attachNoOfRowsOption = (wrapper: HTMLElement): void => {
    const rowOptionWrapper = document.createElement('div');
    rowOptionWrapper.className = 'tableRowOptionWrapper';
    const title = document.createElement('div');
    title.className = 'tableRowOption title';
    title.innerHTML = `${translate('sidebar.rows')}`;
    rowOptionWrapper.appendChild(title);
    const input = document.createElement('input');
    input.type = 'number';
    input.value = this.getSelectedTableRows();
    input.onchange = this.handleTableRowOnChange;
    input.className = 'tableRowOption input form-control-design';
    rowOptionWrapper.appendChild(input);
    wrapper.appendChild(rowOptionWrapper);
  };

  getSelectedTableRows = (): string => {
    const selectedElement = this.getSelectedElement();
    let value = '0';
    if (!selectedElement) return value;
    const table = selectedElement.getElementsByTagName('table')[0];
    if (!table) return value;
    let tableBody;
    if (table.children[0].nodeName === 'TBODY') tableBody = table.children[0];
    else tableBody = table.children[1];
    if (!tableBody) return value;
    value = `${tableBody.children.length}`;
    return value;
  };

  updateLanguageAttribute = (): string => {
    return getUserLanguageStore().selectedMiniMenuLanguage !== ''
      ? getUserLanguageStore().selectedMiniMenuLanguage
      : getCurrentI18nLanguage();
  };

  handleTableRowOnChange = (e: Event): void => {
    e.preventDefault();
    const element = e.target as HTMLInputElement;
    if (element) {
      const value = parseInt(element.value);
      if (value >= 2) {
        element.value = value.toString();
        const selectedElement = this.getSelectedElement();
        if (selectedElement) {
          const tbody = selectedElement.getElementsByTagName('tbody')[0];
          const rows = parseInt(this.getSelectedTableRows());
          const column = parseInt(this.getSelectedTableColumn());
          if (value > rows) {
            const newRowsCount = value - rows;
            const lastRow = tbody.children[tbody.children.length - 1];
            let header = false;
            if (lastRow) {
              const firstData = lastRow.children[0];
              if (firstData && firstData.tagName === 'TH') header = true;
            }
            for (let i = 0; i < newRowsCount; i++) {
              const row = document.createElement('tr');
              row.style.border = '1px solid #a5a5a5';
              for (let j = 0; j < column; j++) {
                let column = document.createElement('td');
                column.innerHTML = `<p class=${
                  CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
                } style="border-width: 0; border-style: solid; border-color: #000000;color: #000000;font-size: ${
                  editorConfig.customHeaderStyle.customHeader[6].paragraph.size
                }; font-family:${
                  editorConfig.customHeaderStyle.customHeader[6].paragraph.font
                }; padding: 5px; margin:0px" lang= ${this.updateLanguageAttribute()}>${translate(
                  'designmock.inserttexthere',
                )}</p>`;
                if (j === 0 && header) {
                  column = document.createElement('th');
                  column.innerHTML = `<h3 class=${
                    CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
                  } style="border-width: 0; border-style: solid; border-color: #000000;color: #000000;font-size: ${
                    editorConfig.customHeaderStyle.customHeader[2].h3.size
                  };font-family: ${
                    editorConfig.customHeaderStyle.customHeader[2].h3.font
                  }; padding: 5px; margin:0px" lang= ${this.updateLanguageAttribute()}>${translate(
                    'sidebar.header',
                  )}</h3>`;
                }
                column.style.border = '1px solid #a5a5a5';
                column.style.padding = '5px';
                row.appendChild(column);
              }
              if (tbody) tbody.appendChild(row);
            }
          }
          if (value <= rows) {
            if (tbody) {
              const rows = tbody.children;
              const newRows = Array.from(rows).slice(0, value);
              tbody.innerHTML = '';
              newRows.forEach((row) => {
                tbody.appendChild(row);
              });
            }
          }
        }
      } else {
        element.value = '2';
      }
      this.handleSelectedContentChanges();
    }
  };

  attachNoOfColumnsOption = (wrapper: HTMLElement): void => {
    const rowOptionWrapper = document.createElement('div');
    rowOptionWrapper.className = 'tableColumnOptionWrapper';
    const title = document.createElement('div');
    title.className = 'tableColumnOption title';
    title.innerHTML = `${translate('sidebar.columns')}`;
    rowOptionWrapper.appendChild(title);
    const input = document.createElement('input');
    input.type = 'number';
    input.value = this.getSelectedTableColumn();
    input.onchange = this.handleTableColumnOnChange;
    input.className = 'tableColumnOption input form-control-design';
    rowOptionWrapper.appendChild(input);
    wrapper.appendChild(rowOptionWrapper);
  };

  getSelectedTableColumn = (): string => {
    const selectedElement = this.getSelectedElement();
    let value = '0';
    if (!selectedElement) return value;
    const table = selectedElement.getElementsByTagName('table')[0];
    if (!table) return value;
    let tableBody;
    if (table.children[0].nodeName === 'TBODY') tableBody = table.children[0];
    else tableBody = table.children[1];
    if (!tableBody) return value;
    const tableRows = tableBody.children[0];
    if (!tableRows) return value;
    value = `${tableRows.children.length}`;
    return value;
  };

  handleTableColumnOnChange = (e: Event): void => {
    e.preventDefault();
    const element = e.target as HTMLInputElement;
    if (element) {
      const value = parseInt(element.value);
      if (value >= 2) {
        element.value = value.toString();
        const selectedElement = this.getSelectedElement();
        if (selectedElement) {
          const tbody = selectedElement.getElementsByTagName('tbody')[0];
          const column = parseInt(this.getSelectedTableColumn());
          if (value > column) {
            const newColumnsCount = value - column;
            const firstRow = tbody.children[0];
            let header = false;
            if (firstRow) {
              const firstData = firstRow.children[firstRow.children.length - 1];
              if (firstData && firstData.tagName === 'TH') header = true;
            }
            Array.from(tbody.children).forEach((row, index) => {
              for (let i = 0; i < newColumnsCount; i++) {
                let column = document.createElement('td');
                column.innerHTML = `<p class=${
                  CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
                } style="border-width: 0; border-style: solid; border-color: #000000;color: #000000;font-size: ${
                  editorConfig.customHeaderStyle.customHeader[6].paragraph.size
                }; font-family:${
                  editorConfig.customHeaderStyle.customHeader[6].paragraph.font
                }; padding: 5px; margin:0px" lang= ${this.updateLanguageAttribute()}>${translate(
                  'designmock.inserttexthere',
                )}</p>`;
                if (index === 0 && header) {
                  column = document.createElement('th');
                  column.innerHTML = `<h3 class=${
                    CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT
                  } style="border-width: 0; border-style: solid; border-color: #000000;color: #000000;font-size: ${
                    editorConfig.customHeaderStyle.customHeader[2].h3.size
                  };font-family: ${
                    editorConfig.customHeaderStyle.customHeader[2].h3.font
                  }; padding: 5px; margin:0px"lang= ${this.updateLanguageAttribute()}>${translate(
                    'sidebar.header',
                  )}</h3>`;
                }
                column.style.border = '1px solid #a5a5a5';
                column.style.padding = '5px';
                row.appendChild(column);
              }
            });
          }
          if (value <= column) {
            if (tbody) {
              Array.from(tbody.children).forEach((row) => {
                const columns = row.children;
                const newColumns = Array.from(columns).slice(0, value);
                row.innerHTML = '';
                newColumns.forEach((data) => {
                  row.appendChild(data);
                });
              });
            }
          }
        }
      } else {
        element.value = '2';
      }
      this.handleSelectedContentChanges();
    }
  };

  attachRowColumnSelectionOption = (wrapper: HTMLElement): void => {
    const rowColumnSelectionOption = document.createElement('div');
    rowColumnSelectionOption.className = 'rowColumnSelectionOption';

    const rowOption = document.createElement('div');
    rowOption.className = 'option';
    const rowInput = document.createElement('input');
    rowInput.type = 'checkbox';
    rowInput.value = 'selectEntireRow';
    rowInput.name = 'selectEntireRow';
    rowInput.checked = false;
    rowInput.onchange = (e): void => this.handleEntireTableRowSelection(e);
    rowOption.appendChild(rowInput);

    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('sidebar.selectentirerow')}`;
    rowOption.appendChild(label);

    const columnOption = document.createElement('div');
    columnOption.className = 'option';
    const columnInput = document.createElement('input');
    columnInput.type = 'checkbox';
    columnInput.value = 'selectEntireColumn';
    columnInput.name = 'selectEntireColumn';
    columnInput.checked = false;
    columnInput.onchange = (e): void => this.handleEntireTableColumnSelection(e);
    columnOption.appendChild(columnInput);
    const columnLabel = document.createElement('div');
    columnLabel.className = 'label';
    columnLabel.innerHTML = `${translate('sidebar.selectentirecolumn')}`;
    columnOption.appendChild(columnLabel);

    rowColumnSelectionOption.appendChild(rowOption);
    rowColumnSelectionOption.appendChild(columnOption);
    wrapper.appendChild(rowColumnSelectionOption);
  };

  resetPreviousElementColorStyles = () => {
    previousElementColorStyles = [];
  };

  storePreviousElementColorStyles = (tableElement: _Any[]) => {
    this.resetPreviousElementColorStyles();
    tableElement.forEach((tableData) => {
      const getTableElement = tableData.getElementsByTagName('TABLE')[0] as HTMLTableElement;
      const element = getTableElement || tableData;
      previousElementColorStyles.push(element.style.backgroundColor);
    });
  };

  getPreviousElementColorStyles = () => {
    return previousElementColorStyles;
  };

  tableMultiRowIndexStart = 0;
  handleEntireTableRowSelection = (e: Event): void => {
    const selectedElement = this.getSelectedElement();
    const selectedElements = this.getSelectedElements();
    if (!selectedElement) return;
    const element = e.target as HTMLInputElement;
    const checked = element.checked;
    if (checked) {
      this.tableMultiRowIndexStart = selectedElements.length;
      selectedElements.forEach((ele) => {
        const parent = ele.parentElement;
        if (!parent) return;
        Array.from(parent.children).forEach((child) => {
          const htmlChild = child as HTMLElement;
          this.addElementToSelectedElements(htmlChild);
        });
      });
    } else {
      const original = selectedElements.slice(0, this.tableMultiRowIndexStart);
      this.tableMultiRowIndexStart = 0;
      this.removeElementsFromSelectedElements();
      original.forEach((td) => {
        this.addElementToSelectedElements(td);
      });
      if (original.length <= 1) this.changeSelectedElement(original[0]);
    }
    this.storePreviousElementColorStyles(selectedElements);
  };

  tableMultiColumnIndexStart = 0;
  handleEntireTableColumnSelection = (e: Event): void => {
    const selectedElement = this.getSelectedElement() as HTMLTableCellElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElement) return;
    const element = e.target as HTMLInputElement;
    const checked = element.checked;
    if (checked) {
      this.tableMultiColumnIndexStart = selectedElements.length;
      selectedElements.forEach((child) => {
        const childElement = child as HTMLTableCellElement;
        const index = childElement.cellIndex;
        const rowParent = childElement.parentElement;
        if (!rowParent) return;
        const tableBody = rowParent.parentElement;
        if (!tableBody) return;
        Array.from(tableBody.children).forEach((child) => {
          const htmlChild = child.children[index] as HTMLElement;
          this.addElementToSelectedElements(htmlChild);
        });
      });
    } else {
      const original = selectedElements.slice(0, this.tableMultiColumnIndexStart);
      this.tableMultiColumnIndexStart = 0;
      this.removeElementsFromSelectedElements();
      original.forEach((td) => {
        this.addElementToSelectedElements(td);
      });
      if (original.length <= 1) this.changeSelectedElement(original[0]);
    }
    this.storePreviousElementColorStyles(selectedElements);
  };

  attachFontOption = (wrapper: HTMLElement): void => {
    const fontWrapper = document.createElement('div');
    fontWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-font`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('design.font')}</span>
    `;
    fontWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-font';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    this.attachFontFamilyOption(accordionContent);
    this.attachFontSizeOption(accordionContent);
    this.attachFontWeightOption(accordionContent);
    this.attachFontStyletOption(accordionContent);
    fontWrapper.appendChild(accordionContent);
    wrapper.appendChild(fontWrapper);
  };

  attachFontFamilyOption = (wrapper: HTMLElement): void => {
    const fontFamilyOption = document.createElement('div');
    fontFamilyOption.className = 'fontFamilyOption';
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('sidebar.family')}`;
    const selectBoxWrapper = document.createElement('div');
    selectBoxWrapper.className = 'selectBoxWrapper';
    const select = document.createElement('select');
    const defaultFonts = [
      {
        fontFamily: 'Arial',
        name: 'Arial',
      },
      {
        fontFamily: 'Helvetica',
        name: 'Helvetica',
      },
    ];
    const customFonts = editorConfig.customFonts.fonts;
    const fonts = [...defaultFonts, ...customFonts];
    fonts.sort((a, b) => (a.name > b.name ? 1 : b.name > a.name ? -1 : 0));
    fonts.forEach((font: { name: string; fontFamily: string }) => {
      const option = document.createElement('option');
      option.value = font.fontFamily;
      option.innerHTML = font.name;
      select.appendChild(option);
    });
    select.value = this.getTableFontFamily();
    select.onchange = this.handleTableFontFamilyChange;
    selectBoxWrapper.appendChild(select);
    fontFamilyOption.appendChild(label);
    fontFamilyOption.appendChild(selectBoxWrapper);
    wrapper.appendChild(fontFamilyOption);
  };

  getTableFontFamily = (): string => {
    let value = '';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const styles = window.getComputedStyle(selectedElement);
    value = styles.getPropertyValue('font-family');
    return value;
  };

  handleTableFontFamilyChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((ele) => {
      this.applyFontFamilyForTableElement(ele, element.value);
    });
    this.handleSelectedContentChanges();
  };

  applyFontFamilyForTableElement = (element: HTMLElement, value: string) => {
    element.style.fontFamily = value;
    if (element.children.length > 0) {
      Array.from(element.children).forEach((child) => {
        this.applyFontFamilyForTableElement(child as HTMLElement, value);
      });
    }
  };

  attachFontSizeOption = (wrapper: HTMLElement): void => {
    const fontWeightOption = document.createElement('div');
    fontWeightOption.className = 'fontFamilyOption';
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('global.size')}`;
    const inputBoxWrapper = document.createElement('div');
    inputBoxWrapper.className = 'inputBoxWrapper';
    const input = document.createElement('input');
    input.type = 'number';
    input.value = this.getTableFontSize();
    input.onchange = this.handleTableFontSizeChange;
    inputBoxWrapper.appendChild(input);
    fontWeightOption.appendChild(label);
    fontWeightOption.appendChild(inputBoxWrapper);
    wrapper.appendChild(fontWeightOption);
  };

  getTableFontSize = (): string => {
    let value = '';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const styles = window.getComputedStyle(selectedElement);
    value = styles.getPropertyValue('font-size').split('px')[0];
    return value;
  };

  handleTableFontSizeChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((ele) => {
      this.applyFontSizeForTableElement(ele, `${element.value}px`);
    });
    this.handleSelectedContentChanges();
  };

  applyFontSizeForTableElement = (element: HTMLElement, value: string) => {
    element.style.fontSize = value;
    if (element.children.length > 0) {
      Array.from(element.children).forEach((child) => {
        this.applyFontSizeForTableElement(child as HTMLElement, value);
      });
    }
  };

  attachFontWeightOption = (wrapper: HTMLElement): void => {
    const fontWeightOption = document.createElement('div');
    fontWeightOption.className = 'fontFamilyOption';
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('sidebar.weight')}`;
    const inputBoxWrapper = document.createElement('div');
    inputBoxWrapper.className = 'inputBoxWrapper';
    const input = document.createElement('input');
    input.type = 'text';
    input.value = this.getTableFontWeight();
    input.onchange = this.handleTableFontWeightChange;
    inputBoxWrapper.appendChild(input);
    fontWeightOption.appendChild(label);
    fontWeightOption.appendChild(inputBoxWrapper);
    wrapper.appendChild(fontWeightOption);
  };

  getTableFontWeight = (): string => {
    let value = '';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const styles = window.getComputedStyle(selectedElement);
    value = styles.getPropertyValue('font-weight');
    return value;
  };

  handleTableFontWeightChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((ele) => {
      this.applyFontWeightForTableElement(ele, element.value);
    });
  };

  applyFontWeightForTableElement = (element: HTMLElement, fontFamily: string) => {
    element.style.fontWeight = fontFamily;
    if (element.children.length > 0) {
      Array.from(element.children).forEach((child) => {
        this.applyFontSizeForTableElement(child as HTMLElement, fontFamily);
      });
    }
    this.handleSelectedContentChanges();
  };

  attachFontStyletOption = (wrapper: HTMLElement): void => {
    const fontStyleOption = document.createElement('div');
    fontStyleOption.className = 'fontFamilyOption';
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('sidebar.style')}`;
    const selectBoxWrapper = document.createElement('div');
    selectBoxWrapper.className = 'selectBoxWrapper';
    const select = document.createElement('select');
    select.innerHTML = `
      <option value="italic">${translate('sidebar.italic')}</option>
      <option value="normal">${translate('sidebar.normal')}</option>
      <option value="oblique">${translate('sidebar.oblique')}</option>
    `;
    select.value = this.getTableFontStyle();
    select.onchange = this.handleTableFontStyleChange;
    selectBoxWrapper.appendChild(select);
    fontStyleOption.appendChild(label);
    fontStyleOption.appendChild(selectBoxWrapper);
    wrapper.appendChild(fontStyleOption);
  };

  getTableFontStyle = (): string => {
    let value = '';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    const styles = window.getComputedStyle(selectedElement);
    value = styles.getPropertyValue('font-style');
    return value;
  };

  handleTableFontStyleChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((ele) => {
      this.applyFontStyleForTableElement(ele, element.value);
    });
  };

  applyFontStyleForTableElement = (element: HTMLElement, fontFamily: string) => {
    element.style.fontStyle = fontFamily;
    if (element.children.length > 0) {
      Array.from(element.children).forEach((child) => {
        this.applyFontSizeForTableElement(child as HTMLElement, fontFamily);
      });
    }
    this.handleSelectedContentChanges();
  };

  attachAlignmentOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-alignment`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.alignment')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-alignment';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const leftIcon = document.createElement('button');
    leftIcon.className = 'alignmentButton';
    leftIcon.id = CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT;
    leftIcon.innerHTML = ICONS.LEFT_ALIGNMENT_ICON;
    leftIcon.onclick = (): void => this.handleTableAlignment('start');
    accordionContent.appendChild(leftIcon);
    const centerIcon = document.createElement('button');
    centerIcon.className = 'alignmentButton';
    centerIcon.id = CONSTANTS.LOREE_TABLE_DESIGN_CENTER_ALIGNMENT;
    centerIcon.innerHTML = ICONS.CENTER_ALIGNMENT_ICON;
    centerIcon.onclick = (): void => this.handleTableAlignment('center');
    accordionContent.appendChild(centerIcon);
    const rightIcon = document.createElement('button');
    rightIcon.className = 'alignmentButton';
    rightIcon.id = CONSTANTS.LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT;
    rightIcon.innerHTML = ICONS.RIGHT_ALIGNMENT_ICON;
    rightIcon.onclick = (): void => this.handleTableAlignment('end');
    accordionContent.appendChild(rightIcon);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
    highlightSelectedContainerAlignment(this.getSelectedElement());
  };

  handleTableAlignment = (position: string): void => {
    const selectedElement = this.getSelectedElement();
    const alignCenter = document.getElementById(
      CONSTANTS.LOREE_TABLE_DESIGN_CENTER_ALIGNMENT,
    ) as HTMLElement;
    const alignLeft = document.getElementById(
      CONSTANTS.LOREE_TABLE_DESIGN_LEFT_ALIGNMENT,
    ) as HTMLElement;
    const alignRight = document.getElementById(
      CONSTANTS.LOREE_TABLE_DESIGN_RIGHT_ALIGNMENT,
    ) as HTMLElement;
    if (!selectedElement) return;
    switch (position) {
      case 'start':
        alignLeft.style.stroke = 'blue';
        alignRight.style.stroke = '';
        alignCenter.style.stroke = '';
        this.applyAlignmentToTable('margin-left', '');
        this.applyAlignmentToTable('margin-right', 'auto');
        break;
      case 'center':
        alignLeft.style.stroke = '';
        alignRight.style.stroke = '';
        alignCenter.style.stroke = 'blue';
        this.applyAlignmentToTable('margin-left', 'auto');
        this.applyAlignmentToTable('margin-right', 'auto');
        break;
      case 'end':
        alignLeft.style.stroke = '';
        alignRight.style.stroke = 'blue';
        alignCenter.style.stroke = '';
        this.applyAlignmentToTable('margin-left', 'auto');
        this.applyAlignmentToTable('margin-right', '');
        break;
    }
    this.handleSelectedContentChanges();
  };

  handleAccordionButtonClick = (e: Event): void => {
    const wrapper = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER,
    );
    if (wrapper) {
      const element = e.target as HTMLElement;
      let label = '';
      if (element.tagName === 'SPAN') {
        const parent = element.parentElement;
        if (parent) {
          const icon = parent.getElementsByClassName('icon')[0];
          label = parent.getElementsByClassName('label')[0].innerHTML.trim();
          if (icon) {
            if (icon.innerHTML === '+') {
              icon.innerHTML = 'I';
            } else {
              icon.innerHTML = '+';
            }
          }
        }
      } else {
        const icon = element.getElementsByClassName('icon')[0];
        label = element.getElementsByClassName('label')[0].innerHTML.trim();
        if (icon) {
          if (icon.innerHTML === '+') {
            icon.innerHTML = 'I';
          } else {
            icon.innerHTML = '+';
          }
        }
      }
      const accordionButton = wrapper.getElementsByClassName('accordionButton');
      Array.from(accordionButton).forEach((button) => {
        const icon = button.getElementsByClassName('icon')[0];
        const buttonLabel = button.getElementsByClassName('label')[0].innerHTML.trim();
        if (icon) {
          if (buttonLabel !== label) icon.innerHTML = '+';
        }
      });
      this.handleSelectedContentChanges();
    }
  };

  attachSizeOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-size`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.size')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-size';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const widthOption = document.createElement('div');
    widthOption.className = 'widthOption';

    // Width label
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('sidebar.widthpercent')}`;
    widthOption.appendChild(label);

    // Width input box
    const input = document.createElement('input');
    input.className = 'input form-control-design';
    input.type = 'number';
    input.value = this.getTableWidth();
    input.onchange = this.handleTableWidthChange;
    widthOption.appendChild(input);

    accordionContent.appendChild(widthOption);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
  };

  getTableWidth = (): string => {
    let value = '100';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;

    const table = selectedElement.getElementsByTagName('table')[0];
    if (!table) return value;

    value = table.style.width.split('%')[0];
    return value;
  };

  handleTableWidthChange = (e: Event) => {
    e.preventDefault();
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return;

    const table = selectedElement.getElementsByTagName('table')[0];
    if (!table) return;

    const element = e.target as HTMLInputElement;
    const value = parseInt(element.value);
    if (value >= 0 && value <= 100) {
      element.value = `${value}`;
      table.style.width = `${value}%`;
    } else {
      if (value < 0) element.value = '0';
      if (value > 100) element.value = '100';
    }
    this.handleSelectedContentChanges();
  };

  hideMarginSpaceOption = (tableMarginSpaceWrapper: HTMLElement) => {
    const nodeName = this.getSelectedElement()?.nodeName as string;
    const arr = ['TD', 'TH'];
    if (arr.includes(nodeName)) {
      tableMarginSpaceWrapper.className += ' d-none';
    }
  };

  attachSpaceOption = (wrapper: HTMLElement) => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-space`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('design.space')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);

    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-space';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const tableMarginSpaceWrapper = document.createElement('div');
    tableMarginSpaceWrapper.className = 'tableMarginSpaceWrapper';
    this.hideMarginSpaceOption(tableMarginSpaceWrapper);
    const marginLabel = document.createElement('div');
    marginLabel.className =
      'label d-flex justify-content-between mb-2 editor-design-margin-heading';
    marginLabel.innerHTML = `<b>${translate('design.margin')}</b><span><i>${translate(
      'design.pixelsuffix',
    )}</i></span>`;
    tableMarginSpaceWrapper.appendChild(marginLabel);
    const marginOptions = document.createElement('div');
    marginOptions.className = 'options';

    this.leftMarginOption(marginOptions);
    this.topMarginOption(marginOptions);
    this.rightMarginOption(marginOptions);
    this.bottomMarginOption(marginOptions);
    tableMarginSpaceWrapper.appendChild(marginOptions);

    const tablePaddingSpaceWrapper = document.createElement('div');
    tablePaddingSpaceWrapper.className = 'tablePaddingSpaceWrapper';
    const paddingLabel = document.createElement('div');
    paddingLabel.className =
      'label d-flex justify-content-between mb-2 editor-design-margin-heading';
    paddingLabel.innerHTML = `<b>${translate('design.padding')}</b><span><i>${translate(
      'design.pixelsuffix',
    )}</i></span>`;
    tablePaddingSpaceWrapper.appendChild(paddingLabel);
    const paddingOptions = document.createElement('div');
    paddingOptions.className = 'options';

    this.leftPaddingOption(paddingOptions);
    this.topPaddingOption(paddingOptions);
    this.rightPaddingOption(paddingOptions);
    this.bottomPaddingOption(paddingOptions);
    tablePaddingSpaceWrapper.appendChild(paddingOptions);

    accordionContent.appendChild(tableMarginSpaceWrapper);
    accordionContent.appendChild(tablePaddingSpaceWrapper);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);

    return wrapper;
  };

  leftMarginOption = (wrapper: HTMLElement): void => {
    const leftOptionMargin = document.createElement('div');
    leftOptionMargin.className = 'option';
    const leftOptionMarginChild = document.createElement('div');
    leftOptionMarginChild.className = 'leftOptionMargin';
    const leftOptionMarginInput = document.createElement('input');
    leftOptionMarginInput.type = 'number';
    leftOptionMarginInput.className = 'form-control-design';
    leftOptionMarginInput.value = this.getTableMargin('left');
    leftOptionMarginInput.onchange = (e: Event): void => this.handleTableMarginChange(e, 'left');
    leftOptionMarginChild.appendChild(leftOptionMarginInput);
    leftOptionMargin.appendChild(leftOptionMarginChild);
    wrapper.appendChild(leftOptionMargin);
  };

  topMarginOption = (wrapper: HTMLElement): void => {
    const topOptionMargin = document.createElement('div');
    topOptionMargin.className = 'option';
    const topOptionMarginChild = document.createElement('div');
    topOptionMarginChild.className = 'topOptionMargin';
    const topOptionMarginInput = document.createElement('input');
    topOptionMarginInput.type = 'number';
    topOptionMarginInput.className = 'form-control-design';
    topOptionMarginInput.value = this.getTableMargin('top');
    topOptionMarginInput.onchange = (e: Event): void => this.handleTableMarginChange(e, 'top');
    topOptionMarginChild.appendChild(topOptionMarginInput);
    topOptionMargin.appendChild(topOptionMarginChild);
    wrapper.appendChild(topOptionMargin);
  };

  rightMarginOption = (wrapper: HTMLElement): void => {
    const rightOptionMargin = document.createElement('div');
    rightOptionMargin.className = 'option';
    const rightOptionMarginChild = document.createElement('div');
    rightOptionMarginChild.className = 'rightOptionMargin';
    const rightOptionMarginInput = document.createElement('input');
    rightOptionMarginInput.type = 'number';
    rightOptionMarginInput.className = 'form-control-design';
    rightOptionMarginInput.value = this.getTableMargin('right');
    rightOptionMarginInput.onchange = (e: Event): void => this.handleTableMarginChange(e, 'right');
    rightOptionMarginChild.appendChild(rightOptionMarginInput);
    rightOptionMargin.appendChild(rightOptionMarginChild);
    wrapper.appendChild(rightOptionMargin);
  };

  bottomMarginOption = (wrapper: HTMLElement): void => {
    const bottomOptionMargin = document.createElement('div');
    bottomOptionMargin.className = 'option';
    const bottomOptionMarginChild = document.createElement('div');
    bottomOptionMarginChild.className = 'bottomOptionMargin';
    const bottomOptionMarginInput = document.createElement('input');
    bottomOptionMarginInput.type = 'number';
    bottomOptionMarginInput.className = 'form-control-design';
    bottomOptionMarginInput.value = this.getTableMargin('bottom');
    bottomOptionMarginInput.onchange = (e: Event): void =>
      this.handleTableMarginChange(e, 'bottom');
    bottomOptionMarginChild.appendChild(bottomOptionMarginInput);
    bottomOptionMargin.appendChild(bottomOptionMarginChild);
    wrapper.appendChild(bottomOptionMargin);
  };

  getTableMargin = (position: string): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    switch (position) {
      case 'top':
        value = getComputedStyle(selectedElement).marginTop.replace('px', '');
        break;
      case 'bottom':
        value = getComputedStyle(selectedElement).marginBottom.replace('px', '');
        break;
      case 'left':
        value = getComputedStyle(selectedElement).marginLeft.replace('px', '');
        break;
      case 'right':
        value = getComputedStyle(selectedElement).marginRight.replace('px', '');
        break;
    }
    if (!value) value = '0';
    return value;
  };

  handleTableMarginChange = (e: Event, position: string): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (selectedElements.length < 2) {
      switch (position) {
        case 'top':
          this.applyStyleToSelectedElement('margin-top', `${element.value}px`);
          break;
        case 'bottom':
          this.applyStyleToSelectedElement('margin-bottom', `${element.value}px`);
          break;
        case 'left':
          this.applyStyleToSelectedElement('margin-left', `${element.value}px`);
          break;
        case 'right':
          this.applyStyleToSelectedElement('margin-right', `${element.value}px`);
          break;
      }
    } else {
      selectedElements.forEach((tdElement) => {
        switch (position) {
          case 'top':
            tdElement.style.marginTop = `${element.value}px`;
            break;
          case 'bottom':
            tdElement.style.marginBottom = `${element.value}px`;
            break;
          case 'left':
            tdElement.style.marginLeft = `${element.value}px`;
            break;
          case 'right':
            tdElement.style.marginRight = `${element.value}px`;
            break;
        }
      });
    }
    this.handleSelectedContentChanges();
  };

  leftPaddingOption = (wrapper: HTMLElement): void => {
    const leftOptionMargin = document.createElement('div');
    leftOptionMargin.className = 'option';
    const leftOptionMarginChild = document.createElement('div');
    leftOptionMarginChild.className = 'leftOptionPadding';
    const leftOptionMarginInput = document.createElement('input');
    leftOptionMarginInput.type = 'number';
    leftOptionMarginInput.className = 'form-control-design';
    leftOptionMarginInput.value = this.getTablePadding('left');
    leftOptionMarginInput.onchange = (e: Event): void => this.handleTablePaddingChange(e, 'left');
    leftOptionMarginChild.appendChild(leftOptionMarginInput);
    leftOptionMargin.appendChild(leftOptionMarginChild);
    wrapper.appendChild(leftOptionMargin);
  };

  topPaddingOption = (wrapper: HTMLElement): void => {
    const topOptionMargin = document.createElement('div');
    topOptionMargin.className = 'option';
    const topOptionMarginChild = document.createElement('div');
    topOptionMarginChild.className = 'topOptionPadding';
    const topOptionMarginInput = document.createElement('input');
    topOptionMarginInput.type = 'number';
    topOptionMarginInput.className = 'form-control-design';
    topOptionMarginInput.value = this.getTablePadding('top');
    topOptionMarginInput.onchange = (e: Event): void => this.handleTablePaddingChange(e, 'top');
    topOptionMarginChild.appendChild(topOptionMarginInput);
    topOptionMargin.appendChild(topOptionMarginChild);
    wrapper.appendChild(topOptionMargin);
  };

  rightPaddingOption = (wrapper: HTMLElement): void => {
    const rightOptionMargin = document.createElement('div');
    rightOptionMargin.className = 'option';
    const rightOptionMarginChild = document.createElement('div');
    rightOptionMarginChild.className = 'rightOptionPadding';
    const rightOptionMarginInput = document.createElement('input');
    rightOptionMarginInput.type = 'number';
    rightOptionMarginInput.className = 'form-control-design';
    rightOptionMarginInput.value = this.getTablePadding('right');
    rightOptionMarginInput.onchange = (e: Event): void => this.handleTablePaddingChange(e, 'right');
    rightOptionMarginChild.appendChild(rightOptionMarginInput);
    rightOptionMargin.appendChild(rightOptionMarginChild);
    wrapper.appendChild(rightOptionMargin);
  };

  bottomPaddingOption = (wrapper: HTMLElement): void => {
    const bottomOptionMargin = document.createElement('div');
    bottomOptionMargin.className = 'option';
    const bottomOptionMarginChild = document.createElement('div');
    bottomOptionMarginChild.className = 'bottomOptionPadding';
    const bottomOptionMarginInput = document.createElement('input');
    bottomOptionMarginInput.type = 'number';
    bottomOptionMarginInput.className = 'form-control-design';
    bottomOptionMarginInput.value = this.getTablePadding('bottom');
    bottomOptionMarginInput.onchange = (e: Event): void =>
      this.handleTablePaddingChange(e, 'bottom');
    bottomOptionMarginChild.appendChild(bottomOptionMarginInput);
    bottomOptionMargin.appendChild(bottomOptionMarginChild);
    wrapper.appendChild(bottomOptionMargin);
  };

  getTablePadding = (position: string): string => {
    let value = '0';
    const selectedElement = this.getSelectedElement();
    if (!selectedElement) return value;
    switch (position) {
      case 'top':
        value = getComputedStyle(selectedElement).paddingTop.replace('px', '');
        break;
      case 'bottom':
        value = getComputedStyle(selectedElement).paddingBottom.replace('px', '');
        break;
      case 'left':
        value = getComputedStyle(selectedElement).paddingLeft.replace('px', '');
        break;
      case 'right':
        value = getComputedStyle(selectedElement).paddingRight.replace('px', '');
        break;
    }
    if (!value) value = '0';
    return value;
  };

  handleTablePaddingChange = (e: Event, position: string): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (selectedElements.length < 2) {
      switch (position) {
        case 'top':
          this.applyStyleToSelectedElement('padding-top', `${element.value}px`);
          break;
        case 'bottom':
          this.applyStyleToSelectedElement('padding-bottom', `${element.value}px`);
          break;
        case 'left':
          this.applyStyleToSelectedElement('padding-left', `${element.value}px`);
          break;
        case 'right':
          this.applyStyleToSelectedElement('padding-right', `${element.value}px`);
          break;
      }
    } else {
      selectedElements.forEach((tdElement) => {
        switch (position) {
          case 'top':
            tdElement.style.paddingTop = `${element.value}px`;
            break;
          case 'bottom':
            tdElement.style.paddingBottom = `${element.value}px`;
            break;
          case 'left':
            tdElement.style.paddingLeft = `${element.value}px`;
            break;
          case 'right':
            tdElement.style.paddingRight = `${element.value}px`;
            break;
        }
      });
    }
    this.handleSelectedContentChanges();
  };

  attachBorderOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-border`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.border')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);
    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-border';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const borderOption = document.createElement('div');
    borderOption.className = 'borderOption';

    const borderWidth = document.createElement('div');
    borderWidth.className = 'borderWidth d-flex justify-content-between';

    const labelWrapper = document.createElement('div');
    labelWrapper.className = 'd-flex py-1';
    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = `${translate('design.widthpixel')}`;
    borderWidth.appendChild(label);

    const selectedElement = this.getSelectedElement();
    const input = document.createElement('input');
    input.className = 'input form-control-design';
    input.type = 'number';
    input.value = this.getTableBorderWidthOfTable();
    input.onchange = (e: Event): void => this.handleTableBorderWidthChange(e);
    labelWrapper.appendChild(input);

    const color = document.createElement('button');
    color.className = 'color';
    color.style.backgroundColor = this.getBorderColorOfTable(selectedElement);
    labelWrapper.appendChild(color);
    this.attachBorderColorPicker(color, selectedElement);
    borderWidth.appendChild(labelWrapper);
    borderOption.appendChild(borderWidth);

    const borderType = document.createElement('div');
    borderType.className = 'borderType';

    const select = document.createElement('select');
    select.innerHTML = `
      <option value="solid">${translate('design.solid')}</option>
      <option value="dashed">${translate('design.dashed')}</option>
      <option value="dotted">${translate('design.dotted')}</option>
      <option value="double">${translate('design.double')}</option>
      <option value="none">${translate('design.none')}</option>
    `;
    select.value = this.getBorderStyleOfTable(selectedElement);
    select.id = CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_BORDER_STYLE;
    select.onchange = (e: Event): void => this.handleTableBorderStyleChange(e);
    borderType.appendChild(select);

    borderOption.appendChild(borderType);
    accordionContent.appendChild(borderOption);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
    this.fixColorPickerBehaviour();
  };

  getTableBorderWidthOfTable = (): string => {
    let value = '1';
    const selectedElements = this.getSelectedElements();
    if (selectedElements.length <= 1) {
      const [selectedElement] = selectedElements;
      if (!selectedElement) return value;
      let borderWidth;
      const selectedTableElement = Array.from(
        selectedElement.getElementsByTagName('TABLE') as HTMLCollectionOf<HTMLElement>,
      );
      if (selectedTableElement[0])
        borderWidth = getComputedStyle(selectedTableElement[0])?.borderWidth;
      else borderWidth = getComputedStyle(selectedElement).borderWidth;
      if (!borderWidth) return value;
      value = borderWidth.replace('px', '');
      return value;
    }
    return value;
  };

  handleTableBorderWidthChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((tableElement) => {
      const selectedTablelement = Array.from(
        tableElement.getElementsByTagName('TABLE') as HTMLCollectionOf<HTMLElement>,
      )[0];
      let tableBorderStyle = this.getBorderStyleOfTable(tableElement);
      const input = document.getElementById(
        CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_BORDER_STYLE,
      ) as HTMLSelectElement;
      if (input) {
        tableBorderStyle = input.value;
      }
      const style = selectedTablelement ? selectedTablelement.style : tableElement.style;
      style.setProperty('border-width', `${element.value}px`);
      style.setProperty('border-style', tableBorderStyle);
    });
    this.handleSelectedContentChanges();
  };

  getBorderStyleOfTable = (selectedElement: HTMLElement | null): string => {
    let value = 'solid';
    const selectedElements = this.getSelectedElements();
    if (selectedElements.length <= 1) {
      if (!selectedElement) return value;
      let borderStyle;
      const selectedTableElement = Array.from(
        selectedElement.getElementsByTagName('TABLE') as HTMLCollectionOf<HTMLElement>,
      );
      if (selectedTableElement[0]) borderStyle = selectedTableElement[0]?.style.borderStyle;
      else borderStyle = selectedElement.style.borderStyle;
      if (!borderStyle) return value;
      value = borderStyle;
    }
    return value;
  };

  handleTableBorderStyleChange = (e: Event): void => {
    const element = e.target as HTMLInputElement;
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((tableElement) => {
      const selectedTablelement = Array.from(
        tableElement.getElementsByTagName('TABLE') as HTMLCollectionOf<HTMLElement>,
      )[0];
      const style = selectedTablelement ? selectedTablelement.style : tableElement.style;
      style.setProperty('border-style', element.value);
    });
  };

  getBorderColorOfTable = (selectedElement: HTMLElement | null): string => {
    let value = '#a5a5a5';
    const selectedElements = this.getSelectedElements();
    if (selectedElements.length <= 1) {
      if (!selectedElement) return value;
      let borderColor;
      const selectedTableElement = Array.from(
        selectedElement.getElementsByTagName('TABLE') as HTMLCollectionOf<HTMLElement>,
      );
      if (selectedTableElement[0]) borderColor = selectedTableElement[0]?.style.borderColor;
      else borderColor = selectedElement.style.borderColor;
      if (!borderColor) return value;
      value = borderColor;
    }
    return value;
  };

  attachBorderColorPicker = (element: HTMLElement, selectedElement: HTMLElement | null): void => {
    let defaultBorderColor = this.getBorderColorOfTable(selectedElement);
    if (element) {
      const options: COLORPICKERINTERFACE = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_COLOR_PICKERS,
        default: defaultBorderColor,
        defaultRepresentation: 'HEXA',
        useAsButton: true,
        lockOpacity: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            rgba: false,
            hex: true,
            input: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      borderColorPicker = new Pickr(options);

      borderColorPicker.on('show', () => {
        const toHexa = this.rgb2hex(this.getBorderColorOfTable(selectedElement));
        borderColorPicker?.setColor(toHexa, true);
      });

      borderColorPicker.on('change', (color: Pickr.HSVaColor) => {
        const hexValue = color.toHEXA();
        this.handleTableBorderColorChange(hexValue.toString(), element);
      });

      borderColorPicker.on('clear', () => {
        defaultBorderColor = '#a5a5a5';
        this.clearBorderStyles(element);
      });

      borderColorPicker.on('save', (color: Pickr.HSVaColor) => {
        if (!color) return;
        const hexValue = color.toHEXA().toString();
        this.saveBorderStyles(hexValue, element);
        defaultBorderColor = hexValue;
      });

      borderColorPicker.on('cancel', () => {
        this.handleTableBorderColorChange(defaultBorderColor, element);
        borderColorPicker?.hide();
      });
    }
  };

  clearBorderStyles = (element: HTMLElement) => {
    this.handleTableBorderColorChange('#a5a5a5', element);
    const selectedTable = this.getSelectedElement()?.childNodes[0] as HTMLTableElement;
    const tableBorderWidthInput = getEditorElementById(
      'table-design-section-border',
    )?.getElementsByClassName('input')[0] as HTMLInputElement;
    const borderStyle = getEditorElementById(
      CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_BORDER_STYLE,
    ) as HTMLSelectElement;
    tableBorderWidthInput.value = '1';
    borderStyle.selectedIndex = 0;
    selectedTable.style.setProperty('border-width', '1px');
    selectedTable.style.setProperty('border-style', 'Solid');
  };

  saveBorderStyles = (color: string, element: HTMLElement) => {
    this.handleTableBorderColorChange(color, element, 'save');
    borderColorPicker?.hide();
  };

  handleTableBorderColorChange = (color: string, element: HTMLElement, option?: string): void => {
    const selectedElements = this.getSelectedElements();
    if (!selectedElements.length) return;
    selectedElements.forEach((tableElement) => {
      const selectedTablelement = Array.from(
        tableElement.getElementsByTagName('TABLE') as HTMLCollectionOf<HTMLElement>,
      )[0];
      const style = selectedTablelement ? selectedTablelement.style : tableElement.style;
      style.setProperty('border-color', color.toString());
    });
    element.style.backgroundColor = color;
    if (option === 'save') this.handleSelectedContentChanges();
  };

  attachColorOption = (wrapper: HTMLElement): void => {
    const alignmentWrapper = document.createElement('div');
    alignmentWrapper.className = 'accordionItem';
    const accordionButton = document.createElement('button');
    accordionButton.className = 'accordionButton';
    accordionButton.onclick = this.handleAccordionButtonClick;
    accordionButton.type = 'button';
    accordionButton.dataset.toggle = 'collapse';
    accordionButton.dataset.target = `#table-design-section-color`;
    accordionButton.innerHTML = `
      <span class='icon'>${translate('design.plus')}</span>
      <span class="label">${translate('global.color')}</span>
    `;
    alignmentWrapper.appendChild(accordionButton);

    const accordionContent = document.createElement('div');
    accordionContent.className = 'accordionContent collapse';
    accordionContent.id = 'table-design-section-color';
    accordionContent.dataset.parent = `#${CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_ACCORDION_WRAPPER}`;
    const colorOption = document.createElement('div');
    colorOption.className = 'colorOption';

    const backgroundColorOption = document.createElement('div');
    backgroundColorOption.className = 'backgroundColorOption';

    const label = document.createElement('div');
    label.className = 'label';
    label.innerHTML = translate('design.background');
    backgroundColorOption.appendChild(label);

    const color = document.createElement('button');
    color.className = 'color';
    const defaultColor = this.fetchColorPickerBtnColor();
    color.style.backgroundColor = defaultColor || '#ffffff';
    backgroundColorOption.appendChild(color);
    colorOption.appendChild(backgroundColorOption);
    this.attachBackgroundColorPicker(color);
    accordionContent.appendChild(colorOption);
    alignmentWrapper.appendChild(accordionContent);
    wrapper.appendChild(alignmentWrapper);
    this.fixColorPickerBehaviour();
  };

  fetchColorPickerBtnColor = (): string => {
    const btnColor =
      this.getSelectedElement()?.tagName !== 'DIV'
        ? (this.getSelectedElement()?.style.backgroundColor as string)
        : (this.getSelectedElement()?.getElementsByTagName('TABLE')[0] as HTMLTableElement).style
            .backgroundColor;
    return btnColor;
  };

  attachBackgroundColorPicker = (element: HTMLElement): void => {
    let defaultColor = this.fetchColorPickerBtnColor();
    if (element) {
      const options: COLORPICKERINTERFACE = {
        el: element,
        theme: 'nano',
        swatches: customColors,
        appClass: CONSTANTS.LOREE_SIDEBAR_TABLE_DESIGN_SECTION_COLOR_PICKERS,
        default: defaultColor || '#ffffff',
        defaultRepresentation: 'HEXA',
        useAsButton: true,
        lockOpacity: true,
        components: {
          preview: true,
          opacity: false,
          hue: true,
          interaction: {
            hex: true,
            rgba: false,
            input: true,
            clear: true,
            cancel: true,
            save: true,
          },
        },
        i18n: {
          'btn:save': translate('global.apply'),
          'btn:cancel': translate('global.cancel'),
        },
      };
      backgroundColorPicker = new Pickr(options);
      backgroundColorPicker.on('show', () => {
        const toHexa = this.rgb2hex(this.fetchColorPickerBtnColor());
        backgroundColorPicker?.setColor(toHexa, true);
      });
      backgroundColorPicker.on('change', (color: Pickr.HSVaColor) => {
        const hexValue = color.toHEXA();
        this.handleTableBackgroundColorChange(hexValue.toString(), element, 'change');
      });
      backgroundColorPicker.on('save', (color: Pickr.HSVaColor) => {
        const hexValue = color ? color.toHEXA() : '#ffffff';
        this.handleTableBackgroundColorChange(hexValue.toString(), element, 'save');
        defaultColor = this.getSelectedElement()?.style.backgroundColor as string;
        backgroundColorPicker?.hide();
      });
      backgroundColorPicker.on('cancel', () => {
        this.handleTableBackgroundColorChange(defaultColor, element, 'cancel');
        backgroundColorPicker?.hide();
      });
      backgroundColorPicker.on('clear', () => {
        const hexValue = '#ffffff';
        this.handleTableBackgroundColorChange(hexValue.toString(), element, 'clear');
        defaultColor = hexValue.toString();
      });
    }
  };

  handleTableBackgroundColorChange = (color: string, element: HTMLElement, key: string): void => {
    const previousStyle = this.getPreviousElementColorStyles();
    const selectedElements = this.getSelectedElements();
    let getTableElement: HTMLTableElement | null = null;
    if (key === 'cancel') {
      selectedElements.forEach((ele, idx) => {
        getTableElement = ele.getElementsByTagName('TABLE')[0]
          ? (ele.getElementsByTagName('TABLE')[0] as HTMLTableElement)
          : null;
        const element = getTableElement || ele;
        element.style.backgroundColor = previousStyle[idx];
      });
      const buttonColor = this.fetchColorPickerBtnColor();
      element.style.backgroundColor = buttonColor || '#ffffff';
      return;
    }
    if (key === 'save' || key === 'clear') {
      this.storePreviousElementColorStyles(selectedElements);
      this.handleSelectedContentChanges();
    }

    selectedElements.forEach((tableElement) => {
      const getTableElement = tableElement.getElementsByTagName('TABLE')[0] as HTMLTableElement;
      const style = getTableElement ? getTableElement.style : tableElement.style;
      if (color) {
        style.setProperty('background-color', color.toString());
        element.style.backgroundColor = color;
      }
    });
  };

  fixColorPickerBehaviour = (): void => {
    const pcrSelections = document.querySelectorAll('.pcr-selection');
    const pcrResult = document.querySelectorAll('.pcr-result');
    pcrSelections.forEach((selection) => {
      selection.addEventListener('click', () => {
        pcrResult.forEach((input) => {
          const inputField = input as HTMLInputElement;
          inputField.blur();
        });
      });
    });
  };
}

export default TableDesign;
