import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';

import { ExpandableBlock } from './expandableBlock';
import axe from '../../axeHelper';
import CONSTANTS from '../constant';

describe('expandableBlock', () => {
  test('loads and displays greeting collapsed', async () => {
    render(
      <ExpandableBlock
        title='My H5P'
        interactiveId={CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_WRAPPER}
        headingId={CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_HEADING}
        contentId={CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE}
        otherContentId='myInteractives'
        contentParent={CONSTANTS.LOREE_H5P_INTERACTIVES_SECTION}
        initialExpand={false}
      />,
    );

    fireEvent.click(screen.getByText('My H5P'));
    await waitFor(() => screen.getByRole('button'));
    expect(screen.getByRole('button')).toHaveTextContent('IMy H5P');
    // expect(screen.getByText('Loading...')).toBeFalsy(); // driven by CSS styles so may not be doable
  });

  test('expand on load, collapse with mouse click, expand on pressing spacebar', async () => {
    render(
      <ExpandableBlock
        title='My H5P'
        interactiveId={CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_WRAPPER}
        headingId={CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_HEADING}
        contentId={CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE}
        otherContentId='myInteractives'
        contentParent={CONSTANTS.LOREE_H5P_INTERACTIVES_SECTION}
        initialExpand
      />,
    );

    // expand
    await waitFor(() => screen.getByRole('button'));
    expect(screen.getByRole('button')).toHaveTextContent('IMy H5P');
    expect(screen.getByText('Loading...')).toBeInTheDocument();

    // collapse
    fireEvent.click(screen.getByText('My H5P'));
    const contextSection = screen.getByTestId(CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE);
    await waitFor(() => screen.getByRole('button'));
    await waitFor(() => expect(contextSection.getAttribute('class')).toContain('collapse'));
    expect(screen.getByRole('button')).toHaveTextContent('+My H5P');
    expect(screen.getByRole('button').getAttribute('aria-expanded')).toEqual('false');
    expect(contextSection.getAttribute('aria-hidden')).toEqual('true');
    // expect(screen.getByText('Loading...')).not.toBeVisible();

    // expand again but this time with keypress
    fireEvent.keyPress(screen.getByRole('button'), { charCode: 32 });
    await waitFor(() => screen.getByRole('button'));
    expect(screen.getByRole('button')).toHaveTextContent('IMy H5P');
    expect(screen.getByRole('button').getAttribute('aria-expanded')).toEqual('true');
    expect(contextSection.getAttribute('aria-hidden')).toEqual('false');
    await waitFor(() => expect(contextSection.getAttribute('class')).toContain('show'));
    expect(contextSection.getAttribute('class')).toContain('show');
    expect(screen.getByText('Loading...')).toBeVisible();

    // accessibility tests
    const results = await axe(document.body);
    expect(results).toHaveNoViolations();
  });

  // test('load multiple components and on expand of one the other collapses', async () => {
  //   expect(true).toEqual(true);
  // });
});
