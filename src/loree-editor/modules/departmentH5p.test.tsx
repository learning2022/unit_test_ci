/* eslint-disable */ // Remove this line when editing this file
import * as h5p from './h5p';

let accountId: any = null;

const h5pContent = [
  {
    id: '166',
    title: 'Dept Acc',
    created_user: '70',
    account_id: '6',
  },
  {
    id: '164',
    title: 'National Quality Framework - hotspot',
    created_user: '66',
    account_id: '18',
  },
];
const departmentH5ps: any = [];
const getCourseAccountDetail = jest.fn().mockImplementation(() => {
  stall();
  accountId = '6';
});

jest.mock('./h5p', () => ({
  handleWordpressLogin: jest.fn(),
  getCourseAccountDetails: jest.fn(),
  fetchAccountInfo: jest.fn(),
}));

const h5pElement = document.createElement('div');
h5pElement.innerText = 'H5p Interactive';
h5pElement.onclick = () => {
  h5p.fetchAccountInfo();
};

async function stall(stallTime = 3000) {
  await new Promise(resolve => setTimeout(resolve, stallTime));
}

describe('When user clicks h5p for SubAccount fetch', () => {
  test('Trigger h5p calls', () => {
    h5pElement.click();
    expect(h5p.fetchAccountInfo).toHaveBeenCalledTimes(1);
  });

  test('fetch acocunt details', () => {
    expect(accountId).toBeNull();
    getCourseAccountDetail();
    expect(accountId).not.toBeNull();
  });

  test('fetch the my department h5p data', () => {
    getCourseAccountDetail();
    h5pContent.map(item => {
      if (item.account_id === accountId) departmentH5ps.push(item);
    });
    expect(departmentH5ps[0].account_id).toBe('6');
  });
});
