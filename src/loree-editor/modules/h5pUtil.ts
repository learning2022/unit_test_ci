export const showTagFilter = (): boolean => {
  const tagFilterUser = [
    'https://lms.educationapps.vic.gov.au',
    'https://crystaldelta.instructure.com',
  ];
  return tagFilterUser.includes(sessionStorage.getItem('lmsUrl') as string);
};
