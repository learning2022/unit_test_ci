import CONSTANTS from '../../constant';
import Base from '../../base';
import { textOptionIcon } from '../textOptions/UI/icons';
import { editorKeyCode } from '../../../utils/keycodeUtils';
import { getIframedocumentElementById } from '../../utils';
import { hideLinkInfoPopup } from '../../document/linkInfoPopup';
import {
  addLinkToElement,
  getLinkForElement,
  removeLinkFromElement,
} from '../../../loree-document/links';
import { isD2l } from '../../../lmsConfig';
import { hideLinkEditPopup, showLinkEditPopup } from '../../document/linkEditPopup';
import { getIFrameElement } from '../../common/dom';
import { getCurrentSelectedElement } from '../../editorSelection/selection';

const baseClass = () => {
  const base = new Base();
  return base;
};

export const appendExternalLinkOptionWrapper = () => {
  const iframeDocument = baseClass().getDocument();
  if (iframeDocument) {
    const linkOptionWrapper = document.createElement('div');
    linkOptionWrapper.id = CONSTANTS.LOREE_IMAGE_OPTIONS_LINK_CONTENT_WRAPPER;
    linkOptionWrapper.className = 'link-options-alignment-wrapper';
    linkOptionWrapper.style.display = 'none';
    const input = document.createElement('input');
    input.id = CONSTANTS.LOREE_IMAGE_USER_ANCHOR_LINK_INPUT;
    input.className = 'link-options-alignment-wrapper-input-image';
    input.placeholder = 'https://www.crystaldelta.com';
    linkOptionWrapper.appendChild(input);
    appendNewTabCheckbox(linkOptionWrapper);
    iframeDocument.body.appendChild(linkOptionWrapper);
  }
};

export const appendLink = (url: string, selectedElement: HTMLElement) => {
  const urlFormat: Array<string> = ['https://', 'http://', 'ftp://', 'smtp://'];
  const urlFormatCheck = urlFormat.some((prefix: string) => url.includes(prefix));
  if (!urlFormatCheck) url = 'https://' + url;
  urlExpressionCheck(url, selectedElement);
};

const urlExpressionCheck = (url: string, selectedElement: HTMLElement) => {
  const iframeDocument = baseClass().getDocument();
  const checkBox = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_LINK_OPTION_TAB_CHECKBOX_IMAGE,
  ) as HTMLInputElement;
  const input = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_IMAGE_USER_ANCHOR_LINK_INPUT,
  ) as HTMLInputElement;
  const urlExpression = baseClass().urlValidatorUrl();
  if (urlExpression[0].test(url)) {
    addLinkToSelectedElement(selectedElement, url, checkBox.checked);
    baseClass().hideBlockOptionsToSelectedElements();
  } else input.style.borderColor = 'red';
};

const addLinkToSelectedElement = (
  selectedElement: HTMLElement,
  url: string,
  openInNewTab: boolean,
) => {
  // const iframeDocument = baseClass().getDocument();
  hideLinkInfoPopup();

  const anchor = addLinkToElement(selectedElement, url, openInNewTab);
  if (isD2l()) anchor.setAttribute('target', '_blank');
  anchor.onclick = (e) => e.preventDefault();
  anchor.rel = 'noreferrer noopener';
};

export const handleImageLinkOption = (referenceElement: HTMLElement) => {
  const selectedElement = getCurrentSelectedElement();
  const linkData = getLinkForElement(selectedElement);

  let newUrl: string | undefined;
  let newWindowSetting: boolean;

  hideLinkEditPopup();

  showLinkEditPopup(
    referenceElement,
    (url, newWindow) => {
      newUrl = url;
      newWindowSetting = newWindow;
    },
    () => {
      hideLinkEditPopup();
      removeLinkFromElement(selectedElement);
      hideLinkInfoPopup();
    },
    () => {
      hideLinkEditPopup();
      if (selectedElement && newUrl !== undefined) {
        if (!newUrl.startsWith('http')) {
          newUrl = 'https://' + newUrl;
        }
        addLinkToSelectedElement(selectedElement, newUrl, newWindowSetting);
      }
    },
    linkData?.url ?? '',
    linkData?.newWindow ?? false,
    isD2l(),
    getIFrameElement(),
  );
};

export const appendNewTabCheckbox = (linkOptionWrapper: HTMLDivElement) => {
  const removeLink = document.createElement('span');
  removeLink.className = 'link-options-remove-link';
  removeLink.innerHTML = textOptionIcon.linkRemoverIcon;
  removeLink.onclick = () => removeAttachedLinkForImage();
  const openInNewTab = document.createElement('span');
  const newTabCheckBox = document.createElement('input');
  newTabCheckBox.type = 'checkbox';
  newTabCheckBox.className = 'link-options-checkbox';
  newTabCheckBox.id = CONSTANTS.LOREE_LINK_OPTION_TAB_CHECKBOX_IMAGE;
  newTabCheckBox.onclick = () => handleImageLinkCheckBox(newTabCheckBox);
  const newTabLabel = document.createElement('label');
  newTabLabel.innerHTML = 'Open link in new tab';
  newTabLabel.className = 'link-options-checkbox-text';
  openInNewTab.appendChild(newTabCheckBox);
  openInNewTab.appendChild(newTabLabel);
  linkOptionWrapper.appendChild(removeLink);
  linkOptionWrapper.appendChild(openInNewTab);
};

export const handleImageLinkCheckBox = (checkBox: HTMLInputElement) => {
  const iframeDocument = baseClass().getDocument();
  const selectedElement = baseClass().getSelectedElement();
  const input = iframeDocument?.getElementById(
    CONSTANTS.LOREE_IMAGE_USER_ANCHOR_LINK_INPUT,
  ) as HTMLInputElement;
  const openInNewTab = checkBox.checked;
  if (openInNewTab && selectedElement?.parentElement) {
    if (selectedElement?.parentElement?.tagName === 'A') {
      selectedElement?.parentElement.setAttribute('target', 'blank');
    }
  } else if (!openInNewTab) {
    if (selectedElement?.parentElement?.tagName === 'A') {
      selectedElement?.parentElement.removeAttribute('target');
    }
  }
  if (input.value !== '') {
    appendLink(input.value, selectedElement as HTMLElement);
  }
};

export const externalLinkCheckBoxCheck = (selectedElement: HTMLElement | null) => {
  const iframeDocument = baseClass().getDocument();
  const checkBox = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_LINK_OPTION_TAB_CHECKBOX_IMAGE,
  ) as HTMLInputElement;
  if (selectedElement?.parentElement?.tagName === 'A') {
    const anchorTag = (selectedElement?.parentElement as HTMLAnchorElement).target;
    if (anchorTag) {
      checkBox.checked = true;
    } else {
      checkBox.checked = false;
    }
  }
};

export const openExternalinkOptionValuesCheck = () => {
  const iframeDocument = baseClass().getDocument();
  const selectedElement = baseClass().getSelectedElement();
  const input = iframeDocument?.getElementById(
    CONSTANTS.LOREE_IMAGE_USER_ANCHOR_LINK_INPUT,
  ) as HTMLInputElement;
  const checkBox = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_LINK_OPTION_TAB_CHECKBOX_IMAGE,
  ) as HTMLInputElement;
  if (selectedElement?.parentElement?.tagName === 'DIV') {
    checkBox.checked = false;
  }
  baseClass().disableNewTabLinkForD2L(checkBox);
  input.style.borderColor = '';
  input.value = inputValueCheck(selectedElement);
  input.addEventListener('keydown', (e: KeyboardEvent) => {
    if (e.keyCode === editorKeyCode.KEYENTER) e.preventDefault();
  });
};

const inputValueCheck = (selectedElement: HTMLElement | null) => {
  return selectedElement?.parentElement?.tagName === 'A'
    ? (selectedElement?.parentElement as HTMLAnchorElement).href
    : '';
};

export const removeAttachedLinkForImage = () => {
  const selectedElement = baseClass().getSelectedElement() as HTMLElement;
  const iframeDocument = baseClass().getDocument();
  const input = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_IMAGE_USER_ANCHOR_LINK_INPUT,
  ) as HTMLInputElement;
  input.value = '';
  input.style.borderColor = '';
  const parent = selectedElement.parentElement;
  if (selectedElement?.parentElement?.tagName === 'A') {
    if (parent) {
      parent.outerHTML = selectedElement.outerHTML;
    }
  } else if (selectedElement?.parentNode?.parentElement?.tagName === 'A') {
    if (parent?.parentElement) {
      parent.parentElement.outerHTML = parent?.outerHTML;
    }
  }
  baseClass().closeImageLinkOption();
  baseClass().hideAnchorOption();
};

export const specialBlockImageSizeAdjustment = (
  anchorTagElement: HTMLElement,
  imgWrapper: HTMLElement,
) => {
  if (imgWrapper?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT)) {
    anchorTagElement.className = CONSTANTS.LOREE_IFRAME_CONTENT_ICON_TEXT;
    anchorTagElement.style.maxWidth = CONSTANTS.LOREE_SPECIAL_BLOCK_ICON_IMAGE_STATIC_SIZE;
    imgWrapper.style.maxWidth = '100%';
  }
};
