import {
  addLinkToElement,
  getLinkForElement,
  removeLinkFromElement,
} from '../../../loree-document/links';
import { mocked } from '../../../testUtils';
import { hideLinkEditPopup, showLinkEditPopup } from '../../document/linkEditPopup';
import { getCurrentSelectedElement } from '../../editorSelection/selection';
import { handleImageLinkOption } from './index';
import * as imageLinkWrapper from './index';
import { getEditorElementsByClassName } from '../../utils';

jest.mock('../../document/linkEditPopup');
jest.mock('../../editorSelection/selection');
jest.mock('../../../loree-document/links');

describe('#specialElement Size Adjustment', () => {
  let element;
  let anchorTag: HTMLElement;
  let divWrapper: HTMLDivElement;
  beforeEach(() => {
    element = document.createElement('div');
    anchorTag = document.createElement('a');
    divWrapper = document.createElement('div');
    divWrapper.className = 'loree-iframe-content-icon-text';
    anchorTag.append(divWrapper);
    element.append(anchorTag);
    document.body.append(element);
  });
  test('image max-width change to div wrapper to anchor element', () => {
    imageLinkWrapper.specialBlockImageSizeAdjustment(anchorTag, divWrapper);
    expect(getEditorElementsByClassName('loree-iframe-content-icon-text')[0]).toHaveAttribute(
      'style',
      'max-width: 20%;',
    );
  });
  test('image wrapper max-width change to full max-width', () => {
    imageLinkWrapper.specialBlockImageSizeAdjustment(anchorTag, divWrapper);
    expect(getEditorElementsByClassName('loree-iframe-content-icon-text')[1]).toHaveAttribute(
      'style',
      'max-width: 100%;',
    );
  });
});

describe('handleImageLInkOption', () => {
  test('is shown', () => {
    const button = document.createElement('button');
    mocked(getLinkForElement).mockReturnValue({ url: 'http://x.com/', newWindow: false });

    handleImageLinkOption(button);
    expect(hideLinkEditPopup).toHaveBeenCalled();
    expect(showLinkEditPopup).toHaveBeenCalled();
  });

  test('can remove the link', () => {
    const button = document.createElement('button');
    const div = document.createElement('div');
    mocked(getCurrentSelectedElement).mockReturnValue(div);
    mocked(getLinkForElement).mockReturnValue({ url: 'http://x.com/', newWindow: false });

    (showLinkEditPopup as jest.MockedFunction<typeof showLinkEditPopup>).mockImplementation(
      (_e, _set, remove) => {
        remove();
      },
    );

    handleImageLinkOption(button);
    expect(removeLinkFromElement).toHaveBeenCalledWith(div);
  });

  test('can update the link', () => {
    const button = document.createElement('button');
    const div = document.createElement('div');
    mocked(getCurrentSelectedElement).mockReturnValue(div);
    mocked(getLinkForElement).mockReturnValue({ url: 'http://x.com/', newWindow: false });

    (showLinkEditPopup as jest.MockedFunction<typeof showLinkEditPopup>).mockImplementation(
      (_e, set, r, c) => {
        set('abc.com/', true);
        c();
      },
    );
    mocked(addLinkToElement).mockImplementation((div) => {
      return document.createElement('a');
    });

    handleImageLinkOption(button);
    expect(addLinkToElement).toHaveBeenCalledWith(div, 'https://abc.com/', true);
  });
});
