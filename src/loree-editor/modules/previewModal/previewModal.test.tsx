import { PreviewModal } from './previewModal';
import CONSTANTS from '../../constant';

describe('Loree app preview modal', () => {
  beforeEach(() => {
    const loreePreview = document.createElement('div');
    loreePreview.id = CONSTANTS.LOREE_PREVIEW_MODAL_DIALOG;
    document.body.append(loreePreview);
  });
  test('Load preview modal', () => {
    const expectedResult = `<link rel="stylesheet" href="/stylesheets/bootstrapStyle.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> 
  `;
    const previewModalInstance = new PreviewModal();
    previewModalInstance.initiate();
    previewModalInstance.previewModalEventHandlers();

    const previewIframe: HTMLIFrameElement | null = document.getElementById(
      'loree-preview',
    ) as HTMLIFrameElement;
    const previewModal = previewIframe.contentDocument;
    expect(previewModal?.head.innerHTML).toBe(expectedResult);
  });
});
