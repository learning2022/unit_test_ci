import Base from '../../base';
import CONSTANTS from '../../constant';
import { headerIcon } from '../header/headerIcon';
import { translate } from '../../../i18n/translate';

export class PreviewModal extends Base {
  initiate = (): void => {
    const previewModalDialog = document.getElementById(CONSTANTS.LOREE_PREVIEW_MODAL_DIALOG);
    const previewModalContent: HTMLElement = document.createElement('div');
    previewModalContent.className = 'modal-content';
    previewModalContent.id = CONSTANTS.LOREE_PREVIEW_MODAL_CONTENT;
    let innerHTML = this.appendModalContentHeader();
    innerHTML += this.appendModalContentBody();
    previewModalContent.innerHTML = innerHTML;
    if (previewModalDialog) previewModalDialog.appendChild(previewModalContent);
    this.appendPreviewIframeContent();
  };

  appendModalContentHeader = (): string => {
    return `
  <div id=${
    CONSTANTS.LOREE_PREVIEW_MODAL_CONTENT_HEADER
  } class="d-flex flex-row flex-wrap justify-content-between align-items-center mx-3">
    <h5 class="modal-title">${translate('global.preview')}</h5>
    <div class="d-flex preview-device-container">
      <div class="p-2 preview-active-device" id="${CONSTANTS.LOREE_PREVIEW_MODAL_DESKTOP_VIEW}">${
      headerIcon.desktopIcon
    }</div>
      <div class="p-2" id="${CONSTANTS.LOREE_PREVIEW_MODAL_TABLET_VIEW}">${
      headerIcon.tabletIcon
    }</div>
      <div class="p-2" id="${CONSTANTS.LOREE_PREVIEW_MODAL_MOBILE_VIEW}">${
      headerIcon.mobileIcon
    }</div>
    </div>
    <button id=${CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER_CLOSE} type="button">
    ${translate('modal.exitpreview')}
    </button>
  </div>
`;
  };

  appendModalContentBody = (): string => {
    return `
    <div id=${CONSTANTS.LOREE_PREVIEW_MODAL_CONTENT_BODY}>
        <iframe id="loree-preview">
        </iframe>
    </div>
  `;
  };

  appendPreviewIframeContent = (): void => {
    const previewIframe: HTMLIFrameElement | null = document.getElementById(
      'loree-preview',
    ) as HTMLIFrameElement;
    if (previewIframe) {
      const previewModal = previewIframe.contentDocument;
      previewModal?.open();
      previewModal?.write(this.iframeContent);
      previewModal?.close();
    }
  };

  previewModalEventHandlers = (): void => {
    const previewModalCloseButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER_CLOSE,
    ) as HTMLButtonElement;
    if (previewModalCloseButton) this.previewCloseButtonHandler(previewModalCloseButton);
    const previewModalMobileViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_MOBILE_VIEW,
    ) as HTMLElement;
    const previewModalTabletViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_TABLET_VIEW,
    ) as HTMLElement;
    const previewModalDesktopViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_DESKTOP_VIEW,
    ) as HTMLElement;
    if (previewModalMobileViewButton) this.previewMobileViewHandler(previewModalMobileViewButton);
    if (previewModalTabletViewButton) this.previewTabletViewHandler(previewModalTabletViewButton);
    if (previewModalDesktopViewButton)
      this.previewDesktopViewHandler(previewModalDesktopViewButton);
  };

  previewEditorMobileView = (): void => {
    const iframeViewPort = document.getElementById('loree-preview');
    const previewModalMobileViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_MOBILE_VIEW,
    ) as HTMLElement;
    const previewModalTabletViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_TABLET_VIEW,
    ) as HTMLElement;
    const previewModalDesktopViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_DESKTOP_VIEW,
    ) as HTMLElement;
    iframeViewPort?.classList.add('editor-mobile-preview');
    iframeViewPort?.classList.remove('editor-tablet-preview', 'editor-desktop-preview');
    previewModalMobileViewButton.classList.add('preview-active-device');
    previewModalTabletViewButton.classList.remove('preview-active-device');
    previewModalDesktopViewButton.classList.remove('preview-active-device');
  };

  previewEditorTabletView = (): void => {
    const iframeViewPort = document.getElementById('loree-preview');
    const previewModalTabletViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_TABLET_VIEW,
    ) as HTMLElement;
    const previewModalDesktopViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_DESKTOP_VIEW,
    ) as HTMLElement;
    const previewModalMobileViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_MOBILE_VIEW,
    ) as HTMLElement;
    iframeViewPort?.classList.add('editor-tablet-preview');
    iframeViewPort?.classList.remove('editor-mobile-preview', 'editor-desktop-preview');
    previewModalTabletViewButton.classList.add('preview-active-device');
    previewModalMobileViewButton.classList.remove('preview-active-device');
    previewModalDesktopViewButton.classList.remove('preview-active-device');
  };

  previewEditorDesktopView = (): void => {
    const iframeViewPort = document.getElementById('loree-preview');
    const previewModalDesktopViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_DESKTOP_VIEW,
    ) as HTMLElement;
    const previewModalTabletViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_TABLET_VIEW,
    ) as HTMLElement;
    const previewModalMobileViewButton = document.getElementById(
      CONSTANTS.LOREE_PREVIEW_MODAL_MOBILE_VIEW,
    ) as HTMLElement;
    iframeViewPort?.classList.add('editor-desktop-preview');
    iframeViewPort?.classList.remove('editor-mobile-preview', 'editor-tablet-preview');
    previewModalDesktopViewButton.classList.add('preview-active-device');
    previewModalTabletViewButton.classList.remove('preview-active-device');
    previewModalMobileViewButton.classList.remove('preview-active-device');
  };

  previewCloseButtonHandler = (previewModalCloseButton: HTMLButtonElement): void => {
    previewModalCloseButton.onclick = () => this.hidePreviewModal();
  };

  previewMobileViewHandler = (previewModalMobileViewButton: HTMLElement): void => {
    previewModalMobileViewButton.onclick = () => this.previewEditorMobileView();
  };

  previewTabletViewHandler = (previewModalTabletViewButton: HTMLElement): void => {
    previewModalTabletViewButton.onclick = () => this.previewEditorTabletView();
  };

  previewDesktopViewHandler = (previewModalDesktopViewButton: HTMLElement): void => {
    previewModalDesktopViewButton.onclick = () => this.previewEditorDesktopView();
  };

  iframeContent = `
  <link rel="stylesheet" href="/stylesheets/bootstrapStyle.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"> 
  <div id="previewContent"></div>
  `;
}
