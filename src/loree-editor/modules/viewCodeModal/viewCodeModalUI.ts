import CONSTANTS from '../../constant';
import { closeIcon } from '../sidebar/iconHolder';
import { translate } from '../../../i18n/translate';

export const appendModalContentHeader = (): string => {
  return `
  <div id=${
    CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_HEADER
  } class="d-flex flex-row justify-content-between flex-wrap align-items-center">
    <h5 class="modal-title text-primary">${translate('modal.codeproperties')}</h5>
    <button id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_WRAPPER_CLOSE} type="button" class="close">
      ${closeIcon}
    </button>
  </div>
`;
};

export const appendModalContentHeaderDivider = (): string => {
  return `
  <div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_DIVIDER}></div>
  `;
};

export const appendModalContentBody = (): string => {
  return `
    <div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY} class="d-flex flex-column flex-nowrap">
      <div id=${
        CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_HIERARCHY_SECTION
      } class="d-flex flex-row flex-nowrap justify-content-between w-100">
        <div id=${
          CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ELEMENT
        } class="d-none d-flex flex-column flex-fill"><div></div><p>${translate(
    'global.element',
  )}</p></div>
        <div id=${
          CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_COLUMN
        } class="d-none d-flex flex-column flex-fill"><div></div><p>${translate(
    'global.column',
  )}</p></div>
        <div id=${
          CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ROW
        } class="d-none d-flex flex-column flex-fill"><div></div><p>${translate(
    'global.row',
  )}</p></div>
        <div id=${
          CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_PAGE
        } class="d-none d-flex flex-column flex-fill"><div></div><p>${translate(
    'global.page',
  )}</p></div>
      </div>
      <div class="d-flex flex-row flex-nowrap justify-content-between w-100">
        <div class="d-flex flex-column flex-nowrap loree-code-viewer-editor-container">
          <div class="d-flex flex-row flex-nowrap loree-code-viewer-editor-header">
           <p class="flex-grow-1">${translate('modal.htmlcode')}</p>
           <button id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_HTML_CODE_COPY_BUTTON}>${translate(
    'modal.copycode',
  )}</button>
          </div>
          <div class="loree-code-viewer-editor-divider"></div>
          <div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_HTML_CODE_EDITOR}>
            <textarea id=${
              CONSTANTS.LOREE_VIEW_CODE_MODAL_HTML_CODE_TEXTAREA
            } style="display: none;"></textarea>
          </div>
        </div>
        <div class="d-flex flex-column flex-nowrap loree-code-viewer-editor-container">
         <div class="d-flex flex-row flex-nowrap loree-code-viewer-editor-header">
          <p class="flex-grow-1">${translate('modal.csscode')}</p>
          <button id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_CSS_CODE_COPY_BUTTON}>${translate(
    'modal.copycode',
  )}</button>
         </div>
         <div class="loree-code-viewer-editor-divider"></div>
         <div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_CSS_CODE_EDITOR}>
          <textarea id=${
            CONSTANTS.LOREE_VIEW_CODE_MODAL_CSS_CODE_TEXTAREA
          } style="display: none;"></textarea>
         </div>
       </div>
      </div>
    </div>
  `;
};

export const appendFooterSetButtons = (): string => {
  return `
    <button id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_CANCEL} class="mx-1">${translate(
    'global.cancel',
  )}</button>
    <div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_EXPORT_BUTTON_WRAPPER}></div>
    <button id=${
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_EDIT
    } class="editor-btn-primary mx-1">${translate('global.edit')}</button>`;
};

export const appendModalContentFooter = (): string => {
  return `
  <div id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER} class="d-flex flex-row flex-nowrap">
    <div id=${
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER_BUTTON_WRAPPER
    } style="display:flex;margin: auto">
      ${appendFooterSetButtons()}
    </div>
  </div>
  `;
};

export const appendUpdateSetButtons = (): string => {
  return `
    <button id=${CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_CANCEL_UPDATE} class="mx-1">${translate(
    'modal.dontupdate',
  )}</button>
    <button id=${
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_UPDATE
    } class="editor-btn-primary mx-1">${translate('global.update')}</button>
    `;
};
