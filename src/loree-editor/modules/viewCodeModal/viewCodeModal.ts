/* eslint-disable prefer-regex-literals */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-case-declarations */
import 'codemirror/lib/codemirror.css';
import 'codemirror/mode/htmlmixed/htmlmixed.js';
import 'codemirror/mode/css/css.js';
import 'codemirror/addon//edit/closetag.js';
import Base from '../../base';
import CONSTANTS from '../../constant';
import juice from 'juice';
import { savePageAlert, savePageAlertContainerDisapper } from '../../alert';
import { handleDownloadPageExport } from '../../../../src/utils/export';
import { generateDynamicClassNameForElement } from '../../utils';
import {
  appendModalContentHeader,
  appendModalContentHeaderDivider,
  appendModalContentBody,
  appendModalContentFooter,
  appendFooterSetButtons,
  appendUpdateSetButtons,
} from './viewCodeModalUI';
import { translate } from '../../../i18n/translate';

const CodeMirror = require('codemirror/lib/codemirror.js');
const htmlBeautify = require('js-beautify').html;
const cssBeautify = require('js-beautify').css;
const closeIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="7.586" height="7.587" viewBox="0 0 7.586 7.587">
<path id="close" d="M-5641.125-5116.2l-2.672-2.673-2.673,2.673a.657.657,0,0,1-.929,0,.657.657,0,0,1,0-.929l2.673-2.673-2.673-2.673a.656.656,0,0,1,0-.928.659.659,0,0,1,.929,0l2.673,2.673,2.672-2.673a.658.658,0,0,1,.928,0,.655.655,0,0,1,0,.928l-2.673,2.673,2.673,2.673a.656.656,0,0,1,0,.929.654.654,0,0,1-.464.192A.656.656,0,0,1-5641.125-5116.2Z" transform="translate(5647.591 5123.591)"/>
</svg>`;
let htmlCodeMirrorEditorInstance: any = null;
let cssCodeMirrorEditorInstance: any = null;
let htmlCode = '';
let cssCode = '';
let selectedBlockType = '';
let copyOfSelectedElement: HTMLElement | null;
let currentHierarchySectionBlock: HTMLElement | null;
let clonedHierarchySectionBlock: HTMLElement | null;
let temporaryPageDiv: HTMLElement | null;

export class ViewCodeModal extends Base {
  initiate = (): void => {
    const viewCodeModalDialog = document.getElementById(CONSTANTS.LOREE_VIEW_CODE_MODAL_DIALOG);
    const viewCodeModalContentTemplate: HTMLElement = document.createElement('div');
    viewCodeModalContentTemplate.className = 'modal-content';
    viewCodeModalContentTemplate.id = CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT;
    let innerHTML = appendModalContentHeader();
    innerHTML += appendModalContentHeaderDivider();
    innerHTML += appendModalContentBody();
    innerHTML += appendModalContentFooter();
    viewCodeModalContentTemplate.innerHTML = innerHTML;
    if (viewCodeModalDialog) viewCodeModalDialog.appendChild(viewCodeModalContentTemplate);
  };

  viewCodeModalEventHandlers = (): void => {
    this.footerSetButtonsHandler();
    const viewCodeButton = document.getElementById(
      CONSTANTS.LOREE_HEADER_REVEAL_CODE_BUTTON,
    ) as HTMLButtonElement;
    const viewCodeModalCloseButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_WRAPPER_CLOSE,
    ) as HTMLButtonElement;
    const viewCodeModalElementButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ELEMENT,
    ) as HTMLElement;
    const viewCodeModalColumnButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_COLUMN,
    ) as HTMLElement;
    const viewCodeModalRowButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ROW,
    ) as HTMLElement;
    const viewCodeModalPageButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_PAGE,
    ) as HTMLElement;
    const viewCodeModalHtmlCodeCopyButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_HTML_CODE_COPY_BUTTON,
    ) as HTMLButtonElement;
    const viewCodeModalCssCodeCopyButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_CSS_CODE_COPY_BUTTON,
    ) as HTMLButtonElement;

    if (viewCodeButton) viewCodeButton.onclick = () => this.showViewCodeModal();

    if (viewCodeModalHtmlCodeCopyButton)
      viewCodeModalHtmlCodeCopyButton.onclick = () =>
        this.copyCodeToClipBoard(htmlCodeMirrorEditorInstance.getValue());

    if (viewCodeModalCssCodeCopyButton)
      viewCodeModalCssCodeCopyButton.onclick = () =>
        this.copyCodeToClipBoard(cssCodeMirrorEditorInstance.getValue());

    if (viewCodeModalCloseButton) viewCodeModalCloseButton.onclick = () => this.hideViewCodeModal();

    if (viewCodeModalElementButton)
      viewCodeModalElementButton.onclick = () => this.showElementBlockCode();

    if (viewCodeModalColumnButton)
      viewCodeModalColumnButton.onclick = () => this.showColumnBlockCode();

    if (viewCodeModalRowButton) viewCodeModalRowButton.onclick = () => this.showRowBlockCode();

    if (viewCodeModalPageButton) viewCodeModalPageButton.onclick = () => this.showPageBlockCode();
  };

  closeWarningAlert = (): void => {
    const viewCodeAlert = document.getElementById(CONSTANTS.LOREE_VIEW_CODE_MODAL_EDIT_ALERT);
    if (viewCodeAlert) {
      viewCodeAlert.remove();
    }
  };

  codeEditWarningAlert = (): void => {
    const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
    if (loreeWrapper) {
      const editModeAlertWrapper = document.createElement('div');
      editModeAlertWrapper.className = 'editor-alert-modal';
      editModeAlertWrapper.style.display = 'flex';
      editModeAlertWrapper.id = CONSTANTS.LOREE_VIEW_CODE_MODAL_EDIT_ALERT;
      const editAlertContent = document.createElement('div');
      editAlertContent.className = 'col-11 col-md-6 col-xl-4 modal-content px-0';
      // Header
      const alertHeader = document.createElement('div');
      alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
      alertHeader.innerHTML = `<h5 class="modal-title text-primary">${translate(
        'modal.warning',
      )}</h5>`;
      const alertCloseButton = document.createElement('div');
      alertCloseButton.className = 'btn-icon';
      alertCloseButton.innerHTML = closeIcon;
      alertCloseButton.onclick = (): void => this.closeWarningAlert();
      alertHeader.appendChild(alertCloseButton);
      // Body
      const alertBody = document.createElement('div');
      alertBody.className = 'modal-body';
      const message1 = document.createElement('p');
      message1.innerHTML = translate('modal.areyousureyouwanttoedit');
      const message2 = document.createElement('p');
      message2.innerHTML = translate('modal.noteeditingcodemayimpactexistingelements');
      // Footer
      const alertFooter = document.createElement('div');
      alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
      const modalYes = document.createElement('button');
      modalYes.className = 'editor-btn-primary mx-1';
      modalYes.innerHTML = translate('global.continue');
      modalYes.id = 'deleteYesButton';
      modalYes.onclick = (): void => this.editCodeHandler();
      const modalNo = document.createElement('button');
      modalNo.className = 'mx-1';
      modalNo.innerHTML = translate('global.cancel');
      modalNo.onclick = (): void => this.closeWarningAlert();
      editAlertContent.appendChild(alertHeader); // header append
      editAlertContent.appendChild(alertBody);
      editAlertContent.appendChild(alertFooter);
      alertBody.appendChild(message1);
      alertBody.appendChild(message2);
      alertFooter.appendChild(modalNo);
      alertFooter.appendChild(modalYes);
      editModeAlertWrapper.appendChild(editAlertContent);
      loreeWrapper.appendChild(editModeAlertWrapper);
    }
  };

  footerSetButtonsHandler = (): void => {
    const viewCodeModalCancelButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_CANCEL,
    ) as HTMLButtonElement;
    const viewCodeModalEditButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_EDIT,
    ) as HTMLButtonElement;
    if (viewCodeModalCancelButton)
      viewCodeModalCancelButton.onclick = () => this.hideViewCodeModal();
    if (viewCodeModalEditButton)
      viewCodeModalEditButton.onclick = () => this.codeEditWarningAlert();
  };

  appendExportToZipButton = (type: string) => {
    const buttonContainer = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EXPORT_BUTTON_WRAPPER,
    ) as HTMLElement;
    switch (type) {
      case 'PAGE':
        buttonContainer.innerHTML = `<button id=${
          CONSTANTS.LOREE_VIEW_CODE_MODAL_EXPORT_BUTTON
        } type="button" class="mx-1">${translate('modal.export')}</button>`;
        const exportButton = document.getElementById(
          CONSTANTS.LOREE_VIEW_CODE_MODAL_EXPORT_BUTTON,
        ) as HTMLButtonElement;
        if (exportButton)
          exportButton.onclick = () =>
            handleDownloadPageExport(
              htmlCodeMirrorEditorInstance.getValue(),
              cssCodeMirrorEditorInstance.getValue(),
            );
        break;
      default:
        buttonContainer.innerHTML = `<button id=${
          CONSTANTS.LOREE_VIEW_CODE_MODAL_DISABLED_EXPORT_BUTTON
        } class="btn-secondary-disable mx-1">${translate('modal.export')}</button>`;
        const disabledExportButton = document.getElementById(
          CONSTANTS.LOREE_VIEW_CODE_MODAL_DISABLED_EXPORT_BUTTON,
        ) as HTMLButtonElement;
        if (disabledExportButton) disabledExportButton.disabled = true;
    }
  };

  updateSetButtonsHandler = (): void => {
    const updatebutton = document.getElementById(CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_UPDATE);
    const viewCodeModalCancelUpdateButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_EDITOR_CANCEL_UPDATE,
    ) as HTMLButtonElement;
    if (updatebutton) updatebutton.onclick = () => this.updateHtmlCodeHandler();
    if (viewCodeModalCancelUpdateButton)
      viewCodeModalCancelUpdateButton.onclick = () => this.cancelEditCodeHandler();
  };

  htmlFormatSelection = (code: any) => {
    const formatedHTML = htmlBeautify(code, { indent_size: 2 });
    return formatedHTML;
  };

  cssFormatSelection = (code: any) => {
    const formatedCSS = cssBeautify(code, { indent_size: 2 });
    return formatedCSS;
  };

  setViewCodeModalDisplay = (): void => {
    const viewCodeModalBackground = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_BACKGROUND,
    ) as HTMLElement;
    const viewCodeModal = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_WRAPPER,
    ) as HTMLElement;
    viewCodeModalBackground.style.display = 'block';
    viewCodeModal.style.display = 'block';
  };

  resetViewCodeModalDisplay = (): void => {
    const viewCodeModalBackground = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_BACKGROUND,
    ) as HTMLElement;
    const viewCodeModal = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_WRAPPER,
    ) as HTMLElement;
    viewCodeModalBackground.style.display = 'none';
    viewCodeModal.style.display = 'none';
  };

  initializeHtmlCodeEditor = (htmlCode: string): void => {
    const htmlCodeTextArea: any = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_HTML_CODE_TEXTAREA,
    );
    if (htmlCodeTextArea) {
      if (htmlCodeMirrorEditorInstance !== null) htmlCodeMirrorEditorInstance.toTextArea();
      htmlCodeMirrorEditorInstance = CodeMirror.fromTextArea(htmlCodeTextArea, {
        mode: 'htmlmixed',
        lineNumbers: true,
        lineWrapping: true,
        autoCloseTags: true,
        readOnly: true,
      });
      htmlCodeMirrorEditorInstance.setValue(this.htmlFormatSelection(htmlCode));
      htmlCodeMirrorEditorInstance.setSize('440', '346');
    }
  };

  initializeCssCodeEditor = (cssCode: string): void => {
    const cssCodeTextArea: any = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CSS_CODE_TEXTAREA,
    );
    if (cssCodeTextArea) {
      if (cssCodeMirrorEditorInstance !== null) cssCodeMirrorEditorInstance.toTextArea();
      cssCodeMirrorEditorInstance = CodeMirror.fromTextArea(cssCodeTextArea, {
        mode: 'css',
        lineNumbers: true,
        lineWrapping: true,
        autoCloseTags: true,
        readOnly: true,
      });
      cssCodeMirrorEditorInstance.setValue(this.cssFormatSelection(cssCode));
      cssCodeMirrorEditorInstance.setSize('440', '346');
    }
  };

  getStyle = (className: any): string | undefined => {
    let cssText = '';
    const cssStyleSheetIdArray = [
      CONSTANTS.LOREE_IFRAME_STYLE_WRAPPER,
      CONSTANTS.LOREE_IFRAME_EXTERNAL_CANVAS_STYLE_WRAPPER,
    ];
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    for (let x = 0; x < cssStyleSheetIdArray.length; x++) {
      const style = iframeDocument.getElementById(cssStyleSheetIdArray[x]) as HTMLStyleElement;
      if (!style) return;
      const sheet = style.sheet as CSSStyleSheet;
      if (!sheet) return;
      const cssRules = sheet.rules || sheet.cssRules;
      for (let x = 0; x < cssRules.length; x++) {
        const rule = cssRules[x] as CSSStyleRule;
        if (rule.selectorText === '.' + className) {
          cssText += rule.cssText || rule.style.cssText;
        }
      }
    }
    return cssText;
  };

  addNewCssText = (iframeDocument: HTMLDocument, className: any, attr: string): void => {
    const loreeStyleSheet = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_STYLE_WRAPPER);
    if (loreeStyleSheet) loreeStyleSheet.innerHTML += '.' + className + ' { ' + attr + ' }';
  };

  updateExistingCssText = (className: any, attr: string): void => {
    const activities: any[] = [];
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const style = iframeDocument.getElementById(
      CONSTANTS.LOREE_IFRAME_STYLE_WRAPPER,
    ) as HTMLStyleElement;
    if (!style) return;
    const sheet = style.sheet as CSSStyleSheet;
    if (!sheet) return;
    const cssRules = sheet.rules || sheet.cssRules;
    for (let x = 0; x < cssRules.length; x++) {
      const rule = cssRules[x] as CSSStyleRule;
      if (!rule.selectorText) return;
      activities.push(rule.selectorText);
    }
    if (activities.includes(`.${className}`)) {
      for (let x = 0; x < cssRules.length; x++) {
        const rule = cssRules[x] as CSSStyleRule;
        if (rule.selectorText === `.${className}`) {
          rule.style.cssText += attr;
        }
      }
    } else {
      this.addNewCssText(iframeDocument, className, attr);
    }
  };

  addDynamicClass = (selectedBlock: any) => {
    const attrs = selectedBlock?.style?.cssText;
    selectedBlock.style = '';
    if (attrs !== '' && attrs !== undefined && typeof attrs === 'string') {
      const className = generateDynamicClassNameForElement(selectedBlock);
      if (!selectedBlock.classList.contains(className)) {
        selectedBlock.classList.add(className);
        const iframeElement = this.getDocument();
        if (iframeElement) {
          const loreeStyleSheet = iframeElement.getElementById(
            CONSTANTS.LOREE_IFRAME_STYLE_WRAPPER,
          );
          if (loreeStyleSheet) loreeStyleSheet.innerHTML += '.' + className + ' { ' + attrs + ' }';
        }
      } else {
        this.updateExistingCssText(className, attrs);
      }
    }
  };

  checkRowInlineStyle = (selectedBlock: HTMLElement) => {
    const sampleArray: any = [];
    if (selectedBlock) {
      sampleArray.push(selectedBlock);
      for (let i = 0; i < selectedBlock?.childNodes.length; i++) {
        sampleArray.push(selectedBlock?.childNodes[i]);
      }
      for (let j = 0; j < sampleArray.length; j++) {
        this.checkColumnInlineStyle(sampleArray[j]);
      }
    }
  };

  checkColumnInlineStyle = (selectedBlock: HTMLElement) => {
    const sampleArray: any = [];
    if (selectedBlock) {
      sampleArray.push(selectedBlock);
      for (let i = 0; i < selectedBlock?.childNodes.length; i++) {
        sampleArray.push(selectedBlock?.childNodes[i]);
      }
      for (let j = 0; j < sampleArray.length; j++) {
        this.checkElementInlineStyle(sampleArray[j]);
      }
    }
  };

  checkElementInlineStyle = (selectedBlock: HTMLElement) => {
    const elementBlockChildArray: any = [];
    if (selectedBlock.nodeType !== 3 && selectedBlock.nodeType !== 8) {
      if (selectedBlock.children.length > 0) {
        this.addDynamicClass(selectedBlock);
        selectedBlock?.childNodes?.forEach((element) => {
          elementBlockChildArray.push(element);
          element?.childNodes?.forEach((elements) => {
            elementBlockChildArray.push(elements);
            elements?.childNodes?.forEach((childElements) => {
              elementBlockChildArray.push(childElements);
              childElements?.childNodes?.forEach((superChildElements) => {
                elementBlockChildArray.push(superChildElements);
                superChildElements?.childNodes?.forEach((superSuperChildElements) => {
                  elementBlockChildArray.push(superSuperChildElements);
                });
              });
            });
          });
        });
      } else {
        elementBlockChildArray.push(selectedBlock);
      }
    }
    if (elementBlockChildArray) {
      for (let i = 0; i < elementBlockChildArray.length; i++) {
        this.addDynamicClass(elementBlockChildArray[i]);
      }
    }
  };

  createClone = (selectedBlock: any) => {
    const clonedElement = selectedBlock.cloneNode(true);
    return clonedElement;
  };

  removeInlineStyle = (currentHierarchySectionBlock: HTMLElement | null, type: string): string => {
    let code = '';
    if (currentHierarchySectionBlock)
      clonedHierarchySectionBlock = this.createClone(currentHierarchySectionBlock) as HTMLElement;
    if (clonedHierarchySectionBlock) {
      switch (type) {
        case 'ELEMENT':
          this.checkElementInlineStyle(clonedHierarchySectionBlock);
          break;
        case 'COLUMN':
          this.checkColumnInlineStyle(clonedHierarchySectionBlock);
          break;
        case 'ROW':
          this.checkRowInlineStyle(clonedHierarchySectionBlock);
          break;
      }
      code = clonedHierarchySectionBlock?.outerHTML;
    }
    return code;
  };

  clearStyleAttribute = (htmlCode: string): string => {
    const styleAttribute = new RegExp('style=("|\')[^("|\')]*("|\')', 'g');
    if (htmlCode !== '') {
      htmlCode = htmlCode.replace(styleAttribute, '');
    }
    return htmlCode;
  };

  getSelectedBlockHtml = (type: string): string => {
    let html = '';
    htmlCode = '';
    switch (type) {
      case 'ELEMENT':
        if (copyOfSelectedElement) currentHierarchySectionBlock = copyOfSelectedElement;
        if (currentHierarchySectionBlock)
          html = this.removeInlineStyle(currentHierarchySectionBlock, 'ELEMENT');
        break;
      case 'COLUMN':
        if (copyOfSelectedElement?.classList.contains('loree-iframe-content-column')) {
          currentHierarchySectionBlock = copyOfSelectedElement;
        } else {
          currentHierarchySectionBlock = copyOfSelectedElement?.closest(
            '.loree-iframe-content-column',
          ) as HTMLElement;
        }
        if (currentHierarchySectionBlock)
          html = this.removeInlineStyle(currentHierarchySectionBlock, 'COLUMN');
        break;
      case 'ROW':
        if (copyOfSelectedElement?.classList.contains('loree-iframe-content-row')) {
          currentHierarchySectionBlock = copyOfSelectedElement;
        } else {
          currentHierarchySectionBlock = copyOfSelectedElement?.closest(
            '.loree-iframe-content-row',
          ) as HTMLElement;
        }
        if (currentHierarchySectionBlock)
          html = this.removeInlineStyle(currentHierarchySectionBlock, 'ROW');
        break;
      case 'PAGE':
        // eslint-disable-next-line no-case-declarations
        const iframeDocument = this.getDocument();
        if (iframeDocument) {
          const wrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
          if (wrapper) {
            let divForPageInnerHtml = '';
            temporaryPageDiv = document.createElement('div');
            for (let i = 0; i < wrapper?.children.length; i++) {
              clonedHierarchySectionBlock = this.createClone(wrapper?.children[i]) as HTMLElement;
              this.checkRowInlineStyle(clonedHierarchySectionBlock);
              divForPageInnerHtml += clonedHierarchySectionBlock?.outerHTML;
            }
            temporaryPageDiv.innerHTML = divForPageInnerHtml;
            html = divForPageInnerHtml.replace(/<!--[\s\S]*?-->/g, '').replace(/(^[ \t]*\n)/gm, '');
          }
        }
        break;
    }
    if (html !== '') htmlCode = this.cleanHtml(html);
    if (htmlCode !== '') htmlCode = this.clearStyleAttribute(htmlCode);
    return htmlCode;
  };

  pageBlockClassNames = () => {
    const wrapperChildArray: any = [];
    const pageBlockClassArray: any = [];
    const wrapper = temporaryPageDiv;
    if (wrapper) {
      wrapper.childNodes.forEach((element) => {
        wrapperChildArray.push(element);
      });
      for (let i = 0; i < wrapperChildArray.length; i++) {
        const classArray = this.rowBlockClassNames(wrapperChildArray[i]);
        for (let j = 0; j < classArray.length; j++) {
          pageBlockClassArray.push(classArray[j]);
        }
      }
    }
    return pageBlockClassArray;
  };

  fetchParentBlockClassNames = (selectedBlock: HTMLElement) => {
    const parentBlockClassArray: any = [];
    for (let i = 0; i < selectedBlock?.classList?.length; i++) {
      if (
        !parentBlockClassArray.includes(selectedBlock.classList[i]) &&
        selectedBlock.classList[i] !== 'element-highlight' &&
        selectedBlock.classList[i] !== 'column-highlight' &&
        selectedBlock.classList[i] !== 'row-highlight'
      ) {
        parentBlockClassArray.push(selectedBlock.classList[i]);
      }
    }
    return parentBlockClassArray;
  };

  rowBlockClassNames = (selectedBlock: HTMLElement) => {
    const rowBlockClassArray: any = [];
    let parentBlockClassList: any = [];
    parentBlockClassList = this.fetchParentBlockClassNames(selectedBlock);
    for (let i = 0; i < parentBlockClassList.length; i++) {
      rowBlockClassArray.push(parentBlockClassList[i]);
    }
    const sampleArray: any = [];
    let arrayList: any = [];
    for (let i = 0; i < selectedBlock?.childNodes.length; i++) {
      sampleArray.push(selectedBlock?.childNodes[i]);
    }
    for (let j = 0; j < sampleArray.length; j++) {
      arrayList = this.columnBlockClassNames(sampleArray[j]);
      for (let k = 0; k < arrayList.length; k++) {
        rowBlockClassArray.push(arrayList[k]);
      }
    }
    return rowBlockClassArray;
  };

  columnBlockClassNames = (selectedBlock: HTMLElement) => {
    const columnBlockClassArray: any = [];
    let parentBlockClassList: any = [];
    parentBlockClassList = this.fetchParentBlockClassNames(selectedBlock);
    for (let i = 0; i < parentBlockClassList.length; i++) {
      columnBlockClassArray.push(parentBlockClassList[i]);
    }
    const sampleArray: any = [];
    let arrayList: any = [];
    for (let i = 0; i < selectedBlock?.childNodes.length; i++) {
      sampleArray.push(selectedBlock?.childNodes[i]);
    }
    for (let j = 0; j < sampleArray.length; j++) {
      arrayList = this.elementBlockClassNames(sampleArray[j]);
      for (let k = 0; k < arrayList.length; k++) {
        columnBlockClassArray.push(arrayList[k]);
      }
    }
    return columnBlockClassArray;
  };

  elementBlockClassNames = (selectedBlock: HTMLElement) => {
    const elementBlockChildArray: any = [];
    const elementBlockClassArray: any = [];
    let parentBlockClassList: any = [];
    parentBlockClassList = this.fetchParentBlockClassNames(selectedBlock);
    for (let i = 0; i < parentBlockClassList.length; i++) {
      elementBlockClassArray.push(parentBlockClassList[i]);
    }
    if (selectedBlock.nodeType !== 3 && selectedBlock.nodeType !== 8) {
      if (selectedBlock.children.length > 0) {
        selectedBlock?.childNodes?.forEach((element) => {
          elementBlockChildArray.push(element);
          element?.childNodes?.forEach((elements) => {
            elementBlockChildArray.push(elements);
            elements?.childNodes?.forEach((childElements) => {
              elementBlockChildArray.push(childElements);
              childElements?.childNodes?.forEach((superChildElements) => {
                elementBlockChildArray.push(superChildElements);
              });
            });
          });
        });
      } else {
        elementBlockChildArray.push(selectedBlock);
      }
    }
    for (let i = 0; i < elementBlockChildArray.length; i++) {
      const classListLength = elementBlockChildArray[i].classList?.length;
      if (!isNaN(classListLength)) {
        for (let j = 0; j < classListLength; j++) {
          if (
            !elementBlockClassArray.includes(elementBlockChildArray[i].classList[j]) &&
            elementBlockChildArray[i].classList[j] !== 'element-highlight' &&
            elementBlockChildArray[i].classList[j] !== 'column-highlight' &&
            elementBlockChildArray[i].classList[j] !== 'row-highlight'
          )
            elementBlockClassArray.push(elementBlockChildArray[i].classList[j]);
        }
      }
    }
    return elementBlockClassArray;
  };

  getSelectedBlockCss = (type: string): string => {
    cssCode = '';
    let classNameArray: any = [];
    switch (type) {
      case 'ELEMENT':
        if (clonedHierarchySectionBlock !== null)
          classNameArray = this.elementBlockClassNames(clonedHierarchySectionBlock);
        break;
      case 'COLUMN':
        if (clonedHierarchySectionBlock !== null)
          classNameArray = this.columnBlockClassNames(clonedHierarchySectionBlock);
        break;
      case 'ROW':
        if (clonedHierarchySectionBlock !== null)
          classNameArray = this.rowBlockClassNames(clonedHierarchySectionBlock);
        break;
      case 'PAGE':
        classNameArray = this.pageBlockClassNames();
        break;
    }
    if (classNameArray) {
      for (let i = 0; i < classNameArray.length; i++) {
        cssCode += this.getStyle(classNameArray[i]);
      }
    }
    if (cssCode) {
      cssCode = cssCode.replace(/rgb\([^)]+\)/g, (rgb) => {
        return this.rgb2hex(rgb);
      });
    }
    return cssCode;
  };

  setTypeAsClassName = (selectedBlockType: string): void => {
    const footerSet = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER_BUTTON_WRAPPER,
    ) as HTMLElement;
    footerSet.className = selectedBlockType;
  };

  showViewCodeModal = (): void => {
    this.hideLanguageIsoLabel();
    let hierarchyPosition = 0;
    const viewCodeModalElementButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ELEMENT,
    ) as HTMLElement;
    const viewCodeModalColumnButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_COLUMN,
    ) as HTMLElement;
    const viewCodeModalRowButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ROW,
    ) as HTMLElement;
    const viewCodeModalPageButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_PAGE,
    ) as HTMLElement;
    const selectedElementBlock = this.getSelectedElement();
    copyOfSelectedElement = selectedElementBlock;
    if (selectedElementBlock === null) {
      selectedBlockType = 'PAGE';
      viewCodeModalPageButton?.classList.remove('d-none');
      hierarchyPosition = 7;
    } else {
      if (selectedElementBlock?.classList.contains('loreeContentWrapper')) {
        selectedBlockType = 'PAGE';
        viewCodeModalPageButton?.classList.remove('d-none');
        hierarchyPosition = 7;
      } else if (selectedElementBlock?.classList.contains('loree-iframe-content-row')) {
        selectedBlockType = 'ROW';
        viewCodeModalRowButton?.classList.remove('d-none');
        viewCodeModalPageButton?.classList.remove('d-none');
        hierarchyPosition = 5;
      } else if (selectedElementBlock?.classList.contains('loree-iframe-content-column')) {
        selectedBlockType = 'COLUMN';
        viewCodeModalColumnButton?.classList.remove('d-none');
        viewCodeModalRowButton?.classList.remove('d-none');
        viewCodeModalPageButton?.classList.remove('d-none');
        hierarchyPosition = 3;
      } else if (selectedElementBlock) {
        selectedBlockType = 'ELEMENT';
        viewCodeModalElementButton?.classList.remove('d-none');
        viewCodeModalColumnButton?.classList.remove('d-none');
        viewCodeModalRowButton?.classList.remove('d-none');
        viewCodeModalPageButton?.classList.remove('d-none');
        hierarchyPosition = 1;
      }
    }
    this.setTypeAsClassName(selectedBlockType);
    this.appendExportToZipButton(selectedBlockType);
    this.setViewCodeModalDisplay();
    this.updateInactiveSection(hierarchyPosition);
    const htmlCode = this.getSelectedBlockHtml(selectedBlockType);
    const cssCode = this.getSelectedBlockCss(selectedBlockType);
    this.initializeHtmlCodeEditor(htmlCode);
    this.initializeCssCodeEditor(cssCode);
  };

  updateInactiveSection = (position: number): void => {
    const hierarchySectionChilds = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_HIERARCHY_SECTION,
    )?.childNodes;
    if (hierarchySectionChilds) {
      for (let i = 0; i < hierarchySectionChilds.length; i++) {
        if (hierarchySectionChilds[i].nodeType !== 3) {
          const hierarchySection = hierarchySectionChilds[i] as HTMLElement;
          if (position === i) {
            if (hierarchySection?.classList.contains('inactive'))
              hierarchySection.classList.remove('inactive');
            continue;
          }
          if (!hierarchySection?.classList.contains('inactive'))
            hierarchySection.classList.add('inactive');
        }
      }
    }
  };

  // show selected element code in view code modal popup
  showElementBlockCode = (): void => {
    this.resetViewCodeModalFooter();
    this.updateInactiveSection(1);
    selectedBlockType = 'ELEMENT';
    this.setTypeAsClassName(selectedBlockType);
    this.appendExportToZipButton(selectedBlockType);
    const htmlCode = this.getSelectedBlockHtml(selectedBlockType);
    const cssCode = this.getSelectedBlockCss(selectedBlockType);
    this.initializeHtmlCodeEditor(htmlCode);
    this.initializeCssCodeEditor(cssCode);
  };

  // show selected column code in view code modal popup
  showColumnBlockCode = (): void => {
    this.resetViewCodeModalFooter();
    this.updateInactiveSection(3);
    selectedBlockType = 'COLUMN';
    this.setTypeAsClassName(selectedBlockType);
    this.appendExportToZipButton(selectedBlockType);
    const htmlCode = this.getSelectedBlockHtml(selectedBlockType);
    const cssCode = this.getSelectedBlockCss(selectedBlockType);
    this.initializeHtmlCodeEditor(htmlCode);
    this.initializeCssCodeEditor(cssCode);
  };

  // show selected row code in view code modal popup
  showRowBlockCode = (): void => {
    this.resetViewCodeModalFooter();
    this.updateInactiveSection(5);
    selectedBlockType = 'ROW';
    this.setTypeAsClassName(selectedBlockType);
    this.appendExportToZipButton(selectedBlockType);
    const htmlCode = this.getSelectedBlockHtml(selectedBlockType);
    const cssCode = this.getSelectedBlockCss(selectedBlockType);
    this.initializeHtmlCodeEditor(htmlCode);
    this.initializeCssCodeEditor(cssCode);
  };

  // show selected page code in view code modal popup
  showPageBlockCode = (): void => {
    this.resetViewCodeModalFooter();
    this.updateInactiveSection(7);
    selectedBlockType = 'PAGE';
    this.setTypeAsClassName(selectedBlockType);
    this.appendExportToZipButton(selectedBlockType);
    const htmlCode = this.getSelectedBlockHtml(selectedBlockType);
    const cssCode = this.getSelectedBlockCss(selectedBlockType);
    this.initializeHtmlCodeEditor(htmlCode);
    this.initializeCssCodeEditor(cssCode);
  };

  editCodeHandler = (): void => {
    this.closeWarningAlert();
    htmlCodeMirrorEditorInstance.setOption('readOnly', false);
    cssCodeMirrorEditorInstance.setOption('readOnly', false);
    const footer = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER_BUTTON_WRAPPER,
    ) as HTMLElement;
    footer.innerHTML = appendUpdateSetButtons();
    this.updateSetButtonsHandler();
  };

  stringToTabelElement = (str: string) => {
    const tableTrElement = document.createElement('TR') as HTMLTableRowElement;
    tableTrElement.innerHTML = str;
    return tableTrElement.cells.item(0);
  };

  stringToDivElement = (str: string) => {
    const html = this.convertContent(str);
    const divElement = document.createElement('div');
    divElement.innerHTML = html;
    const docFrag = document.createDocumentFragment();
    while (divElement.firstChild) {
      const child = divElement.removeChild(divElement.firstChild);
      docFrag.appendChild(child);
    }
    return docFrag;
  };

  stringToHTML = (code: string) => {
    let htmlElement: any;
    if (copyOfSelectedElement?.tagName === 'TD' || copyOfSelectedElement?.tagName === 'TH') {
      htmlElement = this.stringToTabelElement(code);
    } else {
      htmlElement = this.stringToDivElement(code);
    }
    return htmlElement;
  };

  updateHtmlCodeHandler = (): void => {
    htmlCodeMirrorEditorInstance.setOption('readOnly', true);
    cssCodeMirrorEditorInstance.setOption('readOnly', true);
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const result = juice(
      `<style>${cssCodeMirrorEditorInstance.getValue()}</style>${htmlCodeMirrorEditorInstance.getValue()}`,
    );
    const style = iframeDocument.getElementById(
      CONSTANTS.LOREE_IFRAME_STYLE_WRAPPER,
    ) as HTMLStyleElement;
    style.innerHTML = '';
    if (selectedBlockType === 'PAGE') {
      const wrapper = iframeDocument.getElementById(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
      if (wrapper) wrapper.innerHTML = result;
    } else {
      const htmlElement = this.stringToHTML(result);
      if (htmlElement) currentHierarchySectionBlock?.replaceWith(htmlElement);
    }
    this.handleSelectedContentChanges();
    this.hideViewCodeModal();
    savePageAlert(translate('alert.codeupdatedsuccessfully'));
    savePageAlertContainerDisapper();
  };

  cancelEditCodeHandler = (): void => {
    this.initializeHtmlCodeEditor(htmlCode);
    this.initializeCssCodeEditor(cssCode);
    htmlCodeMirrorEditorInstance.setOption('readOnly', true);
    cssCodeMirrorEditorInstance.setOption('readOnly', true);
    const footerButtonWrapper = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER_BUTTON_WRAPPER,
    ) as HTMLElement;
    if (footerButtonWrapper) {
      footerButtonWrapper.innerHTML = appendFooterSetButtons();
      this.footerSetButtonsHandler();
      this.appendExportToZipButton(footerButtonWrapper.className);
    }
  };

  resetViewCodeModalFooter = (): void => {
    const footerButtonWrapper = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_FOOTER_BUTTON_WRAPPER,
    ) as HTMLElement;
    footerButtonWrapper.innerHTML = appendFooterSetButtons();
    this.footerSetButtonsHandler();
  };

  // hide view code modal popup
  hideViewCodeModal = (): void => {
    this.resetViewCodeModalFooter();
    this.resetViewCodeModalDisplay();
    const viewCodeModalElementButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ELEMENT,
    ) as HTMLElement;
    const viewCodeModalColumnButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_COLUMN,
    ) as HTMLElement;
    const viewCodeModalRowButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_ROW,
    ) as HTMLElement;
    const viewCodeModalPageButton = document.getElementById(
      CONSTANTS.LOREE_VIEW_CODE_MODAL_CONTENT_BODY_PAGE,
    ) as HTMLElement;
    viewCodeModalElementButton?.classList.add('d-none');
    viewCodeModalColumnButton?.classList.add('d-none');
    viewCodeModalRowButton?.classList.add('d-none');
    viewCodeModalPageButton?.classList.add('d-none');
  };

  copyCodeToClipBoard = (str: string): void => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    savePageAlert(translate('alert.copiedtoclipboard'));
    savePageAlertContainerDisapper();
  };
}
