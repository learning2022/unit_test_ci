import { API, graphqlOperation, Auth } from 'aws-amplify';
import { apm } from '@elastic/apm-rum';
import CONSTANTS from '../constant';
import { interactiveElementAlert } from '../alert';
import { interactiveVerticalDotIcons } from '../iconHolder';
import { listHFivePUserss, getH5PContent, courseDetails } from '../../graphql/queries';
import { h5pUserInitiate, createHFivePUsers, deleteH5PContent } from '../../graphql/mutations';
import Base from '../base';
import { Icon, interactiveIcons, videoModalIcons } from '../modules/sidebar/iconHolder';
import { handleAutoSaveOnCustomBlockAppend } from '../../utils/saveContent';
import h5p from '../../assets/images/h5p.png';
import { buildInteractiveBlock } from './expandableBlock';
import { attatchHorizontalLine } from '../../utils/dom';
import { showTagFilter } from './h5pUtil';
import { getEditorElementById } from '../utils';
import { getElementById } from '../common/dom';
import { translate } from '../../i18n/translate';

export type H5pDataObject = {
  account_id: string;
  created_user: string;
  id: string;
  tags: string;
  title: string;
  updated_at: string;
};
export type H5pData = H5pDataObject[];
export type H5pUserDetails = {
  email: string | null;
};
export type H5pLoginDetails = {
  userlogin: string;
  password: string;
  redirectUrl: string;
  formActionUrl: string;
  platformId: string | null;
  platformDomain: string;
};

export type Select2Data = {
  disabled: boolean;
  element: {};
  id: string;
  selected: boolean;
  text: string;
  title: string;
  _resultId: string;
};

const base = new Base();
let userData: string;
let elementData: H5pData = [];
let h5pContent: H5pData = [];
let searchData: H5pData = [];
let availableH5pTags: string[] = [];
let userDetails: H5pUserDetails;
let h5pLoginDetails: H5pLoginDetails;
let domainUrl: string;
let currentUserId: string;
let sharedInteractives: H5pData = [];
let depatmentInteractives: H5pData = [];
let searchSharedData: H5pData = [];
let searchDepartmentData: H5pData = [];
let accountId: string;
let accountName: string;
let filteredTags: string[] = [];

const H5PURL = () => {
  let h5pSiteUrl = 'https://loree-h5p-dev.crystaldelta.net';
  if (
    sessionStorage.getItem('envName') === 'staging' ||
    sessionStorage.getItem('envName') === 'production' ||
    sessionStorage.getItem('envName') === 'productionus'
  ) {
    h5pSiteUrl = 'https://loree-h5p.crystaldelta.net';
  }
  return h5pSiteUrl;
};

// api calls to fetch h5p

// initiate API
export const initiateApi = async () => {
  const user = await Auth.currentAuthenticatedUser();
  const initializeUser = {
    email: sessionStorage.getItem('lmsEmail'),
    firstName: user.attributes.name,
    role: 'author',
    platformId: sessionStorage.getItem('ltiPlatformId'),
    platformDomain: domainUrl,
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let checkUser: any = await API.graphql(
    graphqlOperation(listHFivePUserss, {
      filter: {
        email: {
          eq: sessionStorage.getItem('lmsEmail'),
        },
        ltiPlatformID: {
          eq: sessionStorage.getItem('ltiPlatformId'),
        },
      },
    }),
  );

  checkUser = checkUser.data.listHFivePUserss.items;

  if (checkUser.length === 0) {
    try {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      let h5pInitiateUser: any = await API.graphql(
        graphqlOperation(h5pUserInitiate, initializeUser),
      );
      h5pInitiateUser = JSON.parse(h5pInitiateUser.data.h5pUserInitiate);
      if (h5pInitiateUser.statusCode === 200) {
        const inputDetails = {
          email: userDetails.email,
          loreeUser: user.attributes.email,
          ltiPlatformID: sessionStorage.getItem('ltiPlatformId'),
        };
        await API.graphql(graphqlOperation(createHFivePUsers, { input: inputDetails }));
      }
    } catch (error) {
      apm.captureError(error as Error);
      console.log('error in creating user', error);
      return error;
    }
  }
  return null;
};

export const fetchAccountInfo = async () => {
  domainUrl = sessionStorage.getItem('lmsUrl')?.split('https://')[1] as string;
  const lmsName = sessionStorage.getItem('domainName');
  if (lmsName === 'canvas') {
    const courseAccountDetails = await getCourseAccountDetails();
    accountName = courseAccountDetails.account.name;
    accountId = courseAccountDetails.account.id;
  } else accountId = '0';
};

export const fetchH5PLoginDetails = (user: string) => {
  const h5pSiteUrl = H5PURL();
  h5pLoginDetails = {
    userlogin: `${user}_${sessionStorage.getItem('ltiPlatformId')}`,
    password: user,
    redirectUrl: `${h5pSiteUrl}/wp-admin/admin.php?page=h5p_new`,
    formActionUrl: `${h5pSiteUrl}/wp-login.php`,
    platformId: sessionStorage.getItem('ltiPlatformId'),
    platformDomain: domainUrl,
  };
  return null;
};

// Get content API
export const fetchContentApi = async () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let h5pInteractives: any = await API.graphql(
    graphqlOperation(getH5PContent, {
      userName: `${userData}_${sessionStorage.getItem('ltiPlatformId')}`,
      platformId: sessionStorage.getItem('ltiPlatformId'),
    }),
  );
  h5pInteractives = JSON.parse(h5pInteractives.data.getH5PContent);
  if (h5pInteractives.statusCode === 200) {
    currentUserId = h5pInteractives.body.current_user;
    h5pContent = h5pInteractives.body.h5pContents;
    return h5pContent;
  } else {
    console.log('error in fetching h5p interactives');
    return null;
  }
};

export const appendH5PElement = (elementSection: HTMLElement) => {
  base.hideSubSidebar();
  base.removeSelectedElementSection();
  appendH5PElementSection(elementSection);
  return null;
};

// H5P integration in side bar
export const appendH5PElementSection = (elementSection: HTMLElement) => {
  if (userDetails === undefined) {
    userDetails = {
      email: sessionStorage.getItem('lmsEmail'),
    };
    if (userDetails.email) userData = userDetails.email.substr(0, userDetails.email.indexOf('@'));
    fetchH5PLoginDetails(userData);
    appendElements(elementSection, true);
    void fetchAccountInfo().then(() => {
      appendElements(elementSection, false);
      void initiateApi().then(() => {
        void fetchContentApi().then(() => {
          retriveH5PBlockElement(h5pContent);
        });
      });
    });
  } else {
    appendElements(elementSection, false);
    retriveH5PBlockElement(h5pContent);
  }
};

export const appendElements = (elementSection: Node, isInitialised: boolean) => {
  const initialH5pLoader = document.getElementById('h5p-initial-loader');
  if (initialH5pLoader) initialH5pLoader.remove();
  const sidebarElementWrapper = document.getElementsByClassName('sidebarElementsWrapper')[0];
  if (sidebarElementWrapper) sidebarElementWrapper.classList.remove('show');
  const interactiveRowInputWrapper = document.createElement('div');
  interactiveRowInputWrapper.id = 'loree-sidebar-H5P-element-input-wrapper';
  let innerHTML = `<div class = "H5P-block-title-name">${translate(
    'element.h5pinteractives',
  )}</div>`;
  innerHTML += searchH5PWrapper();
  interactiveRowInputWrapper.innerHTML = innerHTML;
  if (isInitialised)
    interactiveRowInputWrapper.innerHTML =
      innerHTML + `<div id="h5p-initial-loader">${H5PmodalLoader()}</div>`;
  else {
    const designEditOptions = document.createElement('div');
    designEditOptions.className = 'accordion';
    designEditOptions.id = CONSTANTS.LOREE_H5P_INTERACTIVES_SECTION;
    const elementDetails = document.createElement('div');
    elementDetails.id = CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_ELEMENT_DETAILS;
    designEditOptions.appendChild(myInteractiveBlock());
    attatchHorizontalLine(designEditOptions);
    const lmsName = sessionStorage.getItem('domainName');
    const lmsDomainUrl = sessionStorage.getItem('lmsUrl');
    if (lmsName === 'canvas') {
      designEditOptions.appendChild(departmentInteractiveBlock());
      attatchHorizontalLine(designEditOptions);
    }
    if (!lmsDomainUrl?.includes('vetsharedresources.instructure.com')) {
      designEditOptions.appendChild(sharedInteractiveBlock());
      attatchHorizontalLine(designEditOptions);
    }

    interactiveRowInputWrapper.appendChild(designEditOptions);
    document.getElementById('loree-sidebar-H5P-element-input-wrapper')?.remove();
  }
  (interactiveRowInputWrapper.childNodes[1] as HTMLElement).insertAdjacentElement(
    'beforebegin',
    newH5PWrapper(),
  );
  elementSection.childNodes[1]?.appendChild(interactiveRowInputWrapper);
  const searchOnInput = document.getElementById('loree-sidebar-H5P-element-search-input-value');
  if (searchOnInput) {
    searchOnInput.oninput = () => handleH5PSearchBlocks();
  }
  return null;
};

export const myInteractiveBlock = (): HTMLElement =>
  buildInteractiveBlock(
    `${translate('element.myh5p')}`,
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_WRAPPER,
    CONSTANTS.LOREE_DESIGN_SECTION_SPACE_BLOCK_HEADING,
    CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE,
    'myInteractives',
    CONSTANTS.LOREE_H5P_INTERACTIVES_SECTION,
    true,
  );

export const sharedInteractiveBlock = (): HTMLElement =>
  buildInteractiveBlock(
    `${translate('element.myorganisationsh5p')}`,
    // TODO make IDs unique
    CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_WRAPPER,
    CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_HEADING,
    CONSTANTS.LOREE_H5P_SHARED_INTERACTIVES_BLOCK_COLLAPSE,
    'sharedInteractives',
    CONSTANTS.LOREE_H5P_INTERACTIVES_SECTION,
  );

export const departmentInteractiveBlock = (): HTMLElement =>
  buildInteractiveBlock(
    `${accountName}'s H5P`,
    // TODO make IDs unique
    CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_WRAPPER,
    CONSTANTS.LOREE_DESIGN_SECTION_BACKGROUND_BLOCK_HEADING,
    CONSTANTS.LOREE_H5P_DEPARTMENT_BLOCK_COLLAPSE,
    'departmentInteractives',
    CONSTANTS.LOREE_H5P_INTERACTIVES_SECTION,
  );

// List the H5P interactives on the sideBar
export const H5PBlockThumbnail = () => {
  const myInteractiveContent = document.getElementById(
    CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE,
  );
  const sharedContent = document.getElementById(
    CONSTANTS.LOREE_H5P_SHARED_INTERACTIVES_BLOCK_COLLAPSE,
  );
  const departmentContent = document.getElementById(CONSTANTS.LOREE_H5P_DEPARTMENT_BLOCK_COLLAPSE);

  const accordionContentDiv = document.createElement('div');
  accordionContentDiv.id = 'myInteractives';

  const sharedContentDiv = document.createElement('div');
  sharedContentDiv.id = 'sharedInteractives';

  const departmentContentDiv = document.createElement('div');
  departmentContentDiv.id = 'departmentInteractives';

  if (elementData.length === 0) {
    accordionContentDiv.innerHTML = `<p class='loree-H5P-block-no-results font-italic' style='width:160px'>${translate(
      'modal.nointeractivesfound',
    )}</p>`;
  } else {
    elementData.sort(function (a, b) {
      return parseInt(b.id) - parseInt(a.id);
    });
    Array.from(elementData).forEach((data) => {
      const rowElement = document.createElement('div');
      rowElement.className = 'H5P-block-thumbnail position-relative mb-3';
      rowElement.id = data.id;
      const rowThumbnail = document.createElement('div');
      rowThumbnail.className = 'H5P-blocks-thumbnail-element';
      rowThumbnail.prepend(appendThumbnailMenu(translate('modal.interactive'), false, elementData));
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = h5p;
      thumbnailImg.className = 'loree-sidebar-H5P-block-thumbnail';
      const thumbnailOverlay = document.createElement('div');
      thumbnailOverlay.className = 'H5P-block-thumbnail-overlay';
      rowThumbnail.append(thumbnailImg);
      rowThumbnail.append(thumbnailOverlay);
      const title = document.createElement('p');
      title.className = 'H5P-block-title';
      title.innerHTML = `<span title="${data.title}">${data.title}</span>`;
      rowElement.onclick = (e): void => handleAddH5PElement(e, elementData);
      rowElement.append(rowThumbnail);
      rowElement.appendChild(title);
      accordionContentDiv?.append(rowElement);
    });
  }
  if (sharedInteractives.length === 0) {
    sharedContentDiv.innerHTML = `<p class='loree-H5P-block-no-results font-italic' style='width:160px'>${translate(
      'modal.nointeractivesfound',
    )}</p>`;
  } else {
    sharedInteractives.sort(function (a, b) {
      return parseInt(b.id) - parseInt(a.id);
    });
    Array.from(sharedInteractives).forEach((data: H5pDataObject) => {
      const rowElement = document.createElement('div');
      rowElement.className = 'H5P-block-thumbnail position-relative mb-3';
      rowElement.id = data.id;
      const rowThumbnail = document.createElement('div');
      rowThumbnail.className = 'H5P-blocks-thumbnail-element';
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = h5p;
      thumbnailImg.className = 'loree-sidebar-H5P-block-thumbnail';
      const thumbnailOverlay = document.createElement('div');
      thumbnailOverlay.className = 'H5P-block-thumbnail-overlay';
      rowThumbnail.append(thumbnailImg);
      rowThumbnail.append(thumbnailOverlay);
      const title = document.createElement('p');
      title.className = 'H5P-block-title';
      title.innerHTML = `<span title="${data.title}">${data.title}</span>`;
      rowElement.onclick = (e): void => handleAddH5PElement(e, sharedInteractives);
      rowElement.append(rowThumbnail);
      rowElement.appendChild(title);
      sharedContentDiv?.append(rowElement);
    });
  }

  if (depatmentInteractives.length === 0) {
    departmentContentDiv.innerHTML = `<p class='loree-H5P-block-no-results font-italic' style='width:160px'>${translate(
      'modal.nointeractivesfound',
    )}</p>`;
  } else {
    depatmentInteractives.sort(function (a, b) {
      return parseInt(b.id) - parseInt(a.id);
    });
    Array.from(depatmentInteractives).forEach((data: H5pDataObject) => {
      const rowElement = document.createElement('div');
      rowElement.className = 'H5P-block-thumbnail position-relative mb-3';
      rowElement.id = data.id;
      const rowThumbnail = document.createElement('div');
      rowThumbnail.className = 'H5P-blocks-thumbnail-element';
      if (
        base?.getFeatureList()?.editorganisationh5p ||
        base?.getFeatureList()?.deleteorganisationh5p
      )
        rowThumbnail.prepend(
          appendThumbnailMenu(
            translate('modal.departmentinteractive'),
            false,
            depatmentInteractives,
          ),
        );
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = h5p;
      thumbnailImg.className = 'loree-sidebar-H5P-block-thumbnail';
      const thumbnailOverlay = document.createElement('div');
      thumbnailOverlay.className = 'H5P-block-thumbnail-overlay';
      rowThumbnail.append(thumbnailImg);
      rowThumbnail.append(thumbnailOverlay);
      const title = document.createElement('p');
      title.className = 'H5P-block-title';
      title.innerHTML = `<span title="${data.title}">${data.title}</span>`;
      rowElement.onclick = (e): void => handleAddH5PElement(e, depatmentInteractives);
      rowElement.append(rowThumbnail);
      rowElement.appendChild(title);
      departmentContentDiv?.append(rowElement);
    });
  }
  myInteractiveContent?.appendChild(accordionContentDiv);
  departmentContent?.appendChild(departmentContentDiv);
  sharedContent?.appendChild(sharedContentDiv);
  return myInteractiveContent;
};

export const newH5PWrapper = () => {
  const btnWrapper = document.createElement('div');
  btnWrapper.className = 'new-interactive-name';
  const btn = document.createElement('button');
  btn.id = CONSTANTS.LOREE_BLOCK_OPTIONS_H5P_BUTTON;
  btn.className = 'create-new-interactive btn btn-primary editor-H5P-btn';
  btn.onclick = () => {
    createNewH5P();
  };
  btn.id = 'create-new-interactive-btn';
  btn.innerHTML = translate('element.createnew');
  btnWrapper.appendChild(btn);
  return btnWrapper;
};

export const createNewH5P = () => {
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  if ((document.getElementById(CONSTANTS.LOREE_H5P_BLOCK_BACKDROP) as HTMLElement) === null) {
    const H5PBlockBackdrop = document.createElement('div');
    H5PBlockBackdrop.id = CONSTANTS.LOREE_H5P_BLOCK_BACKDROP;
    H5PBlockBackdrop.className = 'loree-H5P-block-backdrop';
    editorParentWrapper?.appendChild(H5PBlockBackdrop);
    const H5PBlockPopup = document.createElement('div');
    H5PBlockPopup.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_WRAPPER;
    H5PBlockPopup.className = 'modal';
    const H5PModalContent = document.createElement('div');
    H5PModalContent.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_CONTENT;
    H5PModalContent.className = 'H5P-block-modal-dialog modal-dialog-centered';
    H5PModalContent?.appendChild(H5PmodalContainer());
    H5PBlockPopup?.appendChild(H5PModalContent);
    editorParentWrapper.appendChild(H5PBlockPopup);

    if (editorParentWrapper) {
      eventHandlers();
    }
  }
  return editorParentWrapper;
};
export const searchH5PWrapper = () => {
  return `<div id='loree-sidebar-h5p-row-search-input' style='margin-bottom:25px'>
  <input id='loree-sidebar-H5P-element-search-input-value' class='loree-sidebar-H5P-block-search-input'; style ="width: 155px"; name="search" type="text" autoComplete="off" placeholder="Search by Title" autoFocus/>
  <div id="loree-sidebar-H5P-search-filter-icon" class="loree-sidebar-H5P-search-filter-icon-style">
   ${Icon.searchIcon}
  </div>
  </div>
  ${showTagFilter() ? '<div id="select2-tag-wrapper"></div>' : ''}`;
};

const handleSuggestionFormation = (availableH5pTags: string[]) => {
  const suggestionWrapper = document.createElement('select');
  suggestionWrapper.className = 'h5p-tag-dropdown';
  suggestionWrapper.setAttribute('name', 'states[]');
  suggestionWrapper.setAttribute('multiple', 'multiple');

  availableH5pTags
    .map((tag: string) => {
      const optionWrapper = document.createElement('option');
      optionWrapper.value = tag;
      optionWrapper.innerText = tag;
      suggestionWrapper.appendChild(optionWrapper);
      return null;
    })
    .join('');
  return suggestionWrapper;
};

export const handleSuggestionClick = (tag: string) => {
  // @ts-expect-error
  const selectedTags = $('.h5p-tag-dropdown').select2('val');
  filteredTags = [...selectedTags];
  const textArea = document.getElementsByClassName(
    'select2-search__field',
  )[0] as HTMLTextAreaElement;
  textArea.value = '';
  handleH5PSearchBlocks();
};

export const handleH5PSearchBlocks = () => {
  elementData = [];
  sharedInteractives = [];
  depatmentInteractives = [];
  const searchInput = document.getElementById(
    'loree-sidebar-H5P-element-search-input-value',
  ) as HTMLInputElement;
  let filteredList = searchData;
  let filterSharedList = searchSharedData;
  let filteredDepartmentList = searchDepartmentData;
  const searchInputText = searchInput?.value.trim();
  if (searchInputText && filteredTags.length > 0) {
    filteredList = filterByTitleAndTags(filteredList, searchInputText, filteredTags);
    filterSharedList = filterByTitleAndTags(filterSharedList, searchInputText, filteredTags);
    filteredDepartmentList = filterByTitleAndTags(
      filteredDepartmentList,
      searchInputText,
      filteredTags,
    );
  } else if (searchInputText && filteredTags.length === 0) {
    filteredList = filterByTitle(filteredList, searchInputText);
    filterSharedList = filterByTitle(filterSharedList, searchInputText);
    filteredDepartmentList = filterByTitle(filteredDepartmentList, searchInputText);
  } else if (searchInputText === '' && filteredTags.length > 0) {
    filteredList = filterByTags(filteredList, filteredTags);
    filterSharedList = filterByTags(filterSharedList, filteredTags);
    filteredDepartmentList = filterByTags(filteredDepartmentList, filteredTags);
  } else if (searchInputText === '' && filteredTags.length === 0) {
    filteredList = searchData;
    filterSharedList = searchSharedData;
    filteredDepartmentList = searchDepartmentData;
  }
  elementData = filteredList;
  sharedInteractives = filterSharedList;
  depatmentInteractives = filteredDepartmentList;
  elementData.sort();
  elementListAppend();
  return null;
};

const filterByTitleAndTags = (
  listToFilter: H5pData,
  searchInputValue: string,
  searchTagValue: string[],
) => {
  const filteredList = listToFilter.filter((item: H5pDataObject) => {
    const multiSearch = (tagName: string) =>
      tagName.toLowerCase().includes(searchTagValue[0].toLowerCase().substr(0, 20));
    const singleSearch = (tagName: string) =>
      item.tags
        ?.toLowerCase()
        .split(',')
        .some((tag: string) => tag.toLowerCase().includes(tagName.toLowerCase().substr(0, 20)));
    return (
      item.title.toLowerCase().includes(searchInputValue.toLowerCase().substr(0, 20)) &&
      (searchTagValue.length === 1
        ? item.tags?.toLowerCase().split(',').some(multiSearch)
        : searchTagValue.every(singleSearch))
    );
  });
  return filteredList;
};

const filterByTitle = (listToFilter: H5pData, searchInputValue: string) => {
  const filteredList = listToFilter.filter((item: H5pDataObject) => {
    return item.title.toLowerCase().includes(searchInputValue.toLowerCase().substr(0, 20));
  });
  return filteredList;
};

const filterByTags = (listToFilter: H5pData, searchTagValue: string[]) => {
  const filteredList = listToFilter.filter((item: H5pDataObject) => {
    const multiSearch = (tagName: string) =>
      tagName.toLowerCase().includes(searchTagValue[0].toLowerCase().substr(0, 20));
    const singleSearch = (tagName: string) =>
      item.tags
        ?.toLowerCase()
        .split(',')
        .some((tag: string) => tag.toLowerCase().includes(tagName.toLowerCase().substr(0, 20)));
    return searchTagValue.length === 1
      ? item.tags?.toLowerCase().split(',').some(multiSearch)
      : searchTagValue.every(singleSearch);
  });
  return filteredList;
};

// Crearte New H5P popup modal
export const H5PmodalContainer = () => {
  const interactiveModalContainer = document.createElement('div');
  interactiveModalContainer.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_CONTAINER;
  interactiveModalContainer.className = 'modal-content';
  let innerHTML = appendH5PModalContentHeader();
  innerHTML += appendH5PModalContentBody();
  interactiveModalContainer.innerHTML = innerHTML;
  return interactiveModalContainer;
};

export const appendH5PModalContentHeader = () => {
  const modalheader = document.createElement('div');
  modalheader.className = 'modal-header p-0';
  modalheader.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_HEADER;
  const headerTitle = document.createElement('h5');
  headerTitle.className = 'modal-title text-primary mb-2';
  headerTitle.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_TITLE;
  headerTitle.innerText = translate('modal.createnewh5pinteractive');
  const headerCloseButton = document.createElement('button');
  headerCloseButton.id = CONSTANTS.LOREE_H5P_BLOCK_CLOSE_ICON;
  headerCloseButton.className = 'close';
  headerCloseButton.setAttribute('data-dismiss', 'modal');
  headerCloseButton.setAttribute('aria-label', 'Close');
  headerCloseButton.innerHTML = interactiveIcons.interactiveEditCloseIcon;
  headerCloseButton.onclick = (e) => {
    removeCreatePopup(e);
  };
  modalheader.appendChild(headerTitle);
  modalheader.appendChild(headerCloseButton);
  return modalheader.outerHTML;
};
const appendH5PModalContentBody = () => {
  const iframeDiv = document.createElement('div');
  iframeDiv.className = 'modal-body p-0';
  const iframe = document.createElement('iframe');
  iframe.id = CONSTANTS.LOREE_IFRAME_H5P_CREATE_MODAL;
  iframe.className = 'interactiveLineIframe';
  const html = `<body onload="setTimeout(function() { document.frm1.submit() }, 100)">
  <form  name="frm1" action='${h5pLoginDetails.formActionUrl}' method="post">
    <input type='hidden' name='log' value='${h5pLoginDetails.userlogin}'>
    <input type='hidden' name='pwd' value='${h5pLoginDetails.password}'>
    <input type='hidden' name='platformId' value='${h5pLoginDetails.platformId}'>
    <input type='hidden' name='platform_domain' value='${h5pLoginDetails.platformDomain}'>
    <input type='hidden' name='account_id' value='${accountId}'>
    <input type='hidden' name='wp-submit' value='Log In'>
    <input type='hidden' name='rememberme' value='forever'>
    <input type='hidden' name='redirect_to' value='${h5pLoginDetails.redirectUrl}'>
  </form>
</body>`;
  iframe.src = 'data:text/html;charset=utf-8,' + encodeURI(html);
  iframeDiv.appendChild(iframe);
  return iframeDiv.outerHTML;
};

// H5P modalPopup close button function
export const eventHandlers = () => {
  const closeButton = document.getElementById(CONSTANTS.LOREE_H5P_BLOCK_CLOSE_ICON);
  if (closeButton)
    closeButton.onclick = (e) => {
      removeCreatePopup(e);
      void H5PModalCloseAction();
    };
  const editcloseButton = document.getElementById(CONSTANTS.LOREE_H5P_EDIT_BLOCK_CLOSE_ICON);
  if (editcloseButton)
    editcloseButton.onclick = (e) => {
      removeEditPopup(e);
      void H5PModalCloseAction();
    };
  return null;
};

export const H5PModalCloseAction = async () => {
  const thumbnailWrapper = getEditorElementById('myInteractives') as HTMLElement;
  const sharedThumbnailWrapper = document.getElementById('sharedInteractives') as HTMLElement;
  thumbnailWrapper?.remove();
  sharedThumbnailWrapper?.remove();

  const myInteractiveContent = getEditorElementById(
    CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE,
  );
  const sharedContent = document.getElementById(
    CONSTANTS.LOREE_H5P_SHARED_INTERACTIVES_BLOCK_COLLAPSE,
  );
  const accordionContentDiv = document.createElement('div');
  accordionContentDiv.id = 'myInteractives';
  accordionContentDiv.innerHTML = H5PmodalLoader();

  const sharedContentDiv = document.createElement('div');
  sharedContentDiv.id = 'sharedInteractives';
  sharedContentDiv.innerHTML = H5PmodalLoader();

  myInteractiveContent?.appendChild(accordionContentDiv);
  sharedContent?.appendChild(sharedContentDiv);

  const searchInput = getEditorElementById(
    'loree-sidebar-H5P-element-search-input-value',
  ) as HTMLInputElement;
  filteredTags = [];
  if (searchInput) {
    searchInput.value = '';
  }
  await h5pContentSeparation();
  const select2Wrapper = getElementById('select2-tag-wrapper') as HTMLDivElement;
  if (select2Wrapper) {
    select2Wrapper.innerHTML = '';
  }
  // @ts-expect-error
  $('.h5p-tag-dropdown').val(null).trigger('change');
  elementListAppend();
};

const h5pContentSeparation = async () => {
  elementData = [];
  searchData = [];
  sharedInteractives = [];
  searchSharedData = [];
  depatmentInteractives = [];
  searchDepartmentData = [];
  h5pContent = [];

  let contentApi: _Any = await API.graphql(
    graphqlOperation(getH5PContent, {
      userName: `${userData}_${sessionStorage.getItem('ltiPlatformId')}`,
      platformId: sessionStorage.getItem('ltiPlatformId'),
    }),
  );
  contentApi = JSON.parse(contentApi.data.getH5PContent);
  const finalContent = contentApi.body.h5pContents;
  if (finalContent.length !== 0) {
    for (const listBlock of finalContent) {
      h5pContent.push(listBlock);
      if (listBlock.created_user === currentUserId) {
        elementData.push(listBlock);
        searchData.push(listBlock);
      } else {
        sharedInteractives.push(listBlock);
        searchSharedData.push(listBlock);
      }
      if (
        listBlock.account_id === JSON.stringify(accountId) &&
        listBlock.created_user !== currentUserId
      ) {
        depatmentInteractives.push(listBlock);
        searchDepartmentData.push(listBlock);
      }
    }
  }
};

export const retriveH5PBlockElement = (h5pContent: H5pData) => {
  elementData = [];
  searchData = [];
  sharedInteractives = [];
  searchSharedData = [];
  depatmentInteractives = [];
  searchDepartmentData = [];
  if (h5pContent !== undefined) {
    for (const listBlock of h5pContent) {
      if (currentUserId === listBlock.created_user) {
        elementData.push(listBlock);
        searchData.push(listBlock);
      } else {
        sharedInteractives.push(listBlock);
        searchSharedData.push(listBlock);
      }
      if (
        listBlock.account_id === JSON.stringify(accountId) &&
        listBlock.created_user !== currentUserId
      ) {
        depatmentInteractives.push(listBlock);
        searchDepartmentData.push(listBlock);
      }
    }
  }
  elementListAppend();
  return null;
};

export const getAvailableTagsFromH5ps = (h5pContent: H5pData) => {
  const tagsArray: string[] = [];
  h5pContent.forEach((h5p: H5pDataObject) => {
    if (!h5p.tags) return;
    const tags = h5p.tags.split(',');
    tags.forEach((tag: string) => {
      if (!tagsArray.includes(tag)) tagsArray.push(tag);
    });
  });
  return tagsArray;
};

export const elementListAppend = (): void => {
  const thumbnailWrapper = document.getElementById('myInteractives') as HTMLElement;
  const sharedThumbnailWrapper = document.getElementById('sharedInteractives') as HTMLElement;
  const departmentThumbnailWrapper = document.getElementById(
    'departmentInteractives',
  ) as HTMLElement;
  const interactiveBlockWrapper = document.getElementById(
    'loree-sidebar-H5P-element-input-wrapper',
  ) as HTMLElement;
  thumbnailWrapper?.remove();
  sharedThumbnailWrapper?.remove();
  departmentThumbnailWrapper?.remove();
  if (interactiveBlockWrapper) H5PBlockThumbnail();
  availableH5pTags = getAvailableTagsFromH5ps(h5pContent);

  const select2Wrapper = document.getElementById('select2-tag-wrapper') as HTMLDivElement;
  if (select2Wrapper?.innerHTML !== '') return;
  const result = handleSuggestionFormation(availableH5pTags);
  select2Wrapper.append(result);
  // @ts-expect-error
  // prettier-ignore
  $('.h5p-tag-dropdown').on('select2:unselect',
    function (e: React.ChangeEvent<HTMLLIElement>) {
      const selectedTagItemsCount =
        e.target?.nextElementSibling?.firstElementChild?.firstElementChild?.firstElementChild
          ?.childNodes?.length;
      if (selectedTagItemsCount === 0) {
        filteredTags = [];
        handleH5PSearchBlocks();
      }
    },
  );
  // @ts-expect-error
  $('.h5p-tag-dropdown').select2({
    placeholder: '  Search by Tags',
    dropdownParent: $('#select2-tag-wrapper'),
    templateSelection: (data: Select2Data) => {
      handleSuggestionClick(data.text);
      return data.text;
    },
  });
};

// Append edit and delete option in the interactives
export const appendThumbnailMenu = (blockType: string, isGlobal: boolean, elementData: H5pData) => {
  const menuWrapper = document.createElement('div');
  menuWrapper.className = 'dropdown dropdown-layout';
  menuWrapper.setAttribute('style', 'position: inherit;color: black;');
  menuWrapper.id = 'loree-sidebar-H5P-block-menu-wrapper';
  const menuBtn = document.createElement('button');
  menuBtn.id = 'loree-sidebar-H5P-block-menu-btn';
  menuBtn.className = 'btn H5P-block-menu-btn dropdown-toggle';
  menuBtn.innerHTML = interactiveVerticalDotIcons;
  menuBtn.type = 'button';
  menuBtn.setAttribute('data-toggle', 'dropdown');
  const showMenu = document.createElement('div');
  showMenu.className = 'dropdown-menu H5P-block-thumnail-dropdown-menu';
  showMenu.id = 'loree-sidebar-H5P-block-thumbnail-dropdown-menu';
  showMenu.setAttribute('aria-labeledby', 'loree-sidebar-H5P-row-category-list');
  const list = [translate('global.edit')];
  if (
    !base?.getFeatureList()?.editorganisationh5p &&
    blockType === translate('modal.departmentinteractive')
  )
    list.shift();
  if (!isGlobal) list.push(translate('global.delete'));
  if (
    !base?.getFeatureList()?.deleteorganisationh5p &&
    blockType === translate('modal.departmentinteractive') &&
    list.includes(translate('global.delete'))
  )
    list.pop();
  if (list.length > 0)
    list.forEach((val) => {
      const li = document.createElement('div');
      li.className = 'H5P-block-thumbnail-menu-option';
      li.onclick = () => handleThumbnailMenu(menuWrapper, val, blockType, elementData);
      li.innerHTML = val;
      showMenu.appendChild(li);
    });
  menuWrapper.appendChild(menuBtn);
  menuWrapper.appendChild(showMenu);
  return menuWrapper;
};

// edit and delete confirmation popup
export const appendH5PMenuModal = (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: H5pData,
) => {
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const interactiveBlockBackdrop = document.createElement('div');
  interactiveBlockBackdrop.id = `loree-custom-block-${modalType}-backdrop`;
  interactiveBlockBackdrop.className = 'loree-H5P-block-backdrop';
  editorParentWrapper?.appendChild(interactiveBlockBackdrop);
  const interactiveBlockPopup = document.createElement('div');
  interactiveBlockPopup.id = `loree-custom-block-${modalType}-modal-wrapper`;
  interactiveBlockPopup.className = 'modal';
  const interactiveModalContent = document.createElement('div');
  interactiveModalContent.id = `loree-custom-block-${modalType}-modal-content`;
  interactiveModalContent.className = 'modal-dialog modal-dialog-centered';
  interactiveModalContent?.appendChild(
    menuH5PModalContainer(modalType, menuWrapper, blockType, elementData),
  );
  interactiveBlockPopup?.appendChild(interactiveModalContent);
  editorParentWrapper.appendChild(interactiveBlockPopup);
  return editorParentWrapper;
};

export const menuH5PModalContainer = (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: H5pData,
) => {
  const interactiveModalContainer = document.createElement('div');
  interactiveModalContainer.id = `loree-custom-block-${modalType}-modal-container`;
  interactiveModalContainer.className = 'modal-content';
  let innerHTML = `<div class="modal-header p-0 mx-0" id=loree-custom-block-${modalType}-modal-header>
  <h5 class="modal-title text-primary" id=loree-custom-block-${modalType}-modal-title><span class="text-capitalize">${modalType}</span> ${blockType}</h5>
  <button type="button" id=loree-custom-block-${modalType}-close-icon class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div><span class ="loree-H5P-block-section-divider" ></span>`;
  if (modalType === translate('modal.delete')) {
    innerHTML += `<div class="modal-body p-0 "><p> ${translate(
      'modal.doyouwanttodeletethis',
    )} <span class="text-lowercase">${blockType}</span> permanently?</div>
    `;
  } else {
    innerHTML += `<div class="modal-body p-0 "><p> ${translate(
      'modal.doyouwanttoeditthis',
    )} <span class="text-lowercase">${translate('modal.interactive')}</span>?</div>
    `;
  }
  const modalButtonWrapper = document.createElement('div');
  modalButtonWrapper.className = 'd-flex flex-row flex-nowrap align-items-center';
  modalButtonWrapper.id = `loree-custom-block-${modalType}-modal-footer`;
  const modalButtonChildWrapper = document.createElement('div');
  modalButtonChildWrapper.className = 'm-auto';
  const cancelBtn = document.createElement('button');
  cancelBtn.id = `loree-custom-block-${modalType}-modal-footer-cancel-button`;
  cancelBtn.innerHTML = translate('global.cancel');
  cancelBtn.className = 'mx-1';
  cancelBtn.onclick = () => deletePopup(modalType);
  const menuBtn = document.createElement('button');
  if (modalType === translate('modal.delete')) {
    menuBtn.className = 'ok-button mx-1';
    menuBtn.onclick = async () =>
      await deleteBlockCustomBlock(modalType, menuWrapper, blockType, elementData);
    menuBtn.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_FOOTER_DELETE_BUTTON;
    menuBtn.innerHTML = translate('global.delete');
  } else {
    menuBtn.className = 'ok-button mx-1';
    menuBtn.onclick = (e) =>
      appendH5PEditMenuModal('edith5pmodal', menuWrapper, blockType, elementData);
    menuBtn.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_EDIT_BUTTON;
    menuBtn.innerHTML = translate('modal.edit');
  }
  modalButtonChildWrapper.appendChild(cancelBtn);
  modalButtonChildWrapper.appendChild(menuBtn);
  modalButtonWrapper.appendChild(modalButtonChildWrapper);
  interactiveModalContainer.innerHTML = innerHTML;
  interactiveModalContainer.appendChild(modalButtonWrapper);
  return interactiveModalContainer;
};

// Edit H5p popup modal
export const appendH5PEditMenuModal = (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: H5pData,
) => {
  const interactiveEditBlockBackdrop = document.getElementById(`loree-custom-block-edit-backdrop`);
  interactiveEditBlockBackdrop?.remove();
  const customBlockModalPopup = document.getElementById(`loree-custom-block-edit-modal-wrapper`);
  customBlockModalPopup?.remove();
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const interactiveBlockBackdrop = document.createElement('div');
  interactiveBlockBackdrop.id = `loree-H5P-block-${modalType}-backdrop`;
  interactiveBlockBackdrop.className = 'loree-H5P-block-backdrop';
  editorParentWrapper?.appendChild(interactiveBlockBackdrop);
  const interactiveBlockPopup = document.createElement('div');
  interactiveBlockPopup.id = `loree-H5P-block-${modalType}-modal-wrapper`;
  interactiveBlockPopup.className = 'modal';
  const interactiveModalContent = document.createElement('div');
  interactiveModalContent.id = `loree-H5P-block-${modalType}-modal-content`;
  interactiveModalContent.className = 'H5P-block-modal-dialog modal-dialog-centered';
  interactiveModalContent?.appendChild(menuH5PEditModalContainer(menuWrapper, elementData));
  interactiveBlockPopup?.appendChild(interactiveModalContent);
  editorParentWrapper.appendChild(interactiveBlockPopup);
  if (editorParentWrapper) {
    eventHandlers();
  }
  return interactiveBlockBackdrop;
};
export const menuH5PEditModalContainer = (menuWrapper: HTMLElement, elementData: H5pData) => {
  const interactiveModalContainer = document.createElement('div');
  interactiveModalContainer.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_CONTAINER;
  interactiveModalContainer.className = 'modal-content';
  let innerHTML = appendH5PEditModalContentHeader();
  // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
  innerHTML += appendH5PModalContentHeaderDivider();
  const id = menuWrapper.parentElement?.parentElement?.getAttribute('id');
  if (id) innerHTML += appendH5PEditModalContentBody(id, elementData);
  interactiveModalContainer.innerHTML = innerHTML;
  return interactiveModalContainer;
};

export const appendH5PEditModalContentHeader = () => {
  const modalheader = document.createElement('div');
  modalheader.className = 'modal-header p-0';
  modalheader.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_HEADER;
  const headerTitle = document.createElement('h5');
  headerTitle.className = 'modal-title text-primary';
  headerTitle.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_TITLE;
  headerTitle.innerText = translate('modal.edith5p');
  const headerCloseButton = document.createElement('button');
  headerCloseButton.id = CONSTANTS.LOREE_H5P_EDIT_BLOCK_CLOSE_ICON;
  headerCloseButton.className = 'close';
  headerCloseButton.setAttribute('data-dismiss', 'modal');
  headerCloseButton.setAttribute('aria-label', 'Close');
  headerCloseButton.innerHTML = interactiveIcons.interactiveEditCloseIcon;
  headerCloseButton.onclick = (e) => {
    removeEditPopup(e);
  };
  modalheader.appendChild(headerTitle);
  modalheader.appendChild(headerCloseButton);
  return modalheader.outerHTML;
};

export const appendH5PModalContentHeaderDivider = () => {
  const divider = document.createElement('div');
  divider.className = 'loree-interactive-block-section-divider';
  return divider.outerHTML;
};
export const appendH5PEditModalContentBody = (id: string | null, elementData: H5pData) => {
  const h5pSiteUrl = H5PURL();
  for (const h5pData of elementData) {
    if (id === h5pData.id.toString()) {
      const editH5pDetails = {
        userlogin: `${userData}_${sessionStorage.getItem('ltiPlatformId')}`,
        password: userData,
        redirectUrl: `${h5pSiteUrl}/wp-admin/admin.php?page=h5p_new&id=${id}`,
        formActionUrl: `${h5pSiteUrl}/wp-login.php`,
        platformId: sessionStorage.getItem('ltiPlatformId'),
        platformDomain: domainUrl,
      };
      const iframeDiv = document.createElement('div');
      iframeDiv.className = 'modal-body p-0';
      const iframe = document.createElement('iframe');
      iframe.id = CONSTANTS.LOREE_IFRAME_H5P_CREATE_MODAL;
      iframe.className = 'interactiveLineIframe';
      const html = `<body onload="setTimeout(function() { document.frm1.submit() }, 100)">
                      <form  name="frm1" action='${editH5pDetails.formActionUrl}' method="post">
                        <input type='hidden' name='log' value='${editH5pDetails.userlogin}'>
                        <input type='hidden' name='pwd' value='${editH5pDetails.password}'>
                        <input type='hidden' name='platformId' value='${editH5pDetails.platformId}'>
                        <input type='hidden' name='platform_domain' value='${editH5pDetails.platformDomain}'>
                        <input type='hidden' name='account_id' value='${accountId}'>
                        <input type='hidden' name='wp-submit' value='Log In'>
                        <input type='hidden' name='rememberme' value='forever'>
                        <input type='hidden' name='redirect_to' value='${editH5pDetails.redirectUrl}'>
                      </form>
                    </body>`;
      iframe.src = 'data:text/html;charset=utf-8,' + encodeURI(html);
      iframeDiv.appendChild(iframe);
      return iframeDiv.outerHTML;
    }
  }
};

const removeEditPopup = (e: MouseEvent) => {
  e.preventDefault();
  const iframe: HTMLIFrameElement | null = document.getElementById(
    CONSTANTS.LOREE_IFRAME,
  ) as HTMLIFrameElement;
  if (iframe) {
    const editModalPopup = document.getElementById('loree-H5P-block-edith5pmodal-modal-wrapper');
    editModalPopup?.remove();
    const editModalPopupBackdrop = document.getElementById('loree-H5P-block-edith5pmodal-backdrop');
    editModalPopupBackdrop?.remove();
  }
};

export const removeCreatePopup = (e: MouseEvent) => {
  e.preventDefault();
  const iframe: HTMLIFrameElement | null = document.getElementById(
    CONSTANTS.LOREE_IFRAME,
  ) as HTMLIFrameElement;
  if (iframe) {
    const createModalPopup = document.getElementById('loree-H5P-block-modal-wrapper');
    createModalPopup?.remove();
    const createModalPopupBackdrop = document.getElementById('loree-H5P-block-backdrop');
    createModalPopupBackdrop?.remove();
  }
};

export const attachDeleteEventHandlers = (modalType: string) => {
  const closeButton = document.getElementById(`loree-custom-block-${modalType}-close-icon`);
  if (closeButton) closeButton.onclick = () => deletePopup(modalType);
};

// Delete interactive from sideBar
export const deleteBlockCustomBlock = async (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: H5pData,
) => {
  const deleteButtonText = document.getElementById(
    CONSTANTS.LOREE_H5P_BLOCK_MODAL_FOOTER_DELETE_BUTTON,
  );
  if (deleteButtonText) deleteButtonText.innerHTML = translate('global.delete');
  const updateId = menuWrapper.parentElement?.parentElement?.getAttribute('id');
  for (const content of elementData) {
    if (updateId === content.id.toString()) {
      menuWrapper.parentElement?.parentElement?.remove();
      await API.graphql(
        graphqlOperation(deleteH5PContent, {
          contentId: content.id,
          platformId: sessionStorage.getItem('ltiPlatformId'),
        }),
      );
    }
  }
  const elementDataHTML = document.getElementById('myInteractives') as HTMLElement;
  const contentList = elementDataHTML.childNodes;
  if (contentList.length === 0) {
    elementDataHTML.innerHTML = `<p class='loree-H5P-block-no-results font-italic' style='width:160px'>${translate(
      'modal.nointeractivesfound',
    )}</p>`;
  }
  const sharedDataHTML = document.getElementById('sharedInteractives') as HTMLElement;
  const sharedDataHTMLList = sharedDataHTML.childNodes;
  if (sharedDataHTMLList.length === 0) {
    sharedDataHTML.innerHTML = `<p class='loree-H5P-block-no-results font-italic' style='width:160px'>${translate(
      'modal.nointeractivesfound',
    )}</p>`;
  }
  elementData = elementData.filter(function (el: H5pDataObject) {
    return el.id.toString() !== updateId;
  });
  searchData = searchData.filter(function (el: H5pDataObject) {
    return el.id.toString() !== updateId;
  });
  searchSharedData = searchSharedData.filter(function (el: H5pDataObject) {
    return el.id.toString() !== updateId;
  });
  h5pContent = h5pContent.filter(function (el: H5pDataObject) {
    return el.id.toString() !== updateId;
  });
  deletePopup(modalType);
  interactiveElementAlert(blockType, translate('modal.delete'), '');
  const alertPopup = document.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
  alertPopup?.classList.remove('d-none');
  setTimeout(() => {
    alertPopup?.remove();
  }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
  return null;
};

export const deletePopup = (modalType: string) => {
  const interactiveBlockBackdrop = document.getElementById(
    `loree-custom-block-${modalType}-backdrop`,
  );
  interactiveBlockBackdrop?.remove();
  const customBlockModalPopup = document.getElementById(
    `loree-custom-block-${modalType}-modal-wrapper`,
  );
  customBlockModalPopup?.remove();
  base.hideCloseRowButton();
  base.showAddRowButton();
  base.hideAddElementButtonToSelectedElement();
  return null;
};

// triggers the edit/delete confirmation popup
export const handleThumbnailMenu = (
  menuWrapper: HTMLElement,
  val: string,
  blockType: string,
  elementData: H5pData,
) => {
  if (val === 'Delete') {
    appendH5PMenuModal('delete', menuWrapper, blockType, elementData);
    attachDeleteEventHandlers('delete');
  } else {
    appendH5PMenuModal('edit', menuWrapper, blockType, elementData);
    attachDeleteEventHandlers('edit');
  }
  return val;
};

// Add the interactives inside the editor
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const handleAddH5PElement = (content: any, elementData: H5pData): void => {
  const id = content.target?.parentElement.parentElement.id;
  for (const h5pData of elementData) {
    if (h5pData.id.toString() === id) {
      handleAutoSaveOnCustomBlockAppend(true);
      base.hideBlockOptionsToSelectedElements();
      const h5pElement = h5pData;
      const h5pIframe = appendCustomElementToEditor(h5pElement);
      base.appendElementToSelectedColumn(h5pIframe);
    }
  }
};

export const appendCustomElementToEditor = (data: H5pDataObject) => {
  const h5pSiteUrl = H5PURL();
  const element = document.createElement('div');
  element.innerHTML = `<div style="padding:10px; margin-bottom: 20px;" class = '${CONSTANTS.LOREE_IFRAME_CONTENT_H5P_WRAPPER}'>
    <iframe 
      class = '${CONSTANTS.LOREE_IFRAME_CONTENT_H5P_ELEMENT}'
      title="${data.title}"
      frameBorder="0"
      width ="100%"
      height="400"
      scrolling="yes"
      src="${h5pSiteUrl}/wp-admin/admin-ajax.php?action=h5p_embed&id=${data.id}">
    </iframe>
  </div>`;
  return element.innerHTML;
};

// Loader trigers when fetching the data
export const H5PmodalLoader = (): string => {
  return `
  <div id="modal-loader" class="m-auto justify-content-center">
  <div class="icon rotating">
  ${videoModalIcons.loader}
  </div>
  <div class="title ml-3">Loading...</div>
</div>`;
};

export const getCourseAccountDetails = async () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const courses: any = await API.graphql(
    graphqlOperation(courseDetails, {
      courseId: sessionStorage.getItem('course_id'),
    }),
  );
  return JSON.parse(courses.data.courseDetails).body;
};
