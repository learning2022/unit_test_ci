import {
  handleParagraphOption,
  handleBoldOption,
  handleItalicOption,
  handleSpacingOption,
  handleForegroundColorPicker,
  handleBackgroundColorPicker,
  handleBorderColorPicker,
  handleUnorderedList,
  handleOrderedList,
  handleWordOption,
  handleAlignmentOption,
  handleLinkOption,
  clearFormatFeature,
} from './native-styler/textOptionsEvents';
import { CustomHeader, TextOptions, TextOptionsTranslationKeys } from './UI/textOptionsData';

export const stylerEventHandlerFactory = (
  type: string,
  button: HTMLButtonElement,
  customLink: { [x: string]: string; color: string; font: string } | '',
  customHeader: CustomHeader,
) => {
  switch (type) {
    case TextOptionsTranslationKeys.Style:
    case TextOptions.Style:
      handleParagraphOption();
      break;
    case TextOptionsTranslationKeys.Bold:
    case TextOptions.Bold:
      handleBoldOption(button);
      break;
    case TextOptionsTranslationKeys.Italic:
    case TextOptions.Italic:
      handleItalicOption(button);
      break;
    case TextOptionsTranslationKeys.LineHeight:
    case TextOptions.LineHeight:
      handleSpacingOption();
      break;
    case TextOptionsTranslationKeys.TextColor:
    case TextOptions.TextColor:
      handleForegroundColorPicker();
      break;
    case TextOptionsTranslationKeys.BackgroundColor:
    case TextOptions.BackgroundColor:
      handleBackgroundColorPicker();
      break;
    case TextOptionsTranslationKeys.BorderColor:
    case TextOptions.BorderColor:
      handleBorderColorPicker();
      break;
    case TextOptionsTranslationKeys.UnorderedList:
    case TextOptions.UnorderedList:
      handleUnorderedList(button);
      break;
    case TextOptionsTranslationKeys.OrderedList:
    case TextOptions.OrderedList:
      handleOrderedList(button);
      break;
    case TextOptionsTranslationKeys.WordOptions:
    case TextOptions.WordOptions:
      handleWordOption();
      break;
    case TextOptionsTranslationKeys.Alignment:
    case TextOptions.Alignment:
      handleAlignmentOption();
      break;
    case TextOptionsTranslationKeys.InsertLink:
    case TextOptions.InsertLink:
      handleLinkOption(customLink);
      break;
    case TextOptionsTranslationKeys.ClearFormat:
    case TextOptions.ClearFormat:
      clearFormatFeature(customHeader);
      break;
  }
};
