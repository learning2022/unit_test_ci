import Base, {
  borderColorPickerPopperInstance,
  foregroundColorPickerPopperInstance,
  textBackgroundColorPickerPopperInstance,
} from '../../../base';
import CONSTANTS from '../../../constant';
import { textOptionIcon } from '../UI/icons';
import {
  getEditorElementsByClassName,
  removeClassToElement,
  getIframedocumentElementById,
  getEditorElementById,
  getIframedocumentElementsByClassName,
} from '../../../utils';
import { customColors, CustomHeader } from '../UI/textOptionsData';
import TextOptions from '../UI/textOptionsUI';
import { WordStyler } from './wordStyler';
import { validateHTMLColorHex } from '../../colorPicker/utils';

const baseClass = (): Base => {
  const base = new Base();
  base.wordStylerObj = new WordStyler();
  return base;
};

const textOptionsClass = (): TextOptions => {
  const textOptions = new TextOptions();
  return textOptions;
};

export const handleParagraphOption = (): void => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER,
  );
  if (!wrapper || !button) return;
  if (wrapper.style.display === 'none') {
    baseClass().openParagraphOption(button, wrapper);
  } else {
    baseClass().closeParagraphOption();
  }
};

export const handleBoldOption = (button: HTMLButtonElement): void => {
  baseClass().boldStyleFeature(button);
};

export const handleItalicOption = (button: HTMLButtonElement): void => {
  baseClass().italicStyleFeature(button);
};

export const handleSpacingOption = (): void => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_CONTENT_WRAPPER,
  );
  if (!wrapper || !button) return;
  if (wrapper.style.display === 'none') {
    baseClass().openSpacingOption(button, wrapper);
    setAppliedSpacingStyles(iframeDocument);
  } else {
    baseClass().closeSpacingOption();
  }
};

const setAppliedSpacingStyles = (iframeDocument: HTMLDocument): void => {
  const lineHeightInput = getIframedocumentElementById(
    iframeDocument,
    'line-height-input',
  ) as HTMLInputElement;
  const wordSpacingInput = getIframedocumentElementById(
    iframeDocument,
    'word-spacing-input',
  ) as HTMLInputElement;
  lineHeightInput.value = baseClass().userLineHeight();
  wordSpacingInput.value = baseClass().userWordSpacing();
};

export const handleForegroundColorPicker = (): void => {
  hideSideBarColorPickers();
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER,
  );
  if (!button || !wrapper) return;
  if (wrapper.style.display === 'none') {
    baseClass().openIframeColorPicker(
      foregroundColorPickerPopperInstance,
      [0, 16],
      button,
      wrapper,
    );
    textOptionsClass().appendCustomHexInputStyle(
      CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER,
    );
    customiseForegroundColorPicker();
    setForegroundColor();
  } else {
    baseClass().closeForegroundColorPicker();
  }
};

export const customiseForegroundColorPicker = () => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const pcrBorder = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER,
  );
  const pcrBorderCancel = getIframedocumentElementsByClassName(
    pcrBorder,
    'pcr-cancel',
  )[0] as HTMLElement;
  const pcrBorderApply = getIframedocumentElementsByClassName(
    pcrBorder,
    'pcr-save',
  )[0] as HTMLElement;
  const pcrBorderClear = getIframedocumentElementsByClassName(
    pcrBorder,
    'pcr-clear',
  )[0] as HTMLElement;
  pcrBorderApply.onclick = () => {
    const borderInputColor = getIframedocumentElementsByClassName(
      pcrBorder,
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    baseClass().setuserForegroundColor(borderInputColor.value);
  };
  pcrBorderCancel.onclick = () => {
    baseClass().handleForeGroundColorPickerCancel(pcrBorder);
  };
  pcrBorderClear.onclick = () => {
    baseClass().setuserForegroundColor('#000000');
    const borderInputColor = getIframedocumentElementsByClassName(
      pcrBorder,
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    borderInputColor.value = '#000000';
  };
};

const setForegroundColor = (): void => {
  const iframe = getEditorElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
  if (!iframe || !iframe.contentWindow) return;
  const data = {
    key: 'setForeGroundColor',
    value: baseClass().userForegroundColor(),
    swatches: customColors,
  };

  iframe.contentWindow.postMessage(data, window.location.origin);
};

export const handleBackgroundColorPicker = (): void => {
  hideSideBarColorPickers();
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER,
  );
  if (!button || !wrapper) return;
  if (wrapper.style.display === 'none') {
    baseClass().openIframeColorPicker(
      textBackgroundColorPickerPopperInstance,
      [0, 16],
      button,
      wrapper,
    );
    textOptionsClass().appendCustomHexInputStyle(
      CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER,
    );
    setBackgroundColor();
    baseClass().customiseTextBackgroundColorPickr();
  } else {
    baseClass().closeBackgroundColorPicker();
  }
};

const hideDesignBackgroundColorPicker = (): void => {
  const designBgColorPicker = getEditorElementsByClassName(
    CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS,
  )[0] as HTMLElement;
  if (designBgColorPicker?.classList.contains('visible'))
    removeClassToElement(designBgColorPicker, 'visible');
};

const setBackgroundColor = (): void => {
  const iframe = getEditorElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
  if (!iframe || !iframe.contentWindow) return;
  const selection = iframe.contentWindow?.getSelection();
  const anchorNode = selection?.anchorNode as HTMLElement;
  const parentNode = anchorNode?.parentElement as HTMLElement;
  let value: string | undefined;
  if (
    (anchorNode?.tagName === 'SPAN' || !anchorNode?.tagName) &&
    !anchorNode?.style?.backgroundColor &&
    parentNode?.style?.backgroundColor
  ) {
    value = parentNode?.style?.backgroundColor
      ? parentNode?.style?.backgroundColor
      : parentNode.parentElement?.style.backgroundColor;
  } else {
    value = baseClass().userBackgroundColor();
  }
  const data = {
    key: 'setBackgroundColor',
    value: value || '',
    swatches: customColors,
  };
  iframe.contentWindow.postMessage(data, window.location.origin);
};

export const handleBorderColorPicker = (): void => {
  hideSideBarColorPickers();
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
  );
  if (!button || !wrapper) return;
  if (wrapper.style.display === 'none') {
    baseClass().openIframeColorPicker(borderColorPickerPopperInstance, [0, 18], button, wrapper);
    textOptionsClass().appendCustomHexInputStyle(
      CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
    );
    baseClass().customiseTextBorderColorPicker();
    textOptionsClass().appendBorderStyles();
    setBorderColor();
    const textBorderStyleDropdown = getIframedocumentElementById(
      iframeDocument,
      'text-border-style-dropdown',
    );
    const textBorderWidthInput = getIframedocumentElementById(
      iframeDocument,
      'text-border-width-input',
    ) as HTMLInputElement;
    if (textBorderStyleDropdown) textBorderStyleDropdown.innerHTML = baseClass().userBorderStyle();
    textBorderWidthInput.value = baseClass().userBorderWidth();
  } else {
    baseClass().closeBorderColorPicker();
  }
};

export const appendCustomHexInput = (wrapper: HTMLElement, id: string): void => {
  const customHexInput = document.createElement('input');
  customHexInput.className = 'custom-hex-color-input';
  customHexInput.type = 'text';
  customHexInput.maxLength = 7;
  customHexInput.onkeyup = (e: KeyboardEvent): void => {
    if (e.key === 'Enter') {
      validateColorPickerHexInput(customHexInput, id);
    }
  };
  customHexInput.oninput = () => {
    if (customHexInput.classList.contains('custom-border-danger'))
      customHexInput.classList.remove('custom-border-danger');
    if (customHexInput.value.length === 7) validateColorPickerHexInput(customHexInput, id);
  };
  wrapper.appendChild(customHexInput);
};

export const validateColorPickerHexInput = (customHexInput: HTMLInputElement, id: string) => {
  if (validateHTMLColorHex(customHexInput.value)) {
    let key;
    switch (id) {
      case CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER:
        key = 'setForeGroundColor';
        baseClass().foregroundColorChange(key, customHexInput.value);
        break;
      case CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER:
        key = 'setBackgroundColor';
        baseClass().textBackgroundColorChange(key, customHexInput.value, 'validatedHex');
        break;
      case CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER:
        key = 'setBorderColor';
        baseClass().borderColorChange(key, customHexInput.value);
        break;
    }
    const iframe = getEditorElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
    if (!iframe || !iframe.contentWindow) return;
    const data = {
      key,
      value: customHexInput.value,
      swatches: customColors,
      isInput: true,
    };
    iframe.contentWindow.postMessage(data, window.location.origin);
  } else {
    customHexInput.classList.add('custom-border-danger');
  }
};

export const appendBorderWidthInput = (wrapper: HTMLElement): void => {
  const borderWidth = document.createElement('div');
  borderWidth.className = 'border-width-container';
  const borderWidthLabel = document.createElement('label');
  borderWidthLabel.innerHTML = 'Width';
  const borderWidthInput = document.createElement('input');
  borderWidthInput.id = 'text-border-width-input';
  borderWidthInput.className = 'border-width-input';
  borderWidthInput.type = 'number';
  borderWidthInput.onkeypress = (e: KeyboardEvent): void => baseClass().preventNonNumericalInput(e);
  borderWidthInput.onkeyup = (e: KeyboardEvent): void =>
    baseClass().onKeyUp(e, borderWidthInput.id);
  borderWidthInput.oninput = (): void => baseClass().handleBorderWidth(borderWidthInput, 'apply');
  borderWidth.appendChild(borderWidthLabel);
  borderWidth.appendChild(borderWidthInput);
  wrapper.appendChild(borderWidth);
};

export const hideDesignBorderColorPicker = (): void => {
  const designBorderColorPicker = getEditorElementsByClassName(
    CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS,
  )[1] as HTMLElement;
  if (designBorderColorPicker?.classList.contains('visible'))
    removeClassToElement(designBorderColorPicker, 'visible');
};

const setBorderColor = (): void => {
  const iframe = getEditorElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
  if (!iframe || !iframe.contentWindow) return;
  const data = {
    key: 'setBorderColor',
    value: baseClass().userBorderColor(),
    swatches: customColors,
  };
  iframe.contentWindow.postMessage(data, window.location.origin);
};

export const handleDropdownValue = (dropdownButton: HTMLElement, option: string): void => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  baseClass().handleBorderStyleChange(option);
  dropdownButton.innerHTML = option;
};

export const handleWordOption = (): void => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_WORD_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_WORD_CONTENT_WRAPPER,
  );
  if (!wrapper || !button) return;
  if (wrapper.style.display === 'none') {
    baseClass().openWordOption(button, wrapper);
  } else {
    baseClass().closeWordOption();
  }
};

export const handleAlignmentOption = (): void => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_CONTENT_WRAPPER,
  );
  if (!wrapper || !button) return;
  if (wrapper.style.display === 'none') {
    baseClass().openAlignmentOption(button, wrapper);
  } else {
    baseClass().closeAlignmentOption();
  }
};

export const handleUnorderedList = (button: HTMLButtonElement): void => {
  baseClass().handleUnOrderList(button);
};

export const handleOrderedList = (button: HTMLButtonElement): void => {
  baseClass().handleOrderList(button);
};

export const appendNewTabCheckbox = (linkOptionWrapper: HTMLDivElement) => {
  const removeLink = document.createElement('span');
  removeLink.className = 'link-options-remove-link';
  removeLink.innerHTML = textOptionIcon.linkRemoverIcon;
  removeLink.onclick = () => baseClass().removeAttachedLink();
  const openInNewTab = document.createElement('span');
  const newTabCheckBox = document.createElement('input');
  newTabCheckBox.type = 'checkbox';
  newTabCheckBox.className = 'link-options-checkbox';
  newTabCheckBox.id = 'link-option-tab-checkbox';
  newTabCheckBox.onclick = () => baseClass().handleLinkCheckBox(newTabCheckBox, 'click');
  const newTabLabel = document.createElement('label');
  newTabLabel.innerHTML = 'Open link in new tab';
  newTabLabel.className = 'link-options-checkbox-text';
  openInNewTab.appendChild(newTabCheckBox);
  openInNewTab.appendChild(newTabLabel);
  linkOptionWrapper.appendChild(removeLink);
  linkOptionWrapper.appendChild(openInNewTab);
};

export const handleLinkOption = (
  customLink: { [x: string]: string; color: string; font: string } | '',
): void => {
  const iframeDocument = baseClass().getDocument();
  if (!iframeDocument) return;
  const button = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON,
  );
  const wrapper = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER,
  );
  const input = getIframedocumentElementById(
    iframeDocument,
    CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT,
  ) as HTMLInputElement;
  const newTabCheckBox = getIframedocumentElementById(
    iframeDocument,
    'link-option-tab-checkbox',
  ) as HTMLInputElement;
  if (!wrapper || !button) return;
  if (wrapper.style.display === 'none') {
    baseClass().openLinkOption(button, wrapper);
    input.onblur = (e: FocusEvent) => {
      const event = e.target as HTMLInputElement;
      validateUrl(event.value, customLink);
      if ((e.relatedTarget as HTMLElement)?.id === 'link-option-tab-checkbox') {
        baseClass().handleLinkCheckBox(newTabCheckBox, 'blur');
      }
    };
    input.onkeydown = (e: KeyboardEvent) => {
      const event = e.target as HTMLInputElement;
      input.style.borderColor = '';
      if (e.keyCode === 13) {
        e.stopPropagation();
        validateUrl(event.value, customLink);
      }
    };
  } else {
    baseClass().closeLinkOption();
  }
};

export const validateUrl = (
  value: string,
  customLink: { [x: string]: string; color: string; font: string } | '',
) => {
  const iframeDocument = baseClass().getDocument();
  const input = iframeDocument?.getElementById(
    CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT,
  ) as HTMLInputElement;
  const urlExpression = baseClass().urlValidatorUrl();
  if (urlExpression[0].test(value)) {
    baseClass().inputLinkHandler(value, customLink);
  } else {
    input.style.borderColor = 'red';
  }
};

export const clearFormatFeature = (customHeader: CustomHeader): void => {
  baseClass().removeAllStyle(customHeader);
};

export const hideSideBarColorPickers = () => {
  hideDesignBackgroundColorPicker();
  hideDesignBorderColorPicker();
};
