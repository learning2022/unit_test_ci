import Base from '../../../base';
import CONSTANTS from '../../../constant';
import Iframe from '../../iframe/iframe';
import { getIFrameElementById } from '../../../common/dom';
import { isD2l } from '../../../../lmsConfig';
import { applyStyleToElement } from '../native-styler/styleApplier';
export class WordStyler extends Base {
  iframeElements: Iframe;
  constructor() {
    super();
    this.iframeElements = new Iframe();
  }

  fontFamily = 'Source Sans Pro';
  fontSize = '12px';
  enableStyler = false;
  applyFontSize = false;
  applyFontFamily = false;
  styleValue = '';
  attribute = '';
  browser = 'firefox';
  applyPartialStyle = false;
  prefontFamily = '';
  prefontSize = '';
  applyFontColor = false;
  applyBGColor = false;
  applyBorderColor = false;
  applyBorderStyle = false;
  applyBorderWidth = false;
  applyFontWeight = false;
  applyFontStyle = false;
  applyTextTransform = false;
  applyTextDecoration = false;
  applyVerticalAlign = false;
  applyColorType = '';
  fontColor = '';
  bgColor = '';
  borderColor = '';
  borderWidth = '';
  borderStyle = '';
  fontWeight = '';
  fontStyle = '';
  textTransform = '';
  textDecoration = '';
  verticalAlign = '';
  newPoint = false;
  linkUrl = '';
  selectedFontFamily = '';
  selectedFontSize = '';
  isTripleClick = false;
  excludeListTags = ['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'OL', 'UL', 'CAPTION'];
  excludeListTagsCopied = ['P', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'OL', 'UL', 'LI', 'CAPTION'];
  colorProperties = ['borderColor', 'borderStyle', 'borderWidth', 'color', 'backgroundColor'];

  fontFamilyStyle(event: HTMLSelectElement): void {
    const fontFamily = event.options[event.selectedIndex].value;
    this.fontFamily = fontFamily;
    this.styleValue = fontFamily;
    this.attribute = 'fontFamily';
    this.enableStyler = true;
    this.applyFontFamily = true;
    this.applyFontSize = false;
    this.styleAction('SPAN');
    this.applyFontFamily = false;
    this.enableStyler = false;
  }

  fontSizeStyle(value: string, units: string): void {
    const fontSize = value;
    this.fontSize = fontSize + units;
    this.styleValue = this.fontSize;
    this.attribute = 'fontSize';
    this.enableStyler = true;
    this.applyFontSize = true;
    this.applyFontFamily = false;
    this.styleAction('SPAN');
    this.applyFontSize = false;
    this.enableStyler = false;
  }

  fontColorStyle(type: string, color: string): void {
    this.applyColorType = type;
    this.fontColor = color;
    this.styleValue = color;
    this.enableStyler = true;
    this.attribute = 'color';
    this.applyFontColor = true;
    this.applyBGColor = false;
    this.styleAction('SPAN');
    this.applyFontColor = false;
    this.enableStyler = false;
  }

  bgColorStyle(type: string, color: string): void {
    this.applyColorType = type;
    this.bgColor = color;
    this.styleValue = color;
    this.enableStyler = true;
    this.attribute = 'backgroundColor';
    this.applyBGColor = true;
    this.applyFontColor = false;
    this.styleAction('SPAN');
    this.applyBGColor = false;
    this.enableStyler = false;
  }

  borderColorStyle(type: string, color: string): void {
    this.applyColorType = type;
    this.borderColor = color;
    this.styleValue = color;
    this.enableStyler = true;
    this.attribute = 'borderColor';
    this.applyBorderColor = true;
    this.styleAction('SPAN');
    this.applyBorderColor = false;
    this.enableStyler = false;
  }

  borderStyleChange(option: string): void {
    this.borderStyle = option;
    this.styleValue = option;
    this.enableStyler = true;
    this.attribute = 'borderStyle';
    this.applyBorderStyle = true;
    this.styleAction('SPAN');
    this.applyBorderStyle = false;
    this.enableStyler = false;
  }

  borderWidthChange(width: string): void {
    this.borderWidth = width + 'px';
    this.styleValue = this.borderWidth;
    this.enableStyler = true;
    this.attribute = 'borderWidth';
    this.applyBorderWidth = true;
    this.styleAction('SPAN');
    this.applyBorderWidth = false;
    this.enableStyler = false;
  }

  boldStyleChange(property: string): void {
    this.fontWeight = property;
    this.styleValue = property;
    this.enableStyler = true;
    this.attribute = 'fontWeight';
    this.applyFontWeight = true;
    this.styleAction('SPAN');
    this.applyFontWeight = false;
    this.enableStyler = false;
  }

  italicStyleChange(property: string): void {
    this.fontStyle = property;
    this.styleValue = property;
    this.enableStyler = true;
    this.attribute = 'fontStyle';
    this.applyFontStyle = true;
    this.styleAction('SPAN');
    this.applyFontStyle = false;
    this.enableStyler = false;
  }

  textTransformChange(property: string): void {
    this.textTransform = property;
    this.styleValue = property;
    this.enableStyler = true;
    this.attribute = 'textTransform';
    this.applyTextTransform = true;
    this.styleAction('SPAN');
    this.applyTextTransform = false;
    this.enableStyler = false;
  }

  textDecorationChange(property: string): void {
    this.textDecoration = property;
    this.styleValue = property;
    this.enableStyler = true;
    this.attribute = 'textDecoration';
    this.applyTextDecoration = true;
    this.styleAction('SPAN');
    this.applyTextDecoration = false;
    this.enableStyler = false;
  }

  textVerticalAlignChange(property: string): void {
    this.verticalAlign = property;
    this.styleValue = property;
    this.enableStyler = true;
    this.attribute = 'verticalAlign';
    this.applyVerticalAlign = true;
    this.styleAction('SPAN');
    this.applyVerticalAlign = false;
    this.enableStyler = false;
  }

  createListElement(type: string) {
    const selected = this.getSelectedContent();
    const range = selected?.getRangeAt(0);
    const preCaretRange = range?.cloneRange() as Range;
    const isExist = this.isListExist(selected?.anchorNode as HTMLElement);
    if (!isExist[0]) {
      this.paragraphToList(selected as Selection, preCaretRange, type);
    } else if (this.neededListConversion(type, this.elementPicker.getElement(isExist[1], 'list'))) {
      const listElement = this.elementPicker.getElement(isExist[1], 'list');
      this.listConversion(listElement, type);
    } else {
      this.listToParagraph(selected as Selection, isExist[1]);
    }
    this.handleSelectedContentChanges();
  }

  neededListConversion(type: string, listElement: HTMLElement): boolean {
    if (
      (type === 'OL' && listElement.tagName === 'UL') ||
      (type === 'UL' && listElement.tagName === 'OL')
    ) {
      return true;
    }
    return false;
  }

  paragraphToList(selected: Selection, preCaretRange: Range, type: string) {
    const anchorParent = this.elementPicker.getElement(selected?.anchorNode as HTMLElement, 'list');
    if (this.isMultiLine(anchorParent)) {
      this.multiLineItems(anchorParent, selected, type);
    } else {
      this.singleLineItem(selected, preCaretRange, anchorParent, type);
    }
  }

  isMultiLine(element: HTMLElement): boolean {
    const childes = Array.from(element.children);
    for (let i = 0; i < childes.length; i++) {
      const element = childes[i];
      if (element.tagName.toLowerCase() === 'br') {
        return true;
      }
    }
    return false;
  }

  listToParagraph(selected: Selection, element: HTMLElement) {
    const parentElement = element.parentElement as HTMLElement;
    parentElement.style.padding = '5px';
    if (!this.isMultipleListItemSelected(selected)) {
      (element.parentElement as HTMLElement).outerHTML = this.listItemToParagraph(
        element.parentElement as HTMLElement,
        element,
      );
    } else {
      const parenListElement = element.parentElement as HTMLElement;
      parenListElement.style.cssText = parenListElement.style.cssText + element.style.cssText;
      if (element.hasAttribute('lang')) parenListElement.lang = element.lang;
      parenListElement.className = element.className;
      const convertedString = this.removeListElement(parenListElement.outerHTML);
      parenListElement.outerHTML = convertedString
        .substring(0, convertedString.lastIndexOf('<br>'))
        .concat('</p>');
    }
  }

  isMultipleListItemSelected(selected: Selection) {
    const anchorItem = this.elementPicker.getElement(selected.anchorNode as HTMLElement, 'LI');
    const focusItem = this.elementPicker.getElement(selected.focusNode as HTMLElement, 'LI');
    return !anchorItem.isSameNode(focusItem);
  }

  checkSelectedNode = (listNode: HTMLElement, listItem: HTMLElement) => {
    return listNode.isSameNode(listItem);
  };

  listItemToParagraph(list: HTMLElement, listItem: HTMLElement): string {
    const listOne = this.createHTMLElement(
      list.tagName,
      list.className,
      list.style.cssText,
      list.hasAttribute('lang') ? list.lang : null,
    );
    const listTwo = this.createHTMLElement(
      list.tagName,
      list.className,
      list.style.cssText,
      list.hasAttribute('lang') ? list.lang : null,
    );
    const newParagraph = this.createHTMLElement(
      'p',
      list.className,
      list.style.cssText + listItem.style.cssText,
      list.hasAttribute('lang') ? list.lang : null,
    );
    let count = 0;
    const childes = Array.from(list.children);
    if (childes.length <= 1) {
      newParagraph.innerHTML = childes[0].firstElementChild
        ? childes[0].innerHTML
        : (childes[0] as HTMLElement).innerText;
      return newParagraph.outerHTML;
    }
    for (let i = 0; i < childes.length; i++) {
      const listNode = childes[i] as HTMLElement;
      if (listNode.nodeName === 'LI' && this.checkSelectedNode(listNode, listItem)) {
        newParagraph.innerHTML = listNode.innerHTML;
        count = 1;
      } else if (count === 1) listTwo.appendChild(listNode);
      else listOne.appendChild(listNode);
    }
    const convertedString =
      (listOne.innerHTML !== '' ? listOne.outerHTML : '') +
      newParagraph.outerHTML +
      (listTwo.innerHTML !== '' ? listTwo.outerHTML : '');
    return convertedString;
  }

  createHTMLElement(
    ElementName: string,
    className: string,
    cssStyle: string,
    langAttrib?: string | null,
  ): HTMLElement {
    const element = document.createElement(ElementName);
    element.className = className;
    element.style.cssText = cssStyle;
    if (langAttrib) element.lang = langAttrib;
    return element;
  }

  setStyleForLI = (anchorParent: HTMLElement, listItem: HTMLElement) => {
    if (!anchorParent.getAttribute('style')) return;
    listItem.setAttribute('style', `${anchorParent.getAttribute('style')}`);
  };

  singleLineItem(
    selected: Selection | null,
    preCaretRange: Range,
    anchorParent: HTMLElement,
    type: string,
  ) {
    const list = document.createElement(type);
    list.style.padding = '0 0 0 40px';
    list.className = anchorParent?.className;
    if (anchorParent?.hasAttribute('lang')) list.lang = anchorParent.lang;
    let listItem = document.createElement('li') as HTMLElement;
    this.setStyleForLI(anchorParent, listItem);
    listItem.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    listItem = this.getSpaceValuesToList(anchorParent, listItem);
    let targetNode = this.findCurrentLineItem(selected as Selection);
    if (
      this.excludeListTags.includes(targetNode.tagName) ||
      targetNode.tagName === 'SPAN' ||
      (targetNode.parentElement as HTMLElement).tagName === 'SPAN'
    ) {
      targetNode = this.elementPicker.getElement(targetNode, 'singleLineItem');
      listItem.innerHTML = targetNode.innerHTML;
      targetNode.textContent = '';
    } else {
      listItem.innerHTML = targetNode.nodeValue as string;
      targetNode.nodeValue = '';
    }
    list.appendChild(listItem);
    preCaretRange?.insertNode(list);
    const childElements = preCaretRange?.commonAncestorContainer?.childNodes;
    const listElement = Array.from(childElements).indexOf(list);
    const prevElements = document.createElement('p');
    if (anchorParent.getAttribute('style'))
      prevElements.setAttribute('style', `${anchorParent.getAttribute('style')}`);
    prevElements.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    prevElements.innerHTML = '';
    const nextElements = document.createElement('p');
    if (anchorParent.getAttribute('style'))
      nextElements.setAttribute('style', `${anchorParent.getAttribute('style')}`);
    nextElements.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    nextElements.innerHTML = '';
    for (let i = 0; i < childElements.length; i++) {
      if (i < listElement) {
        if (childElements[i].nodeValue)
          prevElements.innerHTML += childElements[i].nodeValue as string;
        if ((childElements[i] as HTMLElement).tagName === 'BR') prevElements.innerHTML += '<br>';
      } else if (i > listElement) {
        if (childElements[i].nodeValue)
          nextElements.innerHTML += childElements[i].nodeValue as string;
        if ((childElements[i] as HTMLElement).tagName === 'BR') nextElements.innerHTML += '<br>';
      }
    }
    if (prevElements.firstChild && (prevElements.firstChild as HTMLElement).tagName === 'BR')
      prevElements.firstChild?.remove();
    if (nextElements.firstChild && (nextElements.firstChild as HTMLElement).tagName === 'BR')
      nextElements.firstChild?.remove();
    anchorParent.innerHTML = list.outerHTML;
    const textTag = preCaretRange?.commonAncestorContainer as HTMLElement;
    if (prevElements.innerHTML !== '' && nextElements.innerHTML !== '')
      textTag.outerHTML = prevElements.outerHTML + list.outerHTML + nextElements.outerHTML;
    else if (prevElements.innerHTML !== '')
      textTag.outerHTML = prevElements.outerHTML + list.outerHTML;
    else if (nextElements.innerHTML !== '')
      textTag.outerHTML = list.outerHTML + nextElements.outerHTML;
    else textTag.outerHTML = list.outerHTML;
    if (
      textTag.textContent === list.textContent &&
      this.excludeListTags.includes(textTag.tagName) &&
      textTag.parentNode
    ) {
      (textTag.parentNode as HTMLElement).replaceChild(list, textTag);
    }
  }

  multiLineItems(anchorParent: HTMLElement, selected: Selection | null, type: string) {
    const list = document.createElement(type);
    list.style.padding = '0 0 0 40px';
    list.className = anchorParent?.className;
    if (anchorParent.hasAttribute('lang')) list.lang = anchorParent.lang;
    list.contentEditable = 'true';
    if (selected) {
      const tag = anchorParent.innerHTML.split('<br>');
      tag.forEach((text: string) => {
        const textHtml = new DOMParser().parseFromString(text, 'text/html');
        const textChild = textHtml.getElementsByTagName('body')[0].childNodes[0] as HTMLElement;
        text = text.trim();
        if (text !== '' && textChild) {
          let listItem = document.createElement('li') as HTMLElement;
          this.setStyleForLI(anchorParent, listItem);
          listItem.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
          listItem = this.getSpaceValuesToList(anchorParent, listItem);
          if (
            textHtml.getElementsByTagName('body')[0].childNodes.length === 1 &&
            textChild.tagName === 'SPAN'
          ) {
            if (textChild.getAttribute('style')) {
              listItem.setAttribute('style', `${textChild.getAttribute('style')}`);
            }
            listItem.innerHTML = textChild.innerHTML;
          } else listItem.innerHTML = text;
          list.appendChild(listItem);
        }
      });
      if (anchorParent) {
        anchorParent.outerHTML = list.outerHTML;
      }
    }
    return list;
  }

  getSpaceValuesToList(anchorParent: HTMLElement, listItem: HTMLElement): HTMLElement {
    listItem.style.padding = anchorParent.style.padding;
    listItem.style.marginTop = anchorParent.style.marginTop;
    listItem.style.marginBottom = anchorParent.style.marginBottom;
    listItem.style.marginLeft = anchorParent.style.marginLeft;
    listItem.style.marginRight = anchorParent.style.marginRight;
    return listItem;
  }

  findCurrentLineItem(selected: Selection): HTMLElement {
    const contentArray = selected.anchorNode as HTMLElement;
    return contentArray;
  }

  isListExist(anchor: HTMLElement): [boolean, HTMLElement] {
    if (this.excludeListTagsCopied.includes(anchor.tagName) && anchor.children[0] !== undefined) {
      if (anchor.children[0].nodeName === 'OL' || anchor.children[0].nodeName === 'UL') {
        const listParent = anchor.children[0] as HTMLElement;
        return [true, listParent];
      }
      // return [true, anchor];
    }

    anchor = this.elementPicker.getElement(anchor, 'isList');
    if (anchor.tagName === 'LI') {
      return [true, anchor];
    }
    return [false, anchor];
  }

  listConversion(list: HTMLElement, changeTO: string) {
    const convertedList = this.createHTMLElement(
      changeTO,
      list.className,
      list.style.cssText,
      list.hasAttribute('lang') ? list.lang : null,
    );
    const childes = Array.from(list.children);
    for (let i = 0; i < childes.length; i++) {
      const listItem = childes[i];
      if (listItem.tagName === 'LI') {
        convertedList.appendChild(listItem);
      }
    }
    list.outerHTML = convertedList.outerHTML;
  }

  handleLinkForSelection(
    link: string,
    customLink: { [x: string]: string; color: string; font: string } | '',
  ) {
    this.linkUrl = link;
    this.enableStyler = false;
    let urlFormat: [string, string, string, string] | boolean = [
      'https://',
      'http://',
      'ftp://',
      'smtp://',
    ];
    urlFormat = (urlFormat as [string, string, string, string]).some((prefix: string) =>
      link.includes(prefix),
    );
    if (!urlFormat) link = 'https://' + link;
    this.createLink(link, customLink);
  }

  createLink = (
    link: string,
    customLink: { [x: string]: string; color: string; font: string } | '',
  ) => {
    const selected = this.getSelectedContent();
    // check do we need to create a new anchor tag or edit existing one
    const isAnchorExist = this.checkAnchorExist(selected?.anchorNode as HTMLElement);
    if (isAnchorExist) {
      this.editLink(link);
      return;
    }
    // check do we need to apply anchor on word or on span tag
    const isSPANExist = this.anchorWithSPAN(selected?.anchorNode as HTMLElement);
    const range = selected?.getRangeAt(0);
    const preCaretRange = range?.cloneRange();
    const anchor = document.createElement('a');
    if (link.includes('@')) anchor.setAttribute('href', `mailto:${link}`);
    else anchor.setAttribute('href', link);
    anchor.setAttribute('title', link);
    if (isD2l()) anchor.setAttribute('target', '_blank');
    if (customLink) this.addCustomLinkStyles(anchor, customLink);
    anchor.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    if (isSPANExist[0]) {
      const target = isSPANExist[1];
      anchor.appendChild(target);
    } else {
      const selectedRange = preCaretRange?.cloneContents();
      const text = document.createTextNode(selectedRange?.textContent as string);
      anchor.appendChild(text);
    }
    const anchorIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON);
    (anchorIcon?.childNodes[0] as HTMLElement).classList.add('text-options-icon-highlight');
    preCaretRange?.deleteContents();
    preCaretRange?.insertNode(anchor);
    this.handleSelectedContentChanges();
  };

  checkAnchorExist(wrapper: HTMLElement) {
    while (
      !wrapper?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) &&
      this.elementPicker.checkTagName(wrapper)
    ) {
      if (wrapper?.tagName === 'A') {
        return true;
      }
      wrapper = wrapper?.parentElement as HTMLElement;
    }
    return false;
  }

  anchorWithSPAN(wrapper: HTMLElement): [boolean, HTMLElement] {
    while (
      !wrapper?.classList?.contains(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS) &&
      this.elementPicker.checkTagName(wrapper)
    ) {
      if (wrapper.tagName === 'SPAN') {
        return [true, wrapper];
      }
      wrapper = wrapper.parentElement as HTMLElement;
    }
    return [false, wrapper];
  }

  editLink(link: string) {
    const selected = this.getSelectedContent();
    const targetNode = this.findAnchorNode(selected?.anchorNode as HTMLElement);
    const range = selected?.getRangeAt(0);
    const preCaretRange = range?.cloneRange();
    const selectedRange = preCaretRange?.cloneContents();
    targetNode.href = link;
    targetNode.title = link;
    if (selectedRange?.textContent !== targetNode.textContent) {
      preCaretRange?.deleteContents();
      targetNode.innerText = '';
      targetNode.innerText = selectedRange?.textContent as string;
    }
    this.handleSelectedContentChanges();
  }

  removeLink() {
    // capture the link and remove it
    const selected = this.getSelectedContent();
    const targetNode = this.findAnchorNode(selected?.anchorNode as HTMLElement);
    const docFrag: DocumentFragment = document.createDocumentFragment();
    if (targetNode?.tagName === 'A') {
      while (targetNode.firstChild) {
        docFrag.appendChild(targetNode.firstChild);
      }
      (targetNode.parentNode as Node).replaceChild(docFrag, targetNode);
    }
    this.hideTextOptions();
    this.handleSelectedContentChanges();
  }

  findAnchorNode(wrapper: HTMLElement): HTMLAnchorElement {
    if (!this.elementPicker.checkTagName(wrapper)) {
      wrapper = wrapper.childNodes[0] as HTMLElement;
    } else if (wrapper?.childNodes[0] && (wrapper.childNodes[0] as HTMLElement).tagName === 'A') {
      return wrapper.childNodes[0] as HTMLAnchorElement;
    } else {
      wrapper = this.elementPicker.getElement(wrapper, 'anchorNode');
    }
    return wrapper as HTMLAnchorElement;
  }

  partialOrCompleteSelction(selected: Selection): boolean {
    if (selected && selected.rangeCount > 0) {
      const range = selected.getRangeAt(0);
      const preCaretRange = range.cloneRange();
      const selectedRange = preCaretRange.cloneContents();
      const selectedLength = (selectedRange.textContent as string).length;
      const nodeLength = ((selected.anchorNode as HTMLElement).textContent as string).length;
      if (selectedLength !== nodeLength) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  getMajorStyleSlection(selected: Selection, userStyle: string): Selection {
    switch (userStyle) {
      case 'H1':
      case 'H2':
      case 'H3':
      case 'H4':
      case 'H5':
      case 'H6':
      case 'P': {
        const node: HTMLElement = selected.anchorNode as HTMLElement;
        if (node.parentElement?.tagName === 'SPAN') {
          const range = document.createRange();
          range.selectNodeContents(this.elementPicker.getElement(node.parentElement, 'paragraph'));
          selected?.removeAllRanges();
          selected?.addRange(range);
          this.newPoint = true;
          return selected;
        } else {
          const range = document.createRange();
          range.selectNodeContents(this.elementPicker.getElement(node, 'majorParent'));
          selected?.removeAllRanges();
          selected?.addRange(range);
          this.newPoint = true;
          return selected;
        }
      }
    }
    return selected;
  }

  applyTextTransformStyle(
    wrapper: Node | null,
    wrapperParent: HTMLElement,
    selectedRange: DocumentFragment,
    bold: HTMLElement,
  ) {
    if (wrapperParent.textContent === selectedRange.textContent)
      this.updateTripleClickStyle(wrapper as HTMLElement);
    else bold.style.textTransform = this.textTransform;
    let style = '';
    switch (this.textTransform) {
      case 'uppercase':
        style = 'text-uppercase';
        break;
      case 'lowercase':
        style = 'text-lowercase';
        break;
      case 'capitalize':
        style = 'text-capitalize';
        break;
    }
    bold.classList.add(style);
  }

  styleAction(userStyle: string): void {
    this.isTripleClick = false;
    // check wheather style is applied or not
    let selected = this.getSelectedContent() as Selection;
    if (selected && selected.rangeCount > 0) {
      selected = this.getMajorStyleSlection(selected, userStyle);
      const partially = this.partialOrCompleteSelction(selected);
      if (
        partially &&
        this.iframeElements?.isListItemExist() &&
        this.isMultipleListItemSelected(selected)
      ) {
        this.iframeElements?.modifyOverallSelectionRange();
        selected = this.getSelectedContent() as Selection;
      }
      const wrapper = selected.anchorNode;
      let wrapperParent = wrapper as HTMLElement;
      if (wrapper?.nodeName !== 'LI') {
        wrapperParent = this.elementPicker.getElement(
          selected.anchorNode as HTMLElement,
          'majorParent',
        );
      }
      const range = selected.getRangeAt(0);
      const preCaretRange = range.cloneRange();
      const selectedRange = preCaretRange.cloneContents();
      let isApplied = false;
      if (!partially) {
        isApplied = this.applyOrRemoveStyle(wrapper as HTMLElement, userStyle);
      }

      if (partially) {
        // get parent style FF, and FS to apply with partial style
        const style: string[] = this.selectPreviousStyle(selected);
        this.prefontFamily = style[0];
        this.prefontSize = style[1];
        this.applyPartialStyle = true;
      }

      if (wrapperParent.textContent === selectedRange.textContent) this.isTripleClick = true;

      if (isApplied) {
        if (wrapperParent.textContent === selectedRange.textContent) {
          this.isTripleClick = true;
          this.updateExitingStyle(wrapper?.parentElement as HTMLElement);
        } else {
          this.updateExitingStyle(wrapper as HTMLElement);
        }
      } else {
        // apply style tag
        // before apply this style I have to remove the current tag
        if (userStyle !== 'SPAN') {
          // delete the selected anchor node
          if (this.newPoint) {
            this.replaceWithParagraph(selected.anchorNode as HTMLElement, userStyle);
            this.newPoint = false;
          } else {
            this.replaceWithParagraph(
              (selected.anchorNode as HTMLElement).parentElement as Element,
              userStyle,
            );
          }
          this.handleSelectedContentChanges();
          return;
        }

        const bold = document.createElement(userStyle.toString());
        bold.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
        bold.contentEditable = 'true';

        if (userStyle.toString() === 'A') {
          bold.setAttribute('href', 'https://www.google.com/');
        }

        if (this.applyFontSize) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.fontSize = this.fontSize;
          this.applyFontSize = false;
        }

        if (this.applyFontFamily) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.fontFamily = this.fontFamily;
          this.applyFontFamily = false;
        }

        if (this.applyFontColor) {
          const wrapperElement = selected.anchorNode;
          const wrapperParentElement = wrapperElement?.parentElement as HTMLElement;
          const style = { color: this.fontColor, onSpanCallback: this.attachAttributesToSpan };
          const isHTMLElement = wrapperElement?.textContent === selectedRange.textContent;

          if (isHTMLElement) {
            switch (true) {
              case wrapperElement?.nodeName === 'A':
                applyStyleToElement(wrapperElement as HTMLAnchorElement, style); // update color to span or anchor tag
                return;
              case wrapperParentElement?.nodeName === 'A' && isHTMLElement:
                applyStyleToElement(wrapperParentElement, style); // update color to span or anchor tag
                return;
              default:
                this.updateTripleClickStyle(wrapper as HTMLElement);
                break;
            }
          } else {
            bold.style.color = this.fontColor;
          }

          this.applyFontColor = false;
        }

        if (this.applyBGColor) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.backgroundColor = this.bgColor;
          this.applyBGColor = false;
        }

        if (this.applyBorderColor) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.borderColor = this.borderColor;
          this.applyBorderColor = false;
        }

        if (this.applyBorderStyle) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.borderStyle = this.borderStyle;
          this.applyBorderStyle = false;
        }

        if (this.applyBorderWidth) {
          if (wrapperParent.textContent === selectedRange.textContent) {
            this.updateTripleClickStyle(wrapper as HTMLElement);
          } else {
            bold.style.borderWidth = this.borderWidth;
          }
          this.applyBorderWidth = false;
        }

        if (this.applyFontWeight) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.fontWeight = this.fontWeight;
          if (bold.style.fontWeight === 'bold') {
            bold.classList.add('bStyle');
          } else if (bold.style.fontWeight === 'normal') {
            bold.classList.remove('bStyle');
            bold.classList.add('normalStyle');
          }
          this.applyFontWeight = false;
        }

        if (this.applyFontStyle) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.fontStyle = this.fontStyle;
          this.applyFontStyle = false;
        }
        if (this.applyTextTransform) {
          this.applyTextTransformStyle(wrapper, wrapperParent, selectedRange, bold);
          this.applyTextTransform = false;
        }

        if (this.applyTextDecoration) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.textDecoration = this.textDecoration;
          this.applyTextDecoration = false;
        }

        if (this.applyVerticalAlign) {
          if (wrapperParent.textContent === selectedRange.textContent)
            this.updateTripleClickStyle(wrapper as HTMLElement);
          else bold.style.verticalAlign = this.verticalAlign;
          this.applyVerticalAlign = false;
        }
        this.appendSpanToSelectedRange(wrapperParent, selectedRange, preCaretRange, bold);
        const newSelected = this.getSelectedContent() as Selection;
        this.updateStyleToChildElement(newSelected.anchorNode as HTMLElement);
      }
      this.handleSelectedContentChanges();
    }
  }

  attachAttributesToSpan = (spanElement: HTMLSpanElement) => {
    spanElement.className = 'loree-iframe-content-element';
    spanElement.contentEditable = 'true';
    return spanElement;
  };

  appendSpanToSelectedRange = (
    wrapperParent: HTMLElement,
    selectedRange: DocumentFragment,
    preCaretRange: Range,
    bold: HTMLElement,
  ) => {
    if (wrapperParent.textContent !== selectedRange.textContent || !this.isTripleClick) {
      const childList = selectedRange.childNodes;
      let childHTML = '';
      for (let i = 0; i < childList.length; i++) {
        if (
          (childList[i] as HTMLElement).tagName === 'SPAN' ||
          (childList[i] as HTMLElement).tagName === 'A'
        ) {
          childHTML += (childList[i] as HTMLElement).outerHTML;
        } else if ((childList[i] as HTMLElement).tagName === 'BR') {
          childHTML += '<br>';
        } else {
          childHTML += (childList[i] as HTMLElement).textContent as string;
        }
        if (i === childList.length - 1) {
          bold.innerHTML = childHTML;
        }
      }
      preCaretRange.deleteContents();
      preCaretRange.insertNode(bold);
      const selected2 = this.getSelectedContent();
      if (selected2) {
        selected2.selectAllChildren(bold);
      }
      this.removeEmptyElements(selected2?.anchorNode as HTMLElement);
    }
    return bold;
  };

  removeEmptyElements = (element: HTMLElement) => {
    const elementParent = this.elementPicker.getElement(element, 'blockElement');
    elementParent.childNodes.forEach((elementChild) => {
      if (
        elementChild?.textContent?.length === 0 &&
        (elementChild.nodeName === 'SPAN' || elementChild.nodeName === 'A')
      ) {
        elementParent.removeChild(elementChild);
      }
    });
  };

  updateTripleClickStyle(wrapper: HTMLElement) {
    if (wrapper.nodeName !== 'LI') {
      wrapper = this.elementPicker.getElement(wrapper, 'tripleClick');
    }
    this.updateExitingStyle(wrapper);
    if (this.attribute !== 'verticalAlign') {
      for (const child of wrapper.childNodes) {
        const element = child as HTMLElement;
        if (element && element.tagName === 'SPAN') this.checkUpdateStyles(wrapper, element);
        if (element && element.tagName === 'A' && element.childNodes) {
          this.checkUpdateStyles(wrapper, element);
          for (const anchorTag of child.childNodes) {
            const childElement = anchorTag as HTMLElement;
            if (childElement && childElement.tagName === 'SPAN')
              this.checkUpdateStyles(wrapper, childElement);
          }
        }
        if ((element && element.tagName === 'OL') || (element && element.tagName === 'UL')) {
          for (const listTag of element.childNodes) {
            const liTag = listTag as HTMLElement;
            if (liTag && liTag.tagName === 'LI') this.checkUpdateStyles(wrapper, liTag);
          }
        }
      }
    }
    this.isTripleClick = true;
  }

  checkUpdateStyles(wrapper: HTMLElement, childElement: HTMLElement) {
    if (
      childElement &&
      childElement.tagName !== '' &&
      !this.colorProperties.includes(this.attribute)
    ) {
      this.updateExitingStyle(childElement);
    } else if (childElement.style !== undefined && wrapper.style.borderWidth !== '0px') {
      if (childElement.tagName !== 'SPAN') childElement.style.border = '';
    }
  }

  removeExistingTextCases(tagStyle: string) {
    const uppercaseIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_UPPERCASE_BUTTON);
    const lowercaseIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_LOWERCASE_BUTTON);
    const titlecaseIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_TITLECASE_BUTTON);
    const textCaseStyles = ['uppercase', 'lowercase', 'capitalize'];
    const textCaseButtons = [uppercaseIcon, lowercaseIcon, titlecaseIcon];
    if (textCaseStyles.includes(tagStyle)) {
      for (let i = 0; i < textCaseStyles.length; i++) {
        if (tagStyle === textCaseStyles[i]) {
          (textCaseButtons[i]?.childNodes[0] as HTMLElement).classList.toggle(
            'text-options-icon-highlight',
          );
        } else {
          (textCaseButtons[i]?.childNodes[0] as HTMLElement).classList.remove(
            'text-options-icon-highlight',
          );
        }
      }
    }
  }

  removeExistingTextDecorations(tagStyle: string) {
    const underlineIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_UNDERLINE_BUTTON);
    const strikethroughIcon = getIFrameElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_STRIKETHROUGH_BUTTON,
    );
    const superscriptIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_SUPERSCRIPT_BUTTON);
    const subscriptIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_OPTIONS_SUBSCRIPT_BUTTON);
    switch (tagStyle) {
      case 'underline':
        (strikethroughIcon?.childNodes[0] as HTMLElement).classList.remove(
          'text-options-icon-highlight',
        );
        (underlineIcon?.childNodes[0] as HTMLElement).classList.toggle(
          'text-options-icon-highlight',
        );
        break;
      case 'line-through':
        (underlineIcon?.childNodes[0] as HTMLElement).classList.remove(
          'text-options-icon-highlight',
        );
        (strikethroughIcon?.childNodes[0] as HTMLElement).classList.toggle(
          'text-options-icon-highlight',
        );
        break;
      case 'sub':
        (superscriptIcon?.childNodes[0] as HTMLElement).classList.remove(
          'text-options-icon-highlight',
        );
        (subscriptIcon?.childNodes[0] as HTMLElement).classList.toggle(
          'text-options-icon-highlight',
        );
        break;
      case 'super':
        (subscriptIcon?.childNodes[0] as HTMLElement).classList.remove(
          'text-options-icon-highlight',
        );
        (superscriptIcon?.childNodes[0] as HTMLElement).classList.toggle(
          'text-options-icon-highlight',
        );
        break;
    }
  }

  removeExistingAlignments(tagStyle: string) {
    const leftAlignIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_ALIGN_LEFT_BUTTON);
    const centerAlignIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_ALIGN_CENTER_BUTTON);
    const rightAlignIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_ALIGN_RIGHT_BUTTON);
    const justifyAlignIcon = getIFrameElementById(CONSTANTS.LOREE_TEXT_ALIGN_JUSTIFY_BUTTON);
    const alignmentIcons = [leftAlignIcon, centerAlignIcon, rightAlignIcon, justifyAlignIcon];
    const alignmentValues = ['left', 'center', 'right', 'justify'];
    if (alignmentValues.includes(tagStyle)) {
      for (let i = 0; i < alignmentValues.length; i++) {
        if (tagStyle === alignmentValues[i]) {
          (alignmentIcons[i]?.childNodes[0] as HTMLElement).classList.toggle(
            'text-options-icon-highlight',
          );
        } else {
          (alignmentIcons[i]?.childNodes[0] as HTMLElement).classList.remove(
            'text-options-icon-highlight',
          );
        }
      }
    }
  }

  handleSelectedTagStyle(customHeader: _Any, type: string) {
    if (customHeader !== '') {
      if (type === 'P') {
        this.selectedFontFamily = customHeader[6].paragraph.font;
        this.selectedFontSize = customHeader[6].paragraph.size;
      } else {
        let typeNumber: string | number = type.replace(/^\D+/g, '');
        typeNumber = parseInt(typeNumber);
        this.selectedFontFamily = customHeader[typeNumber - 1][`h${typeNumber}`].font;
        this.selectedFontSize = customHeader[typeNumber - 1][`h${typeNumber}`].size;
      }
    }
  }

  replaceWithParagraph(node: Element, tag: string) {
    const ele = document.createElement(tag);
    ele.className = node.className;
    if (node.id) {
      ele.id = node.id;
    }
    if (node.getAttribute('style')) ele.setAttribute('style', `${node.getAttribute('style')}`);
    if (node.getAttribute('lang')) ele.setAttribute('lang', `${node.getAttribute('lang')}`);
    ele.style.fontFamily = this.selectedFontFamily;
    ele.style.fontSize = this.selectedFontSize;
    ele.innerHTML = node.innerHTML;
    for (const child of ele.childNodes as _Any) {
      if (child?.tagName === 'A' && child?.childNodes) {
        for (const anchorTag of child.childNodes) {
          if (anchorTag?.tagName === 'SPAN') {
            this.updateChildStyles(anchorTag, this.selectedFontFamily, this.selectedFontSize);
          }
        }
      }
      if (child.tagName === 'SPAN') {
        this.updateChildStyles(child, this.selectedFontFamily, this.selectedFontSize);
      }
    }
    node?.parentElement?.insertBefore(ele, node);
    node.remove();
  }

  updateChildStyles(child: HTMLElement, fontFamily: string, fontSize: string) {
    child.style.fontFamily = fontFamily;
    child.style.fontSize = fontSize;
  }

  updateExitingStyle(wrapper: HTMLElement): void {
    if (wrapper?.style !== undefined) {
      this.updateAttributeValue(wrapper);
    } else {
      wrapper = wrapper.parentElement as HTMLElement;
      this.updateAttributeValue(wrapper);
    }
    wrapper.style[this.attribute as _Any] = this.styleValue;
    this.updateStyleToChildElement(wrapper);
  }

  updateAttributeValue(element: HTMLElement) {
    switch (this.styleValue) {
      case 'bold':
        element.classList?.toggle('bStyle');
        element.classList?.remove('normalStyle');
        break;
      case 'normal':
        element.classList?.toggle('normalStyle');
        element.classList?.remove('bStyle');
        break;
      case 'uppercase':
        element.classList?.toggle('text-uppercase');
        element.classList?.remove('text-lowercase', 'text-capitalize');
        break;
      case 'lowercase':
        element.classList?.toggle('text-lowercase');
        element.classList?.remove('text-uppercase', 'text-capitalize');
        break;
      case 'capitalize':
        element.classList?.toggle('text-capitalize');
        element.classList?.remove('text-uppercase', 'text-lowercase');
        break;
      case 'revert':
        element.classList?.remove('text-uppercase', 'text-lowercase', 'text-capitalize');
        break;
    }
  }

  updateStyleToChildElement(element: HTMLElement) {
    this.updateChildElements(element);
  }

  updateChildElements(element: HTMLElement) {
    const elList = element.childNodes;
    for (let i = 0; i < elList?.length; i++) {
      const element = elList[i] as HTMLElement;
      this.actionOnElementStyle(element);
      if (element.nodeName !== '#text') {
        this.updateAttributeValue(element);
      }
      if (element.childNodes.length > 0) {
        this.updateChildElements(element);
      }
    }
  }

  actionOnElementStyle(element: HTMLElement) {
    const borderProperties = ['borderColor', 'borderWidth', 'borderStyle'];
    if (element.tagName === 'SPAN' || element.tagName === 'LI' || element.tagName === 'A') {
      if (this.styleValue === 'bold') {
        element.classList.toggle('bStyle');
      } else if (this.attribute === 'fontWeight' && this.styleValue === 'normal') {
        element.classList.toggle('normalStyle');
      }
      if (
        this.colorProperties.includes(this.attribute) &&
        this.applyColorType !== 'colorPickerSave'
      ) {
        return;
      }
      if (borderProperties.includes(this.attribute)) {
        element.style[this.attribute as _Any] = '';
        return;
      }
      element.style[this.attribute as _Any] = this.styleValue;
    }
  }

  getSelectedContent(): Selection | null {
    const iframeDocument = this.getDocument();
    if (iframeDocument) {
      const selected = iframeDocument.getSelection();
      return selected;
    }
    return null;
  }

  textAlignment(align: string): void {
    const selected = this.getSelectedContent();
    if (!selected) return;
    const wrapper = this.elementPicker.getElement(
      selected.anchorNode as HTMLElement,
      'majorParent',
    );
    wrapper.style.textAlign = align;
    this.applyStyleToListElement(wrapper, align);
    this.handleSelectedContentChanges();
  }

  applyStyleToListElement = (wrapper: HTMLElement, align: string) => {
    // to set alignment for list tags
    if (wrapper.nodeName !== 'UL' && wrapper.nodeName !== 'OL') return;
    const listItems = Array.from(wrapper.getElementsByTagName('li'));
    listItems?.forEach((list) => {
      if (!(list as HTMLElement).style.textAlign) return;
      (list as HTMLElement).style.textAlign = align;
    });
  };

  // line-height
  spacingText(val: string): void {
    const selected = this.getSelectedContent();
    if (selected) {
      const wrapper = this.elementPicker.getElement(
        selected.focusNode as HTMLElement,
        'spacingText',
      );
      wrapper.style.lineHeight = val;
    }
    this.handleSelectedContentChanges();
  }

  // word spacing
  wordSpacing(val: string): void {
    const selected = this.getSelectedContent();
    if (selected) {
      const wrapper = this.elementPicker.getElement(
        selected.focusNode as HTMLElement,
        'wordSpacing',
      );
      wrapper.style.wordSpacing = val + 'px';
    }
    this.handleSelectedContentChanges();
  }

  // find the selected tag
  removeAdjecentTags(selected: Selection): Node | null {
    let wrapper = selected.anchorNode;
    if (this.browser === 'chrome') {
      wrapper = (selected.anchorNode as Node).parentElement;
    }
    const current = ((wrapper as Node).textContent as string).length;
    let parentLength = current;
    let targetWrapper = wrapper;
    while (current === parentLength && (wrapper as HTMLElement).tagName !== 'P') {
      targetWrapper = wrapper; // hold previous node
      wrapper = (targetWrapper as Node).parentElement; // increase to parent node
      parentLength = ((wrapper as Node).textContent as string).length; // check parent node content length
    }
    return targetWrapper;
  }

  applyOrRemoveStyle(wrapper: HTMLElement | null, style: string): boolean {
    // check same div is applied or not (upto "DIV")
    if (wrapper?.tagName === undefined) {
      wrapper = wrapper?.parentElement ? wrapper.parentElement : null;
    }
    while (wrapper !== null && wrapper?.tagName !== 'DIV' && wrapper?.tagName !== 'BODY') {
      if (wrapper?.tagName === style) {
        return true;
      }
      wrapper = wrapper?.parentElement ? wrapper.parentElement : null;
    }
    return false;
  }
}
