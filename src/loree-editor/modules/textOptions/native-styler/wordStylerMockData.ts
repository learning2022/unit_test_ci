export const fontFamilyInput = `<p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: Lato; font-size: 16px;"><span class="loree-iframe-content-element" style="font-family: &quot;Mont Bold&quot;;"><span class="loree-iframe-content-element" style="font-family: &quot;Dancing Script&quot;;">Insert</span> text</span> here</p>`;
export const fontFamilyOutput = `<p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #000000; color: rgb(0, 0, 0); padding: 5px; margin: 0px 0px 10px; font-family: Times New Roman; font-size: 16px;"><span class="loree-iframe-content-element" style="font-family: Times New Roman;"><span class="loree-iframe-content-element" style="font-family: Times New Roman;">Insert</span> text</span> here</p>`;
export const fontColorInput = `<p class="loree-iframe-content-element element-highlight" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px; font-family: ABeeZee; font-size: 20px;" contenteditable="true"><span class="loree-iframe-content-element element-highlight" contenteditable="true" style="color: rgb(192, 134, 134);"><span class="loree-iframe-content-element" contenteditable="true" style="color: rgb(67, 106, 190);">Insert</span> text</span> here</p>`;
export const fontColorOutput = `<p class="loree-iframe-content-element element-highlight" style="border-width: 0px; border-style: solid; border-color: #000000; color: rgb(0, 13, 156); padding: 5px; margin: 0px; font-family: ABeeZee; font-size: 20px;" contenteditable="true"><span class="loree-iframe-content-element element-highlight" contenteditable="true" style="color: rgb(0, 13, 156);"><span class="loree-iframe-content-element" contenteditable="true" style="color: rgb(0, 13, 156);">Insert</span> text</span> here</p>`;
export const borderColorInput = `<p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: Lato; font-size: 16px;"><span class="loree-iframe-content-element " style="border-width: 3px; border-style: solid; border-color: #c99292;"><span class="loree-iframe-content-element" style="border-color: #bc34db; border-style: dotted; border-width: 3px;">Insert</span> text</span> here</p>`;
export const borderColorOutput = `<p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: #ffffff; color: rgb(0, 0, 0); padding: 5px; margin: 0px 0px 10px; font-family: Lato; font-size: 16px;"><span class="loree-iframe-content-element " style="border-width: 3px; border-style: solid;"><span class="loree-iframe-content-element" style="border-style: dotted; border-width: 3px;">Insert</span> text</span> here</p>`;
export const convertedUlList =
  '<ul style="padding: 0px 0px 0px 40px;" class="loree-iframe-content-element element-highlight"><li style="fontWeight:\'bold\',fontStyle:\'italic\',textDecoration:\'line-through\'," class="loree-iframe-content-element">Insert text here</li></ul>';
export const convertedOlList =
  '<ol style="padding: 0px 0px 0px 40px;" class="loree-iframe-content-element element-highlight" lang="ta"><li style="fontWeight:\'bold\',fontStyle:\'italic\',textDecoration:\'line-through\'," class="loree-iframe-content-element">Insert text here</li></ol>';

export const selection = {
  anchorNode: {
    nodeName: '#text',
    nextSibling: null,
    firstChild: null,
    className: 'loree-iframe-content-element element-highlight',
    innerHTML: 'Insert text here',
    tagName: 'P',
    style: {
      fontWeight: 'bold',
      fontStyle: 'italic',
      textDecoration: 'line-through',
    },
    lang: 'ta',
    parentElement: {
      nodeName: '#text',
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      tagName: 'P',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        textDecoration: 'line-through',
      },
    },
    getAttribute: jest
      .fn()
      .mockImplementation(
        () => `fontWeight:'bold',fontStyle:'italic',textDecoration:'line-through',`,
      ),
    hasAttribute: jest
      .fn()
      .mockImplementationOnce(() => false)
      .mockImplementationOnce(() => true),
    children: [
      {
        nodeName: '#text',
        nextSibling: null,
        firstChild: null,
        className: 'loree-iframe-content-element element-highlight',
        innerHTML: 'Insert text here',
        tagName: 'P',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
          textDecoration: 'line-through',
        },
      },
    ],
  } as unknown as HTMLElement,
  type: 'Range',
} as unknown as Selection;

export const rangeCountForPartialSelection = {
  anchorNode: {
    nodeName: '#text',
    nextSibling: null,
    firstChild: null,
    className: 'loree-iframe-content-element element-highlight',
    innerHTML: 'Insert text here',
    textContent: 'Insert text here',
    tagName: 'P',
    style: {
      fontWeight: 'bold',
      fontStyle: 'italic',
      textDecoration: 'line-through',
    },
    parentElement: {
      nodeName: '#text',
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      tagName: 'P',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        textDecoration: 'line-through',
      },
    },
  } as unknown as HTMLElement,
  textContent: 'Insert text here',
  type: 'Range',
  rangeCount: 1,
  getRangeAt: jest.fn().mockImplementation(() => {
    return {
      cloneRange: jest.fn().mockImplementation(() => {
        return {
          cloneContents: jest.fn().mockImplementation(() => {
            return rangeCountForPartialSelection;
          }),
        };
      }),
    };
  }),
} as unknown as Selection;

export const validRangeCountForSelection = {
  anchorNode: {
    nodeName: '#text',
    nextSibling: null,
    firstChild: null,
    className: 'loree-iframe-content-element element-highlight',
    innerHTML: 'Insert text here',
    textContent: 'Insert text here Insert text here',
    tagName: 'P',
    style: {
      fontWeight: 'bold',
      fontStyle: 'italic',
      textDecoration: 'line-through',
    },
    parentElement: {
      nodeName: '#text',
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      tagName: 'P',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        textDecoration: 'line-through',
      },
    } as unknown as Element,
  } as unknown as HTMLElement,
  textContent: 'Insert text here',
  type: 'Range',
  rangeCount: 1,
  getRangeAt: jest.fn().mockImplementation(() => {
    return {
      cloneRange: jest.fn().mockImplementation(() => {
        return {
          cloneContents: jest.fn().mockImplementation(() => {
            return validRangeCountForSelection;
          }),
        };
      }),
    };
  }),
} as unknown as Selection;

export const preCaretRnage = {
  commonAncestorContainer: {
    childNodes: [
      {
        nodeName: '#text',
        nextSibling: null,
        firstChild: null,
        className: 'loree-iframe-content-element element-highlight',
        innerHTML: 'Insert text here',
        tagName: 'P',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
          textDecoration: 'line-through',
        },
      },
    ],
  },
  insertNode: jest.fn(),
} as unknown as Range;

const selectionForFontColor = {
  anchorNode: {
    nodeName: 'A',
    nextSibling: null,
    firstChild: null,
    className: 'loree-iframe-content-element element-highlight',
    innerHTML: 'Insert text here',
    textContent: 'Insert text here',
    tagName: 'P',
    style: {
      fontWeight: 'bold',
      fontStyle: 'italic',
      textDecoration: 'line-through',
    },
    childNodes: [],
    parentElement: {
      nodeName: '#text',
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      tagName: 'P',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        textDecoration: 'line-through',
      },
    },
    querySelectorAll: jest.fn().mockImplementation(() => {
      return [];
    }),
  } as unknown as HTMLElement,
  textContent: 'Insert text here',
  type: 'Range',
  collapsed: true,
  rangeCount: 1,
  getRangeAt: jest.fn().mockImplementation(() => {
    return {
      cloneRange: jest.fn().mockImplementation(() => {
        return {
          cloneContents: jest.fn().mockImplementation(() => {
            return selectionForFontColor;
          }),
        };
      }),
    };
  }),
} as unknown as Selection;

const selectionForFontColorWithAnchor = {
  anchorNode: {
    nodeName: '#text',
    nextSibling: null,
    firstChild: null,
    className: 'loree-iframe-content-element element-highlight',
    innerHTML: 'Insert text here',
    textContent: 'Insert text here',
    tagName: 'P',
    style: {
      fontWeight: 'bold',
      fontStyle: 'italic',
      textDecoration: 'line-through',
    },
    parentElement: {
      nodeName: 'A',
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      tagName: 'P',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        textDecoration: 'line-through',
      },
      childNodes: [],
      querySelectorAll: jest.fn().mockImplementation(() => {
        return [];
      }),
    },
  } as unknown as HTMLElement,
  textContent: 'Insert text here',
  type: 'Range',
  collapsed: true,
  rangeCount: 1,
  getRangeAt: jest.fn().mockImplementation(() => {
    return {
      cloneRange: jest.fn().mockImplementation(() => {
        return {
          cloneContents: jest.fn().mockImplementation(() => {
            return selectionForFontColorWithAnchor;
          }),
        };
      }),
    };
  }),
} as unknown as Selection;

export const mockDataToApplyFontColor = [
  { selection: selectionForFontColor, content: selectionForFontColor.anchorNode },
  {
    selection: selectionForFontColorWithAnchor,
    content: selectionForFontColorWithAnchor.anchorNode?.parentElement,
  },
];
