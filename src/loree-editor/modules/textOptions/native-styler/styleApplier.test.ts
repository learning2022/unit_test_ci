import { applyStyleToElement, applyStyleToRange, StyleApplierOptions } from './styleApplier';

describe('styleApplier', () => {
  let div: HTMLDivElement;
  let text: Node;

  beforeEach(() => {
    div = document.createElement('div');
    text = document.createTextNode('This is a string');
    div.appendChild(text);
    document.body.innerHTML = '';
    document.body.appendChild(div);
  });

  test('empty range does nothing', () => {
    const r = document.createRange();
    r.setStart(text, 1);
    r.setEnd(text, 1);

    applyStyleToRange(r, { backgroundColor: 'red' }); // var(--lt-theme1-color1)' });

    expect(div.innerHTML).toEqual('This is a string');
  });

  test('applyStyle 1', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { backgroundColor: 'red' });

    expect(div.innerHTML).toEqual('Th<span style="background-color: red;">is is a </span>string');
  });

  test('applyStyle 2', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { backgroundColor: 'red' });
    expect(div.innerHTML).toEqual('Th<span style="background-color: red;">is is a </span>string');
    applyStyleToRange(r, { backgroundColor: 'blue' });
    expect(div.innerHTML).toEqual('Th<span style="background-color: blue;">is is a </span>string');
  });

  test('applyStyle 3', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { color: 'red' });
    r.setStart(text, 1);
    applyStyleToRange(r, { backgroundColor: 'blue' });
    expect(div.innerHTML).toEqual(
      'T<span style="background-color: blue;">h<span style="color: red;">is is a </span></span>string',
    );
  });

  test('applyStyle to element', () => {
    applyStyleToElement(div, { backgroundColor: 'red' });
    expect(document.body.innerHTML).toEqual(
      '<div style="background-color: red;">This is a string</div>',
    );
  });

  test('applyStyle to element with child span', () => {
    document.body.innerHTML = '<div>This <span style="color: blue;">is </span>a test</div>';
    div = document.body.querySelector('div')!;
    applyStyleToElement(div, { color: 'red' });
    expect(document.body.innerHTML).toEqual('<div style="color: red;">This is a test</div>');
  });

  test('applyStyle to element with child span2', () => {
    document.body.innerHTML =
      '<div>This <span style="background-color: orange; color: blue;">is </span>a test</div>';
    div = document.body.querySelector('div')!;
    applyStyleToElement(div, { backgroundColor: 'red' });
    expect(document.body.innerHTML).toEqual(
      '<div style="background-color: red;">This <span style="color: blue;">is </span>a test</div>',
    );
  });

  test('applyStyle overlapping', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { backgroundColor: 'red' });

    r.setStart(text, 1);
    const sp = div.querySelector('span') as HTMLSpanElement;
    // console.log(sp);
    expect(sp).not.toBeNull();
    r.setEnd(sp.childNodes[0], 2);

    applyStyleToRange(r, { color: 'blue' }); // var(--lt-theme1-color1)' });
    expect(document.body.innerHTML).toEqual(
      '<div>T<span style="color: blue;">h<span style="background-color: red;">is</span></span><span style="background-color: red;"> is a </span>string</div>',
    );
  });

  test('applyStyle overlapping collapsing', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { backgroundColor: 'red' });
    const sp = div.querySelector('span') as HTMLSpanElement;
    // console.log(sp);
    expect(sp).not.toBeNull();
    r.setEnd(sp.childNodes[0], 2);

    applyStyleToRange(r, { color: 'blue' });
    expect(document.body.innerHTML).toEqual(
      '<div>Th<span style="color: blue; background-color: red;">is</span><span style="background-color: red;"> is a </span>string</div>',
    );
  });

  test('applyStyle overlapping removing redundant', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { fontSize: '12px' });
    const sp = div.querySelector('span') as HTMLSpanElement;
    // console.log(sp);
    r.setStart(text, 1);
    expect(sp).not.toBeNull();
    r.setEnd(sp.childNodes[0], 4);

    applyStyleToRange(r, { fontSize: '14px' });
    expect(document.body.innerHTML).toEqual(
      '<div>T<span style="font-size: 14px;">his i</span><span style="font-size: 12px;">s a </span>string</div>',
    );
  });

  test('applyStyle overlapping not removing semi-redundant', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { fontSize: '12px', color: 'red' });
    const sp = div.querySelector('span') as HTMLSpanElement;
    // console.log(sp);
    r.setStart(text, 1);
    expect(sp).not.toBeNull();
    r.setEnd(sp.childNodes[0], 4);

    applyStyleToRange(r, { fontSize: '14px' });
    expect(document.body.innerHTML).toEqual(
      '<div>T<span style="font-size: 14px;">h<span style="color: red;">is i</span></span><span style="color: red; font-size: 12px;">s a </span>string</div>',
    );
  });

  test('applyStyle on spans complete child text nodes', () => {
    div.innerHTML = '';
    const span = document.createElement('span');
    const text = document.createTextNode('Hello');
    span.appendChild(text);
    div.appendChild(span);
    const r = document.createRange();
    r.setStart(text, 0);
    r.setEnd(text, 5);

    applyStyleToRange(r, { fontSize: '12px' });
    expect(document.body.innerHTML).toEqual(
      '<div><span style="font-size: 12px;">Hello</span></div>',
    );
  });

  test('applyStyle on spans incomplete child text nodes', () => {
    div.innerHTML = '';
    const span = document.createElement('span');
    const text = document.createTextNode('Hello');
    span.appendChild(text);
    div.appendChild(span);
    const r = document.createRange();
    r.setStart(text, 0);
    r.setEnd(text, 4);

    applyStyleToRange(r, { fontSize: '12px' });
    expect(document.body.innerHTML).toEqual(
      '<div><span><span style="font-size: 12px;">Hell</span>o</span></div>',
    );
  });

  test('applyStyle does callback on new span', () => {
    div.innerHTML = '';
    const span = document.createElement('span');
    const text = document.createTextNode('Hello');
    span.appendChild(text);
    div.appendChild(span);
    const r = document.createRange();
    r.setStart(text, 0);
    r.setEnd(text, 4);

    const style: StyleApplierOptions = { fontfamily: 'Lato', onSpanCallback: jest.fn() };

    applyStyleToRange(r, style);
    expect(document.body.innerHTML).toEqual(
      '<div><span><span style="font-family: Lato;">Hell</span>o</span></div>',
    );
    const s = document.body.querySelector('span span');
    expect(style.onSpanCallback).toHaveBeenCalledWith(s);
  });

  test('applyStyle repeated', () => {
    const r = document.createRange();
    r.setStart(text, 2);
    r.setEnd(text, 10);

    applyStyleToRange(r, { backgroundColor: 'red' });
    console.log(r);
    console.log(r.startContainer.textContent);
    console.log(r.endContainer.textContent);
    applyStyleToRange(r, { backgroundColor: 'green' });

    expect(div.innerHTML).toEqual('Th<span style="background-color: green;">is is a </span>string');
  });

  describe('Scenarios', function () {
    test('scenario 1', () => {
      document.body.innerHTML =
        '<p class="loree-content-iframe-element"><a>Insert text here</a></p>';

      const r = document.createRange();
      r.selectNode(document.body.querySelector('a')!);

      applyStyleToRange(r, { backgroundColor: 'red' });
      expect(document.body.innerHTML).toEqual(
        '<p class="loree-content-iframe-element"><a style="background-color: red;">Insert text here</a></p>',
      );
    });
    test('scenario 2', () => {
      document.body.innerHTML =
        '<p class="loree-content-iframe-element">Insert <a>text</a> here</p>';

      const p = document.body.querySelector('p')!;
      const r = document.createRange();
      r.setStart(p.childNodes[0], 0);
      r.setEnd(p.childNodes[2], 5);

      applyStyleToRange(r, { backgroundColor: 'red' });
      expect(document.body.innerHTML).toEqual(
        '<p class="loree-content-iframe-element"><span style="background-color: red;">Insert <a>text</a> here</span></p>',
      );
    });
    test('scenario 3', () => {
      document.body.innerHTML =
        '<p class="loree-content-iframe-element">Insert <a>text</a> here</p>';

      const p = document.body.querySelector('p')!;
      const r = document.createRange();
      r.setStart(p.childNodes[0], 0);
      r.setEnd(p.childNodes[1], 1);

      applyStyleToRange(r, { backgroundColor: 'red' });
      expect(document.body.innerHTML).toEqual(
        '<p class="loree-content-iframe-element"><span style="background-color: red;">Insert <a>text</a></span> here</p>',
      );
    });
    test('scenario 4', () => {
      document.body.innerHTML =
        '<p class="loree-content-iframe-element"><a>Insert text here</a></p>';

      const p = document.body.querySelector('p')!;
      const r = document.createRange();
      r.setStart(p.childNodes[0].childNodes[0], 7);
      r.setEnd(p.childNodes[0].childNodes[0], 11);

      applyStyleToRange(r, { backgroundColor: 'red' });
      expect(document.body.innerHTML).toEqual(
        '<p class="loree-content-iframe-element"><a>Insert <span style="background-color: red;">text</span> here</a></p>',
      );
    });
  });
});
