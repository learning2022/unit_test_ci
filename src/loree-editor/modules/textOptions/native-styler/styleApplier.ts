export interface StyleApplierOptions {
  backgroundColor?: string;
  color?: string;
  fontSize?: string;
  fontfamily?: string;

  onSpanCallback?: (span: HTMLSpanElement) => void;
}

export function applyStyleToElement(element: HTMLElement, style: StyleApplierOptions) {
  addStylesToElement(element, style);
  removeStyleFromChildSpans(element, style);
  removeEmptyStyleChildSpans(element);
}

export function applyStyleToRange(range: Range, style: StyleApplierOptions) {
  if (range.collapsed || !range.startContainer || !range.endContainer) {
    return;
  }

  // Is the selected range really just an existing span?  If so
  // lets just add our styles to that
  const innerNode = getSurroundedNode(range);
  if (innerNode && (isSpanElement(innerNode) || isAnchorElement(innerNode))) {
    const span = innerNode as HTMLSpanElement;
    applyStyleToCompleteRange(span, style);
    return;
  }

  // Nothing between the start and end except text? ie selected text from a
  // text node
  if (range.startContainer === range.endContainer) {
    const parentNode = range.startContainer.parentElement;

    // is the parent of this text node a span?
    if (parentNode && isSpanElement(parentNode)) {
      const parentSpan = parentNode as HTMLSpanElement;

      // Lets create a range that is the whole text node and see if it matches our range
      const nodeRange = range.cloneRange();
      nodeRange.selectNodeContents(range.startContainer);

      if (rangesMatch(nodeRange, range)) {
        applyStyleToCompleteRange(parentSpan, style);
        return;
      }
    }

    // otherwise we have selected a block of text that is only part of a text node
    // wrap it with a span
    applyStyleToSimpleRange(range, style);
    return;
  }

  // So now we have the harder case, ie overlaps

  // create a span
  const span = document.createElement('span');

  // slice out the selected bits and move them into our new span
  // element slicing will be auto managed for us by HTML
  span.appendChild(range.extractContents());

  // now since our range is now empty lets put the new span back into the range
  // this gives the original range content now wrapped in a span
  range.insertNode(span);

  // Add all our styles to our new span
  addStylesToElement(span, style);

  // check that the slicing hasnt made any spans we can now collapse together
  collapseSpans(range);

  // Remove child spans that had style values we set on the parent that should
  // no longer override
  removeAllRedundantChildSpans(span, style);
  if (range.commonAncestorContainer.nodeType === document.ELEMENT_NODE) {
    removeAllEmptyAnchors(range.commonAncestorContainer as HTMLElement);
  }
  span.normalize();
  doCallback(span, style);
}

/**
 * Return true if the node is a SPAN element
 * @param node
 * @returns
 */
function isSpanElement(node: Node): boolean {
  return isElement(node, 'SPAN');
}

/**
 * Return true if the node is a SPAN element
 * @param node
 * @returns
 */
function isAnchorElement(node: Node): boolean {
  return isElement(node, 'A');
}

/**
 * return true if the node is an element of the specified tag
 * @param node
 * @param tagName
 * @returns boolean
 */
function isElement(node: Node, tagName: string): boolean {
  if (node.nodeType === node.ELEMENT_NODE) {
    const nodeElement = node as HTMLElement;
    return nodeElement.tagName === tagName;
  }
  return false;
}

/**
 * If a range surrounds a node and the total contents of the node is
 * equal to the range, return it.
 * Returns null if no such node exists
 * @param range Range
 */
function getSurroundedNode(range: Range): Node | null {
  if (range.startContainer !== range.endContainer) {
    return null;
  }

  const r = range.cloneRange();

  for (let i = 0; i < range.startContainer.childNodes.length; i++) {
    const n = range.startContainer.childNodes[i];

    // create a range on that node so we can do some comparisons
    r.selectNode(n);
    if (rangesMatch(r, range)) {
      return n;
    }
  }

  // get rid of the range
  r.detach();
  return null;
}

function rangesMatch(range1: Range, range2: Range): boolean {
  const compareStart = range1.compareBoundaryPoints(Range.START_TO_START, range2);
  const compareEnd = range1.compareBoundaryPoints(Range.END_TO_END, range2);

  if (compareEnd === 0 && compareStart === 0) {
    return true;
  }

  return false;
}

function applyStyleToSimpleRange(range: Range, style: StyleApplierOptions) {
  const span = document.createElement('span');

  range.surroundContents(span);
  addStylesToElement(span, style);
  doCallback(span, style);
}

function applyStyleToCompleteRange(span: HTMLSpanElement, style: StyleApplierOptions) {
  addStylesToElement(span, style);
  doCallback(span, style);
}

function addStylesToElement(element: HTMLElement, style: StyleApplierOptions) {
  style.backgroundColor && (element.style.backgroundColor = style.backgroundColor);
  style.color && (element.style.color = style.color);
  style.fontSize && (element.style.fontSize = style.fontSize);
  style.fontfamily && (element.style.fontFamily = style.fontfamily);
}

/**
 * Collapse any spans that are just totally wrapped into a single span
 * @param range
 */
function collapseSpans(range: Range) {
  const container = range.commonAncestorContainer;

  container.childNodes.forEach((n) => {
    if (isSpanElement(n) || isAnchorElement(n)) {
      const span = n as HTMLSpanElement;

      // if this span has one child that is a span, then merge it
      if (span.childNodes.length === 1 && isSpanElement(span.childNodes[0])) {
        const childSpan = span.childNodes[0] as HTMLSpanElement;
        copyCssProperties(childSpan, span);
        removeElementMovingChildrenUp(childSpan);
      }
    }
  });
}

/**
 * Go through all the children and remove the styles that have been applied from this span
 * since a user would expect that to happen
 * @param element
 * @param style
 */
function removeAllRedundantChildSpans(element: HTMLElement, style: StyleApplierOptions) {
  // Remove the overridden styles
  let spans = element.querySelectorAll('span');
  spans.forEach((sp) => {
    removeStylesFromElement(sp, style);
  });

  // Now go through and remove any spans that have no style left, since they are now worthless
  spans = element.querySelectorAll('span');
  spans.forEach((sp) => {
    if (isSpanRedundant(sp)) {
      removeElementMovingChildrenUp(sp);
    }
  });
}

function removeAllEmptyAnchors(element: HTMLElement) {
  const anchors = element.querySelectorAll('a');
  anchors.forEach((a) => {
    if (a.childNodes.length === 0) {
      removeElementMovingChildrenUp(a);
    }
  });
}

function isSpanRedundant(span: HTMLSpanElement): boolean {
  if (span.style.length === 0) {
    return true;
  }
  return false;
}

/**
 * Copy CSS style properties from one Element to another
 * @param fromElement
 * @param toElement
 */
function copyCssProperties(fromElement: HTMLElement, toElement: HTMLElement) {
  for (let index = 0; index < fromElement.style.length; index++) {
    const property: string = fromElement.style.item(index);
    toElement.style.setProperty(property, fromElement.style.getPropertyValue(property));
  }
}

function removeElementMovingChildrenUp(element: Element) {
  while (element.childNodes.length) {
    element.before(element.childNodes[0]);
  }
  element.remove();
}

function removeStyleFromChildSpans(element: HTMLElement, style: StyleApplierOptions) {
  element.querySelectorAll('span').forEach((sp) => {
    removeStylesFromElement(sp, style);
  });
}

function removeStylesFromElement(element: HTMLSpanElement, style: StyleApplierOptions) {
  style.backgroundColor && (element.style.backgroundColor = '');
  style.color && (element.style.color = '');
  style.fontSize && (element.style.fontSize = '');
  style.fontfamily && (element.style.fontFamily = '');
}

function removeEmptyStyleChildSpans(element: HTMLElement) {
  for (let ix = 0; ix < element.childNodes.length; ix++) {
    const node = element.childNodes[ix];
    if (isSpanElement(node)) {
      const span = node as HTMLSpanElement;
      if (span.style.length === 0) {
        removeElementMovingChildrenUp(span);
      }
    }
  }
}

function doCallback(span: HTMLSpanElement, style: StyleApplierOptions) {
  if (style.onSpanCallback) {
    style.onSpanCallback(span);
  }
}
