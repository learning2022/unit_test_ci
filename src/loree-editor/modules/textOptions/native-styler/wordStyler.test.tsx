import { WordStyler } from './wordStyler';
import CONSTANTS from '../../../constant';
import {
  fontFamilyInput,
  fontFamilyOutput,
  fontColorInput,
  fontColorOutput,
  borderColorInput,
  borderColorOutput,
  convertedOlList,
  convertedUlList,
  preCaretRnage,
  selection,
  validRangeCountForSelection,
  rangeCountForPartialSelection,
  mockDataToApplyFontColor,
} from './wordStylerMockData';
import * as styleApplier from '../native-styler/styleApplier';
import { createElement } from '../../../common/dom';

const stringToHTML = function (linkToUpdate: string): HTMLElement {
  const parser = new DOMParser();
  const doc = parser.parseFromString(linkToUpdate, 'text/html');
  return doc.body.firstChild as HTMLElement;
};

describe('applying styling like bold and font ,color test while applying to anchor tag', () => {
  let wordStylerInstance: WordStyler;
  beforeEach(() => {
    wordStylerInstance = new WordStyler();
    wordStylerInstance.handleSelectedContentChanges = jest.fn();
  });
  test('updateTripleClickStyle - while appliying font and color in anchor tag', () => {
    const linkToUpdate = "<li><a style='border:1px'>Insert</a></li>";
    const wrapper = stringToHTML(linkToUpdate);
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(wordStylerInstance.handleSelectedContentChanges).toBeCalledTimes(0);
    expect(wrapper.outerHTML).toEqual('<li><a style="border:1px">Insert</a></li>');
  });

  test('updateTripleClickStyle - while appliying font and color in span tag', () => {
    const spycheckupdates = jest.spyOn(wordStylerInstance, 'checkUpdateStyles');
    const linkToUpdate =
      "<li><span style='width:10%;margin:0 0 10px;padding:5px;display'>Insert</span></li>";
    const wrapper = stringToHTML(linkToUpdate);
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(spycheckupdates).toBeCalledTimes(1);
    expect(wordStylerInstance.handleSelectedContentChanges).toBeCalledTimes(0);
    expect(wrapper.outerHTML).toEqual(
      '<li><span style="width:10%;margin:0 0 10px;padding:5px;display">Insert</span></li>',
    );
  });

  test('update font-family for nested span on triple click', () => {
    const wrapper = stringToHTML(fontFamilyInput);
    wordStylerInstance.attribute = 'fontFamily';
    wordStylerInstance.styleValue = 'Times New Roman';
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(wrapper.outerHTML).toEqual(fontFamilyOutput);
  });

  test('update color for nested span on triple click', () => {
    const wrapper = stringToHTML(fontColorInput);
    wordStylerInstance.attribute = 'color';
    wordStylerInstance.styleValue = '#000d9c';
    wordStylerInstance.applyColorType = 'colorPickerSave';
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(wrapper.outerHTML).toEqual(fontColorOutput);
  });

  test('update border for nested span on triple click', () => {
    const wrapper = stringToHTML(borderColorInput);
    wordStylerInstance.attribute = 'borderColor';
    wordStylerInstance.styleValue = '#ffffff';
    wordStylerInstance.applyColorType = 'colorPickerSave';
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(wrapper.outerHTML).toEqual(borderColorOutput);
  });

  test('while appliying font and color in anchor tag', () => {
    const spy = jest.spyOn(wordStylerInstance, 'checkUpdateStyles');
    const linkToUpdate = "<li><a  style='border:1px'>Insert</a></li>";
    const wrapper = stringToHTML(linkToUpdate);
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(spy).toBeCalledTimes(1);
    expect(wrapper.outerHTML).toEqual('<li><a style="border:1px">Insert</a></li>');
  });

  test('while appliying font and color in OL tag', () => {
    const spy = jest.spyOn(wordStylerInstance, 'checkUpdateStyles');
    const linkToUpdate =
      "<li><OL  style='width:10%;margin:0 0 10px;padding:5px;display'>Insert</OL></li>";
    const wrapper = stringToHTML(linkToUpdate);
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(spy).toBeCalledTimes(0);
    expect(wrapper.outerHTML).toEqual(
      '<li><ol style="width:10%;margin:0 0 10px;padding:5px;display">Insert</ol></li>',
    );
  });

  test('while appliying font and color in UL tag', () => {
    const spy = jest.spyOn(wordStylerInstance, 'checkUpdateStyles');
    const linkToUpdate = "<li><ul  style='border:1px'>Insert</ul></li>";
    const wrapper = stringToHTML(linkToUpdate);
    wordStylerInstance.updateTripleClickStyle(wrapper);
    expect(spy).toBeCalledTimes(0);
    expect(wrapper.outerHTML).toEqual('<li><ul style="border:1px">Insert</ul></li>');
  });
});

describe('#listConversions', () => {
  let mockOl: HTMLElement;
  let selectedLi: HTMLElement;
  let mockedOutputMultiList: string;
  let mockedOutputSingleList: string;
  const wordStylerInstance = new WordStyler();
  beforeAll(() => {
    mockOl = document.createElement('ol');
    mockOl.className = 'loree-iframe-content-element';
    selectedLi = document.createElement('li');
    selectedLi.className = 'loree-iframe-content-element element-highlight';
    selectedLi.innerText = 'Insert text here';
    mockOl.appendChild(selectedLi);
    mockedOutputMultiList =
      '<p class="loree-iframe-content-element" style=""></p><ol class="loree-iframe-content-element" style=""><li class="loree-iframe-content-element">Insert text here</li><li class="loree-iframe-content-element">Insert text here</li></ol>';
    mockedOutputSingleList =
      '<p class="loree-iframe-content-element" style="">Insert text here</p>';
  });

  test('single list to paragraph', () => {
    const conversion = wordStylerInstance.listItemToParagraph(mockOl, selectedLi);
    expect(conversion).toEqual(mockedOutputSingleList);
  });

  test('multi list to paragraph', () => {
    wordStylerInstance.checkSelectedNode = jest.fn().mockImplementationOnce(() => true);
    mockOl.innerHTML +=
      '<li class="loree-iframe-content-element">Insert text here</li><li class="loree-iframe-content-element">Insert text here</li>';
    const conversion = wordStylerInstance.listItemToParagraph(mockOl, selectedLi);
    expect(conversion).toEqual(mockedOutputMultiList);
  });

  test('if no selected list item', () => {
    wordStylerInstance.checkSelectedNode = jest.fn().mockImplementation(() => false);
    mockOl.innerHTML +=
      '<li class="loree-iframe-content-element">Insert text here</li><li class="loree-iframe-content-element">Insert text here</li>';
    const conversion = wordStylerInstance.listItemToParagraph(mockOl, selectedLi);
    expect(conversion).not.toEqual(mockedOutputMultiList);
  });
});

const spanNodeDom = function () {
  const span = document.createElement('span');
  span.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
  span.innerHTML = 'Here';
  return span;
};

describe('#TextTransform', () => {
  let paragraphElement: HTMLParagraphElement;
  let fragment: DocumentFragment;
  let spanElement: HTMLSpanElement;
  const wordStylerInstance = new WordStyler();
  function paragraphNodeDom() {
    const paragraph = document.createElement('p');
    paragraph.className = CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT;
    paragraph.innerHTML = 'Insert Text Here';
    return paragraph;
  }
  function fragmentDom() {
    const fragment = document.createDocumentFragment();
    fragment.append('Here');
    return fragment;
  }
  beforeAll(() => {
    paragraphElement = paragraphNodeDom();
    fragment = fragmentDom();
    spanElement = spanNodeDom();
    wordStylerInstance.applyTextTransform = true;
  });
  const textTransformValues = ['uppercase', 'lowercase', 'capitalize'];
  textTransformValues.forEach((textTransformValue) => {
    describe(`while applying ${textTransformValue} css property and classname`, () => {
      test(`#check ${textTransformValue} css property and classname is added`, () => {
        wordStylerInstance.textTransform = `${textTransformValue}`;
        wordStylerInstance.applyTextTransformStyle(null, paragraphElement, fragment, spanElement);
        expect(spanElement).toHaveStyle(`text-transform: ${textTransformValue}`);
        expect(spanElement).toHaveClass(
          `${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} text-${textTransformValue}`,
        );
      });
    });
  });
});

describe('#while updating text transform classname to existing text', () => {
  let spanElement: HTMLSpanElement | null;
  const wordStylerInstance = new WordStyler();
  beforeEach(() => {
    spanElement = spanNodeDom();
  });
  afterEach(() => {
    spanElement = null;
  });
  test('#check classname from lowercase to uppercase is updated', () => {
    wordStylerInstance.styleValue = 'uppercase';
    (spanElement as HTMLSpanElement).classList.add('text-lowercase');
    wordStylerInstance.updateAttributeValue(spanElement as HTMLSpanElement);
    expect(spanElement).toHaveClass(`${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} text-uppercase`);
  });

  test('#check classname from uppercase to lowercase is updated', () => {
    wordStylerInstance.styleValue = 'lowercase';
    (spanElement as HTMLSpanElement).classList.add('text-uppercase');
    wordStylerInstance.updateAttributeValue(spanElement as HTMLSpanElement);
    expect(spanElement).toHaveClass(`${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} text-lowercase`);
  });

  test('#check classname from lowercase to capitalize is updated', () => {
    wordStylerInstance.styleValue = 'capitalize';
    (spanElement as HTMLSpanElement).classList.add('text-lowercase');
    wordStylerInstance.updateAttributeValue(spanElement as HTMLSpanElement);
    expect(spanElement).toHaveClass(`${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} text-capitalize`);
  });

  test('#when revert check applied classnames of text transform are removed', () => {
    wordStylerInstance.styleValue = 'revert';
    (spanElement as HTMLSpanElement).classList.add('text-lowercase');
    wordStylerInstance.updateAttributeValue(spanElement as HTMLSpanElement);
    expect(spanElement).toHaveClass(`${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT}`);
  });
});

describe('#while updating font weight classname to existing text', () => {
  let spanElement: HTMLSpanElement | null;
  const wordStylerInstance = new WordStyler();
  beforeEach(() => {
    spanElement = spanNodeDom();
  });
  afterEach(() => {
    spanElement = null;
  });
  test('#check classname from normalStyle to bStyle is updated', () => {
    wordStylerInstance.styleValue = 'bold';
    (spanElement as HTMLSpanElement).classList.add('normalStyle');
    wordStylerInstance.updateAttributeValue(spanElement as HTMLSpanElement);
    expect(spanElement).toHaveClass(`${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} bStyle`);
  });

  test('#check classname from bStyle to normalStyle is updated', () => {
    wordStylerInstance.styleValue = 'normal';
    (spanElement as HTMLSpanElement).classList.add('bStyle');
    wordStylerInstance.updateAttributeValue(spanElement as HTMLSpanElement);
    expect(spanElement).toHaveClass(`${CONSTANTS.LOREE_IFRAME_CONTENT_ELEMENT} normalStyle`);
  });
});

describe('#check style is applied for same element or not (upto "DIV")', () => {
  let textNode: Text;
  let style: string;
  let spanElement: HTMLSpanElement;
  const wordStylerInstance = new WordStyler();
  function createTextNode() {
    const textNode = document.createTextNode('Hello World');
    return textNode;
  }
  beforeAll(() => {
    spanElement = spanNodeDom();
  });

  test('#check span tag when text node is selected', () => {
    style = 'SPAN';
    textNode = createTextNode();
    // @ts-expect-error
    expect(wordStylerInstance.applyOrRemoveStyle(textNode, style)).toBeFalsy();
  });

  test('#check span tag when SPAN is selected', () => {
    style = 'SPAN';
    expect(wordStylerInstance.applyOrRemoveStyle(spanElement, style)).toBeTruthy();
  });

  test('#check span tag when P is selected', () => {
    style = 'P';
    expect(wordStylerInstance.applyOrRemoveStyle(spanElement, style)).toBeFalsy();
  });

  test('#check div tag when SPAN is selected', () => {
    style = 'SPAN';
    const divElement = document.createElement('DIV');
    expect(wordStylerInstance.applyOrRemoveStyle(divElement, style)).toBeFalsy();
  });

  test('#check body tag when SPAN is selected', () => {
    style = 'SPAN';
    const body = document.createElement('BODY');
    expect(wordStylerInstance.applyOrRemoveStyle(body, style)).toBeFalsy();
  });
});

describe('#styleAction', () => {
  let wordStylerInstance: WordStyler;
  beforeEach(() => {
    wordStylerInstance = new WordStyler();
    wordStylerInstance.updateStyleToChildElement = jest.fn();
  });
  test('the removeEmptyElements function', () => {
    const linkToUpdate = '<p> <span> loree </span>  <span></span></p>';
    const element = stringToHTML(linkToUpdate);
    wordStylerInstance.removeEmptyElements(element);
    expect(element.outerHTML).toEqual('<p> <span> loree </span>  </p>');
  });
  test('updateStyleToChildElement calledon each style update', () => {
    wordStylerInstance.updateExitingStyle(spanNodeDom());
    expect(wordStylerInstance.updateStyleToChildElement).toBeCalledTimes(1);
  });
});

describe('#setAlignment for Migrated List Items', () => {
  let wordStylerInstance: WordStyler;
  const alignment = [['left'], ['right'], ['center'], ['justify']];
  const selection = {
    anchorNode: {
      nodeName: '#text',
      nextSibling: null,
      firstChild: null,
      parentElement: {
        className: 'loree-iframe-content-element element-highlight',
        innerHTML: 'Insert text here',
        tagName: 'P',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
        },
      } as HTMLElement,
      getAttribute: {},
    } as HTMLElement,
    type: 'Range',
  } as unknown as Selection;
  const listSelection = {
    anchorNode: {
      nodeName: '#text',
      nextSibling: null,
      firstChild: null,
      parentElement: {
        className: 'loree-iframe-content-element element-highlight',
        tagName: 'UL',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
        },
      } as HTMLElement,
      getAttribute: {},
    } as HTMLElement,
    type: 'Range',
  } as unknown as Selection;
  beforeEach(() => {
    jest.clearAllMocks();
    wordStylerInstance = new WordStyler();
    wordStylerInstance.handleSelectedContentChanges = jest.fn();
    wordStylerInstance.applyStyleToListElement = jest.fn();
  });

  test.each(alignment)('alignment is not applied when the content is not selected', (align) => {
    wordStylerInstance.textAlignment(align);
    expect(wordStylerInstance.handleSelectedContentChanges).toHaveBeenCalledTimes(0);
    expect(wordStylerInstance.applyStyleToListElement).toHaveBeenCalledTimes(0);
  });

  test.each(alignment)('apply alignment when the p tag is selected', (align) => {
    wordStylerInstance.getSelectedContent = jest.fn().mockImplementation(() => selection);
    wordStylerInstance.textAlignment(align);
    expect(wordStylerInstance.handleSelectedContentChanges).toHaveBeenCalledTimes(1);
    expect(wordStylerInstance.applyStyleToListElement).toHaveBeenCalledTimes(1);
    expect(selection.anchorNode?.parentElement?.style.textAlign).toBe(align);
  });

  test.each(alignment)('apply alignment when list is selected', (align) => {
    wordStylerInstance.getSelectedContent = jest.fn().mockImplementation(() => listSelection);
    wordStylerInstance.textAlignment(align);
    expect(wordStylerInstance.applyStyleToListElement).toHaveBeenCalledTimes(1);
    expect(wordStylerInstance.handleSelectedContentChanges).toHaveBeenCalledTimes(1);
    expect(listSelection.anchorNode?.parentElement?.style.textAlign).toBe(align);
  });
});

describe('#setAlignmentForMigratedList', () => {
  const listItems = `<ol class="loree-iframe-content-element" style=" margin: 0px; padding: 5px; text-align: left; font-family: Helvetica; color: #000000; font-size: 16px;" contenteditable="true"><li data-highlightable="1" class="loree-iframe-content-element element-highlight" style=" padding: 10px; margin: 0px 0px 0px 30px; font-family: Arial Black, Gadget, sans-serif; font-size: 20px; text-align: center;">Insert Text Here</li><li data-highlightable="1" class="loree-iframe-content-element" style=" padding: 10px; margin: 0px 0px 0px 30px; font-family: Arial Black, Gadget, sans-serif; font-size: 20px; text-align: center;">Insert Text Here</li><li data-highlightable="1" class="gjs-hovered loree-iframe-content-element" style=" padding: 10px; margin: 0px 0px 0px 30px; font-family: Arial Black, Gadget, sans-serif; font-size: 20px; text-align: center; color: #db0909;">Insert Text Here</li><li data-highlightable="1" class="loree-iframe-content-element" style=" padding: 10px; margin: 0px 0px 0px 30px; font-family: Arial Black, Gadget, sans-serif; font-size: 20px; text-align: center;">Insert Text Here</li></ol>`;
  const listElement: HTMLElement = stringToHTML(listItems);
  const migratedList = [
    [listElement, 'right'],
    [listElement, 'left'],
    [listElement, 'center'],
    [listElement, 'justify'],
  ] as const;
  let wordStylerInstance: WordStyler;

  beforeEach(() => {
    wordStylerInstance = new WordStyler();
  });
  test.each(migratedList)(`apply alignment to listItems`, (list, align) => {
    wordStylerInstance.applyStyleToListElement(list, align as string);
    expect((list.firstChild as HTMLElement)?.style.textAlign).toBe(align);
    expect((list.lastChild as HTMLElement)?.style.textAlign).toBe(align);
  });
});

describe('#Paragraph to list conversion', () => {
  let wordStylerInstance: WordStyler;
  const ListItems = [
    ['UL', convertedUlList],
    ['OL', convertedOlList],
  ];
  beforeEach(() => {
    wordStylerInstance = new WordStyler();
    (selection.anchorNode as HTMLElement).innerHTML = 'Insert text here';
  });

  test.each(ListItems)(
    'when converting a paragraph to list the style should be applied in the li tags',
    (type, outerHTML) => {
      wordStylerInstance.paragraphToList(selection, preCaretRnage, type);
      expect((preCaretRnage.commonAncestorContainer as HTMLElement).outerHTML).toBe(outerHTML);
    },
  );
});

describe('#Range Selection', () => {
  let wordStylerInstance: WordStyler;
  const selectionRanges = [
    [selection, false],
    [rangeCountForPartialSelection, false],
    [validRangeCountForSelection, true],
  ];
  beforeEach(() => {
    wordStylerInstance = new WordStyler();
  });
  test.each(selectionRanges)(
    'should return the boolean value based on the selected Content',
    (selection, isSelected) => {
      const hasSelectionRange = wordStylerInstance.partialOrCompleteSelction(
        selection as Selection,
      );
      expect(hasSelectionRange).toBe(isSelected);
    },
  );
});

describe('#StyleAction', () => {
  let wordStylerInstance: WordStyler;
  beforeEach(() => {
    jest.clearAllMocks();
    wordStylerInstance = new WordStyler();
  });

  test('should not do any action when the selection is invalid', () => {
    const mockgetMajorStyleFunction = jest.spyOn(wordStylerInstance, 'getMajorStyleSlection');
    wordStylerInstance.styleAction('SPAN');
    expect(mockgetMajorStyleFunction).not.toBeCalled();
  });

  test('should apply the style when the selection is valid', () => {
    wordStylerInstance.updateStyleToChildElement = jest.fn();
    wordStylerInstance.getSelectedContent = jest.fn().mockImplementation(() => {
      return rangeCountForPartialSelection;
    });
    wordStylerInstance.styleAction('SPAN');
    expect(wordStylerInstance.updateStyleToChildElement).toBeCalledTimes(1);
  });
});

describe('#StyleAction in applying fontColor', () => {
  let wordStylerInstance: WordStyler;
  beforeEach(() => {
    jest.clearAllMocks();
    wordStylerInstance = new WordStyler();
  });

  test.each(mockDataToApplyFontColor)(
    'should apply font color to the selected text',
    ({ selection, content }) => {
      wordStylerInstance.getSelectedContent = jest.fn().mockImplementation(() => {
        return selection;
      });
      const applyFontcolor = jest.spyOn(styleApplier, 'applyStyleToElement');
      wordStylerInstance.fontColorStyle('colorPickerChange', '#741717');
      expect(applyFontcolor).toBeCalledWith(content, {
        color: '#741717',
        onSpanCallback: wordStylerInstance.attachAttributesToSpan,
      });
      expect((content as HTMLElement)?.style?.color).toBe('#741717');
    },
  );

  test('should add loree classes to the created span', () => {
    const spanElement = createElement('span');
    const elementWithAddedClasses = wordStylerInstance.attachAttributesToSpan(spanElement);
    expect(elementWithAddedClasses.outerHTML).toBe(
      '<span class="loree-iframe-content-element"></span>',
    );
  });
});

describe('#langAttribOnListConversion', () => {
  const listSelection = {
    anchorNode: {
      nodeName: 'li',
      nextSibling: null,
      firstChild: null,
      parentElement: {
        className: 'loree-iframe-content-element element-highlight',
        children: [
          {
            nextSibling: null,
            firstChild: null,
            className: 'loree-iframe-content-element element-highlight',
            innerHTML: 'Insert text here',
            innerText: 'Insert text here',
            tagName: 'LI',
          },
        ],
        lang: 'nl',
        tagName: 'UL',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
        },
        hasAttribute: jest.fn().mockImplementation(() => true),
      } as unknown as HTMLElement,
      getAttribute: {},
      style: {
        color: '#000000',
      },
    } as HTMLElement,
    type: 'Range',
  } as unknown as Selection;
  let wordStylerInstance: WordStyler;
  beforeEach(() => {
    wordStylerInstance = new WordStyler();
  });
  test('single list to paragraph', () => {
    const conversion = wordStylerInstance.listItemToParagraph(
      listSelection.anchorNode?.parentElement as HTMLElement,
      listSelection.anchorNode as HTMLElement,
    );
    expect(conversion).toEqual(
      '<p class="loree-iframe-content-element element-highlight" style="" lang="nl">Insert text here</p>',
    );
  });
});

describe('#Convert List to paragraph withour addding padding', () => {
  const listSelection = {
    anchorNode: {
      nodeName: 'li',
      nextSibling: null,
      firstChild: null,
      parentElement: {
        className: 'loree-iframe-content-element element-highlight',
        children: [
          {
            nextSibling: null,
            firstChild: null,
            className: 'loree-iframe-content-element element-highlight',
            innerHTML: 'Insert text here',
            innerText: 'Insert text here',
            tagName: 'LI',
          },
        ],
        lang: 'nl',
        tagName: 'UL',
        style: {
          fontWeight: 'bold',
          fontStyle: 'italic',
          padding: '40px',
        },
        hasAttribute: jest.fn().mockImplementation(() => true),
      } as unknown as HTMLElement,
      getAttribute: {},
      style: {
        color: '#000000',
      },
      hasAttribute: jest.fn().mockImplementation(() => true),
    } as unknown as HTMLElement,
    type: 'Range',
  } as unknown as Selection;

  let wordStylerInstance: WordStyler;
  beforeEach(() => {
    wordStylerInstance = new WordStyler();
  });
  test('check the padding of returned element once unlisting paragraph element', () => {
    const conversionSpy = jest.spyOn(wordStylerInstance, 'listItemToParagraph');
    wordStylerInstance.isMultipleListItemSelected = jest.fn().mockImplementation(() => false);
    wordStylerInstance.listToParagraph(listSelection, listSelection.anchorNode as HTMLElement);
    expect(conversionSpy).toHaveLastReturnedWith(
      '<p class="loree-iframe-content-element element-highlight" style="" lang="nl">Insert text here</p>',
    );
  });
});
