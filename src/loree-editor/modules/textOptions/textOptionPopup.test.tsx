import Base from '../../base';
import CONSTANTS from '../../constant';
import TextOptions from './UI/textOptionsUI';
import { WordStyler } from './native-styler/wordStyler';
import { featuresList } from './baseFeatureListMockData';
import {
  createElement,
  createDiv,
  createParagraph,
  getNElementByClassName,
} from '../../common/dom';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Text option popup', () => {
  const baseInstance = new Base();

  let textOptionWrapper: HTMLElement;
  const textOptionsInstance = new TextOptions();
  let wordStylerInstance: WordStyler;

  beforeEach(() => {
    wordStylerInstance = new WordStyler();
    wordStylerInstance.updateStyleToChildElement = jest.fn();
    document.body.innerHTML = '';
    const iframeElement = createElement('iframe', CONSTANTS.LOREE_IFRAME);
    const popupSection = createDiv(CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER);
    iframeElement.append(popupSection);
    document.body.appendChild(iframeElement);

    textOptionsInstance.appendParagraphOption(
      document.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER) as HTMLElement,
    );

    textOptionWrapper = createDiv(CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER);
    textOptionWrapper.style.visibility = 'hidden';
    document.body.append(textOptionWrapper);
    baseInstance?.getDocument()?.body.appendChild(textOptionWrapper);

    const parentSapmpleDiv = createDiv('', CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS);
    const sampleDiv = createParagraph('dummyContent', 'dummyContent');
    sampleDiv.innerHTML = `This is a <span style="font-family:'dancingMirum'; font-size: 16px;"> inner child 1</span> content`;
    parentSapmpleDiv.append(sampleDiv);
    document.body.append(parentSapmpleDiv);

    baseInstance.updateFeaturesList(featuresList);

    const range = document.createRange();
    range.setStart(getNElementByClassName('dummyContent', 0), 0);
    range.setEnd(getNElementByClassName('dummyContent', 0), 1);
    document.getSelection()?.addRange(range);

    baseInstance.getSelectedText = jest.fn().mockImplementation(() => document.getSelection());
    const getDocument = baseInstance?.getDocument();
    if (getDocument) {
      getDocument.getSelection = jest.fn().mockImplementation(() => document.getSelection());
    }
  });

  test('Restricting access to the same style', () => {
    const selectedElement = document.getElementById('dummyContent');
    baseInstance.changeSelectedElement(selectedElement);
    const textOption = baseInstance
      ?.getDocument()
      ?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER) as HTMLElement;
    baseInstance.getSelectedDetails(document.getSelection() as Selection, textOption);
    const stylePElement = baseInstance
      .getDocument()
      ?.getElementById(`${CONSTANTS.LOREE_TEXT_OPTIONS_NORMAL_PARAGRAPH_BUTTON}`) as HTMLElement;
    expect(stylePElement).toHaveProperty('disabled', true);
  });

  test('Change font properties to the child elements while changing the header', () => {
    wordStylerInstance.selectedFontFamily = 'Arial';
    wordStylerInstance.selectedFontSize = '20px';
    const selectedElement = getNElementByClassName('dummyContent', 0);
    wordStylerInstance.replaceWithParagraph(selectedElement, 'SPAN');
    expect(getNElementByClassName('dummyContent', 0)?.style.fontFamily).toBe(
      wordStylerInstance.selectedFontFamily,
    );
    expect(getNElementByClassName('dummyContent', 0)?.style.fontSize).toBe(
      wordStylerInstance.selectedFontSize,
    );
  });
});
