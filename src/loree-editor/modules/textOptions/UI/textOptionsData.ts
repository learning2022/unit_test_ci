import CONSTANTS from '../../../constant';
import { HeaderListType } from '../../../interface';
import { textOptionIcon } from './icons';
import { translate } from '../../../../i18n/translate';

export const TextOptions = {
  Style: translate('textoption.style'),
  Bold: translate('textoption.bold'),
  Italic: translate('textoption.italic'),
  LineHeight: translate('textoption.lineheight'),
  TextColor: translate('textoption.textcolor'),
  BackgroundColor: translate('textoption.backgroundcolor'),
  BorderColor: translate('textoption.bordercolor'),
  UnorderedList: translate('textoption.unorderedlist'),
  OrderedList: translate('textoption.orderedlist'),
  WordOptions: translate('textoption.wordoptions'),
  Alignment: translate('textoption.alignment'),
  InsertLink: translate('textoption.insertlink'),
  ClearFormat: translate('textoption.clearformat'),
};

export enum TextOptionsTranslationKeys {
  Style = 'textoption.style',
  Bold = 'textoption.bold',
  Italic = 'textoption.italic',
  LineHeight = 'textoption.lineheight',
  TextColor = 'textoption.textcolor',
  BackgroundColor = 'textoption.backgroundcolor',
  BorderColor = 'textoption.bordercolor',
  UnorderedList = 'textoption.unorderedlist',
  OrderedList = 'textoption.orderedlist',
  WordOptions = 'textoption.wordoptions',
  Alignment = 'textoption.alignment',
  InsertLink = 'textoption.insertlink',
  ClearFormat = 'textoption.clearformat',
}

export const paragraphOptions = [
  { label: '<h1>Header 1</h1>', value: 'H1', id: CONSTANTS.LOREE_TEXT_OPTIONS_HEADER1_BUTTON },
  { label: '<h2>Header 2</h2>', value: 'H2', id: CONSTANTS.LOREE_TEXT_OPTIONS_HEADER2_BUTTON },
  { label: '<h3>Header 3</h3>', value: 'H3', id: CONSTANTS.LOREE_TEXT_OPTIONS_HEADER3_BUTTON },
  { label: '<h4>Header 4</h4>', value: 'H4', id: CONSTANTS.LOREE_TEXT_OPTIONS_HEADER4_BUTTON },
  { label: '<h5>Header 5</h5>', value: 'H5', id: CONSTANTS.LOREE_TEXT_OPTIONS_HEADER5_BUTTON },
  { label: '<h6>Header 6</h6>', value: 'H6', id: CONSTANTS.LOREE_TEXT_OPTIONS_HEADER6_BUTTON },
  {
    label: '<p>Normal Paragraph Text</p>',
    value: 'P',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_NORMAL_PARAGRAPH_BUTTON,
  },
];

export const textTransformOptions = [
  {
    label: textOptionIcon.upperCaseTextIcon,
    value: 'uppercase',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_UPPERCASE_BUTTON,
  },
  {
    label: textOptionIcon.lowerCaseTextIcon,
    value: 'lowercase',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_LOWERCASE_BUTTON,
  },
  {
    label: textOptionIcon.titleCaseTextIcon,
    value: 'capitalize',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_TITLECASE_BUTTON,
  },
];

export const textDecorationOptions = [
  {
    label: textOptionIcon.underlineTextIcon,
    value: 'underline',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_UNDERLINE_BUTTON,
  },
  {
    label: textOptionIcon.strikeThroughTextIcon,
    value: 'line-through',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_STRIKETHROUGH_BUTTON,
  },
];

export const textVerticalAlignOptions = [
  {
    label: textOptionIcon.subScriptCaseTextIcon,
    value: 'sub',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_SUBSCRIPT_BUTTON,
  },
  {
    label: textOptionIcon.superScriptCaseTextIcon,
    value: 'super',
    id: CONSTANTS.LOREE_TEXT_OPTIONS_SUPERSCRIPT_BUTTON,
  },
];

export const alignmentOptions = [
  {
    label: textOptionIcon.leftAlignTextIcon,
    value: 'left',
    id: CONSTANTS.LOREE_TEXT_ALIGN_LEFT_BUTTON,
  },
  {
    label: textOptionIcon.centerAlignTextIcon,
    value: 'center',
    id: CONSTANTS.LOREE_TEXT_ALIGN_CENTER_BUTTON,
  },
  {
    label: textOptionIcon.rightAlignTextIcon,
    value: 'right',
    id: CONSTANTS.LOREE_TEXT_ALIGN_RIGHT_BUTTON,
  },
  {
    label: textOptionIcon.justifyAlignTextIcon,
    value: 'justify',
    id: CONSTANTS.LOREE_TEXT_ALIGN_JUSTIFY_BUTTON,
  },
];

export const BorderStyleOptions = [
  { label: 'Dashed' },
  { label: 'Dotted' },
  { label: 'Double' },
  { label: 'None' },
  { label: 'Solid' },
];

export interface CustomStyle {
  customColor: { colors: { color: string }[] };
  customFonts: { fonts: { fontFamily: string; name: string; url: string; selected: boolean }[] };
  customHeaderStyle: {
    customHeader: CustomHeader;
  };

  customLinkStyle: { customLink: [{ [x: string]: string; color: string; font: string }] };
  features: { fontstyles: boolean };
}

export type CustomHeader = [
  { h1: { size: string; font: string } },
  { h2: { size: string; font: string } },
  { h3: { size: string; font: string } },
  { h4: { size: string; font: string } },
  { h5: { size: string; font: string } },
  { h6: { size: string; font: string } },
  { paragraph: { size: string; font: string } },
];

export const customFont: HeaderListType[] = [];
export const customColors: string[] = [];

export interface TextOptionsButtonAttribute {
  innerHTML?: string;
  id?: string;
  className?: string;
}

export const getTextOptionsButtonAttributes = (type: string): TextOptionsButtonAttribute => {
  switch (type) {
    case TextOptionsTranslationKeys.Style:
    case TextOptions.Style:
      return {
        innerHTML: textOptionIcon.textStyles,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON,
      };
    case TextOptionsTranslationKeys.Bold:
    case TextOptions.Bold:
      return {
        innerHTML: textOptionIcon.boldTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_BOLD_BUTTON,
      };
    case TextOptionsTranslationKeys.Italic:
    case TextOptions.Italic:
      return {
        innerHTML: textOptionIcon.italicTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_ITALIC_BUTTON,
      };
    case TextOptionsTranslationKeys.LineHeight:
    case TextOptions.LineHeight:
      return {
        innerHTML: textOptionIcon.lineSpacingIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_BUTTON,
      };
    case TextOptionsTranslationKeys.TextColor:
    case TextOptions.TextColor:
      return {
        innerHTML: textOptionIcon.foregroundTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_BUTTON,
      };
    case TextOptionsTranslationKeys.BackgroundColor:
    case TextOptions.BackgroundColor:
      return {
        innerHTML: textOptionIcon.backgroundTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_BUTTON,
        className: ' backgroundColorSvgTextIcon',
      };
    case TextOptionsTranslationKeys.BorderColor:
    case TextOptions.BorderColor:
      return {
        innerHTML: 'B',
        id: CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_BUTTON,
        className: ' borderColorSvgTextIcon',
      };
    case TextOptionsTranslationKeys.UnorderedList:
    case TextOptions.UnorderedList:
      return {
        innerHTML: textOptionIcon.unorderedTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_UNORDER_LIST_BUTTON,
      };
    case TextOptionsTranslationKeys.OrderedList:
    case TextOptions.OrderedList:
      return {
        innerHTML: textOptionIcon.orderedTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON,
      };
    case TextOptionsTranslationKeys.WordOptions:
    case TextOptions.WordOptions:
      return {
        innerHTML: textOptionIcon.titleCaseTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_WORD_BUTTON,
      };
    case TextOptionsTranslationKeys.Alignment:
    case TextOptions.Alignment:
      return {
        innerHTML: textOptionIcon.justifyAlignTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_BUTTON,
      };
    case TextOptionsTranslationKeys.InsertLink:
    case TextOptions.InsertLink:
      return {
        innerHTML: textOptionIcon.linkTextIcon,
        id: CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON,
      };
    case TextOptionsTranslationKeys.ClearFormat:
    case TextOptions.ClearFormat:
      return {
        innerHTML: textOptionIcon.clearTextOptionIcon,
        id: '',
      };
    default:
      return {};
  }
};
