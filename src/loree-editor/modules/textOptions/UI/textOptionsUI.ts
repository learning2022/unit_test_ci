import Base from '../../../base';
import { WordStyler } from '../native-styler/wordStyler';
import CONSTANTS from '../../../constant';
import { textOptionIcon } from './icons';
import { LmsAccess } from '../../../../utils/lmsAccess';
import {
  paragraphOptions,
  BorderStyleOptions,
  textTransformOptions,
  textDecorationOptions,
  textVerticalAlignOptions,
  alignmentOptions,
  customColors,
  CustomStyle,
  customFont,
  getTextOptionsButtonAttributes,
  CustomHeader,
} from './textOptionsData';
import {
  handleDropdownValue,
  appendBorderWidthInput,
  appendNewTabCheckbox,
  appendCustomHexInput,
} from '../native-styler/textOptionsEvents';
import { stylerEventHandlerFactory } from '../styleEventHandler';
import {
  checkFontFamily,
  getIframedocumentElementById,
  getIframedocumentElementsByClassName,
} from '../../../utils';
import { translate } from '../../../../i18n/translate';
import { ensure } from '../../../../utils/generalUtils';

let customHeader: CustomHeader;
let customLink: { [x: string]: string; color: string; font: string } | '';

class TextOptions extends Base {
  wordStylerObj?: WordStyler;
  iframeDocument?: HTMLDocument;
  initiate = (config: CustomStyle): void => {
    const iframeDocument = this.getDocument();
    const featuresList = config.features;
    if (iframeDocument && (featuresList.fontstyles || featuresList.fontstyles !== undefined)) {
      this.iframeDocument = iframeDocument;
      const textOptionsWrapper = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER,
      );
      if (textOptionsWrapper) {
        this.appendCustomStyles(config);
        this.appendParagraphOption(textOptionsWrapper);
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.bold'));
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.italic'));
        this.appendFontFamilyOption(textOptionsWrapper);
        this.appendFontSizeOption(textOptionsWrapper);
        this.appendSpacingOption(textOptionsWrapper);
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.textcolor'));
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.backgroundcolor'));
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.bordercolor'));
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.unorderedlist'));
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.orderedlist'));
        this.appendWordOption(textOptionsWrapper);
        this.appendAlignmentOption(textOptionsWrapper);
        this.appendLinkOption(textOptionsWrapper);
        this.appendTextOptionsButton(textOptionsWrapper, translate('textoption.clearformat'));
        this.appendArrow(textOptionsWrapper);
        this.appendAnchorArrow();
        this.handleIframeMessageEvent();
        this.wordStylerObj = new WordStyler();
      }
    }
  };

  appendCustomStyles = (config: CustomStyle) => {
    const colors = config.customColor.colors;
    if (colors.length) {
      colors.forEach((clr: { color: string }) => {
        customColors.push(clr.color);
      });
    }
    const fonts = config.customFonts.fonts;
    for (const font of fonts) {
      customFont.push(font);
    }
    customFont.sort((a: { name: string }, b: { name: string }) => a.name.localeCompare(b.name));
    customHeader = config.customHeaderStyle.customHeader;
    customLink = config.customLinkStyle.customLink ? config.customLinkStyle.customLink[0] : '';
  };

  appendTextOptionsButton = (wrapper: HTMLElement, type: string): void => {
    const button = document.createElement('button');

    button.className = `text-option-button editor-tooltip${
      getTextOptionsButtonAttributes(type)?.className
        ? getTextOptionsButtonAttributes(type || '').className
        : ''
    }`;

    button.innerHTML = ensure(getTextOptionsButtonAttributes(type).innerHTML);
    button.id = ensure(getTextOptionsButtonAttributes(type)?.id);
    button.setAttribute('data-toggle', 'tooltip');
    button.setAttribute('title', type);
    button.setAttribute('data-placement', 'bottom');
    button.onclick = () => stylerEventHandlerFactory(type, button, customLink, customHeader);
    wrapper.appendChild(button);
  };

  appendParagraphOption = (wrapper: HTMLElement): void => {
    checkFontFamily(customHeader, customFont, 'editor');
    this.appendTextOptionsButton(wrapper, translate('textoption.style'));
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const paragraphWrapper = document.createElement('div');
    paragraphWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER;
    paragraphWrapper.className = 'text-options-paragraph-wrapper';
    paragraphWrapper.style.display = 'none';
    paragraphOptions.forEach((option) => {
      const button = document.createElement('button');
      button.innerHTML = option.label;
      button.id = option.id;
      button.onclick = (): void => this.paragraphFeature(option.value, customHeader);
      paragraphWrapper.appendChild(button);
    });
    iframeDocument.body.appendChild(paragraphWrapper);
  };

  appendFontFamilyOption = (wrapper: HTMLElement): void => {
    const select = document.createElement('select');
    select.className = 'text-option-font-family';
    const option = document.createElement('option');
    option.style.display = 'none';
    select.appendChild(option);
    customFont.forEach((font: { name: string }) => {
      const option = document.createElement('option');
      option.value = font.name;
      option.innerHTML = font.name;
      select.appendChild(option);
    });
    select.onclick = (): void => this.hidePopperInstances();
    select.onchange = (): void => this.fontFamilyStyleFeature(select);
    wrapper.appendChild(select);
  };

  appendFontSizeOption = (wrapper: HTMLElement): void => {
    const fontSizeInput = document.createElement('select');
    fontSizeInput.className = 'text-option-font-size';
    const sizes = [
      4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 26, 28, 30, 32, 36, 40,
      44,
    ];
    const blankEntry = document.createElement('option');
    blankEntry.value = ``;
    blankEntry.text = ``;
    fontSizeInput.appendChild(blankEntry);

    sizes.forEach((n) => {
      const px = document.createElement('option');
      px.value = `${n}`;
      px.text = `${n}`;

      fontSizeInput.appendChild(px);
    });

    wrapper.appendChild(fontSizeInput);

    const fontSizeType = document.createElement('select');
    fontSizeType.className = 'text-option-font-size';

    const sizeType = localStorage.getItem('fontSizeType') || 'px';

    const pt = document.createElement('option');
    pt.value = 'pt';
    pt.text = 'pt';
    pt.selected = sizeType === 'pt';
    const px = document.createElement('option');
    px.value = 'px';
    px.text = 'px';
    px.selected = sizeType !== 'pt';

    fontSizeType.appendChild(pt);
    fontSizeType.appendChild(px);

    fontSizeType.onchange = (e) => {
      const selectedValue: string = (e.target as _Any)?.value;
      let cv = parseFloat(fontSizeInput.value);
      if (selectedValue === 'pt') {
        cv = Math.round((cv * 72.0) / 96.0);
      } else {
        cv = Math.round((cv * 96.0) / 72.0);
      }
      fontSizeInput.value = cv.toString();
      localStorage.setItem('fontSizeType', selectedValue);
    };
    fontSizeInput.oninput = (): void =>
      this.fontSizeChange(fontSizeInput.value, fontSizeInput, fontSizeType.value);

    wrapper.appendChild(fontSizeType);
  };

  appendSpacingOption = (wrapper: HTMLElement): void => {
    const lms = new LmsAccess();
    this.appendTextOptionsButton(wrapper, translate('textoption.lineheight'));
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const spacingWrapper = document.createElement('div');
    spacingWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_CONTENT_WRAPPER;
    spacingWrapper.className = 'text-options-space-wrapper';
    spacingWrapper.style.display = 'none';

    // Line height
    const lineHeight = document.createElement('div');
    lineHeight.className = 'spacing-option-element';
    lineHeight.innerHTML = `<div class="label" id=${CONSTANTS.LOREE_TEXT_OPTIONS_LINE_HEIGHT_BUTTON}>${textOptionIcon.lineSpacingIcon}</div>`;
    const lineInputWrapper = document.createElement('div');
    lineInputWrapper.className = 'input';
    const lineInput = document.createElement('input');
    lineInput.id = 'line-height-input';
    lineInput.type = 'number';
    lineInput.setAttribute('step', 'any');
    lineInput.min = '0';
    lineInput.onchange = (event): void => this.updatePositionValues(event);
    lineInput.onkeypress = (event): void => this.preventNonNumericalInput(event);
    lineInput.onkeyup = (event): void => this.onKeyUp(event, 'line-height-input');
    lineInput.onkeydown = (event): void => this.onKeyDownForTextSpacing(event);
    lineInputWrapper.appendChild(lineInput);
    lineHeight.appendChild(lineInputWrapper);
    spacingWrapper.appendChild(lineHeight);
    lineInput.oninput = (): void => this.handleLineHeight(lineHeight, lineInput);

    // letter spacing
    const letterSpacing = document.createElement('div');
    letterSpacing.className = lms.getAccess() ? 'd-none' : 'spacing-option-element';
    letterSpacing.innerHTML = `<div class="label" id=${CONSTANTS.LOREE_TEXT_OPTIONS_LINE_SPACE_BUTTON}>${textOptionIcon.wordHeightIcon}</div>`;
    const wordSpaceInputWrapper = document.createElement('div');
    wordSpaceInputWrapper.className = 'input';
    const wordSpaceInput = document.createElement('input');
    wordSpaceInput.id = 'word-spacing-input';
    wordSpaceInput.type = 'number';
    wordSpaceInput.min = '0';
    wordSpaceInput.setAttribute('step', 'any');
    wordSpaceInput.onchange = (event): void => this.updatePositionValues(event);
    wordSpaceInput.onkeypress = (event): void => this.preventNonNumericalInput(event);
    wordSpaceInput.onkeyup = (event): void => this.onKeyUp(event, 'word-spacing-input');
    wordSpaceInput.onkeydown = (event): void => this.onKeyDownForTextSpacing(event);
    wordSpaceInputWrapper.appendChild(wordSpaceInput);
    letterSpacing.appendChild(wordSpaceInputWrapper);
    spacingWrapper.appendChild(letterSpacing);
    wordSpaceInput.oninput = (): void => this.handleLetterSpacing(letterSpacing, wordSpaceInput);
    iframeDocument.body.appendChild(spacingWrapper);
  };

  appendBorderStyles = (): void => {
    const iframeDocument = this.getDocument();
    if (getIframedocumentElementsByClassName(iframeDocument, 'border-style-section').length === 0) {
      if (!iframeDocument) return;
      const pcrBorder = getIframedocumentElementById(
        iframeDocument,
        CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
      );
      if (!pcrBorder) return;
      const child = pcrBorder.childNodes[1] as HTMLElement;
      if (!child) return;
      const swatches = getIframedocumentElementsByClassName(
        child,
        'pcr-swatches ',
      )[0] as HTMLElement;
      if (!swatches) return;
      this.appendBorderOptionsForText(swatches);
    }
  };

  appendCustomHexInputStyle = (id: string) => {
    const iframeDocument = this.getDocument();
    const colorPicker = iframeDocument?.getElementById(id);
    const wrapper = document.createElement('div');
    if (getIframedocumentElementsByClassName(colorPicker, 'custom-hex-color-input').length === 0) {
      const inputWrapper = colorPicker?.getElementsByClassName('pcr-interaction')[0] as HTMLElement;
      appendCustomHexInput(wrapper, id);
      inputWrapper?.insertBefore(wrapper.childNodes[0], inputWrapper.childNodes[0]);
    }
  };

  appendBorderOptionsForText = (colorPickerWrapper: HTMLElement): void => {
    const wrapper = document.createElement('div');
    wrapper.className = 'border-options-for-text-options';
    appendBorderWidthInput(wrapper);
    this.appendBorderStyleDropdown(wrapper);
    const parentNode = colorPickerWrapper.parentNode;
    if (!parentNode) return;
    parentNode.insertBefore(wrapper, colorPickerWrapper.nextSibling);
  };

  appendBorderStyleDropdown = (pcrBorder: HTMLElement): void => {
    const borderStyleDropdown = document.createElement('div');
    borderStyleDropdown.className = 'border-style-section';
    const dropdownButton = document.createElement('button');
    dropdownButton.id = 'text-border-style-dropdown';
    dropdownButton.className = 'btn dropdown-toggle border-style-dropdown';
    dropdownButton.innerHTML = 'Solid';
    dropdownButton.setAttribute('data-toggle', 'dropdown');
    borderStyleDropdown.appendChild(dropdownButton);
    const dropdownOptions = document.createElement('div');
    dropdownOptions.className = 'dropdown-menu border-style-options';
    BorderStyleOptions.forEach((borderStyle) => {
      const option = document.createElement('div');
      option.className = 'dropdown-item';
      option.innerHTML = borderStyle.label;
      dropdownOptions.appendChild(option);
      option.onclick = (): void =>
        handleDropdownValue(dropdownButton as HTMLElement, option.innerHTML);
    });
    borderStyleDropdown.appendChild(dropdownOptions);
    pcrBorder.appendChild(borderStyleDropdown);
  };

  appendWordOption = (wrapper: HTMLElement): void => {
    this.appendTextOptionsButton(wrapper, translate('textoption.wordoptions'));
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const wordOptionWrapper = document.createElement('div');
    wordOptionWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_WORD_CONTENT_WRAPPER;
    wordOptionWrapper.className = 'text-options-word-wrapper';
    wordOptionWrapper.style.display = 'none';
    textTransformOptions.forEach((option) => {
      const button = document.createElement('button');
      button.innerHTML = option.label;
      button.id = option.id;
      button.onclick = (): void => this.textTransformFeature(button, option.value);
      wordOptionWrapper.appendChild(button);
    });
    textDecorationOptions.forEach((option) => {
      const button = document.createElement('button');
      button.innerHTML = option.label;
      button.id = option.id;
      button.onclick = (): void => this.textDecorationFeature(button, option.value);
      wordOptionWrapper.appendChild(button);
    });
    textVerticalAlignOptions.forEach((option) => {
      const button = document.createElement('button');
      button.innerHTML = option.label;
      button.id = option.id;
      button.onclick = (): void => this.textVerticalAlignFeature(button, option.value);
      wordOptionWrapper.appendChild(button);
    });
    iframeDocument.body.appendChild(wordOptionWrapper);
  };

  appendAlignmentOption = (wrapper: HTMLElement): void => {
    this.appendTextOptionsButton(wrapper, translate('textoption.alignment'));
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const alignmentOptionWrapper = document.createElement('div');
    alignmentOptionWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_CONTENT_WRAPPER;
    alignmentOptionWrapper.className = 'text-options-alignment-wrapper';
    alignmentOptionWrapper.style.display = 'none';
    alignmentOptions.forEach((option) => {
      const button = document.createElement('button');
      button.innerHTML = option.label;
      button.id = option.id;
      button.onclick = (): void => this.alignmentFeature(button, option.value);
      alignmentOptionWrapper.appendChild(button);
    });
    iframeDocument.body.appendChild(alignmentOptionWrapper);
  };

  appendLinkOption = (wrapper: HTMLElement): void => {
    this.appendTextOptionsButton(wrapper, translate('textoption.insertlink'));
    const iframeDocument = this.getDocument();
    if (!iframeDocument) return;
    const linkOptionWrapper = document.createElement('div');
    linkOptionWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER;
    linkOptionWrapper.className = 'link-options-alignment-wrapper';
    linkOptionWrapper.style.display = 'none';
    const input = document.createElement('input');
    input.id = CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT;
    input.className = 'link-options-alignment-wrapper-input';
    input.placeholder = 'https://www.crystaldelta.com';
    linkOptionWrapper.appendChild(input);
    appendNewTabCheckbox(linkOptionWrapper);
    iframeDocument.body.appendChild(linkOptionWrapper);
  };

  appendArrow = (wrapper: HTMLElement): void => {
    const textOptionBottomArrow = document.createElement('div');
    textOptionBottomArrow.id = CONSTANTS.LOREE_TEXT_OPTIONS_ARROW_REFERENCE_ID;
    textOptionBottomArrow.innerHTML = `
    <div id=${CONSTANTS.LOREE_TEXT_OPTIONS_ARROW} class="textOptionArrowWrapper bottom">
    <div class="textOptionArrow bottom">
    </div>
    </div>`;
    wrapper.appendChild(textOptionBottomArrow);
  };

  appendAnchorArrow = (): void => {
    const wrapper = getIframedocumentElementById(
      this.getDocument(),
      'loree-text-anchor-link-wrapper',
    );
    const textOptionBottomArrow = document.createElement('div');
    textOptionBottomArrow.id = CONSTANTS.LOREE_TEXT_ANCHOR_ARROW_REFERENCE_ID;
    textOptionBottomArrow.innerHTML = `
    <div id=${CONSTANTS.LOREE_TEXT_OPTIONS_ANCHOR_ARROW} class="textOptionAnchorArrowWrapper bottom">
    <div class="textOptionArrow bottom">
    </div>
    </div>`;
    wrapper?.appendChild(textOptionBottomArrow);
  };
}

export default TextOptions;
