import { CustomHeader, CustomStyle } from './textOptionsData';

export const headerList: CustomHeader = [
  { h1: { font: 'Times New Roman', size: '66px' } },
  { h2: { font: '', size: '54px' } },
  { h3: { font: '', size: '46px' } },
  { h4: { font: '', size: '34px' } },
  { h5: { font: '', size: '28px' } },
  { h6: { font: '', size: '22px' } },
  { paragraph: { font: '', size: '20px' } },
];

export const fontList = [
  {
    fontFamily: 'Times New Roman',
    name: 'Times New Roman',
    url: 'https://fonts.googleapis.com/css2?family=Times New Roman',
    selected: true,
  },
  {
    fontFamily: 'Actor, sans-serif',
    name: 'Actor',
    url: 'https://fonts.googleapis.com/css2?family=Actor',
    selected: true,
  },
];

export const config: CustomStyle = {
  customColor: { colors: [{ color: '#000000' }] },
  customFonts: {
    fonts: [
      {
        fontFamily: 'Arial',
        name: 'Arial',
        url: 'https://fonts.googleapis.com/css2?family=Arial',
        selected: true,
      },
      {
        fontFamily: 'ABeezee, sans-serif',
        name: 'ABeezee',
        url: 'https://fonts.googleapis.com/css2?family=ABeezee',
        selected: true,
      },
    ],
  },
  customHeaderStyle: {
    customHeader: [
      {
        h1: {
          size: '66px',
          font: 'Arial',
        },
      },
      {
        h2: {
          size: '54px',
          font: 'Arial',
        },
      },
      {
        h3: {
          size: '46px',
          font: 'Arial',
        },
      },
      {
        h4: {
          size: '34px',
          font: 'SourceSansPro',
        },
      },
      {
        h5: {
          size: '28px',
          font: 'Arial',
        },
      },
      {
        h6: {
          size: '22px',
          font: 'Arial',
        },
      },
      {
        paragraph: {
          size: '20px',
          font: 'SourceSansPro',
        },
      },
    ],
  },
  customLinkStyle: { customLink: [{ color: '#000000', font: 'Arial' }] },
  features: { fontstyles: true },
};
