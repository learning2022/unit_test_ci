import CONSTANTS from '../../../constant';
import TextOptions from './textOptionsUI';
import { config } from './mockData';

jest.mock('../../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Pixels/Points', function () {
  let textOptions: TextOptions;
  let div: HTMLElement;
  let textNode: Node;
  let span: HTMLSpanElement;

  beforeEach(() => {
    textOptions = new TextOptions();
    jest.spyOn(textOptions, 'getDocument').mockImplementation(() => document);
    div = document.createElement('div');
    div.id = CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER;

    textNode = document.createTextNode('abcdefghijk');
    const d1 = document.createElement('div');
    span = document.createElement('span');
    span.append(textNode);
    d1.append(span);
    document.body.append(d1);
    document.body.append(div);
    textOptions.initiate({
      features: config.features,
      customColor: config.customColor,
      customFonts: config.customFonts,
      customHeaderStyle: config.customHeaderStyle,
      customLinkStyle: config.customLinkStyle,
    });
    const r = new Range();
    r.setStart(textNode, 1);
    r.setEnd(textNode, 10);
    document.getSelection()?.removeAllRanges();
    document.getSelection()?.addRange(r);
  });

  afterEach(() => {
    div.remove();
  });

  test('Options bar shows value in pixels', () => {
    span.style.fontSize = '24px';

    const fontSizeElement = div.children[4] as HTMLInputElement;
    const fontUnitElement = div.children[5] as HTMLSelectElement;

    fontUnitElement.value = 'px';

    textOptions.getSelectedDetails(document.getSelection()!, div);

    expect(fontSizeElement.value).toBe('24');
  });

  test('Options bar shows value in points', () => {
    span.style.fontSize = '24px';

    const fontSizeElement = div.children[4] as HTMLInputElement;
    const fontUnitElement = div.children[5] as HTMLSelectElement;

    fontUnitElement.value = 'pt';

    textOptions.getSelectedDetails(document.getSelection()!, div);

    expect(fontSizeElement.value).toBe('18');
  });

  test('Change from points to pixels updates', () => {
    span.style.fontSize = '24px';

    const fontSizeElement = div.children[4] as HTMLInputElement;
    const fontUnitElement = div.children[5] as HTMLSelectElement;

    fontUnitElement.value = 'pt';

    textOptions.getSelectedDetails(document.getSelection()!, div);
    expect(fontSizeElement.value).toBe('18');
    fontUnitElement.value = 'px';
    fontUnitElement.dispatchEvent(new Event('change'));
    expect(fontSizeElement.value).toBe('24');
  });

  test('Change from pixel to points updates', () => {
    span.style.fontSize = '24px';

    const fontSizeElement = div.children[4] as HTMLInputElement;
    const fontUnitElement = div.children[5] as HTMLSelectElement;

    fontUnitElement.value = 'px';

    textOptions.getSelectedDetails(document.getSelection()!, div);
    expect(fontSizeElement.value).toBe('24');
    fontUnitElement.value = 'pt';
    fontUnitElement.dispatchEvent(new Event('change'));
    expect(fontSizeElement.value).toBe('18');
  });
});
