import TextOptions from '../UI/textOptionsUI';
import { WordStyler } from '../native-styler/wordStyler';
import CONSTANTS from '../../../constant';
import Base from '../../../base';
import {
  validateUrl,
  customiseForegroundColorPicker,
  handleOrderedList,
} from '../native-styler/textOptionsEvents';
import Bootstrap from '../../iframe/templates/bootstrap';
import {
  checkFontFamily,
  getEditorElementsByClassName,
  getIframeDocument,
  getIframedocumentElementById,
  getIframedocumentElementsByClassName,
} from '../../../utils';
import { headerList, fontList, config } from './mockData';
import Design from '../../sidebar/design';
import { getElementById, createDiv } from '../../../common/dom';
import { fireEvent } from '@testing-library/react';
import * as textOption from '../native-styler/textOptionsEvents';

jest.mock('../../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

const selection = {
  anchorNode: {
    nodeName: '#text',
    nextSibling: null,
    firstChild: null,
    parentElement: {
      className: 'loree-iframe-content-element element-highlight',
      innerHTML: 'Insert text here',
      nodeName: 'p',
      style: {
        fontWeight: 'bold',
        fontStyle: 'italic',
        backgroundColor: '#ffffff',
      },
    } as HTMLElement,
    getAttribute: {},
  } as HTMLElement,
  type: 'Range',
} as unknown as Selection;
describe('TextOptionsUI', () => {
  const nonLinkFormats = [['www.Google.com'], ['HTTPS://WWW.Google.com'], ['crystaldelta']];

  const iframe = document.createElement('iframe');
  function loreeWrapper() {
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    iframe.appendChild(wrapper);
    iframe.innerHTML += Bootstrap;
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  loreeWrapper();
  const textOptionsInstance = new TextOptions();
  const baseInstance = new Base();
  const iframeDoc = getIframeDocument();
  const parentSampleDiv = document.createElement('div');
  baseInstance.validateWindowSelection = jest.fn().mockImplementation(() => true);
  parentSampleDiv.className = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER_CLASS;
  const sampleParagraph = document.createElement('p');
  sampleParagraph.id = 'dummyContent';
  sampleParagraph.className = 'dummyContent';
  sampleParagraph.innerHTML = 'Lorem Ipsum dolor sit Amet';
  parentSampleDiv.append(sampleParagraph);
  document.body.append(parentSampleDiv);
  const designColorPickerWrapper = document.createElement('div');
  designColorPickerWrapper.innerHTML = `<div class=${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS} visible></div><div class=${CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS}></div>`;
  document.body.append(designColorPickerWrapper);
  const range = document.createRange();
  range.setStart(document.getElementsByClassName('dummyContent')[0], 0);
  range.setEnd(document.getElementsByClassName('dummyContent')[0], 1);
  document.getSelection()?.addRange(range);
  baseInstance.getSelectedText = jest.fn().mockImplementation(() => document.getSelection());
  const colorPickerInputWrapper = document.createElement('div');
  colorPickerInputWrapper.className = 'pcr-interaction';
  const getDocument = baseInstance?.getDocument();
  if (getDocument) {
    getDocument.getSelection = jest.fn().mockImplementation(() => document.getSelection());
  }
  const designSectionColorPickerSpy = jest.spyOn(textOption, 'hideSideBarColorPickers');
  textOption.hideSideBarColorPickers();

  test('UI rendered', () => {
    textOptionsInstance.initiate(config);
    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER)?.children,
    ).toHaveLength(17);
  });

  test('click on style button opens popup with headers, paragraph options', () => {
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_BUTTON)?.click();

    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_PARAGRAPH_CONTENT_WRAPPER)
        ?.style.display,
    ).toEqual('inline-flex');
  });
  test('click on header1 changes P into H1 tag', () => {
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_HEADER1_BUTTON)?.click();
    expect(parentSampleDiv.innerHTML).toEqual(
      `<h1 class="dummyContent" id="dummyContent" style="font-family: Arial; font-size: 66px;">Lorem Ipsum dolor sit Amet</h1>`,
    );
  });
  test('click on line height button opens popup with line height input box', () => {
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_BUTTON)?.click();
    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_SPACING_CONTENT_WRAPPER)
        ?.style.display,
    ).toEqual('inline-flex');
  });
  test('click on text color button opens text color picker', () => {
    const wrapper = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER_WRAPPER,
    );
    const wrapperChild = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER,
    ) as HTMLElement;
    wrapperChild.appendChild(colorPickerInputWrapper);
    iframe.contentDocument
      ?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_BUTTON)
      ?.click();
    expect(wrapper?.style.display).toEqual('inline-flex');
    const input = iframe.contentDocument?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    expect(input).toBeDefined();
  });
  test.each([
    CONSTANTS.LOREE_TEXT_OPTIONS_FOREGROUND_COLOR_PICKER,
    CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER,
    CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER,
  ])('text option color picker should close design section color pickers', (colorPicker) => {
    const textOptionColorPicker = getIframedocumentElementById(iframeDoc, colorPicker);
    textOptionColorPicker.click();
    expect(designSectionColorPickerSpy).toHaveBeenCalledTimes(1);
    expect(
      getEditorElementsByClassName(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS)[0]
        .classList,
    ).not.toContain('visible');
    expect(
      getEditorElementsByClassName(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION_COLOR_PICKERS)[1]
        .classList,
    ).not.toContain('visible');
  });
  test('click on background color button opens background color picker', () => {
    iframe.contentDocument
      ?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_BUTTON)
      ?.click();
    const wrapperChild = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER,
    ) as HTMLElement;
    wrapperChild.appendChild(colorPickerInputWrapper);
    expect(
      iframe.contentDocument?.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER,
      )?.style.display,
    ).toEqual('inline-flex');
    const input = iframe.contentDocument?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    expect(input).toBeDefined();
  });

  test('should open the colorPicker to change the color of the background', () => {
    textOption?.handleBackgroundColorPicker();
    const inputElement = iframe.contentDocument?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    expect(inputElement.tagName).toBe('INPUT');
  });

  test('should change the background color for the selected Text', () => {
    jest.spyOn(iframe?.contentWindow as Window, 'getSelection').mockReturnValueOnce(selection);
    textOption.handleBackgroundColorPicker();
    const input = iframe.contentDocument?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    fireEvent.change(input, { target: { value: '#000000' } });
    expect(input.value).toBe('#000000');
  });

  test('click on border color button opens border color picker', () => {
    iframe.contentDocument
      ?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_BUTTON)
      ?.click();
    const wrapperChild = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER,
    ) as HTMLElement;
    wrapperChild.appendChild(colorPickerInputWrapper);
    expect(
      iframe.contentDocument?.getElementById(
        CONSTANTS.LOREE_TEXT_OPTIONS_BORDER_COLOR_PICKER_WRAPPER,
      )?.style.display,
    ).toEqual('inline-flex');
    const input = iframe.contentDocument?.getElementsByClassName(
      'custom-hex-color-input',
    )[0] as HTMLInputElement;
    expect(input).toBeDefined();
  });
  test('click on word options button opens popup with word options', () => {
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_WORD_BUTTON)?.click();
    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_WORD_CONTENT_WRAPPER)
        ?.style.display,
    ).toEqual('inline-flex');
  });
  test('click on alignment button opens popup with alignment options', () => {
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_BUTTON)?.click();
    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_ALIGNMENT_CONTENT_WRAPPER)
        ?.style.display,
    ).toEqual('inline-flex');
  });
  test('click on insert link button opens popup with insert link input box', () => {
    iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON)?.click();
    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER)
        ?.style.display,
    ).toEqual('inline-flex');
  });

  test.each(nonLinkFormats)('should throw error when links are not in exact format', (link) => {
    textOptionsInstance.inputLinkHandler = jest.fn();
    validateUrl(link, '');
    baseInstance.urlValidatorUrl();
    expect(textOptionsInstance.inputLinkHandler).toBeCalledTimes(0);
    expect(
      iframe.contentDocument?.getElementById(CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT)?.style
        .border,
    ).toBe('red');
  });

  test('click on clear format', () => {
    const designInstance = new Design();
    const sideBar = createDiv();
    sideBar.id = CONSTANTS.LOREE_SIDEBAR_CONTENT_WRAPPER;
    document.body.appendChild(sideBar);
    designInstance.initiate({});
    designInstance.attachDesignContent('element', sampleParagraph, { fontstyles: true });
    baseInstance.showDesignSection('element', sampleParagraph);
    const textOptionButton = iframe.contentDocument?.getElementsByClassName(
      'text-option-button',
    ) as HTMLCollectionOf<HTMLElement>;
    const sidebarDesign = getElementById(CONSTANTS.LOREE_SIDEBAR_DESIGN_SECTION);

    expect(sidebarDesign?.style.display).toEqual('block');
    textOptionButton[textOptionButton.length - 1].click();
    expect(sidebarDesign?.style.display).toEqual('none');
  });

  test('apply valid link using enter key', () => {
    const linkInput = getIframedocumentElementById(
      iframe.contentDocument,
      CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT,
    ) as HTMLInputElement;
    fireEvent.change(linkInput, { target: { value: 'www.google.com' } });
    fireEvent.keyDown(linkInput, { key: 'Enter', keyCode: 13, charCode: 13 });
    expect(linkInput.style.borderColor).toEqual('');
  });
  test('click on remove link icon should close text options popper', () => {
    const removeLink = getIframedocumentElementsByClassName(
      iframe.contentDocument,
      'link-options-remove-link',
    )[0] as HTMLElement;
    const textOptionsPopper = getIframedocumentElementById(
      iframe.contentDocument,
      CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER,
    ) as HTMLElement;
    removeLink.click();
    expect(textOptionsPopper.style.visibility).toEqual('hidden');
  });
});

describe('When text option popper is opened', () => {
  describe('Append Ordered List Option', () => {
    const textOptionsInstance = new TextOptions();
    const baseInstance = new Base();
    const wordStylerInstance = new WordStyler();
    let textOptionWrapper: HTMLDivElement;
    let wrapper: HTMLElement;
    let anchorParent;
    let listConvertedElement: HTMLElement;
    beforeEach(() => {
      wrapper = document.body;
      textOptionWrapper = document.createElement('div');
      textOptionWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER;
      textOptionWrapper.style.visibility = 'hidden';
      const pTagElement = document.createElement('p');
      pTagElement.className = 'loree-iframe-content-element';
      pTagElement.style.cssText =
        'border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Actor; padding: 5px; margin: 0px 0px 10px; font-size: 20px;';
      pTagElement.setAttribute('contenteditable', 'true');
      pTagElement.innerHTML = `<span class="loree-iframe-content-element" style="padding: 5px; margin: 0px 0px 10px; font-size: 20px;">Insert <span class="loree-iframe-content-element" style="font-size: 20px;">text</span> here</span><br><span class="loree-iframe-content-element" style="padding: 5px; margin: 0px 0px 10px; font-size: 20px;">Insert text here</span><br><span class="loree-iframe-content-element element-highlight" style="padding: 5px; margin: 0px 0px 10px; font-size: 20px;">Insert text here</span><br>`;
      wrapper.appendChild(textOptionWrapper);
      wrapper.appendChild(pTagElement);
      textOptionsInstance.appendTextOptionsButton(textOptionWrapper, 'textoption.orderedlist');
      anchorParent = wrapper.getElementsByClassName(
        'loree-iframe-content-element',
      )[0] as HTMLElement;
      listConvertedElement = wordStylerInstance.multiLineItems(anchorParent, selection, 'OL');
    });
    test('Verify the appended option is text-option element or not', () => {
      expect(textOptionWrapper.firstChild).toHaveClass('text-option-button editor-tooltip');
    });
    test('Verify the option appended is Ordered List or not', () => {
      expect(textOptionWrapper.firstChild).toContainHTML(
        `id="${CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON}"`,
      );
    });

    test.skip('check text turned into Ordered List or not', () => {
      baseInstance.getDocument = jest.fn().mockImplementation(() => selection);
      baseInstance.validateWindowSelection = jest.fn().mockImplementation(() => true);
      wordStylerInstance.getSelectedContent = jest.fn().mockImplementation(() => selection);
      handleOrderedList(
        document?.getElementById(
          CONSTANTS.LOREE_TEXT_OPTIONS_ORDER_LIST_BUTTON,
        ) as HTMLButtonElement,
      );
      expect(listConvertedElement.localName).toBe('ol');
    });
    test('check ordered list returned as expected text and structure', () => {
      expect(listConvertedElement.TEXT_NODE).toBe(3);
    });
    test('verify the orderedlist style and editable property', () => {
      expect(listConvertedElement.contentEditable).toBeTruthy();
      expect(listConvertedElement.style.padding).toEqual('0px 0px 0px 40px');
    });
  });
});

describe('text colorpicker  testcases', () => {
  let textOptionsInstance: TextOptions;
  let wrapper: HTMLElement;
  let button: HTMLElement;
  let textOptionWrapper: HTMLDivElement;
  let iframeElement: HTMLIFrameElement;
  let pcr: HTMLDivElement;
  beforeEach(() => {
    textOptionsInstance = new TextOptions();
    wrapper = document.body;
    pcr = document.createElement('div');
    pcr.className = 'loree-design-color-pickers visible';
    iframeElement = document.createElement('iframe');
    iframeElement.id = CONSTANTS.LOREE_IFRAME;
    button = document.createElement('button');
    button.id = CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_PICKER_WRAPPER;
    textOptionWrapper = document.createElement('div');
    textOptionWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_BACKGROUND_COLOR_BUTTON;
    iframeElement.appendChild(textOptionWrapper);
    iframeElement.appendChild(button);
    wrapper.appendChild(iframeElement);
    wrapper.appendChild(pcr);
  });

  test('calling the function customiseTextBackgroundColorPickr()  in the textbackgroundclick while opening the color picker', () => {
    textOptionsInstance.customiseTextBackgroundColorPickr = jest
      .fn()
      .mockImplementation(() => 'true');
    expect(textOptionsInstance.customiseTextBackgroundColorPickr()).toBe('true');
  });
});

describe('#CustomLinks', () => {
  const textOptionsInstance = new TextOptions();
  const baseInstance = new Base();
  const wordStylerInstance = new WordStyler();
  let textOptionWrapper: HTMLDivElement;
  let wrapper: HTMLElement;
  let customLink: '' | { [x: string]: string; color: string; font: string };
  const linkFormats = [
    [
      'https://crystaldelta.d2l-partners.brightspace.com/d2l/common/dialogs/quickLink/quickLink.d2l?ou=6800&type=lti&rcode=4527DE4C-056A-414A-AEA2-B0AEC6B26387-3560&srcou=6606&launchFramed=1&framedName=Loree-develop',
    ],
    ['http://crystaldeta.instructure.com'],
    ['www.google.com'],
    ['ftp://crystaldelta.instructure.com'],
    ['smtp://crystaldelta.instructure.com'],
    ['https://crystaldelta.atlassian.net/secure/RapidBoard.jspa?rapidView=57&projectKey=LOREE'],
  ];

  beforeEach(() => {
    jest.clearAllMocks();
    wrapper = document.body;
    textOptionWrapper = document.createElement('div');
    textOptionWrapper.id = CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER;
    textOptionWrapper.style.visibility = 'hidden';
    const pTagElement = document.createElement('p');
    pTagElement.className = 'loree-iframe-content-element';
    pTagElement.style.cssText =
      'border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: Actor; padding: 5px; margin: 0px 0px 10px; font-size: 20px;';
    pTagElement.setAttribute('contenteditable', 'true');
    pTagElement.innerHTML = `<span class="loree-iframe-content-element" style="padding: 5px; margin: 0px 0px 10px; font-size: 20px;">Insert <span class="loree-iframe-content-element" style="font-size: 20px;">text</span> here</span>`;
    wrapper.appendChild(textOptionWrapper);
    wrapper.appendChild(pTagElement);
    textOptionsInstance.getDocument = jest.fn().mockImplementation(() => document);
    textOptionsInstance.appendLinkOption(textOptionWrapper);
    customLink = {
      color: '#b01a8b',
      'text-decoration': 'Overline',
      ' text-decoration-style': 'Dotted',
      'text-style': 'Italic',
      font: 'Arial',
    };
  });
  afterEach(() => {
    document.getElementsByTagName('html')[0].innerHTML = '';
  });

  test('render the textoptions popper when a text is selected', () => {
    expect(textOptionWrapper.firstChild).toHaveClass('text-option-button editor-tooltip');
  });
  test('verify Insert link option is rendered inside the textoptions popper', () => {
    expect(textOptionWrapper.firstChild).toContainHTML(
      `id="${CONSTANTS.LOREE_TEXT_OPTIONS_LINK_BUTTON}"`,
    );
  });
  test('before triggering click on Insert Link icon the insert link popper should be hidden', () => {
    expect(
      document?.getElementById(CONSTANTS.LOREE_TEXT_OPTIONS_LINK_CONTENT_WRAPPER)?.style.display,
    ).toBe('none');
  });
  test.each(linkFormats)('validate the input box handler', (link) => {
    baseInstance.validateWindowSelection = jest.fn();
    baseInstance.inputLinkHandler(link, customLink);
    expect(baseInstance.validateWindowSelection).toBeCalledTimes(1);
  });
  test.each(linkFormats)('validate the link given in the input box', (link) => {
    textOptionsInstance.inputLinkHandler = jest.fn();
    validateUrl(link, '');
    baseInstance.urlValidatorUrl();
    expect(getElementById(CONSTANTS.LOREE_TEXT_USER_ANCHOR_LINK_INPUT)?.style.border).toBe('');
  });
  test.each(linkFormats)('validate the link for the selected text', (link) => {
    wordStylerInstance.createLink = jest.fn();
    wordStylerInstance.handleLinkForSelection(link, customLink);
    expect(wordStylerInstance.createLink).toBeCalledTimes(1);
  });
});

describe('testcase for customiseForegroundColorPicker', () => {
  let textOptionsInstance: TextOptions;
  beforeEach(() => {
    textOptionsInstance = new TextOptions();
    const element = document.createElement('div');
    element.innerHTML =
      '<input class="pcr-save" value="Apply" type="button" aria-label="save and close">';
    document.body.append(element);
  });

  test('clicking save button in the customiseForegroundColorPicker', () => {
    const color = '#FFFFFF';
    customiseForegroundColorPicker();
    document.body.firstChild?.addEventListener('click', function () {
      textOptionsInstance.setuserForegroundColor = jest
        .fn()
        .mockImplementation((color) => 'colorchange');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(textOptionsInstance.setuserForegroundColor(color)).toBe('colorchange');
  });

  test('clicking clear button in the customiseForegroundColorPicker', () => {
    const color = '#FFFFFF';
    customiseForegroundColorPicker();
    document.body.firstChild?.addEventListener('click', function () {
      textOptionsInstance.setuserForegroundColor = jest
        .fn()
        .mockImplementation((color) => 'colorchange');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(textOptionsInstance.setuserForegroundColor(color)).toBe('colorchange');
  });

  test('clicking clear button need to set the black in the customiseForegroundColorPicker', () => {
    const color = '#000000';
    customiseForegroundColorPicker();
    document.body.firstChild?.addEventListener('click', function () {
      textOptionsInstance.setuserForegroundColor = jest
        .fn()
        .mockImplementation((color) => '#000000');
    });
    document.body.firstChild?.dispatchEvent(new Event('click'));
    expect(textOptionsInstance.setuserForegroundColor(color)).toBe('#000000');
  });

  test('check font family', () => {
    checkFontFamily(headerList, fontList, 'editor');
    expect(headerList[0].h1?.font).toBe('Times New Roman');
    expect(headerList[1].h2?.font).toBe(CONSTANTS.DEFAULT_EDITOR_FONT_FAMILY);
    expect(headerList[6].paragraph?.font).toBe(CONSTANTS.DEFAULT_EDITOR_FONT_FAMILY);
  });
});
