import Pickr from '@simonwep/pickr';
import { getElementsByClassName } from '../../common/dom';
import { translate } from '../../../i18n/translate';

interface ColorPickerInterface extends Pickr.Options {
  i18n: {
    'btn:save': string;
    'btn:cancel': string;
  };
}

const hideColorPickerofElementSection = (elementRef: string) => {
  const pickers = Array.from(getElementsByClassName(elementRef));
  for (const picker of pickers) {
    picker.outerHTML = '';
  }
};

export const validateHTMLColorHex = (color: string) => {
  if (color.toString()) {
    /* validating the value is proper
     ** Matches the string starts with # , () groups multiple val togther that contains 0-9 digits and a-f alphabets for first 3 
       chars |  4 chars 
    */
    const regex = /^#([\da-f]{3}){1,2}$|^#([\da-f]{4}){1,2}$/i;
    return color && regex.test(color);
  }
};

export const componentToHex = (num: number): string => {
  const hex = num.toString(16);
  return hex.length === 1 ? '0' + hex : hex;
};

export const rgbToHex = (input: string, defaultValue: string): string => {
  // test input one or more is between 0-9 (\d+)
  const numbers = input.match(/\d+/g)?.map(Number) as number[];
  if (numbers !== undefined) {
    const red = numbers[0];
    const green = numbers[1];
    const blue = numbers[2];
    return '#' + componentToHex(red) + componentToHex(green) + componentToHex(blue);
  } else {
    return defaultValue;
  }
};

export const hex2rgb = (hex: string) => {
  // converts hex to rgb by matching word chars both alpha and numeric
  const [r, g, b] = hex.match(/\w\w/g)?.map((x) => parseInt(x, 16)) as number[];
  return `rgb(${r}, ${g}, ${b})`;
};

export const mockColorPickerInstance = (
  elementRef: HTMLElement,
  elementClass: string,
  defaultColor = '#FFFFFF',
): Pickr => {
  const options: ColorPickerInterface = {
    el: elementRef,
    theme: 'nano',
    swatches: [],
    appClass: elementClass,
    default: defaultColor,
    defaultRepresentation: 'HEXA',
    useAsButton: true,
    lockOpacity: true,
    components: {
      preview: true,
      opacity: false,
      hue: true,
      interaction: {
        input: true,
        rgba: false,
        hex: true,
        clear: true,
        cancel: true,
        save: true,
      },
    },
    i18n: {
      'btn:save': translate('global.apply'),
      'btn:cancel': translate('global.cancel'),
    },
  };
  return new Pickr(options);
};

export { hideColorPickerofElementSection as default };
