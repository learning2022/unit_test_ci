import CONSTANTS from '../../constant';
import { videoModalIcons, imageModalIcons, closeIcon } from '../sidebar/iconHolder';
import { LmsAccess } from '../../../utils/lmsAccess';
import { getAllCanvasImages, getAllImages, getStandaloneMyImageList } from './imageModalUtils';
import { ShapesIcon, CropIcon } from '../space/icons';
import propertiesIcon from '../../../assets/Icons/properties.svg';
import effectIcon from '../../../assets/Icons/effects.svg';
import circleShapeIcon from '../../../assets/Icons/Ellipse 242.svg';
import rectangleShapeIcon from '../../../assets/Icons/Rectangle 2947.svg';
import Cropper, { croppedImageSource } from './image-cropper';
import { imageInfo } from './imageModal';
import { isBB } from '../../../lmsConfig';
import { getElementById } from '../../common/dom';
import { translate } from '../../../i18n/translate';

export const appendModalContentHeader = (): HTMLElement => {
  const header = document.createElement('div');
  header.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_HEADER;
  header.className = 'd-flex flex-row flex-wrap justify-content-between align-items-center';
  const headerText = document.createElement('p');
  headerText.innerHTML = translate('modal.images');
  headerText.id = CONSTANTS.LOREE_IMAGE_VIEW_MODAL;
  headerText.className = 'modal-title';
  const headerEditImageText = document.createElement('p');
  headerEditImageText.innerHTML = translate('modal.editimage');
  headerEditImageText.id = CONSTANTS.LOREE_EDIT_IMAGE_MODAL_HEADER;
  headerEditImageText.className = 'modal-title d-none';
  const headerIcon = document.createElement('button');
  headerIcon.id = CONSTANTS.LOREE_IMAGE_MODAL_WRAPPER_CLOSE;
  headerIcon.type = 'button';
  headerIcon.className = 'close';
  headerIcon.innerHTML = closeIcon;
  header.appendChild(headerEditImageText);
  header.appendChild(headerText);
  header.appendChild(headerIcon);
  return header;
};

export const appendModalContentHeaderDivider = (): HTMLElement => {
  const divider = document.createElement('div');
  divider.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_DIVIDER;
  return divider;
};

export const appendImageModalFilterContainer = (): HTMLElement => {
  const lms = new LmsAccess();
  const imageModalHeaderFilter = document.createElement('div');
  imageModalHeaderFilter.className = 'd-flex flex-row justify-content-between';
  imageModalHeaderFilter.id = CONSTANTS.LOREE_IMAGE_MODAL_FILTER_CONTAINER;
  // search bar
  const searchInput = document.createElement('input');
  searchInput.id = CONSTANTS.LOREE_IMAGE_MODAL_FILTER_SEARCH_BOX;
  searchInput.className = 'image-search-box';
  searchInput.name = 'search';
  searchInput.type = 'text';
  searchInput.autocomplete = 'off';
  searchInput.placeholder = translate('global.search');
  // @ts-expect-error
  searchInput.autofocus = true;
  const searchIconDiv = document.createElement('div');
  searchIconDiv.id = CONSTANTS.LOREE_IMAGE_MODAL_FILTER_SEARCH_ICON;
  searchIconDiv.className = 'loree-image-modal-filter-search-icon-style';
  searchIconDiv.innerHTML = imageModalIcons.imageModalSearchIcon;
  // dropdown
  const filterDropDown = document.createElement('div');
  filterDropDown.className = 'dropdown dropdown-layout';
  const filterDropDownButton = document.createElement('button');
  filterDropDownButton.id = CONSTANTS.LOREE_IMAGE_MODAL_FILTER_BUTTON;
  filterDropDownButton.className =
    'btn btn-primary dropdown-toggle d-flex justify-content-between align-items-center';
  filterDropDownButton.type = 'button';
  filterDropDownButton.setAttribute('data-toggle', 'dropdown');
  filterDropDownButton.innerHTML = translate('modal.recent');
  const dropDownItem = document.createElement('div');
  dropDownItem.className = 'dropdown-menu';
  dropDownItem.id = CONSTANTS.LOREE_IMAGE_MODAL_FILTER_DROPDOWN;
  dropDownItem.setAttribute('aria-labelledby', CONSTANTS.LOREE_IMAGE_MODAL_FILTER_DROPDOWN);
  const dropDownButton = document.createElement('div');
  dropDownButton.className = 'dropdown-item';
  dropDownButton.innerHTML = translate('modal.recent');
  const dropDownAscendingButton = document.createElement('div');
  dropDownAscendingButton.className = 'dropdown-item';
  dropDownAscendingButton.innerHTML = translate('modal.ascending');
  const dropDownDescedingButton = document.createElement('div');
  dropDownDescedingButton.className = 'dropdown-item';
  dropDownDescedingButton.innerHTML = translate('modal.descending');
  const dropDownSizeButton = document.createElement('div');
  dropDownSizeButton.className = !lms.getAccess() ? 'dropdown-item' : 'd-none';
  dropDownSizeButton.innerHTML = translate('global.size');
  dropDownItem.appendChild(dropDownButton);
  dropDownItem.appendChild(dropDownAscendingButton);
  dropDownItem.appendChild(dropDownDescedingButton);
  dropDownItem.appendChild(dropDownSizeButton);
  filterDropDown.appendChild(filterDropDownButton);
  filterDropDown.appendChild(dropDownItem);
  imageModalHeaderFilter.appendChild(searchInput);
  imageModalHeaderFilter.appendChild(searchIconDiv);
  imageModalHeaderFilter.appendChild(filterDropDown);
  return imageModalHeaderFilter;
};

export const appendModalLeftContentHeader = (): HTMLElement => {
  const lms = new LmsAccess();
  const imageModalLeftContainerHeader = document.createElement('div');
  imageModalLeftContainerHeader.className = 'd-flex flex-row left-container-header';
  const imageModalTitleHeader = document.createElement('div');
  imageModalTitleHeader.className = 'left-container-header-title';
  const titleHeaderNav = document.createElement('nav');
  const titleHeaderNavChild = document.createElement('div');
  titleHeaderNavChild.className = 'nav nav-tabs';
  titleHeaderNavChild.setAttribute('role', 'tablist');
  const courseImageHeader = document.createElement('a');
  courseImageHeader.className = 'nav-item nav-link modal-tab';
  courseImageHeader.id = lms.getAccess()
    ? CONSTANTS.LOREE_IMAGE_MODAL_NAV_LMS_IMAGE
    : CONSTANTS.LOREE_IMAGE_MODAL_NAV_MY_IMAGE;
  courseImageHeader.setAttribute('data-toggle', 'tab');
  courseImageHeader.href = lms.getAccess()
    ? `#${CONSTANTS.LOREE_IMAGE_MODAL_LMS_IMAGE_CONTAINER}`
    : `#${CONSTANTS.LOREE_IMAGE_MODAL_MY_IMAGE_CONTAINER}`;
  courseImageHeader.setAttribute('role', 'tab');
  courseImageHeader.setAttribute(
    'aria-controls',
    lms.getAccess()
      ? CONSTANTS.LOREE_IMAGE_MODAL_LMS_IMAGE_CONTAINER
      : CONSTANTS.LOREE_IMAGE_MODAL_MY_IMAGE_CONTAINER,
  );
  courseImageHeader.setAttribute('aria-selected', 'true');
  // to note
  courseImageHeader.innerText = lms.getAccess()
    ? translate('modal.courseimages')
    : translate('modal.myimages');
  courseImageHeader.onclick = async () =>
    lms.getAccess() ? await getAllCanvasImages() : await getStandaloneMyImageList();
  const loreeImageHeader = document.createElement('a');
  loreeImageHeader.className = 'nav-item nav-link modal-tab';
  loreeImageHeader.id = CONSTANTS.LOREE_IMAGE_MODAL_NAV_LOREE_IMAGE;
  loreeImageHeader.setAttribute('data-toggle', 'tab');
  loreeImageHeader.href = `#${CONSTANTS.LOREE_IMAGE_MODAL_LOREE_IMAGE_CONTAINER}`;
  loreeImageHeader.setAttribute('role', 'tab');
  loreeImageHeader.setAttribute('aria-controls', CONSTANTS.LOREE_IMAGE_MODAL_LOREE_IMAGE_CONTAINER);
  loreeImageHeader.setAttribute('aria-selected', 'false');
  loreeImageHeader.onclick = async () => await getAllImages();
  loreeImageHeader.innerText = translate('modal.loreeimages');
  if (sessionStorage.getItem('domainName') !== 'D2l') {
    titleHeaderNavChild.appendChild(courseImageHeader);
  }
  titleHeaderNavChild.appendChild(loreeImageHeader);
  titleHeaderNav.appendChild(titleHeaderNavChild);
  imageModalTitleHeader.appendChild(titleHeaderNav);
  imageModalLeftContainerHeader.appendChild(imageModalTitleHeader);
  imageModalLeftContainerHeader.appendChild(appendImageModalFilterContainer());
  return imageModalLeftContainerHeader;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const appendModalContentBody = (featuresList: any): HTMLElement => {
  const lms = new LmsAccess();
  const imageModalBody = document.createElement('div');
  imageModalBody.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY;
  imageModalBody.className = 'd-flex flex-row flex-nowrap';
  // left container
  const imageModalBodyLeftContainerParent = document.createElement('div');
  imageModalBodyLeftContainerParent.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY_LEFT_CONTAINER;
  imageModalBodyLeftContainerParent.className = 'd-flex flex-column';
  imageModalBodyLeftContainerParent.appendChild(appendModalLeftContentHeader());
  const tabContent = document.createElement('div');
  tabContent.id = 'nav-tabContent';
  tabContent.className = 'tab-content';
  const courseImageTabParent = document.createElement('div');
  courseImageTabParent.className = 'tab-pane fade modal-tab-container';
  courseImageTabParent.id = lms.getAccess()
    ? CONSTANTS.LOREE_IMAGE_MODAL_LMS_IMAGE_CONTAINER
    : CONSTANTS.LOREE_IMAGE_MODAL_MY_IMAGE_CONTAINER;
  courseImageTabParent.setAttribute('role', 'tabpanel');
  courseImageTabParent.setAttribute(
    'aria-labelledby',
    lms.getAccess()
      ? CONSTANTS.LOREE_IMAGE_MODAL_NAV_LMS_IMAGE
      : CONSTANTS.LOREE_IMAGE_MODAL_NAV_MY_IMAGE,
  );
  const courseImage = document.createElement('div');
  courseImage.id = lms.getAccess()
    ? CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER
    : CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER;
  courseImage.className = 'd-flex flex-row flex-wrap';
  courseImageTabParent.appendChild(courseImage);
  tabContent.appendChild(courseImageTabParent);
  const loreeImageTabParent = document.createElement('div');
  loreeImageTabParent.className = 'tab-pane fade modal-tab-container';
  loreeImageTabParent.id = CONSTANTS.LOREE_IMAGE_MODAL_LOREE_IMAGE_CONTAINER;
  loreeImageTabParent.setAttribute('role', 'tabpanel');
  loreeImageTabParent.setAttribute('aria-labelledby', CONSTANTS.LOREE_IMAGE_MODAL_NAV_LOREE_IMAGE);
  const loreeImage = document.createElement('div');
  loreeImage.id = CONSTANTS.LOREE_IMAGE_LIST_CONTAINER;
  loreeImage.className = 'd-flex flex-row flex-wrap';
  loreeImageTabParent.appendChild(loreeImage);
  tabContent.appendChild(loreeImageTabParent);
  imageModalBodyLeftContainerParent.appendChild(tabContent);
  imageModalBody.appendChild(imageModalBodyLeftContainerParent);
  // middle container
  if (featuresList.uploadimage && featuresList.insertlink) {
    const middleContainer = document.createElement('div');
    middleContainer.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY_MIDDLE_CONTAINER;
    middleContainer.className = 'd-flex align-items-center';
    middleContainer.innerHTML = `<div>(${translate('global.or')})</div>`;
    imageModalBody.appendChild(middleContainer);
  }
  // appended all containers
  imageModalBody.appendChild(appendVideoModalRightContainer(featuresList));
  return imageModalBody;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const appendVideoModalRightContainer = (featuresList: any): HTMLElement => {
  const rightContainer = document.createElement('div');
  rightContainer.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY_RIGHT_CONTAINER;
  rightContainer.className = 'd-flex align-items-center';
  const rightContainerLayout = document.createElement('div');
  rightContainerLayout.className = `right-container-layout`;
  // Local uploader
  const localUploadBody = document.createElement('div');
  localUploadBody.style.marginBottom = '12px';
  const uploaderInput = document.createElement('div');
  uploaderInput.className = 'upload-add-icon';
  if (featuresList.uploadimage) {
    const uploadButton = document.createElement('button');
    uploadButton.id = CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT;
    uploadButton.innerHTML = `<span>${videoModalIcons.addIcon}</span>
    ${
      sessionStorage.getItem('domainName') === 'canvas'
        ? `${translate('modal.uploadnewcourseimage')}`
        : `${translate('modal.uploadnewimage')}`
    }`;
    uploaderInput.appendChild(uploadButton);
    const uploadMessage = document.createElement('div');
    uploadMessage.id = CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_MESSAGE;
    uploadMessage.innerHTML = `<p>${translate('modal.supportedfiles')} - .jpg, .jpeg, .png.</p>
    <p>${translate('modal.morethan500kb')}</p>`;
    const errorMessage = document.createElement('div');
    errorMessage.innerHTML += `<div id="${
      CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_FILESIZE_ERROR_MESSAGE
    }" class="d-none">
    <p>${translate('modal.imageswhichexceeds5MB')}</p>
  </div>`;
    localUploadBody.appendChild(uploaderInput);
    localUploadBody.appendChild(uploadMessage);
    isBB() && localUploadBody.appendChild(errorMessage);
  }

  // byurl uploader
  if (featuresList.insertbyurl) {
    const byUrlUploader = document.createElement('div');
    byUrlUploader.style.marginBottom = '12px';
    const byUrlIcon = document.createElement('span');
    byUrlIcon.innerHTML = videoModalIcons.addIcon;
    const byUrlInput = document.createElement('input');
    byUrlInput.id = CONSTANTS.LOREE_IMAGE_BYURL_INPUT;
    byUrlInput.type = 'text';
    byUrlInput.placeholder = translate('global.insertlink');
    byUrlInput.autocomplete = 'off';
    byUrlUploader.appendChild(byUrlIcon);
    byUrlUploader.appendChild(byUrlInput);
    byUrlUploader.innerHTML += `<div id="${
      CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_ERROR_MESSAGE
    }" class="d-none">
    <p>${translate('modal.enteravalidimageurl')}</p>
  </div>`;
    // appended all uploader
    rightContainerLayout.appendChild(localUploadBody);
    rightContainerLayout.appendChild(byUrlUploader);
  }
  rightContainer.appendChild(rightContainerLayout);
  return rightContainer;
};

export const appendImageDetailModal = (): HTMLElement => {
  const detailModalContainer = document.createElement('div');
  detailModalContainer.id = CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY;
  detailModalContainer.className =
    'd-none d-flex flex-column flex-md-row image-details-modal-wrapper';
  detailModalContainer.innerHTML = `<div class="col-12 col-md-6 pl-0">
    <div id=${CONSTANTS.LOREE_IMAGE_DETAILS_PREVIEW} class="w-100 d-flex image-preview-block justify-content-center align-items-center"></div></div>`;
  const detailRightContainer = document.createElement('div');
  detailRightContainer.className = 'col-12 col-md-6 pr-0';
  const innerDiv = document.createElement('div');
  innerDiv.className = 'image-details-specifications d-flex flex-column';
  // image title text
  const inputFieldDiv = document.createElement('div');
  inputFieldDiv.className = 'd-flex flex-column';
  const labelField = document.createElement('label');
  labelField.id = CONSTANTS.LOREE_IMAGE_MODAL_LABEL_FOR_FILE_NAME;
  labelField.className = 'pb-1 mb-0';
  labelField.innerHTML = translate('modal.filename*');
  const imgInput = document.createElement('input');
  imgInput.id = CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT;
  imgInput.type = 'text';
  imgInput.required = false;
  imgInput.className = 'p-1';
  imgInput.autocomplete = 'off';
  const labelAlertField = document.createElement('label');
  labelAlertField.id = CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT;
  labelAlertField.className = 'pb-1 mb-0 text-danger d-none';
  labelAlertField.innerHTML = translate('modal.filenamealreadyexists');
  inputFieldDiv.appendChild(labelField);
  inputFieldDiv.appendChild(imgInput);
  inputFieldDiv.appendChild(labelAlertField);

  // img alt text
  const inputFieldDivAltText = document.createElement('div');
  inputFieldDivAltText.className = 'd-flex flex-column mt-2';
  const labelFieldForAlt = document.createElement('label');
  labelFieldForAlt.className = 'pb-1 mb-0';
  labelFieldForAlt.innerHTML = translate('modal.alttext');
  labelFieldForAlt.id = CONSTANTS.LOREE_IMAGE_MODAL_LABEL_FOR_ALT_TEXT;
  const altInput = document.createElement('textarea');
  altInput.id = CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT;
  altInput.className = 'p-1';
  altInput.required = true;
  altInput.style.outline = 'none';
  inputFieldDivAltText.appendChild(labelFieldForAlt);
  inputFieldDivAltText.appendChild(altInput);
  // decorative image option
  const decorativeImgParentDiv = document.createElement('div');
  decorativeImgParentDiv.className = 'pt-2';
  const labelFieldForDecorative = document.createElement('label');
  labelFieldForDecorative.className = 'pb-1 mb-0 h5 ml-2';
  labelFieldForDecorative.style.fontSize = '15px';
  labelFieldForDecorative.innerHTML = translate('modal.decorativeimage');
  labelFieldForDecorative.htmlFor = CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT;
  const altInputCheck = document.createElement('input');
  altInputCheck.type = 'checkbox';
  altInputCheck.id = CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT;
  altInputCheck.className = 'p-1';
  altInputCheck.onclick = () => handlingImageDecorativeOption();
  decorativeImgParentDiv.appendChild(altInputCheck);
  decorativeImgParentDiv.appendChild(labelFieldForDecorative);
  innerDiv.appendChild(inputFieldDiv);
  innerDiv.appendChild(inputFieldDivAltText);
  innerDiv.appendChild(decorativeImgParentDiv);
  detailRightContainer.appendChild(innerDiv);
  detailModalContainer.appendChild(detailRightContainer);
  return detailModalContainer;
};

const handlingImageDecorativeOption = (): void => {
  const decorativeImg = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
  ) as HTMLInputElement;
  const altTextLabel = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_LABEL_FOR_ALT_TEXT,
  ) as HTMLInputElement;
  const disableAltText = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
  ) as HTMLInputElement;
  const okButton = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  const imageTitle = getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT,
  ) as HTMLInputElement;
  if (decorativeImg.checked) {
    disableAltText.disabled = decorativeImg.checked;
    altTextLabel.innerHTML = translate('modal.alttext');
    okButton.disabled = imageTitle?.value.trim() === '';
  } else if (disableAltText.value === '') {
    disableAltText.disabled = decorativeImg.checked;
    altTextLabel.innerHTML = translate('modal.alttext');
    okButton.disabled = true;
  } else if (decorativeImg.checked && disableAltText.value === '') {
    disableAltText.disabled = decorativeImg.checked;
    altTextLabel.innerHTML = translate('modal.alttext');
    okButton.disabled = imageTitle?.value.trim() === '';
  } else {
    disableAltText.disabled = decorativeImg.checked;
    altTextLabel.innerHTML = translate('modal.alttext');
  }
};

export const appendModelContentFooter = (): HTMLElement => {
  const contentFooter = document.createElement('div');
  contentFooter.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER;
  contentFooter.className = 'd-flex flex-row flex-nowrap align-items-center';
  const footerButton = document.createElement('div');
  footerButton.style.margin = 'auto';
  const backButton = document.createElement('button');
  backButton.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON;
  backButton.innerHTML = translate('global.back');
  backButton.className = 'mx-1';

  const cancelButton = document.createElement('button');
  cancelButton.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_CANCEL_BUTTON;
  cancelButton.innerHTML = translate('global.cancel');
  cancelButton.className = 'mx-1';

  const addButton = document.createElement('button');
  addButton.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON;
  addButton.innerHTML = translate('global.add');
  addButton.className = 'editor-btn-primary mx-1';

  const applyButton = document.createElement('button');
  applyButton.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_APPLY_BUTTON;
  applyButton.innerHTML = translate('global.apply');
  applyButton.className = 'editor-btn-primary d-none mx-1';

  const editBackButton = document.createElement('button');
  editBackButton.id = CONSTANTS.LOREE_IMAGE_EDIT_MODAL_CONTENT_FOOTER_BACK_BUTTON;
  editBackButton.innerHTML = translate('global.back');
  editBackButton.className = 'd-none mx-1';

  footerButton.appendChild(backButton);
  footerButton.appendChild(editBackButton);
  footerButton.appendChild(cancelButton);
  footerButton.appendChild(addButton);
  footerButton.appendChild(applyButton);
  contentFooter.appendChild(footerButton);
  return contentFooter;
};

export const appendEditImageModal = (): HTMLElement => {
  const editModalContainer = document.createElement('DIV');
  editModalContainer.id = CONSTANTS.LOREE_IMAGE_EDIT_MODAL;
  editModalContainer.className =
    'd-none d-flex flex-column flex-md-row image-details-modal-wrapper edit-modal';

  const applyCircle = document.createElement('BUTTON');
  applyCircle.className = 'btn';
  applyCircle.innerHTML = `<img class="btn mt-3 ml-2" src="${circleShapeIcon}" style="width: 7vw">`;
  applyCircle.onclick = (): void => handleImageMask(true);

  const applyRectangle = document.createElement('BUTTON');
  applyRectangle.className = 'btn';
  applyRectangle.innerHTML = `<img class="btn mt-3 ml-2" src="${rectangleShapeIcon}" style="width: 7vw">`;
  applyRectangle.onclick = (): void => handleImageMask(false);

  const editOptions = document.createElement('DIV');
  editOptions.className = 'd-flex flex-column';
  editOptions.style.padding = '10px';
  editOptions.style.backgroundColor = '#ecedef';
  editOptions.style.border = '1px solid #c1bebe';

  const optionTray = document.createElement('DIV');
  optionTray.className = 'd-flex flex-column';

  const shapeIconOption = document.createElement('DIV');
  shapeIconOption.id = 'shapes';
  shapeIconOption.style.opacity = '0.5';
  shapeIconOption.innerHTML = `${ShapesIcon}<p style="font-size: small;text-align: center; margin-bottom: 0px;">${translate(
    'modal.croptoshape',
  )}</p>`;

  const propertyIconOption = document.createElement('DIV');
  propertyIconOption.style.opacity = '0.5';
  propertyIconOption.innerHTML = `<img class="btn mt-3 ml-2" src="${propertiesIcon}" style="width: 85%; cursor: unset" ><p style="font-size: small;text-align: center; margin-bottom: 0px;">${translate(
    'modal.properties',
  )}</p>`;

  const effectIconOption = document.createElement('DIV');
  effectIconOption.style.opacity = '0.5';
  effectIconOption.innerHTML = `<img class="btn mt-3 ml-2" src="${effectIcon}" style="width: 85%; cursor: unset" ><p style="font-size: small;text-align: center; margin-bottom: 0px;">${translate(
    'modal.effects',
  )}</p>`;

  const cropIconOption = document.createElement('DIV');
  cropIconOption.className = 'mt-3';
  cropIconOption.id = 'crop';
  cropIconOption.innerHTML = `${CropIcon}<p style="font-size: small;text-align: center; margin-bottom: 0px;">${translate(
    'modal.crop',
  )}</p>`;

  optionTray.appendChild(shapeIconOption);
  optionTray.appendChild(propertyIconOption);
  optionTray.appendChild(effectIconOption);
  optionTray.appendChild(cropIconOption);

  editOptions.appendChild(optionTray);

  const shapes = document.createElement('DIV');
  shapes.id = 'shape-option';
  shapes.className = 'd-flex flex-column';
  shapes.appendChild(applyCircle);
  shapes.appendChild(applyRectangle);

  const editProperties = document.createElement('DIV');
  editProperties.id = CONSTANTS.LOREE_SECOND_TRAY_EDIT_IMAGE;
  editProperties.className = 'd-flex flex-column';
  editProperties.style.padding = '10px';
  editProperties.style.backgroundColor = '#ecedef';
  editProperties.appendChild(shapes);

  const editModalPreviewContainer = document.createElement('DIV');
  editModalPreviewContainer.className = 'd-flex ml-2 w-100';
  editModalPreviewContainer.innerHTML = `
    <div id=${CONSTANTS.LOREE_EDIT_IMAGE_STYLES_PREVIEW} class="w-100 d-flex flex-column edit-image-shape-preview justify-content-center align-items-center" style='position: relative'></div>`;
  editModalContainer.appendChild(editOptions);
  editModalContainer.appendChild(editProperties);
  editModalContainer.appendChild(editModalPreviewContainer);

  return editModalContainer;
};

export const handleImageMask = (isCircle: boolean) => {
  new Cropper({
    originalSrc: imageInfo.url,
    isCircle: isCircle,
    crop: croppedImageSource.crop,
    imageRef: croppedImageSource.imageRef,
  }).handleCropperShape(isCircle);
};
