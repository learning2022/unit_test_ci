import CONSTANTS from '../../constant';
import {
  appendImageModalFilterContainer,
  appendModalContentBody,
  appendModalLeftContentHeader,
  appendVideoModalRightContainer,
  appendImageDetailModal,
  appendModelContentFooter,
} from './imageModalUI';
import { getElementById, getInputElementById } from '../../common/dom';

describe('#videoModalUI', () => {
  it('renders a ImageModalFilter with autofocus set', () => {
    const myFilterContainer = appendImageModalFilterContainer();
    const input = myFilterContainer.children.item(0);
    expect(input?.getAttribute('autofocus')).toBe('');
  });
  describe('#image uploader', () => {
    let myContainer: HTMLElement;
    beforeEach(() => {
      myContainer = appendVideoModalRightContainer({ uploadImage: true, insertbyurl: true });
    });
    test('image uploader UI', () => {
      expect(myContainer).toBeInstanceOf(HTMLDivElement);
    });
    test('the parent div contains class name as d-flex', () => {
      expect(myContainer?.className).toStrictEqual('d-flex align-items-center');
    });
    test('render the div with child nodes', () => {
      expect(myContainer.childNodes[0].childNodes.length).toEqual(2);
    });
  });
  describe('#append modal body', () => {
    test('image modal body', () => {
      expect(appendModalContentBody({ uploadImage: true, insertbyurl: true })).toBeInstanceOf(
        HTMLDivElement,
      );
    });
    test('#appendModalLeftContainer', () => {
      expect(appendModalLeftContentHeader()).toBeInstanceOf(HTMLDivElement);
    });
  });
});

describe('#ImageModalUI', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
    document.body.append(appendImageDetailModal());
    document.body.append(appendModelContentFooter());
  });
  test('Renders the image modal content', () => {
    expect(getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY)).toBeTruthy();
  });
  test('Empty file title content and empty alt values', () => {
    getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT)?.click();
    expect(getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON)).toBeDisabled();
  });
  test('Non-Empty file title content and empty alt values', () => {
    const imageTitle = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT);
    const imageAlt = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT);
    imageTitle.value = 'Dummy File Title';
    imageAlt.value = '';
    getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT)?.click();
    expect(
      getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON),
    ).not.toBeDisabled();
  });
  test('Empty file title content and non-empty alt values', () => {
    const imageTitle = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT);
    const imageAlt = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT);
    imageTitle.value = '';
    imageAlt.value = 'Dummy File Alternative';
    getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT)?.click();
    expect(getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON)).toBeDisabled();
  });
  test('Non-Empty file title content and non-empty alt values', () => {
    const imageTitle = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT);
    const imageAlt = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT);
    imageTitle.value = 'Dummy File Title';
    imageAlt.value = 'Dummy File Alternative';
    getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT)?.click();
    expect(
      getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON),
    ).not.toBeDisabled();
  });
});
