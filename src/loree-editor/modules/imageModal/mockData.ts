import CONSTANTS from '../../constant';

export const imageMockData = {
  canvasImages: {
    data: {
      canvasImages: `{
        "statusCode":200,
        "body":
          [{
            "id":185620,
            "uuid":"vvMSnPiRcKrkzhh34hbYFj536BM8EC00YpIC85I0",
            "folder_id":13053,
            "display_name":"investigation.png",
            "filename":"investigation.png",
            "upload_status":"success",
            "content-type":"image/png",
            "url":"https://crystaldelta.instructure.com/files/185620/download?download_frd=1&verifier=vvMSnPiRcKrkzhh34hbYFj536BM8EC00YpIC85I0","size":596,
            "created_at":"2021-02-16T11:56:14Z",
            "updated_at":"2021-06-05T05:56:05Z",
            "unlock_at":null,
            "locked":false,
            "hidden":false,
            "lock_at":null,
            "hidden_for_user":false,
            "thumbnail_url":"https://crystaldelta.instructure.com/images/thumbnails/185620/vvMSnPiRcKrkzhh34hbYFj536BM8EC00YpIC85I0","modified_at":"2021-02-16T11:56:14Z","mime_class":"image","media_entry_id":null,"locked_for_user":false}]
        }`,
    },
  },
};

export const replaceImage = {
  imageObjectUrl:
    'https://crystaldelta.instructure.com/files/230901/download?download_frd=1&verifier=Jlw19DOI1ynixvveDXPt6ttkpuO42fbAqByYMVHX ',
  imageAltText: 'Screenshot from 2021-08-02 17-30-01.png',
  selected:
    '<div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper element-highlight"><img alt="Screenshot from 2021-08-02 17-30-01.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="https://crystaldelta.instructure.com/files/230901/download?download_frd=1&amp;verifier=Jlw19DOI1ynixvveDXPt6ttkpuO42fbAqByYMVHX"></div>',
  editedimageobject:
    '<img class="loree-iframe-content-image" src="https://crystaldelta.instructure.com/files/230901/download?download_frd=1&amp;verifier=Jlw19DOI1ynixvveDXPt6ttkpuO42fbAqByYMVHX" data-type="https://crystaldelta.instructure.com/images/thumbnails/230901/Jlw19DOI1ynixvveDXPt6ttkpuO42fbAqByYMVHX" image-key="undefined" style="width: 100%; height: 100%; object-fit: scale-down;">',
  exceptedResult:
    '<img alt="Screenshot from 2021-08-02 17-30-01.png" style="width: auto; height: auto; max-width: 100%; max-height: 100%; border-width: 0px; border-style: solid; border-color: #000000;" class="loree-iframe-content-image" src="https://crystaldelta.instructure.com/files/230901/download?download_frd=1&amp;verifier=Jlw19DOI1ynixvveDXPt6ttkpuO42fbAqByYMVHX ">',
};

export const imagePreviewScreenModalMockData = (imageSource: string) => {
  return `<div id='${CONSTANTS.LOREE_IMAGE_DETAILS_PREVIEW}'><img src='${imageSource}' /></div><div id='${CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT}' class='d-none'></div>`;
};
