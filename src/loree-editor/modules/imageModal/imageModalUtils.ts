import CONSTANTS from '../../constant';
import { LmsAccess } from '../../../utils/lmsAccess';
import {
  listCanvasImages,
  listBbCourseImage,
  getImageUrl,
  getS3ImageList,
  getD2LS3Images,
  getStandaloneLoreeImages,
  getStandaloneMyImages,
} from '../../../utils/imageUpload';
import Base from '../../base';
import Cropper, { croppedImageSource } from './image-cropper';
import { getInputElementById } from '../../common/dom';
import { translate } from '../../../i18n/translate';
const lms = new LmsAccess();

export enum DropdownTranslationKeys {
  ASCENDING = 'modal.ascending',
  DESCENDING = 'modal.descending',
  RECENT = 'modal.recent',
  SIZE = 'modal.size',
}

export const DropdownValues = {
  ASCENDING: translate('modal.ascending'),
  DESCENDING: translate('modal.descending'),
  RECENT: translate('modal.recent'),
  SIZE: translate('modal.size'),
};

export let courseImageFileList: Array<{
  created_at: string;
  created: string;
  size: number;
  name: string;
  display_name: string;
}> = [];

export let loreeImageFileList: {
  key: string;
  name: string;
  lastModified: string;
  size: number;
  created_at: string;
  display_name: string;
}[] = [];

export let myImageFileList: {
  display_name: string;
  name: string;
  created_at: string;
  lastModified: string;
  size: Number;
}[] = [];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const selectedImageUrl: {
  objectUrl: string;
  thumbnailObjectUrl: string;
}[] = [];
let selectedTabId = '';
const baseClass = () => {
  const base = new Base();
  return base;
};

const sortComparision = (arg: { name: string }, arg2: { name: string }) => {
  const ProjectTitle1 = arg.name.toUpperCase();
  const ProjectTitle2 = arg2.name.toUpperCase();
  let data = 0;
  if (ProjectTitle1 > ProjectTitle2) {
    data = 1;
  } else if (ProjectTitle1 < ProjectTitle2) {
    data = -1;
  }
  return data;
};

const sortCanvasImageComparision = (
  arg: { display_name: string; name: string },
  arg2: { display_name: string; name: string },
) => {
  if (sessionStorage.getItem('domainName') === 'BB') {
    arg.display_name = arg.name;
    arg2.display_name = arg2.name;
  }
  const ProjectTitle1 = arg.display_name.toUpperCase();
  const ProjectTitle2 = arg2.display_name.toUpperCase();
  let data = 0;
  if (ProjectTitle1 > ProjectTitle2) {
    data = 1;
  } else if (ProjectTitle1 < ProjectTitle2) {
    data = -1;
  }
  return data;
};

export const selectedTab = () => {
  const canvasImagesTab = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_NAV_LMS_IMAGE,
  ) as HTMLElement;
  let selectedId = CONSTANTS.LOREE_IMAGE_LIST_CONTAINER;
  if (lms.getAccess() && canvasImagesTab?.classList?.contains('active')) {
    selectedId = CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER;
  }
  return selectedId;
};

const selectedImageHighlight = (selectedElement: HTMLElement) => {
  const id = selectedTab();
  const loreeImageListContainer = document.getElementById(id) as HTMLElement;
  if (loreeImageListContainer.childNodes) {
    const imageLists = Array.prototype.slice.call(loreeImageListContainer.childNodes);
    imageLists.forEach((imageList) => {
      imageList.classList.remove('active');
    });
  }
  selectedElement.classList.add('active');
  const okButton = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  if (okButton) {
    okButton.disabled = false;
  }
};

export const appendImageThumbail = (
  src: string,
  url: { name: string; key: string; thumbnailObjectUrl: string },
  fileType: string,
): string => {
  return `<img src="${
    selectedTabId === 'LOREE-IMAGES-TAB' || !url.thumbnailObjectUrl ? src : url.thumbnailObjectUrl
  }" title="${url.name}" image-key="${
    url.key
  }" data-type="${fileType}" class="loree-image-list-image-thumbnail" loading="lazy"></img><div class="loree-image-list-image-title text-truncate" data-toggle="tooltip" data-placement="top" title="${
    url.name
  }">${url.name}</div>
`;
};

const appendImageListToContainer = async (
  uploadUrl: { name: string; key: string; thumbnailObjectUrl: string },
  objectUrl: string,
) => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  const imageThumbnail = document.createElement('div');
  imageThumbnail.className = 'd-flex flex-column image-thumbnail';
  imageThumbnail.onclick = () => selectedImageHighlight(imageThumbnail);
  imageThumbnail.innerHTML = appendImageThumbail(objectUrl, uploadUrl, 'image');
  loreeImageListContainer.appendChild(imageThumbnail);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const appendCanvasImageListToContainer = async (uploadUrl: {
  name: string;
  key: string;
  objectUrl: string;
  thumbnailObjectUrl: string;
}) => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  const imageThumbnail = document.createElement('div');
  imageThumbnail.className = 'd-flex flex-column image-thumbnail';
  imageThumbnail.onclick = () => selectedImageHighlight(imageThumbnail);
  selectedImageUrl.push(uploadUrl);
  imageThumbnail.innerHTML = appendImageThumbail(
    uploadUrl.objectUrl,
    uploadUrl,
    uploadUrl.thumbnailObjectUrl !== null ? uploadUrl.thumbnailObjectUrl : uploadUrl.objectUrl,
  );
  loreeImageListContainer.appendChild(imageThumbnail);
};

const appendMyImageListToContainer = async (
  uploadUrl: { name: string; key: string; thumbnailObjectUrl: string },
  objectUrl: string,
) => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  const imageThumbnail = document.createElement('div');
  imageThumbnail.className = 'd-flex flex-column image-thumbnail';
  imageThumbnail.onclick = () => selectedImageHighlight(imageThumbnail);
  imageThumbnail.innerHTML = appendImageThumbail(objectUrl, uploadUrl, 'image');
  loreeImageListContainer.appendChild(imageThumbnail);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const listFilteredImages = (imageFilesList: any) => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeImageListContainer) {
    loreeImageListContainer.innerHTML = '';
    if (imageFilesList.length > 0) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      imageFilesList.forEach(async (imageFilesList: any): Promise<void> => {
        const objectUrl = await getImageUrl(imageFilesList.key);
        void appendImageListToContainer(
          imageFilesList,
          sessionStorage.getItem('domainName') === 'D2l' ? imageFilesList.src : objectUrl,
        );
      });
    } else {
      const loreeImageListContainer = document.getElementById(
        CONSTANTS.LOREE_IMAGE_LIST_CONTAINER,
      ) as HTMLElement;
      const imageThumbnail = document.createElement('div');
      imageThumbnail.className = 'm-auto';
      imageThumbnail.innerHTML = 'No results found';
      loreeImageListContainer.appendChild(imageThumbnail);
    }
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const listLmsFilteredImages = (imageFilesList: any, newImgUploderId: number) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const imagesList: any = [];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  imageFilesList.map((item: any) =>
    imagesList.push({
      id: item.id,
      name:
        sessionStorage.getItem('domainName') === 'canvas'
          ? item.display_name
          : sessionStorage.getItem('domainName') === 'BB' && item.name,
      objectUrl:
        sessionStorage.getItem('domainName') === 'canvas'
          ? item.url
          : sessionStorage.getItem('domainName') === 'BB' && item.downloadUrl,
      thumbnailObjectUrl: item.id === newImgUploderId ? item.url : item.thumbnail_url, // need to update
    }),
  );
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeImageListContainer) {
    loreeImageListContainer.innerHTML = '';
    if (imagesList.length > 0) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      imagesList.forEach((imageList: any): void => {
        void appendCanvasImageListToContainer(imageList);
      });
    } else {
      const loreeImageListContainer = document.getElementById(
        CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER,
      ) as HTMLElement;
      const imageThumbnail = document.createElement('div');
      imageThumbnail.className = 'm-auto';
      imageThumbnail.innerHTML = 'No results found';
      loreeImageListContainer.appendChild(imageThumbnail);
    }
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const listMyFilteredImages = (imageFilesList: any, level?: string) => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeImageListContainer) {
    loreeImageListContainer.innerHTML = '';
    if (imageFilesList.length > 0) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      imageFilesList.forEach(async (imageFilesList: any): Promise<void> => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const objectUrl: any = await getImageUrl(imageFilesList.key, level);
        void appendMyImageListToContainer(imageFilesList, objectUrl);
      });
    } else {
      const loreeImageListContainer = document.getElementById(
        CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER,
      ) as HTMLElement;
      const imageThumbnail = document.createElement('div');
      imageThumbnail.className = 'm-auto';
      imageThumbnail.innerHTML = 'No results found';
      loreeImageListContainer.appendChild(imageThumbnail);
    }
  }
};

export const handleImageSort = async (sortDropdownValue: string) => {
  if (loreeImageFileList.length === 0) {
    const lms = new LmsAccess();
    if (!lms.getAccess()) loreeImageFileList = await getStandaloneLoreeImages();
    else {
      loreeImageFileList =
        sessionStorage.getItem('domainName') === 'D2l'
          ? await getD2LS3Images()
          : await getS3ImageList();
    }
  }
  switch (sortDropdownValue) {
    case DropdownTranslationKeys.ASCENDING:
    case DropdownValues.ASCENDING:
      loreeImageFileList.sort(sortComparision);
      listFilteredImages(loreeImageFileList);
      break;
    case DropdownTranslationKeys.DESCENDING:
    case DropdownValues.DESCENDING:
      // eslint-disable-next-line no-case-declarations
      const sorting = loreeImageFileList.sort(sortComparision);
      // eslint-disable-next-line no-case-declarations
      const sortDescent = sorting.reverse();
      listFilteredImages(sortDescent);
      break;
    case DropdownTranslationKeys.RECENT:
    case DropdownValues.RECENT:
      loreeImageFileList.sort((arg: { lastModified: string }, arg2: { lastModified: string }) => {
        const dateOfArg: Number | Date = new Date(arg.lastModified);
        const dateOfArg2: Number | Date = new Date(arg2.lastModified);
        return Number(dateOfArg2) - Number(dateOfArg);
      });
      listFilteredImages(loreeImageFileList);
      break;
    case DropdownTranslationKeys.SIZE:
    case DropdownValues.SIZE:
      loreeImageFileList.sort((arg: { size: Number }, arg2: { size: Number }) => {
        const sizeOfArg: Number = arg.size;
        const sizeOfArg2: Number = arg2.size;
        return Number(sizeOfArg2) - Number(sizeOfArg);
      });
      listFilteredImages(loreeImageFileList);
      break;
  }
};

export const handleMyImageSort = async (sortDropdownValue: string) => {
  if (myImageFileList.length === 0) {
    myImageFileList = await getStandaloneMyImages();
  }
  switch (sortDropdownValue) {
    case DropdownTranslationKeys.ASCENDING:
    case DropdownValues.ASCENDING:
      myImageFileList.sort(sortComparision);
      listMyFilteredImages(myImageFileList, 'private');
      break;
    case DropdownTranslationKeys.DESCENDING:
    case DropdownValues.DESCENDING:
      // eslint-disable-next-line no-case-declarations
      const sorting = myImageFileList.sort(sortComparision);
      // eslint-disable-next-line no-case-declarations
      const sortDescent = sorting.reverse();
      listMyFilteredImages(sortDescent, 'private');
      break;
    case DropdownTranslationKeys.RECENT:
    case DropdownValues.RECENT:
      myImageFileList.sort((arg: { lastModified: string }, arg2: { lastModified: string }) => {
        const dateOfArg: Number | Date = new Date(arg.lastModified);
        const dateOfArg2: Number | Date = new Date(arg2.lastModified);
        return Number(dateOfArg2) - Number(dateOfArg);
      });
      listMyFilteredImages(myImageFileList, 'private');
      break;
    case DropdownTranslationKeys.SIZE:
    case DropdownValues.SIZE:
      myImageFileList.sort((arg: { size: Number }, arg2: { size: Number }) => {
        const sizeOfArg: Number = arg.size;
        const sizeOfArg2: Number = arg2.size;
        return Number(sizeOfArg2) - Number(sizeOfArg);
      });
      listMyFilteredImages(myImageFileList, 'private');
      break;
  }
};

// temporary function for D2l images
export const refreshD2lLoreeImage = async () => {
  loreeImageFileList = await getD2LS3Images();
  loreeImageFileList.sort((arg: { lastModified: string }, arg2: { lastModified: string }) => {
    const dateOfArg: Number | Date = new Date(arg.lastModified);
    const dateOfArg2: Number | Date = new Date(arg2.lastModified);
    return Number(dateOfArg2) - Number(dateOfArg);
  });
  await listFilteredImages(loreeImageFileList);
};

export const refreshCanvasImage = async (newImgUploderId: number) => {
  const lms = new LmsAccess();
  const courseDetails = lms.getDetails();
  if (sessionStorage.getItem('domainName') === 'canvas') {
    courseImageFileList = await listCanvasImages(courseDetails.courseId);
  } else if (sessionStorage.getItem('domainName') === 'BB') {
    courseImageFileList = await listBbCourseImage();
  }

  courseImageFileList.sort((arg: { created_at: string }, arg2: { created_at: string }) => {
    const dateOfArg: Number | Date = new Date(arg.created_at);
    const dateOfArg2: Number | Date = new Date(arg2.created_at);
    return Number(dateOfArg2) - Number(dateOfArg);
  });
  await listLmsFilteredImages(courseImageFileList, newImgUploderId);
};

export const refreshLoreeImage = async () => {
  loreeImageFileList = await getS3ImageList();
  loreeImageFileList.sort((arg: { created_at: string }, arg2: { created_at: string }) => {
    const dateOfArg: Number | Date = new Date(arg.created_at);
    const dateOfArg2: Number | Date = new Date(arg2.created_at);
    return Number(dateOfArg2) - Number(dateOfArg);
  });
  await listFilteredImages(loreeImageFileList);
};

export const refreshMyImage = async () => {
  myImageFileList = await getStandaloneMyImages();
  myImageFileList.sort((arg: { created_at: string }, arg2: { created_at: string }) => {
    const dateOfArg: Number | Date = new Date(arg.created_at);
    const dateOfArg2: Number | Date = new Date(arg2.created_at);
    return Number(dateOfArg2) - Number(dateOfArg);
  });
  await listMyFilteredImages(myImageFileList, 'private');
};

// handle course images section on lms
export const handleLmsImageSort = async (sortDropdownValue: string) => {
  const lms = new LmsAccess();
  const courseDetails = lms.getDetails();
  if (courseImageFileList.length === 0) {
    if (sessionStorage.getItem('domainName') === 'canvas') {
      courseImageFileList = await listCanvasImages(courseDetails.courseId);
    } else if (sessionStorage.getItem('domainName') === 'BB') {
      courseImageFileList = await listBbCourseImage();
    }
  }
  switch (sortDropdownValue) {
    case DropdownTranslationKeys.ASCENDING:
    case DropdownValues.ASCENDING:
      courseImageFileList.sort(sortCanvasImageComparision);
      listLmsFilteredImages(courseImageFileList, 0);
      break;
    case DropdownTranslationKeys.DESCENDING:
    case DropdownValues.DESCENDING:
      // eslint-disable-next-line no-case-declarations
      const sorting = courseImageFileList.sort(sortCanvasImageComparision);
      // eslint-disable-next-line no-case-declarations
      const sortDescent = sorting.reverse();
      listLmsFilteredImages(sortDescent, 0);
      break;
    case DropdownTranslationKeys.RECENT:
    case DropdownValues.RECENT:
      courseImageFileList.sort(
        (
          arg: { created: string; created_at: string },
          arg2: { created: string; created_at: string },
        ) => {
          if (sessionStorage.getItem('domainName') === 'BB') {
            arg.created_at = arg.created;
            arg2.created_at = arg2.created;
          }
          const dateOfArg: Number | Date = new Date(arg.created_at);
          const dateOfArg2: Number | Date = new Date(arg2.created_at);
          return Number(dateOfArg2) - Number(dateOfArg);
        },
      );
      listLmsFilteredImages(courseImageFileList, 0);
      break;
    case DropdownTranslationKeys.SIZE:
    case DropdownValues.SIZE:
      courseImageFileList.sort((arg: { size: Number }, arg2: { size: Number }) => {
        const sizeOfArg: Number = arg.size;
        const sizeOfArg2: Number = arg2.size;
        return Number(sizeOfArg2) - Number(sizeOfArg);
      });
      listLmsFilteredImages(courseImageFileList, 0);
      break;
  }
};

export const getAllImages = async () => {
  const addButton = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  addButton.disabled = true;
  selectedTabId = 'LOREE-IMAGES-TAB';
  const sortingName = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_FILTER_BUTTON,
  ) as HTMLElement;
  baseClass().initializeModalLoader('IMAGE', 'LOREE_IMAGES');
  void handleImageSort(sortingName?.innerHTML);
};

export const getStandaloneMyImageList = async () => {
  const sortingName = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_FILTER_BUTTON,
  ) as HTMLElement;
  baseClass().initializeModalLoader('IMAGE', 'MY_IMAGES');
  void handleMyImageSort(sortingName?.innerHTML);
};

export const getAllCanvasImages = async () => {
  const addButton = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  addButton.disabled = true;
  selectedTabId = 'CANVAS-IMAGES-TAB';
  const sortingName = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_FILTER_BUTTON,
  ) as HTMLElement;
  baseClass().initializeModalLoader('IMAGE', 'CANVAS_IMAGES');
  void handleLmsImageSort(sortingName?.innerHTML);
};

export const clearImageUrlInputFieldValue = (): void => {
  const errorMessage = document.getElementById(
    CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_ERROR_MESSAGE,
  );
  if (errorMessage) errorMessage.className = 'd-none';
  const imageUploaderInput = document.getElementById(
    CONSTANTS.LOREE_IMAGE_UPLOADER_INPUT,
  ) as HTMLInputElement;
  const youtubeInput = document.getElementById(
    CONSTANTS.LOREE_IMAGE_BYURL_INPUT,
  ) as HTMLInputElement;
  if (imageUploaderInput && youtubeInput) {
    imageUploaderInput.value = '';
    youtubeInput.style.border = '';
    youtubeInput.value = '';
  }
};

export const removeImageDisableClassForInputField = (): void => {
  const localUploadInput = document.getElementById(CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT);
  const youtubeInput = document.getElementById(
    CONSTANTS.LOREE_IMAGE_BYURL_INPUT,
  ) as HTMLInputElement;
  if (localUploadInput && youtubeInput) {
    localUploadInput.classList.remove('active');
    if (localUploadInput.parentElement && youtubeInput.parentElement) {
      localUploadInput.parentElement.classList.remove('disable');
      youtubeInput.parentElement.classList.remove('disable');
    }
  }
};

export const resetImageModalDetailScreen = (): void => {
  const imageAltText = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
  ) as HTMLInputElement;
  const imageUploadScreen = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY,
  ) as HTMLElement;
  const addButton = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
  ) as HTMLButtonElement;
  const modalLeftContainer = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY_LEFT_CONTAINER,
  ) as HTMLElement;
  const imagePreview = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_PREVIEW,
  ) as HTMLElement;
  const imagePreviewScreen = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY,
  ) as HTMLElement;
  const imageTitle = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT,
  ) as HTMLInputElement;
  const imageModalBackBtn = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON,
  ) as HTMLButtonElement;
  const decorativeImg = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
  ) as HTMLInputElement;

  const id = selectedTab();
  const loreeImageListContainer = document.getElementById(id) as HTMLElement;
  if (loreeImageListContainer.childNodes) {
    const imageLists = Array.prototype.slice.call(loreeImageListContainer.childNodes);
    imageLists.forEach((imageList) => {
      imageList.classList?.remove('active');
    });
  }
  imageUploadScreen?.classList.remove('d-none');
  modalLeftContainer?.classList.remove('disable');
  imagePreviewScreen?.classList.add('d-none');
  imageModalBackBtn?.classList.remove('d-inline-block');
  imageModalBackBtn?.classList.add('d-none');
  if (addButton || imagePreview || imageTitle) {
    addButton.disabled = true;
    imagePreview.innerHTML = '';
    imageTitle.value = '';
    imageAltText.value = '';
    imageAltText.disabled = false;
    decorativeImg.checked = false;
  }

  document
    .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON)
    ?.classList.remove('d-none');
  document
    .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_APPLY_BUTTON)
    ?.classList.add('d-none');
  document.getElementById(CONSTANTS.LOREE_IMAGE_EDIT_MODAL)?.classList.add('d-none');
  document
    .getElementById(CONSTANTS.LOREE_IMAGE_EDIT_MODAL_CONTENT_FOOTER_BACK_BUTTON)
    ?.classList.add('d-none');
  document
    .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
    ?.classList.remove('d-none');
  document
    .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
    ?.classList.add('d-inline-block');
  document.getElementById(CONSTANTS.LOREE_EDIT_IMAGE_MODAL_HEADER)?.classList.add('d-none');
  document.getElementById(CONSTANTS.LOREE_IMAGE_VIEW_MODAL)?.classList.remove('d-none');
  const imageNameAlertField = document.getElementById(
    CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT,
  );
  if (!imageNameAlertField?.classList.contains('d-none'))
    imageNameAlertField?.classList.add('d-none');
  croppedImageSource.src = '';
  croppedImageSource.isCircle = true;
  croppedImageSource.imageType = '';
  const ImageCropper = new Cropper({ originalSrc: '', isCircle: true, crop: '', imageRef: '' });
  ImageCropper.unRenderCropper();
};

const setImageModalDisplay = (): void => {
  const imageModalBackground = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_BACKGROUND,
  ) as HTMLElement;
  const imageModal = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_WRAPPER) as HTMLElement;
  imageModalBackground.style.display = 'block';
  imageModal.style.display = 'block';
};

const resetImageModalDisplay = (): void => {
  const imageModalBackground = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_BACKGROUND,
  ) as HTMLElement;
  const imageModal = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_WRAPPER) as HTMLElement;
  imageModalBackground.style.display = 'none';
  imageModal.style.display = 'none';
};

const clearImageListContainer = (): void => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeImageListContainer) loreeImageListContainer.innerHTML = '';
};

const clearMyImageListContainer = (): void => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeImageListContainer) loreeImageListContainer.innerHTML = '';
};

const clearCanvasImageListContainer = (): void => {
  const loreeImageListContainer = document.getElementById(
    CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER,
  ) as HTMLElement;
  if (loreeImageListContainer) loreeImageListContainer.innerHTML = '';
};

const resetImageSearchBar = (): void => {
  const imageModalCloseButtom = document.getElementById(
    CONSTANTS.LOREE_IMAGE_MODAL_FILTER_SEARCH_ICON,
  ) as HTMLElement;
  if (imageModalCloseButtom?.classList.contains('active')) {
    imageModalCloseButtom.click();
  }
};

const resetImageDesignSection = (): void => {
  const imageElement = document.getElementById('image');
  imageElement?.classList.remove('active');
};

// show image upload/insertion modal popup
export const showImageModal = (): void => {
  const lms = new LmsAccess();
  clearImageUrlInputFieldValue();
  removeImageDisableClassForInputField();
  resetImageDesignSection();
  resetImageModalDetailScreen();
  clearImageListContainer();
  resetImageSearchBar();
  setImageModalDisplay();
  baseClass().resetModalTabs('IMAGE');
  if (sessionStorage.getItem('domainName') !== 'D2l') {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    lms.getAccess() ? getAllCanvasImages() : getStandaloneMyImageList();
  } else {
    void getAllImages();
  }
  const errorMsg = getInputElementById(
    CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_FILESIZE_ERROR_MESSAGE,
  );
  if (errorMsg) errorMsg.className = 'd-none';
};

// hide image upload/insertion modal popup
export const hideImageModal = (): void => {
  clearImageUrlInputFieldValue();
  removeImageDisableClassForInputField();
  resetImageModalDetailScreen();
  resetImageModalDisplay();
  clearImageListContainer();
  clearMyImageListContainer();
  clearCanvasImageListContainer();
  resetImageSearchBar();
};
