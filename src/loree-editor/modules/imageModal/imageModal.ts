import { API, graphqlOperation } from 'aws-amplify';
import awsExports from '../../../aws-exports';
import Base from '../../base';
import CONSTANTS from '../../constant';
import { createFile } from '../../../graphql/mutations';
import { showFileUploadConfirmAlert, showFileTypeAlert } from '../../../utils/alert';
import {
  handleUploadImage,
  uploadLmsCourseImageToBB,
  uploadLmsLoreeImageToS3,
} from '../../../utils/imageUpload';
import { LmsAccess } from '../../../utils/lmsAccess';
import {
  appendModalContentHeader,
  appendModalContentHeaderDivider,
  appendModalContentBody,
  appendImageDetailModal,
  appendEditImageModal,
  appendModelContentFooter,
} from './imageModalUI';
import {
  handleImageSort,
  handleLmsImageSort,
  courseImageFileList,
  loreeImageFileList,
  myImageFileList,
  selectedTab,
  showImageModal,
  hideImageModal,
  clearImageUrlInputFieldValue,
  removeImageDisableClassForInputField,
  resetImageModalDetailScreen,
  refreshCanvasImage,
  refreshD2lLoreeImage,
  handleMyImageSort,
  refreshMyImage,
  selectedImageUrl,
} from './imageModalUtils';
import Cropper, { croppedImageSource } from './image-cropper';
import { FeatureInterface } from '../../interface';
import { isBB } from '../../../lmsConfig';
import { getInputElementById, getButtonById } from '../../common/dom';
import { translate } from '../../../i18n/translate';

let imageBlockInnerHtml: string;
let imageInsertionType: string;
let featureList: FeatureInterface | undefined;
export const imageInfo = {
  title: '' as string,
  url: '' as string,
  imageKey: '' as string,
};

export class ImageModal {
  initiate = (featuresList: FeatureInterface | undefined): void => {
    featureList = featuresList;
    const imageModalDialog = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_DIALOG);
    const imageModalContentTemplate: HTMLElement = document.createElement('div');
    imageModalContentTemplate.className = 'modal-content';
    imageModalContentTemplate.id = CONSTANTS.LOREE_IMAGE_MODAL_CONTENT;
    imageModalContentTemplate.appendChild(appendModalContentHeader());
    imageModalContentTemplate.appendChild(appendModalContentHeaderDivider());
    imageModalContentTemplate.appendChild(appendModalContentBody(featuresList));
    imageModalContentTemplate.appendChild(appendImageDetailModal());
    imageModalContentTemplate.appendChild(appendEditImageModal());
    imageModalContentTemplate.appendChild(appendModelContentFooter());
    if (imageModalDialog) {
      imageModalDialog.appendChild(imageModalContentTemplate);
    }
  };

  baseClass = (): Base => {
    const baseTest = new Base();
    return baseTest;
  };

  getImageBlockInnerHtml = (): string => {
    return imageBlockInnerHtml;
  };

  getImageBlockInsertionType = (): string => {
    return imageInsertionType;
  };

  imageModalEventHandlers = (): void => {
    const localUploadInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT,
    ) as HTMLInputElement;
    const uploadImageByUrlInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_BYURL_INPUT,
    ) as HTMLInputElement;
    const modalLeftContainer = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY_LEFT_CONTAINER,
    ) as HTMLElement;
    const cancelButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_CANCEL_BUTTON,
    ) as HTMLButtonElement;
    const backButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON,
    ) as HTMLButtonElement;
    const addButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    const imageTitle = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    const imageAltText = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
    ) as HTMLInputElement;
    const imageModalSearchBox = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_FILTER_SEARCH_BOX,
    ) as HTMLInputElement;
    const imageModalSearchIcon = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_FILTER_SEARCH_ICON,
    ) as HTMLElement;
    const imageModalFilterDropdown = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_FILTER_DROPDOWN,
    ) as HTMLElement;
    const imageModalFilterButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_FILTER_BUTTON,
    ) as HTMLButtonElement;
    const imageModalCloseButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_WRAPPER_CLOSE,
    ) as HTMLButtonElement;
    const thumbnailLabel = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_THUMBNAIL_LABEL,
    ) as HTMLInputElement;
    const thumbnailInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_THUMBNAIL_INPUT,
    ) as HTMLInputElement;
    const searchBoxIcon = document.getElementById('image-search-icon') as HTMLElement;
    const applyButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_APPLY_BUTTON,
    ) as HTMLButtonElement;
    const ediBackButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_EDIT_MODAL_CONTENT_FOOTER_BACK_BUTTON,
    ) as HTMLButtonElement;
    if (
      localUploadInput ||
      uploadImageByUrlInput ||
      modalLeftContainer ||
      cancelButton ||
      backButton ||
      addButton ||
      imageTitle ||
      imageModalSearchBox ||
      imageModalSearchIcon ||
      imageModalFilterDropdown ||
      imageModalFilterButton ||
      imageModalCloseButton ||
      thumbnailLabel ||
      thumbnailInput ||
      searchBoxIcon ||
      applyButton ||
      ediBackButton
    ) {
      this.imageModalSearchIconHandler(imageModalSearchIcon, imageModalSearchBox, searchBoxIcon);
      this.imageModalSearchBoxHandler(imageModalSearchBox);
      this.imageModalFilterDropdownHandler(imageModalFilterDropdown, imageModalFilterButton);
      this.imageModalIframeImageInputHandler(
        localUploadInput,
        uploadImageByUrlInput,
        modalLeftContainer,
        addButton,
      );
      this.imageModalAddButtonHandler(addButton);
      this.imageModalApplyButton(applyButton);
      this.imageModalEditBackButton(ediBackButton);
      this.imageModalImageAltInputHandler(imageAltText);
      this.imageModalImageTitleInputHandler(imageTitle);
      this.imageModalLeftContainerHandler(
        localUploadInput,
        uploadImageByUrlInput,
        modalLeftContainer,
        addButton,
      );
      this.imageModalBackButtonHandler(backButton);
      this.imageModalCancelButtonHandler(cancelButton, imageModalCloseButton);
      this.imageModalFileInputHandler(localUploadInput);
    }
  };

  imageModalSearchIconHandler = (
    imageModalSearchIcon: HTMLElement,
    imageModalSearchBox: HTMLInputElement,
    searchBoxIcon: HTMLElement,
  ): void => {
    if (imageModalSearchIcon) {
      imageModalSearchIcon.onclick = () =>
        this.imageSearchBox(imageModalSearchIcon, imageModalSearchBox, searchBoxIcon);
    }
  };

  imageSearchBox = (
    imageModalSearchIcon: HTMLElement,
    imageModalSearchBox: HTMLInputElement,
    searchBoxIcon: HTMLElement,
  ): void => {
    imageModalSearchBox.value = '';
    if (imageModalSearchIcon.classList.contains('active')) {
      imageModalSearchBox.style.display = 'none';
      imageModalSearchIcon.classList.remove('active');
      searchBoxIcon.setAttribute('width', '24px');
      searchBoxIcon.setAttribute('height', '20px');
    } else {
      imageModalSearchBox.style.display = 'block';
      imageModalSearchIcon.classList.add('active');
      searchBoxIcon.setAttribute('width', '15px');
      searchBoxIcon.setAttribute('height', '12px');
    }
  };

  imageModalSearchBoxHandler = (imageModalSearchBox: HTMLInputElement): void => {
    imageModalSearchBox.onkeyup = async () =>
      await this.handleSearchImage(imageModalSearchBox.value);
  };

  imageModalFilterDropdownHandler = (
    imageModalFilterDropdown: HTMLElement,
    imageModalFilterButton: HTMLButtonElement,
  ): void => {
    imageModalFilterDropdown.onclick = (event: Event) =>
      this.onFilterApply(event, imageModalFilterButton);
  };

  onFilterApply = (event: Event, imageModalFilterButton: HTMLButtonElement): void => {
    const addButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    addButton.disabled = true;
    const selectedFilter = (<HTMLButtonElement>event.target)?.innerText;
    imageModalFilterButton.innerText = selectedFilter;
    const target: string = this.baseClass().uploadTarget();
    if (target === 'STANDALONE') {
      void handleMyImageSort(selectedFilter);
    } else if (target === 'CANVAS' || target === 'BBLMSIMAGES') {
      this.baseClass().initializeModalLoader('IMAGE', 'CANVAS_IMAGES');
      void handleLmsImageSort(selectedFilter);
    } else {
      void handleImageSort(selectedFilter);
    }
  };

  imageModalIframeImageInputHandler = (
    localUploadInput: HTMLInputElement,
    uploadImageByUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (uploadImageByUrlInput)
      uploadImageByUrlInput.onclick = () =>
        this.imageModalByUrlInputHandler(
          localUploadInput,
          uploadImageByUrlInput,
          modalLeftContainer,
          addButton,
        );
  };

  imageModalByUrlInputHandler = (
    localUploadInput: HTMLInputElement,
    uploadImageByUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (localUploadInput.parentElement && uploadImageByUrlInput.parentElement) {
      localUploadInput.parentElement.classList.add('disable');
      uploadImageByUrlInput.parentElement.classList.remove('disable');
      localUploadInput.classList.remove('active');
    }
    modalLeftContainer.classList.add('disable');
    addButton.disabled = true;
    uploadImageByUrlInput.oninput = (e: Event): void => this.checkInputFieldValue(e);
    uploadImageByUrlInput.onfocus = (): void => clearImageUrlInputFieldValue();
  };

  imageModalAddButtonHandler = (addButton: HTMLButtonElement): void => {
    addButton.onclick = () => this.addButtonOnClickHandler();
  };

  imageModalApplyButton = (applyButon: HTMLButtonElement): void => {
    applyButon.onclick = () => this.EditFooterButtonOnClickHandler(true);
  };

  imageModalEditBackButton = (editBackButon: HTMLButtonElement): void => {
    editBackButon.onclick = () => this.EditFooterButtonOnClickHandler(false);
  };

  imageModalImageAltInputHandler = (imageAltText: HTMLInputElement): void => {
    imageAltText.oninput = (e: Event): void => {
      this.checkPreviewScreenInputFieldValue(e, imageAltText);
    };
  };

  imageModalImageTitleInputHandler = (imageTitleText: HTMLInputElement): void => {
    imageTitleText.oninput = (e: Event): void => {
      this.checkPreviewScreenTitleFieldValue(e, imageTitleText);
    };
  };

  checkPreviewScreenInputFieldValue = (e: Event, imageAltText: HTMLInputElement): void => {
    const okButton = getButtonById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON);
    const imageTitle = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT);
    if (
      (<HTMLInputElement>e.target).value.trim() === '' ||
      imageAltText?.value.trim() === '' ||
      imageTitle?.value.trim() === ''
    ) {
      okButton.disabled = true;
    } else {
      okButton.disabled = false;
    }
  };

  checkPreviewScreenTitleFieldValue = (e: Event, imageTitleText: HTMLInputElement): void => {
    const okButton = getButtonById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON);
    const imageAlt = getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT);
    const imageDecoration = getInputElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
    );
    if (
      imageTitleText?.value.trim() === '' ||
      (!imageDecoration.checked && imageAlt?.value.trim() === '')
    ) {
      okButton.disabled = true;
    } else {
      okButton.disabled = false;
    }
  };

  checkInputFieldValue = (e: Event): void => {
    const okButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if ((<HTMLInputElement>e.target).value === '') {
      okButton.disabled = true;
    } else {
      okButton.disabled = false;
    }
  };

  // replace a existing image
  replaceImageUploadHandler = () => {
    const selectedElement = this.baseClass().getSelectedElement();
    if (selectedElement?.id === CONSTANTS.LOREE_BANNER_IMAGE_IDENTITY) {
      imageInsertionType = 'REPLACE BANNER';
    } else {
      imageInsertionType = 'REPLACE';
    }
    if (featureList?.image) {
      showImageModal();
    }
  };

  imageModalLeftContainerHandler = (
    localUploadInput: HTMLInputElement,
    uploadImageByUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    modalLeftContainer.onclick = () =>
      this.leftContainerHandler(
        localUploadInput,
        uploadImageByUrlInput,
        modalLeftContainer,
        addButton,
      );
  };

  leftContainerHandler = (
    localUploadInput: HTMLInputElement,
    uploadImageByUrlInput: HTMLInputElement,
    modalLeftContainer: HTMLElement,
    addButton: HTMLButtonElement,
  ): void => {
    if (modalLeftContainer?.classList.contains('disable')) {
      if (localUploadInput?.parentElement && uploadImageByUrlInput?.parentElement) {
        localUploadInput?.parentElement?.classList.remove('disable');
        uploadImageByUrlInput?.parentElement?.classList.remove('disable');
        uploadImageByUrlInput.value = '';
        localUploadInput?.classList.remove('active');
      }
      modalLeftContainer?.classList.remove('disable');
      addButton.disabled = false;
    }
  };

  imageModalBackButtonHandler = (backButton: HTMLButtonElement): void => {
    backButton.onclick = () => this.backButtonHandler();
  };

  imageModalCancelButtonHandler = (
    cancelButton: HTMLButtonElement,
    imageModalCloseButton: HTMLButtonElement,
  ): void => {
    cancelButton.onclick = (): void => hideImageModal();
    imageModalCloseButton.onclick = (): void => hideImageModal();
  };

  imageModalFileInputHandler = (localUploadInput: HTMLInputElement): void => {
    if (localUploadInput) {
      localUploadInput.onclick = () => this.uploadImage();
    }
  };

  createImageTag = (
    selectedContainer: HTMLElement | null,
    fileType: string,
    objectUrl: HTMLImageElement,
    isEdited: boolean,
    imageKey: string,
    isUploadedByExternalUrl: boolean,
  ): void => {
    const imageTag: HTMLImageElement = document.createElement('img');
    imageTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE;
    imageTag.style.cssText = isEdited
      ? 'object-fit: scale-down; max-height:100%; height:100%;'
      : 'width: 100%; height:100%; object-fit: scale-down;';
    imageTag.src = (isEdited ? objectUrl.src : objectUrl) as string;
    imageTag.setAttribute('data-type', fileType);
    if (imageKey !== '') {
      imageTag.setAttribute('image-key', imageKey);
    }
    const imagepreviewWrapper = document.getElementById('loree-image-details-preview');
    if (imagepreviewWrapper) {
      imagepreviewWrapper.innerHTML = '';
    }
    if (isEdited) {
      document.getElementById('loree-image-details-preview')?.appendChild(imageTag);
    } else {
      selectedContainer?.prepend(imageTag);
    }
    if (!isUploadedByExternalUrl) {
      const imageEditButton = document.createElement('BUTTON');
      imageEditButton.id = CONSTANTS.LOREE_IMAGE_EDIT_BUTTON;
      imageEditButton.className = 'btn btn-primary';
      imageEditButton.style.position = 'absolute';
      imageEditButton.style.bottom = '2%';
      imageEditButton.innerText = translate('modal.editimage');
      imageEditButton.onclick = async () => await this.handleImageEdit(imageTag);
      const insertType = this.getImageBlockInsertionType();
      const innerHTML = this.getImageBlockInnerHtml();
      if (
        !(
          this.baseClass().checkDomainName() ||
          (insertType === 'NEW' && innerHTML === 'BANNER') ||
          insertType === 'REPLACE BANNER' ||
          innerHTML === 'BANNER'
        )
      ) {
        if (isEdited) {
          document.getElementById('loree-image-details-preview')?.appendChild(imageEditButton);
        } else {
          selectedContainer?.append(imageEditButton);
        }
      }
    }
  };

  getImageEditedClass = (HtmlImgContent: string) => {
    let image;
    const rex = /<img.*?class="(.*?)"[^>]+>/g;
    // eslint-disable-next-line no-unreachable-loop
    while ((image = rex.exec(HtmlImgContent))) {
      return image[1];
    }
  };

  handleImageEdit = async (imageTag: HTMLImageElement) => {
    const imageTitle = document.getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT);
    imageInfo.title = (<HTMLInputElement>imageTitle)?.value;
    imageInfo.url = imageTag.src;
    imageInfo.imageKey = imageTag.getAttribute('image-key') || '';

    document.getElementById(CONSTANTS.LOREE_IMAGE_EDIT_MODAL)?.classList.remove('d-none');

    document
      .getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY)
      ?.classList.add('d-none');

    document.getElementById(CONSTANTS.LOREE_EDIT_IMAGE_MODAL_HEADER)?.classList.remove('d-none');

    document.getElementById(CONSTANTS.LOREE_IMAGE_VIEW_MODAL)?.classList.add('d-none');

    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_APPLY_BUTTON)
      ?.classList.remove('d-none');

    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON)
      ?.classList.add('d-none');

    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.add('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.remove('d-inline-block');

    document
      .getElementById(CONSTANTS.LOREE_IMAGE_EDIT_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.remove('d-none');

    void new Cropper({
      originalSrc: imageTag.src,
      isCircle: croppedImageSource.isCircle,
      crop: croppedImageSource.crop,
      imageRef: croppedImageSource.imageRef,
    }).renderCropper();
  };

  imageFileUploadPreview = (
    fileName: string | null,
    fileType: string,
    objectUrl: _Any,
    isEdited: boolean,
    imageKey: _Any,
    isUploadedByExternalUrl: boolean = false,
  ): void => {
    const imageUploadScreen = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY);
    const imagePreviewScreen = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY,
    );
    const imagePreviewContainer: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_PREVIEW,
    );
    const imageApplyEditPreview: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_EDIT_IMAGE_STYLES_PREVIEW,
    );
    const decorativeImg = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
    ) as HTMLInputElement;
    const imageAltText = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
    ) as HTMLInputElement;
    const imageTitle = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    const imageModalBackBtn = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON,
    ) as HTMLButtonElement;
    imageTitle.value = fileName || '';

    if (isEdited) {
      imageTitle.value = '';
      this.createImageTag(
        imageApplyEditPreview,
        fileType,
        objectUrl,
        isEdited,
        imageKey || '',
        isUploadedByExternalUrl,
      );
    } else {
      imageTitle.value = fileName || '';
      this.createImageTag(
        imagePreviewContainer,
        fileType,
        objectUrl,
        isEdited,
        imageKey || '',
        isUploadedByExternalUrl,
      );
    }
    imageUploadScreen?.classList.add('d-none');
    imagePreviewScreen?.classList.remove('d-none');
    imageModalBackBtn?.classList.remove('d-none');
    imageModalBackBtn?.classList.add('d-inline-block');
    const okButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if ((!decorativeImg.checked && imageAltText.value === '') || imageTitle.value === '') {
      okButton.disabled = true;
    } else {
      okButton.disabled = false;
    }
  };

  saveImageDetails = async (
    fileType: string | null,
    imageDetails: { key: string; size: string },
  ): Promise<void> => {
    const key = imageDetails.key;
    const imageName = imageDetails.key.slice(10);
    const size = imageDetails.size;
    const imageInput = {
      name: imageName,
      size: size,
      location: {
        bucket: awsExports.aws_user_files_s3_bucket,
        key: key,
        region: awsExports.aws_appsync_region,
      },
      type: fileType,
    };
    await API.graphql(graphqlOperation(createFile, { input: imageInput }));
  };

  appendImageSrcToSelectedInnerHtml = (
    innerHTML: HTMLElement | string | null,
    imageTitle: string,
    imageObjectUrl: string,
    imageAltText: string,
    selectedElement: HTMLElement | null,
    editedImageElement: HTMLElement,
  ) => {
    const imageTagDiv: HTMLElement = document.createElement('div');
    imageTagDiv.innerHTML = innerHTML as string;
    const imageTag = imageTagDiv.getElementsByTagName('img')[0];
    imageTag.src = imageObjectUrl;
    if (editedImageElement?.classList[2]) {
      imageTag.classList.add(editedImageElement.classList[2]);
    }
    imageTag.alt = imageAltText;
    this.baseClass().appendImageToSelectedColumn(imageTagDiv.innerHTML, selectedElement);
  };

  imageUploadOnChangeHandler = async (event: Event) => {
    const fileTarget = event.target as HTMLInputElement | null;
    if (fileTarget) {
      const files = fileTarget.files as FileList;
      if (files.length > 0) {
        const file: File = files[0];
        const fileName = file.name;
        const fileType = file.type;
        if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
          showFileTypeAlert('image');
        } else if (
          file.size >= CONSTANTS.LOREE_IMAGE_UPLOADER_VALIDATION_SIZE_RESTRICTION_BB &&
          isBB()
        ) {
          const errorMsg = getInputElementById(
            CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_FILESIZE_ERROR_MESSAGE,
          );
          if (errorMsg) errorMsg.className = 'd-block';
        } else if (file.size > CONSTANTS.LOREE_IMAGE_UPLOADER_VALIDATION_SIZE) {
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          showFileUploadConfirmAlert('image', async (upload) => {
            if (upload) {
              const previewUrl: string = URL.createObjectURL(file);
              this.imageFileUploadPreview(fileName, fileType, previewUrl, false, false);
            }
          });
        } else {
          const previewUrl: string = URL.createObjectURL(file);
          this.imageFileUploadPreview(fileName, fileType, previewUrl, false, false);
        }
      }
      this.baseClass().removeSelectedElementSection();
    }
  };

  uploadImage = (): void => {
    const localUploadInput = document.getElementById(CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT);
    const uploadImageByUrlInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_BYURL_INPUT,
    ) as HTMLInputElement;
    const modalLeftContainer = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY_LEFT_CONTAINER,
    );
    const addButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if (localUploadInput && uploadImageByUrlInput && modalLeftContainer && addButton) {
      if (localUploadInput.parentElement && uploadImageByUrlInput.parentElement) {
        localUploadInput.parentElement.classList.remove('disable');
        uploadImageByUrlInput.parentElement.classList.add('disable');
        const errorMessage = document.getElementById(
          CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_ERROR_MESSAGE,
        );
        if (errorMessage) {
          errorMessage.className = 'd-none';
        }
        uploadImageByUrlInput.style.border = '';
        localUploadInput.classList.add('active');
      }
      modalLeftContainer.classList.add('disable');
      addButton.disabled = true;
    }
    const imageUploader = document.getElementById(CONSTANTS.LOREE_IMAGE_UPLOADER_INPUT);
    if (imageUploader) {
      imageUploader.click();
      imageUploader.onchange = async (event) => await this.imageUploadOnChangeHandler(event);
    }
  };

  insertByUrlImage = (isEdited: boolean): void => {
    const uploadImageByUrlInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_BYURL_INPUT,
    ) as HTMLInputElement;
    const imageUploadScreen = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY);
    const imagePreviewScreen = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY,
    );
    const objectUrl = uploadImageByUrlInput.value;
    const errorMessage = document.getElementById(
      CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_ERROR_MESSAGE,
    );
    if (errorMessage) {
      errorMessage.className = 'd-none';
    }
    const validation = objectUrl.match(/^https.*\.(jpeg|jpg|gif|png)$/);
    if (validation !== null || objectUrl.match('http') !== null) {
      this.imageFileUploadPreview(
        '',
        'image',
        isEdited ? croppedImageSource : objectUrl,
        isEdited,
        false,
        true,
      );
      imageUploadScreen?.classList.add('d-none');
      imagePreviewScreen?.classList.remove('d-none');
    } else {
      const errorMessage = document.getElementById(
        CONSTANTS.LOREE_IMAGE_LOCAL_UPLOAD_INPUT_ERROR_MESSAGE,
      );
      uploadImageByUrlInput.style.border = '1px solid red';
      if (errorMessage) {
        errorMessage.className = 'd-block';
      }
    }
    const okButton = document.getElementById(
      CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
    ) as HTMLButtonElement;
    if (okButton) {
      okButton.disabled = true;
    }
  };

  checkImageModalScreenVisiblity = () => {
    let screenType;
    const imageUploadScreen = document.getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_BODY);
    const imagePreviewScreen = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY,
    ) as HTMLElement;
    if (!imageUploadScreen?.classList.contains('d-none')) {
      screenType = 'imageInputScreen';
    } else if (!imagePreviewScreen?.classList.contains('d-none')) {
      screenType = 'imagePreviewScreen';
    }
    return screenType;
  };

  backButtonHandler = (): void => {
    const screenType = this.checkImageModalScreenVisiblity();
    switch (screenType) {
      case 'imageInputScreen':
        hideImageModal();
        break;
      case 'imagePreviewScreen':
        clearImageUrlInputFieldValue();
        removeImageDisableClassForInputField();
        resetImageModalDetailScreen();
        break;
    }
  };

  checkImageInputType = () => {
    let inputType;
    const uploadImageByUrlInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_BYURL_INPUT,
    ) as HTMLInputElement;
    if (uploadImageByUrlInput.value !== '') {
      inputType = 'BYURL';
    } else {
      inputType = 'LOCAL';
    }
    return inputType;
  };

  insertLocalImage = (isEdited: boolean): void => {
    const selectedId: string = selectedTab();
    const loreeImageListContainer = document.getElementById(selectedId) as HTMLElement;
    const imageThumbnailList = Array.prototype.slice.call(loreeImageListContainer.childNodes);
    const imageActiveCheck = (image: HTMLElement) => image?.classList?.contains('active');
    const isImageActive = imageThumbnailList.some(imageActiveCheck);
    if (isImageActive) {
      imageThumbnailList.forEach((imageThumbnail) => {
        if (imageThumbnail.classList.contains('active')) {
          let objectUrl = imageThumbnail.children[0].src;
          if (
            selectedId !== 'loree-image-list-container' &&
            objectUrl.includes('/images/thumbnails')
          ) {
            const selectedImage = selectedImageUrl.filter(
              (image: { thumbnailObjectUrl: string }) => image.thumbnailObjectUrl === objectUrl,
            );
            objectUrl = selectedImage[0].objectUrl;
          }
          const fileName = imageThumbnail.children[0].title;
          const fileType = imageThumbnail.children[0].getAttribute('data-type');
          const imageKey = imageThumbnail.children[0].getAttribute('image-key');
          this.imageFileUploadPreview(
            fileName,
            fileType,
            isEdited ? croppedImageSource : objectUrl,
            isEdited,
            imageKey,
          );
        }
      });
    } else
      this.imageFileUploadPreview(
        imageInfo.title,
        'image',
        isEdited ? croppedImageSource : imageInfo.url,
        isEdited,
        false,
      );
  };

  appendImageToPreviewScreen = (isEdited: boolean): void => {
    const imageInputType = this.checkImageInputType();
    switch (imageInputType) {
      case 'BYURL':
        this.insertByUrlImage(isEdited);
        break;
      case 'LOCAL':
        this.insertLocalImage(isEdited);
        break;
    }
  };

  checkAppendElementType = () => {
    let selectedBlock;
    const insertType = this.getImageBlockInsertionType();
    if (insertType === 'NEW') {
      selectedBlock = 'IMAGE';
    } else if (insertType === 'REPLACE') {
      const selectedElement = this.baseClass().getSelectedElement();
      const isImageSelected = selectedElement?.getElementsByTagName('img')[0] as HTMLImageElement;
      const isVideoSelected = selectedElement?.getElementsByTagName('video')[0] as HTMLVideoElement;
      if (isImageSelected) {
        selectedBlock = 'IMAGE';
      } else if (isVideoSelected) {
        selectedBlock = 'VIDEO';
      } else {
        selectedBlock = 'PREVIEW_ERROR';
      }
    } else {
      selectedBlock = 'IMAGE';
    }
    return selectedBlock;
  };

  // append image based on insert type
  appendImageTypeBased = (
    insertType: string,
    innerHTML: HTMLElement | string | null,
    imageTitle: string,
    previewImage: HTMLImageElement,
    imageAltText: string,
    replaceSrcUrl: string,
    selectedElement: HTMLElement | null,
  ) => {
    const img = new Image();
    if (insertType === 'NEW') {
      if (innerHTML === 'BANNER') {
        img.onload = () => {
          img.height < CONSTANTS.LOREE_BANNER_IMAGEHEIGHT_VALIDATION
            ? this.setBannerToSelectedHtmlValidatingHeight(
                previewImage.src,
                imageTitle,
                imageAltText,
                selectedElement,
              )
            : this.showBannerImageModal(replaceSrcUrl, imageTitle, imageAltText, selectedElement);
        };
        img.src = previewImage.src;
      } else {
        this.appendImageSrcToSelectedInnerHtml(
          innerHTML,
          imageTitle,
          replaceSrcUrl,
          imageAltText,
          selectedElement,
          previewImage,
        );
      }
    } else if (insertType === 'REPLACE') {
      this.appendReplaceImageSrcToSelectedImage(
        replaceSrcUrl,
        imageAltText,
        selectedElement,
        previewImage,
      );
    } else if (insertType === 'REPLACE BANNER') {
      img.onload = () => {
        img.height < CONSTANTS.LOREE_BANNER_IMAGEHEIGHT_VALIDATION
          ? this.appendReplaceBannerImageSrcToSelectedImage(
              replaceSrcUrl,
              imageAltText,
              selectedElement,
            )
          : this.replaceShowBannerImageModal(
              replaceSrcUrl,
              imageTitle,
              imageAltText,
              selectedElement,
            );
      };
      img.src = previewImage.src;
    }
  };

  uploadLmsLoreeImages = async (
    imageData: File | object,
    insertType: string,
    innerHTML: HTMLElement | string,
    imageTitle: string,
    previewImage: HTMLImageElement,
    imageAltText: string,
    selectedElement: HTMLElement | null,
  ) => {
    const srcUrl: string = await uploadLmsLoreeImageToS3(imageData, `${imageTitle}`);
    if (!srcUrl) return;
    this.appendImageTypeBased(
      insertType,
      innerHTML,
      imageTitle,
      previewImage,
      imageAltText,
      srcUrl,
      selectedElement,
    );
    void refreshD2lLoreeImage();
  };

  appendImageFileToSelectedBlock = async (
    imagePreviewScreen: HTMLElement,
    imageTitle: string,
    imageAltText: string,
    selectedElement: HTMLElement | null,
  ) => {
    let id = 0;
    const lms = new LmsAccess();
    let target = '';
    const insertType = this.getImageBlockInsertionType();
    const innerHTML = this.getImageBlockInnerHtml();
    const previewImage = imagePreviewScreen?.getElementsByTagName('img')[0];
    const fileType: string | null = previewImage.getAttribute('data-type');
    const imageUploaderInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_UPLOADER_INPUT,
    ) as HTMLInputElement;
    const imageFiles = imageUploaderInput.files as FileList;
    let imageData: Object;
    target = this.baseClass().uploadTarget();
    if (imageFiles.length > 0) {
      const ext = imageFiles[0].name.substr(imageFiles[0].name.lastIndexOf('.') + 1);
      if (previewImage.src.includes('data:image')) {
        const getImageFile: Object = await this.srcToFile(
          previewImage.src,
          imageTitle,
          `image/${ext}`,
        );
        imageData = getImageFile;
      } else imageData = imageFiles[0];
      // temporary condition for D2l and BB images
      if (target === 'D2LLOREEIMAGES') {
        void this.uploadLmsLoreeImages(
          imageData,
          insertType,
          innerHTML,
          imageTitle,
          previewImage,
          imageAltText,
          selectedElement,
        );
      }
      if (target === 'BBLMSIMAGES' || sessionStorage.getItem('domainName') === 'BB') {
        const srcUrl = await uploadLmsCourseImageToBB(imageData, imageTitle, 'uploadLocal');
        this.appendImageTypeBased(
          insertType,
          innerHTML,
          imageTitle,
          previewImage,
          imageAltText,
          srcUrl,
          selectedElement,
        );
      } else {
        lms.getAccess() && (target = 'CANVAS');
        !lms.getAccess() && (target = '');
        const srcUrl = await handleUploadImage(imageData, `${imageTitle}`, target);
        if (!srcUrl) {
          return;
        }
        target === 'LOREE' && this.saveImageDetails(fileType, srcUrl[0]);
        this.appendImageTypeBased(
          insertType,
          innerHTML,
          imageTitle,
          previewImage,
          imageAltText,
          srcUrl[1],
          selectedElement,
        );
        id = srcUrl[2];
      }
    } else {
      const lms = new LmsAccess();
      if (target === 'LOREE' || target === 'BBLOREEIMAGES') {
        const objectUrl = previewImage.src;
        if (objectUrl.includes('data:image')) {
          let croppedImageExtenstion = croppedImageSource.imageType.split('/');
          croppedImageExtenstion = croppedImageExtenstion[croppedImageExtenstion.length - 1];
          const getImageFile = await this.srcToFile(
            previewImage.src,
            imageTitle,
            `image/${croppedImageExtenstion}`,
          );
          lms.getAccess() && (target = 'CANVAS');
          const srcUrl = await handleUploadImage(
            getImageFile,
            `${imageTitle}.${croppedImageExtenstion}`,
            target,
          );
          if (!srcUrl) return;
          target === 'LOREE' && this.saveImageDetails(fileType, srcUrl[0]);
          this.appendImageTypeBased(
            insertType,
            innerHTML,
            imageTitle,
            previewImage,
            imageAltText,
            srcUrl[1],
            selectedElement,
          );
        } else {
          const file = [];
          await file.push({
            type: fileType,
            src: objectUrl,
          });
          if (!objectUrl) return;
          this.appendImageTypeBased(
            insertType,
            innerHTML,
            imageTitle,
            previewImage,
            imageAltText,
            objectUrl,
            selectedElement,
          );
        }
      } else {
        const img = new Image();
        if (insertType === 'NEW') {
          if (innerHTML === 'BANNER') {
            img.onload = () => {
              img.height < CONSTANTS.LOREE_BANNER_IMAGEHEIGHT_VALIDATION
                ? this.setBannerToSelectedHtmlValidatingHeight(
                    previewImage.src,
                    imageTitle,
                    imageAltText,
                    selectedElement,
                  )
                : this.showBannerImageModal(
                    previewImage.src,
                    imageTitle,
                    imageAltText,
                    selectedElement,
                  );
            };
            img.src = previewImage.src;
          } else {
            if (previewImage.src.includes('data:image')) {
              let croppedImageExtenstion = croppedImageSource.imageType.split('/');
              croppedImageExtenstion = croppedImageExtenstion[croppedImageExtenstion.length - 1];
              const getImageFile = await this.srcToFile(
                previewImage.src,
                imageTitle,
                `image/${croppedImageExtenstion}`,
              );
              lms.getAccess() && (target = 'CANVAS');
              const srcUrl = await handleUploadImage(getImageFile, `${imageTitle}`, target);
              if (!srcUrl) return;
              target === 'LOREE' && this.saveImageDetails(fileType, srcUrl[0]);
              this.appendImageTypeBased(
                insertType,
                innerHTML,
                imageTitle,
                previewImage,
                imageAltText,
                srcUrl[1],
                selectedElement,
              );
            } else {
              this.appendImageSrcToSelectedInnerHtml(
                innerHTML,
                imageTitle,
                previewImage.src,
                imageAltText,
                selectedElement,
                previewImage,
              );
            }
          }
        } else if (insertType === 'REPLACE') {
          if (previewImage.src.includes('data:image')) {
            let croppedImageExtenstion = croppedImageSource.imageType.split('/');
            croppedImageExtenstion = croppedImageExtenstion[croppedImageExtenstion.length - 1];
            const getImageFile = await this.srcToFile(
              previewImage.src,
              imageTitle,
              `image/${croppedImageExtenstion}`,
            );
            lms.getAccess() && (target = 'CANVAS');
            const srcUrl = await handleUploadImage(getImageFile, `${imageTitle}`, target);
            if (!srcUrl) return;
            target === 'LOREE' && this.saveImageDetails(fileType, srcUrl[0]);
            this.appendImageTypeBased(
              insertType,
              innerHTML,
              imageTitle,
              previewImage,
              imageAltText,
              srcUrl[1],
              selectedElement,
            );
          } else {
            this.appendReplaceImageSrcToSelectedImage(
              previewImage.src,
              imageAltText,
              selectedElement,
              previewImage,
            );
          }
        } else if (insertType === 'REPLACE BANNER') {
          img.onload = () => {
            img.height < CONSTANTS.LOREE_BANNER_IMAGEHEIGHT_VALIDATION
              ? this.appendReplaceBannerImageSrcToSelectedImage(
                  previewImage.src,
                  imageAltText,
                  selectedElement,
                )
              : this.replaceShowBannerImageModal(
                  previewImage.src,
                  imageTitle,
                  imageAltText,
                  selectedElement,
                );
          };
          img.src = previewImage.src;
        }
      }
    }
    (sessionStorage.getItem('domainName') === 'canvas' ||
      sessionStorage.getItem('domainName') === 'BB') &&
      refreshCanvasImage(id);
    if (!lms.getAccess()) void refreshMyImage();
    this.baseClass().removeSelectedElementSection();
    hideImageModal();
  };

  srcToFile = async (src: string, fileName: string, mimeType: string) => {
    return await fetch(src)
      .then(async function (res) {
        return await res.arrayBuffer();
      })
      .then(function (buf) {
        return new File([buf], fileName, { type: mimeType });
      });
  };

  appendImageFileToSelectedVideoBlock = async (
    imagePreviewScreen: HTMLElement,
    imageTitle: string,
    selectedElement: HTMLElement | null,
  ) => {
    const previewImage = imagePreviewScreen?.getElementsByTagName('img')[0];
    const imageUploaderInput = document.getElementById(
      CONSTANTS.LOREE_IMAGE_UPLOADER_INPUT,
    ) as HTMLInputElement;
    const imageFiles = imageUploaderInput.files as FileList;
    const target: string = this.baseClass().uploadTarget();
    if (imageFiles.length > 0) {
      const fileType = imageFiles[0].type;
      const ext = imageFiles[0].name.substr(imageFiles[0].name.lastIndexOf('.') + 1);
      const srcUrl = await handleUploadImage(imageFiles[0], `${imageTitle}.${ext}`, target);
      if (!srcUrl) return;
      if (target === 'LOREE') void this.saveImageDetails(fileType, srcUrl[0]);
      this.appendImageAsThumbnailToSelectedVideo(srcUrl[1], selectedElement);
    } else {
      this.appendImageAsThumbnailToSelectedVideo(previewImage.src, selectedElement);
    }
  };

  appendImageToSelectedBlock = (): void => {
    const imagePreviewScreen = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_CONTENT_BODY,
    ) as HTMLElement;
    const imageTitle = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    const imageAltText = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
    ) as HTMLInputElement;
    if (imageTitle.value === '') {
      imageTitle.value = imageAltText.value;
    }
    const selectedElement = this.baseClass().getSelectedElement();
    const appendElementType = this.checkAppendElementType();
    switch (appendElementType) {
      case 'IMAGE':
        void this.appendImageFileToSelectedBlock(
          imagePreviewScreen,
          imageTitle.value,
          imageAltText.value,
          selectedElement,
        );
        break;
      case 'VIDEO':
        void this.appendImageFileToSelectedVideoBlock(
          imagePreviewScreen,
          imageTitle.value,
          selectedElement,
        );
        break;
    }
  };

  addButtonOnClickHandler = () => {
    const screenType = this.checkImageModalScreenVisiblity();
    switch (screenType) {
      case 'imageInputScreen':
        this.appendImageToPreviewScreen(false);
        break;
      case 'imagePreviewScreen':
        // eslint-disable-next-line no-case-declarations
        const isImageNameExists = this.isImageNameAlreadyExistsInCanvas();
        // eslint-disable-next-line no-case-declarations
        const isCroppedImageSource = document
          ?.getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_PREVIEW)
          ?.getElementsByTagName('img')[0].src;
        // eslint-disable-next-line no-case-declarations
        const imageNameAlertField = document.getElementById(
          CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT,
        );
        if (
          isImageNameExists &&
          (isCroppedImageSource?.includes('blob:') || isCroppedImageSource?.startsWith('data:'))
        ) {
          if (imageNameAlertField?.classList.contains('d-none'))
            imageNameAlertField?.classList.remove('d-none');
        } else {
          if (!imageNameAlertField?.classList.contains('d-none'))
            imageNameAlertField?.classList.add('d-none');
          this.appendImageToSelectedBlock();
          hideImageModal();
        }
        break;
    }
  };

  isImageNameAlreadyExistsInCanvas = () => {
    const imageTitle = document.getElementById(
      CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT,
    ) as HTMLInputElement;
    const imageNameCheck = (image: { display_name: string }) =>
      image.display_name === imageTitle.value;
    const isImageActive = courseImageFileList.some(imageNameCheck);
    return isImageActive;
  };

  EditFooterButtonOnClickHandler = (isEdited: boolean) => {
    document.getElementById(CONSTANTS.LOREE_IMAGE_EDIT_MODAL)?.classList.add('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON)
      ?.classList.remove('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.remove('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_APPLY_BUTTON)
      ?.classList.add('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_EDIT_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.add('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.remove('d-none');
    document
      .getElementById(CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_BACK_BUTTON)
      ?.classList.add('d-inline-block');
    document.getElementById(CONSTANTS.LOREE_EDIT_IMAGE_MODAL_HEADER)?.classList.add('d-none');
    document.getElementById(CONSTANTS.LOREE_IMAGE_VIEW_MODAL)?.classList.remove('d-none');
    this.appendImageToPreviewScreen(isEdited);
  };

  handleSearchImage = async (searchText: string) => {
    const target: string = this.baseClass().uploadTarget();
    if (target === 'CANVAS' || target === 'BBLMSIMAGES') {
      void this.handleImageModalSearch(searchText, 'LMS');
    } else if (target === 'STANDALONE') {
      void this.handleImageModalSearch(searchText, 'STANDALONE');
    } else {
      void this.handleImageModalSearch(searchText, 'LoreeImage');
    }
  };

  // upload a new image
  imageUploadHandler = (innerHTML: string): void => {
    this.baseClass().hideSubSidebar();
    this.baseClass().hideCloseElementButtonToSelectedElement();
    this.baseClass().collapseElementSection();
    this.baseClass().disableElementSection();
    imageBlockInnerHtml = innerHTML;
    imageInsertionType = 'NEW';
    showImageModal();
  };

  handleImageModalSearch = async (searchText: string, sectionType: string) => {
    const parentImageList: _Any = document.getElementsByClassName('image-thumbnail');
    const parentWrapper =
      sectionType === 'LoreeImage'
        ? CONSTANTS.LOREE_IMAGE_LIST_CONTAINER
        : sectionType === 'LMS'
        ? CONSTANTS.LOREE_LMS_IMAGE_LIST_CONTAINER
        : CONSTANTS.LOREE_MY_IMAGE_LIST_CONTAINER;
    const ImageList =
      sectionType === 'LoreeImage'
        ? loreeImageFileList
        : sectionType === 'LMS'
        ? courseImageFileList
        : myImageFileList;
    if (ImageList.length > 0) {
      const noResults = document.getElementById(`${parentWrapper}-no-results`);
      if (noResults) noResults.className = 'd-none';
      const imageSearchData = [];
      if (searchText.length > 0) {
        for (const data of ImageList) {
          const fileName =
            sectionType === 'LMS' && sessionStorage.getItem('domainName') === 'canvas'
              ? data.display_name
              : data.name;
          if (fileName.toUpperCase().includes(searchText.toUpperCase())) {
            imageSearchData.push(fileName);
          }
        }
      }
      for (let i = 0; i < parentImageList.length; i++) {
        if (imageSearchData.includes(parentImageList[i].childNodes[1].innerText)) {
          parentImageList[i].classList.add('d-flex');
          parentImageList[i].classList.remove('d-none');
        } else {
          parentImageList[i].classList.add('d-none');
          parentImageList[i].classList.remove('d-flex');
        }
        if (searchText === '') {
          parentImageList[i].classList.add('d-flex');
          parentImageList[i].classList.remove('d-none');
        }
      }
      if (imageSearchData.length === 0 && searchText.length > 0) {
        if (
          document.getElementById(parentWrapper)?.querySelector(`#${parentWrapper}-no-results`) ===
          null
        ) {
          const noResultsTag = document.createElement('span');
          noResultsTag.id = `${parentWrapper}-no-results`;
          noResultsTag.className = 'd-block';
          noResultsTag.style.fontSize = '14px';
          noResultsTag.style.margin = 'auto';
          noResultsTag.innerHTML = 'No results found';
          (document.getElementById(parentWrapper) as HTMLElement).appendChild(noResultsTag);
        } else {
          if (noResults) noResults.className = 'd-block';
        }
      }
    }
  };

  appendReplaceImageSrcToSelectedImage = (
    imageObjectUrl: string,
    imageAltText: string,
    selectedElement: HTMLElement | null,
    editedImageObject: HTMLElement | null,
  ) => {
    const selectedImage = selectedElement?.getElementsByTagName('img')[0] as HTMLImageElement;

    if (selectedImage) {
      selectedImage.src = imageObjectUrl;
      selectedImage.alt = imageAltText;
      selectedImage.classList.remove('image-shape-square');
      selectedImage.classList.remove('image-shape-circle');
      if (editedImageObject?.classList[2]) {
        selectedImage.classList.add(editedImageObject.classList[2]);
      }
      selectedImage.style.width = selectedImage.classList.contains(
        CONSTANTS.LOREE_NAVIGATION_IMAGE_LOGO,
      )
        ? '80px'
        : 'auto';
      selectedImage.style.height = 'auto';
      this.baseClass().changeSelectedElement(selectedElement);
    }

    return selectedElement;
  };

  appendReplaceBannerImageSrcToSelectedImage = (
    imageObjectUrl: string,
    imageAltText: string,
    selectedElement: HTMLElement | null,
  ) => {
    const selectedImage = selectedElement?.getElementsByTagName('img')[0] as HTMLImageElement;
    if (selectedImage) {
      selectedImage.src = imageObjectUrl;
      selectedImage.alt = imageAltText;
      const img = new Image();
      img.onload = () => {
        if (img.height < CONSTANTS.LOREE_BANNER_IMAGEHEIGHT_VALIDATION) {
          selectedImage.style.width = 'auto';
          selectedImage.style.height = 'auto';
        }
      };
      img.src = imageObjectUrl;
      this.baseClass().changeSelectedElement(selectedElement);
    }
  };

  appendImageAsThumbnailToSelectedVideo = (
    imageObjectUrl: string,
    selectedElement: HTMLElement | null,
  ) => {
    const selectedVideo = selectedElement?.getElementsByTagName('video')[0] as HTMLVideoElement;
    if (selectedVideo) {
      if (imageObjectUrl != null) selectedVideo.poster = imageObjectUrl;
      selectedVideo.load();
      this.baseClass().changeSelectedElement(selectedElement);
    }
  };
  // Handling banner image

  handleImageEditButtonClick = (): void => {
    const selectedElement = this.baseClass().getSelectedElement();
    const imageElement = selectedElement?.getElementsByTagName('img')[0];
    if (imageElement) {
      const position = imageElement.style.objectPosition;
      let objectPosition = 'top';
      if (position === 'center center') objectPosition = 'center';
      if (position === 'center bottom') objectPosition = 'bottom';
      this.showBannerImageModal(
        imageElement.src,
        imageElement.title,
        imageElement.alt,
        selectedElement,
      );
      this.handleBannerImageCropPositionChange(objectPosition);
      const applyButton = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_APPLY_BUTTON);
      if (applyButton) {
        applyButton.onclick = (): void => this.changeBannerImagePosition();
      }
    }
  };

  showBannerImageModal = (
    image: string,
    fileName: string,
    imageAltText: string,
    selectedElement: HTMLElement | null,
  ): void => {
    const bannerImageUploadModal = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_WRAPPER,
    );
    const imageElement = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE,
    ) as HTMLImageElement;
    const applyButton = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_APPLY_BUTTON);
    if (bannerImageUploadModal && imageElement && applyButton) {
      bannerImageUploadModal.style.display = 'flex';
      imageElement.src = image;
      imageElement.title = fileName;
      imageElement.alt = imageAltText;
      this.handleBannerImageCropPositionChange('top');
      applyButton.onclick = (): void => this.setBannerToSelectedHtml(selectedElement);
    }
  };

  setBannerToSelectedHtml = (selectedElement: HTMLElement | null): void => {
    const section = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION);
    const imageElement = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE,
    ) as HTMLImageElement;
    if (section && imageElement) {
      const position = section.style.alignItems;
      let objectPosition = 'object-position-top';
      if (position === 'center') objectPosition = 'object-position-center';
      if (position === 'flex-end') objectPosition = 'object-position-bottom';
      const innerHtml = `<div style="height:auto;overflow:hidden;margin-bottom:10px;padding:5px" id=${CONSTANTS.LOREE_BANNER_IMAGE_IDENTITY} class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER} ><img alt="${imageElement.alt}" style="max-height: 226px; height: auto; width: 100%;" class="${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} object-fit-cover ${objectPosition}" src=${imageElement.src} /></div>`;
      this.baseClass().appendImageToSelectedColumn(innerHtml, selectedElement);
      this.hideBannerImageModal();
    }
  };

  handlePositionImageBanner = (selectedImage: HTMLElement, positionValue: string) => {
    const checkVal = ['object-position-top', 'object-position-center', 'object-position-bottom'];
    if (checkVal.includes(positionValue)) {
      for (let i = 0; i < checkVal.length; i++) {
        if (positionValue === checkVal[i]) {
          selectedImage.classList.add(positionValue);
        } else {
          selectedImage.classList.remove(checkVal[i]);
        }
      }
    }
  };

  changeBannerImagePosition = (): void => {
    const selectedElement = this.baseClass().getSelectedElement();
    const section = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION);
    const imageElement = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE,
    ) as HTMLImageElement;
    if (section && imageElement) {
      const position = section.style.alignItems;
      let newPosition = 'object-position-top';
      if (position === 'center') newPosition = 'object-position-center';
      if (position === 'flex-end') newPosition = 'object-position-bottom';
      if (selectedElement) {
        const imageElement = selectedElement.getElementsByTagName('img')[0];
        if (imageElement) {
          this.handlePositionImageBanner(imageElement, newPosition);
        }
        this.hideBannerImageModal();
      }
    }
  };

  replaceShowBannerImageModal = (
    image: string,
    fileName: string,
    imageAltText: string,
    selectedElement: HTMLElement | null,
  ): void => {
    const bannerImageUploadModal = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_WRAPPER,
    );
    const imageElement = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE,
    ) as HTMLImageElement;
    const applyButton = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_APPLY_BUTTON);
    if (bannerImageUploadModal && imageElement && applyButton) {
      bannerImageUploadModal.style.display = 'flex';
      imageElement.src = image;
      imageElement.title = fileName;
      imageElement.alt = imageAltText;
      this.handleBannerImageCropPositionChange('top');
      applyButton.onclick = (): void => this.replaceSetBannerToSelectedHtml(selectedElement);
    }
  };

  replaceSetBannerToSelectedHtml = (selectedElement: HTMLElement | null): void => {
    const section = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION);
    const imageElement = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE,
    ) as HTMLImageElement;
    const selectedImage = selectedElement?.getElementsByTagName('img')[0] as HTMLImageElement;
    if (section && imageElement) {
      selectedImage.style.objectPosition = '';
      const position = section.style.alignItems;
      let objectPosition = 'object-position-top';
      if (position === 'center') objectPosition = 'object-position-center';
      if (position === 'flex-end') objectPosition = 'object-position-bottom';
      selectedImage.src = imageElement.src;
      selectedImage.alt = imageElement.alt;
      this.handlePositionImageBanner(selectedImage, objectPosition);
      selectedImage.style.width = '100%';
      selectedImage.style.height = '226px';
      this.baseClass().fetchImageOnDesignSection(selectedImage.src);
      this.hideBannerImageModal();
    }
  };

  setBannerToSelectedHtmlValidatingHeight = (
    imageSrc: string,
    title: string,
    altText: string,
    selectedElement: HTMLElement | null,
  ) => {
    const innerHtml = `<div style="width:100%;height:auto;overflow:hidden;margin-bottom:10px;padding:5px" id=${CONSTANTS.LOREE_BANNER_IMAGE_IDENTITY} class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER}><img alt="${altText}" style="max-width: 100%" class=${CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE} src=${imageSrc} /></div>`;
    this.baseClass().appendImageToSelectedColumn(innerHtml, selectedElement);
  };

  handleBannerImageCropPositionChange = (position: string): void => {
    const section = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION);
    const options = document.getElementById(CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_SECTION);
    if (section && options) {
      const children = Array.from(options.children);
      children.forEach((child) => child.classList.remove('active'));
      const topButton = document.getElementById(
        CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_TOP_BUTTON,
      );
      const centerButton = document.getElementById(
        CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_CENTER_BUTTON,
      );
      const bottomButton = document.getElementById(
        CONSTANTS.LOREE_BANNER_IMAGE_MODAL_OPTION_BOTTOM_BUTTON,
      );
      switch (position) {
        case 'top':
          section.style.alignItems = 'flex-start';
          if (topButton) topButton.classList.add('active');
          break;
        case 'center':
          section.style.alignItems = 'center';
          if (centerButton) centerButton.classList.add('active');
          break;
        case 'bottom':
          section.style.alignItems = 'flex-end';
          if (bottomButton) bottomButton.classList.add('active');
          break;
      }
    }
  };

  hideBannerImageModal = (): void => {
    const bannerImageUploadModal = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_WRAPPER,
    );
    const imageElement = document.getElementById(
      CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE,
    ) as HTMLImageElement;
    if (bannerImageUploadModal && imageElement) {
      bannerImageUploadModal.style.display = 'none';
      imageElement.src = '';
    }
  };
}
