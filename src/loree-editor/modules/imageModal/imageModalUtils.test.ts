import { getElementsByClassName } from '../../common/dom';
import { appendImageThumbail } from './imageModalUtils';

describe('Append image thumbnail', () => {
  const uploadUrl = {
    name: 'Dummy image',
    key: 'th@kkida-3445',
    thumbnailObjectUrl: 'http://image-thumbnail-content.png',
  };
  test('Lazy loading attribute added', () => {
    const imageSrc = appendImageThumbail('http://global-thumbnail.png', uploadUrl, 'image');
    const parser: DOMParser = new DOMParser();
    const documentDummy = parser.parseFromString(imageSrc, 'text/html');
    expect(
      (
        getElementsByClassName('loree-image-list-image-thumbnail', documentDummy)[0] as HTMLElement
      ).hasAttribute('loading'),
    ).toBeTruthy();
    expect(
      (
        getElementsByClassName('loree-image-list-image-thumbnail', documentDummy)[0] as HTMLElement
      ).getAttribute('loading'),
    ).toBe('lazy');
  });
});
