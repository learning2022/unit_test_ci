import React from 'react';
import { shallow } from 'enzyme';
import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from '../../../aws-exports';
import Sidebar from '../sidebar/sidebar';
import * as imageUtils from './imageModalUtils';
import * as uploadImage from '../../../utils/imageUpload';
import { imageMockData, replaceImage, imagePreviewScreenModalMockData } from './mockData';
import Cropper from './image-cropper';
import { ImageModal } from './imageModal';
import CONSTANTS from '../../constant';
import {
  createDiv,
  getElementById,
  getElementsByClassName,
  getInputElementById,
} from '../../common/dom';
import {
  appendImageDetailModal,
  appendModalContentBody,
  appendModelContentFooter,
} from './imageModalUI';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

const stringToHTML = function (linkToUpdate: string): HTMLElement {
  const parser = new DOMParser();
  const doc = parser.parseFromString(linkToUpdate, 'text/html');
  return doc.body.firstChild as HTMLElement;
};

describe('Render Cropper React Component', () => {
  const originalSrc = '';
  const isCircle = true;
  const cropperComponent = shallow(<Cropper {...originalSrc} {...isCircle} />);

  test('While calling React image cropper Component with canvas URL it should show loading', () => {
    expect(cropperComponent.find('.title').at(0).text()).toEqual('Loading...');
  });

  test('When loading completed', () => {
    cropperComponent.setState({ isLoading: false });
    expect(cropperComponent.render().text()).toBe('');
  });
  test('check the component is rendered with Reat crop', () => {
    cropperComponent.setState({ isLoading: false });
    expect(cropperComponent.render().children().toArray()).toHaveLength(1);
  });

  test('change and drag the cropper', () => {
    cropperComponent.setState({ isLoading: false });
    const onCropChange = jest.fn();
    // eslint-disable-next-line jest/valid-expect
    expect(
      cropperComponent
        .find({ 'data-testid': 'loree-image-cropper' })
        .first()
        .simulate('drag', onCropChange()),
    );
    expect(onCropChange).toHaveBeenCalled();
  });

  test('Show alert note if Gif image is to edit', () => {
    cropperComponent.setState({ isLoading: false, isGifImage: true });
    expect(cropperComponent.render().text()).toBe(
      `Important: Cropping image GIF's or any animated image extensions will result in loss of animation.`,
    );
  });
});
describe('alt text in design textbox', () => {
  const image = new ImageModal();
  const altText = 'alter text';
  beforeEach(() => {
    const imageSection = createDiv('div');
    imageSection.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE_SECTION;
    document.body.append(imageSection);
    const imageBanner = document.createElement('img');
    imageBanner.id = CONSTANTS.LOREE_BANNER_IMAGE_MODAL_IMAGE;
    imageBanner.alt = altText;
    document.body.append(imageBanner);
    const selectedElement = createDiv('div');
    document.body.append(selectedElement);

    image.setBannerToSelectedHtml(selectedElement);
  });
  test('check the alt text for a image', () => {
    expect(
      getElementsByClassName('loree-iframe-content-image')[0].getAttribute('title'),
    ).toBeFalsy();
    expect(getElementsByClassName('loree-iframe-content-image')[0].getAttribute('alt')).toBe(
      altText,
    );
  });
});

describe('replace image should not contain the title attribute', () => {
  const image = new ImageModal();

  test('check the title attrible not replace  image', () => {
    const selectedHtml = stringToHTML(replaceImage.selected);
    const editedimageobjectHtml = stringToHTML(replaceImage.editedimageobject);
    const result = image.appendReplaceImageSrcToSelectedImage(
      replaceImage.imageObjectUrl,
      replaceImage.imageAltText,
      selectedHtml,
      editedimageobjectHtml,
    );
    expect(result?.innerHTML).toBe(replaceImage.exceptedResult);
  });
});

describe('Given user is on loree editor', () => {
  const sideBar = new Sidebar();
  beforeAll(() => {
    window.sessionStorage.setItem('lmsUrl', 'https://test.com');
    window.sessionStorage.setItem('domainName', 'canvas');
    window.sessionStorage.setItem('course_id', '848');
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
    sideBar.initiate({ features: { image: true } });
  });

  beforeEach(() => {
    Amplify.configure(awsconfig);
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'andiswamy',
        sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    API.graphql = jest.fn().mockImplementation(() => imageMockData.canvasImages);
  });

  describe('When loaded and entered into an editor', () => {
    // Test the entire sibar items initated and rendered
    test('the Side bar Image elements present', () => {
      const sideBarImageElement = document.getElementById('image') as HTMLElement;
      expect(sideBarImageElement).not.toBeNull();
      expect(sideBarImageElement.childNodes[0].nodeName).toEqual('svg');
    });
    test('Trigger & Verify click on Image option from left side panel', () => {
      const loreeSubSidebar = document.getElementsByClassName('loreeSubSidebar')[0] as HTMLElement;
      expect(loreeSubSidebar.style.display).toEqual('none'); // before opening Sub Side Bar
      const sidebarElement = document.getElementsByClassName('sidebarElement')[0] as HTMLElement; // click on image option from elements
      sidebarElement.click();
      expect(loreeSubSidebar.style.display).toEqual('flex');
    });
    // Open sub sidebar images option (banner, image, imageWithAlt etc...)
    test('Check and verify the type of Image components available in sub Side panel', () => {
      const subSidebarElements = document.getElementsByClassName('subSidebarElements');
      const subSidebarElementWrapper = document.getElementsByClassName(
        'subSidebarElementWrapper',
      )[0];
      expect(subSidebarElements[0]?.parentElement?.className).toEqual('subSidebarElementWrapper');
      expect((subSidebarElementWrapper.childNodes[0].childNodes[0] as HTMLElement).id).toEqual(
        'bannerImg',
      );
      expect(subSidebarElements[0]?.lastChild?.textContent).toContain('element.image');
      expect(subSidebarElements[1]?.lastChild?.textContent).toContain('element.imagewithtext');
      expect(subSidebarElements[2]?.lastChild?.textContent).toContain('element.imagewithcaption');
      expect(subSidebarElements[3]?.lastChild?.textContent).toContain('element.centeredimage');
      expect(subSidebarElements[4]?.lastChild?.textContent).toContain(
        'element.leftalignedwithtext',
      );
      expect(subSidebarElements[5]?.lastChild?.textContent).toContain(
        'element.rightalignedwithtext',
      );
    });
    describe('As a user, When they click over IMAGE to Add into editor', () => {
      let showImageModal: jest.SpyInstance;
      let listCanvasImages: jest.SpyInstance;
      beforeEach(() => {
        showImageModal = jest.spyOn(imageUtils, 'showImageModal');
        listCanvasImages = jest.spyOn(uploadImage, 'listCanvasImages');
      });

      const subSidebarElements = document.getElementsByClassName(
        'subSidebarElements',
      ) as HTMLCollectionOf<HTMLElement>;
      test('Verify the Image modal is opened', () => {
        // modal should be opened
        const loreeImageModal = document.getElementById('loree-image-modal-wrapper');
        expect(loreeImageModal?.style.display).toEqual('none'); // modal should be hidden before click to Add
        subSidebarElements[0].click();
        expect(showImageModal).toHaveBeenCalledTimes(1);
        expect(loreeImageModal?.style.display).toEqual('block'); // modal should be visible once clicked to Add
      });

      test('Check the canvas images fetched and listed into image modal', () => {
        expect(listCanvasImages).toHaveBeenCalledTimes(1);
        const firstChild = document.getElementsByClassName('image-thumbnail')[0]
          .childNodes[0] as HTMLImageElement;
        expect(firstChild.tagName).toBe('IMG'); // should be an image
        expect(firstChild.src).toEqual(
          'https://crystaldelta.instructure.com/images/thumbnails/185620/vvMSnPiRcKrkzhh34hbYFj536BM8EC00YpIC85I0',
        );
        expect(firstChild.title).toBe('investigation.png'); // should have a title name
      });

      test('Verify the button elements present', () => {
        const modalFooter = document.getElementById('loree-image-modal-content-footer');
        const childNodes = modalFooter?.childNodes[0].childNodes as NodeListOf<HTMLElement>;
        expect(childNodes.length).toBe(5);
        expect(childNodes[0].innerHTML).toEqual('global.back');
        expect(childNodes[1].innerHTML).toEqual('global.back'); // Edit modal Back button
        expect(childNodes[2].innerHTML).toEqual('global.cancel');
        expect(childNodes[3].innerHTML).toEqual('global.add');
        expect(childNodes[4].innerHTML).toEqual('global.apply'); // Edit modal Apply button
      });
      test('Check the ADD button should be in-active by default', () => {
        const addButton = document.getElementById(
          'loree-image-modal-content-footer-add-button',
        ) as HTMLButtonElement;
        expect(addButton?.disabled).toBeTruthy();
      });

      test('Check the Close icon button should be active by default', () => {
        const closeButton = document.getElementById(
          'loree-image-modal-wrapper-close',
        ) as HTMLButtonElement;
        expect(closeButton?.disabled).toBeFalsy();
      });

      test('Check the Back button should be active by default', () => {
        const backButton = document.getElementById(
          'loree-image-modal-content-footer-back-button',
        ) as HTMLButtonElement;
        expect(backButton?.disabled).toBeFalsy();
      });

      test('Check the Cancel button should be active by default', () => {
        const cancelButton = document.getElementById(
          'loree-image-modal-content-footer-cancel-button',
        ) as HTMLButtonElement;
        expect(cancelButton?.disabled).toBeFalsy();
      });

      test('Check the Dec button should be in-active by default', () => {
        const decorativeImg = document.getElementById(
          CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
        ) as HTMLInputElement;
        expect(decorativeImg?.checked).toBeFalsy();
      });

      test('Check the alttextLable  should have Alt text * by default', () => {
        const altTextLabel = document.getElementById(
          CONSTANTS.LOREE_IMAGE_MODAL_LABEL_FOR_ALT_TEXT,
        ) as HTMLInputElement;
        expect(altTextLabel.innerHTML).toBe('modal.alttext');
      });

      test('Check the Alt text field has empty by default', () => {
        const imageAltText = document.getElementById(
          CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
        ) as HTMLInputElement;
        expect(imageAltText.innerHTML).toBe('');
      });

      test('Check the Alt text field has values add button should be active', () => {
        const imageAltText = document.getElementById(
          CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_ALT_INPUT,
        ) as HTMLInputElement;
        imageAltText.innerHTML = 'image';
        const addButton = document.getElementById(
          'loree-image-modal-content-footer-add-button',
        ) as HTMLButtonElement;
        expect(addButton.disabled).toBeTruthy();
      });

      test('Check the Add button should be deactive by decorative box checked', () => {
        const decorativeImg = document.getElementById(
          CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
        ) as HTMLInputElement;
        decorativeImg.checked = true;
        const addButton = document.getElementById(
          'loree-image-modal-content-footer-add-button',
        ) as HTMLButtonElement;
        expect(addButton.disabled).toBeTruthy();
      });
      test('Check the ADD button should be active by default while click the back button', () => {
        const decorativeImg = document.getElementById(
          CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_DECORATIVE_OPTION_INPUT,
        ) as HTMLInputElement;
        decorativeImg.checked = false;
        const addButton = document.getElementById(
          CONSTANTS.LOREE_IMAGE_MODAL_CONTENT_FOOTER_ADD_BUTTON,
        ) as HTMLInputElement;
        expect(addButton?.disabled).toBeTruthy();
      });
    });
  });
});
describe('Image modal upload process', () => {
  let imageModalInstance: ImageModal;
  beforeEach(() => {
    document.body.innerHTML = '';
    imageModalInstance = new ImageModal();
    imageModalInstance.checkImageModalScreenVisiblity = jest
      .fn()
      .mockImplementation(() => 'imagePreviewScreen');
    imageModalInstance.isImageNameAlreadyExistsInCanvas = jest.fn().mockImplementation(() => true);
  });
  test('Course/Loree image implement to loree editor', () => {
    const appendImageToSelectedBlock = jest
      .spyOn(imageModalInstance, 'appendImageToSelectedBlock')
      .mockImplementation(() => '');
    const hideImageModal = jest.spyOn(imageUtils, 'hideImageModal').mockImplementation(() => {});
    document.body.innerHTML = imagePreviewScreenModalMockData('http://www.image-src.jest');
    imageModalInstance.addButtonOnClickHandler();
    expect(
      getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT)?.classList.contains('d-none'),
    ).toBeTruthy();
    expect(appendImageToSelectedBlock).toHaveBeenCalled();
    expect(hideImageModal).toHaveBeenCalled();
  });
  test('Validate the name after cropped images which contains base64 as image source', () => {
    document.body.innerHTML = imagePreviewScreenModalMockData(
      'data:base64-image/png-138u343248jdm',
    );
    imageModalInstance.addButtonOnClickHandler();
    expect(
      getElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_ALERT)?.classList.contains('d-none'),
    ).toBeFalsy();
  });
});
describe('Adding images from external URL to Loree editor', () => {
  let imageModalInstance: ImageModal;
  beforeEach(() => {
    document.body.innerHTML = `<input type='text' id='${CONSTANTS.LOREE_IMAGE_BYURL_INPUT}' value='https://i.pinimg.com/originals/75/47/9d/75479dbc2f40a18d969bc0bcbeb379d8.jpg
    ' />`;
    document.body.append(
      appendModalContentBody({
        uploadimage: 'blob://redVelvet.rv',
        insertlink: 'http://redVelvet-reveluv.png',
      }),
    );
    document.body.append(appendImageDetailModal());
    document.body.append(appendModelContentFooter());
    imageModalInstance = new ImageModal();
  });
  test.each([true, false])('Add images via external url with/without editable', (isEdited) => {
    imageModalInstance.insertByUrlImage(isEdited);
    expect(document.getElementById(CONSTANTS.LOREE_IMAGE_EDIT_BUTTON)).toBeFalsy();
  });
  test('Image modal preview section', () => {
    imageModalInstance.imageFileUploadPreview(
      '',
      'image',
      'http://redVelvet-reveluv.png',
      false,
      false,
      false,
    );
    expect(document.getElementById(CONSTANTS.LOREE_IMAGE_EDIT_BUTTON)).toBeTruthy();
  });
  test('Image modal preview section with edited content', () => {
    imageModalInstance.imageFileUploadPreview(
      'Red Velvet',
      'image',
      'http://redVelvet-reveluv.png',
      true,
      false,
      false,
    );
    expect(getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT)?.value).toBe('');
  });
  test('Image modal preview section with non-edited content', () => {
    imageModalInstance.imageFileUploadPreview(
      'Red Velvet',
      'image',
      'http://redVelvet-reveluv.png',
      false,
      false,
      false,
    );
    expect(getInputElementById(CONSTANTS.LOREE_IMAGE_DETAILS_MODAL_TITLE_INPUT)?.value).toBe(
      'Red Velvet',
    );
  });
});
