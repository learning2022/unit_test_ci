/* eslint-disable */ // Remove this line when editing this file
import ReactDOM, { unmountComponentAtNode } from 'react-dom';
import React from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import CONSTANTS from '../../constant';
import { Storage } from 'aws-amplify';
import { imageInfo } from './imageModal';

export const croppedImageSource: any = {
  src: '',
  isCircle: true,
  imageType: '',
  imageRef: '',
  crop: '',
};
interface originalSrc {
  originalSrc: string;
  isCircle: boolean;
  crop: any;
  imageRef: any;
}
class Cropper extends React.Component<originalSrc> {
  state = {
    src: '',
    crop: undefined,
    circularCrop: true,
    imageRef: '',
    fileUrl: '',
    croppedImageUrl: '',
    isLoading: true,
    isFirstRender: true,
    isGifImage: false,
  };

  async componentDidMount() {
    this.setState({
      crop: {
        aspect: 1,
        x: 0,
        y: 0,
        width: 200,
        height: 200,
        unit: 'px',
      },
    });
    let objectURL, imageBlobData;
    if (this.props.originalSrc.includes('loreev2storage')) {
      const s3ImageDetails: any = await Storage.get(imageInfo.imageKey, { download: true });
      imageBlobData = await s3ImageDetails.Body;
      objectURL = URL.createObjectURL(imageBlobData);
    } else {
      const imageDetails = await fetch(this.props.originalSrc, {
        cache: 'no-store',
      });
      imageBlobData = await imageDetails.blob();
      objectURL = URL.createObjectURL(imageBlobData);
    }
    const myImage: any = new Image();
    myImage.src = objectURL;
    if (imageBlobData.type.includes('gif'))
      this.setState({
        isGifImage: true,
      });
    croppedImageSource.imageType = imageBlobData.type;
    croppedImageSource.imageRef = myImage;
    this.setState({ isLoading: false, src: objectURL, imageRef: myImage });
  }

  unRenderCropper = () => {
    const editmodalImagePanel: any = document.getElementById(CONSTANTS.LOREE_EDIT_IMAGE_STYLES_PREVIEW);
    unmountComponentAtNode(editmodalImagePanel);
  };

  renderCropper = async () => {
    ReactDOM.render(
      <Cropper originalSrc={this.props.originalSrc} isCircle={this.props.isCircle} crop={this.props.crop} imageRef={this.props.imageRef} />,
      document.getElementById(CONSTANTS.LOREE_EDIT_IMAGE_STYLES_PREVIEW),
    );
  };

  handleCropperShape = (isCircle: boolean) => {
    croppedImageSource.isCircle = isCircle;
    ReactDOM.render(
      <Cropper originalSrc={this.props.originalSrc} isCircle={this.props.isCircle} crop={this.props.crop} imageRef={this.props.imageRef} />,
      document.getElementById(CONSTANTS.LOREE_EDIT_IMAGE_STYLES_PREVIEW),
    );
    this.getCroppedImg(this.props.imageRef, this.props.crop);
  };

  onCropComplete = (crop: any) => {
    croppedImageSource.crop = crop;
    this.makeClientCrop(crop);
  };

  onCropChange = (crop: any) => {
    this.setState({ crop });
  };

  makeClientCrop(crop: any) {
    if (this.state.imageRef && crop.width && crop.height) {
      const base64Image = this.getCroppedImg(this.state.imageRef, crop);
      this.setState({ croppedImageUrl: base64Image });
    }
  }

  getCroppedImg(image: any, crop: any) {
    let cropperProperty = crop;
    if (this.state.isFirstRender) {
      if (image.naturalWidth < 200 || image.naturalHeight < 200) {
        const minimumCropLength = image.naturalWidth < image.naturalHeight ? image.naturalWidth : image.naturalHeight;
        cropperProperty = {
          aspect: 1,
          x: 0,
          y: 0,
          width: minimumCropLength,
          height: minimumCropLength,
          unit: 'px',
        };
        this.setState({
          crop: cropperProperty,
        });
      }
      this.setState({
        isFirstRender: false,
      });
    }
    const canvas = document.createElement('canvas');
    const imageElement: any = document.getElementsByClassName('ReactCrop__image')[0];
    const scaleX = image.naturalWidth / imageElement.width;
    const scaleY = image.naturalHeight / imageElement.height;
    canvas.width = cropperProperty.width;
    canvas.height = cropperProperty.height;
    const ctx: any = canvas.getContext('2d');
    let image_type = '';
    if (this.props.isCircle) {
      // add circular white clip when circular crop is enabled
      ctx.beginPath();
      ctx.arc(cropperProperty.width / 2, cropperProperty.height / 2, cropperProperty.width / 2, 0, 2 * Math.PI);
      ctx.clip();

      ctx.drawImage(
        image,
        cropperProperty.x * scaleX,
        cropperProperty.y * scaleY,
        cropperProperty.width * scaleX,
        cropperProperty.height * scaleY,
        0,
        0,
        cropperProperty.width,
        cropperProperty.height,
      );

      image_type = 'image/png';
    } else {
      ctx.fillStyle = '#fff';
      ctx.fillRect(0, 0, cropperProperty.width, cropperProperty.height);
      image_type = 'image/jpeg';
      ctx.drawImage(
        image,
        cropperProperty.x * scaleX,
        cropperProperty.y * scaleY,
        cropperProperty.width * scaleX,
        cropperProperty.height * scaleY,
        0,
        0,
        cropperProperty.width,
        cropperProperty.height,
      );
    }

    const base64Image = canvas.toDataURL(image_type);
    croppedImageSource.src = base64Image;
    return base64Image;
  }

  render() {
    const { crop } = this.state;
    return this.state.isLoading ? (
      <div>
        <div className='title ml-3'>Loading...</div>
      </div>
    ) : (
      <>
        {this.state.isGifImage ? (
          <label className='edit-image-note'>
            Important: Cropping image GIF&apos;s or any animated image extensions will result in loss of animation.
          </label>
        ) : (
          ''
        )}

        <ReactCrop
          data-testid={'loree-image-cropper'}
          src={this.state.src}
          circularCrop={this.props.isCircle}
          keepSelection={true}
          crop={crop}
          ruleOfThirds={true}
          onComplete={this.onCropComplete}
          onChange={this.onCropChange}
          style={{ margin: '20px' }}
        />
      </>
    );
  }
}

export default Cropper;
