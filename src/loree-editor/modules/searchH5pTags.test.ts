import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './../../aws-exports';
import Sidebar from './sidebar/sidebar';
import { h5pMocks } from './h5pMockData';
import * as h5PUtils from './h5p';

jest.mock('../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Given user is on loree editor', () => {
  const sideBar = new Sidebar();
  beforeAll(() => {
    window.sessionStorage.setItem('domainName', 'canvas');
    window.sessionStorage.setItem('course_id', '848');
    window.sessionStorage.setItem('lmsEmail', 'andiswamy.raja@crystaldelta.com');
    window.sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
    sideBar.initiate({ features: { h5p: true } });
  });

  beforeEach(() => {
    Amplify.configure(awsconfig);
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'andiswamy',
        sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => h5pMocks.canvasCourseDetails)
      .mockImplementationOnce(() => h5pMocks.listUser)
      .mockImplementationOnce(() => h5pMocks.h5PContents);
  });

  describe('When loaded and entered into an editor', () => {
    let spyfetchH5PLoginDetails: jest.SpyInstance;
    beforeEach(() => {
      spyfetchH5PLoginDetails = jest.spyOn(h5PUtils, 'appendH5PElement');
    });
    test('the Side bar H5P elements present', () => {
      const sideBarH5pElement = document.getElementById(
        'loree-design-section-H5P-element',
      ) as HTMLElement;
      expect(sideBarH5pElement).not.toBeNull();
      expect(sideBarH5pElement.childNodes[0].nodeName).toEqual('svg');
    });

    test('Trigger & Verify click on H5P option from left side panel', () => {
      const loreeSubSidebar = document.getElementById(
        'loree-sidebar-H5P-element-input-wrapper',
      ) as HTMLElement;
      expect(loreeSubSidebar).toBeNull(); // before opening Sub Side Bar
      const sidebarElement = document.getElementsByClassName('sidebarElement')[0] as HTMLElement; // click on image option from elements
      sidebarElement.click();
      expect(spyfetchH5PLoginDetails).toHaveBeenCalledTimes(1);
    });
    test('should contain the H5P Interactives side panel elements', () => {
      const sideBarH5pElement = document.getElementById(
        'loree-sidebar-H5P-element-input-wrapper',
      ) as HTMLElement;
      expect(sideBarH5pElement).not.toBeNull();
      expect(sideBarH5pElement.firstChild?.textContent).toEqual('element.h5pinteractives');
    });
    test('should render the My H5P from an api data', () => {
      const myH5pWrapper = document.getElementById('myInteractives');
      expect(myH5pWrapper?.firstChild?.lastChild?.textContent).toEqual('Course Shell1');
    });
    test('simulate title search and verify the h5p search result', () => {
      const myH5pWrapper = document.getElementById('myInteractives');
      const searchByTitleWrapper = document.getElementById(
        'loree-sidebar-H5P-element-search-input-value',
      ) as HTMLInputElement;
      searchByTitleWrapper.value = 'Cou';
      const event5 = new Event('input', {
        bubbles: true,
        cancelable: true,
      });
      searchByTitleWrapper.dispatchEvent(event5);
      expect(myH5pWrapper?.firstChild?.lastChild?.textContent).toEqual('Course Shell1');
    });
    test('simulate tag search and verify the h5p search result', () => {
      const myH5pWrapper = document.getElementById('myInteractives');
      const searchByTitleWrapper = document.getElementById(
        'loree-sidebar-H5P-element-search-input-value',
      ) as HTMLInputElement;
      searchByTitleWrapper.value = 'rai';
      const event5 = new Event('input', {
        bubbles: true,
        cancelable: true,
      });
      searchByTitleWrapper.dispatchEvent(event5);
      expect(myH5pWrapper?.firstChild?.lastChild?.textContent).toEqual('Course Shell1');
    });
  });
});
