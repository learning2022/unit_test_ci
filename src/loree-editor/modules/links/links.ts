import { removeLinkFromElement } from '../../../loree-document/links';
import { ensure } from '../../../utils/generalUtils';
import { findUpwardsByTag, getIFrameElement, isElementOneOf } from '../../common/dom';
import { hideLinkEditPopup } from '../../document/linkEditPopup';
import { hideLinkInfoPopup, showLinkInfoPopup } from '../../document/linkInfoPopup';

export function showLinkDetails(targetElement: HTMLElement | null) {
  const tags = ['P', 'SPAN', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'IMG', 'LI', 'UL', 'OL'];
  const anchorElement = findUpwardsByTag(targetElement, 'a');

  if (!targetElement) {
    return;
  }

  if (!anchorElement) {
    if (isElementOneOf(targetElement, tags)) {
      hideLinkInfoPopup();
    }
    return;
  }

  const a: HTMLAnchorElement = anchorElement;

  const parent = ensure(a.parentElement);

  showLinkInfoPopup(
    parent,
    () => {
      removeLinkFromElement(targetElement);
      hideLinkInfoPopup();
      hideLinkEditPopup();
    },
    a.href,
    getIFrameElement(),
  );
}
