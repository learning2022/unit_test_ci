import { removeLinkFromElement } from '../../../loree-document/links';
import CONSTANTS from '../../constant';
import { hideLinkInfoPopup, showLinkInfoPopup } from '../../document/linkInfoPopup';
import { showLinkDetails } from './links';

hideLinkInfoPopup();

jest.mock('../../document/linkInfoPopup', () => {
  return {
    hideLinkInfoPopup: jest.fn(),
    showLinkInfoPopup: jest.fn(),
  };
});

jest.mock('../../../loree-document/links', () => {
  return {
    removeLinkFromElement: jest.fn(),
  };
});

describe('showLinkDetails', function () {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  test('does nothing on null', async () => {
    showLinkDetails(null);
    expect(hideLinkInfoPopup).not.toHaveBeenCalled();
    expect(showLinkInfoPopup).not.toHaveBeenCalled();
  });

  test('returns image wrapper on img', async () => {
    document.body.innerHTML = `<div><a href="http://x.com/"><img></a></div><iframe id="${CONSTANTS.LOREE_IFRAME}"></iframe>`;
    const img = document.body.querySelector('img');
    const div = document.body.querySelector('div');
    const iframe = document.body.querySelector('iframe');

    showLinkDetails(img);
    expect(showLinkInfoPopup).toHaveBeenCalledWith(div, expect.anything(), 'http://x.com/', iframe);
  });

  test('removes link if x pressed', async () => {
    document.body.innerHTML = `<div><a href="http://x.com/"><img></a></div><iframe id="${CONSTANTS.LOREE_IFRAME}"></iframe>`;
    const img = document.body.querySelector('img');

    (showLinkInfoPopup as jest.MockedFunction<typeof showLinkInfoPopup>).mockImplementation(
      (e, r, u, i) => r(),
    );
    showLinkDetails(img);

    expect(removeLinkFromElement).toHaveBeenCalledWith(img);
  });
});
