import {
  myInteractiveBlock,
  sharedInteractiveBlock,
  H5PBlockThumbnail,
  newH5PWrapper,
  handleH5PSearchBlocks,
  eventHandlers,
  H5PModalCloseAction,
  retriveH5PBlockElement,
  elementListAppend,
  appendThumbnailMenu,
  appendH5PModalContentHeaderDivider,
  attachDeleteEventHandlers,
  deleteBlockCustomBlock,
  appendH5PEditModalContentHeader,
  appendH5PEditModalContentBody,
  menuH5PEditModalContainer,
  H5PmodalLoader,
  appendCustomElementToEditor,
  initiateApi,
  fetchH5PLoginDetails,
  H5PmodalContainer,
  appendH5PModalContentHeader,
  fetchContentApi,
  handleAddH5PElement,
  searchH5PWrapper,
  createNewH5P,
  appendH5PMenuModal,
  menuH5PModalContainer,
  appendH5PEditMenuModal,
  deletePopup,
  handleThumbnailMenu,
  departmentInteractiveBlock,
  appendH5PElementSection,
  appendElements,
} from './h5p';
import CONSTANTS from '../constant';
import Base from '../base';
import { getByText, waitFor } from '@testing-library/dom';
import { StorageMock } from '../../utils/storageMock';
import API from '@aws-amplify/api';
import { getEditorElementById } from '../utils';
import { h5pContentData } from './h5pMockData';

jest.mock('../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

const elementSection = document.createElement('div');
const h5pContent = [
  {
    id: '166',
    title: 'Dept Acc',
    created_user: '70',
    account_id: '6',
    tags: 'sample',
    updated_at: '2021-08-13 04:45:03',
  },
  {
    id: '164',
    title: 'National Quality Framework - hotspot',
    created_user: '66',
    account_id: '18',
    tags: 'sample',
    updated_at: '2021-08-13 04:45:03',
  },
];

describe('H5P Menu Build', () => {
  it('new interactive block with enabled = true', () => {
    const block = myInteractiveBlock();
    expect(block.getElementsByClassName('accordion-icon')[0].textContent).toEqual('I');
    expect(block).toMatchSnapshot();
  });
  it('new interactive block with enabled = false', () => {
    const block = sharedInteractiveBlock();
    expect(block.getElementsByClassName('accordion-icon')[0].textContent).toEqual('+');
    expect(block).toMatchSnapshot();
  });

  test('User clicks one + when Everyhing collapsed', async () => {
    const container = myInteractiveBlock();

    getByText(container, 'I').click();

    await waitFor(() => expect(getByText(container, 'element.myh5p')).toBeTruthy());
    expect(container).toMatchSnapshot();
  });
});

describe('H5P', () => {
  beforeAll(() => {
    const base = new Base();
    jest.spyOn(base, 'updateFeaturesList');
    base.updateFeaturesList({
      features: { editorganisationh5p: true, deleteorganisationh5p: true },
    });
  });
  it('should fetch the user credentials while initiated', () => {
    const userData = 'keerthanashri';
    expect(fetchH5PLoginDetails(userData)).toBeNull();
  });
  it('should call the api to fetch all the content and fail with no creds', async () => {
    await expect(fetchContentApi).rejects.toThrow('No credentials');
  });

  it('should render after the initiation and fail with no creds', async () => {
    await expect(initiateApi).rejects.toContain('not authenticated');
  });

  it('should render with a wrapper', () => {
    expect(newH5PWrapper()).toBeInstanceOf(HTMLDivElement);
  });
  it('should render with a searchbox', () => {
    expect(typeof searchH5PWrapper()).toBe('string');
  });

  it('should render with a create button', () => {
    elementSection.id = CONSTANTS.LOREE_WRAPPER;
    document.body.appendChild(elementSection);
    expect(createNewH5P()).toBeInstanceOf(HTMLDivElement);
  });
  it('should render accordion contains user interactives', () => {
    expect(myInteractiveBlock()).toBeInstanceOf(HTMLDivElement);
  });

  it('should render with accordion contains organisation interactives', () => {
    expect(sharedInteractiveBlock()).toBeInstanceOf(HTMLDivElement);
  });

  it('should render with accordion contains department interactives', () => {
    expect(departmentInteractiveBlock()).toBeInstanceOf(HTMLDivElement);
  });

  it('should retrive all the data created', () => {
    expect(retriveH5PBlockElement(h5pContent)).toBeNull();
  });

  it('should append all the retrived data', () => {
    expect(elementListAppend()).toBeUndefined();
  });

  it('should render the interactives created', () => {
    elementSection.id = CONSTANTS.LOREE_H5P_MY_INTERACTIVES_BLOCK_COLLAPSE;
    document.body.appendChild(elementSection);
    expect(H5PBlockThumbnail()).toBeInstanceOf(HTMLDivElement);
  });

  it('should render the interactives created with menu', () => {
    elementSection.id = CONSTANTS.LOREE_WRAPPER;
    document.body.appendChild(elementSection);
    expect(handleThumbnailMenu(elementSection, 'Delete', 'h5p', [])).toEqual('Delete');
    expect(handleThumbnailMenu(elementSection, 'Edit', 'h5p', [])).toEqual('Edit');
  });

  it('should render the modal when clicking the create button', () => {
    expect(H5PmodalContainer()).toBeInstanceOf(HTMLDivElement);
  });
  it('should render with the content in the modal', () => {
    expect(appendH5PMenuModal('create', elementSection, 'edit', [])).toBeInstanceOf(HTMLDivElement);
  });
  it('should render with the container  in the modal', () => {
    expect(menuH5PModalContainer('create', elementSection, 'edit', [])).toBeInstanceOf(
      HTMLDivElement,
    );
  });
  it('should have a header in the modal', () => {
    expect(typeof appendH5PModalContentHeader()).toBe('string');
  });

  it('should render with menu option', () => {
    expect(appendThumbnailMenu('edit', true, [])).toBeInstanceOf(HTMLDivElement);
  });

  it('should return the filtered value', () => {
    expect(handleH5PSearchBlocks()).toBeNull();
  });

  it('should render with a event handler to close the modal', () => {
    expect(eventHandlers()).toBeNull();
  });

  // TODO fix
  it('should render with a modal close action and fail with no creds', async () => {
    await expect(H5PModalCloseAction).rejects.toThrow('No credentials'); // No credentials
  });
  it('shoud remove the popup when the close icon is clicked', () => {
    expect(deletePopup('edit')).toBeNull();
  });

  it('should render with a divider in modal header', () => {
    expect(appendH5PModalContentHeaderDivider()).toEqual(
      '<div class="loree-interactive-block-section-divider"></div>',
    );
  });
  it('should render with the modal content', () => {
    expect(typeof appendH5PEditModalContentHeader()).toBe('string');
  });
  it('should not render with the modal content when the array is empty', () => {
    expect(appendH5PEditModalContentBody('2', [])).toBeUndefined();
  });

  it('should delete the interatives when the user deletes', () => {
    expect(attachDeleteEventHandlers('edit')).toBeUndefined();
  });
  it('should render the modal while editing a content', () => {
    const parentElement = document.createElement('div') as HTMLElement;
    parentElement.id = '2';
    parentElement.appendChild(elementSection);
    document.body.appendChild(parentElement);
    expect(menuH5PEditModalContainer(elementSection, [])).toBeInstanceOf(HTMLDivElement);
  });

  it('should render with the menu inside the edit modal', () => {
    expect(appendH5PEditMenuModal('create', elementSection, 'edit', [])).toBeInstanceOf(
      HTMLDivElement,
    );
  });

  // NG this test never did anything meaningful and now that it is executed with an await
  // it fails with a runtime error - needs to be fixed separately
  it.skip('should render with a event handler to delete interactives', async () => {
    elementSection.id = CONSTANTS.LOREE_H5P_BLOCK_MODAL_FOOTER_DELETE_BUTTON;
    document.body.appendChild(elementSection);
    expect(await deleteBlockCustomBlock('edit', elementSection, 'edit', [])).toEqual(null);
  });

  it('should render with the loader when the api called', () => {
    expect(typeof H5PmodalLoader()).toBe('string');
  });

  it('should append inside the editor when it is clickes', () => {
    const inputDetails = {
      title: 'h5p',
      id: '1',
      account_id: '1',
      created_user: '70',
      tags: 'sample',
      updated_at: '2021-08-13 04:45:03',
    };
    expect(typeof appendCustomElementToEditor(inputDetails)).toBe('string');
  });

  it('should not add the interactives into the editor', () => {
    expect(handleAddH5PElement(elementSection, [])).toBeUndefined();
  });

  describe('should append the tag filter based on the lms', () => {
    const elementSection = document.createElement('div');
    elementSection.id = CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION;
    elementSection.className = 'section';
    elementSection.innerHTML = '<div></div><div></div>';
    elementSection.style.display = 'none';
    document.body.appendChild(elementSection);
    beforeEach(() => {
      global.sessionStorage = new StorageMock() as any;
      API.graphql = jest.fn().mockImplementationOnce(() => h5pContentData);
    });
    afterEach(() => {
      const sideBar = getEditorElementById(CONSTANTS.LOREE_SIDEBAR_ELEMENT_SECTION) as HTMLElement;
      (sideBar.childNodes[1] as HTMLElement).innerHTML = '';
    });
    it('should append the tag filter for permitted lms', () => {
      sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
      expect(sessionStorage.getItem('lmsUrl')).toEqual('https://crystaldelta.instructure.com');
      appendElements(elementSection, false);
      const tagFilter = getEditorElementById(`select2-tag-wrapper`);
      expect(tagFilter).toBeInTheDocument();
    });
    it('should not append the tag filter for non-permitted lms', () => {
      sessionStorage.setItem('lmsUrl', 'https://crystaldelta.com');
      expect(sessionStorage.getItem('lmsUrl')).toEqual('https://crystaldelta.com');
      appendH5PElementSection(elementSection);
      const tagFilter = getEditorElementById(`select2-tag-wrapper`);
      expect(tagFilter).not.toBeInTheDocument();
    });
    it('should not render filter tag for non permitted lms', () => {
      expect(sessionStorage.getItem('lmsUrl')).toEqual('https://crystaldelta.com');
      appendH5PElementSection(elementSection);
      const closeButton = getEditorElementById(
        CONSTANTS.LOREE_H5P_BLOCK_CLOSE_ICON,
      ) as HTMLButtonElement;
      eventHandlers();
      closeButton.click();
      const tagFilter = getEditorElementById(`select2-tag-wrapper`);
      expect(tagFilter).not.toBeInTheDocument();
    });
  });
});
