import { getElementById } from '../../common/dom';
import CONSTANTS from '../../constant';
import getTemplate from './template';

describe('#getTemplate', () => {
  const config = {
    header: {},
    features: {
      'zh-language': true,
    },
  };
  beforeEach(() => {
    document.body.appendChild(getTemplate(config));
    process.env.REACT_APP_ENABLE_LOCALISATION = 'true';
  });
  afterAll(() => {
    process.env.REACT_APP_ENABLE_LOCALISATION = 'false';
  });
  test('Language dropdwon present in dom when language feature is enabled', () => {
    expect(getElementById(CONSTANTS.LOREE_HEADER_LANGUAGE_WRAPPER)).toBeInTheDocument();
  });
});
