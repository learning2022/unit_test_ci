import Header from './header';
import { fireEvent } from '@testing-library/dom';
import { templateConfig } from './templateMockData';
import CONSTANTS from '../../constant';
import { createDiv, getElementById, getElementsByClassName } from '../../common/dom';
import { getEditorElementsByClassName } from '../../utils';
import { API } from 'aws-amplify';
import { isSharedTemplateCalled } from '../customBlocks/customTemplateEvents';
describe('#Templates', () => {
  const headerInstance = new Header();
  beforeAll(() => {
    const containerDesign = document.createElement('div');
    containerDesign.id = 'loree-sidebar-container-design-section';
    document.body.appendChild(containerDesign);
    document.body.innerHTML += `<div id=${CONSTANTS.LOREE_WRAPPER}></div><div id=${CONSTANTS.LOREE_PREVIEW_MODAL_BACKGROUND}></div><div id=${CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER}></div><div id=${CONSTANTS.LOREE_SIDEBAR_ROW_SECTION}></div>`;
    headerInstance.initiate(templateConfig);
  });
  test('should hide the design section when fetching the templates', () => {
    headerInstance.handleGlobalTemplate();
    const templateButton = getElementById(CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON);
    const containerDesignSection = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);
    fireEvent.click(templateButton);
    expect(containerDesignSection.style.display).toEqual('none');
  });
  test('click on outline should change icon', () => {
    const outlineButton = getElementById(CONSTANTS.LOREE_HEADER_OUTLINE_BUTTON);
    const containerDesignSection = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);
    outlineButton.click();
    expect(outlineButton.classList.contains('outline-active')).toBeTruthy();
    expect(containerDesignSection.style.display).toEqual('none');
  });
  test('loree header undo button', () => {
    const undoButton = getElementById(CONSTANTS.LOREE_HEADER_UNDO_BUTTON);
    const containerDesignSection = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);
    undoButton.click();
    expect(containerDesignSection.style.display).toEqual('none');
  });
  test('loree header redo button', () => {
    const redoButton = getElementById(CONSTANTS.LOREE_HEADER_REDO_BUTTON);
    const containerDesignSection = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);
    redoButton.click();
    expect(containerDesignSection.style.display).toEqual('none');
  });
  test('loree header reveal code button', () => {
    const revealButton = getElementById(CONSTANTS.LOREE_HEADER_REVEAL_CODE_BUTTON);
    const containerDesignSection = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);
    revealButton.click();
    expect(containerDesignSection.style.display).toEqual('none');
  });
  test('click on preview button should show preview modal', () => {
    const previewButton = getElementById(CONSTANTS.LOREE_HEADER_PREVIEW_BUTTON);
    const previewModal = getElementById(CONSTANTS.LOREE_PREVIEW_MODAL_WRAPPER);
    const containerDesignSection = getElementById(CONSTANTS.LOREE_SIDEBAR_CONTAINER_DESIGN_SECTION);
    previewButton.click();
    expect(previewModal.style.display).toEqual('block');
    expect(containerDesignSection.style.display).toEqual('none');
  });
  test('click on home button should show confirmation popup', () => {
    const homeButton = getElementById(CONSTANTS.LOREE_HEADER_HOME_BUTTON);
    homeButton.click();
    expect(getElementById(CONSTANTS.LOREE_EXIT_TO_HOME_WRAPPER)).toBeInTheDOM();
  });
  test.skip('click on accessibility button should show accessibility modal', () => {
    const accessibilityButton = getElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON);
    const accessibilityModal = getElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_MODAL_WRAPPER);
    accessibilityButton.click();
    expect(accessibilityModal.style.display).toEqual('flex');
  });
  test.skip('click on cancel button should close accessibility modal', () => {
    headerInstance.destroyAlreadyInitiatedCkEditorInstances = jest.fn();
    const cancelButton = getEditorElementsByClassName(
      'accessibility-cancel-button',
    )[0] as HTMLElement;
    const accessibilityModal = getElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_MODAL_WRAPPER);
    cancelButton.click();
    expect(accessibilityModal.style.display).toEqual('none');
  });
});
describe('#Templates in Sidebar', () => {
  const headerInstance = new Header();
  beforeAll(() => {
    API.graphql = jest.fn();
    const containerDesign = createDiv('div');
    containerDesign.id = CONSTANTS.LOREE_SIDEBAR;
    document.body.appendChild(containerDesign);
    headerInstance.initiate(templateConfig);
  });
  test('should show the loader to fetch the templates and should refresh the template list', async () => {
    headerInstance.handleGlobalTemplate();
    const templateButton = getElementById(CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON);
    fireEvent.click(templateButton);
    const sidebarTemplateLoader = getElementsByClassName('sidebarTemplateLoader')[0];
    expect(sidebarTemplateLoader.textContent?.includes('Loading...')).toBe(true);
    expect(isSharedTemplateCalled).toBe(false);
  });
});
