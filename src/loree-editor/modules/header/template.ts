import { translate } from '../../../i18n/translate';
import CONSTANTS from '../../constant';
import { headerIcon } from './headerIcon';
import headerLoreeLogo from '../../../assets/images/loree-header-logo.png';
import { LmsAccess } from '../../../utils/lmsAccess';
import s from '../../stylesheets/header.module.scss';
import { ConfigInterface, DynamicFeatureKey } from '../../interface';
import Base from '../../base';

const getSaveButton = (): string => {
  return `
  <button class="icon" id=${CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON} aria-label="${translate(
    'global.template',
  )}">
    ${headerIcon.saveIcon}
    <label>${translate('global.template')}</label>
  </button>
`;
};

const getRedoButton = (): string => {
  return `
    <button class="icon" id=${CONSTANTS.LOREE_HEADER_REDO_BUTTON} aria-label="${translate(
    'nav.redo',
  )}">
      ${headerIcon.redoIcon}
      <label>${translate('nav.redo')}</label>
    </button>
  `;
};

const getUndoButton = (): string => {
  return `
    <button class="icon" id=${CONSTANTS.LOREE_HEADER_UNDO_BUTTON} aria-label="${translate(
    'nav.undo',
  )}">
    ${headerIcon.undoIcon}
    <label>${translate('nav.undo')}</label>
    </button>
  `;
};

const getLanguageButton = () => {
  return `
    <div class="icon" id=${CONSTANTS.LOREE_HEADER_LANGUAGE_WRAPPER} aria-label="${translate(
    'global.language',
  )}">
    </div>
  `;
};

const getTitle = (title: string | undefined): string => {
  if (title !== undefined && title !== '') {
    return `
    <div class="tittle" id=${CONSTANTS.LOREE_HEADER_TITLE} aria-label="${title}">
      ${title}
    </div>
  `;
  }
  return '';
};

const getRevealCodeButton = (): string => {
  return `
    <button class="icon" id=${CONSTANTS.LOREE_HEADER_REVEAL_CODE_BUTTON}  aria-label="${translate(
    'nav.code',
  )}">
      ${headerIcon.revealCodeIcon}
      <label>${translate('nav.code')}</label>
    </button>
  `;
};

const getAutoSavingLoader = (): string => {
  return `
  <div id='saving-loader' class='d-none'>
    <div class='d-flex px-3 py-2 saving-loader-block'>
      <div class='icon rotating'>
        ${headerIcon.loaderIcon}
      </div>
      <label class='ml-3'>${translate('global.autosaving')}</label>
    </div>
  </div>
`;
};

const getOutlineButton = (): string => {
  return `
  <div class="divider"></div>
    <button class="icon" id=${CONSTANTS.LOREE_HEADER_OUTLINE_BUTTON} aria-label="${translate(
    'nav.outline',
  )}">
      ${headerIcon.outLineIcon}
      <label>${translate('nav.outline')}</label>
    </button>
  `;
};

const getAccessibilityButton = (): string => {
  return `
    <button class="icon a11yIcon" id=${
      CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON
    } aria-label="${translate('nav.accessibility')}" disabled>
      ${headerIcon.accessibilityIcon}
      <label>${translate('nav.accessibility')}</label>
      <span id=${CONSTANTS.LOREE_A11Y_NOTIFICATION_COUNT} class=${
    s.a11yNotificationCount
  }>${translate('nav.00')}</span>
    </button>
  `;
};

const getPreviewButton = (): string => {
  return `
    <button class="icon" id=${CONSTANTS.LOREE_HEADER_PREVIEW_BUTTON} aria-label="${translate(
    'nav.preview',
  )}">
      ${headerIcon.previewIcon}
      <label>${translate('global.preview')}</label>
    </button>
  `;
};

const getDivider = (): string => {
  return `    <div class="divider"></div>
  `;
};

const baseClass = () => {
  const base = new Base();
  return base;
};

const getTemplate = (config: ConfigInterface): HTMLElement => {
  const lms = new LmsAccess();
  const title = config?.header?.title;
  const features = config?.features;
  let isLanguageFeatureEnabled = false;
  for (const [key, value] of Object.entries(features as DynamicFeatureKey)) {
    if (key.includes('-language') && value) isLanguageFeatureEnabled = true;
  }
  const navBar: HTMLElement = document.createElement('nav');
  navBar.className = 'navbar loreeNavBar fixed-top';
  navBar.id = CONSTANTS.LOREE_HEADER;
  let innerHTML = `
    <div class="container-fluid px-0">
      <div class="leftOptions">
        <div class="loreeHeaderLogo mr-3"><img src="${headerLoreeLogo}" alt="${translate(
    'nav.headerlogo_alt',
  )}"/></div>
        <div class="buttonSection">
        <button class="icon"  id=${CONSTANTS.LOREE_HEADER_HOME_BUTTON} aria-label="${translate(
    'nav.home',
  )}">
         ${headerIcon.HomeIcon}
         <label>${translate('nav.home')}</label>
        </button>`;
  innerHTML +=
    features?.templatesfiltersearch === undefined || !features?.templatesfiltersearch
      ? ''
      : getSaveButton();
  innerHTML += '</div>';
  innerHTML += getTitle(title);
  innerHTML += `</div><div class="rightOptions">`;
  innerHTML += !lms.getAccess() ? getAutoSavingLoader() : '';
  innerHTML += !features?.outline ? '' : getOutlineButton();
  innerHTML += !features?.undoredo ? '' : getDivider();
  innerHTML += !features?.undoredo ? '' : getUndoButton();
  innerHTML += !features?.undoredo ? '' : getRedoButton();
  if (isLanguageFeatureEnabled && process.env.REACT_APP_ENABLE_LOCALISATION === 'true')
    innerHTML += getLanguageButton();
  innerHTML +=
    (features?.codeproperties || features?.accessibilitychecker || features?.preview) === false
      ? ''
      : getDivider();
  innerHTML += !features?.codeproperties ? '' : getRevealCodeButton();
  innerHTML += !features?.accessibilitychecker ? '' : getAccessibilityButton();
  innerHTML += !features?.preview ? '' : getPreviewButton();
  innerHTML += '</div>';
  innerHTML += '</div>';
  navBar.innerHTML = innerHTML;
  navBar.addEventListener('click', () => {
    baseClass().hideAnchorOption();
  });

  return navBar;
};
export default getTemplate;
