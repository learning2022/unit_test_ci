import getTemplate from './template';
import CONSTANTS from '../../constant';
import Base from '../../base';
import { exitToHomeAlert } from '../../alert';
import { browserPromptHandler } from '../../../utils/saveContent';
import Sidebar from '../sidebar/sidebar';
import { videoModalIcons, closeIcon } from '../sidebar/iconHolder';
import { headerIcon } from './headerIcon';
import { a11yGlobalContext } from '../../../a11y-service/context/a11y-context';
import { translate } from '../../../i18n/translate';
import React from 'react';
import ReactDOM from 'react-dom';
import { LanguageDropdown } from '../../../lti/components/language';
import { ConfigInterface } from '../../interface';
import { createDiv } from '../../common/dom';
import { removeClassToElement } from '../../utils';

declare global {
  interface Window {
    CKEDITOR: _Any;
  }
}
const CKEDITOR = window.CKEDITOR;
class Header extends Base {
  sidebar: Sidebar;
  isTemplateClicked: boolean;
  constructor() {
    super();
    this.sidebar = new Sidebar();
    this.isTemplateClicked = false;
  }

  initiate = (config: ConfigInterface = {}): void => {
    this.attachHeader(config);
    this.handleGlobalTemplate();
    this.handleUndoRedoEventListener();
    this.handleHeaderWrapperEvents();
    this.handleViewComponentsEventListener();
    this.handlePreviewModalEventListener();
    this.handleHomeButton();
    this.initiateLanguageDropdown();
    this.attachAccessibilityCheckerModal();
    this.handleAccessibilityChecker();
  };

  hideNonHeadElementSection = () => {
    this.hideDesignSection();
    this.hideMenuDesignSection();
    this.hideTableDesignSection();
    this.hideCustomElementSection();
    this.hideContainerDesignSection();
    this.hideInteractiveElementSection();
    this.hideH5PElementSection();
    this.hideLanguageIsoLabel();
  };

  initiateLanguageDropdown = () => {
    const headerLanguageWrapper = document.getElementById('loree-header-language-wrapper');
    if (!headerLanguageWrapper) return;
    ReactDOM.render(
      <React.StrictMode>
        <LanguageDropdown isEditor />
      </React.StrictMode>,
      headerLanguageWrapper,
    );
  };

  getElement = (id: string) => {
    return document.getElementById(id);
  };

  attachHeader = (config: {}): void => {
    const editorWrapper: HTMLElement | null = this.getElement(CONSTANTS.LOREE_WRAPPER);
    if (editorWrapper) {
      const headerTemplate: HTMLElement = getTemplate(config);
      editorWrapper.insertBefore(headerTemplate, editorWrapper.firstChild);
    }
  };

  /**
   * Handle template eventlistener.
   */
  handleGlobalTemplate = () => {
    const templateButton = this.getElement(CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON);
    if (!templateButton) {
      return;
    }
    templateButton.addEventListener('click', () => {
      void (async () => {
        this.showSidebar();
        this.hideRowSection();
        this.hideElementSection();
        this.hideDesignSection();
        this.hideTableDesignSection();
        this.hideMenuDesignSection();
        this.hideLanguageIsoLabel();
        this.hideContainerDesignSection();

        if (this.isTemplateClicked) return;
        this.hideGlobalTemplate();
        this.isTemplateClicked = true;
        const sidebar: HTMLElement | null = this.getElement(CONSTANTS.LOREE_SIDEBAR);
        const loaderElement = createDiv('div');
        loaderElement.id = 'template-modal-loader';
        loaderElement.className = 'sidebarTemplateLoader m-auto justify-content-center';
        loaderElement.innerHTML = `<div class="icon rotating">
        ${videoModalIcons.loader}
        </div>
        <div class="title ml-3">Loading...</div>`;
        sidebar?.appendChild(loaderElement);
        await this.sidebar.attachGlobalTemplateHandler();
        this.isTemplateClicked = false;
      })();
    });
  };

  /**
   * Handle Undo Redo eventlistener.
   */
  handleUndoRedoEventListener = () => {
    const redoButton = this.getElement(CONSTANTS.LOREE_HEADER_REDO_BUTTON);
    if (redoButton) {
      redoButton.addEventListener('mouseup', (_e: MouseEvent): void => {
        this.hideNonHeadElementSection();
        this.handleRedoHistory();
      });
    }
    const undoButton = this.getElement(CONSTANTS.LOREE_HEADER_UNDO_BUTTON);
    if (undoButton) {
      undoButton.addEventListener('mouseup', (_e: MouseEvent): void => {
        this.hideNonHeadElementSection();
        this.handleUndoHistory();
      });
    }
  };

  handleHeaderWrapperEvents = (): void => {
    const headerWrapper = this.getElement(CONSTANTS.LOREE_HEADER);
    if (headerWrapper) {
      headerWrapper.onclick = () => {
        this.changeSelectedElement(null);
        this.collapseElementSection();
        this.hideAddElementButtonToSelectedElement();
        this.hideCloseElementButtonToSelectedElement();
        this.hideCloseRowButton();
        this.showAddRowButton();
        this.hideElementSection();
        this.hideSubSidebar();
        this.collapseRowSection();
        this.disableRowSection();
        this.hideNonHeadElementSection();
      };
    }
  };

  handleViewComponentsEventListener = (): void => {
    const viewComponentButton = this.getElement(CONSTANTS.LOREE_HEADER_OUTLINE_BUTTON);
    if (viewComponentButton) {
      viewComponentButton.onclick = (): void => {
        viewComponentButton.classList.toggle('outline-active');
        viewComponentButton.innerHTML = viewComponentButton.classList.contains('outline-active')
          ? `${headerIcon.outLineActiveIcon}<label>${translate('nav.outline')}</label>`
          : `${headerIcon.outLineIcon}<label>${translate('nav.outline')}</label>`;
        this.viewComponentsOutline();
        this.hideNonHeadElementSection();
      };
    }
  };

  handlePreviewModalEventListener = (): void => {
    const previewButton = this.getElement(CONSTANTS.LOREE_HEADER_PREVIEW_BUTTON);
    if (previewButton) {
      previewButton.addEventListener('click', (_e: MouseEvent): void => {
        this.hideNonHeadElementSection();
        this.showPreviewModal();
      });
    }
  };

  handleHomeButton = (): void => {
    const homeButton = this.getElement(CONSTANTS.LOREE_HEADER_HOME_BUTTON);
    if (!homeButton) return;
    homeButton.onclick = () => {
      browserPromptHandler(false);
      exitToHomeAlert();
    };
  };

  // Accessibility checker
  attachAccessibilityCheckerModal = (): void => {
    const editorWrapper = this.getElement(CONSTANTS.LOREE_WRAPPER);
    if (!editorWrapper) return;
    const modalWrapper = document.createElement('div');
    modalWrapper.id = CONSTANTS.LOREE_HEADER_ACCESSIBILITY_MODAL_WRAPPER;
    modalWrapper.className = 'accessibilityCheckerModalWrapper';
    modalWrapper.style.display = 'none';
    const modalContent = this.getAccessibilityModalContent();
    modalWrapper.appendChild(modalContent);
    editorWrapper.appendChild(modalWrapper);
  };

  handleAccessibilityChecker = (): void => {
    const accessibilityButton = this.getElement(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON);
    if (!accessibilityButton) return;
    accessibilityButton.onclick = async () => await this.showAccessibilityModal();
    this.hideNonHeadElementSection();
  };

  showAccessibilityModal = async (): Promise<void> => {
    this.hideLanguageIsoLabel();
    if (process.env.REACT_APP_A11Y_CHECKER_STATE === 'true') {
      const { check, isProcessing, setNotificationFlag } = a11yGlobalContext.value;
      setNotificationFlag(false);
      // Trigger a recheck if the checker is already active
      await check(isProcessing); // false
      const accessibilityCloseButton = this.getElement(CONSTANTS.LOREE_A11Y_TRAY_CLOSE_BUTTON);
      removeClassToElement(accessibilityCloseButton, 'd-none');
    } else {
      const modal = this.getElement(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_MODAL_WRAPPER);
      if (!modal) return;
      modal.style.display = 'flex';
      this.initiateAccessibilityChecker();
    }
  };

  hideAccessibilityModal = (): void => {
    const modal = this.getElement(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_MODAL_WRAPPER);
    if (!modal) return;
    modal.style.display = 'none';
    this.destroyAlreadyInitiatedCkEditorInstances();
  };

  getAccessibilityModalContent = (): HTMLElement => {
    const content = document.createElement('div');
    content.className = 'accessibilityCheckerModalContent';
    const modalHeader = this.getAccessibilityModalHeader();
    content.appendChild(modalHeader);
    const modalBody = this.getAccessibilityModalBody();
    content.appendChild(modalBody);
    const modalFooter = this.getAccessibilityModalFooter();
    content.appendChild(modalFooter);
    return content;
  };

  getAccessibilityModalHeader = (): HTMLElement => {
    const header = document.createElement('div');
    header.className = 'accessibilityCheckerModalHeader mx-3';

    // title
    const title = document.createElement('div');
    title.className = 'accessibilityCheckerModalHeaderTitle';
    title.innerHTML = translate('nav.accessibility');
    header.appendChild(title);

    // close button
    const button = document.createElement('button');
    button.className = 'accessibilityCheckerModalHeaderClose';
    button.onclick = () => this.hideAccessibilityModal();
    button.innerHTML = closeIcon;
    header.appendChild(button);

    return header;
  };

  getAccessibilityModalBody = (): HTMLElement => {
    const body = document.createElement('div');
    body.className = 'accessibilityCheckerModalBody';
    body.id = CONSTANTS.LOREE_HEADER_ACCESSIBILITY_WRAPPER;
    return body;
  };

  getAccessibilityModalFooter = (): HTMLElement => {
    const footer = document.createElement('div');
    footer.className = 'accessibilityCheckerModalFooter';
    const cancelButton = document.createElement('button');
    cancelButton.className = 'accessibility-cancel-button mx-1';
    cancelButton.innerHTML = translate('global.cancel');
    cancelButton.onclick = () => this.hideAccessibilityModal();
    footer.appendChild(cancelButton);
    const reCheckButton = document.createElement('button');
    reCheckButton.className = 'accessibility-re-check-button mx-1';
    reCheckButton.innerHTML = translate('header.recheck');
    reCheckButton.onclick = () => this.initiateAccessibilityChecker();
    footer.appendChild(reCheckButton);
    const saveButton = document.createElement('button');
    saveButton.className = 'accessibility-save-button mx-1';
    saveButton.innerHTML = translate('global.save');
    saveButton.onclick = () => this.updateFixedHtml();
    footer.appendChild(saveButton);
    return footer;
  };

  updateFixedHtml = (): void => {
    const html = CKEDITOR.instances[CONSTANTS.LOREE_HEADER_ACCESSIBILITY_WRAPPER].getData();
    this.setHtml(html);
    this.hideAccessibilityModal();
  };

  destroyAlreadyInitiatedCkEditorInstances = (): void => {
    for (const name in CKEDITOR.instances) {
      CKEDITOR.instances[name].destroy(true);
    }
  };

  initiateAccessibilityChecker = (): void => {
    const html = this.getHtml();
    if (html) {
      this.destroyAlreadyInitiatedCkEditorInstances();
      CKEDITOR.editorConfig = function (config: {
        allowedContent: boolean;
        extraAllowedContent: string;
      }) {
        config.allowedContent = true;
        config.extraAllowedContent = '*(*);*{*}';
        config.extraAllowedContent = 'div(col-md-*,container-fluid,row,loree-iframe-content-row)';
        config.extraAllowedContent = '*(loree-iframe-content-element)';
      };
      // Bootstrap JS
      CKEDITOR.scriptLoader.load([
        'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js',
      ]);
      // Bootstrap CSS
      CKEDITOR.config.contentsCss = [
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css',
      ];
      // CK-editor start point
      CKEDITOR.replace(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_WRAPPER, {
        removePlugins: 'contextmenu,tabletools,link,clipboard',
        on: {
          allowedContent: true,
          instanceReady: function (event: {
            editor: {
              _: {
                a11ychecker: {
                  once: Function;
                  exec: string;
                  engine: {
                    fixesMapping: { videoTitle: string[]; tableCaption: string[] };
                    issueDetails: {
                      videoTitle: {};
                      colorContrast: {};
                      tableCaption: {};
                      tableSummary: {};
                      elementId: {};
                    };
                    on: Function;
                  };
                };
              };
            };
          }) {
            const editor = event.editor;
            const a11ychecker = editor._.a11ychecker;
            // Depending on whether it is a dev version or not, Accessibility Checker might not be available yet (#246).
            if (a11ychecker.exec) {
              a11yCheckerReady(editor);
            } else {
              a11ychecker.once('loaded', function () {
                a11yCheckerReady(editor);
              });
            }

            function a11yCheckerReady(editor: _Any) {
              const a11ychecker = editor._.a11ychecker;
              // Bind Quick Fix.
              a11ychecker.engine.fixesMapping.videoTitle = ['VideoTitleReplace'];
              a11ychecker.engine.fixesMapping.tableCaption = ['AddTableCaption'];
              a11ychecker.engine.on(
                'process',
                (evt: {
                  data: { contentElement: { find: Function }; issues: { addItem: Function } };
                }) => {
                  // This is where the actual checking occurs, and this is where you want to report custom issues.
                  const Issue = CKEDITOR.plugins.a11ychecker.Issue;
                  const contentElement = evt.data.contentElement;
                  const issues = evt.data.issues;
                  a11ychecker.engine.issueDetails.videoTitle = {
                    title: 'Videos must provide the title',
                    descr:
                      "A title can be used to identify the work, to place it in context. The title needs to convey a minimal summary of its contents and to pique the reader's curiosity.",
                  };

                  a11ychecker.engine.issueDetails.colorContrast = {
                    title: 'Inconsistent Color Contrast',
                    descr:
                      'Colors must have sufficient contrast between text color and its background. This includes text on images, icons, and buttons. Users, including users with visual disabilities, must be able to perceive content on the page.',
                  };

                  a11ychecker.engine.issueDetails.tableCaption = {
                    title: 'Data tables should contain a caption',
                    descr:
                      'A caption functions like a heading for a table. Most screen readers announce the content of captions.',
                  };

                  a11ychecker.engine.issueDetails.tableSummary = {
                    title: 'Data tables should contain a summary',
                    descr:
                      'A summary conveys information about the organization of the data in a table and helps users navigate it.',
                  };

                  a11ychecker.engine.issueDetails.elementId = {
                    title: 'Element ID should contain a unique value',
                    descr:
                      'The value of an id attribute must be unique to prevent other instances from being overlooked by assistive technologies.',
                  };

                  const accessibilityChecker = (issueId: string | number, element: {}) => {
                    issues.addItem(
                      new Issue(
                        {
                          originalElement: element,
                          testability: Issue.testability.ERROR,
                          id: issueId,
                        },
                        a11ychecker.engine,
                      ),
                    );
                  };
                  CKEDITOR.tools.array.forEach(
                    contentElement.find('video').toArray(),
                    (video: { [x: string]: { title: [] } }) => {
                      const videoElem = video.$;
                      if (videoElem.title.length === 0) {
                        accessibilityChecker('videoTitle', video);
                      }
                    },
                  );

                  CKEDITOR.tools.array.forEach(
                    contentElement.find('iframe').toArray(),
                    (iframe: { [x: string]: { title: [] } }) => {
                      const iframeElem = iframe.$;
                      if (iframeElem.title.length === 0) {
                        accessibilityChecker('videoTitle', iframe);
                      }
                    },
                  );

                  const tagArray = ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'caption'];
                  for (let tagElem = 0; tagElem < tagArray.length; tagElem++) {
                    CKEDITOR.tools.array.forEach(
                      contentElement.find(tagArray[tagElem]).toArray(),
                      async (text: {
                        [x: string]: {
                          style: { color: []; [key: string]: [] };
                          parentNode: { style: { [key: string]: [] } };
                        };
                      }) => {
                        const textElem = text.$;
                        const color =
                          textElem.style.color.length === 0 ? '#000000' : textElem.style.color;
                        let backgroundColor =
                          textElem.style['background-color'].length === 0
                            ? '#ffffff'
                            : textElem.style['background-color'];
                        if (
                          backgroundColor === '#ffffff' &&
                          textElem.parentNode.style['background-color'].length > 0
                        ) {
                          backgroundColor = textElem.parentNode.style['background-color'];
                        }
                        const ratio = await calculateRatio(color, backgroundColor);
                        const colorResult = ratio < 1 / 4.5;
                        if (!colorResult) {
                          accessibilityChecker('colorContrast', text);
                        }
                      },
                    );
                  }

                  CKEDITOR.tools.array.forEach(
                    contentElement.find('table').toArray(),
                    (table: {
                      [x: string]: _Any;
                      find: (arg0: string) => { (): Function; new (): Function; [x: string]: [{}] };
                    }) => {
                      const tableElem = table.$;
                      const captionElem = table.find('caption').$;
                      if (captionElem.length < 1) {
                        accessibilityChecker('tableCaption', table);
                      }
                      if (tableElem.summary === '') {
                        accessibilityChecker('tableSummary', table);
                      }
                    },
                  );

                  CKEDITOR.tools.array.forEach(
                    contentElement.find('*').toArray(),
                    (element: { [x: string]: { id: string } }) => {
                      const eleTag = element.$;
                      $('[id]').each(function () {
                        const ids = $('[id="' + eleTag.id + '"]');
                        if (ids.length > 1 && ids[0] === this)
                          accessibilityChecker('elementId', element);
                      });
                    },
                  );
                },
              );
            }
          },
        },
      }).setData(html);
      setTimeout(() => {
        $('.cke_button__a11ychecker').trigger('click');
      }, 5000);
    }

    const hexToRgb = (hex: string) => {
      if (hex && hex.substring(0, 3) === 'rgb') {
        const rgb: _Any = hex.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+),?([^,\s)]+)?/i);
        hex = rgb
          ? (rgb[1] | (1 << 8)).toString(16).slice(1) +
            (rgb[2] | (1 << 8)).toString(16).slice(1) +
            (rgb[3] | (1 << 8)).toString(16).slice(1)
          : hex;
      }
      const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function (r: string, g: string, b: string) {
        return r + r + g + g + b + b;
      });

      const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result
        ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16),
          }
        : null;
    };
    // Calculate the relative luminance value
    const luminance = (r: number, g: number, b: number) => {
      const a = [r, g, b].map(function (v: number) {
        v /= 255;
        return v <= 0.03928 ? v / 12.92 : Math.pow((v + 0.055) / 1.055, 2.4);
      });
      return a[0] * 0.2126 + a[1] * 0.7152 + a[2] * 0.0722;
    };

    const calculateRatio = (color: [] | string, backgroundColor: string | []) => {
      // read the colors and transform them into rgb format
      const color1rgb: { r: number; g: number; b: number } | null = hexToRgb(color as string);
      const color2rgb: { r: number; g: number; b: number } | null = hexToRgb(
        backgroundColor as string,
      );
      // calculate the relative luminance
      const color1luminance: number = luminance(
        color1rgb?.r as number,
        color1rgb?.g as number,
        color1rgb?.b as number,
      );
      const color2luminance: number = luminance(
        color2rgb?.r as number,
        color2rgb?.g as number,
        color2rgb?.b as number,
      );
      // calculate the color contrast ratio
      const ratio =
        color1luminance > color2luminance
          ? (color2luminance + 0.05) / (color1luminance + 0.05)
          : (color1luminance + 0.05) / (color2luminance + 0.05);
      return ratio;
    };
  };
  // Accessibility checker
}
export default Header;
