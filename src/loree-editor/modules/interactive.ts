import CONSTANTS from '../constant';
import { interactiveElementAlert } from '../alert';
import { Icon, interactiveIcons } from './sidebar/iconHolder';
import { elementAddRemoveIcons, interactiveVerticalDotIcons } from '../iconHolder';
import { initiateInteractive, updateInteractiveContent } from '../../graphql/mutations';
import { API, graphqlOperation } from 'aws-amplify';
import html2canvas from 'html2canvas';
import { handleUploadThumbanail } from '../../utils/imageUpload';
import { translate } from '../../i18n/translate';
import Base from '../base';

import {
  customBlockType,
  customBlockCategoryName,
  handleAutoSaveOnCustomBlockAppend,
} from '../../utils/saveContent';
import accordionimage from '../../assets/images/Accordion.png';
import tabsimage from '../../assets/images/Tab.png';
import buttonimage from '../../assets/images/Button.png';
import flipcardimage from '../../assets/images/FlipCard.png';
import multiplechoiceimage from '../../assets/images/MultipleChoice.png';
import clickandrevealimage from '../../assets/images/Click&reveal.png';
import sliderimage from '../../assets/images/ImageSlider.png';
import dragAndDrop from '../../assets/images/Drag&Drop.png';
import hotspot from '../../assets/images/Hotspot.png';
import {
  inititateApi,
  encryptedApi,
  initiateData,
  encryptData,
  contentApi,
} from '../../loree-editor/modules/interactiveapi';
import { UpdateInteractiveContentMutation } from '../../API';
import { baseClass } from './customBlocks/customBlockHandler';
import { isInteractiveElement } from '../base/baseUtil';
import { interactiveEncrypt } from '../../utils/interactiveEncrypt';
import { getElementsByClassName } from '../common/dom';

let type = '';
let selectedBlock: HTMLElement | null = null;
let filterCategoryId: _Any = [];
let interactiveLists: _Any;
let encryptedList: { user_encrypt: string; org_id_encrypt: string; userId: number };
export let verifyApiData: {};
export let elementData: _Any = [];
const checkedCheckBoxes: _Any = [];
export let liContent: _Any = [];
let contentDetails: _Any = [];
let searchData: _Any = [];
let selectedCatogery = [];
let libraries = [];

export const interactiveBlockInfoModal = async (
  typeOfElement: string,
  selectedElement: HTMLElement | null,
) => {
  type = typeOfElement;
  selectedBlock = selectedElement;
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  if (
    (document.getElementById(CONSTANTS.LOREE_INTERACTIVE_BLOCK_BACKDROP) as HTMLElement) === null
  ) {
    const interactiveBlockBackdrop = document.createElement('div');
    interactiveBlockBackdrop.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_BACKDROP;
    interactiveBlockBackdrop.className = 'loree-interactive-block-backdrop d-none';
    editorParentWrapper.appendChild(interactiveBlockBackdrop);
    const interactiveBlockPopup = document.createElement('div');
    interactiveBlockPopup.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_WRAPPER;
    interactiveBlockPopup.className = 'd-none modal';
    const interactiveModalContent = document.createElement('div');
    interactiveModalContent.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_CONTENT;
    interactiveModalContent.className = 'interactive-block-modal-dialog modal-dialog-centered';
    const modalcontainer = await modalContainer();
    interactiveModalContent.appendChild(modalcontainer);
    interactiveBlockPopup.appendChild(interactiveModalContent);
    editorParentWrapper.appendChild(interactiveBlockPopup);
    if (editorParentWrapper) {
      void eventHandlers();
    }
  }
};

const modalContainer = async () => {
  const interactiveModalContainer = document.createElement('div');
  interactiveModalContainer.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_CONTAINER;
  interactiveModalContainer.className = 'modal-content';
  let innerHTML = appendModalContentHeader();
  innerHTML += await appendModalContentBody();
  innerHTML += appendModelContentFooter();
  interactiveModalContainer.innerHTML = innerHTML;
  return interactiveModalContainer;
};

const appendModalContentHeader = () => {
  return `<div class="modal-header p-0" id=${CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_HEADER}>
  <h5 class="modal-title text-primary" id=${
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_TITLE
  }>${translate('modal.loreeinteractive')}</h5>
  <button type="button" id=${
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_CLOSE_ICON
  } class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div><span class ="loree-interactive-block-section-divider" ></span>`;
};

// New Interactive popup modal
const appendModalContentBody = async () => {
  if (!initiateData) {
    interactiveLists = await inititateApi();
  }
  if (!encryptData) {
    encryptedList = await encryptedApi();
  }
  if (interactiveLists === undefined || encryptedList === undefined) {
    return null;
  } else {
    const iframeUrl = `${interactiveLists.body.url}/${encryptedList.user_encrypt}/${encryptedList.org_id_encrypt}/2`;
    return `
  <div class="modal-body p-0">
  <iframe id='${CONSTANTS.LOREE_IFRAME_INTERACTIVE_CREATE_MODAL}' class="interactiveLineIframe" src="${iframeUrl}"></iframe>
  </div>
  `;
  }
};

const appendModelContentFooter = () => {
  return `
  <div id=${CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER} class="d-flex flex-row flex-nowrap align-items-center">
    <div class = 'powered-by-section' style="margin: auto;">
    <p>Powered by <span>Crystal Delta</span> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ${CONSTANTS.LOREE_APP_VERSION}</p>
    </div>
</div>
  `;
};

const eventHandlers = async () => {
  const closeButton = document.getElementById(CONSTANTS.LOREE_INTERACTIVE_BLOCK_CLOSE_ICON);
  if (closeButton)
    closeButton.onclick = () => {
      hideBlocks();
      void refreshInteractiveBlockThumbnail(false, 0);
    };
  const editcloseButton = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_EDIT_BLOCK_CLOSE_ICON,
  );
  if (editcloseButton)
    editcloseButton.onclick = (e) => {
      removeEditPopup(e);
      void refreshInteractiveBlockThumbnail(false, 0);
    };
  const cancelButton = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_CANCEL_BUTTON,
  );
  if (cancelButton) cancelButton.onclick = () => hideBlocks();
  const saveButton = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_SAVE_BUTTON,
  );
  if (saveButton) saveButton.onclick = () => validateSaveCustomBlocks();
};

const validateSaveCustomBlocks = () => {
  const categoryDropdown = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_CATEGORY_DROPDOWN,
  );
  if (categoryDropdown?.innerHTML !== translate('global.selectcategory')) {
    return true;
  } else {
    return false;
  }
};

// custom block thumbnail
export const interactiveBlockThumbnailImage = async (
  templateName: string,
  assignBlock?: HTMLElement,
) => {
  let thumbnail: _Any;
  const toSave =
    assignBlock !== undefined || assignBlock ? assignBlock : (selectedBlock as HTMLElement);
  if (assignBlock === undefined || assignBlock === null) toSave?.setAttribute('id', 'custom-block');
  await html2canvas(toSave).then(
    function (block) {
      thumbnail = block.toDataURL('image/png');
    },
    () => toSave.removeAttribute('id'),
  );
  const blob = b64toBlob(thumbnail, 'image/png');
  const blobUrl = await handleUploadThumbanail(blob, templateName, 'interactive');
  return blobUrl;
};

export const getEditContentDetails = () => {
  const saveButton = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_SAVE_BUTTON,
  );
  saveButton?.removeAttribute('disabled');
  if (customBlockType === type) {
    const categoryDropdown = document.getElementById(
      CONSTANTS.LOREE_INTERACTIVE_BLOCK_CATEGORY_DROPDOWN,
    );
    if (categoryDropdown && customBlockCategoryName) {
      categoryDropdown.innerHTML = customBlockCategoryName;
    }
  }
};

const refreshInteractiveBlockThumbnail = async (BisGlobal: boolean, number: number) => {
  const interactiveWrapper = document.createElement('div');
  interactiveWrapper.id = 'interactive-dropdown-loader';
  const space = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER,
  );
  space?.insertBefore(interactiveWrapper, null);
  if (interactiveWrapper) {
    interactiveWrapper.innerHTML = interactivemodalLoader();
  }
  contentDetails = await contentApi();
  const removedLoader = document.getElementById('interactive-dropdown-loader');
  removedLoader?.remove();
  const thumbnailWrapper: HTMLElement = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER,
  ) as HTMLElement;
  elementData = [];
  await retriveInteractiveBlockElement(BisGlobal, contentDetails, number);
  thumbnailWrapper?.remove();
};

const hideBlocks = () => {
  const ifrma = document.getElementById(CONSTANTS.LOREE_IFRAME_INTERACTIVE_CREATE_MODAL);
  if (interactiveLists === undefined || encryptedList === undefined) {
    return null;
  } else {
    (
      ifrma as HTMLIFrameElement
    ).src = `${interactiveLists.body.url}/${encryptedList.user_encrypt}/${encryptedList.org_id_encrypt}/2`;
  }
  const interactiveBlockBackdrop = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_BACKDROP,
  );
  interactiveBlockBackdrop?.classList.add('d-none');
  const customBlockModalPopup = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_WRAPPER,
  );

  customBlockModalPopup?.classList.add('d-none');

  const categoryDropdown = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_CATEGORY_DROPDOWN,
  );
  if (categoryDropdown) categoryDropdown.innerHTML = translate('global.selectcategory');

  const infoText = document.getElementById('update-info-content-text');
  infoText?.classList.add('d-none');
};

export const newInteractiveWrapper = (type: string) => {
  const btnWrapper = document.createElement('div');
  btnWrapper.className = 'new-interactive-name';
  const btn = document.createElement('button');
  btn.id = CONSTANTS.LOREE_BLOCK_OPTIONS_INTERACTIVE_BUTTON;
  btn.className = 'create-new-interactive btn btn-primary editor-interactive-btn';
  btn.onclick = () => {
    createNewInteractive();
  };
  btn.id = 'create-new-interactive-btn';
  btn.innerHTML = `${translate('element.createnew')}`;
  btnWrapper.appendChild(btn);
  return btnWrapper;
};

const createNewInteractive = (): void => {
  const interactiveBlockModalHeader = document.getElementById(
    'loree-interactive-block-modal-title',
  );
  if (interactiveBlockModalHeader)
    interactiveBlockModalHeader.innerHTML = translate('modal.loreeinteractive');
  const interactiveBlockBackdrop = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_BACKDROP,
  );
  if (interactiveBlockBackdrop) interactiveBlockBackdrop.classList.remove('d-none');
  const interactiveBlockModal = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_WRAPPER,
  );
  if (interactiveBlockModal) interactiveBlockModal.classList.remove('d-none');
  const saveModalSaveButton = document.getElementById(
    'loree-custom-block-modal-footer-save-button',
  );
  if (saveModalSaveButton) saveModalSaveButton.setAttribute('disabled', 'true');
};

export const searchInteractiveWrapper = (type: string) => {
  return `<div id='loree-sidebar-interactive-row-search-input'>
  <input id='loree-sidebar-interactive-${type}-search-input-value' class='loree-sidebar-interactive-block-search-input' name="search" type="text" autoComplete="off" placeholder="Search" autoFocus/>
  <div id="loree-sidebar-interactive-search-filter-icon" class="loree-sidebar-interactive-search-filter-icon-style">
   ${Icon.searchIcon}
  </div>
  </div>`;
};

export const categoryInteractiveWrapper = async (
  type: string,
  libraryData: [{ id: string; title: string }],
) => {
  if (libraryData === undefined) {
    return null;
  } else {
    libraries = libraryData;
    return `<div class="dropdown dropdown-layout" id="loree-sidebar-interactive-${type}-category-list">
      <button id="loree-sidebar-interactive-${type}-category-dropdown" class="btn dropdown-toggle loree-sidebar-interactive-block-category-dropdown" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">${translate(
      'global.selectcategory',
    )}</button>
      <div class="dropdown-menu loree-sidebar-interactive-block-category-dropdown-menu" id="loree-sidebar-interactive-${type}-category-dropdown-menu" aria-labelledby="loree-sidebar-interactive-row-category-list">
      <div id ='loree-sidebar-interactive-${type}-category-list-wrapper'>
        <div id="loree_interactive_${type}_label_all_category_wrapper"><label id="loree_interactive_${type}_label_all_category"><input type="checkbox" class="mx-1"/>${translate(
      'global.all',
    )}</ label></div>
        ${libraryData
          .map(
            (list: { id: string; title: string }) =>
              `<div id="label-check" class ="tittle text-capitalize" title = "${list.title}"><label id=${list.id}><input type="checkbox" class="mx-1 loree_interactive_${type}_checkbox"/>${list.title}</ label></div>`,
          )
          .join('')}
      </div>
      <div id="loree-sidebar-interactive-block-category-button" class="d-flex flex-row flex-nowrap align-items-center mt-1">
        <div style="margin: auto;">
          <button id='loree-sidebar-interactive-row-category-button-cancel' class='btn btn-outline-primary interactive-dropdown-btn mx-1'>${translate(
            'global.cancel',
          )}</button>
          <button class="apply-button btn btn-primary interactive-dropdown-btn mx-1" id='loree-sidebar-interactive-${type}-category-button-apply'>${translate(
      'global.apply',
    )}</button>
        </div>
      </div>
    </div>
  </div>`;
  }
};

export const refreshInteractiveCategoryDropdown = async (type: string) => {
  const checkBox = document.getElementsByClassName(
    `loree_interactive_${type}_checkbox`,
  ) as HTMLCollection;
  const allCategories: [{}] = [{}];
  for (const check of checkBox) {
    allCategories.push(check?.parentElement?.id as string);
  }
  const newCategoryList: [{}] = [{}];
  liContent.map((list: { id: string }) => newCategoryList.push(list.id));
  if (newCategoryList.length > 0) {
    const newCategoryIds = newCategoryList.filter((x) => !allCategories.includes(x));
    if (newCategoryIds.length > 0) {
      const allCategoryCheckBox = document.getElementById(
        `loree_interactive_${type}_label_all_category`,
      )?.childNodes[0] as HTMLInputElement;
      allCategoryCheckBox.checked = false;
      newCategoryIds.reverse();
    }
  }
  checkSelectedCheckBoxes(checkBox, type);
};

export const refreshInteractiveCheckBox = async (type: string, BisGlobal: boolean) => {
  const checkBox = document.getElementsByClassName(
    `loree_interactive_${type}_checkbox`,
  ) as HTMLCollection;
  const allCategoryCheckBox = document.getElementById(
    `loree_interactive_${type}_label_all_category`,
  )?.childNodes[0] as HTMLInputElement;
  if (allCategoryCheckBox) {
    for (const check of checkBox) {
      (check as HTMLInputElement).checked = true;
    }
    allCategoryCheckBox.checked = true;
    await handleInteractiveCategoryApplyButton(type, BisGlobal);
  } else {
    return null;
  }
};

const checkSelectedCheckBoxes = (checkBox: HTMLCollection, type: string) => {
  const allCategoryCheckBox = document.getElementById(
    `loree_interactive_${type}_label_all_category`,
  )?.childNodes[0] as HTMLInputElement;
  const checkBoxLabel: _Any = [];
  checkBoxLabel.push(allCategoryCheckBox.parentElement);
  for (const check of checkBox) {
    checkBoxLabel.push(check.parentElement as HTMLElement);
    (check as HTMLInputElement).checked = false;
  }
  allCategoryCheckBox.checked = false;
  if (checkedCheckBoxes.length !== 0) {
    for (const label of checkBoxLabel) {
      if (checkedCheckBoxes.includes(label))
        (label.childNodes[0] as HTMLInputElement).checked = true;
    }
  }
};

export const retriveInteractiveBlockElement = async (
  BisGlobal: boolean,
  resData: [],
  number: number,
) => {
  const filteredElementData: _Any = [];
  elementData = [];
  searchData = [];
  liContent = resData;
  const searchInput = document.getElementById(
    'loree-sidebar-interactive-element-search-input-value',
  ) as HTMLInputElement;
  if (searchInput.value) {
    searchInput.value = '';
  }
  if (verifyApiData === undefined) {
    if (BisGlobal) {
      for (const listBlock of liContent) {
        if (!filteredElementData.includes(listBlock)) filteredElementData.push(listBlock);
        if (filterCategoryId.length !== 0) {
          for (const category of filterCategoryId) {
            if (listBlock.library_id.toString() === category) {
              elementData.push(listBlock);
              searchData.push(listBlock);
            }
          }
        }
      }
    } else if (filterCategoryId.length !== 0 || filterCategoryId.length === 0) {
      const interactiveWrapper = document.createElement('div');
      interactiveWrapper.id = 'interactive-dropdown-loader';
      const space = document.getElementById(
        CONSTANTS.LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER,
      );
      space?.insertBefore(interactiveWrapper, null);
      if (interactiveWrapper) {
        interactiveWrapper.innerHTML = interactivemodalLoader();
      }
      const removedLoader = document.getElementById('interactive-dropdown-loader');
      removedLoader?.remove();
      for (const listBlock of liContent) {
        if (!filteredElementData.includes(listBlock)) filteredElementData.push(listBlock);
        if (filterCategoryId.length !== 0) {
          for (const category of filterCategoryId) {
            if (listBlock.library_id.toString() === category) {
              elementData.push(listBlock);
              searchData.push(listBlock);
            }
          }
        } else if (filterCategoryId.length === 0) elementData = [];
        else elementData = filteredElementData;
      }
    }
  }
  // });
  // elementData.sort(sortComparision);
  elementListAppend(number);
};

const elementListAppend = (number: number): void => {
  const interactiveBlockWrapper = document.getElementById(
    'loree-sidebar-interactive-element-input-wrapper',
  ) as HTMLElement;
  const thumbnailWrapper = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER,
  ) as HTMLElement;
  thumbnailWrapper?.remove();
  if (interactiveBlockWrapper)
    void interactiveBlockThumbnail('element', interactiveBlockWrapper, number);
};

export const handleInteractiveSearchBlocks = async () => {
  elementData = [];
  const searchInput = document.getElementById(
    'loree-sidebar-interactive-element-search-input-value',
  ) as HTMLInputElement;

  if (searchInput.value) {
    let filteredList = searchData;
    filteredList = filteredList.filter((item: { title: string }) => {
      return item.title.toLowerCase().includes(searchInput.value.toLowerCase().substr(0, 20));
    });
    elementData = filteredList;
  } else if (searchInput.value === '') {
    elementData = libraries.length === selectedCatogery.length ? liContent : searchData;
  }
  elementData.sort();
  elementListAppend(0);
};

const checkEvents = (type: string) => {
  const checkBox: HTMLCollection = document.getElementsByClassName(
    'loree_interactive_elements_checkbox',
  );
  for (const check of checkBox) {
    if ((check as HTMLInputElement).checked) {
      return false;
    }
  }
  return true;
};

export const handleInteractiveCategoryApplyButton = async (type: string, BisGlobal: boolean) => {
  filterCategoryId = [];
  const categoryDropdownCategory: HTMLElement = document.getElementById(
    'loree-sidebar-interactive-elements-category-list',
  ) as HTMLElement;
  const dropdownMenu = categoryDropdownCategory.children[1] as HTMLElement;
  const dropdownMenuItems = dropdownMenu.children[0] as HTMLElement;
  const selectedList: [] = await handleCategoryListChange(
    type,
    dropdownMenuItems,
    BisGlobal,
    liContent,
  );
  if (selectedList.length !== 0) {
    removeSelectedListDropdown();
    appendSelectedListDropdown(selectedList, categoryDropdownCategory, type, liContent);
  } else {
    removeSelectedListDropdown();
  }
};

const handleCategoryListChange = async (
  type: string,
  dropdownMenuItems: HTMLElement,
  BisGlobal: boolean,
  res: [],
) => {
  selectedCatogery = [];
  const checkBox: HTMLCollection = getElementsByClassName('loree_interactive_elements_checkbox');
  if (checkEvents(type)) {
    for (const check of checkBox) {
      (check as HTMLInputElement).checked = false;
    }
  }
  const selectedList: _Any = [];
  Array.from(dropdownMenuItems.children).forEach((checklist) => {
    const checkListChild = checklist.childNodes;
    if (checklist.id !== 'loree-sidebar-interactive-block-category-button') {
      for (const label of checkListChild) {
        if ((label.childNodes[0] as HTMLInputElement).checked) {
          if (!checkedCheckBoxes.includes(label)) checkedCheckBoxes.push(label);
          if ((label as HTMLElement).id === `loree_interactive_${type}_label_all_category`) {
            for (const check of checkBox) {
              (check as HTMLInputElement).checked = true;
            }
          } else {
            if (!filterCategoryId.includes((label as HTMLElement).id))
              filterCategoryId.push((label as HTMLElement).id);
            if (selectedList.indexOf(label.childNodes[1].textContent) === -1)
              selectedList.push(label.childNodes[1].textContent);
          }
          break;
        } else {
          if (filterCategoryId.includes((label as HTMLElement).id))
            filterCategoryId.splice(filterCategoryId.indexOf((label as HTMLElement).id), 1);
          if (selectedList.indexOf(label.childNodes[1].textContent) !== -1)
            selectedList.splice(selectedList.indexOf(label.childNodes[1].textContent), 1);
          if (checkedCheckBoxes.includes(label))
            checkedCheckBoxes.splice(checkedCheckBoxes.indexOf(label), 1);
          const hiddenCategoryList = document.getElementsByClassName(
            'interactive-category-selected-list-item',
          )[2] as HTMLElement;
          if (hiddenCategoryList) hiddenCategoryList.classList.remove('d-none');
        }
      }
    }
  });
  await retriveInteractiveBlockElement(BisGlobal, res, 0);
  selectedCatogery = selectedList;
  return selectedList;
};

const appendSelectedListDropdown = (
  selectedList: [],
  categoryDropdownCategory: HTMLElement,
  type: string,
  res: [],
) => {
  const selectionArea = categoryDropdownCategory.childNodes[1] as HTMLElement;
  const attachListWrapper = document.createElement('div') as HTMLElement;
  attachListWrapper.className = 'category-selected-list';
  attachListWrapper.id = 'category-selected-list-wrapper';
  let innerHTML = '';
  let count = 0;
  selectedList.forEach((element: string) => {
    if (count < 3) {
      innerHTML += `<div class="m-2 interactive-category-selected-list-item text-capitalize" title="${element}"><span>${element}</span><span class='interactive-category-selected-list-item-remove-icon'>${elementAddRemoveIcons.closeElementIcon}</span></div>`;
      count++;
    } else {
      innerHTML += `<div class="m-2 interactive-category-selected-list-item d-none text-capitalize" title="${element}"><span>${element}</span><span class='interactive-category-selected-list-item-remove-icon'>${elementAddRemoveIcons.closeElementIcon}</span></div>`;
    }
  });
  attachListWrapper.innerHTML = innerHTML;
  selectionArea.appendChild(attachListWrapper);
  if (selectedList.length > 3) {
    const moreCategoriesLink = document.createElement('a');
    moreCategoriesLink.id = 'category-selection-more';
    moreCategoriesLink.innerHTML = String(selectedList.length - 3) + ` ${translate('modal.more')}`;
    moreCategoriesLink.onclick = (e) => showAllSelectedCategory(e, type);
    selectionArea.appendChild(moreCategoriesLink);
  }
  void removeUniqueSelectedList(type, res);
};

const removeUniqueSelectedList = async (type: string, res: []) => {
  const removeIcon = document.getElementsByClassName(
    'interactive-category-selected-list-item-remove-icon',
  );
  let allCategoryCheckBox: HTMLInputElement;
  let categoryDropdownCategory: HTMLElement;
  if (type === 'row') {
    categoryDropdownCategory = document.getElementById(
      'loree-sidebar-interactive-row-category-list',
    ) as HTMLElement;
    allCategoryCheckBox = document.getElementById('loree_interactive_row_label_all_category')
      ?.childNodes[0] as HTMLInputElement;
  } else {
    categoryDropdownCategory = document.getElementById(
      'loree-sidebar-interactive-elements-category-list',
    ) as HTMLElement;
    allCategoryCheckBox = document.getElementById('loree_interactive_elements_label_all_category')
      ?.childNodes[0] as HTMLInputElement;
  }
  Array.from(removeIcon).forEach((element) => {
    (element as HTMLElement).onclick = async (e) => {
      e.stopPropagation();
      element.parentElement?.remove();
      const dropdownMenu = categoryDropdownCategory.children[1] as HTMLElement;
      const dropdownMenuItems = dropdownMenu.children[0] as HTMLElement;
      Array.from(dropdownMenuItems.children).forEach((checklist) => {
        if (checklist.id !== 'loree-sidebar-interactive-block-category-button') {
          checklist.childNodes.forEach((label) => {
            if (
              label.childNodes[1].textContent ===
              (element.parentElement?.childNodes[0] as HTMLElement).innerHTML
            ) {
              (label.childNodes[0] as HTMLInputElement).checked = false;
              allCategoryCheckBox.checked = false;
            }
          });
        }
      });
      const selectedList = await handleCategoryListChange(type, dropdownMenuItems, false, res);
      const moreLink = document.getElementById('category-selection-more') as HTMLElement;
      if (moreLink) {
        if (selectedList.length > 3 && moreLink.innerHTML !== translate('modal.showless'))
          moreLink.innerHTML = String(selectedList.length - 3) + ` ${translate('modal.more')}`;
        else if (selectedList.length <= 3) moreLink.remove();
      }
      const selectionArea = document.getElementById('category-selected-list-wrapper');
      if (selectionArea?.innerHTML === '') selectionArea.remove();
    };
  });
};

const removeSelectedListDropdown = () => {
  const selectionArea = document.getElementById('category-selected-list-wrapper');
  const moreCategoryLink = document.getElementById('category-selection-more');
  if (selectionArea !== null) {
    selectionArea.remove();
    moreCategoryLink?.remove();
  }
};

export const preventInteractiveDropdownClosing = (selectedType: string, e: MouseEvent) => {
  if ((e.target as HTMLElement).tagName !== 'BUTTON') {
    e.stopPropagation();
    let allCategoryCheckBox;
    const targetElement =
      (e.target as HTMLElement).tagName === 'INPUT'
        ? (e.target as HTMLElement).parentElement
        : (e.target as HTMLElement);
    const checkBox: HTMLCollection = document.getElementsByClassName(
      'loree_interactive_elements_checkbox',
    );
    if (
      targetElement?.id === 'loree_interactive_row_label_all_category' ||
      targetElement?.id === 'loree_interactive_elements_label_all_category'
    ) {
      allCategoryCheckBox = e.target as HTMLInputElement;
      if (allCategoryCheckBox.checked) {
        for (const check of checkBox) {
          (check as HTMLInputElement).checked = true;
        }
      } else {
        for (const check of checkBox) {
          (check as HTMLInputElement).checked = false;
        }
      }
    } else {
      let isAllChecked;
      allCategoryCheckBox = document.getElementById('loree_interactive_elements_label_all_category')
        ?.childNodes[0] as HTMLInputElement;
      for (const check of checkBox) {
        if (!(check as HTMLInputElement).checked) {
          isAllChecked = false;
          break;
        }
        isAllChecked = true;
      }
      allCategoryCheckBox.checked = !!isAllChecked;
    }
  }
};

const showAllSelectedCategory = (e: MouseEvent, type: string) => {
  e.stopPropagation();
  const hiddenCategoryList = document.getElementsByClassName(
    'interactive-category-selected-list-item',
  ) as HTMLCollection;
  const moreCategoryLink = document.getElementById('category-selection-more');
  if (moreCategoryLink?.innerHTML !== translate('modal.showless'))
    for (const category of hiddenCategoryList) category.classList.remove('d-none');
  else void handleInteractiveCategoryApplyButton(type, true);
  if (moreCategoryLink) moreCategoryLink.innerHTML = translate('modal.showless');
};

export const editInteractiveUsingBlockOption = async (selectedElement: HTMLElement) => {
  if (elementData.length > 0 || (interactiveLists && encryptedList)) {
    await handleThumbnailMenu(selectedElement, 'edit', 'Interactive', elementData);
  } else {
    const inititateApi: _Any = await API.graphql(
      graphqlOperation(initiateInteractive, { type: 'CANVAS' }),
    );
    interactiveLists = JSON.parse(inititateApi.data.initiateInteractive);
    const encryptedData: _Any = interactiveEncrypt(interactiveLists);
    encryptedList = encryptedData;
    await handleThumbnailMenu(selectedElement, 'edit', 'Interactive', elementData);
  }
};

// thumbnail menu
export const appendThumbnailMenu = (
  blockType: string,
  isGlobal: boolean,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  const menuWrapper = document.createElement('div');
  menuWrapper.className = 'dropdown dropdown-layout';
  menuWrapper.id = 'loree-sidebar-interactive-block-menu-wrapper';
  const menuBtn = document.createElement('button');
  menuBtn.id = 'loree-sidebar-interactive-block-menu-btn';
  menuBtn.className = 'btn interactive-block-menu-btn dropdown-toggle';
  menuBtn.innerHTML = interactiveVerticalDotIcons;
  menuBtn.type = 'button';
  menuBtn.setAttribute('data-toggle', 'dropdown');
  const showMenu = document.createElement('div');
  showMenu.className = 'dropdown-menu interactive-block-thumnail-dropdown-menu';
  showMenu.id = 'loree-sidebar-interactive-block-thumbnail-dropdown-menu';
  showMenu.setAttribute('aria-labeledby', 'loree-sidebar-interactive-row-category-list');
  const list = [translate('global.edit')];
  if (!isGlobal) list.push(translate('global.delete'));
  list.forEach((val) => {
    const li = document.createElement('div');
    li.className = 'interactive-block-thumbnail-menu-option';
    li.onclick = async () => await handleThumbnailMenu(menuWrapper, val, blockType, elementData);
    li.innerHTML = val;
    showMenu.appendChild(li);
  });
  menuWrapper.appendChild(menuBtn);
  menuWrapper.appendChild(showMenu);
  return menuWrapper;
};

const appendMenuModal = (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const interactiveBlockBackdrop = document.createElement('div');
  interactiveBlockBackdrop.id = `loree-custom-block-${modalType}-backdrop`;
  interactiveBlockBackdrop.className = 'loree-interactive-block-backdrop';
  editorParentWrapper.appendChild(interactiveBlockBackdrop);
  const interactiveBlockPopup = document.createElement('div');
  interactiveBlockPopup.id = `loree-custom-block-${modalType}-modal-wrapper`;
  interactiveBlockPopup.className = 'modal';
  const interactiveModalContent = document.createElement('div');
  interactiveModalContent.id = `loree-custom-block-${modalType}-modal-content`;
  interactiveModalContent.className = 'modal-dialog modal-dialog-centered';
  interactiveModalContent.appendChild(
    menuModalContainer(modalType, menuWrapper, blockType, elementData),
  );
  interactiveBlockPopup.appendChild(interactiveModalContent);
  editorParentWrapper.appendChild(interactiveBlockPopup);
};

const menuModalContainer = (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  console.log('blockType===', blockType);
  const interactiveModalContainer = document.createElement('div');
  interactiveModalContainer.id = `loree-custom-block-${modalType}-modal-container`;
  interactiveModalContainer.className = 'modal-content';
  let innerHTML = `<div class="modal-header p-0 mx-0" id=loree-custom-block-${modalType}-modal-header>
  <h5 class="modal-title text-primary" id=loree-custom-block-${modalType}-modal-title><span class="text-capitalize">${modalType}</span> ${blockType}</h5>
  <button type="button" id=loree-custom-block-${modalType}-close-icon class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div><span class ="loree-interactive-block-section-divider" ></span>`;
  if (modalType === translate('modal.delete')) {
    innerHTML += `<div class="modal-body p-0 "><p> ${translate(
      'modal.doyouwanttodeletethis',
    )} <span class="text-lowercase">${blockType}</span> ${translate('modal.permanently')}?</div>
    `;
  } else {
    innerHTML += `<div class="modal-body p-0 "><p> ${translate(
      'modal.doyouwanttoeditthis',
    )} <span class="text-lowercase">${translate('modal.interactive')}</span>?</div>
    `;
  }
  const modalButtonWrapper = document.createElement('div');
  modalButtonWrapper.className = 'd-flex flex-row flex-nowrap align-items-center';
  modalButtonWrapper.id = `loree-custom-block-${modalType}-modal-footer`;
  const modalButtonChildWrapper = document.createElement('div');
  modalButtonChildWrapper.className = 'm-auto';
  const cancelBtn = document.createElement('button');
  cancelBtn.id = `loree-custom-block-${modalType}-modal-footer-cancel-button`;
  cancelBtn.className = 'mx-1';
  cancelBtn.innerHTML = translate('global.cancel');
  cancelBtn.onclick = () => deletePopup(modalType);
  const menuBtn = document.createElement('button');
  if (modalType === translate('modal.delete')) {
    menuBtn.className = 'ok-button mx-1';
    menuBtn.onclick = async () =>
      await deleteBlockCustomBlock(modalType, menuWrapper, blockType, elementData);
    menuBtn.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_DELETE_BUTTON;
    menuBtn.innerHTML = translate('global.delete');
  } else {
    menuBtn.className = 'ok-button mx-1';
    menuBtn.onclick = (e) => appendEditMenuModal('editmodal', menuWrapper, elementData);
    menuBtn.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_EDIT_BUTTON;
    menuBtn.innerHTML = translate('global.edit');
  }
  modalButtonChildWrapper.appendChild(cancelBtn);
  modalButtonChildWrapper.appendChild(menuBtn);
  modalButtonWrapper.appendChild(modalButtonChildWrapper);
  interactiveModalContainer.innerHTML = innerHTML;
  interactiveModalContainer.appendChild(modalButtonWrapper);
  return interactiveModalContainer;
};

const appendEditMenuModal = (
  modalType: string,
  menuWrapper: HTMLElement,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  const interactiveEditBlockBackdrop = document.getElementById(`loree-custom-block-edit-backdrop`);
  interactiveEditBlockBackdrop?.remove();
  const customBlockModalPopup = document.getElementById(`loree-custom-block-edit-modal-wrapper`);
  customBlockModalPopup?.remove();
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const interactiveBlockBackdrop = document.createElement('div');
  interactiveBlockBackdrop.id = `loree-interactive-block-${modalType}-backdrop`;
  interactiveBlockBackdrop.className = 'loree-interactive-block-backdrop';
  editorParentWrapper.appendChild(interactiveBlockBackdrop);
  const interactiveBlockPopup = document.createElement('div');
  interactiveBlockPopup.id = `loree-interactive-block-${modalType}-modal-wrapper`;
  interactiveBlockPopup.className = 'modal';
  const interactiveModalContent = document.createElement('div');
  interactiveModalContent.id = `loree-interactive-block-${modalType}-modal-content`;
  interactiveModalContent.className = 'interactive-block-modal-dialog modal-dialog-centered';
  interactiveModalContent.appendChild(menuEditModalContainer(menuWrapper, elementData));
  interactiveBlockPopup.appendChild(interactiveModalContent);
  editorParentWrapper.appendChild(interactiveBlockPopup);
  if (editorParentWrapper) {
    void eventHandlers();
  }
};

const menuEditModalContainer = (
  menuWrapper: HTMLElement,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  let id = menuWrapper.parentElement?.parentElement?.getAttribute('id');
  const iframeElement = menuWrapper.getElementsByTagName('iframe');
  if (!id) {
    id = iframeElement[0].id.split('_')[1];
  }
  const interactiveModalContainer = document.createElement('div');
  interactiveModalContainer.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_CONTAINER;
  interactiveModalContainer.className = 'modal-content';
  let innerHTML = appendEditModalContentHeader();
  innerHTML += appendModalContentHeaderDivider();
  innerHTML += appendEditModalContentBody(id, elementData, iframeElement[0]?.id.split('_')[0]);
  innerHTML += appendEditModelContentFooter();
  interactiveModalContainer.innerHTML = innerHTML;
  return interactiveModalContainer;
};

const appendEditModalContentHeader = () => {
  const modalheader = document.createElement('div');
  modalheader.className = 'modal-header p-0';
  modalheader.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_HEADER;
  const headerTitle = document.createElement('h5');
  headerTitle.className = 'modal-title text-primary';
  headerTitle.id = CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_TITLE;
  headerTitle.innerText = translate('modal.loreeinteractive');
  const headerCloseButton = document.createElement('button');
  headerCloseButton.id = CONSTANTS.LOREE_INTERACTIVE_EDIT_BLOCK_CLOSE_ICON;
  headerCloseButton.className = 'close';
  headerCloseButton.setAttribute('data-dismiss', 'modal');
  headerCloseButton.setAttribute('aria-label', 'Close');
  headerCloseButton.innerHTML = interactiveIcons.interactiveEditCloseIcon;
  headerCloseButton.onclick = (e) => {
    removeEditPopup(e);
  };
  modalheader.appendChild(headerTitle);
  modalheader.appendChild(headerCloseButton);
  return modalheader.outerHTML;
};

const appendModalContentHeaderDivider = () => {
  const divider = document.createElement('div');
  divider.className = 'loree-interactive-block-section-divider';
  return divider.outerHTML;
};

const removeEditPopup = (e: MouseEvent) => {
  e.preventDefault();
  const iframe: HTMLIFrameElement | null = document.getElementById(
    CONSTANTS.LOREE_IFRAME,
  ) as HTMLIFrameElement;
  if (iframe) {
    const editModalPopup = document.getElementById(
      'loree-interactive-block-editmodal-modal-wrapper',
    );
    editModalPopup?.remove();
    const editModalPopupBackdrop = document.getElementById(
      'loree-interactive-block-editmodal-backdrop',
    );
    editModalPopupBackdrop?.remove();
    refreshInteractiveIframe();
  }
};

const refreshInteractiveIframe = () => {
  const selectedInteractiveElement = baseClass().getSelectedElement();
  const iframeElement = selectedInteractiveElement?.getElementsByTagName(
    'iframe',
  ) as HTMLCollectionOf<HTMLIFrameElement>;
  if (!isInteractiveElement() && !iframeElement[0]) return;
  iframeElement[0].src = iframeElement[0].src.toString();
};

// New Interactive popup modal
const appendEditModalContentBody = (
  id: string,
  elementData: [{ id: number; library_type: string; user_id: number }],
  libraryType?: string,
) => {
  const interactiveurl = interactiveLists.body.url;
  const splitUrl = interactiveurl.split('/interactives');
  const liContentUrl = splitUrl[0];
  let iFrameUrl = `${liContentUrl}/content/${libraryType}/${encryptedList.userId}/${id}/${encryptedList.org_id_encrypt}/2`;
  if (
    libraryType &&
    (libraryType === 'Button' || libraryType === 'multiplechoice' || libraryType === 'flipcard')
  ) {
    iFrameUrl = `${liContentUrl}/${libraryType}/edit/${encryptedList.userId}/${id}/${encryptedList.org_id_encrypt}/2`;
  }
  for (const lidata of elementData) {
    if (id === lidata.id.toString()) {
      iFrameUrl = `${liContentUrl}/content/${lidata.library_type}/${lidata.user_id}/${id}/${encryptedList.org_id_encrypt}/2`;
      if (
        lidata.library_type !== undefined &&
        (lidata.library_type === 'Button' ||
          lidata.library_type === 'multiplechoice' ||
          lidata.library_type === 'flipcard')
      ) {
        iFrameUrl = `${liContentUrl}/${lidata.library_type}/edit/${lidata.user_id}/${id}/${encryptedList.org_id_encrypt}/2`;
      }
    }
  }
  return `
  <div class="modal-body p-0">
  <iframe class="interactiveLineIframe" src="${iFrameUrl}"></iframe>
  </div>
  `;
};

const appendEditModelContentFooter = () => {
  return `
  <div id=${
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER
  } class="d-flex flex-row flex-nowrap align-items-center">
    <div class = 'powered-by-section' style="margin: auto;">
      <p>${translate('modal.poweredby')} <span>${translate(
    'global.crystaldelta',
  )}</span> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ${CONSTANTS.LOREE_APP_VERSION}</p>
    </div>
  </div>
  `;
};

const attachDeleteEventHandlers = (modalType: string) => {
  const closeButton = document.getElementById(`loree-custom-block-${modalType}-close-icon`);
  if (closeButton) closeButton.onclick = () => deletePopup(modalType);
};

const deleteBlockCustomBlock = async (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  const deleteButtonText: HTMLElement = document.getElementById(
    CONSTANTS.LOREE_INTERACTIVE_BLOCK_MODAL_FOOTER_DELETE_BUTTON,
  ) as HTMLElement;
  deleteButtonText.innerHTML = translate('global.deleting');
  const updateId = menuWrapper.parentElement?.parentElement?.getAttribute('id');
  for (const content of elementData) {
    if (updateId === content.id.toString()) {
      menuWrapper.parentElement?.parentElement?.remove();
      let updateInteractive = await API.graphql<UpdateInteractiveContentMutation>(
        graphqlOperation(updateInteractiveContent, {
          contentId: content.id,
          userId: content.user_id,
        }),
      );
      updateInteractive =
        JSON.parse(updateInteractive?.data?.updateInteractiveContent as string) ?? [];
    }
  }
  const elementDataHTML = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER,
  ) as HTMLElement;
  const contentList = elementDataHTML.childNodes;
  if (contentList.length === 0) {
    elementDataHTML.className = 'loree-interactive-block-no-results font-italic text-center';
    elementDataHTML.innerHTML = translate('global.noresultsfound');
  }
  searchData = searchData.filter(function (el: { id: string }) {
    return el.id.toString() !== updateId;
  });
  liContent = liContent.filter(function (el: { id: string }) {
    return el.id.toString() !== updateId;
  });
  deletePopup(modalType);
  interactiveElementAlert(blockType, 'deleteee', '');
  const alertPopup = document.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
  alertPopup?.classList.remove('d-none');
  setTimeout(() => {
    alertPopup?.remove();
  }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
};

const deletePopup = (modalType: string) => {
  const interactiveBlockBackdrop = document.getElementById(
    `loree-custom-block-${modalType}-backdrop`,
  );
  interactiveBlockBackdrop?.remove();
  const customBlockModalPopup = document.getElementById(
    `loree-custom-block-${modalType}-modal-wrapper`,
  );
  customBlockModalPopup?.remove();
  const base = new Base();
  base.hideCloseRowButton();
  base.showAddRowButton();
  base.hideAddElementButtonToSelectedElement();
};

const handleThumbnailMenu = async (
  menuWrapper: HTMLElement,
  val: string,
  blockType: string,
  elementData: [{ id: number; library_type: string; user_id: number }],
) => {
  if (val === 'Delete') {
    appendMenuModal(translate('modal.delete'), menuWrapper, blockType, elementData);
    attachDeleteEventHandlers(translate('modal.delete'));
  } else {
    appendMenuModal(translate('modal.edit'), menuWrapper, blockType, elementData);
    attachDeleteEventHandlers(translate('modal.edit'));
  }
};

// sidebar custom block list
export const interactiveBlockThumbnail = async (
  type: string,
  customSidebarWrapper: HTMLElement,
  number: number,
) => {
  const elementDataHTML = document.createElement('div');
  elementDataHTML.id = CONSTANTS.LOREE_SIDEBAR_INTERACTIVE_ELEMENTS_THUMBNAIL_WRAPPER;
  if (elementData.length === 0) {
    elementDataHTML.className = 'loree-interactive-block-no-results font-italic text-center';
    elementDataHTML.innerHTML = translate('global.noresultsfound');
  } else {
    elementData.reverse();
    Array.from(elementData).forEach((data: _Any) => {
      if (data.active === 1) {
        const rowElement = document.createElement('div');
        rowElement.className = 'interactive-block-thumbnail position-relative';
        rowElement.id = data.id;
        const rowThumbnail = document.createElement('div');
        rowThumbnail.className = 'interactiv-blocks-thumbnail-element';
        rowThumbnail.prepend(
          appendThumbnailMenu(translate('modal.interactive'), false, elementData),
        );
        const thumbnailImg = document.createElement('img');
        thumbnailImg.src =
          data.library_type === 'Accordion'
            ? accordionimage
            : data.library_type === 'Tabs'
            ? tabsimage
            : data.library_type === 'Button'
            ? buttonimage
            : data.library_type === 'multiplechoice'
            ? multiplechoiceimage
            : data.library_type === 'flipcard'
            ? flipcardimage
            : data.library_type === 'imageslider'
            ? sliderimage
            : data.library_type === 'DragandDrop'
            ? dragAndDrop
            : data.library_type === 'Hotspot'
            ? hotspot
            : clickandrevealimage;
        thumbnailImg.className = 'loree-sidebar-interactive-block-thumbnail';
        const thumbnailOverlay = document.createElement('div');
        thumbnailOverlay.className = 'interactive-block-thumbnail-overlay';
        rowThumbnail.append(thumbnailImg);
        rowThumbnail.append(thumbnailOverlay);
        const title = document.createElement('p');
        title.className = 'interactive-block-title';
        title.innerHTML = `<span title="${data.title}">${data.title}</span>`;
        rowElement.onclick = (e): void => handleAddInteractiveElement(e, elementData);
        rowElement.append(rowThumbnail);
        rowElement.appendChild(title);
        elementDataHTML.append(rowElement);
      }
    });
  }
  customSidebarWrapper.appendChild(elementDataHTML as HTMLElement);
};

export const changeHeaderTitle = (title: string) => {
  const headerTitle = document.getElementById('loree-header-title');
  if (headerTitle) headerTitle.innerHTML = title;
};

const handleAddInteractiveElement = (
  content: _Any,
  elementData: [{ library_type: string; title: string; id: string }],
): void => {
  const id = content.target.parentElement.parentElement.id;
  for (const contentdata of elementData) {
    if (contentdata.id.toString() === id) {
      const base = new Base();
      handleAutoSaveOnCustomBlockAppend(true);
      base.hideBlockOptionsToSelectedElements();
      const interactivElement = contentdata;
      const interactiveIframe = appendCustomElementToEditor(interactivElement);
      base.appendElementToSelectedColumn(interactiveIframe);
    }
  }
};

const appendCustomElementToEditor = (data: { library_type: string; title: string; id: string }) => {
  const interactiveurl = interactiveLists.body.url;
  const splitUrl = interactiveurl.split('/interactives');
  const liContentUrl = splitUrl[0];
  const iframeHeight = data.library_type === 'Button' ? '200' : '400';
  const element = document.createElement('div');
  element.innerHTML = `<div style="padding:10px; margin-bottom: 20px;" class ='${CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_WRAPPER}'>
    <iframe 
      class ='${CONSTANTS.LOREE_IFRAME_CONTENT_INTERACTIVE_ELEMENT}'
      title="${data.title}"
      id="${data.library_type}_${data.id}"
      frameBorder="0"
      width ="100%"
      height="${iframeHeight}"
      scrolling="yes"
      src="${liContentUrl}/${data.library_type}/${data.id}">
    </iframe>
  </div>`;
  return element.innerHTML;
};

// thumbnail image blob conversion
const b64toBlob = (b64Data: string, contentType: string, sliceSize = 512) => {
  const data = b64Data.substring(b64Data.indexOf(',') + 1);
  const byteCharacters = window.atob(data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);
    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
};

const interactivemodalLoader = (): string => {
  return `
<div id="interactive-dropdown-loader" class=" loader m-aauto justify-content-center">
  <div class="icon rotating">
  ${interactiveIcons.interactiveloader}
  </div>
  <div class="title ml-3">${translate('global.loading')}</div>
</div>
`;
};
