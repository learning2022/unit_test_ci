import * as customBlockUI from './customBlocksUI';
import { StorageMock } from '../../../utils/storageMock';
import CONSTANTS from '../../constant';
import * as customTemplateEvents from '../customBlocks/customTemplateEvents';
import * as customBlockAction from '../customBlocks/customBlocksAction';
import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from '../../../aws-exports';
import Base from '../../base';
import BlockOptions from '../../modules/blockOptions/blockOptions';
import {
  customBlockMockData,
  globalBlocksResponse,
  categoryResponse,
  blocksResponse,
  userMockData,
  globalblocksResponse,
  globalblocksResponseOne,
} from './customBlockMockData';
import Bootstrap from '../iframe/templates/bootstrap';
import Loree from '../../loree';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#customBlockUI', () => {
  let baseInstance: Base;
  let blockOptions: BlockOptions;
  const iframe = document.createElement('iframe');

  function loreeWrapper() {
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = document.createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    iframe.appendChild(wrapper);
    iframe.innerHTML += customBlockMockData.iframeInnerHtml;
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }

  function handleClickTemplateEdit(
    templateData: Array<{ id: string; thumbnail: string; title: string }>,
  ) {
    customTemplateEvents.templateData.push(templateData[0]);
    const elementSection = customBlockUI.templateThumbnail(
      templateData,
      'loree-my-templates-collapse',
      'global.mytemplate',
      false,
    );
    elementSection
      .getElementsByClassName('custom-block-thumbnail-menu-option')[0]
      .dispatchEvent(new Event('click'));
    document.getElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_EDIT_BUTTON)?.click();
  }

  beforeEach(() => {
    global.sessionStorage = new StorageMock() as _Any;
    loreeWrapper();
    baseInstance = new Base();
    blockOptions = new BlockOptions();
    jest.spyOn(baseInstance, 'updateFeaturesList');
    baseInstance.updateFeaturesList({ features: { saveascustomrow: true, deleterow: true } });
    blockOptions.initiate({});
  });
  test('search bar UI in customBlock', () => {
    const searchWrapper = customBlockUI.searchWrapper();
    expect(searchWrapper.firstChild).toContainHTML('input');
    expect(searchWrapper).toBeInstanceOf(HTMLDivElement);
  });
  test('search bar has autocomplete switched off', () => {
    const searchWrapper = customBlockUI.searchWrapper();
    const input = searchWrapper.firstElementChild;
    expect(input?.getAttribute('autocomplete')).toBe('off');
  });
  test('search bar has autofocus switched on', () => {
    const searchWrapper = customBlockUI.searchWrapper();
    const input = searchWrapper.firstElementChild;
    expect(input?.getAttribute('autofocus')).toBe('');
  });

  test('search autofocus is switched on in customSearchWrapper', () => {
    const searchWrapper = customBlockUI.customBlockSearchWrapper('Row');
    const input = searchWrapper.firstElementChild;
    expect(input?.getAttribute('autofocus')).toBe('');
  });

  test('modal content header in custom block', () => {
    expect(typeof customBlockUI.appendModalContentHeader('Row')).toBe('string');
  });
  test('modal body in custom block', () => {
    expect(typeof customBlockUI.appendModalContentBody()).toBe('string');
  });
  test('modal footer in custom block', () => {
    expect(typeof customBlockUI.appendModelContentFooter()).toBe('string');
  });
  test('custom block search bar', () => {
    expect(customBlockUI.customBlockSearchWrapper('Row')).toBeInstanceOf(HTMLDivElement);
  });
  describe('#template list ui', () => {
    let templateData: [
      {
        id: string;
        thumbnail: string;
        title: string;
        active: boolean;
        categoryID: string;
        content: string;
        category: { id: string; name: string };
      },
    ];
    beforeEach(() => {
      sessionStorage.setItem('domainName', 'BB');
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      templateData = [
        {
          active: true,
          category: {
            id: 'e3c96110-b55e-45c8-bbe5-fd0d2f8db16d',
            name: 'BB-category',
          },
          categoryID: 'e3c96110-b55e-45c8-bbe5-fd0d2f8db16d',
          content:
            ' \n \n<div class="loree-iframe-content-row row" style="padding: 10px; position: relative; margin: 0px;"> \n    <div style="padding: 10px;" class="col-12 loree-iframe-content-column"> \n     <h3 style="color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: \'Source Sans Pro\'; font-size: 38px; text-align: center; border: 0px solid #000000;" class="loree-iframe-content-element bStyle">Unit Overview or Content Page TITLE HERE</h3></div></div>',
          id: '02e948ed-8020-4d9f-b481-87d8cf0562cb',
          thumbnail:
            'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Thumbnail/template/template-1?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3H2MTLIHY%2F20210609%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210609T050821Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEM3%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDmFwLXNvdXRoZWFzdC0yIkcwRQIhALucz7X6hcPmQp4ZTVrF%2Bhh8SHlo7RtB1G9suvvpr6OdAiB63nimCyGlSSpvgK6zH3gKlnhclz2l%2BcgKKxigP7%2FLKyrrBAiG%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAIaDDIyMTI4MzAzNDI5NCIMcSK459b1A9n4Dh5NKr8EWl1lonzpModvnJZGiJd8E2yODGCY%2BMBXOTchwEuZZq1gIpGOUSI48WYXbSZo1f%2BgII6B%2BZ77%2BNux6ldLUjCoZqtAmNYACuTeCACwV86UmU6VF45ShoyoBSUkZBpaHH24WCf3YYgeNSY592lQU1VSYTpAVuAnlenpZ0BNoy%2BMY525PAye8a%2F6I8yJm2%2B450IB440v%2BMQ%2BzvY9SRIS8K7KuRjIZ%2BNy6b9OPTr602efYuGBavPDF0fhvkllPmobq6VhMKMlNMx7Jm0JsawVqjjW9qq73feVEejFWZf7%2F4Y2xmBMjfFf3biCN9r8zw%2BV25PFx7cvm4D28YzsQ%2BoX4ijxrBu%2B0APxKb%2F86wVjE3p724KRQ2ssqEDLcZf3kkP%2BEYoC3j3xHt4YFC5jQ6vTOsclqMm%2F1GwqtWkNobcYNO1h5ChM92NX2c180aMv%2FLCu7ST1fIm4dNaFXfLwbEt5kvy4bt7DYpE3XyuqsoTrFaIv9dUvw4OKuqomQhG6YbYs617NUh1vubhb6Hctf9Y%2B3JvO1E1QAGhm%2Fi92bqYq7RYEU08shyat2WE0d6%2FdP2MElec4mJAn3hKc90OYdjxAdNW%2BpMxvw8qtuuJPK7SqEtHSdsJViDv2KOFDncCniChSbfZrj6WRNzULMS4dv9iMA%2BqP%2B7AcMK2L9LEcMqlDklKcFQKlEtSwYfhBTYruNerqUSom9u%2BUUgm1UqxscUXowLdFQf6p9kJaKwPrmDi5EkerCH8GKH5Ed%2FOWe%2BY0m8j1CJowxZmBhgY6hQIVyhf4OctyckOM0ObViOwrELdxn0bQgI21FZJV0pYQA2aADLOh25ojFF%2FTkENZdisst0%2FICe12PsS2FwFnJ82gjxKZyqPWX%2FjadW0LkXEl5WIVAz7Iz55d6nWLLfxaTqOa1CMdtPDCdDWWjlN3QLPPn9cmKP4AN8UmRfUZXiedit0y5MPLORmHLrAiz2DCdnzd5Xl%2FIu90KKAd0u943NupZlcrw%2FofyRVuAAaiSuYGL6W08n5pFNpQxVUop1WvDGKzdXCNwTIon6Z8naob12YoFPw8Ykd3t19rDWJQFLmXtvDnh02PTYT3UbfSzcHG9DfJ12KOqEppys%2FmDJOwzI1gtzbBpwM%3D&X-Amz-Signature=8da16fe15b2ef074a6e794f79ab39e001282e9ef422973f9d42580248a52af31&X-Amz-SignedHeaders=host&x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Linux%20x86_64%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F90.0.4430.93%20Safari%2F537.36%20aws-amplify%2F3.8.1%20js&x-id=GetObject',
          title: 'template-1',
        },
      ];
      API.graphql = jest.fn().mockImplementation(() => 'success');
    });
    test('my template list', () => {
      const element = customBlockUI.templateThumbnail(
        templateData,
        'loree-my-templates-collapse',
        'My Template',
        false,
      );
      expect(element).toBeInstanceOf(HTMLDivElement);
      expect(element.getElementsByClassName('template-accordion')).toHaveLength(1);
    });
    test('global template list', () => {
      const element = customBlockUI.templateThumbnail(
        templateData,
        'loree-global-templates-collapse',
        'Global Template',
        true,
      );
      expect(element).toBeInstanceOf(HTMLDivElement);
      element.click();
      expect(element.getElementsByClassName('template-accordion')).toHaveLength(1);
    });

    test('show delete option while editing my template', async () => {
      handleClickTemplateEdit(templateData);
      await new Promise((resolve) => setTimeout(resolve, 0));
      const editorContent = iframe.contentDocument?.getElementById(
        CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
      ) as HTMLElement;
      baseInstance.getSelectedElement = jest
        .fn()
        .mockImplementation(() => editorContent.firstElementChild);
      baseInstance.changeSelectedElement(editorContent?.firstElementChild as HTMLElement);
      expect(
        iframe.contentDocument?.getElementById('loree-block-options-delete-button')?.style.display,
      ).toEqual('inline-flex');
    });

    test('search template block', async () => {
      const mock = jest.spyOn(customBlockAction, 'listingCustomTemplates');
      mock.mockImplementation(() => templateData as _Any);
      await customTemplateEvents.retrieveCustomTemplate();
      const searchWrapper = customBlockUI.searchWrapper();
      const input = searchWrapper.firstElementChild as HTMLInputElement;
      input.value = 'my element';
      const element = customTemplateEvents.templatesBlock();
      document.body.append(element, searchWrapper);
      customTemplateEvents.updateTemplateThumbnail();
      const noResultsTag = document.getElementById('loree-My Template-no-results');
      expect(noResultsTag?.tagName).toBe('SPAN');
    });
  });

  describe('#row data display UI', () => {
    let rowData: [
      {
        active: boolean;
        content: string;
        id: string;
        thumbnail: string;
        title: string;
        type: string;
      },
    ];
    beforeEach(() => {
      sessionStorage.setItem('domainName', 'BB');
      rowData = [
        {
          active: true,
          content:
            '<div class="loree-iframe-content-row row row-highlight element-highlight" style="padding: 10px; position: relative; margin: 0px; background: #943131;"> \n <div class="col-12 loree-iframe-content-column" style="padding: 10px;"> \n  <div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper "> \n   <img alt="477393.jpg" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://crystaldelta-eval.blackboard.com/bbcswebdav/pid-52152-dt-content-rid-375829_1/xid-375829_1?VxJw3wfC56=1623145670&amp;Kq3cZcYS15=4f2b64601af14af98b2451080fc5c9ba&amp;3cCnGYSz89=el%2BP8TeKQ%2FET8KBhToBPtmOj2p1outxUzHHLJknY4BQ%3D"> \n  </div> \n </div> \n</div>',
          id: '8d5e442a-b512-4b45-a22b-6e6cf085e43e',
          thumbnail:
            'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Thumbnail/blocks/row-1?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3FS6YJMP3%2F20210608%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210608T122943Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEL3%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDmFwLXNvdXRoZWFzdC0yIkgwRgIhANIz2wIu5SVsPGlqm7PituQO11tg5jmYWlKZNaVp6LhoAiEA2F8wl5Md7dyq%2BiTvWwixNbp4dS9vfRST3M5GK2JE3x4q4gQIdhACGgwyMjEyODMwMzQyOTQiDHFwTf2dp2RV5jwg0iq%2FBEqTX%2F6F6Wi%2FSWhX5RiU2AZ1UFvdKM8xDs0gY6YZx8wjC%2Fn4Bdoyp5eu%2F1%2BTob93o6t7%2BROQ9H4j0Wa1mArgTH5ZU7zECzhEa0uTAVUJI6xv9BQVv0aP1Xx%2B3IxwEfqjNBw7YqcY2J97UxPfHMtCRa6qNXo%2BIH%2B6RWhOgC23E2SjqidpyO5bHGV05FsirzR2oepULjUAgD1o0%2FOpvz5pfKvuny0PuvtMul%2FAlb77mljSchVngZRTQyq6BsSIGgOhnxpgGN2ZsUoWRoYMU8Kq2bcl%2FiaKiW%2Bj1HGO35yDzUDuO7Id9Yst4QzrSkxPJ%2BttnUIwsUrzDGLY%2FfcvLPkFyimv4T4QWpvyejB1DZmFeSBPhknfSCPYxDfBS%2FoV5i9sW5zKPC7rdLOWLf%2F2s77dg0QYXJEk2NyjJ0M%2FfN7V7QmBVYsqF3shZVXRJTwaiyNgNkuxVPOaXGpsVUj%2BKOEKnUO2r3bA2kOb%2B3IA5riMcn6KTuj7RBOeGKowVGW1udMO4BCIW037qs649HI4c1%2BzCCIca0uvI6uIa3uQyseS9YaHvOsdA1n8xU2XHpA8t7PYoAyNaFevIv0211y45MnEmhnkotWp8oEugt63os9v8EiYf6ZG%2BlugcFsvue%2FXWFCAGj4NCEayK5rj80MJ%2Bha4IubEjkJ8BTeZLUnTAVPOwU79umpe0%2F3Ini2g%2FsrcceSWgAXLnNHaU4WYG5Pl0%2BC71qv5bqtqpya3KiyA%2FOhtbS1avut3MlUiu3wKA%2FMyahibMITF%2FYUGOoQCKyey5vqYYvV6G9nz4tuxMLQRCqwyKBWgM7wHqMDrszPDak2hRy2T020elyqQ%2B9oQ36a%2BZFoaEtcnitcCga9IiCSAzH2djn1VZS0o6NycEQTble%2Bm%2BMhDAMi3e4KxAhHS2rGehGDDcwEw8dO%2Byti6vjteYorYVuXZyKezxF4XYmHOEW92mMgw4TPiW%2BuS43NcNCriWvOJNAuujeVnvHhMFlRDQzDv%2FNQ70tokcUnnGXdmP4Gui6y57e0zxdkHmNIkFdfK9McW8rrGN%2FgTbPAAblXhO07kKM%2BTh9TdeUvvzgYRSQk%2Bl4bZ8Cjzvpp8Ac%2BTE%2Fr1dON%2FT%2FmcRNMBRJgZHCQ3D%2BM%3D&X-Amz-Signature=8521c3cd125b9d7e234cf3ce25559dd369a671c2f369ec3edf6cba02f6cc7afa&X-Amz-SignedHeaders=host&x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Linux%20x86_64%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F90.0.4430.93%20Safari%2F537.36%20aws-amplify%2F3.8.1%20js&x-id=GetObject',
          title: 'row-1',
          type: 'Row',
        },
      ];
    });
    test('my row list ui', () => {
      const element = customBlockUI.rowThumbnail(
        rowData,
        'loree-my-rows-collapse',
        'My Row',
        false,
      );
      expect(element).toBeInstanceOf(HTMLDivElement);
      expect(element.getElementsByClassName('row-block-accordion')).toHaveLength(1);
    });
    test('global row list ui', () => {
      const element = customBlockUI.rowThumbnail(
        rowData,
        'loree-global-rows-collapse',
        'Global Row',
        true,
      );
      expect(element).toBeInstanceOf(HTMLDivElement);
      expect(element.getElementsByClassName('row-block-accordion')).toHaveLength(1);
    });
  });
  describe('#element list ui', () => {
    let elementData: [
      {
        active: boolean;
        content: string;
        id: string;
        thumbnail: string;
        title: string;
        type: string;
      },
    ];
    beforeEach(() => {
      sessionStorage.setItem('domainName', 'BB');
      elementData = [
        {
          active: true,
          content:
            '<div class="" style="" id="custom-block"><div class="loree-iframe-content-table-wrapper" style="display:flex;width: 100%; height:auto;overflow-x: auto;margin:0 0 10px;border-width:0;padding:5px;"><table class="loree-iframe-content-table" style="width:100%; height:auto; border: 1px solid #A5A5A5;"><caption class="loree-iframe-content-element" style=" caption-side:bottom; border-width: 0; border-style: solid; border-color: #000000;color: #000000;  padding: 5px; margin:0px">Insert caption here</caption><tbody><tr style="border: 1px solid #A5A5A5;"><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td></tr><tr style="border: 1px solid #A5A5A5;"><td style="border: 1px solid #A5A5A5;padding: 5px;" class=""><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td><td style="border: 1px solid #A5A5A5;padding: 5px;"><p class="loree-iframe-content-element" style="border-width: 0px; border-style: solid; border-color: rgb(0, 0, 0); color: rgb(0, 0, 0); padding: 5px; margin: 0px; font-family: SourceSansPro; font-size: 20px;"> Insert text here</p></td></tr></tbody></table></div></div>',
          id: '2b12aa4d-1203-4ae7-bf64-22760e1a84ad',
          thumbnail:
            'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Thumbnail/blocks/column-2?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3EEUJABMD%2F20210609%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210609T052113Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEM3%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDmFwLXNvdXRoZWFzdC0yIkgwRgIhAN8Hgtl0vQZoSejLxZl10S%2BJBXClc4q%2FqOzRGXH6PRwNAiEAz60gf6gnETHsuyEf2TgNd2JuOopoaxbykovlDCqVujoq6wQIhv%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FARACGgwyMjEyODMwMzQyOTQiDO%2BkslQYaEglbz%2Fdbiq%2FBMUJ%2F%2BJK7NRnrOdyH290fB4RBbBO6duq21Gnng7jKiktKwxA6fD9HdF5rL7cvG0e%2F0eg%2FknhSRQk%2FUQS6sZV9D9vsOFGdfjAFKIq3QE0j8Xwo5ie2PBfS4qkNb73epLph5PVV5L94sYEMXCHsyIk74C3QV410iZbPEb9eYaJOTkmtQdorP1LPHUd1Du5Tphhc0YMy5NFWkVTUlOan0t13Gr9Eofyxrk%2FKeMwCIwIDsRLW0rpC1WOBG6b%2FFFkMDMhN%2BseMNML768u6zJ8IwMDcOjqz%2FZ1sT%2BXsB2qk5PQjnevXxZUpwBRg3w5OJdmeMUq9kwsnCXle6F9w4ILOnwJLovcohBDqGTTai7QWm5EYrjajjyUPpvyfzA%2Bz8BWW6u4%2Bu8jNdMl8NxMW5D9ozdxVl1NE6GIQ6OV4En2EZ87WWNA0XrHS3bg4DcwTJhXiHhQCK3cUjnDp3G4cNnXNTqPe%2BBYd8IEg80iTEhP4%2FR8GeyH1CV9rgUscIi1wFu1RDh%2BXGlySMMYI1rtCLePeIDEZFeaUqqu1T%2BGJ4voE84wfCZwghOwCbCwO5nbOAAQvkni206gqn%2BP4cZ1%2FwWGPPZCmUx1t3rzy%2B%2B%2FvE8gJgjfj7EhbUYJahGyWK%2F8PKzZ6GZZA2aY4Ew6ciOejmNsgjZeBcxQd0%2BgQ9c4Cndc9QcWB2hPeYwvsiuNpqOEGBflPMuzCJGuO7rPgWxYL40UzyZUfcm3a4mbrQwv9ASeG30hl2kbKmxergWIFTQtEsuS58KuMMmfgYYGOoQCnppocSifVbe5KpecnvA9z%2BphURlV4bWKs6AeifMFXvLv08nCnLkXsCCUMafhsR84GaAEM6QlZam59nVsCStq3gLI9wFF%2BLcusY4DtpGCPaX5ChfQr4%2Bb8oyATyIBmIE%2FXLqm7zUXqpSQc8Y1oOhfMYyQmZ1%2F0KCKkXsM1%2FCcj8CeNyrf1TpYNwmixuleKN7o7QVMbKLDbYBKMaTIyFcg7i9x8nG4ffpFEskgEZCgStHg177HpfEv4tp3WpS%2FHCDCF3GGBS9ri3G3%2F3A2UsSNjlEH%2FGF26Y%2FLjODqZTUJQE6FQdnMbzxUEoi89FkJObFfYE2pp%2BAOlJ%2F3nUrs0aRAcOaTB%2FI%3D&X-Amz-Signature=85123df4d430e742585d8cc681aa0e9107fd605ebc5571fe9cca03f6b8857672&X-Amz-SignedHeaders=host&x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Linux%20x86_64%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F90.0.4430.93%20Safari%2F537.36%20aws-amplify%2F3.8.1%20js&x-id=GetObject',
          title: 'column-2',
          type: 'Elements',
        },
      ];
    });
    test('my element list', () => {
      const element = customBlockUI.elementThumbnail(
        elementData,
        'loree-my-elements-collapse',
        'My Element',
        false,
      );
      expect(element).toBeInstanceOf(HTMLDivElement);
      expect(element.getElementsByClassName('element-block-accordion')).toHaveLength(1);
    });
    test('global element list', () => {
      const element = customBlockUI.elementThumbnail(
        elementData,
        'loree-global-elements-collapse',
        'Global Element',
        true,
      );
      expect(element).toBeInstanceOf(HTMLDivElement);
      expect(element.getElementsByClassName('element-block-accordion')).toHaveLength(1);
    });
  });
});

describe('#customRows', () => {
  let rowInstance: Loree;
  let baseInstance: Base;
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    Amplify.configure(awsconfig);
    const user = {
      attributes: userMockData,
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => globalBlocksResponse)
      .mockImplementationOnce(() => categoryResponse)
      .mockImplementationOnce(() => blocksResponse);
    document.body.innerHTML = Bootstrap;
    document.body.innerHTML += `<div id=${CONSTANTS.LOREE_WRAPPER} class="loree"></div>`;
    rowInstance = new Loree('loreeEditorWrapper');
    baseInstance = new Base();
    rowInstance.initiate({
      features: { customrowfiltersearch: true, myinteractive: false },
      header: {},
      textOptions: {},
      customColor: [],
      customFonts: {},
      customHeaderStyle: { customHeader: [] },
      customLinkStyle: {},
    });
  });
  test('click on add row button', () => {
    baseInstance.shrinkSpaceWidth = jest.fn().mockImplementation(() => '');
    baseInstance.handleAddRowButtonClick();
    expect(document.getElementById(CONSTANTS.LOREE_SIDEBAR_ROW_SECTION)?.style.display).toEqual(
      'block',
    );
  });
  test('click on custom row option', async () => {
    expect(document.getElementsByClassName('sidebarRowsLoader')[0]).toBeUndefined();
    sessionStorage.setItem('domainName', 'BB');
    const mockApiData = {
      data: { listKalturaConfigs: { items: [] } },
    };
    API.graphql = jest.fn().mockImplementationOnce(() => mockApiData);
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => globalblocksResponseOne)
      .mockImplementationOnce(() => globalblocksResponse);
    document.getElementById('customRows')?.click();
    expect(document.getElementsByClassName('sidebarRowsLoader')[0]).not.toBeUndefined();
  });
});
