import { API, graphqlOperation } from 'aws-amplify';
import {
  createSharedBlocks,
  createSharedTemplates,
  updateCustomBlock,
  updateCustomTemplate,
  deleteSharedTemplates,
  deleteSharedBlocks,
} from '../../../graphql/mutations';
import {
  accountByCourse,
  adminSubAccounts,
  courseDetails,
  getCustomBlock,
  getCustomTemplate,
  adminAccounts,
} from '../../../graphql/queries';
import {
  checkedAccountIds,
  hideShareTemplateModal,
  LMSLoader,
  saveSuccessAlert,
  sharedAccounts,
  unCheckedAccountIds,
} from './customBlocksUI';
import { clearTemplateSearch, refreshTemplateList } from './customTemplateEvents';
import { refreshCustomBlockThumbnail } from './customBlockEvents';
import { uploadToS3 } from '../../../lti/admin/globalImagesUpload/uploadImageToS3';
import CONSTANTS from '../../constant';
import { getSelectedSharedBlocks, getSelectedSharedTemplate } from './customBlocksAction';
import { getAccountByCourseFilter, getCourseDetailFilter } from './blocksQueryUtils';
import { getButtonById } from '../../common/dom';

export const handleShareBlocks = async (selectedElementId: string, blockType: string) => {
  const shareBtnEnable = getButtonById('share-button');
  shareBtnEnable.innerHTML = 'Sharing...';
  shareBtnEnable.disabled = true;
  hideShareTemplateModal();
  LMSLoader();
  const selectedBlockData: _Any = await getSelectedBlockDetail(selectedElementId, blockType);
  const Loader = document.getElementById(CONSTANTS.LOREE_SHARED_OPTIONS_LOADER) as HTMLElement;
  Loader.style.display = 'flex';
  const sharedContent: string = await uploadToS3(selectedBlockData.content, `Shared-${blockType}`);
  if (blockType === 'Template') {
    clearTemplateSearch();
    for (const accountDetail of sharedAccounts) {
      const sharedTemplates: _Any = await getSelectedSharedTemplate(
        accountDetail,
        selectedBlockData.id,
      );
      const sharedTemplatesItems = sharedTemplates.data.listSharedTemplatess.items;
      if (sharedTemplatesItems.length === 0) {
        const shareInput = {
          customTemplateID: selectedBlockData.id,
          ltiPlatformID: selectedBlockData.ltiPlatformID,
          title: selectedBlockData.title,
          categoryID: selectedBlockData.categoryID,
          thumbnail: selectedBlockData.thumbnail,
          content: sharedContent,
          createdBy: selectedBlockData.createdBy,
          sharedAccountId: accountDetail,
          active: true,
        };
        await API.graphql(graphqlOperation(createSharedTemplates, { input: shareInput }));
        const updateInput = {
          id: selectedElementId,
          isShared: true,
        };
        await API.graphql(graphqlOperation(updateCustomTemplate, { input: updateInput }));
      }
    }
    for (const unshareAccount of unCheckedAccountIds) {
      const sharedTemplates: _Any = await getSelectedSharedTemplate(
        unshareAccount,
        selectedBlockData.id,
      );
      const sharedTemplatesItems = sharedTemplates.data.listSharedTemplatess.items;
      if (sharedTemplatesItems.length > 0) {
        const unShareInput = {
          id: sharedTemplatesItems[0].id,
        };
        await API.graphql(graphqlOperation(deleteSharedTemplates, { input: unShareInput }));
      }
    }
    if (checkedAccountIds.length > 0 && sharedAccounts.length === 0) {
      const isAllUnshared = isAllUnShared(checkedAccountIds, unCheckedAccountIds);
      if (isAllUnshared) {
        const updateInput = {
          id: selectedElementId,
          isShared: false,
        };
        await API.graphql(graphqlOperation(updateCustomTemplate, { input: updateInput }));
      }
    }

    await refreshTemplateList('sharedTemplates');
    saveSuccessAlert('Template');
    sharedAccounts.splice(0, sharedAccounts.length);
    Loader.style.display = 'none';
  } else {
    for (const accountDetail of sharedAccounts) {
      const sharedBlocks: _Any = await getSelectedSharedBlocks(accountDetail, selectedBlockData.id);
      const sharedBlocksItems = sharedBlocks.data.listSharedBlockss.items;
      if (sharedBlocksItems.length === 0) {
        const shareInput = {
          customBlockID: selectedBlockData.id,
          ltiPlatformID: selectedBlockData.ltiPlatformID,
          title: selectedBlockData.title,
          categoryID: selectedBlockData.categoryID,
          thumbnail: selectedBlockData.thumbnail,
          content: sharedContent,
          createdBy: selectedBlockData.createdBy,
          sharedAccountId: accountDetail,
          active: true,
          type: blockType,
        };
        await API.graphql(graphqlOperation(createSharedBlocks, { input: shareInput }));
        const updateInput = {
          id: selectedElementId,
          isShared: true,
        };
        await API.graphql(graphqlOperation(updateCustomBlock, { input: updateInput }));
      }
    }
    for (const unshareAccount of unCheckedAccountIds) {
      const sharedBlocks: _Any = await getSelectedSharedBlocks(
        unshareAccount,
        selectedBlockData.id,
      );
      const sharedBlockssItems = sharedBlocks.data.listSharedBlockss.items;
      if (sharedBlockssItems.length > 0) {
        const unShareInput = {
          id: sharedBlockssItems[0].id,
        };
        await API.graphql(graphqlOperation(deleteSharedBlocks, { input: unShareInput }));
      }
    }
    if (checkedAccountIds.length > 0 && sharedAccounts.length === 0) {
      const isAllUnshared = isAllUnShared(checkedAccountIds, unCheckedAccountIds);
      if (isAllUnshared) {
        const updateInput = {
          id: selectedElementId,
          isShared: false,
        };
        await API.graphql(graphqlOperation(updateCustomBlock, { input: updateInput }));
      }
    }
    await refreshCustomBlockThumbnail(blockType, 'refreshSharedBlocks');
    saveSuccessAlert(blockType);
    sharedAccounts.splice(0, sharedAccounts.length);
    Loader.style.display = 'none';
  }
  Loader.style.display = 'none';
};

export const handleAccounts = async () => {
  if (sessionStorage.getItem('isAdmin') === 'true') {
    const getAdminAccount: _Any = await API.graphql(graphqlOperation(adminAccounts));
    const subAccounts: _Any = await API.graphql(
      graphqlOperation(adminSubAccounts, {
        accountId: 1,
      }),
    );
    const subAccountData = [JSON.parse(getAdminAccount.data.adminAccounts).body[0]].concat(
      JSON.parse(subAccounts.data.adminSubAccounts).body,
    );
    return subAccountData;
  } else {
    const array = [];
    const courses: _Any = await API.graphql(
      graphqlOperation(courseDetails, getCourseDetailFilter()),
    );
    const accounts: _Any = await API.graphql(
      graphqlOperation(
        accountByCourse,
        getAccountByCourseFilter(JSON.parse(courses.data.courseDetails).body.account_id),
      ),
    );
    array.push(JSON.parse(accounts.data.accountByCourse).body);
    return array;
  }
};

export const getSelectedBlockDetail = async (selectedTemplateId: string, type: string) => {
  if (type === 'Row' || type === 'Elements') {
    const selectedBlockData: _Any = await API.graphql(
      graphqlOperation(getCustomBlock, { id: selectedTemplateId }),
    );
    const selectedTemplateDetails = selectedBlockData.data.getCustomBlock;
    return selectedTemplateDetails;
  } else {
    const selectedBlockData: _Any = await API.graphql(
      graphqlOperation(getCustomTemplate, { id: selectedTemplateId }),
    );
    const selectedTemplateDetails = selectedBlockData.data.getCustomTemplate;
    return selectedTemplateDetails;
  }
};

export const handleAccountByCurrentCourse = async () => {
  const courses: _Any = await API.graphql(graphqlOperation(courseDetails, getCourseDetailFilter()));
  const accounts: _Any = await API.graphql(
    graphqlOperation(
      accountByCourse,
      getAccountByCourseFilter(JSON.parse(courses.data.courseDetails).body.account_id),
    ),
  );
  return JSON.parse(accounts.data.accountByCourse).body;
};

export const handleCurrentShare = async (selectedBlockId: _Any, type: string) => {
  if (type === 'Template') {
    const sharedTemplates: _Any = await getSelectedSharedTemplate('', selectedBlockId);
    return sharedTemplates.data.listSharedTemplatess.items;
  } else {
    const sharedBlocks: _Any = await getSelectedSharedBlocks('', selectedBlockId);
    return sharedBlocks.data.listSharedBlockss.items;
  }
};

const isAllUnShared = (first: _Any, second: string[]) => {
  // check if all template/block unchecked
  if (first.length !== second.length) {
    return false;
  }
  for (let i = 0; i < first.length; i++) {
    if (!second.includes(first[i].sharedAccountId)) {
      return false;
    }
  }
  return true;
};
