import { isCanvasAndBB } from '../../../lmsConfig';
import { getS3urlFromKey } from '../../../lti/admin/globalImagesUpload/uploadImageToS3';
import Base from '../../base';
import { DataContent } from './customBlockAppenders';

const baseClass = () => {
  const base = new Base();
  return base;
};
export const rowMigration = async (data: DataContent) => {
  let editorContent: string = !isCanvasAndBB() ? data.content : await getS3urlFromKey(data.content);
  const cleanedHtml = baseClass().convertContent(editorContent);

  let structuredHtml = '';
  if (!baseClass().isBlank(cleanedHtml)) {
    structuredHtml = baseClass().migrate.migrateElementsWithoutRowColumn(cleanedHtml);
  }
  editorContent = baseClass().removeEmptyTags(structuredHtml);
  return editorContent;
};

export const elementMigration = async (data: DataContent) => {
  let editorContent: string = !isCanvasAndBB() ? data.content : await getS3urlFromKey(data.content);
  editorContent = baseClass().convertContent(editorContent);
  return editorContent;
};
