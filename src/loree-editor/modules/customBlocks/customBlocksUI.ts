/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
import CONSTANTS from '../../constant';
import { Icon, videoModalIcons } from '../sidebar/iconHolder';
import {
  accordionIconClick,
  handleAddCustomTemplate,
  updateTemplateThumbnail,
} from './customTemplateEvents';
import { customElementAlert, successAlertIcon } from '../../alert';
import {
  handleAddCustomRow,
  handleAddCustomElement,
  blockAccordionIconClick,
} from './customBlockEvents';
import { appendThumbnailMenu } from './customBlockAppenders';
import { handleAccounts, handleCurrentShare, handleShareBlocks } from './sharedBlocks';
import { isCanvasAndBB } from '../../../lmsConfig/index';
import { getEditorElementsByClassName, getEditorElementById } from '../../utils';
import { translate } from '../../../i18n/translate';

const closeIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="7.586" height="7.587" viewBox="0 0 7.586 7.587">
<path id="close" d="M-5641.125-5116.2l-2.672-2.673-2.673,2.673a.657.657,0,0,1-.929,0,.657.657,0,0,1,0-.929l2.673-2.673-2.673-2.673a.656.656,0,0,1,0-.928.659.659,0,0,1,.929,0l2.673,2.673,2.672-2.673a.658.658,0,0,1,.928,0,.655.655,0,0,1,0,.928l-2.673,2.673,2.673,2.673a.656.656,0,0,1,0,.929.654.654,0,0,1-.464.192A.656.656,0,0,1-5641.125-5116.2Z" transform="translate(5647.591 5123.591)"/>
</svg>`;
export let sharedAccounts: Array<string> = [];
export let checkedAccountIds: Array<string> = [];
export let unCheckedAccountIds: Array<string> = [];
type ThumbnailDataType = Array<{ id: string; thumbnail: string; title: string }>;
export const searchWrapper = () => {
  // template search
  const searchWrapper = document.createElement('div');
  searchWrapper.id = CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT;
  const searchInput = document.createElement('input');
  searchInput.id = CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT_VALUE;
  searchInput.className = 'loree-sidebar-custom-block-search-input';
  searchInput.name = 'search';
  searchInput.type = 'text';
  searchInput.autocomplete = 'off';
  searchInput.placeholder = translate('global.search');
  // autofocus was accidentally removed from Typescript
  // this is a bug in version 4.4 of typescript, we are good ;-)
  // @ts-expect-error
  searchInput.autofocus = true;
  searchInput.oninput = () => updateTemplateThumbnail();
  const searchIcon = document.createElement('div');
  searchIcon.id = CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_ICON;
  searchIcon.className = 'loree-sidebar-custom-template-search-filter-icon-style';
  searchIcon.innerHTML = Icon.searchIcon;
  searchWrapper.appendChild(searchInput);
  searchWrapper.appendChild(searchIcon);
  return searchWrapper;
};

// custom block UI
export const appendModalContentHeader = (type: string) => {
  return `<div class="modal-header p-0 mx-0" id=${CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_HEADER}>
  <h5 class="modal-title text-primary" id=${CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_TITLE}>${translate('modal.savecustom')} ${type}</h5>
  <button type="button" id=${CONSTANTS.LOREE_CUSTOM_BLOCK_CLOSE_ICON} class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div><span class ="loree-custom-block-section-divider" ></span>`;
};

// Custom block save modal
export const appendModalContentBody = () => {
  return `
  <div class="modal-body p-0">
        <form>
          <div class="form-group">
            <label for=${CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME} id=${
    CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME_LABEL
  } class="col-form-label">${translate('modal.customrowname*')}</label>
            <input type="text" class="form-control" id=${
              CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME
            } autocomplete="off" placeholder=${translate('modal.customrowname')}>
          <small id="loree-row-name" class="d-none text-danger ml-2"></small>
          </div>
          <div class="form-group">
          <label for="category-list" class="col-form-label">${translate('modal.categoryasterisk')}</label>
          <div class="dropdown dropdown-layout" id=${
            CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN_CONTAINER
          }>
          <button id=${
            CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN
          } class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${translate(
    'global.selectcategory',
  )}</button>
          <div class="dropdown-menu" id=${
            CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN_ITEM
          } aria-labelledby=${CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN}>
          </div>
          </div>
          </div>
          <div class="form-group d-none" id=${CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_GROUP}>
          <label for=${CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME} id=${
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME_LABEL
  } class="col-form-label">${translate('modal.categoryname*')}</label>
          <input type="text" class="form-control" id=${
            CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME
          } autocomplete="off" placeholder=${translate('modal.categoryname')}>
          <small id="loree-row-cat-name" class="d-none text-danger ml-2">${translate(
            'global.categorynamealreadyexists',
          )}</small>
        </div>
        <div id='update-info-content-text' class="alert alert-info d-none d-flex" role="alert">
        <span class="update-info-icon">${successAlertIcon}</span>
        <p class="mb-0 ml-1" style="font-size: 12px;">${translate(
          'modal.aftersavingyouwillberedirectedtodashboard',
        )}</p>
      </div>
        </form>
      </div>
  `;
};

export const appendModelContentFooter = () => {
  return `
  <div id=${
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER
  } class="d-flex flex-row flex-nowrap align-items-center">
    <div style="margin: auto;">
      <button id=${CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_CANCEL_BUTTON} class="mx-1">
        ${translate('global.cancel')}
      </button>
      <button class="ok-button mx-1" id=${
        CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON
      } disabled='disabled'>
      ${translate('global.save')}
      </button>
    </div>
</div>
  `;
};

export const appendCategoryDropdownList = (categoryLists: Array<{ id: string; name: string }>) => {
  return `    <div class="dropdown-item loree-custom-block-category" id=${
    CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_OPTION
  }>${translate('modal.addnewcategory')}</div>
  ${categoryLists
    .map(
      (value) =>
        `<div class="dropdown-item loree-custom-block-category" id=${value.id}>${value.name}</div>`,
    )
    .join('')}`;
};

export const categoryListLoader = () => {
  return `
  <div class="d-flex justify-content-center">
    <div class="spinner-border" role="status">
      <span class="sr-only">${translate('global.loading')}</span>
    </div>
  </div>
  `;
};

export const customBlockSearchWrapper = (type: string) => {
  // custom block search
  const searchWrapper = document.createElement('div');
  searchWrapper.id = 'loree-sidebar-custom-row-search-input';
  const searchInput = document.createElement('input');
  searchInput.id = `loree-sidebar-custom-${type}-search-input-value`;
  searchInput.className = 'loree-sidebar-custom-block-search-input';
  searchInput.name = 'search';
  searchInput.type = 'text';
  searchInput.autocomplete = 'off';
  searchInput.placeholder = translate('global.search');
  // @ts-expect-error
  searchInput.autofocus = true;
  const searchIcon = document.createElement('div');
  searchIcon.id = 'loree-sidebar-custom-search-filter-icon';
  searchIcon.className = 'loree-sidebar-custom-search-filter-icon-style';
  searchIcon.innerHTML = Icon.searchIcon;
  searchWrapper.appendChild(searchInput);
  searchWrapper.appendChild(searchIcon);
  return searchWrapper;
};

export const templateThumbnail = (
  templateData: ThumbnailDataType,
  templateId: string,
  templateType: string,
  isGlobal: boolean,
) => {
  const defaultCollapse = isCanvasAndBB() ? 'Global' : 'My Template';
  const templatesParent = document.createElement('div');
  templatesParent.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${templateId}`;
  templateType.includes(defaultCollapse)
    ? headerText.setAttribute('aria-expanded', 'true')
    : headerText.setAttribute('aria-expanded', 'false');
  headerText.className = 'sectionLabel d-block';
  headerText.setAttribute('aria-expanded', 'false');
  headerText.innerHTML = `<span id="template-accordion-header"><span class="accordion-icon template-accordion ${
    templateType.includes(defaultCollapse) ? 'text-primary border-primary' : ''
  }">${templateType.includes(defaultCollapse) ? 'I' : '+'}</span><span class="font-weight-bold ${
    templateType.includes(defaultCollapse) ? 'text-primary' : ''
  }">${templateType}s</span></span>`;
  headerText.onclick = async (e) => await accordionIconClick(e, templateType);
  const accordionBody = document.createElement('div');
  accordionBody.id = templateId;
  accordionBody.dataset.parent = '#loree-templates-link-accordion-wrapper';
  accordionBody.className = `collapse mb-3 ${
    templateType.includes(defaultCollapse) ? 'show' : ''
  } ${(templateData.length > 2 || templateType !== 'Global') && 'custom-scrollbar'}`;
  if (templateData.length === 0) {
    accordionBody.classList.add('loree-custom-block-no-results');
    accordionBody.innerHTML = translate('global.noresultsfound');
  } else {
    for (const data of templateData) {
      const templateElement = document.createElement('div');
      templateElement.className = 'panel mb-3';
      templateElement.id = data.id;
      const rowThumbnail = document.createElement('div');
      rowThumbnail.className = 'custom-blocks-thumbnail-template';
      if (!isGlobal) rowThumbnail.prepend(appendThumbnailMenu(templateType));
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = data.thumbnail;
      thumbnailImg.className = 'loree-sidebar-custom-block-thumbnail';
      rowThumbnail.append(thumbnailImg);
      const title = document.createElement('p');
      title.className = 'custom-block-title mb-0';
      title.innerHTML = data.title;
      title.title = data.title;
      rowThumbnail.onclick = (e): void => handleAddCustomTemplate(e, templateType);
      templateElement.append(rowThumbnail);
      templateElement.appendChild(title);
      accordionBody.append(templateElement);
    }
  }
  const horizontalRuler = document.createElement('hr');
  templatesParent.appendChild(headerText);
  templatesParent.appendChild(accordionBody);
  templatesParent.appendChild(horizontalRuler);
  return templatesParent;
};

export const rowThumbnail = (
  rowData: ThumbnailDataType,
  rowId: string,
  rowType: string,
  isGlobal: boolean,
) => {
  const defaultCollapse = isCanvasAndBB() ? 'Global' : 'My Row';
  const rowParent = document.createElement('div');
  rowParent.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${rowId}`;
  headerText.className = ' sectionLabel d-block';
  if (rowType.includes(defaultCollapse)) {
    headerText.setAttribute('aria-expanded', 'true');
  } else {
    headerText.setAttribute('aria-expanded', 'false');
  }
  headerText.innerHTML = `<span id="row-accordion-header"><span class="accordion-icon row-block-accordion">${
    rowType.includes(defaultCollapse) ? 'I' : '+'
  }</span><span class="font-weight-bold">${rowType}s</span></span>`;
  headerText.onclick = async (e) =>
    await blockAccordionIconClick(e, rowType, 'row-block-accordion');
  const accordionBody = document.createElement('div');
  accordionBody.id = rowId;
  accordionBody.dataset.parent = '#loree-rows-link-accordion-wrapper';
  accordionBody.className = `collapse mb-3 ${rowType.includes(defaultCollapse) ? 'show' : ''}  custom-scrollbar`;
  if (rowData.length === 0) {
    accordionBody.classList.add('loree-custom-block-no-results');
    accordionBody.innerHTML = translate('global.noresultsfound');
  } else {
    for (const data of rowData) {
      const rowElement = document.createElement('div');
      rowElement.className = 'panel mb-3';
      rowElement.id = data.id;
      const rowThumbnail = document.createElement('div');
      rowThumbnail.className = 'custom-blocks-thumbnail-row';
      if (!isGlobal) rowThumbnail.prepend(appendThumbnailMenu(rowType));
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = data.thumbnail;
      thumbnailImg.className = 'loree-sidebar-custom-block-thumbnail';
      rowThumbnail.append(thumbnailImg);
      const title = document.createElement('p');
      title.className = 'custom-block-title mb-0';
      title.innerHTML = data.title;
      title.title = data.title;
      rowThumbnail.onclick = async (e) => await handleAddCustomRow(e, rowType);
      rowElement.append(rowThumbnail);
      rowElement.appendChild(title);
      accordionBody.append(rowElement);
    }
  }
  const horizontalRuler = document.createElement('hr');
  rowParent.appendChild(headerText);
  rowParent.appendChild(accordionBody);
  rowParent.appendChild(horizontalRuler);
  return rowParent;
};

export const elementThumbnail = (
  elementData: ThumbnailDataType,
  elementId: string,
  elementType: string,
  isGlobal: boolean,
) => {
  const defaultCollapse = isCanvasAndBB() ? 'Global' : 'My Element';
  const elementsParent = document.createElement('div');
  elementsParent.className = 'panel';
  const headerText = document.createElement('p');
  headerText.dataset.toggle = 'collapse';
  headerText.dataset.target = `#${elementId}`;
  headerText.className = 'sectionLabel d-block';
  if (elementType.includes(defaultCollapse)) {
    headerText.setAttribute('aria-expanded', 'true');
  } else {
    headerText.setAttribute('aria-expanded', 'false');
  }
  headerText.innerHTML = `<span id="element-accordion-header"><span class="accordion-icon element-block-accordion">${
    elementType.includes(defaultCollapse) ? 'I' : '+'
  }</span><span class="font-weight-bold">${elementType}s</span></span>`;
  headerText.onclick = async (e) =>
    await blockAccordionIconClick(e, elementType, 'element-block-accordion');
  const accordionBody = document.createElement('div');
  accordionBody.id = elementId;
  accordionBody.dataset.parent = '#loree-elements-link-accordion-wrapper';
  accordionBody.className = `collapse mb-3 ${elementType.includes(defaultCollapse) ? 'show' : ''} custom-scrollbar`;
  if (elementData.length === 0) {
    accordionBody.classList.add('loree-custom-block-no-results');
    accordionBody.innerHTML = translate('global.noresultsfound');
  } else {
    for (const data of elementData) {
      const rowElement = document.createElement('div');
      rowElement.className = 'panel mb-3';
      rowElement.id = data.id;
      const elementThumbnail = document.createElement('div');
      elementThumbnail.className = 'custom-blocks-thumbnail-row';
      if (!isGlobal) elementThumbnail.prepend(appendThumbnailMenu(elementType));
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = data.thumbnail;
      thumbnailImg.className = 'loree-sidebar-custom-block-thumbnail';
      elementThumbnail.append(thumbnailImg);
      const title = document.createElement('p');
      title.className = 'custom-block-title mb-0';
      title.innerHTML = data.title;
      title.title = data.title;
      elementThumbnail.onclick = async (e) => await handleAddCustomElement(e, elementType);
      rowElement.append(elementThumbnail);
      rowElement.appendChild(title);
      accordionBody.append(rowElement);
    }
  }
  const horizontalRuler = document.createElement('hr');
  elementsParent.appendChild(headerText);
  elementsParent.appendChild(accordionBody);
  elementsParent.appendChild(horizontalRuler);
  return elementsParent;
};

export const openShareModal = async (menuWrapper: HTMLElement, type: string) => {
  sharedAccounts = [];
  checkedAccountIds = [];
  unCheckedAccountIds = [];
  await appendShareModal(menuWrapper, type);
  await getSharedAccountList(menuWrapper, type);
};

const enableShareButton = (e: MouseEvent) => {
  const shareCheckBox = getEditorElementsByClassName(
    CONSTANTS.LOREE_CUSTOM_BLOCK_SHARE_INPUT,
  ) as HTMLCollectionOf<HTMLInputElement>;
  const eventTarget = e.currentTarget as HTMLInputElement;
  const shareBtn = getEditorElementById('share-button') as HTMLButtonElement;
  if (eventTarget.id && eventTarget.checked) {
    sharedAccounts.push(eventTarget.id);
    if (!unCheckedAccountIds.includes(eventTarget.id))
      unCheckedAccountIds.splice(unCheckedAccountIds.indexOf(eventTarget.id), 1);
  } else {
    if (!sharedAccounts.includes(eventTarget.id))
      sharedAccounts.splice(sharedAccounts.indexOf(eventTarget.id), 1);
    const isCheck = checkedAccountIds.filter((data: _Any) => {
      return data.sharedAccountId === eventTarget.id;
    });
    if (isCheck.length !== 0 && !unCheckedAccountIds.includes(eventTarget.id))
      unCheckedAccountIds.push(eventTarget.id);
  }
  if (eventTarget.checked || isChecked(shareCheckBox) || unCheckedAccountIds.length > 0)
    shareBtn.disabled = false;
  else shareBtn.disabled = true;
};

const isChecked = (shareCheckBox: HTMLCollectionOf<HTMLInputElement>) => {
  for (const check of shareCheckBox) {
    if (check.checked) return true;
  }
  return false;
};

export const hideShareTemplateModal = () => {
  const newPageOptionsWrapper = document.getElementById(CONSTANTS.LOREE_SHARE_TEMPLATE_OPTIONS);
  if (newPageOptionsWrapper) {
    newPageOptionsWrapper.remove();
  }
};

export const appendShareModal = async (menuWrapper: HTMLElement, type: string) => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  const shareModeAlertWrapper = document.createElement('div');
  shareModeAlertWrapper.className = 'editor-alert-modal shared-account-modal';
  shareModeAlertWrapper.style.display = 'flex';
  shareModeAlertWrapper.id = CONSTANTS.LOREE_SHARE_TEMPLATE_OPTIONS;
  const shareAlertContent = document.createElement('div');
  shareAlertContent.className = 'col-11 col-md-8 col-xl-6 modal-content px-0';
  // Header
  const alertHeader = document.createElement('div');
  alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
  alertHeader.innerHTML = `<h4 class="modal-title">${translate('global.share')} ${type}</h4>`;
  const alertCloseButton = document.createElement('div');
  alertCloseButton.className = 'btn-icon';
  alertCloseButton.innerHTML = closeIcon;
  alertCloseButton.onclick = (): void => hideShareTemplateModal();
  alertHeader.appendChild(alertCloseButton);
  // Body
  const alertBody = document.createElement('div');
  alertBody.className = 'modal-body';
  const alertBodyPlainContent = document.createElement('p');
  alertBodyPlainContent.className = 'editor-modal-statement';
  const templateTitle = `<div class="form-group">
  <p>${type} Title</p>
  <input type="text" class="form-control w-25 h-25" value="${
    (menuWrapper.parentElement?.parentElement?.childNodes[1] as HTMLElement).innerText
  }" readonly>
</div>`;
  const accountList = document.createElement('div');
  accountList.innerText = translate('modal.selectdepartmenttoshare');
  const list = document.createElement('div');
  list.className = 'form-check m-3';
  list.id = 'list-check-share';
  accountList.appendChild(list);
  alertBodyPlainContent.innerHTML += templateTitle;
  alertBodyPlainContent.appendChild(accountList);
  // Footer
  const alertFooter = document.createElement('div');
  alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
  const modalYes = document.createElement('button');
  modalYes.id = 'share-button';
  modalYes.setAttribute('disabled', 'true');
  modalYes.className = 'btn editor-btn-primary mx-1 ';
  modalYes.innerHTML = translate('global.share');
  const thumbnailId = menuWrapper.parentElement?.parentElement?.id;
  modalYes.onclick = async () => (thumbnailId ? await handleShareBlocks(thumbnailId, type) : '');
  const modalNo = document.createElement('button');
  modalNo.className = 'btn btn-primary mx-1';
  modalNo.innerHTML = translate('global.cancel');
  modalNo.onclick = () => hideShareTemplateModal();
  shareAlertContent.appendChild(alertHeader); // header append
  shareAlertContent.appendChild(alertBody);
  alertBody.appendChild(alertBodyPlainContent);
  shareAlertContent.appendChild(alertFooter);
  alertFooter.appendChild(modalNo);
  alertFooter.appendChild(modalYes);
  shareModeAlertWrapper.appendChild(shareAlertContent);
  loreeWrapper?.appendChild(shareModeAlertWrapper);
};

const getSharedAccountList = async (menuWrapper: HTMLElement, type: string) => {
  const list = document.getElementById('list-check-share');
  const loader = document.createElement('div');
  loader.id = 'share-option-loader';
  list?.insertBefore(loader, null);
  if (loader) {
    loader.innerHTML = loaderElement();
  }
  const enableShare = await handleCurrentShare(menuWrapper.parentElement?.parentElement?.id, type);
  const accountDetails = await handleAccounts();
  loader.remove();
  if (list) list.style.columns = 'auto 3';
  for (const departmentData of accountDetails) {
    const orderedList = document.createElement('ol');
    orderedList.className = 'subaccount-list';
    const inputCheckBox = document.createElement('input');
    inputCheckBox.type = 'checkbox';
    inputCheckBox.className = `form-check-input ${CONSTANTS.LOREE_CUSTOM_BLOCK_SHARE_INPUT}`;
    inputCheckBox.onclick = (e) => enableShareButton(e);
    const isCheck = enableShare.filter((data: { sharedAccountId: string }) => {
      return parseInt(data.sharedAccountId) === departmentData.id;
    });
    if (isCheck.length === 0) inputCheckBox.checked = false;
    else {
      checkedAccountIds.push(isCheck[0]);
      inputCheckBox.checked = true;
    }
    inputCheckBox.id = departmentData.id;
    const label = document.createElement('label');
    label.className = 'form-check-label';
    label.innerText = departmentData.name;
    orderedList.appendChild(inputCheckBox);
    orderedList.appendChild(label);
    list?.appendChild(orderedList);
  }
};

const loaderElement = (): string => {
  return `
  <div id="modal-loader" class="m-auto justify-content-center">
    <div class="icon rotating">
    ${videoModalIcons.loader}
    </div>
    <div class="title ml-3">${translate('global.loading')}</div>
  </div>
`;
};

export const saveSuccessAlert = (type: string) => {
  customElementAlert(type, 'shared');
  const alertPopup = document.getElementById(CONSTANTS.LOREE_SUCCESS_ALERT_CONTAINER);
  alertPopup?.classList.remove('d-none');
  setTimeout(() => {
    alertPopup?.remove();
  }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
};

const hideSharedLoader = (): void => {
  const newPageOptionsWrapper = document.getElementById(CONSTANTS.LOREE_SHARED_OPTIONS_LOADER);
  if (newPageOptionsWrapper) {
    newPageOptionsWrapper.remove();
  }
};

export const LMSLoader = () => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const loaderWrapper = document.createElement('div');
    loaderWrapper.className = 'editor-alert-modal';
    loaderWrapper.style.display = 'none';
    loaderWrapper.id = CONSTANTS.LOREE_SHARED_OPTIONS_LOADER;
    const loaderContent = document.createElement('div');
    loaderContent.className = 'col-md-4 modal-content px-0';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'modal-header d-flex justify-content-between mx-3 px-0';
    alertHeader.innerHTML = `<h4 class="text-primary font-weight-bold">${translate('modal.sharingtoselecteddepartment')}</h4>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideSharedLoader();
    alertHeader.appendChild(alertCloseButton);
    // Body
    const alertBody = document.createElement('div');
    alertBody.className = 'modal-body mx-auto px-4';
    alertBody.innerHTML = `<div class="text-center"><svg xmlns="http://www.w3.org/2000/svg" width="45.482" height="45.479" viewBox="0 0 45.482 45.479" class="rotating">
    <path id="loader" d="M0,22.739A22.79,22.79,0,0,1,8.261,5.215H4.951a.569.569,0,0,1,0-1.138H9.905a.46.46,0,0,1,.086.018.2.2,0,0,1,.033.006.542.542,0,0,1,.161.06c.018.011.034.023.052.035a.579.579,0,0,1,.1.1c.01.013.024.019.034.032a.6.6,0,0,1,.024.055.574.574,0,0,1,.039.086.531.531,0,0,1,.031.154.252.252,0,0,1,.005.025V9.6a.569.569,0,0,1-1.138,0V5.8A21.606,21.606,0,0,0,30.553,42.886a.568.568,0,1,1,.41,1.06A22.754,22.754,0,0,1,0,22.739ZM35.936,41.147a.55.55,0,0,1-.261-.072h0a.555.555,0,0,1-.174-.146c-.009-.01-.022-.014-.031-.025a.462.462,0,0,1-.03-.064.467.467,0,0,1-.033-.071.559.559,0,0,1-.038-.186v-.016a.662.662,0,0,1,0-.094V35.625a.569.569,0,1,1,1.138,0v3.761a21.607,21.607,0,0,0-21.1-36.973.569.569,0,0,1-.385-1.07,22.738,22.738,0,0,1,22.5,38.667H40.89a.569.569,0,0,1,0,1.138Z" fill="#000d9c"/>
  </svg><svg class="shared-loader-folder-icon" xmlns="http://www.w3.org/2000/svg" width="21.861" height="22.084" viewBox="0 0 21.861 22.084">
  <path id="folder" d="M48.779,29.534H45.5a2.828,2.828,0,0,0-2.767,2.26h-8.6a2.17,2.17,0,0,0-2.167,2.167v1.218a2.281,2.281,0,0,0-2.193,2.676l2.137,12.282a1.784,1.784,0,0,0,1.761,1.481H49.815a1.766,1.766,0,0,0,1.766-1.587.541.541,0,0,0,.021-.105V32.358A2.827,2.827,0,0,0,48.779,29.534Zm-14.642,3.4h9.108a.572.572,0,0,0,.569-.573A1.689,1.689,0,0,1,45.5,30.671h3.279a1.689,1.689,0,0,1,1.687,1.687v11.4l-1.287-6.686a2.287,2.287,0,0,0-2.257-1.9H33.106V33.962A1.031,1.031,0,0,1,34.136,32.931ZM50.312,50.249a.639.639,0,0,1-.5.232H33.674a.649.649,0,0,1-.64-.539L30.9,37.66a1.153,1.153,0,0,1,1.136-1.351H46.922a1.154,1.154,0,0,1,1.138.965l2.4,12.445A.65.65,0,0,1,50.312,50.249Z" transform="translate(-29.742 -29.534)"/>
</svg></div>
    <p class='mt-2 mb-0'>${translate('modal.sharingtoselecteddepartment')}.</p>
    `;
    // Footer
    const alertFooter = document.createElement('div');
    alertFooter.className = 'modal-footer justify-content-center border-0 pt-0';
    const modalNo = document.createElement('button');
    modalNo.className = 'btn btn-primary mx-1';
    modalNo.innerHTML = translate('global.cancel');
    modalNo.onclick = (): void => hideSharedLoader();
    loaderContent.appendChild(alertHeader); // header append
    loaderContent.appendChild(alertBody);
    loaderContent.appendChild(alertFooter);
    alertFooter.appendChild(modalNo);
    loaderWrapper.appendChild(loaderContent);
    loreeWrapper.appendChild(loaderWrapper);
  }
};
