import {
  deletePopup,
  deleteCustomTemplateBlock,
  editCustomBlock,
  archiveCustomTemplateBlock,
} from './customBlockHandler';
import {
  elementsBlock,
  rowsBlock,
  handleAddCustomElement,
  handleAddCustomRow,
  updateCustomBlockThumbnail,
} from './customBlockEvents';
import { verticalDotIcons } from '../../iconHolder';
import { openShareModal } from './customBlocksUI';
import {
  customBlockEditStatus,
  customBlockType,
  setCurrentEditBlock,
} from '../../../utils/saveContent';
import CONSTANTS from '../../constant';
import { getEditorElementsByClassName } from '../../utils';
import { translate } from '../../../i18n/translate';
import { isCanvas } from '../../../lmsConfig/index';
import { ensure } from '../../../utils/generalUtils';

type CustomBlockCategory = {
  createdAt: string;
  id: string;
  ltiPlatformID: string;
  name: string;
  updatedAt: string;
};

export type CustomBlockData = {
  active: boolean;
  category: CustomBlockCategory;
  categoryID: string;
  content: string;
  createdAt: string;
  createdBy: string;
  id: string;
  isGlobal: boolean;
  isShared: boolean;
  ltiPlatformID: string;
  owner: string;
  status: boolean;
  thumbnail: string;
  title: string;
  type: string;
  updatedAt: string;
};

export type DataContent = {
  content: string;
};

export const rowListAppend = (): void => {
  const customBlockWrapper = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_ROW_INPUT_WRAPPER,
  ) as HTMLElement;
  const thumbnailWrapper = document.getElementById(
    CONSTANTS.LOREE_ROWS_LINK_ACCORDION_WRAPPER,
  ) as HTMLElement;
  thumbnailWrapper?.remove();
  if (customBlockWrapper) customBlockWrapper.appendChild(rowsBlock());
};

export const elementListAppend = (): void => {
  const customBlockWrapper = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_ELEMENT_INPUT_WRAPPER,
  ) as HTMLElement;
  const thumbnailWrapper = document.getElementById(
    CONSTANTS.LOREE_ELEMENTS_LINK_ACCORDION_WRAPPER,
  ) as HTMLElement;
  thumbnailWrapper?.remove();
  if (customBlockWrapper) customBlockWrapper.appendChild(elementsBlock());
};

export const checkEvents = (type: string) => {
  let checkBox: HTMLCollectionOf<HTMLInputElement>;
  type === 'row'
    ? (checkBox = getEditorElementsByClassName(
        'loree_custom_row_checkbox',
      ) as HTMLCollectionOf<HTMLInputElement>)
    : (checkBox = getEditorElementsByClassName(
        'loree_custom_elements_checkbox',
      ) as HTMLCollectionOf<HTMLInputElement>);
  for (const check of checkBox) {
    if (check.checked) {
      return false;
    }
  }
  return true;
};

export const appendBlocksUI = (
  blockData: CustomBlockData[],
  blockWrapperId: string,
  blockType: string,
) => {
  const customBlockType = blockType.includes('Element') ? 'elements' : 'row';
  const accordionBody = document.getElementById(blockWrapperId) as HTMLElement;
  if (!accordionBody) {
    return;
  }
  accordionBody.innerHTML = '';
  if (blockData.length === 0) {
    accordionBody.classList.add('loree-custom-block-no-results');
    accordionBody.innerHTML = translate('global.noresultsfound');
  } else {
    for (const data of blockData) {
      const element = document.createElement('div');
      element.className = 'panel mb-3';
      element.id = data.id;
      const thumbNail = document.createElement('div');
      thumbNail.className = 'custom-blocks-thumbnail-row';
      if (blockType === 'My Row' || blockType === 'My Element') {
        thumbNail.prepend(appendThumbnailMenu(blockType));
      }
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = data.thumbnail;
      thumbnailImg.className = 'loree-sidebar-custom-block-thumbnail';
      thumbNail.append(thumbnailImg);
      const title = document.createElement('p');
      title.className = 'custom-block-title mb-0';
      title.innerHTML = data.title;
      title.title = data.title;
      if (blockType.includes('Element')) {
        thumbNail.onclick = async (e) => await handleAddCustomElement(e, blockType);
      }
      if (blockType.includes('Row')) {
        thumbNail.onclick = async (e) => await handleAddCustomRow(e, blockType);
      }
      element.append(thumbNail);
      element.appendChild(title);
      accordionBody.append(element);
    }
    updateCustomBlockThumbnail(customBlockType);
  }
};

// thumbnail menu
export const appendThumbnailMenu = (blockType: string) => {
  const menuWrapper = document.createElement('div');
  menuWrapper.className = 'dropdown dropdown-layout';
  menuWrapper.id = 'loree-sidebar-custom-block-menu-wrapper';
  const menuBtn = document.createElement('button');
  menuBtn.id = CONSTANTS.LOREE_SIDEBAR_CUSTOM_BLOCK_MENU_BUTTON;
  menuBtn.className = 'btn custom-block-menu-btn dropdown-toggle';
  menuBtn.innerHTML = verticalDotIcons;
  menuBtn.type = 'button';
  menuBtn.setAttribute('data-toggle', 'dropdown');
  const showMenu = document.createElement('div');
  showMenu.className = 'dropdown-menu custom-block-thumnail-dropdown-menu';
  showMenu.id = 'loree-sidebar-custom-block-thumbnail-dropdown-menu';
  showMenu.setAttribute('aria-labeledby', 'loree-sidebar-custom-row-category-list');
  const list = [translate('global.edit'), translate('global.delete')];
  if (isCanvas()) list.push(translate('global.share'));
  list.forEach((val) => {
    const li = document.createElement('div');
    li.className = 'custom-block-thumbnail-menu-option';
    li.onclick = async () => await handleThumbnailMenu(menuWrapper, val, blockType);
    li.innerHTML = val;
    showMenu.appendChild(li);
  });
  menuWrapper.appendChild(menuBtn);
  menuWrapper.appendChild(showMenu);
  return menuWrapper;
};

export const handleThumbnailMenu = async (
  menuWrapper: HTMLElement,
  val: string,
  blockType: string,
) => {
  if (val === translate('global.delete')) {
    appendMenuModal(translate('modal.delete'), menuWrapper, blockType);
    attachEventHandlers(translate('modal.delete'));
  } else if (val === translate('global.edit')) {
    const currentCustomBlockId = menuWrapper.closest('.panel')?.getAttribute('id') as string;
    setCurrentEditBlock(currentCustomBlockId);
    appendMenuModal(translate('modal.edit'), menuWrapper, blockType);
    attachEventHandlers(translate('modal.edit'));
  } else {
    let selectedType;
    if (blockType === translate('global.myrow')) {
      // need to refactor
      selectedType = translate('global.row');
    } else if (blockType === translate('global.myelement')) {
      selectedType = translate('global.elements');
    } else {
      selectedType = translate('global.template');
    }
    await openShareModal(menuWrapper, selectedType);
  }
};

// attachEvents
const attachEventHandlers = (modalType: string) => {
  const closeButton = document.getElementById(`loree-custom-block-${modalType}-close-icon`);
  if (closeButton) closeButton.onclick = () => deletePopup(modalType);
};

const appendMenuModal = (modalType: string, menuWrapper: HTMLElement, blockType: string) => {
  const editorParentWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  const customBlockBackdrop = document.createElement('div');
  customBlockBackdrop.id = `loree-custom-block-${modalType}-backdrop`;
  customBlockBackdrop.className = 'loree-custom-block-backdrop';
  editorParentWrapper.appendChild(customBlockBackdrop);
  const customBlockPopup = document.createElement('div');
  customBlockPopup.id = `loree-custom-block-${modalType}-modal-wrapper`;
  customBlockPopup.className = 'modal';
  const customModalContent = document.createElement('div');
  customModalContent.id = `loree-custom-block-${modalType}-modal-content`;
  customModalContent.className = 'modal-dialog modal-dialog-centered';
  customModalContent.appendChild(menuModalContainer(modalType, menuWrapper, blockType));
  customBlockPopup.appendChild(customModalContent);
  editorParentWrapper.appendChild(customBlockPopup);
};

const menuModalContainer = (modalType: string, menuWrapper: HTMLElement, blockType: string) => {
  const type =
    blockType === translate('global.mytemplate')
      ? translate('modal.template')
      : blockType === translate('global.myrow')
      ? translate('modal.row')
      : translate('modal.element');
  const customModalContainer = document.createElement('div');
  customModalContainer.id = `loree-custom-block-${modalType}-modal-container`;
  customModalContainer.className = 'modal-content';
  let innerHTML = `<div class="modal-header p-0 m-0" id=loree-custom-block-${modalType}-modal-header>
  <h5 class="modal-title text-primary text-capitalize" id=loree-custom-block-${modalType}-modal-title>${modalType} ${type}</h5>
  <button type="button" id=loree-custom-block-${modalType}-close-icon class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div><span class ="loree-custom-block-section-divider" ></span>`;
  if (modalType === translate('modal.delete')) {
    innerHTML += `<div class="modal-body p-0" id="delete-custom-block-content"><p> ${translate(
      'modal.areyousureyouwantto',
    )} <span class="font-weight-bold">${translate('modal.permanently')}</span> ${translate(
      'modal.deletethis',
    )} <span class="text-lowercase">${type}</span>?</p><div class="custom-block-delete-note"><p><span class="font-weight-bold">${translate(
      'modal.important:',
    )}</span> ${translate('modal.thisactioncannotbeundone')}.</p></div></div>`;
  } else {
    const blockType: string = customBlockEditStatus
      ? ensure(customBlockType)
      : translate('modal.page');
    innerHTML += `<div class="modal-body p-0 "><p> ${translate(
      'modal.doyouwanttosavethecurrentworking',
      {
        detail1: blockType.toLocaleLowerCase(),
        detail2: type.toLocaleLowerCase(),
      },
    )}</p></div>
    `;
  }
  const modalButtonWrapper = document.createElement('div');
  modalButtonWrapper.className = 'd-flex flex-row flex-nowrap align-items-center';
  modalButtonWrapper.id = `loree-custom-block-${modalType}-modal-footer`;
  const modalButtonChildWrapper = document.createElement('div');
  modalButtonChildWrapper.className = 'm-auto';
  const cancelBtn = document.createElement('button');
  cancelBtn.id = `loree-custom-block-${modalType}-modal-footer-cancel-button`;
  cancelBtn.className = 'btn modal-footer-button mx-1';
  cancelBtn.innerHTML = translate('global.cancel');
  cancelBtn.onclick = () => deletePopup(modalType);
  const menuBtn = document.createElement('button');
  const deleteBtn = document.createElement('button');
  if (modalType === translate('modal.delete')) {
    menuBtn.className = 'archive-button mx-1';
    menuBtn.onclick = async () => await archiveCustomTemplateBlock(modalType, menuWrapper, type);
    menuBtn.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_ARCHIVE_BUTTON;
    menuBtn.innerHTML = translate('global.archive');
    deleteBtn.className = 'ok-button mx-1';
    deleteBtn.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_DELETE_BUTTON;
    deleteBtn.onclick = async () => await deleteCustomTemplateBlock(menuWrapper, modalType, type);
    deleteBtn.innerHTML = translate('global.delete');
  } else {
    menuBtn.className = 'ok-button mx-1';
    menuBtn.onclick = async (e) => await editCustomBlock(e, modalType, menuWrapper, blockType);
    menuBtn.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_EDIT_BUTTON;
    menuBtn.innerHTML = translate('modal.save&edit');
  }
  modalButtonChildWrapper.appendChild(cancelBtn);
  modalButtonChildWrapper.appendChild(menuBtn);
  if (modalType === 'delete') modalButtonChildWrapper.appendChild(deleteBtn);
  modalButtonWrapper.appendChild(modalButtonChildWrapper);
  customModalContainer.innerHTML = innerHTML;
  customModalContainer.appendChild(modalButtonWrapper);
  return customModalContainer;
};
