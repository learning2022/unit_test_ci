import { Auth } from 'aws-amplify';

export const getMyTemplateQueryContext = async () => ({
  ltiPlatformID: sessionStorage.getItem('ltiPlatformId'),
  owner: { eq: (await Auth.currentAuthenticatedUser()).attributes.sub },
});

export const getGlobalTemplateQueryContext = () => ({
  filter: { ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') } },
});

export const getSharedTemplateQueryContext = (shareAccountId: string) => ({
  filter: {
    ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') },
    sharedAccountId: { eq: shareAccountId },
  },
});

export const getSharedTemplateBySelectedTemplate = (
  accountDetail: string,
  selectedBlockDataId: string,
) => ({
  filter: {
    ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') },
    sharedAccountId: { eq: accountDetail },
    customTemplateID: { eq: selectedBlockDataId },
  },
});

export const getTemplateBySelectedTemplate = (selectedBlockDataId: string) => ({
  filter: {
    ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') },
    customTemplateID: { eq: selectedBlockDataId },
  },
});

export const getSharedBlockBySelectedTemplate = (
  accountDetail: string,
  selectedBlockDataId: string,
) => ({
  filter: {
    ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') },
    sharedAccountId: { eq: accountDetail },
    customBlockID: { eq: selectedBlockDataId },
  },
});
export const getBlockBySelectedTemplate = (selectedBlockDataId: string) => ({
  filter: {
    ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') },
    customBlockID: { eq: selectedBlockDataId },
  },
});
export const getCourseDetailFilter = () => ({
  courseId: sessionStorage.getItem('course_id'),
});

export const getAccountByCourseFilter = (accountId: string) => ({
  accountId: accountId,
});
