import { API, graphqlOperation, Auth } from 'aws-amplify';
import html2canvas from 'html2canvas';
import CONSTANTS from '../../constant';
import { customElementAlert } from '../../alert';
import { elementAddRemoveIcons } from '../../iconHolder';
import { translate } from '../../../i18n/translate';
import {
  listCustomBlocks,
  listGlobalBlockss,
  listSharedBlockss,
  categoryByPlatform,
} from '../../../graphql/queries';
import {
  createCategory,
  createCustomBlock,
  updateCustomBlock,
  uploadImagesToS3,
} from '../../../graphql/mutations';
import Base from '../../base';
import { uploadToS3 } from '../../../lti/admin/globalImagesUpload/uploadImageToS3';
import { handleTemplateCategoryApplyButton, addClass, removeClass } from './customTemplateEvents';
import {
  editContentDetails,
  customBlockEditStatus,
  customBlockId,
  customBlockTitle,
  customBlockType,
  customBlockCategoryName,
  customBlockSelectedCategoryId,
  changeCustomBlockTitle,
  changeCustomBlockCategoryId,
  changeCustomBlockCategoryName,
  removePrompt,
  handleAutoSaveOnCustomBlockAppend,
} from '../../../utils/saveContent';
import config from '../../../aws-exports';
import { getImageUrl, handleUploadThumbanail } from '../../../utils/imageUpload';
import {
  appendCategoryDropdownList,
  categoryListLoader,
  rowThumbnail,
  elementThumbnail,
} from './customBlocksUI';
import { videoModalIcons } from '../../modules/sidebar/iconHolder';
import { isCanvas, isCanvasAndBB, lmsUrlRedirection } from '../../../lmsConfig/index';
import {
  appendCustomElementToEditor,
  b64toBlob,
  sortComparision,
  modalContainer,
} from './customBlockHandler';
import {
  rowListAppend,
  elementListAppend,
  checkEvents,
  appendBlocksUI,
  CustomBlockData,
} from './customBlockAppenders';
import {
  listingCustomBlocks,
  listingGlobalBlocks,
  listingSharedBlocks,
} from './customBlocksAction';
import { CreateCategoryMutation, UploadImagesToS3Mutation } from '../../../API';
import {
  getEditorElementById,
  getEditorElementsByClassName,
  removeClassToElement,
  addClassToElement,
} from '../../utils';
import { apm } from '@elastic/apm-rum';
import { CustomBlockInput } from './customBlockEvents.interface';
import { elementMigration, rowMigration } from './customBlockMigration';
import { isSelectionFirstElementInContent } from '../../base/baseUtil';
export const { aws_user_files_s3_bucket_region: region, aws_user_files_s3_bucket: bucket } = config;
export let type = '';
let categoryLists: _Any[];
let categoryId: string = '';
let newCategoryId: string = '';
let selectedBlock: HTMLElement | null;
let filterCategoryId: string[] = [];
let listBlocks: _Any;
export let rowData: CustomBlockData[] = [];
let customBlockFilterData: string[] = [];
let globalRowData: CustomBlockData[] = [];
let sharedRowData: CustomBlockData[] = [];
let sharedElementData: CustomBlockData[] = [];
export let elementData: CustomBlockData[] = [];
let globalElementData: CustomBlockData[] = [];
const checkedCheckBoxes: Array<ChildNode> = [];
const getCategoryLists: string[] = [];
const listAllRowBlocks: string[] = [];
const listAllElementBlocks: string[] = [];
let rowAccordionType: string = !isCanvasAndBB() ? 'My Row' : 'Global Row';
let elementAccordionType: string = !isCanvasAndBB() ? 'My Element' : 'Global Element';
const baseClass = () => {
  const base = new Base();
  return base;
};

export let isMyRowsCalled = false;
export let isSharedRowsCalled = false;
export let isMyElementsCalled = false;
export let isSharedElementsCalled = false;

export const setSharedBlockStatus = (blockType: string) => {
  switch (blockType) {
    case 'row':
      isSharedRowsCalled = false;
      break;
    case 'element':
      isSharedElementsCalled = false;
      break;
  }
};

// category list fetching
export const fetchCategory = async () => {
  let category: _Any = [];
  const ltiPlatformId = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformId) {
    category = await API.graphql(
      graphqlOperation(categoryByPlatform, { ltiPlatformID: ltiPlatformId }),
    );
  } else {
    category = await API.graphql(
      graphqlOperation(categoryByPlatform, { ltiPlatformID: 'STANDALONE' }),
    );
  }
  categoryLists = category.data.categoryByPlatform.items;
  categoryLists.forEach((item: { name: string }) => {
    getCategoryLists.push(item.name);
  });
  categoryLists.sort((a: { createdAt: string }, b: { createdAt: string }) => {
    return b.createdAt.localeCompare(a.createdAt);
  });
};

export const setBlockTypeAndSelectedBlock = (blockType: string) => {
  type = blockType;
  if (blockType === 'Row' && selectedBlock) {
    switch (true) {
      case selectedBlock.classList.contains('loree-iframe-content-row'):
        break;
      case selectedBlock.classList.contains('loree-iframe-content-column'):
        selectedBlock = selectedBlock.parentElement;
        break;
      default:
        selectedBlock = selectedBlock.parentElement?.parentElement as HTMLElement;
        break;
    }
  }
  if (
    blockType === 'Elements' &&
    selectedBlock?.classList.contains('loree-iframe-content-element')
  ) {
    selectedBlock = selectedBlock.parentElement;
  }
};

export const customBlockInfoModal = async (
  typeOfElement: string,
  selectedElement: HTMLElement | null,
) => {
  type = typeOfElement;
  selectedBlock = selectedElement;
  const editorParentWrapper = getEditorElementById(CONSTANTS.LOREE_WRAPPER) as HTMLElement;
  if ((getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_BACKDROP) as HTMLElement) === null) {
    const customBlockBackdrop = document.createElement('div');
    customBlockBackdrop.id = CONSTANTS.LOREE_CUSTOM_BLOCK_BACKDROP;
    customBlockBackdrop.className = 'loree-custom-block-backdrop d-none';
    editorParentWrapper.appendChild(customBlockBackdrop);
    const customBlockPopup = document.createElement('div');
    customBlockPopup.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_WRAPPER;
    customBlockPopup.className = 'd-none modal';
    const customModalContent = document.createElement('div');
    customModalContent.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_CONTENT;
    customModalContent.className = 'modal-dialog modal-dialog-centered';
    customModalContent.appendChild(modalContainer());
    customBlockPopup.appendChild(customModalContent);
    editorParentWrapper.appendChild(customBlockPopup);
    if (editorParentWrapper) {
      eventHandlers();
    }
  }
};

const eventHandlers = () => {
  const closeButton = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CLOSE_ICON);
  if (closeButton) closeButton.onclick = () => hideBlocks();
  const cancelButton = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_CANCEL_BUTTON,
  );
  if (cancelButton) cancelButton.onclick = () => hideBlocks();
  const dropdownContainer = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN,
  ) as HTMLElement;
  dropdownContainer.onclick = async () => await categoryOnClickHandler();
  const saveButton = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON);
  if (saveButton) {
    saveButton.onclick = async () => await validateSaveCustomBlocks();
  }
  const customBlockNameInput = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME);
  const newCategoryNameInput = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME);
  if (customBlockNameInput) {
    customBlockNameInput.oninput = () => enableSaveButton();
  }
  if (newCategoryNameInput) {
    newCategoryNameInput.oninput = () => enableSaveButton();
  }
};

const categoryOnClickHandler = async () => {
  const dropdownItemsList = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN_ITEM,
  ) as HTMLElement;
  dropdownItemsList.innerHTML = categoryListLoader();
  await fetchCategory();
  dropdownItemsList.innerHTML = appendCategoryDropdownList(categoryLists);
  const dropdownList = getEditorElementsByClassName('loree-custom-block-category');
  if (dropdownList)
    Array.from(dropdownList).forEach((option) => {
      (option as HTMLElement).onclick = (e) => changeSelectedCategory(e, option as HTMLElement);
    });
};

export const enableSaveButton = () => {
  const customBlockNameInput = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME,
  ) as HTMLInputElement;
  const saveButton = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON);
  const newCategoryBlock = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_GROUP,
  ) as HTMLDivElement;
  const newCategoryNameInput = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME,
  ) as HTMLInputElement;
  const categoryDropdown = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN);

  if (
    customBlockNameInput.value.trim() !== '' &&
    categoryDropdown?.innerHTML !== translate('global.selectcategory') &&
    (newCategoryBlock?.classList?.contains('d-none') ||
      (!newCategoryBlock?.classList?.contains('d-none') &&
        newCategoryNameInput?.value.trim() !== ''))
  ) {
    saveButton?.removeAttribute('disabled');
  } else saveButton?.setAttribute('disabled', 'true');
};

const changeSelectedCategory = (e: MouseEvent, option: HTMLElement) => {
  const newCategoryBlock = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_GROUP,
  ) as HTMLDivElement;
  const newCategoryNameInput = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME,
  ) as HTMLInputElement;
  if (option.id === CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_OPTION) {
    removeClassToElement(newCategoryBlock, 'd-none');
  } else {
    addClassToElement(newCategoryBlock, 'd-none');
    newCategoryNameInput.value = '';
  }
  categoryId = (e.target as HTMLElement).id;
  const categoryDropdown = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN);
  if (categoryDropdown) categoryDropdown.innerHTML = option.innerHTML;
  changeCustomBlockCategoryName(option.innerHTML);
  enableSaveButton();
};

const validateSaveCustomBlocks = async () => {
  const templateName = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME,
  ) as HTMLInputElement;
  const categoryDropdown = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN);
  const newCategoryBlock = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_GROUP,
  ) as HTMLDivElement;
  const newCategoryNameInput = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME,
  ) as HTMLInputElement;
  if (
    templateName.value !== '' &&
    categoryDropdown?.innerHTML !== translate('global.selectcategory') &&
    newCategoryBlock?.classList?.contains('d-none')
  ) {
    await saveBlocks(templateName.value, categoryId);
    return true;
  } else if (
    templateName.value !== '' &&
    categoryDropdown?.innerHTML !== translate('global.selectcategory') &&
    !newCategoryBlock?.classList?.contains('d-none') &&
    newCategoryNameInput?.value !== ''
  ) {
    await saveCategory(newCategoryNameInput.value, templateName.value);
    return true;
  } else {
    return false;
  }
};

const saveCategory = async (newCategoryNameInput: string, templateName: string): Promise<void> => {
  const alertCatExist: HTMLElement | null = getEditorElementById('loree-row-cat-name');
  const alertNameExist: HTMLElement | null = getEditorElementById('loree-row-name');
  let blocksArray: string[] = [];
  if (type === 'Row') {
    if (customBlockEditStatus) {
      listAllRowBlocks.forEach((block: string) => {
        if (customBlockTitle === block) listAllRowBlocks.splice(listAllRowBlocks.indexOf(block), 1);
      });
      blocksArray = listAllRowBlocks;
    } else blocksArray = listAllRowBlocks;
  } else {
    if (customBlockEditStatus) {
      listAllElementBlocks.forEach((block: string) => {
        if (customBlockTitle === block)
          listAllElementBlocks.splice(listAllElementBlocks.indexOf(block), 1);
      });
      blocksArray = listAllElementBlocks;
    } else blocksArray = listAllElementBlocks;
  }
  if (blocksArray.includes(templateName)) {
    removeClassToElement(alertNameExist, 'd-none');
    if (getCategoryLists.includes(newCategoryNameInput)) {
      removeClassToElement(alertCatExist, 'd-none');
      return;
    } else {
      addClassToElement(alertCatExist, 'd-none');
    }
    return;
  } else {
    addClassToElement(alertNameExist, 'd-none');
  }
  if (getCategoryLists.includes(newCategoryNameInput)) {
    removeClassToElement(alertCatExist, 'd-none');
    return;
  } else {
    addClassToElement(alertCatExist, 'd-none');
  }
  const categoryInput: { name: string; ltiPlatformID: string } = {
    name: newCategoryNameInput,
    ltiPlatformID: sessionStorage.getItem('ltiPlatformId')
      ? (sessionStorage.getItem('ltiPlatformId') as string)
      : 'STANDALONE',
  };
  const createdCategory = await API.graphql<CreateCategoryMutation>(
    graphqlOperation(createCategory, { input: categoryInput }),
  );
  const firstChildAllElement: HTMLElement | null = getEditorElementById(
    `loree_custom_elements_label_all_category_wrapper`,
  );
  const firstChildAllRow: HTMLElement | null = getEditorElementById(
    `loree_custom_row_label_all_category_wrapper`,
  );
  if (firstChildAllElement && type === 'Elements')
    appendNewCategory(createdCategory.data?.createCategory?.id, newCategoryNameInput, type);
  if (firstChildAllRow && type === 'Row')
    appendNewCategory(createdCategory.data?.createCategory?.id, newCategoryNameInput, type);
  newCategoryId = createdCategory.data?.createCategory?.id
    ? createdCategory.data?.createCategory?.id
    : '';
  await saveBlocks(templateName, newCategoryId);
};

// custom block thumbnail
export const customBlockThumbnailImage = async (
  templateName: string,
  thumbnailType: string,
  assignBlock?: HTMLDivElement,
) => {
  let thumbnail: string = '';
  const toSave =
    assignBlock !== undefined || assignBlock ? assignBlock : (selectedBlock as HTMLElement);
  if (assignBlock === undefined || assignBlock === null) {
    toSave.setAttribute('id', 'custom-block');
  }
  await html2canvas(toSave, {
    onclone: function (documentClone) {
      if (type === 'Elements') {
        documentClone.getElementById('custom-block')?.removeAttribute('style');
      }
    },
  }).then(
    function (block: HTMLCanvasElement) {
      thumbnail = block.toDataURL('image/png');
    },
    () => toSave.removeAttribute('id'),
  );
  const blob = b64toBlob(thumbnail, 'image/png');

  return {
    bucket,
    key: await handleUploadThumbanail(blob, templateName, thumbnailType),
    region,
  };
};

export const getEditContentDetails = () => {
  const saveButton = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON);
  saveButton?.removeAttribute('disabled');
  if (customBlockType === type) {
    const templateName = getEditorElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME,
    ) as HTMLInputElement;
    templateName.value = customBlockTitle as string;
    const categoryDropdown = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN);
    if (categoryDropdown) categoryDropdown.innerHTML = customBlockCategoryName as string;
  }
};
const saveSuccessAlert = () => {
  customElementAlert(type, 'saved');
  baseClass().customElementAlertTimer();
};

// custom block save
export const saveBlocks = async (templateName: string, categoryId: string) => {
  const alertCatExist: HTMLElement | null = getEditorElementById('loree-row-cat-name');
  const alertNameExist: HTMLElement | null = getEditorElementById('loree-row-name');
  let blocksArray: string[] = [];

  if (customBlockEditStatus) {
    blocksArray =
      type === 'Row'
        ? filterCustomBlockArray(listAllRowBlocks)
        : filterCustomBlockArray(listAllElementBlocks);
  } else {
    blocksArray = type === 'Row' ? listAllRowBlocks : listAllElementBlocks;
  }

  if (blocksArray.includes(templateName)) {
    removeClassToElement(alertNameExist, 'd-none');
    return;
  }

  addClassToElement(alertNameExist, 'd-none');
  addClassToElement(alertCatExist, 'd-none');

  const saveButton = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_SAVE_BUTTON,
  ) as HTMLButtonElement;

  saveButton.innerHTML = translate('global.saving');
  saveButton?.setAttribute('disabled', 'true');

  const existingBlock = getEditorElementById('custom-block') as HTMLDivElement;
  existingBlock?.removeAttribute('id');

  await createOrUpdateCustomBlock(templateName, categoryId);
  hideBlocks();
  saveSuccessAlert();
};

const filterCustomBlockArray = (customBlockArray: string[]) => {
  customBlockArray.forEach((block: string) => {
    if (customBlockTitle === block) customBlockArray.splice(customBlockArray.indexOf(block), 1);
  });
  return customBlockArray;
};

export const getConvertedHTML = async (currentUserId: string) => {
  const SelectedElementBlock = selectedBlock?.cloneNode(true) as HTMLDivElement;
  if (type === 'Elements' && SelectedElementBlock) {
    SelectedElementBlock.className = '';
    SelectedElementBlock.setAttribute('style', '');
  }

  const blockContent =
    type === 'Elements' ? SelectedElementBlock.outerHTML : selectedBlock?.outerHTML;

  if (!isCanvas()) {
    return await uploadToS3(blockContent as string, `${currentUserId}`);
  } else {
    const response = await API.graphql<UploadImagesToS3Mutation>(
      graphqlOperation(uploadImagesToS3, {
        globalHtmlContent: blockContent,
        type: `${currentUserId}`,
      }),
    );
    return JSON.parse(response?.data?.uploadImagesToS3 as string);
  }
};

export const createOrUpdateCustomBlock = async (templateName: string, categoryId: string) => {
  const currentAuthenticatedUser = await Auth.currentAuthenticatedUser();
  const ltiPlatformID = currentAuthenticatedUser.attributes['custom:platform'];
  const currentUser = currentAuthenticatedUser.attributes.name;
  const currentUserId = currentAuthenticatedUser.attributes.sub;
  const isEditCustomBlock = isSelectionFirstElementInContent();

  const customBlockInput: CustomBlockInput = {
    title: templateName,
    content: await getConvertedHTML(currentUserId),
    thumbnail: await customBlockThumbnailImage(templateName, 'blocks'),
    status: true,
  };

  switch (true) {
    case !customBlockEditStatus || customBlockType !== type || !isEditCustomBlock:
      selectedBlock?.removeAttribute('id');
      customBlockInput.categoryID = categoryId;
      customBlockInput.type = type;
      customBlockInput.createdBy = currentUser;
      customBlockInput.owner = currentUserId;
      customBlockInput.isGlobal = false;
      customBlockInput.active = true;
      customBlockInput.ltiPlatformID = ltiPlatformID || 'STANDALONE';

      await API.graphql(graphqlOperation(createCustomBlock, { input: customBlockInput }));

      await refreshCustomBlockThumbnail(type, 'refreshMyBlocks');
      break;

    case isEditCustomBlock && customBlockEditStatus && customBlockId && customBlockType === type:
      customBlockInput.id = customBlockId;
      customBlockInput.categoryID = categoryId === '' ? customBlockSelectedCategoryId : categoryId;
      customBlockInput.type = customBlockType;
      changeHeaderTitle(templateName);
      changeCustomBlockTitle(templateName);
      if (categoryId !== '') {
        changeCustomBlockCategoryId(categoryId);
      }
      await API.graphql(graphqlOperation(updateCustomBlock, { input: customBlockInput }));
      removePrompt(true);
      window.location.replace(lmsUrlRedirection());
      break;
  }
};

export const refreshCustomBlockThumbnail = async (customBlockType: string, refreshType: string) => {
  rowAccordionType = 'Global Row';
  elementAccordionType = 'Global Element';
  try {
    switch (sessionStorage.getItem('domainName')) {
      case 'BB':
        type === 'Row'
          ? await Promise.all([retrieveGlobalCustomBlockRow(), retrieveCustomBlockRow()])
          : await Promise.all([retrieveGlobalCustomBlockElement(), retrieveCustomBlockElement()]);
        break;
      case 'D2l':
        rowAccordionType = 'My Row';
        elementAccordionType = 'My Element';
        type === 'Row'
          ? await Promise.all([retrieveCustomBlockRow()])
          : await Promise.all([retrieveCustomBlockElement()]);
        break;
      default:
        setRelodStatusForCustomBlocks(customBlockType, refreshType);
        break;
    }
  } catch (err: _Any) {
    apm.captureError(err);
    console.log('Error, while refreshing custom block list', err);
  }
};

const setRelodStatusForCustomBlocks = (customBlockType: string, refreshType: string) => {
  switch (customBlockType) {
    case 'Row':
      refreshType === 'refreshMyBlocks' ? (isMyRowsCalled = false) : (isSharedRowsCalled = false);
      break;
    case 'Elements':
      refreshType === 'refreshMyBlocks'
        ? (isMyElementsCalled = false)
        : (isSharedElementsCalled = false);
      break;
  }

  refreshCustomBlockTrayInCanvas(customBlockType);
};

const refreshCustomBlockTrayInCanvas = (customBlockType: string) => {
  const rowAccordionWrapper = getEditorElementById(
    CONSTANTS.LOREE_ROWS_LINK_ACCORDION_WRAPPER,
  ) as HTMLDivElement;
  const rowSideBarWrapper = getEditorElementById(
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_ROW_INPUT_WRAPPER,
  ) as HTMLDivElement;

  const elementAccordionWrapper = getEditorElementById(
    CONSTANTS.LOREE_ELEMENTS_LINK_ACCORDION_WRAPPER,
  ) as HTMLDivElement;
  const elementSideBarWrapper = getEditorElementById(
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_ELEMENT_INPUT_WRAPPER,
  ) as HTMLDivElement;

  switch (customBlockType) {
    case 'Row':
      if (!rowAccordionWrapper || !rowSideBarWrapper) return;
      rowAccordionWrapper.remove();
      rowSideBarWrapper.appendChild(rowsBlock());
      break;
    case 'Elements':
      if (!elementAccordionWrapper || !elementSideBarWrapper) return;
      elementAccordionWrapper.remove();
      elementSideBarWrapper.appendChild(elementsBlock());
      baseClass().refreshCustomBlockSearch('elements');
      break;
  }
};

const hideBlocks = () => {
  const customBlockBackdrop = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_BACKDROP);
  addClassToElement(customBlockBackdrop, 'd-none');
  const customBlockModalPopup = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_WRAPPER);
  addClassToElement(customBlockModalPopup, 'd-none');
  const templateName = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME,
  ) as HTMLInputElement;
  if (templateName) {
    templateName.value = '';
  }
  const categoryDropdown = getEditorElementById(CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_DROPDOWN);
  if (categoryDropdown) categoryDropdown.innerHTML = translate('global.selectcategory');
  const newCategoryBlock = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_NEW_CATEGORY_GROUP,
  ) as HTMLDivElement;
  const newCategoryNameInput = getEditorElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_CATEGORY_NAME,
  ) as HTMLInputElement;
  addClassToElement(newCategoryBlock, 'd-none');
  if (newCategoryNameInput) {
    newCategoryNameInput.value = '';
  }
  const infoText = getEditorElementById('update-info-content-text');
  addClassToElement(infoText, 'd-none');
  const alertCatExist: HTMLElement | null = getEditorElementById('loree-row-cat-name');
  const alertNameExist: HTMLElement | null = getEditorElementById('loree-row-name');
  addClassToElement(alertCatExist, 'd-none');
  addClassToElement(alertNameExist, 'd-none');
};

export const categoryWrapper = (type: string) => {
  return `<div class="dropdown dropdown-layout" id="loree-sidebar-custom-${type}-category-list">
      <button id="loree-sidebar-custom-${type}-category-dropdown" class="btn dropdown-toggle loree-sidebar-custom-block-category-dropdown" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">Select Category</button>
      <div class="dropdown-menu loree-sidebar-custom-block-category-dropdown-menu" id="loree-sidebar-custom-${type}-category-dropdown-menu" aria-labelledby="loree-sidebar-custom-row-category-list">
      <div id ='loree-sidebar-custom-${type}-category-list-wrapper'>
        <div id="loree_custom_${type}_label_all_category_wrapper"><label id="loree_custom_${type}_label_all_category"><input type="checkbox" class="mx-1"/>All</ label></div>
        ${categoryLists
          .map(
            (list: { id: string; name: string }) =>
              `<div id="label-check"><label id=${list.id} title="${list.name}"><input type="checkbox" class="mx-1 loree_custom_${type}_checkbox"/>${list.name}</ label></div>`,
          )
          .join('')}
      </div>
      <div id="loree-sidebar-custom-block-category-button" class="d-flex flex-row flex-nowrap align-items-center mt-1">
        <div style="margin: auto;">
          <button id='loree-sidebar-custom-row-category-button-cancel' class="mx-1">Cancel</button>
          <button class="apply-button mx-1" id='loree-sidebar-custom-${type}-category-button-apply'>Apply</button>
        </div>
      </div>
    </div>
  </div>`;
};

export const refreshCategoryDropdown = (type: string) => {
  const checkBox = getEditorElementsByClassName(
    `loree_custom_${type}_checkbox`,
  ) as HTMLCollectionOf<HTMLInputElement>;
  const allCategories: Array<string | undefined> = [];
  for (const check of checkBox) {
    allCategories.push(check?.parentElement?.id);
  }
  checkSelectedCheckBoxes(checkBox, type);
};

export const refreshCheckBox = (type: string) => {
  const blockType = type.toLowerCase();
  const checkBox = getEditorElementsByClassName(
    `loree_custom_${blockType}_checkbox`,
  ) as HTMLCollectionOf<HTMLInputElement>;
  const allCategoryCheckBox = getEditorElementById(`loree_custom_${blockType}_label_all_category`)
    ?.childNodes[0] as HTMLInputElement;
  for (const check of checkBox) {
    check.checked = true;
  }
  if (allCategoryCheckBox) allCategoryCheckBox.checked = true;
  handleBlockCategoryApplyButton(blockType);
};

const checkSelectedCheckBoxes = (checkBox: HTMLCollectionOf<HTMLInputElement>, type: string) => {
  const allCategoryCheckBox = getEditorElementById(`loree_custom_${type}_label_all_category`)
    ?.childNodes[0] as HTMLInputElement;
  const checkBoxLabel: Array<HTMLElement> = [];
  if (allCategoryCheckBox.parentElement) checkBoxLabel.push(allCategoryCheckBox.parentElement);
  for (const check of checkBox) {
    if (check.parentElement) checkBoxLabel.push(check.parentElement);
    check.checked = false;
  }
  allCategoryCheckBox.checked = false;
  if (checkedCheckBoxes.length !== 0) {
    for (const label of checkBoxLabel) {
      if (checkedCheckBoxes.includes(label))
        (label?.childNodes[0] as HTMLInputElement).checked = true;
    }
  }
};

export const appendNewCategory = (
  categoryId: string | undefined,
  categoryName: string,
  type: string,
) => {
  const newElementCategory = document.createElement('div');
  newElementCategory.innerHTML = `<label id = ${categoryId} title="${categoryName}"><input type="checkbox" class="mx-1 loree_custom_elements_checkbox checked"/>${categoryName}</ label>`;
  const firstChildAllElement: HTMLElement | null = getEditorElementById(
    `loree_custom_elements_label_all_category_wrapper`,
  );
  firstChildAllElement?.parentNode?.insertBefore(
    newElementCategory,
    firstChildAllElement.nextSibling,
  );
  const newRowCategory = document.createElement('div');
  newRowCategory.innerHTML = `<label id = ${categoryId} title="${categoryName}"><input type="checkbox" class="mx-1 loree_custom_row_checkbox" checked/>${categoryName}</ label>`;
  const firstChildAllRow: HTMLElement | null = getEditorElementById(
    `loree_custom_row_label_all_category_wrapper`,
  );
  firstChildAllRow?.parentNode?.insertBefore(newRowCategory, firstChildAllRow.nextSibling);
  const newTemplateCategory = document.createElement('div');
  newTemplateCategory.innerHTML = `<label id = ${categoryId} title="${categoryName}"><input type="checkbox" class="mx-1 loree-custom-template-checkbox" checked/>${categoryName}</ label>`;
  const firstChildAllTemplate: HTMLElement | null = getEditorElementById(
    'loree_custom_global_template_all_category',
  );
  firstChildAllTemplate?.parentNode?.insertBefore(
    newTemplateCategory,
    firstChildAllTemplate.nextSibling,
  );
  type === 'template' ? handleTemplateCategoryApplyButton() : refreshCheckBox(type);
};

export const retrieveCustomBlockRow = async () => {
  rowData = [];
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listBlocks = await listingCustomBlocks();
  } else {
    listBlocks = await API.graphql(
      graphqlOperation(listCustomBlocks, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listBlocks = listBlocks?.data.listCustomBlocks.items;
  }
  for (const listBlock of listBlocks) {
    listBlock.thumbnail = await getImageUrl(listBlock.thumbnail.key);
    if (listBlock.type === 'Row' && listBlock.active) {
      rowData.push(listBlock);
    }
  }
  rowData.sort(sortComparision);
  if (isMyRowsCalled) rowListAppend();
  isMyRowsCalled = true;
};

export const retrieveGlobalCustomBlockRow = async () => {
  globalRowData = [];
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listBlocks = await listingGlobalBlocks();
  } else {
    listBlocks = await API.graphql(
      graphqlOperation(listCustomBlocks, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listBlocks = listBlocks.data.listCustomBlocks.items;
  }
  for (const listBlock of listBlocks) {
    listBlock.thumbnail = await getImageUrl(listBlock.thumbnail.key);
    if (listBlock.type === 'Row' && listBlock.active) {
      globalRowData.push(listBlock);
    }
  }
  globalRowData.sort(sortComparision);
  rowListAppend();
};
export const retrieveSharedCustomBlockRow = async () => {
  sharedRowData = [];
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listBlocks = await listingSharedBlocks();
  } else {
    listBlocks = await API.graphql(
      graphqlOperation(listSharedBlockss, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listBlocks = listBlocks.data.listSharedBlockss.items;
  }
  for (const listBlock of listBlocks) {
    listBlock.thumbnail = await getImageUrl(listBlock.thumbnail.key);
    if (listBlock.type === 'Row' && listBlock.active) {
      sharedRowData.push(listBlock);
    }
  }
  sharedRowData.sort(sortComparision);
  if (isSharedRowsCalled) rowListAppend();
  isSharedRowsCalled = true;
};
export const retrieveGlobalCustomBlockElement = async () => {
  globalElementData = [];
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listBlocks = await listingGlobalBlocks();
  } else {
    listBlocks = await API.graphql(
      graphqlOperation(listGlobalBlockss, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listBlocks = listBlocks.data.listGlobalBlockss.items;
  }
  for (const listBlock of listBlocks) {
    listBlock.thumbnail = await getImageUrl(listBlock.thumbnail.key);
    if (listBlock.type === 'Elements' && listBlock.active) {
      globalElementData.push(listBlock);
    }
  }
  globalElementData.sort(sortComparision);
  elementListAppend();
};
export const retrieveSharedCustomBlockElement = async () => {
  sharedElementData = [];
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listBlocks = await listingSharedBlocks();
  } else {
    listBlocks = await API.graphql(
      graphqlOperation(listSharedBlockss, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listBlocks = listBlocks.data.listSharedBlockss.items;
  }
  for (const listBlock of listBlocks) {
    listBlock.thumbnail = await getImageUrl(listBlock.thumbnail.key);
    if (listBlock.type === 'Elements' && listBlock.active) {
      sharedElementData.push(listBlock);
    }
  }
  sharedElementData.sort(sortComparision);
  if (isSharedElementsCalled) elementListAppend();
  isSharedElementsCalled = true;
};
export const retrieveCustomBlockElement = async () => {
  elementData = [];
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listBlocks = await listingCustomBlocks();
  } else {
    listBlocks = await API.graphql(
      graphqlOperation(listCustomBlocks, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listBlocks = listBlocks.data.listCustomBlocks.items;
  }
  for (const listBlock of listBlocks) {
    listBlock.thumbnail = await getImageUrl(listBlock.thumbnail.key);
    if (listBlock.type === 'Elements' && listBlock.active) {
      elementData.push(listBlock);
    }
  }
  elementData.sort(sortComparision);
  if (isMyElementsCalled) elementListAppend();
  isMyElementsCalled = true;
};

export const handleBlockCategoryApplyButton = (type: string) => {
  filterCategoryId = [];
  let categoryDropdownCategory: HTMLElement;
  type === 'row'
    ? (categoryDropdownCategory = getEditorElementById(
        'loree-sidebar-custom-row-category-list',
      ) as HTMLElement)
    : (categoryDropdownCategory = getEditorElementById(
        'loree-sidebar-custom-elements-category-list',
      ) as HTMLElement);
  const dropdownMenu = categoryDropdownCategory?.children[1] as HTMLElement;
  const dropdownMenuItems = dropdownMenu?.children[0] as HTMLElement;
  if (dropdownMenuItems) {
    const selectedList = handleCategoryListChange(type, dropdownMenuItems);
    removeSelectedListDropdown();
    if (selectedList.length !== 0)
      appendSelectedListDropdown(selectedList, categoryDropdownCategory, type);
  }
};

export const updateCustomBlockThumbnail = (blockType: string) => {
  let tempData: _Any[] = [];
  let global: NodeListOf<ChildNode> | undefined;
  const customBlockAccordionType =
    blockType === 'elements' ? elementAccordionType : rowAccordionType;
  switch (customBlockAccordionType) {
    case 'Global Row':
      tempData = globalRowData;
      global = getEditorElementById(CONSTANTS.LOREE_GLOBAL_ROWS_COLLAPSE)?.childNodes;
      break;
    case 'My Row':
      tempData = rowData;
      global = getEditorElementById(CONSTANTS.LOREE_MY_ROWS_COLLAPSE)?.childNodes;
      break;
    case 'Shared Row':
      tempData = sharedRowData;
      global = getEditorElementById(CONSTANTS.LOREE_SHARED_ROWS_COLLAPSE)?.childNodes;
      break;
    case 'Global Element':
      tempData = globalElementData;
      global = getEditorElementById(CONSTANTS.LOREE_GLOBAL_ELEMENTS_COLLAPSE)?.childNodes;
      break;
    case 'My Element':
      tempData = elementData;
      global = getEditorElementById(CONSTANTS.LOREE_MY_ELEMENTS_COLLAPSE)?.childNodes;
      break;
    case 'Shared Element':
      tempData = sharedElementData;
      global = getEditorElementById(CONSTANTS.LOREE_SHARED_ELEMENTS_COLLAPSE)?.childNodes;
      break;
  }
  if (tempData.length > 0) {
    const noResults: HTMLElement | null = getEditorElementById(
      `loree-${customBlockAccordionType}-no-results`,
    );
    if (noResults) noResults.className = 'd-none';
    customBlockFilterData = [];
    const templateSearchData = [];
    const searchWrapperInput = getEditorElementById(
      `loree-sidebar-custom-${blockType}-search-input-value`,
    ) as HTMLInputElement;
    if (filterCategoryId.length === 0) customBlockFilterData = [];
    else if (searchWrapperInput?.value !== '') {
      for (const data of tempData) {
        if (data.title.toUpperCase().includes(searchWrapperInput?.value.toUpperCase()))
          templateSearchData.push(data);
      }
      if (filterCategoryId.length !== 0) {
        for (const data of templateSearchData) {
          for (const category of filterCategoryId) {
            if (data.categoryID === category) customBlockFilterData.push(data.id);
          }
        }
      }
    } else if (searchWrapperInput.value === '' && filterCategoryId.length !== 0) {
      for (const data of tempData) {
        for (const category of filterCategoryId) {
          if (data.categoryID === category) customBlockFilterData.push(data.id);
        }
      }
    } else customBlockFilterData = tempData;
    if (global) {
      for (let i = 0; i < global.length; i++) {
        const globalHtmlElement = global[i] as HTMLElement;
        if (globalHtmlElement) {
          globalHtmlElement.style.display = customBlockFilterData.includes(globalHtmlElement.id)
            ? ''
            : 'none';
        }
      }
    }
    if (customBlockFilterData.length === 0 && global) {
      if (getEditorElementById(`loree-${customBlockAccordionType}-no-results`) === null) {
        const noResultsTag = document.createElement('span');
        noResultsTag.id = `loree-${customBlockAccordionType}-no-results`;
        noResultsTag.className = 'd-block';
        noResultsTag.style.fontSize = '14px';
        noResultsTag.style.paddingLeft = '13px';
        noResultsTag.innerHTML = translate('global.noresultsfound');
        global[0].parentElement?.appendChild(noResultsTag);
      } else if (noResults) {
        noResults.className = 'd-block';
      }
    }
  }
};

const handleCategoryListChange = (type: string, dropdownMenuItems: HTMLElement) => {
  let checkBox: HTMLCollectionOf<HTMLInputElement>;
  type === 'row'
    ? (checkBox = getEditorElementsByClassName(
        'loree_custom_row_checkbox',
      ) as HTMLCollectionOf<HTMLInputElement>)
    : (checkBox = getEditorElementsByClassName(
        'loree_custom_elements_checkbox',
      ) as HTMLCollectionOf<HTMLInputElement>);
  if (checkEvents(type)) {
    for (const check of checkBox) {
      check.checked = false;
    }
  }
  const selectedList: string[] = [];
  Array.from(dropdownMenuItems.children).forEach((checklist) => {
    const checkListChild: NodeListOf<ChildNode> = checklist.childNodes;
    if (checklist.id !== 'loree-sidebar-custom-block-category-button') {
      for (const label of checkListChild) {
        if ((label.childNodes[0] as HTMLInputElement).checked) {
          if (!checkedCheckBoxes.includes(label)) checkedCheckBoxes.push(label);
          if ((label as HTMLElement).id === `loree_custom_${type}_label_all_category`) {
            for (const check of checkBox) {
              check.checked = true;
            }
          } else {
            if (!filterCategoryId.includes((label as HTMLElement).id))
              filterCategoryId.push((label as HTMLElement).id);
            if (
              label.childNodes[1].textContent &&
              !selectedList.includes(label.childNodes[1].textContent)
            )
              selectedList.push(label.childNodes[1].textContent);
          }
          break;
        } else {
          if (filterCategoryId.includes((label as HTMLElement).id))
            filterCategoryId.splice(filterCategoryId.indexOf((label as HTMLElement).id), 1);
          if (
            label.childNodes[1].textContent &&
            selectedList.includes(label.childNodes[1].textContent)
          )
            selectedList.splice(selectedList.indexOf(label.childNodes[1].textContent), 1);
          if (checkedCheckBoxes.includes(label))
            checkedCheckBoxes.splice(checkedCheckBoxes.indexOf(label), 1);
          const hiddenCategoryList = getEditorElementsByClassName(
            'category-selected-list-item',
          )[2] as HTMLElement;
          removeClassToElement(hiddenCategoryList, 'd-none');
        }
      }
    }
  });
  updateCustomBlockThumbnail(type);
  return selectedList;
};

const appendSelectedListDropdown = (
  selectedList: string[],
  categoryDropdownCategory: HTMLElement,
  type: string,
) => {
  const selectionArea = categoryDropdownCategory.childNodes[1] as HTMLElement;
  const attachListWrapper = document.createElement('div') as HTMLElement;
  attachListWrapper.className = 'category-selected-list';
  attachListWrapper.id = 'category-selected-list-wrapper';
  let innerHTML = '';
  let count = 0;
  selectedList.forEach((element: string) => {
    if (count < 3) {
      innerHTML += `<div class="m-2 category-selected-list-item"><span>${element}</span><span class='category-selected-list-item-remove-icon'>${elementAddRemoveIcons.closeElementIcon}</span></div>`;
      count++;
    } else {
      innerHTML += `<div class="m-2 category-selected-list-item d-none"><span>${element}</span><span class='category-selected-list-item-remove-icon'>${elementAddRemoveIcons.closeElementIcon}</span></div>`;
    }
  });
  attachListWrapper.innerHTML = innerHTML;
  selectionArea.appendChild(attachListWrapper);
  if (selectedList.length > 3) {
    const moreCategoriesLink = document.createElement('a');
    moreCategoriesLink.id = 'category-selection-more';
    moreCategoriesLink.innerHTML = selectedList.length - 3 + ` ${translate('modal.more')}`;
    moreCategoriesLink.onclick = (e) => showAllSelectedCategory(e, type);
    selectionArea.appendChild(moreCategoriesLink);
  }
  removeUniqueSelectedList(type);
};

const removeUniqueSelectedList = (type: string) => {
  const removeIcon = getEditorElementsByClassName('category-selected-list-item-remove-icon');
  let allCategoryCheckBox: HTMLInputElement;
  let categoryDropdownCategory: HTMLElement;
  if (type === 'row') {
    categoryDropdownCategory = getEditorElementById(
      'loree-sidebar-custom-row-category-list',
    ) as HTMLElement;
    allCategoryCheckBox = getEditorElementById('loree_custom_row_label_all_category')
      ?.childNodes[0] as HTMLInputElement;
  } else {
    categoryDropdownCategory = getEditorElementById(
      'loree-sidebar-custom-elements-category-list',
    ) as HTMLElement;
    allCategoryCheckBox = getEditorElementById('loree_custom_elements_label_all_category')
      ?.childNodes[0] as HTMLInputElement;
  }
  Array.from(removeIcon).forEach((element) => {
    (element as HTMLElement).onclick = (e) => {
      e.stopPropagation();
      element.parentElement?.remove();
      const dropdownMenu = categoryDropdownCategory.children[1] as HTMLElement;
      const dropdownMenuItems = dropdownMenu.children[0] as HTMLElement;
      Array.from(dropdownMenuItems.children).forEach((checklist) => {
        if (checklist.id !== 'loree-sidebar-custom-block-category-button') {
          checklist.childNodes.forEach((label) => {
            if (
              label.childNodes[1].textContent ===
              (element.parentElement?.childNodes[0] as HTMLElement).innerHTML
            ) {
              (label.childNodes[0] as HTMLInputElement).checked = false;
              allCategoryCheckBox.checked = false;
            }
          });
        }
      });
      const selectedList = handleCategoryListChange(type, dropdownMenuItems);
      const moreLink = getEditorElementById('category-selection-more') as HTMLElement;
      if (moreLink) {
        if (selectedList.length > 3 && moreLink.innerHTML !== translate('modal.showless'))
          moreLink.innerHTML = selectedList.length - 3 + ` ${translate('modal.more')}`;
        else if (selectedList.length <= 3) moreLink.remove();
      }
      const selectionArea = getEditorElementById('category-selected-list-wrapper');
      if (selectionArea?.innerHTML === '') selectionArea.remove();
    };
  });
};

const removeSelectedListDropdown = () => {
  const selectionArea = getEditorElementById('category-selected-list-wrapper');
  const moreCategoryLink = getEditorElementById('category-selection-more');
  if (selectionArea !== null) {
    selectionArea.remove();
    moreCategoryLink?.remove();
  }
};

export const preventDropdownClosing = (selectedType: string, e: MouseEvent) => {
  if ((e.target as HTMLElement).tagName !== 'BUTTON') {
    e.stopPropagation();
    let allCategoryCheckBox;
    let checkBox: HTMLCollectionOf<HTMLInputElement>;
    const targetElement =
      (e.target as HTMLElement).tagName === 'INPUT'
        ? (e.target as HTMLElement).parentElement
        : (e.target as HTMLElement);
    selectedType === 'row'
      ? (checkBox = getEditorElementsByClassName(
          'loree_custom_row_checkbox',
        ) as HTMLCollectionOf<HTMLInputElement>)
      : (checkBox = getEditorElementsByClassName(
          'loree_custom_elements_checkbox',
        ) as HTMLCollectionOf<HTMLInputElement>);
    if (
      targetElement?.id === 'loree_custom_row_label_all_category' ||
      targetElement?.id === 'loree_custom_elements_label_all_category'
    ) {
      allCategoryCheckBox = e.target as HTMLInputElement;
      if (allCategoryCheckBox.checked) {
        for (const check of checkBox) {
          check.checked = true;
        }
      } else {
        for (const check of checkBox) {
          check.checked = false;
        }
      }
    } else {
      let isAllChecked: boolean = true;
      selectedType === 'row'
        ? (allCategoryCheckBox = getEditorElementById('loree_custom_row_label_all_category')
            ?.childNodes[0] as HTMLInputElement)
        : (allCategoryCheckBox = getEditorElementById('loree_custom_elements_label_all_category')
            ?.childNodes[0] as HTMLInputElement);
      for (const check of checkBox) {
        if (!check.checked) {
          isAllChecked = false;
          break;
        }
        isAllChecked = true;
      }
      allCategoryCheckBox.checked = isAllChecked;
    }
  }
};

const showAllSelectedCategory = (e: MouseEvent, type: string) => {
  e.stopPropagation();
  const hiddenCategoryList: HTMLCollectionOf<Element> = getEditorElementsByClassName(
    'category-selected-list-item',
  );
  const moreCategoryLink = getEditorElementById('category-selection-more');
  if (moreCategoryLink?.innerHTML !== translate('modal.showless')) {
    for (const category of hiddenCategoryList) {
      removeClassToElement(category as HTMLElement, 'd-none');
    }
  } else handleBlockCategoryApplyButton(type);
  if (moreCategoryLink) moreCategoryLink.innerHTML = translate('modal.showless');
};

// sidebar custom block list
export const blockAccordionIconClick = async (e: Event, blockType: string, className: string) => {
  const accordionIcons: HTMLCollectionOf<Element> = getEditorElementsByClassName(className);
  for (const icon of accordionIcons) {
    icon.innerHTML = icon.innerHTML.replace('I', '+');
    removeClassToElement(icon as HTMLElement, 'accordion-min-icon-position');
  }
  let customBlockType: string;
  let accordionCheck: NodeListOf<ChildNode>;
  const Loader = `<div id="modal-loader" class="m-auto justify-content-center">
  <div class="icon rotating">
  ${videoModalIcons.loader}
  </div>
  <div class="title ml-3">${translate('global.loading')}</div>
</div>`;

  const targetElementParent = e.target as HTMLElement;
  let targetElement: Element =
    targetElementParent.tagName !== 'P'
      ? (targetElementParent.parentElement as Element)
      : (targetElementParent.childNodes[0] as Element);
  if (targetElement && targetElement.children[0].tagName === 'DIV')
    targetElement = targetElement.childNodes[0] as HTMLElement;
  if (targetElement) {
    setTimeout(() => {
      const validateVal =
        targetElement?.parentElement?.tagName === 'DIV'
          ? targetElement?.getAttribute('aria-expanded')
          : targetElement?.parentElement?.getAttribute('aria-expanded');
      if (validateVal === 'true') {
        targetElement.children[0].innerHTML = targetElement.children[0].innerHTML.replace('+', 'I');
        addClass(targetElement.children[0] as HTMLElement, 'accordion-min-icon-position');
        for (const checkType of accordionCheck) {
          if (!(checkType.childNodes[1] as HTMLElement).innerHTML.includes(blockType)) {
            removeClass(checkType.childNodes[0] as HTMLElement, 'text-primary');
            removeClass(checkType.childNodes[0] as HTMLElement, 'border-primary');
            removeClass(checkType.childNodes[1] as HTMLElement, 'text-primary');
          }
        }
        updateCustomBlockThumbnail(customBlockType);
      } else if (validateVal === 'false') {
        targetElement.children[0].innerHTML = targetElement.children[0].innerHTML.replace('I', '+');
        removeClass(targetElement.children[0] as HTMLElement, 'accordion-min-icon-position');
      }
    }, 10);
  }

  if (blockType.includes('Element')) {
    accordionCheck = document.querySelectorAll('#element-accordion-header');
    elementAccordionType = blockType;
    customBlockType = 'elements';
    if (elementAccordionType === 'My Element' && !isMyElementsCalled) {
      const element: HTMLElement | null = getEditorElementById('loree-my-elements-collapse');
      if (element) {
        element.innerHTML = `${Loader}`;
      }
      await retrieveCustomBlockElement();
      appendBlocksUI(elementData, 'loree-my-elements-collapse', elementAccordionType);
    }
    if (elementAccordionType === 'Shared Element' && !isSharedElementsCalled) {
      const element: HTMLElement | null = getEditorElementById('loree-shared-elements-collapse');
      if (element) {
        element.innerHTML = `${Loader}`;
      }
      await retrieveSharedCustomBlockElement();
      appendBlocksUI(sharedElementData, 'loree-shared-elements-collapse', elementAccordionType);
    }
  } else {
    accordionCheck = document.querySelectorAll('#row-accordion-header');
    rowAccordionType = blockType;
    customBlockType = 'row';
    if (rowAccordionType === 'My Row' && !isMyRowsCalled) {
      const element: HTMLElement | null = getEditorElementById('loree-my-rows-collapse');
      if (element) {
        element.innerHTML = `${Loader}`;
      }
      await retrieveCustomBlockRow();
      appendBlocksUI(rowData, 'loree-my-rows-collapse', rowAccordionType);
    }
    if (rowAccordionType === 'Shared Row' && !isSharedRowsCalled) {
      const element: HTMLElement | null = getEditorElementById('loree-shared-rows-collapse');
      if (element) {
        element.innerHTML = `${Loader}`;
      }
      await retrieveSharedCustomBlockRow();
      appendBlocksUI(sharedRowData, 'loree-shared-rows-collapse', rowAccordionType);
    }
  }
};

export const elementsBlock = (): HTMLElement => {
  const templatesBlockParent = document.createElement('div');
  templatesBlockParent.className = 'accordion';
  templatesBlockParent.id = 'loree-elements-link-accordion-wrapper';
  if (isCanvasAndBB())
    templatesBlockParent.appendChild(
      elementThumbnail(
        globalElementData,
        CONSTANTS.LOREE_GLOBAL_ELEMENTS_COLLAPSE,
        `${translate('element.globalelement')}`,
        true,
      ),
    );
  templatesBlockParent.appendChild(
    elementThumbnail(
      elementData,
      CONSTANTS.LOREE_MY_ELEMENTS_COLLAPSE,
      `${translate('element.myelement')}`,
      false,
    ),
  );
  if (sessionStorage.getItem('domainName') === 'canvas')
    templatesBlockParent.appendChild(
      elementThumbnail(
        sharedElementData,
        CONSTANTS.LOREE_SHARED_ELEMENTS_COLLAPSE,
        `${translate('element.sharedelement')}`,
        true,
      ),
    );
  return templatesBlockParent;
};
export const rowsBlock = (): HTMLElement => {
  const templatesBlockParent = document.createElement('div');
  templatesBlockParent.className = 'accordion';
  templatesBlockParent.id = 'loree-rows-link-accordion-wrapper';
  if (isCanvasAndBB())
    templatesBlockParent.appendChild(
      rowThumbnail(globalRowData, CONSTANTS.LOREE_GLOBAL_ROWS_COLLAPSE, 'Global Row', true),
    );
  templatesBlockParent.appendChild(
    rowThumbnail(rowData, CONSTANTS.LOREE_MY_ROWS_COLLAPSE, 'My Row', false),
  );
  if (sessionStorage.getItem('domainName') === 'canvas')
    templatesBlockParent.appendChild(
      rowThumbnail(sharedRowData, CONSTANTS.LOREE_SHARED_ROWS_COLLAPSE, 'Shared Row', true),
    );
  return templatesBlockParent;
};

export const changeHeaderTitle = (title: string) => {
  const headerTitle = getEditorElementById('loree-header-title');
  if (headerTitle) {
    headerTitle.innerHTML = title;
    headerTitle.title = title;
  }
};

// apppend  custom block in editors
export const handleAddCustomRow = async (e: Event, type: string) => {
  let globalStatus = false;
  let rowCheckData = rowData;
  handleAutoSaveOnCustomBlockAppend(true);
  let id = '';
  if (!e.target) {
    id = e as _Any;
  } else {
    id = (e.target as HTMLDivElement)?.parentElement?.parentElement?.id as string;
    const thumbnailTags = ['svg', 'path', 'BUTTON'];
    if (!thumbnailTags.includes((e.target as HTMLDivElement)?.tagName)) {
      baseClass().collapseRowSection();
      baseClass().disableRowSection();
    }
  }
  if (type === 'Global Row') {
    globalStatus = true;
    rowCheckData = globalRowData;
  } else if (type === 'Shared Row') rowCheckData = sharedRowData;
  for (const data of rowCheckData) {
    if (data.id === id) {
      const editorContent = await rowMigration(data);
      baseClass().appendContentToIframeContentWrapper(editorContent);
      if (e.target !== undefined) {
        // append exisiting custom row to editor
        baseClass().handleCustomBlockAppendButton(data.type);
      }
      // edit existing custom row
      else {
        editContentDetails(
          data.title,
          id,
          data.category.id,
          data.category.name,
          'Row',
          globalStatus,
        );
        changeHeaderTitle(data.title);
        baseClass().resetRedoUndoHistory();
        baseClass().setInitialHistory();
        baseClass().hideAddRowButtonWrapper();
        baseClass().disableRowSection();
      }
    }
  }
};

export const handleAddCustomElement = async (content: _Any, type: string) => {
  let globalStatus = false;
  let elementCheckData = elementData;
  handleAutoSaveOnCustomBlockAppend(true);
  baseClass().hideBlockOptionsToSelectedElements();
  let id = '';
  if (!content.target) id = content;
  else {
    id = content.target.parentElement.parentElement.id;
    const thumbnailTags = ['svg', 'path', 'BUTTON'];
    if (!thumbnailTags.includes(content.target.tagName)) {
      baseClass().collapseElementSection();
      baseClass().disableElementSection();
      const customElementWrapper = getEditorElementById(
        'loree-sidebar-custom-element-input-wrapper',
      );
      if (customElementWrapper) {
        removeClassToElement(customElementWrapper, 'd-block');
        addClassToElement(customElementWrapper, 'd-none');
      }
    }
  }
  if (type === 'Global Element') {
    globalStatus = true;
    elementCheckData = globalElementData;
  } else if (type === 'Shared Element') elementCheckData = sharedElementData;
  for (const data of elementCheckData) {
    if (data.id === id) {
      const editorContent = await elementMigration(data);
      // append exisiting custom element to editor
      if (content.target !== undefined) {
        baseClass().appandCustomElementTOSelectedColumn(editorContent);
        baseClass().handleCustomBlockAppendButton(data.type);
      } else {
        // edit existing custom element
        const wrapper = baseClass().getDocument();
        const iFrameDoc = wrapper?.getElementById('loree-iframe-content-wrapper');
        if (iFrameDoc) {
          iFrameDoc.style.padding = '30px';
          iFrameDoc.innerHTML = appendCustomElementToEditor(editorContent);
        }
        baseClass().hideHamburgerIconForRow();
        editContentDetails(
          data.title,
          id,
          data.category.id,
          data.category.name,
          'Elements',
          globalStatus,
        );
        changeHeaderTitle(data.title);
        baseClass().resetRedoUndoHistory();
        baseClass().setInitialHistory();
        baseClass().showHamburgerIconForColumn(iFrameDoc?.childNodes[0] as HTMLElement);
        baseClass().hideCloseElementButtonToSelectedElement();
        baseClass().hideAddRowButtonWrapper();
        baseClass().disableElementSection();
      }
    }
  }
};
