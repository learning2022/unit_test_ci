export interface CustomBlockInput {
  title: string;
  content: string;
  thumbnail: { bucket: string; key: string; region: string };
  status: boolean;
  id?: string | null;
  categoryID?: string | null;
  type?: string | null;
  createdBy?: string;
  owner?: string;
  isGlobal?: boolean;
  active?: boolean;
  ltiPlatformID?: string;
}
