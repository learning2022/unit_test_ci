import CONSTANTS from '../../constant';
import * as customBlockEvents from './customBlockEvents';
import * as customBlockhandlers from './customBlockHandler';
import * as customBlockAppenders from './customBlockAppenders';
import * as saveContent from '../../../utils/saveContent';
import {
  customBlockMockData,
  s3ImageConvertedMockData,
  successAlertMockDataForElement,
  successAlertMockDataForRow,
  userMockData,
} from './customBlockMockData';
import { currentEditBlock } from '../../../utils/saveContent';
import { lmsUrlRedirection } from '../../../lmsConfig';
import { API, Auth } from 'aws-amplify';
import { imageBlockThumbnailMockData } from '../../../views/editor/saveIcon/saveIconMockData';
import { createDiv } from '../../common/dom';
import { selectedColumnElement, selectedRowElement } from '../../baseMockData';
import { getEditorElementsByClassName } from '../../utils';

jest.mock('../../base/baseUtil', () => ({
  isSelectionFirstElementInContent: jest.fn(),
}));

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

function customBlockEditButton(blockType: string, menuWrapper: HTMLElement) {
  const wrapper = document.createElement('div');
  wrapper.id = `loree-custom-block-edit-modal-wrapper`;
  wrapper.innerHTML = `<button>Save & Edit</button>`;
  document.body.appendChild(wrapper);
  createDeleteModal();
  const button = wrapper.querySelector('button');
  button?.addEventListener('click', (e: MouseEvent) => {
    void customBlockhandlers.editCustomBlock(e, 'edit', menuWrapper, blockType);
  });
  return wrapper.childNodes[0] as HTMLElement;
}
function createDeleteModal() {
  const customBlockBackdrop = document.createElement('div');
  customBlockBackdrop.id = `loree-custom-block-edit-backdrop`;
  document.body.appendChild(customBlockBackdrop);
}

describe('# custom blocks events', () => {
  describe('#elements', () => {
    let elementBlocks;
    test('elements block UI', () => {
      elementBlocks = customBlockEvents.elementsBlock();
      expect(elementBlocks).toHaveClass('accordion');
      expect(elementBlocks).toBeInstanceOf(HTMLDivElement);
    });
    describe('#bb lms', () => {
      test('templates block UI in BB render global templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).toContainHTML('element.globalelements');
      });
      test('templates block UI in BB not render shared templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).not.toContainHTML('element.sharedelements');
      });
    });
    describe('# canvas lms', () => {
      test('elements block UI in canvas', () => {
        sessionStorage.setItem('domainName', 'canvas');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).toContainHTML('element.globalelements');
      });
    });
    describe('# d2l lms', () => {
      test('elements block UI in D2l render my rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).toContainHTML('element.myelement');
      });
      test('elements block UI in D2l not render global rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).not.toContainHTML('element.globalelements');
      });
    });
  });
  describe('#rows', () => {
    let rowsBlocks;
    test('rows block UI', () => {
      rowsBlocks = customBlockEvents.rowsBlock();
      expect(rowsBlocks).toBeInstanceOf(HTMLDivElement);
    });
    describe('# d2l lms', () => {
      test('rows block UI in D2l render my rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).toContainHTML('My Rows');
      });
      test('rows block UI in D2l not render global rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).not.toContainHTML('Global Rows');
      });
    });
    describe('#bb lms', () => {
      test('templates block UI in BB render global templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).toContainHTML('Global Rows');
      });
      test('templates block UI in BB not render shared templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).not.toContainHTML('shared Rows');
      });
    });
    describe('#canvas lms', () => {
      test('rows block UI in canvas', () => {
        sessionStorage.setItem('domainName', 'canvas');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).toContainHTML('Global Rows');
      });
    });
  });
});

describe('saveButton disable testcase', () => {
  let saveCategory: HTMLInputElement;
  let customBlockNameInput: HTMLInputElement;
  let newCategoryNameInput: HTMLInputElement;
  let saveButton: HTMLElement;
  let categoryDropdown: HTMLElement;
  let newCategoryBlock: HTMLElement;

  beforeEach(() => {
    const doc = document.body;
    saveCategory = document.createElement('input');
    saveCategory.id = CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME;
    saveCategory.value = 'textGiven';
    customBlockNameInput = document.createElement('input');
    customBlockNameInput.id = 'loree-custom-block-template-name';
    customBlockNameInput.value = 'textGiven';
    newCategoryNameInput = document.createElement('input');
    newCategoryNameInput.id = 'loree-custom-block-category-name';
    newCategoryNameInput.value = 'textGiven';
    saveCategory.append(customBlockNameInput);
    saveCategory.append(newCategoryNameInput);
    doc.append(saveCategory);
    saveButton = document.createElement('div');
    saveButton.innerHTML =
      '<button id="loree-custom-block-modal-footer-save-button" class="ok-button mx-1">';
    document.body.append(saveButton);
    categoryDropdown = document.createElement('div');
    categoryDropdown.innerHTML =
      '<button id="loree-custom-block-category-dropdown" class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    document.body.append(categoryDropdown);
    newCategoryBlock = document.createElement('div');
    newCategoryBlock.innerHTML =
      '<div id="loree-custom-block-new-category-group" class="form-group d-none">';
    document.body.append(newCategoryBlock);
  });
  afterEach(() => {
    document.getElementsByTagName('html')[0].innerHTML = '';
  });

  test('Field validating when the value not given need to disable attribute should be removed', async () => {
    customBlockEvents.enableSaveButton();
    const result = document.getElementById(
      'loree-custom-block-modal-footer-save-button',
    )?.outerHTML;
    expect(result).toBe(
      '<button id="loree-custom-block-modal-footer-save-button" class="ok-button mx-1"></button>',
    );
  });

  describe('#Save custom Blocks', () => {
    const customBlocks = [
      { element: selectedRowElement, elementType: 'Row', alert: successAlertMockDataForRow },
      {
        element: selectedColumnElement,
        elementType: 'Element',
        alert: successAlertMockDataForElement,
      },
    ];
    beforeEach(() => {
      const loreeWrapper = createDiv('div');
      loreeWrapper.id = CONSTANTS.LOREE_WRAPPER;
      document.body.appendChild(loreeWrapper);
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      jest
        .spyOn(customBlockEvents, 'customBlockThumbnailImage')
        .mockReturnValueOnce(imageBlockThumbnailMockData);
      API.graphql = jest.fn().mockImplementationOnce(() => s3ImageConvertedMockData);
      jest.spyOn(customBlockEvents, 'refreshCustomBlockThumbnail');
      jest.spyOn(customBlockEvents, 'retrieveGlobalCustomBlockElement');
      jest.spyOn(customBlockEvents, 'retrieveCustomBlockElement');
      jest.spyOn(customBlockEvents, 'retrieveSharedCustomBlockElement');
    });
    test.each(customBlocks)(
      'should save the customBlocks based on type if canvas',
      async ({ element, elementType, alert }) => {
        sessionStorage.setItem('domainName', 'canvas');
        const selectedElement = createDiv('div');
        selectedElement.innerHTML = element;
        customBlockEvents.customBlockInfoModal(
          elementType,
          selectedElement.firstChild as HTMLDivElement,
        );
        await customBlockEvents.saveBlocks('Element', '2c67b7b2-8361-47d5-b744-e90f12139da5');
        expect(getEditorElementsByClassName('custom-success-container')[0].innerHTML).toBe(alert);
      },
    );
    test.each(customBlocks)(
      'should save the customBlocks based on type if other lms',
      async ({ element, elementType, alert }) => {
        sessionStorage.setItem('domainName', 'bb');
        const selectedElement = createDiv('div');
        selectedElement.innerHTML = element;
        customBlockEvents.customBlockInfoModal(
          elementType,
          selectedElement.firstChild as HTMLDivElement,
        );
        await customBlockEvents.saveBlocks('Element', '2c67b7b2-8361-47d5-b744-e90f12139da5');
        expect(getEditorElementsByClassName('custom-success-container')[0].innerHTML).toBe(alert);
      },
    );
  });
});

describe('saveButton enable testcase', () => {
  let saveCategory: HTMLInputElement;
  let customBlockNameInput: HTMLInputElement;
  let newCategoryNameInput: HTMLInputElement;
  let saveButton: HTMLElement;

  beforeEach(() => {
    const doc = document.body;
    saveCategory = document.createElement('input');
    saveCategory.id = CONSTANTS.LOREE_CUSTOM_BLOCK_TEMPLATE_NAME;
    saveCategory.value = '';
    customBlockNameInput = document.createElement('input');
    customBlockNameInput.id = 'loree-custom-block-template-name';
    customBlockNameInput.value = '';
    newCategoryNameInput = document.createElement('input');
    newCategoryNameInput.id = 'loree-custom-block-category-name';
    newCategoryNameInput.value = '';
    saveCategory.append(customBlockNameInput);
    saveCategory.append(newCategoryNameInput);
    doc.append(saveCategory);
    saveButton = document.createElement('div');
    saveButton.innerHTML =
      '<button id="loree-custom-block-modal-footer-save-button" class="ok-button mx-1" >';
    document.body.append(saveButton);
  });

  test('Field validating when the value given need to disable is to be true', async () => {
    customBlockEvents.enableSaveButton();
    const result = document.getElementById(
      'loree-custom-block-modal-footer-save-button',
    )?.outerHTML;
    expect(result).toBe(
      '<button id="loree-custom-block-modal-footer-save-button" class="ok-button mx-1" disabled="true"></button>',
    );
  });
});

describe('#HideEditorOptions', () => {
  function editMenuParentWrapper() {
    const parentWrapper = document.createElement('div');
    parentWrapper.id = 'loreeWrapper';
    const thumbnailparent = document.createElement('div');
    thumbnailparent.id = 'd123e34fv6';
    thumbnailparent.innerHTML = customBlockMockData.sidebarThumbnailWrapper;
    parentWrapper.appendChild(thumbnailparent);
    return parentWrapper?.childNodes[0] as HTMLElement;
  }
  describe('editor footer icons rendered based on customBlock edit type', () => {
    let editorFooterOptions: HTMLElement;
    beforeEach(() => {
      window.sessionStorage.setItem('lmsUrl', 'https://test.com');
      const doc = document.body;
      const parentWrapper = document.createElement('div');
      parentWrapper.id = CONSTANTS.LOREE_IFRAME;
      parentWrapper.innerHTML = customBlockMockData.iframeInnerHtml;
      doc.append(parentWrapper);
      editorFooterOptions = document.getElementById(
        CONSTANTS.LOREE_FOOTER_SAVE_ICON_POSITION,
      ) as HTMLElement;
    });
    test('custom block element edit should not have clear editor and save as template options', () => {
      const editButton = customBlockEditButton(
        'My Element',
        editMenuParentWrapper().children[0].children[0] as HTMLElement,
      );
      editButton.click();
      expect(editorFooterOptions?.children[0]).toHaveClass('save-icon px-4 py-1 d-flex');
    });
    test('custom block row edit should not have clear editor option and must have save as template option', () => {
      const editButton = customBlockEditButton(
        'My Row',
        editMenuParentWrapper().children[0].children[0] as HTMLElement,
      );
      editButton.click();
      expect(
        editorFooterOptions
          ?.getElementsByClassName('editor-tooltip')[3]
          .getAttribute('data-original-title'),
      ).toEqual('Save as New Template');
      expect(editorFooterOptions?.getElementsByClassName('editor-tooltip')[4]).toHaveClass(
        'editor-tooltip',
      );
      expect(editorFooterOptions?.getElementsByClassName('editor-tooltip')[4]).toHaveClass(
        'editor-tooltip d-none',
      );
    });
    test('custom block template should not call hide options function', () => {
      const editButton = customBlockEditButton(
        'My Template',
        editMenuParentWrapper().children[0].children[0] as HTMLElement,
      );
      editButton.click();
      expect(editorFooterOptions?.children[0]).not.toHaveClass('d-none');
    });

    test('While edit My Elements the templateButton header should be disable', async () => {
      const templateButtonHeader = document.createElement('button');
      templateButtonHeader.id = 'loree-header-template-button';
      document.body.appendChild(templateButtonHeader);
      const editButton = customBlockEditButton(
        'My Element',
        editMenuParentWrapper().children[0].children[0] as HTMLElement,
      );
      editButton.click();
      const templateButton = document.getElementById(
        CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON,
      ) as HTMLElement;
      expect(templateButton).toBeDisabled();
    });
  });

  test('While edit My Rows the templateButton header should be disable', async () => {
    const templateButtonHeader = document.createElement('button');
    templateButtonHeader.id = 'loree-header-template-button';
    document.body.appendChild(templateButtonHeader);
    const editButton = customBlockEditButton(
      'My Row',
      editMenuParentWrapper().children[0].children[0] as HTMLElement,
    );
    editButton.click();
    const templateButton = document.getElementById(
      CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON,
    ) as HTMLElement;
    expect(templateButton).toBeDisabled();
  });

  test('After redirecting to dashboard  the templateButton header should be enabled', async () => {
    document.getElementsByTagName('html')[0].innerHTML = '';
    window.location.replace(lmsUrlRedirection());
    const templateButtonHeader = document.createElement('button');
    templateButtonHeader.id = 'loree-header-template-button';
    document.body.appendChild(templateButtonHeader);
    const templateButton = document.getElementById(
      CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON,
    ) as HTMLElement;
    expect(templateButton).toBeEnabled();
  });
});

describe('#CustomBlockThumbnailOptions', () => {
  const templateList = [
    {
      templateId: 'd123e34fv6',
      templateName: 'Template-1',
    },
    {
      templateId: '9edab38fdjo',
      templateName: 'Template-2',
    },
    {
      templateId: 'nh723nso33n',
      templateName: 'Template-3',
    },
  ];
  function customBlockList(id: string) {
    const parentWrapper = document.createElement('div');
    parentWrapper.id = id;
    templateList.forEach((template) => {
      const thumbnailparent = document.createElement('div');
      thumbnailparent.id = template.templateId;
      thumbnailparent.className = 'panel';
      thumbnailparent.innerHTML = customBlockAppenders.appendThumbnailMenu('My Template').outerHTML;
      parentWrapper.appendChild(thumbnailparent);
    });
    return parentWrapper;
  }
  let templateBlockWrapper: HTMLElement;
  let editButton: HTMLElement;

  beforeEach(() => {
    document.body.appendChild(customBlockList(CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE));
    templateBlockWrapper = document.getElementById(
      CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE,
    ) as HTMLElement;
  });
  test('current custom block id is stored for handling thumbnail options', () => {
    editButton = (templateBlockWrapper?.childNodes[0] as HTMLElement).querySelector(
      '#loree-sidebar-custom-block-thumbnail-dropdown-menu',
    )?.childNodes[0] as HTMLElement;
    editButton.addEventListener('click', function (e) {
      void customBlockAppenders.handleThumbnailMenu(
        editButton.parentElement?.parentElement as HTMLElement,
        'global.edit',
        'global.mytemplate',
      );
    });
    editButton.dispatchEvent(new Event('click'));
    const currentBlockId = editButton.closest('.panel')?.getAttribute('id') as string;
    expect(currentEditBlock.id).toEqual(currentBlockId);
  });

  describe('#ThumbnailOptions', () => {
    test('thumbnail options are hidden for selected block', () => {
      customBlockEditButton(
        'My Template',
        editButton.parentElement?.parentElement as HTMLElement,
      ).click();
      const selectedBlockDeleteBtn = (
        templateBlockWrapper?.childNodes[0] as HTMLElement
      ).querySelector('#loree-sidebar-custom-block-thumbnail-dropdown-menu')
        ?.childNodes[1] as HTMLElement;
      expect(editButton).toHaveClass('d-none');
      expect(selectedBlockDeleteBtn).toHaveClass('d-none');
    });
    test('other selected options must have edit and delete options', () => {
      const templateLists = templateBlockWrapper?.childNodes;
      for (let i = 1; i < templateLists.length; i++) {
        expect(
          (templateLists[i] as HTMLElement).querySelector(
            '#loree-sidebar-custom-block-thumbnail-dropdown-menu',
          )?.childNodes[0],
        ).not.toHaveClass('d-none');
        expect(
          (templateLists[i] as HTMLElement).querySelector(
            '#loree-sidebar-custom-block-thumbnail-dropdown-menu',
          )?.childNodes[1],
        ).not.toHaveClass('d-none');
      }
    });
  });

  describe('Hide thumbnail options', () => {
    const customBlockType = [
      { type: 'My Template', id: CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE },
      { type: 'My Row', id: CONSTANTS.LOREE_MY_ROWS_COLLAPSE },
      { type: 'Elements', id: CONSTANTS.LOREE_MY_ELEMENTS_COLLAPSE },
    ];
    beforeEach(() => {
      document.body.innerHTML = '';
    });
    test.each(customBlockType)(
      'should hide the options for a particular block when it is in edit state',
      ({ type, id }) => {
        document.body.appendChild(customBlockList(id));
        const customBlockWrapper = document.getElementById(id) as HTMLElement;
        editButton = (customBlockWrapper?.childNodes[0] as HTMLElement).querySelector(
          '#loree-sidebar-custom-block-thumbnail-dropdown-menu',
        )?.childNodes[0] as HTMLElement;
        editButton.addEventListener('click', function (e) {
          void customBlockAppenders.handleThumbnailMenu(
            editButton.parentElement?.parentElement as HTMLElement,
            'global.edit',
            'global.mytemplate',
          );
        });
        editButton.dispatchEvent(new Event('click'));
        const currentBlockId = editButton.closest('.panel')?.getAttribute('id') as string;
        expect(currentEditBlock.id).toEqual(currentBlockId);
        customBlockEditButton(type, editButton.parentElement?.parentElement as HTMLElement).click();
        expect(editButton).toHaveClass('d-none');
      },
    );

    describe('Hiding custom Blocks', () => {
      beforeEach(() => {
        saveContent.setCurrentEditBlock('');
        saveContent.setEditMode(false);
      });
      test.each(customBlockType)(
        'should not hide the options for a particular block when it is not in  edit state',
        ({ type }) => {
          expect(customBlockhandlers.setCustomBlockEditOptions(type)).toBeNull();
        },
      );
    });
  });
});
