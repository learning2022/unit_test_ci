import { API, Auth, graphqlOperation } from 'aws-amplify';
import config from '../../../aws-exports';
import { LmsAccess } from '../../../utils/lmsAccess';
import { getImageUrl } from '../../../utils/imageUpload';
import { handleAddCustomTemplate, refreshTemplateList } from './customTemplateEvents';
import {
  fetchPageDetails,
  updateStandaloneEditorContent,
  setEditMode,
  customBlockEditStatus,
  customBlockId,
  customBlockTitle,
  customBlockType,
  customBlockSelectedCategoryId,
  updateCanvasEditorContent,
  updateBBEditorContent,
  updateD2lEditorContent,
  d2lProps,
  currentEditBlock,
  isContainer,
  convertCourseImageAsS3Image,
} from '../../../utils/saveContent';
import {
  appendModalContentHeader,
  appendModalContentBody,
  appendModelContentFooter,
} from './customBlocksUI';
import {
  retrieveCustomBlockRow,
  retrieveCustomBlockElement,
  handleAddCustomElement,
  handleAddCustomRow,
  customBlockThumbnailImage,
  type,
  refreshCustomBlockThumbnail,
} from './customBlockEvents';
import { elementListAppend, rowListAppend } from './customBlockAppenders';
import { customElementAlert, savePageAlert, savePageAlertContainerDisapper } from '../../alert';
import Base from '../../base';
import {
  updateCustomTemplate,
  deleteCustomTemplate,
  deleteCustomBlock,
  updateCustomBlock,
} from '../../../graphql/mutations';
import CONSTANTS from '../../constant';
import { getEditorElementById, removeClassToElement, addClassToElement } from '../../utils';
import { translate } from '../../../i18n/translate';

interface EditorPageDetail {
  app: string;
  pageId: string;
  title: string | undefined;
  contentType: string | undefined;
}
export const { aws_user_files_s3_bucket_region: region, aws_user_files_s3_bucket: bucket } = config;
export const baseClass = () => {
  const base = new Base();
  return base;
};

export const deletePopup = (modalType: string) => {
  const customBlockBackdrop = document.getElementById(`loree-custom-block-${modalType}-backdrop`);
  customBlockBackdrop?.remove();
  const customBlockModalPopup = document.getElementById(
    `loree-custom-block-${modalType}-modal-wrapper`,
  );
  customBlockModalPopup?.remove();
  baseClass().hideCloseRowButton();
  baseClass().showAddRowButton();
  baseClass().hideAddElementButtonToSelectedElement();
};

export const updateTemplateThumbnail = async (
  id: string,
  active: boolean | null,
  thumbnail: string,
) => {
  if (active) thumbnail = (await getImageUrl(thumbnail)) as string;
  const template = document.getElementById(id);
  const templateImage = template?.getElementsByTagName('img')[0] as HTMLImageElement;
  if (templateImage) {
    templateImage.src = thumbnail;
  }
};

export const appendCustomElementToEditor = (data: string) => {
  const element = document.createElement('div');
  element.innerHTML = data;
  const column = element.childNodes[0] as HTMLElement;
  column.classList.add(
    CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN,
    CONSTANTS.LOREE_COLUMN_HIGHLIGHT,
    'element-highlight',
  );
  column.style.padding = '20px';
  column.childNodes.forEach((ele) => {
    if ((ele as HTMLElement).classList?.contains('custom-success-container')) {
      ele.remove();
    }
  });
  return element.innerHTML;
};

// thumbnail image blob conversion
export const b64toBlob = (b64Data: string, contentType: string, sliceSize = 512) => {
  const data = b64Data.substring(b64Data.indexOf(',') + 1);
  const byteCharacters = window.atob(data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);
    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
};

// For deleting custom template/blocks
export const confirmDeleteCustomBlock = async (
  menuWrapper: HTMLElement,
  modalType: string,
  blockType: string,
) => {
  const type = blockType === 'template' ? 'Template' : blockType === 'row' ? 'Row' : 'Elements';
  const deleteButtonText: HTMLElement | null = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_DELETE_BUTTON,
  );
  if (deleteButtonText) {
    deleteButtonText.innerHTML = translate('global.deleting');
  }
  if (blockType === 'template') {
    await API.graphql(
      graphqlOperation(deleteCustomTemplate, {
        input: { id: menuWrapper?.parentElement?.parentElement?.getAttribute('id') },
      }),
    );
    void refreshTemplateList('myTemplates');
  } else {
    await API.graphql(
      graphqlOperation(deleteCustomBlock, {
        input: { id: menuWrapper.parentElement?.parentElement?.getAttribute('id') },
      }),
    );
    if (blockType === 'row') {
      await retrieveCustomBlockRow();
      rowListAppend();
    } else {
      await retrieveCustomBlockElement();
      elementListAppend();
    }
  }
  menuWrapper.parentElement?.parentElement?.remove();
  deletePopup(modalType);
  customElementAlert(type, 'deleted');
  baseClass().customElementAlertTimer();
};

// For changing the modal content
export const deleteCustomTemplateBlock = async (
  menuWrapper: HTMLElement,
  modalType: string,
  blockType: string,
) => {
  const customBlockCancelButton = document.getElementById(
    `loree-custom-block-${modalType}-modal-footer-cancel-button`,
  );
  const deleteButtonText = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_DELETE_BUTTON,
  );
  const modalContent = document.getElementById(`delete-custom-block-content`);
  const archiveButton = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_ARCHIVE_BUTTON,
  );
  if (customBlockCancelButton && deleteButtonText && modalContent && archiveButton) {
    customBlockCancelButton.innerText = translate('global.discard');
    deleteButtonText.innerText = translate('modal.confirmdelete');
    deleteButtonText.onclick = async () =>
      await confirmDeleteCustomBlock(menuWrapper, modalType, blockType);
    modalContent.innerHTML = `<p><span class="text-capitalize">${blockType}s </span>${translate(
      'modal.whicharepermanentlydeleted',
    )}</p>`;
    archiveButton.remove();
  }
};

// For archiving custom template/blocks
export const archiveCustomTemplateBlock = async (
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
) => {
  const type =
    blockType === translate('modal.template')
      ? translate('global.template')
      : blockType === translate('modal.row')
      ? translate('global.row')
      : translate('global.elements');
  const deleteButtonText = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_FOOTER_ARCHIVE_BUTTON,
  );
  if (deleteButtonText) {
    deleteButtonText.innerHTML = translate('global.archiving');
  }
  const deletecustomBlockInput = {
    id: menuWrapper?.parentElement?.parentElement?.getAttribute('id'),
    active: false,
  };
  if (blockType === translate('modal.template')) {
    await API.graphql(graphqlOperation(updateCustomTemplate, { input: deletecustomBlockInput }));
    void refreshTemplateList('myTemplates');
  } else {
    await API.graphql(graphqlOperation(updateCustomBlock, { input: deletecustomBlockInput }));
    if (blockType === translate('modal.row')) {
      await retrieveCustomBlockRow();
      rowListAppend();
    } else {
      await retrieveCustomBlockElement();
      elementListAppend();
    }
  }
  menuWrapper.parentElement?.parentElement?.remove();
  deletePopup(modalType);
  customElementAlert(type, 'archived');
  baseClass().customElementAlertTimer();
};

export const editCustomBlock = async (
  e: Event,
  modalType: string,
  menuWrapper: HTMLElement,
  blockType: string,
) => {
  const target = e.target as HTMLInputElement | null;
  if (target) {
    target.innerText = translate('global.saving');
    target.disabled = true;
  }
  const pageDetail: EditorPageDetail = fetchPageDetails();
  if (pageDetail.app === 'loree-editor' && pageDetail.pageId !== '') {
    void updateStandaloneEditorContent(pageDetail.pageId);
  } else {
    if (customBlockEditStatus && customBlockId && customBlockTitle) {
      const currentAuthenticatedUser = await Auth.currentAuthenticatedUser();
      const iframe = document.getElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
      const iframeContent = iframe.contentDocument?.getElementById(
        'loree-iframe-content-wrapper',
      ) as HTMLDivElement;
      const ltiPlatformId = currentAuthenticatedUser.attributes['custom:platform'];
      const customBlockInput = {
        id: customBlockId,
        title: customBlockTitle,
        categoryID: customBlockSelectedCategoryId,
        ltiPlatformID: ltiPlatformId || 'STANDALONE',
        content: await convertCourseImageAsS3Image(
          baseClass().getHtml(),
          currentAuthenticatedUser.attributes.sub,
        ),
        thumbnail: await customBlockThumbnailImage(customBlockTitle, 'template', iframeContent),
        status: true,
        active: true,
      };
      if (customBlockType === 'My Template') {
        await API.graphql(graphqlOperation(updateCustomTemplate, { input: customBlockInput }));
        await refreshTemplateList('myTemplates');
      } else {
        await API.graphql(graphqlOperation(updateCustomBlock, { input: customBlockInput }));
        await refreshCustomBlockThumbnail(customBlockType as string, 'refreshMyBlocks');
      }
    }
    if (!customBlockId && !customBlockTitle) {
      void updateEditorContentBeforEditMode(pageDetail, modalType);
    }
  }

  if (blockType === 'My Row' || blockType === 'My Element') {
    const templateButton = document.getElementById(
      CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON,
    ) as HTMLButtonElement;
    if (templateButton) {
      templateButton.disabled = true;
    }
  }

  savePageAlert(
    customBlockEditStatus
      ? `<span class="text-capitalize">${
          customBlockType === 'My Template' ? translate('alert.template') : customBlockType
        }</span> ${translate('alert.savesuccessfullydetail', {
          detail: getHeaderTitle(),
        })}`
      : translate('alert.savesuccessfullydetailpage', {
          detail: getHeaderTitle(),
        }),
  );
  savePageAlertContainerDisapper();
  setEditMode(true);
  isContainer.active = true;
  baseClass().wipeOutEditorContent();
  customiseBlockOptionsOnEdit(blockType, menuWrapper);
  deletePopup(modalType);
  setCustomBlockEditOptions(blockType);
};

export const setCustomBlockEditOptions = (blockType: string) => {
  if (!customBlockEditStatus || currentEditBlock.id === '') {
    return null;
  }
  const currentBlock =
    blockType === 'My Template'
      ? CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE
      : blockType === 'My Row'
      ? CONSTANTS.LOREE_MY_ROWS_COLLAPSE
      : CONSTANTS.LOREE_MY_ELEMENTS_COLLAPSE;
  hideCurrentBlockThumbnailOptions(currentBlock);
  showCustomBlockThumbnailOptions(currentBlock);
};

export const updateEditorContentBeforEditMode = async (
  pageDetail: EditorPageDetail,
  modalType?: string,
) => {
  const domainName = sessionStorage.getItem('domainName');
  switch (domainName) {
    case 'BB':
      await updateBBEditorContent(
        pageDetail.pageId,
        pageDetail.title as string,
        pageDetail.contentType,
        modalType,
      );
      break;
    case 'canvas':
      await updateCanvasEditorContent(pageDetail.pageId, 'page', modalType);
      break;
    case 'D2l':
      await updateD2lEditorContent(
        pageDetail.pageId,
        d2lProps.moduleType,
        d2lProps.moduleContent,
        d2lProps.headTagContent,
      );
      break;
  }
};

const customiseBlockOptionsOnEdit = (blockType: string, menuWrapper: HTMLElement) => {
  if (blockType === 'My Template') baseClass().showAddRowButtonWrapper();
  changeSaveOptionList(blockType);
  if (blockType === 'Global Element' || blockType === 'My Element') {
    void handleAddCustomElement(
      menuWrapper.parentElement?.parentElement?.getAttribute('id'),
      blockType,
    );
  } else if (blockType === 'Global Row' || blockType === 'My Row') {
    void handleAddCustomRow(
      menuWrapper.parentElement?.parentElement?.getAttribute('id') as unknown as Event,
      blockType,
    );
  } else
    handleAddCustomTemplate(
      menuWrapper.parentElement?.parentElement?.getAttribute('id') as unknown as Event,
      blockType,
    );
};

const hideCurrentBlockThumbnailOptions = (currentBlock: string) => {
  const currentBlockCollapsePanel = getEditorElementById(currentBlock);
  currentBlockCollapsePanel?.childNodes.forEach((el: ChildNode) => {
    if (!el || el.textContent === translate('global.noresultsfound')) return;
    const thumbnailDropdownWrapper = (el as HTMLElement)?.querySelector(
      '#loree-sidebar-custom-block-thumbnail-dropdown-menu',
    ) as HTMLElement;
    if ((el as HTMLElement).id === currentEditBlock.id) {
      addClassToElement(thumbnailDropdownWrapper?.childNodes[0] as HTMLElement, 'd-none');
      addClassToElement(thumbnailDropdownWrapper?.childNodes[1] as HTMLElement, 'd-none');
    } else {
      removeClassToElement(thumbnailDropdownWrapper?.childNodes[0] as HTMLElement, 'd-none');
      removeClassToElement(thumbnailDropdownWrapper?.childNodes[1] as HTMLElement, 'd-none');
    }
  });
};

const showCustomBlockThumbnailOptions = (customBlockPanelId: string) => {
  const customBlockPanels: string[] = [
    CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE,
    CONSTANTS.LOREE_MY_ROWS_COLLAPSE,
    CONSTANTS.LOREE_MY_ELEMENTS_COLLAPSE,
  ];
  customBlockPanels.splice(customBlockPanels.indexOf(customBlockPanelId), 1);
  customBlockPanels.forEach((id) => {
    const customBlockCollapsePanel = getEditorElementById(id);
    customBlockCollapsePanel?.childNodes.forEach((el: ChildNode) => {
      const thumbnailDropdownWrapper = (el as HTMLElement).querySelector(
        '#loree-sidebar-custom-block-thumbnail-dropdown-menu',
      ) as HTMLElement;
      (thumbnailDropdownWrapper.childNodes[0] as HTMLElement).classList.remove('d-none');
      (thumbnailDropdownWrapper.childNodes[1] as HTMLElement).classList.remove('d-none');
    });
  });
};

export const changeSaveOptionList = (blockType: string) => {
  let changeSaveOptionList;
  const lms = new LmsAccess();
  if (lms.getAccess()) {
    changeSaveOptionList = document.getElementById('save-icon-position')
      ?.childNodes[0] as HTMLElement;
  } else {
    changeSaveOptionList = document.getElementById('save-option-list-wrapper');
    handleClassRemoval(changeSaveOptionList?.childNodes[4] as HTMLElement, 'mr-2');
    handleClassAdd(changeSaveOptionList?.childNodes[5] as HTMLElement, 'd-none');
  }
  if (customBlockEditStatus && changeSaveOptionList) {
    handleClassRemoval(changeSaveOptionList, 'justify-content-between');
    handleClassAdd(changeSaveOptionList, 'justify-content-center');
    handleClassAdd(changeSaveOptionList.childNodes[0] as HTMLElement, 'd-none');
    handleClassAdd(changeSaveOptionList.childNodes[1] as HTMLElement, 'd-none');
    if (changeSaveOptionList?.childNodes[3])
      handleClassRemoval(changeSaveOptionList?.childNodes[3] as HTMLElement, 'd-none');
    else handleClassRemoval(changeSaveOptionList?.childNodes[1] as HTMLElement, 'd-none');
    switch (blockType) {
      case 'My Row':
        handleClassAdd(changeSaveOptionList.childNodes[2] as HTMLElement, 'd-none');
        handleClassAdd(changeSaveOptionList?.childNodes[4] as HTMLElement, 'd-none');
        handleClassRemoval(changeSaveOptionList?.childNodes[3] as HTMLElement, 'mr-3');
        handleClassRemoval(changeSaveOptionList, 'd-none');
        break;
      case 'My Element':
        handleClassAdd(changeSaveOptionList, 'd-none');
        break;
      case 'My Template':
        handleClassRemoval(changeSaveOptionList?.childNodes[2] as HTMLElement, 'd-none');
        handleClassRemoval(changeSaveOptionList, 'd-none');
        break;
    }
  }
};

const handleClassAdd = (element: HTMLElement, property: string) => {
  element?.classList.add(property);
};

const handleClassRemoval = (element: HTMLElement, property: string) => {
  element?.classList.remove(property);
};

export const getHeaderTitle = (): string | undefined => {
  let title: string | undefined;
  const titleContainer = document.getElementById('loree-header-title');
  if (titleContainer && titleContainer.title !== '') {
    title = titleContainer.title;
  } else {
    title = customBlockEditStatus ? customBlockTitle || '' : fetchPageDetails().pageId;
  }
  return title;
};

export const sortComparision = (arg: { createdAt: string }, arg2: { createdAt: string }) => {
  return arg2.createdAt.localeCompare(arg.createdAt);
};

export const modalContainer = () => {
  const customModalContainer = document.createElement('div');
  customModalContainer.id = CONSTANTS.LOREE_CUSTOM_BLOCK_MODAL_CONTAINER;
  customModalContainer.className = 'modal-content';
  let innerHTML = appendModalContentHeader(type);
  innerHTML += appendModalContentBody();
  innerHTML += appendModelContentFooter();
  customModalContainer.innerHTML = innerHTML;
  return customModalContainer;
};

export const customBlockMessage = (blockType: string, isGlobal: boolean) => {
  if (isGlobal) return blockType + ' removed successfully';
  else return blockType + ' updated successfully';
};
