import { API, graphqlOperation } from 'aws-amplify';
import { apm } from '@elastic/apm-rum';
import CONSTANTS from '../../constant';
import { elementAddRemoveIcons } from '../../iconHolder';
import { categoryByPlatform, listCustomTemplates } from '../../../graphql/queries';
import { changeHeaderTitle } from './customBlockEvents';
import Base from '../../base';
import { editContentDetails, handleAutoSaveOnCustomBlockAppend } from '../../../utils/saveContent';
import { getImageUrl } from '../../../utils/imageUpload';
import { templateThumbnail } from './customBlocksUI';
import { appendThumbnailMenu } from './customBlockAppenders';
import { videoModalIcons } from '../../modules/sidebar/iconHolder';
import { isCanvasAndBB } from '../../../lmsConfig/index';
import {
  listingCustomTemplates,
  listingGlobalTemplates,
  listingSharedTemplates,
} from './customBlocksAction';
import { setCustomBlockEditOptions, sortComparision } from './customBlockHandler';
import {
  CategoryByPlatformQuery,
  ListCustomTemplatesQuery,
  ListGlobalTemplatessQuery,
} from '../../../API';
import { getS3urlFromKey } from '../../../lti/admin/globalImagesUpload/uploadImageToS3';
import { getEditorElementById } from '../../utils';

let categoryLists: _Any;
export let templateData: _Any[] = [];
let templateFilterData;
let globalTemplateData: _Any[] = [];
let sharedTemplateData: _Any[] = [];
let filterCategoryId: string[] = [];
const checkedCheckBoxes: (ChildNode | null)[] = [];
let accordionType = !isCanvasAndBB() ? 'My Template' : 'Global Template';
export let isMyTemplateCalled = false;
export let isSharedTemplateCalled = false;
// category list fetching
export const fetchCategory = async () => {
  const ltiPlatformId = sessionStorage.getItem('ltiPlatformId');
  let category;
  if (ltiPlatformId) {
    category = await API.graphql<CategoryByPlatformQuery>(
      graphqlOperation(categoryByPlatform, { ltiPlatformID: ltiPlatformId }),
    );
  } else {
    category = await API.graphql<CategoryByPlatformQuery>(
      graphqlOperation(categoryByPlatform, { ltiPlatformID: 'STANDALONE' }),
    );
  }
  categoryLists = category?.data?.categoryByPlatform?.items ?? [];
  categoryLists?.sort((a: { createdAt: string }, b: { createdAt: string }) => {
    return b?.createdAt.localeCompare(a?.createdAt);
  });
};

export const refreshCheckBox = () => {
  const checkBox = document.getElementsByClassName(
    `loree-custom-template-checkbox`,
  ) as HTMLCollection;
  const allCategoryCheckBox = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY,
  )?.childNodes[0] as HTMLInputElement;
  for (const check of checkBox) {
    (check as HTMLInputElement).checked = true;
  }
  if (allCategoryCheckBox) allCategoryCheckBox.checked = true;
  handleTemplateCategoryApplyButton();
};

export const clearTemplateSearch = () => {
  const templateSearchInput = getEditorElementById(
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT_VALUE,
  ) as HTMLInputElement;
  if (templateSearchInput) {
    templateSearchInput.value = '';
  }
  refreshCheckBox();
};

export const categoryWrapper = () => {
  return `<div class="dropdown dropdown-layout mt-3" id=${
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_LIST
  }>
    <button id=${
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_DROPDOWN
    } class="btn dropdown-toggle loree-sidebar-custom-block-category-dropdown" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">Select Category</button>
    <div class="dropdown-menu loree-sidebar-custom-block-category-dropdown-menu" id=${
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_DROPDOWN_MENU
    } aria-labelledby=${CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_LIST}>
    <div id="loree-sidebar-custom-template-category-list-wrapper">
    <div id=${CONSTANTS.LOREE_CUSTOM_GLOBAL_TEMPLATE_ALL_CATEGORY}><label id=${
    CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY
  }><input type="checkbox" class="mx-1"/>All</ label></div>
          ${categoryLists
            .map(
              (list: { id: string; name: string }) =>
                `<div><label id = ${list.id} title="${list.name}"><input type="checkbox" class="mx-1 loree-custom-template-checkbox"/>${list.name}</ label></div>`,
            )
            .join('')}
          </div>
          <div id="loree-sidebar-custom-block-category-button" class="d-flex flex-row flex-nowrap align-items-center mt-1">
            <div style="margin: auto;">
              <button id='loree-sidebar-custom-row-category-button-cancel' class="mx-1">Cancel</button>
              <button class="apply-button mx-1" id='loree-sidebar-custom-global-template-category-button-apply'>Apply</button>
            </div>
          </div>
        </div>
      </div>
  `;
};

export const refreshCategoryDropdown = () => {
  const checkBox = document.getElementsByClassName(
    'loree-custom-template-checkbox',
  ) as HTMLCollection;
  const allCategories = [];
  for (const check of checkBox) {
    allCategories.push((check as HTMLElement)?.parentElement?.id);
  }
  checkSelectedCheckBoxes(checkBox);
};

const checkSelectedCheckBoxes = (checkBox: HTMLCollection) => {
  const allCategoryCheckBox = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY,
  )?.childNodes[0] as HTMLInputElement;
  const checkBoxLabel = [];
  checkBoxLabel.push(allCategoryCheckBox.parentElement);
  for (const check of checkBox) {
    checkBoxLabel.push(check.parentElement);
    (check as HTMLInputElement).checked = false;
  }
  allCategoryCheckBox.checked = false;
  if (checkedCheckBoxes.length !== 0) {
    for (const label of checkBoxLabel) {
      if (checkedCheckBoxes.includes(label))
        (label?.childNodes[0] as HTMLInputElement).checked = true;
    }
  }
};

export const retrieveCustomGlobalTemplate = async () => {
  globalTemplateData = [];
  const globalTemplatesLists = [];
  const globalTemplatesItems = await listingGlobalTemplates();
  for (const globalTemplate of globalTemplatesItems) {
    if (globalTemplate?.active) {
      if (globalTemplate.thumbnail) {
        globalTemplate.thumbnail = (await getImageUrl(globalTemplate?.thumbnail?.key)) as _Any;
      }
      globalTemplatesLists.push(globalTemplate);
    }
  }
  globalTemplateData = globalTemplatesLists as (ListGlobalTemplatessQuery | null)[];
  globalTemplateData.sort(sortComparision);
};

export const retrieveCustomTemplate = async () => {
  templateData = [];
  let listTemplates;
  const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
  if (ltiPlatformID) {
    listTemplates = await listingCustomTemplates();
  } else {
    listTemplates = await API.graphql<ListCustomTemplatesQuery>(
      graphqlOperation(listCustomTemplates, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
    );
    listTemplates = listTemplates?.data?.listCustomTemplates?.items ?? [];
  }
  isMyTemplateCalled = true;
  for (const listTemplate of listTemplates) {
    if (listTemplate?.active) {
      listTemplate.thumbnail = (await getImageUrl(listTemplate?.thumbnail?.key)) as _Any;
      templateData.push(listTemplate);
    }
  }
  templateData.sort(sortComparision);
};

export const retrieveCustomSharedTemplate = async () => {
  sharedTemplateData = [];
  const sharedTemplatesLists = [];
  const sharedTemplatesItems = await listingSharedTemplates();
  isSharedTemplateCalled = true;
  for (const sharedTemplate of sharedTemplatesItems) {
    if (sharedTemplate?.active) {
      sharedTemplate.thumbnail = (await getImageUrl(sharedTemplate?.thumbnail?.key)) as _Any;
      sharedTemplatesLists.push(sharedTemplate);
    }
  }
  sharedTemplateData = sharedTemplatesLists;
  sharedTemplateData.sort(sortComparision);
};

export const templatesBlock = (): HTMLElement => {
  const templatesBlockParent = document.createElement('div');
  templatesBlockParent.className = 'accordion pb-3';
  templatesBlockParent.id = 'loree-templates-link-accordion-wrapper';
  if (isCanvasAndBB())
    templatesBlockParent.appendChild(
      templateThumbnail(
        globalTemplateData,
        CONSTANTS.LOREE_GLOBAL_TEMPLATES_COLLAPSE,
        'Global Template',
        true,
      ),
    );
  templatesBlockParent.appendChild(
    templateThumbnail(templateData, CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE, 'My Template', false),
  );
  if (sessionStorage.getItem('domainName') === 'canvas')
    templatesBlockParent.appendChild(
      templateThumbnail(
        sharedTemplateData,
        CONSTANTS.LOREE_SHARED_TEMPLATES_COLLAPSE,
        'Shared Template',
        true,
      ),
    );
  return templatesBlockParent;
};

export const accordionIconClick = async (e: Event, templateType: string) => {
  const accordionIcons = document.getElementsByClassName('template-accordion') as HTMLCollection;
  for (const icon of accordionIcons) {
    icon.innerHTML = icon.innerHTML.replace('I', '+');
    icon.classList.remove('accordion-min-icon-position');
  }
  const Loader = `<div id="modal-loader" class="m-auto justify-content-center">
  <div class="icon rotating">
  ${videoModalIcons.loader}
  </div>
  <div class="title ml-3">Loading...</div>
</div>`;

  const accordionCheck = document.querySelectorAll('#template-accordion-header');
  const targetElementParent = e.target as HTMLElement;
  let targetElement: _Any;
  if (targetElementParent.tagName !== 'P')
    targetElement = targetElementParent.parentElement as HTMLElement;
  else targetElement = targetElementParent.childNodes[0];
  accordionType = templateType;
  if (targetElement && targetElement.children[0].tagName === 'DIV')
    targetElement = targetElement.childNodes[0] as HTMLElement;
  if (targetElement) {
    setTimeout(() => {
      let validateVal;
      if (targetElement?.parentElement.tagName === 'DIV')
        validateVal = targetElement?.getAttribute('aria-expanded');
      else validateVal = targetElement?.parentElement?.getAttribute('aria-expanded');
      if (validateVal === 'true') {
        targetElement.children[0].innerHTML = targetElement.children[0].innerHTML.replace('+', 'I');
        addClass(targetElement.children[0], 'accordion-min-icon-position');
        for (const checkType of accordionCheck) {
          if (!(checkType.childNodes[1] as HTMLElement).innerHTML.includes(templateType)) {
            removeClass(checkType.childNodes[0] as HTMLElement, 'text-primary');
            removeClass(checkType.childNodes[0] as HTMLElement, 'border-primary');
            removeClass(checkType.childNodes[1] as HTMLElement, 'text-primary');
          }
        }
        updateTemplateThumbnail();
      } else if (validateVal === 'false') {
        targetElement.children[0].innerHTML = targetElement.children[0].innerHTML.replace('I', '+');
        removeClass(targetElement.children[0], 'accordion-min-icon-position');
      }
    }, 10);
  }

  if (accordionType === 'My Template' && !isMyTemplateCalled) {
    const element = document.getElementById('loree-my-templates-collapse') as HTMLElement;
    element.innerHTML = `${Loader}`;
    await retrieveCustomTemplate();
    appendTemplateUI(templateData, 'loree-my-templates-collapse', accordionType);
    setCustomBlockEditOptions('My Template');
  }
  if (accordionType === 'Shared Template' && !isSharedTemplateCalled) {
    const element = document.getElementById('loree-shared-templates-collapse') as HTMLElement;
    element.innerHTML = `${Loader}`;
    await retrieveCustomSharedTemplate();
    appendTemplateUI(sharedTemplateData, 'loree-shared-templates-collapse', accordionType);
  }
};

const appendTemplateUI = (
  templateData: Array<{
    id: string;
    title: string;
    thumbnail: string;
  }>,
  templateWrapperId: string,
  templateType: string,
) => {
  const accordionBody = getEditorElementById(templateWrapperId) as HTMLElement;
  if (!accordionBody) {
    return;
  }
  accordionBody.innerHTML = '';
  if (templateData.length === 0) {
    accordionBody.classList.add('loree-custom-block-no-results');
    accordionBody.innerHTML = 'No results found';
  } else {
    for (const data of templateData) {
      const templateElement = document.createElement('div');
      templateElement.className = 'panel mb-3';
      templateElement.id = data.id;
      const rowThumbnail = document.createElement('div');
      rowThumbnail.className = 'custom-blocks-thumbnail-template';
      if (templateType === 'My Template') rowThumbnail.prepend(appendThumbnailMenu(templateType));
      const thumbnailImg = document.createElement('img');
      thumbnailImg.src = data.thumbnail;
      thumbnailImg.className = CONSTANTS.LOREE_SIDEBAR_CUSTOM_BLOCK_THUMBNAIL;
      rowThumbnail.append(thumbnailImg);
      const title = document.createElement('p');
      title.className = 'custom-block-title mb-0';
      title.innerHTML = data.title;
      title.title = data.title;
      rowThumbnail.onclick = (e): void => handleAddCustomTemplate(e, templateType);
      templateElement.append(rowThumbnail);
      templateElement.appendChild(title);
      accordionBody.append(templateElement);
    }
    updateTemplateThumbnail();
  }
};

export const getTemplateContent = async (
  id: string,
  base: Base,
  e: Event,
  data: {
    content: string;
    title: string | null;
    category: { id: string | null; name: string | null };
  },
  templateType: string,
) => {
  let globalStatus = false;
  if (templateType === 'Global Template') globalStatus = true;
  const iframe = document.getElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
  const iframeContent: HTMLElement | null | undefined = iframe?.contentDocument?.getElementById(
    CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
  );
  let editorContent: string = !isCanvasAndBB() ? data.content : await getS3urlFromKey(data.content);
  const cleanedHtml = base.convertContent(editorContent);
  let structuredHtml = '';
  if (!base.isBlank(cleanedHtml)) {
    structuredHtml = base.migrate.migrateElementsWithoutRowColumn(cleanedHtml);
  }
  editorContent = base.removeEmptyTags(structuredHtml);
  if (e.target !== undefined) {
    // append existing template to editor
    const selectedContent = addWrapperToContent(editorContent) as HTMLElement;
    iframeContent?.appendChild(selectedContent);
    base.changeSelectedElement(selectedContent);
    base.handleCustomBlockAppendButton('Template');
  } else {
    // edit exisiting custom template
    iframeContent?.insertAdjacentHTML('beforeend', editorContent);
    editContentDetails(
      data.title,
      id,
      data.category.id,
      data.category.name,
      'My Template',
      globalStatus,
    );
    base.resetRedoUndoHistory();
    base.setInitialHistory();
    changeHeaderTitle(data.title as string);
  }
};

export const handleAddCustomTemplate = (e: Event | null | undefined, type: string) => {
  const base = new Base();
  const target: HTMLElement | null = e?.target as HTMLElement | null;
  let templateCheckData = templateData;
  handleAutoSaveOnCustomBlockAppend(true);
  let id;
  if (!target) id = e;
  else id = (target as HTMLElement | null)?.parentElement?.parentElement?.id;
  if (type === 'Global Template') templateCheckData = globalTemplateData;
  else if (type === 'Shared Template') templateCheckData = sharedTemplateData;
  for (const data of templateCheckData) {
    if (data.id === id) {
      void getTemplateContent(id as string, base, e as Event, data, type);
    }
  }
};

const addWrapperToContent = (data: string) => {
  const wrapper = document.createElement('div');
  wrapper.id = CONSTANTS.LOREE_TEMPLATE_BLOCK_WRAPPER;
  wrapper.innerHTML = data.trim();
  return wrapper;
};

export const setSharedTemplateStatus = () => {
  isSharedTemplateCalled = false;
};
export const refreshTemplateList = async (templateToRefresh: 'myTemplates' | 'sharedTemplates') => {
  // Once completed the admin dashboard in d2l,
  // will remove this condition
  accordionType = 'Global Template';
  try {
    switch (sessionStorage.getItem('domainName')) {
      case 'BB':
        await Promise.all([retrieveCustomGlobalTemplate(), retrieveCustomTemplate()]);
        break;
      case 'D2l':
        accordionType = 'My Template';
        await Promise.all([retrieveCustomTemplate()]);
        break;
      default:
        if (templateToRefresh === 'myTemplates') {
          isMyTemplateCalled = false;
        } else if (templateToRefresh === 'sharedTemplates') {
          isSharedTemplateCalled = false;
        }
    }
    const thumbnailWrapper = document.getElementById(
      'loree-templates-link-accordion-wrapper',
    ) as HTMLElement;
    const customSidebarWrapper = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_WRAPPER,
    );
    if (thumbnailWrapper && customSidebarWrapper) {
      thumbnailWrapper.remove();
      customSidebarWrapper.appendChild(templatesBlock());
    }
    clearTemplateSearch();
  } catch (err: _Any) {
    apm.captureError(err);
    console.log('Error, while refreshing template list', err);
  }
};

const checkEvents = () => {
  const checkBox = document.getElementsByClassName(
    'loree-custom-template-checkbox',
  ) as HTMLCollection;
  for (const check of checkBox) {
    if ((check as HTMLInputElement).checked) {
      return false;
    }
  }
  return true;
};

export const handleTemplateCategoryApplyButton = () => {
  filterCategoryId = [];
  const categoryDropdownCategory: HTMLElement = document.getElementById(
    CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_LIST,
  ) as HTMLElement;
  if (!categoryDropdownCategory) return;
  const dropdownMenu = categoryDropdownCategory.children[1] as HTMLElement;
  const dropdownMenuItems = dropdownMenu.children[0] as HTMLElement;
  const selectedList = handleCategoryListChange(dropdownMenuItems);
  removeSelectedListDropdown();
  if (selectedList.length !== 0) {
    appendSelectedListDropdown(selectedList, categoryDropdownCategory);
  }
};

export const updateTemplateThumbnail = () => {
  let tempData = [];
  let global;
  if (accordionType === 'Global Template') {
    tempData = globalTemplateData;
    global = document.getElementById(CONSTANTS.LOREE_GLOBAL_TEMPLATES_COLLAPSE)
      ?.childNodes as NodeListOf<ChildNode>;
  } else if (accordionType === 'My Template') {
    tempData = templateData;
    global = document.getElementById(CONSTANTS.LOREE_MY_TEMPLATES_COLLAPSE)
      ?.childNodes as NodeListOf<ChildNode>;
  } else {
    tempData = sharedTemplateData;
    global = document.getElementById(CONSTANTS.LOREE_SHARED_TEMPLATES_COLLAPSE)
      ?.childNodes as NodeListOf<ChildNode>;
  }
  if (tempData.length > 0) {
    const noResults = document.getElementById(`loree-${accordionType}-no-results`);
    if (noResults) noResults.className = 'd-none';
    templateFilterData = [];
    const templateSearchData = [];
    const searchWrapperInput = document.getElementById(
      CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT_VALUE,
    ) as HTMLInputElement;
    if (filterCategoryId.length === 0) templateFilterData = [];
    else if (searchWrapperInput.value !== '') {
      for (const data of tempData) {
        if (data.title.toUpperCase().includes(searchWrapperInput.value.toUpperCase()))
          templateSearchData.push(data);
      }
      if (filterCategoryId.length !== 0) {
        for (const data of templateSearchData) {
          for (const category of filterCategoryId) {
            if (data.categoryID === category) templateFilterData.push(data.id);
          }
        }
      } else {
        for (const data of templateSearchData) {
          templateFilterData.push(data.id);
        }
      }
    } else if (searchWrapperInput.value === '' && filterCategoryId.length !== 0) {
      for (const data of tempData) {
        for (const category of filterCategoryId) {
          if (data.categoryID === category) templateFilterData.push(data.id);
        }
      }
    } else templateFilterData = tempData;
    for (let i = 0; i < global.length; i++) {
      if ((global[i] as HTMLElement) && (global[i] as HTMLElement).style) {
        if (templateFilterData.includes((global[i] as HTMLElement).id)) {
          (global[i] as HTMLElement).style.display = '';
        } else {
          (global[i] as HTMLElement).style.display = 'none';
        }
      }
    }
    if (templateFilterData.length === 0) {
      if (document.getElementById(`loree-${accordionType}-no-results`) === null) {
        const noResultsTag = document.createElement('span');
        noResultsTag.id = `loree-${accordionType}-no-results`;
        noResultsTag.className = 'd-block';
        noResultsTag.style.fontSize = '14px';
        noResultsTag.style.paddingLeft = '13px';
        noResultsTag.innerHTML = 'No results found';
        global[0]?.parentElement?.appendChild(noResultsTag);
      } else {
        if (noResults) noResults.className = 'd-block';
      }
    }
  }
};

const removeSelectedListDropdown = () => {
  const selectionArea = document.getElementById('template-category-selected-list-wrapper');
  const moreCategoryLink = document.getElementById('category-selection-more');
  if (selectionArea !== null) {
    selectionArea.remove();
    moreCategoryLink?.remove();
  }
};

const handleCategoryListChange = (dropdownMenuItems: HTMLElement) => {
  const checkBox = document.getElementsByClassName(
    'loree-custom-template-checkbox',
  ) as HTMLCollection;
  if (checkEvents()) {
    for (const check of checkBox) {
      (check as HTMLInputElement).checked = false;
    }
  }
  const selectedList: (string | null)[] = [];
  Array.from(dropdownMenuItems.children).forEach((checklist) => {
    const checkListChild = checklist.childNodes;
    if (checklist.id !== 'loree-sidebar-custom-block-category-button') {
      for (const label of checkListChild) {
        if ((label.childNodes[0] as HTMLInputElement).checked) {
          if (!checkedCheckBoxes.includes(label)) checkedCheckBoxes.push(label);
          if ((label as HTMLElement).id === CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY) {
            for (const check of checkBox) {
              (check as HTMLInputElement).checked = true;
            }
          } else {
            if (!filterCategoryId.includes((label as HTMLElement).id))
              filterCategoryId.push((label as HTMLElement).id);
            if (!selectedList.includes(label.childNodes[1].textContent))
              selectedList.push(label.childNodes[1].textContent);
          }
          break;
        } else {
          if (filterCategoryId.includes((label as HTMLElement).id))
            filterCategoryId.splice(filterCategoryId.indexOf((label as HTMLElement).id), 1);
          if (selectedList.includes(label.childNodes[1].textContent))
            selectedList.splice(selectedList.indexOf(label.childNodes[1].textContent), 1);
          if (checkedCheckBoxes.includes(label))
            checkedCheckBoxes.splice(checkedCheckBoxes.indexOf(label), 1);
          const hiddenCategoryList = document.getElementsByClassName(
            'category-selected-list-item',
          )[2];
          if (hiddenCategoryList) hiddenCategoryList.classList.remove('d-none');
        }
      }
    }
  });
  updateTemplateThumbnail();
  return selectedList;
};

export const preventDropdownClosing = (e: MouseEvent) => {
  if ((e.target as HTMLElement).tagName !== 'BUTTON') {
    e.stopPropagation();
    const allCategoryCheckBox = document.getElementById(
      CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY,
    )?.childNodes[0] as HTMLInputElement;
    const checkBox = document.getElementsByClassName(
      'loree-custom-template-checkbox',
    ) as HTMLCollection;
    const targetElement =
      (e.target as HTMLElement).tagName === 'INPUT'
        ? (e.target as HTMLElement).parentElement
        : (e.target as HTMLElement);
    if (targetElement?.id === CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY) {
      const categoryCheckBox = e.target as HTMLInputElement;
      if (categoryCheckBox.checked) {
        for (const check of checkBox) {
          (check as HTMLInputElement).checked = true;
        }
      } else {
        for (const check of checkBox) {
          (check as HTMLInputElement).checked = false;
        }
      }
    } else {
      let isAllChecked;
      for (const check of checkBox) {
        if (!(check as HTMLInputElement).checked) {
          isAllChecked = false;
          break;
        }
        isAllChecked = true;
      }
      allCategoryCheckBox.checked = !!isAllChecked;
    }
  }
};

const appendSelectedListDropdown = (
  selectedList: (string | null)[],
  categoryDropdownCategory: HTMLElement,
) => {
  const selectionArea = categoryDropdownCategory.childNodes[1] as HTMLElement;
  const attachListWrapper = document.createElement('div') as HTMLElement;
  attachListWrapper.className = 'category-selected-list';
  attachListWrapper.id = 'template-category-selected-list-wrapper';
  let innerHTML = '';
  let count = 0;
  selectedList.forEach((element: string | null) => {
    if (count < 3) {
      innerHTML += `<div class="m-2 category-selected-list-item"><span>${element}</span><span class='category-selected-list-item-remove-icon'>${elementAddRemoveIcons.closeElementIcon}</span></div>`;
      count++;
    } else {
      innerHTML += `<div class="m-2 category-selected-list-item d-none"><span>${element}</span><span class='category-selected-list-item-remove-icon'>${elementAddRemoveIcons.closeElementIcon}</span></div>`;
    }
  });
  attachListWrapper.innerHTML = innerHTML;
  selectionArea.appendChild(attachListWrapper);
  if (selectedList.length > 3) {
    const moreCategoriesLink = document.createElement('a');
    moreCategoriesLink.id = 'category-selection-more';
    moreCategoriesLink.innerHTML = String(selectedList.length - 3) + ' More';
    moreCategoriesLink.onclick = (e) => showAllSelectedCategory(e);
    selectionArea.appendChild(moreCategoriesLink);
  }
  void removeUniqueSelectedList();
};

const removeUniqueSelectedList = () => {
  const removeIcon = document.getElementsByClassName('category-selected-list-item-remove-icon');
  const allCategoryCheckBox = document.getElementById(
    CONSTANTS.LOREE_CUSTOM_BLOCK_LABEL_ALL_CATEGORY,
  )?.childNodes[0] as HTMLInputElement;
  Array.from(removeIcon).forEach((element) => {
    (element as HTMLElement).onclick = (e) => {
      e.stopPropagation();
      element.parentElement?.remove();
      const categoryDropdownCategory: HTMLElement = document.getElementById(
        CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_CATEGORY_LIST,
      ) as HTMLElement;
      const dropdownMenu = categoryDropdownCategory.children[1] as HTMLElement;
      const dropdownMenuItems = dropdownMenu.children[0] as HTMLElement;
      Array.from(dropdownMenuItems.children).forEach((checklist) => {
        if (checklist.id !== 'loree-sidebar-custom-block-category-button') {
          checklist.childNodes.forEach((label: ChildNode) => {
            if (
              label.childNodes[1].textContent ===
              (element.parentElement?.childNodes[0] as HTMLElement).innerHTML
            ) {
              (label.childNodes[0] as HTMLInputElement).checked = false;
              allCategoryCheckBox.checked = false;
            }
          });
        }
      });
      const selectedList = handleCategoryListChange(dropdownMenuItems);
      const moreLink = document.getElementById('category-selection-more') as HTMLElement;
      if (moreLink) {
        if (selectedList.length > 3 && moreLink.innerHTML !== 'Show less')
          moreLink.innerHTML = String(selectedList.length - 3) + ' More';
        else if (selectedList.length <= 3) moreLink.remove();
      }
      const selectionArea = document.getElementById('template-category-selected-list-wrapper');
      if (selectionArea?.innerHTML === '') selectionArea.remove();
    };
  });
};

const showAllSelectedCategory = (e: MouseEvent) => {
  e.stopPropagation();
  const hiddenCategoryList = document.getElementsByClassName(
    'category-selected-list-item',
  ) as HTMLCollection;
  const moreCategoryLink = document.getElementById('category-selection-more');
  if (moreCategoryLink) {
    if (moreCategoryLink.innerHTML !== 'Show less')
      for (const category of hiddenCategoryList) category.classList.remove('d-none');
    else handleTemplateCategoryApplyButton();
    moreCategoryLink.innerHTML = 'Show less';
  }
};

export const addClass = (element: HTMLElement, toBeAdded: string) => {
  element.classList.add(toBeAdded);
};

export const removeClass = (element: HTMLElement, toBeRemoved: string) => {
  element.classList.remove(toBeRemoved);
};
