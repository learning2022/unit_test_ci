import CONSTANTS from '../../constant';
import { elementAddRemoveIcons } from '../../iconHolder';

export const customBlockMockData = {
  iframeInnerHtml: `<div id=${CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER}></div><div id=${CONSTANTS.LOREE_HEADER}></div><div id='save-icon-position'><div class="save-icon px-4 py-1 d-flex"><div class="editor-tooltip mr-3" data-toggle="tooltip" title="" data-original-title="Save to LMS"></div><div class="editor-tooltip mr-3" data-toggle="tooltip" title="" data-original-title="Save as Template"></div><div class="editor-tooltip mr-3 d-none" data-toggle="tooltip" title="" data-original-title="Update Template"></div><div class="editor-tooltip mr-3 d-none" data-toggle="tooltip" title="" data-original-title="Save as New Template"></div><div class="editor-tooltip" data-toggle="tooltip" title="" data-original-title="">Clear Editor</div></div></div><div id=${CONSTANTS.LOREE_SIDEBAR}> <div id=${CONSTANTS.LOREE_ADD_ROW_BUTTON_WRAPPER}></div><button id=${CONSTANTS.LOREE_CLOSE_ADD_ELEMENT_BUTTON} class="closeElementIcon" style="display: none">${elementAddRemoveIcons.closeElementIcon}</button><div class="loree-block-options" id=${CONSTANTS.LOREE_BLOCK_OPTIONS_WRAPPER} style="display: none"></div><div class="loree-block-edit-options" id=${CONSTANTS.LOREE_BLOCK_EDIT_OPTIONS_WRAPPER} style="display: none"></div><div class="loree-text-options" id=${CONSTANTS.LOREE_TEXT_OPTIONS_WRAPPER} style="visibility: hidden"></div><button id=${CONSTANTS.LOREE_CLOSE_INSERT_ROW_BUTTON} class="closeElementIcon" style="display: none">${elementAddRemoveIcons.closeElementIcon}</button> <button id=${CONSTANTS.LOREE_INSERT_ROW_BUTTON} class="addElementIcon" style="display: none">${elementAddRemoveIcons.addElementIcon}</button>`,
  sidebarThumbnailWrapper: `<div class="dropdown dropdown-layout" id="loree-sidebar-custom-block-menu-wrapper"><div class="dropdown-menu custom-block-thumnail-dropdown-menu" id="loree-sidebar-custom-block-thumbnail-dropdown-menu" aria-labeledby="loree-sidebar-custom-row-category-list" style=""><div class="custom-block-thumbnail-menu-option">Edit</div><div class="custom-block-thumbnail-menu-option">Delete</div><div class="custom-block-thumbnail-menu-option">Share</div></div></div>`,
};

export const templateMockData = {
  id: '1',
  title: 'test',
  content: '<p>hello</p>',
};

export const blocksMockData = [
  { id: '1', title: 'test', content: '<p>hello</p>', type: 'Row' },
  { id: '2', title: 'test-2', content: '<p>hello test 2</p>', type: 'Elements' },
];
export const userMockData = {
  email: 'canvas_123670000000000245@example.com',
  name: 'Krishnaveni',
  sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
};
export const courseMockDataValue =
  '{"statusCode":200,"body":{"id":262,"name":"ABPL90117_2018_SM2","account_id":18,"uuid":"B2rqrjKbqGVMSWLSWnsUZjizbZlxMjfSx13w0UGl","start_at":"2019-08-10T06:26:00Z","grading_standard_id":null,"is_public":false,"created_at":"2019-05-30T07:15:30Z","course_code":"ABPL90117_2018_SM2","default_view":"modules","root_account_id":1,"enrollment_term_id":1,"license":"private","grade_passback_setting":null,"end_at":null,"public_syllabus":false,"public_syllabus_to_auth":false,"storage_quota_mb":2000,"is_public_to_auth_users":false,"homeroom_course":false,"course_color":null,"hide_final_grades":false,"apply_assignment_group_weights":false,"account":{"id":18,"name":"School of Engineering","workflow_state":"active","parent_account_id":1,"root_account_id":1,"uuid":"zo5onpArzK2DDrNqVsIyqQqJbGb3Wjh6TezypSKz","default_storage_quota_mb":2000,"default_user_storage_quota_mb":50,"default_group_storage_quota_mb":2000,"default_time_zone":"Asia/Kolkata","sis_account_id":null,"sis_import_id":null,"integration_id":null,"course_template_id":null},"calendar":{"ics":"https://crystaldelta.instructure.com/feeds/calendars/course_B2rqrjKbqGVMSWLSWnsUZjizbZlxMjfSx13w0UGl.ics"},"time_zone":"Australia/Melbourne","blueprint":false,"template":false,"sis_course_id":"CD_002","sis_import_id":null,"integration_id":null,"enrollments":[{"type":"designer","role":"DesignerEnrollment","role_id":6,"user_id":245,"enrollment_state":"active","limit_privileges_to_course_section":false}],"workflow_state":"available","restrict_enrollments_to_course_dates":false,"overridden_course_visibility":""}}';

export const accountByCourseMockDataValue =
  '{"statusCode":200,"body":{"id":18,"name":"School of Engineering","workflow_state":"active","parent_account_id":1,"root_account_id":1,"uuid":"zo5onpArzK2DDrNqVsIyqQqJbGb3Wjh6TezypSKz","default_storage_quota_mb":2000,"default_user_storage_quota_mb":50,"default_group_storage_quota_mb":2000,"default_time_zone":"Asia/Kolkata","sis_account_id":null,"sis_import_id":null,"integration_id":null,"course_template_id":null}}';

export const items = [
  {
    id: 26164,
    name: 'tcp',
    position: 1,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 1,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/878/modules/26164/items',
  },
  {
    id: 26542,
    name: 'hanuman',
    position: 2,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 2,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/878/modules/26542/items',
  },
  {
    id: 26543,
    name: 'andi',
    position: 3,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 2,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/878/modules/26543/items',
  },
  {
    id: 26544,
    name: 'arun',
    position: 4,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 2,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/878/modules/26544/items',
  },
];

export const moduleData = [
  {
    id: 26645,
    name: 'Module - 1',
    position: 1,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 2,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/178/modules/26645/items',
  },
];

export const sampleItems = [
  {
    id: 26164,
    name: 'tcp',
    position: 1,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 1,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/878/modules/26164/items',
  },
  {
    id: 26542,
    name: 'hanuman',
    position: 2,
    unlock_at: null,
    require_sequential_progress: false,
    publish_final_grade: false,
    prerequisite_module_ids: [],
    published: false,
    items_count: 2,
    items_url: 'https://crystaldelta.instructure.com/api/v1/courses/878/modules/26542/items',
  },
];

export const listTemplatesResponseOne = {
  data: {
    templateByOwner: {
      items: [templateMockData],
      nextToken: 'dfheriuwfhjsdkshf',
    },
  },
};
export const listTemplatesResponse = {
  data: {
    templateByOwner: {
      items: [
        {
          id: '1',
          title: 'test',
          content: '<p>hello</p>',
          active: 'true',
        },
        {
          id: '2',
          title: 'test',
          content: '<p>This is a dummy message</p>',
          active: 'true',
        },
        {
          id: '3',
          title: 'test',
          content: '<p>Lorem Ipsum dolor sit amet</p>',
          active: 'true',
        },
      ],
      nextToken: null,
    },
  },
};
export const globalTemplatesResponseOne = {
  data: {
    listGlobalTemplatess: {
      items: [templateMockData],
      nextToken: 'eeeetgdfgsfgdsgre',
    },
  },
};

export const globalTemplatesResponse = {
  data: {
    listGlobalTemplatess: {
      items: [templateMockData],
      nextToken: null,
    },
  },
};
export const courseMockData = {
  data: {
    courseDetails: courseMockDataValue,
  },
};
export const accountMockData = {
  data: {
    accountByCourse: accountByCourseMockDataValue,
  },
};
export const sharedTemplatesResponseOne = {
  data: {
    listSharedTemplatess: {
      items: [templateMockData],
      nextToken: 'jdhfjkhdsjfhdskjhfjshf',
    },
  },
};
export const sharedTemplatesResponse = {
  data: {
    listSharedTemplatess: {
      items: [templateMockData],
      nextToken: null,
    },
  },
};

export const globalBlocksResponse = {
  data: {
    listGlobalBlockss: {
      items: [],
      nextToken: null,
    },
  },
};

export const categoryResponse = {
  data: {
    categoryByPlatform: {
      items: [
        {
          id: '7a7e5f44-172a-4f19-ac0f-6bab5f5d6197',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          name: 'byplatform-canvas9',
          projects: {
            nextToken: null,
          },
          createdAt: '2021-05-12T05:58:00.307Z',
          updatedAt: '2021-05-12T05:58:00.307Z',
        },
      ],
      nextToken: null,
    },
  },
};

export const blocksResponse = {
  data: {
    blockByOwner: {
      items: [
        {
          id: 'e30f9b06-5426-494b-910d-c4017a879555',
          title: 'divider element',
          categoryID: '458e9135-2c73-4eb9-82c8-fbf0d4e19b18',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          category: {
            id: '458e9135-2c73-4eb9-82c8-fbf0d4e19b18',
            ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
            name: 'category test',
            createdAt: '2021-02-03T11:25:56.232Z',
            updatedAt: '2021-02-03T11:25:56.232Z',
          },
          type: 'Elements',
          content: '',
          thumbnail: {
            bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox',
            key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/blocks/new element',
            region: 'ap-southeast-2',
          },
          status: true,
          isGlobal: false,
          isShared: null,
          createdBy: 'Priyanga',
          active: true,
          owner: '54fd7e8b-e316-4f0c-9d41-b50bbd38d959',
          createdAt: '2021-10-20T07:45:07.714Z',
          updatedAt: '2021-10-20T07:45:07.714Z',
        },
      ],
      nextToken: null,
    },
  },
};

export const interativeResponse = {
  data: {
    interativeLibrary:
      '{"statusCode":200,"body":[{"id":1,"title":"Accordion","description":"Reduce the amount of text presented to readers by using this responsive accordion. Readers decide which headlines to take a closer look at by expanding the title. Excellent for providing an overview with optional in-depth explanations."},{"id":2,"title":"Tabs","description":"A panel that represents a tabbed set of pages, each of which contains another widget. Its child widgets are shown as the user selects the various tabs associated with them."},{"id":3,"title":"Button","description":"Buttons provide a clickable element, which can be used in forms, or anywhere that needs simple, standard button functionality. They may display text, icons, or both. Buttons can be styled with several attributes to look a specific way."},{"id":4,"title":"multiplechoice","description":"Buttons provide a clickable element, which can be used in forms, or anywhere that needs simple, standard button functionality. They may display text, icons, or both. Buttons can be styled with several attributes to look a specific way."},{"id":5,"title":"flipcard","description":"Flipping card is a card animation that gives an element the effect of flipping to the other side upon any interaction."},{"id":6,"title":"imageslider","description":"Image Slider"},{"id":7,"title":"ClickAndRevealInteractions","description":"Click and Reveal Interactions"},{"id":8,"title":"DragandDrop","description":"DragandDrop"},{"id":9,"title":"Hotspot","description":"Hotspot"}]}',
  },
};

export const sharedBlockData = [
  {
    active: true,
    category: {
      id: '140e3c09-2372-4155-bc14-ad7e171b606c',
      ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      name: 's3 image',
      createdAt: '2021-07-28T07:55:56.610Z',
      updatedAt: '2021-07-28T07:55:56.610Z',
    },
    categoryID: '140e3c09-2372-4155-bc14-ad7e171b606c',
    content:
      '<div class="row loree-iframe-content-row element-highlight row-highlight" style="position:relative; margin: 0px; padding: 10px"><div class="col-12 loree-iframe-content-column" style="padding: 10px"><p class="loree-iframe-content-element" style="padding: 5px;" contenteditable="true">this is the test page 1</p></div></div>',
    createdAt: '2021-12-06T07:54:53.855Z',
    createdBy: 'Krishnaveni',
    customBlockID: '80785345-2501-4cfe-a081-fb4e0454e1d3',
    id: 'd306be84-f371-491c-8b8f-6e6e5eac4997',
    ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
    sharedAccountId: '53',
    status: null,
    thumbnail: {
      bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev',
      key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/blocks/kjk',
      region: 'ap-southeast-2',
    },
    title: 'kjk',
    type: 'Row',
    updatedAt: '2021-12-06T07:54:53.855Z',
  },
  {
    active: true,
    category: {
      id: '140e3c09-2372-4155-bc14-ad7e171b606c',
      ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      name: 's3 image',
      createdAt: '2021-07-28T07:55:56.610Z',
      updatedAt: '2021-07-28T07:55:56.610Z',
    },
    categoryID: '140e3c09-2372-4155-bc14-ad7e171b606c',
    content:
      '<div class="row loree-iframe-content-row element-highlight row-highlight" style="position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px"><p class="loree-iframe-content-element" style="padding: 5px;" contenteditable="true">this is the test page 1</p></div></div>',
    createdAt: '2021-12-06T07:54:55.957Z',
    createdBy: 'Krishnaveni',
    customBlockID: '80785345-2501-4cfe-a081-fb4e0454e1d3',
    id: 'ea51e0a3-49ac-441d-abd4-93735d374811',
    ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
    sharedAccountId: '52',
    status: null,
    thumbnail: {
      bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev',
      key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/blocks/kjk',
      region: 'ap-southeast-2',
    },
    title: 'kjk',
    type: 'Row',
    updatedAt: '2021-12-06T07:54:55.957Z',
  },
];

export const saveCustomBlockMockData = {
  data: {
    createCustomBlock: {
      id: '0a71e041-da70-48f7-9443-af906ccc1dce',
      title: 'OJ',
      categoryID: '194665f0-68c6-44f0-b246-deae98368597',
      ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      category: {
        id: '194665f0-68c6-44f0-b246-deae98368597',
        ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
        name: 'new catogery',
        projects: {
          nextToken: null,
        },
        createdAt: '2022-04-06T08:45:59.956Z',
        updatedAt: '2022-04-06T08:45:59.956Z',
      },
      type: 'Elements',
      content:
        '<div class="" style=""><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="d66b1309-ac2b-48e5-b96f-c0098e8baf0a/fbcad991-ddfe-4e0c-957d-7c0217a93af3/1649241240346-partial-1.png" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/510/files/385629" data-api-returntype="File"></div><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="d66b1309-ac2b-48e5-b96f-c0098e8baf0a/fbcad991-ddfe-4e0c-957d-7c0217a93af3/1649241240426-partial-1.png" data-api-endpoint="https://crystaldelta.instructure.com/api/v1/courses/510/files/385629" data-api-returntype="File"></div></div>',
      thumbnail: {
        bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e105225-local',
        key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/blocks/OJ',
        region: 'ap-southeast-2',
      },
      status: true,
      isGlobal: false,
      isShared: null,
      createdBy: 'Keerthanashri Raj',
      active: true,
      owner: 'fbcad991-ddfe-4e0c-957d-7c0217a93af3',
      createdAt: '2022-04-06T10:34:08.310Z',
      updatedAt: '2022-04-06T10:34:08.310Z',
    },
  },
};

export const s3ImageConvertedMockData = {
  data: {
    uploadImagesToS3:
      '"<div class=\\"\\" style=\\"\\"><div style=\\"width:100%;margin:0 0 10px;padding:5px;display: flex;\\" class=\\"loree-iframe-content-image-wrapper\\"><img alt=\\"\\" style=\\"width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000\\" class=\\"loree-iframe-content-image\\" src=\\"d66b1309-ac2b-48e5-b96f-c0098e8baf0a/fbcad991-ddfe-4e0c-957d-7c0217a93af3/1649241240346-partial-1.png\\" data-api-endpoint=\\"https://crystaldelta.instructure.c…:5px;display: flex;\\" class=\\"loree-iframe-content-image-wrapper\\"><img alt=\\"\\" style=\\"width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000\\" class=\\"loree-iframe-content-image\\" src=\\"d66b1309-ac2b-48e5-b96f-c0098e8baf0a/fbcad991-ddfe-4e0c-957d-7c0217a93af3/1649241240426-partial-1.png\\" data-api-endpoint=\\"https://crystaldelta.instructure.com/api/v1/courses/510/files/385629\\" data-api-returntype=\\"File\\"></div></div>"',
  },
};

export const successAlertMockDataForRow = `<div class="d-inline-block mr-1"><svg xmlns="http://www.w3.org/2000/svg" width="21.002" height="21.002" viewBox="0 0 21.002 21.002">
<path id="alert" d="M17481,15919a10,10,0,1,1,10,10A10.012,10.012,0,0,1,17481,15919Zm1.484,0a8.519,8.519,0,1,0,8.516-8.521A8.526,8.526,0,0,0,17482.482,15919Zm8.367,5.459a.8.8,0,0,1-.137-.039.975.975,0,0,1-.127-.07.772.772,0,0,1-.113-.093.818.818,0,0,1-.094-.109c-.025-.044-.047-.084-.07-.132s-.029-.093-.043-.137a.771.771,0,0,1,0-.289,1.319,1.319,0,0,1,.043-.137,1.378,1.378,0,0,1,.07-.132.6.6,0,0,1,.094-.109,1.184,1.184,0,0,1,.113-.1.862.862,0,0,1,.127-.066.823.823,0,0,1,.137-.044.761.761,0,0,1,.672.207.409.409,0,0,1,.088.109.476.476,0,0,1,.07.132.637.637,0,0,1,.043.137.771.771,0,0,1,0,.289.736.736,0,0,1-.043.137.538.538,0,0,1-.07.132.5.5,0,0,1-.088.109.61.61,0,0,1-.113.093,1.357,1.357,0,0,1-.129.07.709.709,0,0,1-.141.039.79.79,0,0,1-.145.018A.812.812,0,0,1,17490.85,15924.46Zm-.584-3.637v-6.558a.74.74,0,1,1,1.48,0v6.558a.74.74,0,1,1-1.48,0Z" transform="translate(-17480.498 -15908.5)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"></path>
</svg></div><p class="mb-0 d-inline-block">Custom Row <span class="custom-saved">saved</span> alert.successfully</p>`;

export const successAlertMockDataForElement = `<div class="d-inline-block mr-1"><svg xmlns="http://www.w3.org/2000/svg" width="21.002" height="21.002" viewBox="0 0 21.002 21.002">
<path id="alert" d="M17481,15919a10,10,0,1,1,10,10A10.012,10.012,0,0,1,17481,15919Zm1.484,0a8.519,8.519,0,1,0,8.516-8.521A8.526,8.526,0,0,0,17482.482,15919Zm8.367,5.459a.8.8,0,0,1-.137-.039.975.975,0,0,1-.127-.07.772.772,0,0,1-.113-.093.818.818,0,0,1-.094-.109c-.025-.044-.047-.084-.07-.132s-.029-.093-.043-.137a.771.771,0,0,1,0-.289,1.319,1.319,0,0,1,.043-.137,1.378,1.378,0,0,1,.07-.132.6.6,0,0,1,.094-.109,1.184,1.184,0,0,1,.113-.1.862.862,0,0,1,.127-.066.823.823,0,0,1,.137-.044.761.761,0,0,1,.672.207.409.409,0,0,1,.088.109.476.476,0,0,1,.07.132.637.637,0,0,1,.043.137.771.771,0,0,1,0,.289.736.736,0,0,1-.043.137.538.538,0,0,1-.07.132.5.5,0,0,1-.088.109.61.61,0,0,1-.113.093,1.357,1.357,0,0,1-.129.07.709.709,0,0,1-.141.039.79.79,0,0,1-.145.018A.812.812,0,0,1,17490.85,15924.46Zm-.584-3.637v-6.558a.74.74,0,1,1,1.48,0v6.558a.74.74,0,1,1-1.48,0Z" transform="translate(-17480.498 -15908.5)" fill="#fff" stroke="rgba(0,0,0,0)" stroke-miterlimit="10" stroke-width="1"></path>
</svg></div><p class="mb-0 d-inline-block">Custom Element <span class="custom-saved">saved</span> alert.successfully</p>`;

export const customRow = [
  {
    blockType: 'Row',
    refreshType: 'refreshMyBlocks',
    myBlockRefresh: false,
    sharedBlockRefresh: false,
  },
  {
    blockType: 'Row',
    refreshType: 'refreshSharedBlocks',
    myBlockRefresh: false,
    sharedBlockRefresh: false,
  },
];

export const customELement = [
  {
    blockType: 'Elements',
    refreshType: 'refreshMyBlocks',
    myBlockRefresh: false,
    sharedBlockRefresh: false,
  },
  {
    blockType: 'Elements',
    refreshType: 'refreshSharedBlocks',
    myBlockRefresh: false,
    sharedBlockRefresh: false,
  },
];
export const rowMockData =
  '<div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p><h1 class="loree-iframe-content-element" style=" padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 35px;">Insert Header Here</h1><div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div>';
export const migatedRowMockData =
  '<div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div><div class="row loree-iframe-content-row" style="position:relative; margin: 0px; padding: 10px"><div class="col-12 loree-iframe-content-column" style="padding: 10px"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p><h1 class="loree-iframe-content-element" style=" padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 35px;">Insert Header Here</h1></div></div><div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div>';
export const elementMockData =
  '<div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p><h1 class="loree-iframe-content-element" style=" padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 35px;">Insert Header Here</h1><div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div>';
export const migatedElementMockData =
  '<div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p><h1 class="loree-iframe-content-element" style=" padding: 10px; text-align: left; font-family: Helvetica; font: none; margin: 10px 0; color: #000000; font-size: 35px;">Insert Header Here</h1><div class="row loree-iframe-content-row" style=" display: block; width: 100%; max-width: 100%; height: %; padding: 10px;"><div class="col-12 loree-iframe-content-column" style=" display: block; min-height: 50px; padding: 10px 0; margin: 0; width: 100%; height: %; font-family: Helvetica; font: none;"><p class="loree-iframe-content-element" style="padding: 5px; text-align: left; font-family: Helvetica; margin: 10px 0px; color: rgb(0, 0, 0); font-size: 16px;">Insert Text Here</p></div></div>';

export const globalblocksResponseOne = {
  data: {
    listGlobalBlockss: {
      items: [templateMockData],
      nextToken: 'hdsfkjshfjdskjfhds',
    },
  },
};
export const globalblocksResponse = {
  data: {
    listGlobalBlockss: {
      items: [templateMockData],
      nextToken: null,
    },
  },
};
