import { elementMigration, rowMigration } from './customBlockMigration';
import {
  elementMockData,
  migatedElementMockData,
  migatedRowMockData,
  rowMockData,
} from './customBlockMockData';

describe('#handleAddCustomRow in canvas', () => {
  sessionStorage.setItem('domainName', 'canvas');
  test('Row migration', async () => {
    const data = {
      content: rowMockData,
    };
    const expectedMigratedRowMockData = migatedRowMockData;
    const migratedContent = await rowMigration(data);
    expect(migratedContent).toBe(expectedMigratedRowMockData);
  });
  test('Row migration for empty page', async () => {
    const data = {
      content: '',
    };
    const migratedContent = await rowMigration(data);
    expect(migratedContent).toBe('');
  });

  test('element migration', async () => {
    const data = {
      content: elementMockData,
    };
    const expectedMigratedElementMockData = migatedElementMockData;
    const migratedContent = await elementMigration(data);
    expect(migratedContent).toBe(expectedMigratedElementMockData);
  });
});
describe('#handleAddCustomRow in other than canvas and bb', () => {
  sessionStorage.setItem('domainName', 'd2l');
  test('Row migration', async () => {
    const data = {
      content: rowMockData,
    };
    const expectedMigratedRowMockData = migatedRowMockData;
    const migratedContent = await rowMigration(data);
    expect(migratedContent).toBe(expectedMigratedRowMockData);
  });
  test('element migration', async () => {
    const data = {
      content: elementMockData,
    };
    const expectedMigratedElementMockData = migatedElementMockData;
    const migratedContent = await elementMigration(data);
    expect(migratedContent).toBe(expectedMigratedElementMockData);
  });
  test('Row migration for empty page', async () => {
    const data = {
      content: '',
    };
    const migratedContent = await rowMigration(data);
    expect(migratedContent).toBe('');
  });
});
