import * as customTemplateEvents from './customTemplateEvents';
import { StorageMock } from '../../../utils/storageMock';
import Base from '../../base';
import CONSTANTS from '../../constant';
import { createElement, getElementById } from '../../common/dom';
import { data, templatesOutput } from './customTemplateMockData';
import { searchWrapper } from './customBlocksUI';
import { getEditorElementById } from '../../utils';

describe('# custom template events', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
  });
  describe('#templates', () => {
    let templateBlocks: any;
    test('templates block UI', () => {
      templateBlocks = customTemplateEvents.templatesBlock();
      expect(templateBlocks).toHaveClass('accordion pb-3');
      expect(templateBlocks).toBeInstanceOf(HTMLDivElement);
    });
    describe('#bb lms', () => {
      test('templates block UI in BB render global templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        templateBlocks = customTemplateEvents.templatesBlock();
        expect(templateBlocks.firstChild).toContainHTML('Global Templates');
      });
      test('templates block UI in BB not render shared templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        templateBlocks = customTemplateEvents.templatesBlock();
        expect(templateBlocks.firstChild).not.toContainHTML('Shared Templates');
      });
    });
    describe('#canvas lms', () => {
      test('templates block UI in canvas', () => {
        sessionStorage.setItem('domainName', 'canvas');
        templateBlocks = customTemplateEvents.templatesBlock();
        expect(templateBlocks.firstChild).toContainHTML('Global Templates');
      });
    });
    describe('#d2l lms', () => {
      test('templates block UI in D2l render my templates', () => {
        sessionStorage.setItem('domainName', 'D2l');
        templateBlocks = customTemplateEvents.templatesBlock();
        expect(templateBlocks.firstChild).toContainHTML('My Templates');
      });
      test('templates block UI in D2l is not render global templates', () => {
        sessionStorage.setItem('domainName', 'D2l');
        templateBlocks = customTemplateEvents.templatesBlock();
        expect(templateBlocks.firstChild).not.toContainHTML('Global Templates');
      });
    });
  });
  describe('Refreshing templates in canvas LMS', () => {
    beforeAll(() => {
      document.body.append(searchWrapper());
    });
    const myTemplates: 'myTemplates' | 'sharedTemplates' = 'myTemplates';
    const sharedTemplates: 'myTemplates' | 'sharedTemplates' = 'sharedTemplates';
    test.each([myTemplates, sharedTemplates])(
      'refreshing my templates / shared templates in Canvas LMS',
      (templatetype) => {
        const searchInputWrapper = getEditorElementById(
          CONSTANTS.LOREE_SIDEBAR_CUSTOM_GLOBAL_TEMPLATE_SEARCH_INPUT_VALUE,
        ) as HTMLInputElement;
        searchInputWrapper.value = 'Test search';
        sessionStorage.setItem('domainName', 'canvas');
        customTemplateEvents.refreshTemplateList(templatetype);
        expect(customTemplateEvents.isMyTemplateCalled).toBe(false);
        expect(searchInputWrapper.value).toBe('');
      },
    );
    test('refreshing shared template in onDemand', () => {
      customTemplateEvents.setSharedTemplateStatus();
      expect(customTemplateEvents.isSharedTemplateCalled).toBe(false);
    });
  });
  describe('#getTemplateContent', () => {
    const base = new Base();
    const e = new Event('click');
    const iframe = document.createElement('iframe');
    function loreeWrapper() {
      document.body.innerHTML = `<div id='loree-wrapper'></div>`;
      iframe.id = CONSTANTS.LOREE_IFRAME;
      const wrapper = createElement('div');
      wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
      iframe.appendChild(wrapper);
      iframe.innerHTML += '';
      document.body.appendChild(iframe);
      const iframeDoc = iframe.contentWindow?.document;
      iframeDoc?.open().write(iframe.innerHTML);
      iframeDoc?.close();
    }
    test('migrating template content', async () => {
      loreeWrapper();
      sessionStorage.setItem('domainName', 'canvas');
      const migratingFunctionCall = jest.spyOn(base.migrate, 'migrateElementsWithoutRowColumn');
      await customTemplateEvents.getTemplateContent('', base, e, data, 'My Template');
      const iframe = getElementById(CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
      const iframeContent: HTMLElement | null | undefined = iframe?.contentDocument?.getElementById(
        CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
      );
      expect(migratingFunctionCall).toBeCalled();
      expect(iframeContent?.outerHTML).toBe(templatesOutput);
    });
  });
});
