import * as customBlockEvents from './customBlockEvents';
import { StorageMock } from '../../../utils/storageMock';
import { customELement, customRow } from './customBlockMockData';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('# custom blocks events', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as unknown as Storage;
  });
  describe('#elements', () => {
    let elementBlocks: HTMLElement;
    test('elements block UI', () => {
      elementBlocks = customBlockEvents.elementsBlock();
      expect(elementBlocks).toHaveClass('accordion');
      expect(elementBlocks).toBeInstanceOf(HTMLDivElement);
    });
    describe('#bb lms', () => {
      test('templates block UI in BB render global templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).toContainHTML('element.globalelements');
      });
      test('templates block UI in BB not render shared templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).not.toContainHTML('element.sharedelements');
      });
    });
    describe('# canvas lms', () => {
      test('elements block UI in canvas', () => {
        sessionStorage.setItem('domainName', 'canvas');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).toContainHTML('element.globalelements');
      });
    });
    describe('# d2l lms', () => {
      test('elements block UI in D2l render my rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).toContainHTML('element.myelements');
      });
      test('elements block UI in D2l not render global rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        elementBlocks = customBlockEvents.elementsBlock();
        expect(elementBlocks.firstChild).not.toContainHTML('element.globalelements');
      });
    });
  });
  describe('#rows', () => {
    let rowsBlocks: HTMLElement;
    test('rows block UI', () => {
      rowsBlocks = customBlockEvents.rowsBlock();
      expect(rowsBlocks).toBeInstanceOf(HTMLDivElement);
    });
    describe('# d2l lms', () => {
      test('rows block UI in D2l render my rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).toContainHTML('My Rows');
      });
      test('rows block UI in D2l not render global rows', () => {
        sessionStorage.setItem('domainName', 'D2l');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).not.toContainHTML('Global Rows');
      });
    });
    describe('#bb lms', () => {
      test('templates block UI in BB render global templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).toContainHTML('Global Rows');
      });
      test('templates block UI in BB not render shared templates', () => {
        sessionStorage.setItem('domainName', 'BB');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).not.toContainHTML('shared Rows');
      });
    });
    describe('#canvas lms', () => {
      test('rows block UI in canvas', () => {
        sessionStorage.setItem('domainName', 'canvas');
        rowsBlocks = customBlockEvents.rowsBlock();
        expect(rowsBlocks.firstChild).toContainHTML('Global Rows');
      });
    });
    describe('Refreshing customblocks in canvas LMS', () => {
      beforeAll(() => {
        sessionStorage.setItem('domainName', 'canvas');
      });
      test.each(customRow)(
        'refreshing myRow and sharedRow in Canvas LMS',
        async ({ blockType, refreshType, myBlockRefresh, sharedBlockRefresh }) => {
          await customBlockEvents.refreshCustomBlockThumbnail(blockType, refreshType);
          expect(customBlockEvents.isMyRowsCalled).toBe(myBlockRefresh);
          expect(customBlockEvents.isSharedRowsCalled).toBe(sharedBlockRefresh);
        },
      );
      test.each(customELement)(
        'refreshing myElement and sharedElement in Canvas LMS',
        async ({ blockType, refreshType, myBlockRefresh, sharedBlockRefresh }) => {
          await customBlockEvents.refreshCustomBlockThumbnail(blockType, refreshType);
          expect(customBlockEvents.isMyElementsCalled).toBe(myBlockRefresh);
          expect(customBlockEvents.isSharedElementsCalled).toBe(sharedBlockRefresh);
        },
      );
      test('refreshing shared row in onDemand', () => {
        customBlockEvents.setSharedBlockStatus('row');
        expect(customBlockEvents.isSharedRowsCalled).toBe(false);
      });
      test('refreshing shared element in onDemand', () => {
        customBlockEvents.setSharedBlockStatus('element');
        expect(customBlockEvents.isSharedElementsCalled).toBe(false);
      });
    });
  });
});
