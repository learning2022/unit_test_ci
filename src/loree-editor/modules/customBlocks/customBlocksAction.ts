import { API, graphqlOperation } from 'aws-amplify';
import {
  BlockByOwnerQuery,
  ListGlobalBlockssQuery,
  ListGlobalTemplatessQuery,
  ListSharedBlockssQuery,
  ListSharedTemplatessQuery,
  TemplateByOwnerQuery,
} from '../../../API';
import {
  blockByOwner,
  listGlobalBlockss,
  listGlobalTemplatess,
  listSharedBlockss,
  listSharedTemplatess,
  templateByOwner,
} from '../../../graphql/queries';
import {
  getBlockBySelectedTemplate,
  getGlobalTemplateQueryContext,
  getMyTemplateQueryContext,
  getSharedBlockBySelectedTemplate,
  getSharedTemplateBySelectedTemplate,
  getSharedTemplateQueryContext,
  getTemplateBySelectedTemplate,
} from './blocksQueryUtils';
import { handleAccountByCurrentCourse } from './sharedBlocks';

export const templateQuery = async (graphqlOperationData: {} | undefined) => {
  return await API.graphql<TemplateByOwnerQuery>(
    graphqlOperation(templateByOwner, graphqlOperationData),
  );
};

export const globalTemplateQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<ListGlobalTemplatessQuery>(
    graphqlOperation(listGlobalTemplatess, graphqlOperationData),
  );
};

export const sharedTemplateQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<ListSharedTemplatessQuery>(
    graphqlOperation(listSharedTemplatess, graphqlOperationData),
  );
};

export const blockQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<BlockByOwnerQuery>(graphqlOperation(blockByOwner, graphqlOperationData));
};

export const globalBlockQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<ListGlobalBlockssQuery>(
    graphqlOperation(listGlobalBlockss, graphqlOperationData),
  );
};

export const sharedBlockQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<ListSharedBlockssQuery>(
    graphqlOperation(listSharedBlockss, graphqlOperationData),
  );
};
export const listingCustomTemplates = async () => {
  const listTemplates = [];
  const graphqlOperationData: {
    ltiPlatformID: string | null;
    owner: { eq: string };
    nextToken?: string | null | undefined;
  } = await getMyTemplateQueryContext();
  const listTemplatesResponse = await templateQuery(graphqlOperationData);
  listTemplates.push(...(listTemplatesResponse?.data?.templateByOwner?.items ?? []));
  let nextToken = listTemplatesResponse?.data?.templateByOwner?.nextToken;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken as string;
    const listTemplatesResponseLoop = await templateQuery(graphqlOperationData);
    listTemplates.push(...(listTemplatesResponseLoop?.data?.templateByOwner?.items ?? []));
    nextToken = listTemplatesResponseLoop?.data?.templateByOwner?.nextToken;
  }
  return listTemplates;
};

export const listingGlobalTemplates = async () => {
  const globalTemplatesList = [];
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getGlobalTemplateQueryContext();
  const globalTemplates = await globalTemplateQuery(graphqlOperationData);
  globalTemplatesList.push(...(globalTemplates?.data?.listGlobalTemplatess?.items ?? []));
  let nextToken = globalTemplates?.data?.listGlobalTemplatess?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const globalTemplatesLoop = await globalTemplateQuery(graphqlOperationData);
    globalTemplatesList.push(...(globalTemplatesLoop?.data?.listGlobalTemplatess?.items ?? []));
    nextToken = globalTemplatesLoop?.data?.listGlobalTemplatess?.nextToken as string;
  }
  return globalTemplatesList;
};

export const listingSharedTemplates = async () => {
  const sharedTemplatesList = [];
  const shareAccountId = await handleAccountByCurrentCourse();
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getSharedTemplateQueryContext(shareAccountId.id);
  const sharedTemplates = await sharedTemplateQuery(graphqlOperationData);
  sharedTemplatesList.push(...(sharedTemplates?.data?.listSharedTemplatess?.items ?? []));
  let nextToken = sharedTemplates?.data?.listSharedTemplatess?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const sharedTemplatesLoop = await sharedTemplateQuery(graphqlOperationData);
    sharedTemplatesList.push(...(sharedTemplatesLoop?.data?.listSharedTemplatess?.items ?? []));
    nextToken = sharedTemplatesLoop?.data?.listSharedTemplatess?.nextToken as string;
  }
  return sharedTemplatesList;
};

export const listingCustomBlocks = async () => {
  const listBlocks = [];
  const graphqlOperationData: {
    ltiPlatformID: string | null;
    owner: { eq: string };
    nextToken?: string | null;
  } = await getMyTemplateQueryContext();
  const listBlocksResponse = await blockQuery(graphqlOperationData);
  listBlocks.push(...(listBlocksResponse?.data?.blockByOwner?.items ?? []));
  let nextToken = listBlocksResponse?.data?.blockByOwner?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const listBlocksResponseLoop = await blockQuery(graphqlOperationData);
    listBlocks.push(...(listBlocksResponseLoop?.data?.blockByOwner?.items ?? []));
    nextToken = listBlocksResponseLoop?.data?.blockByOwner?.nextToken as string;
  }
  return listBlocks;
};

export const listingGlobalBlocks = async () => {
  const globalBlocksList = [];
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getGlobalTemplateQueryContext();
  const globalBlocks = await globalBlockQuery(graphqlOperationData);
  globalBlocksList.push(...(globalBlocks?.data?.listGlobalBlockss?.items ?? []));
  let nextToken = globalBlocks?.data?.listGlobalBlockss?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const globalBlocksLoop = await globalBlockQuery(graphqlOperationData);
    globalBlocksList.push(...(globalBlocksLoop?.data?.listGlobalBlockss?.items ?? []));
    nextToken = globalBlocksLoop?.data?.listGlobalBlockss?.nextToken as string;
  }
  return globalBlocksList;
};

export const listingSharedBlocks = async () => {
  const sharedBlocksList = [];
  const shareAccountId = await handleAccountByCurrentCourse();
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getSharedTemplateQueryContext(shareAccountId.id);
  const sharedBlocks = await sharedBlockQuery(graphqlOperationData);
  sharedBlocksList.push(...(sharedBlocks?.data?.listSharedBlockss?.items ?? []));
  let nextToken = sharedBlocks?.data?.listSharedBlockss?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const sharedBlocksLoop = await sharedBlockQuery(graphqlOperationData);
    sharedBlocksList.push(...(sharedBlocksLoop?.data?.listSharedBlockss?.items ?? []));
    nextToken = sharedBlocksLoop?.data?.listSharedBlockss?.nextToken as string;
  }
  return sharedBlocksList;
};

export const getSelectedSharedTemplate = async (
  accountDetail: string,
  selectedBlockDataId: string,
) => {
  const getSharedTemplateInput = accountDetail
    ? getSharedTemplateBySelectedTemplate(accountDetail, selectedBlockDataId)
    : getTemplateBySelectedTemplate(selectedBlockDataId);
  return await API.graphql(graphqlOperation(listSharedTemplatess, getSharedTemplateInput));
};

export const getSelectedSharedBlocks = async (
  accountDetail: string,
  selectedBlockDataId: string,
) => {
  const getSharedBlockInput = accountDetail
    ? getSharedBlockBySelectedTemplate(accountDetail, selectedBlockDataId)
    : getBlockBySelectedTemplate(selectedBlockDataId);
  return await API.graphql(graphqlOperation(listSharedBlockss, getSharedBlockInput));
};
