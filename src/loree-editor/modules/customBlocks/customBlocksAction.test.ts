import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from '../../../aws-exports';
import * as customBlockActions from './customBlocksAction';
import { StorageMock } from '../../../utils/storageMock';
import {
  accountByCourseMockDataValue,
  courseMockDataValue,
  sharedBlockData,
  templateMockData,
  userMockData,
} from './customBlockMockData';
import { getMyTemplateQueryContext } from './blocksQueryUtils';

describe('#fetching templates', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
  });
  describe('my templates without  nextToken', () => {
    const listTemplatesResponse = {
      data: {
        templateByOwner: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest.fn().mockImplementation(() => listTemplatesResponse);
    });
    test('retrieving my templates', async () => {
      expect((await customBlockActions.listingCustomTemplates()).length).toEqual(1);
    });
  });
  describe('my templates with nextToken', () => {
    const listTemplatesResponseOne = {
      data: {
        templateByOwner: {
          items: [templateMockData],
          nextToken: 'dfheriuwfhjsdkshf',
        },
      },
    };
    const listTemplatesResponse = {
      data: {
        templateByOwner: {
          items: [
            {
              id: '1',
              title: 'test',
              content: '<p>hello</p>',
            },
          ],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => listTemplatesResponseOne)
        .mockImplementationOnce(() => listTemplatesResponse);
    });
    test('retrieving my templates', async () => {
      expect((await customBlockActions.listingCustomTemplates()).length).toEqual(2);
    });
  });
  describe('#global templates without nextToken', () => {
    const globalTemplatesResponse = {
      data: {
        listGlobalTemplatess: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      API.graphql = jest.fn().mockImplementation(() => globalTemplatesResponse);
    });
    test('retrieving global templates', async () => {
      expect((await customBlockActions.listingGlobalTemplates()).length).toEqual(1);
    });
  });
  describe('#global templates with nextToken', () => {
    const globalTemplatesResponseOne = {
      data: {
        listGlobalTemplatess: {
          items: [templateMockData],
          nextToken: 'eeeetgdfgsfgdsgre',
        },
      },
    };
    const globalTemplatesResponse = {
      data: {
        listGlobalTemplatess: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => globalTemplatesResponseOne)
        .mockImplementationOnce(() => globalTemplatesResponse);
    });
    test('retrieving global templates', async () => {
      expect((await customBlockActions.listingGlobalTemplates()).length).toEqual(2);
    });
  });
  describe('#shared Templates', () => {
    describe('shared templates without null value', () => {
      const courseMockData = {
        data: {
          courseDetails: courseMockDataValue,
        },
      };
      const accountMockData = {
        data: {
          accountByCourse: accountByCourseMockDataValue,
        },
      };
      const sharedTemplatesResponse = {
        data: {
          listSharedTemplatess: {
            items: [templateMockData],
            nextToken: null,
          },
        },
      };
      beforeEach(() => {
        sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
        API.graphql = jest
          .fn()
          .mockImplementationOnce(() => courseMockData)
          .mockImplementationOnce(() => accountMockData)
          .mockImplementationOnce(() => sharedTemplatesResponse);
      });
      test('retrieving shared templates', async () => {
        expect((await customBlockActions.listingSharedTemplates()).length).toEqual(1);
      });
    });
    describe('shared templates with nexttoken value', () => {
      const courseMockData = {
        data: {
          courseDetails: courseMockDataValue,
        },
      };
      const accountMockData = {
        data: {
          accountByCourse: accountByCourseMockDataValue,
        },
      };
      const sharedTemplatesResponseOne = {
        data: {
          listSharedTemplatess: {
            items: [templateMockData],
            nextToken: 'jdhfjkhdsjfhdskjhfjshf',
          },
        },
      };
      const sharedTemplatesResponse = {
        data: {
          listSharedTemplatess: {
            items: [templateMockData],
            nextToken: null,
          },
        },
      };
      beforeEach(() => {
        sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
        API.graphql = jest
          .fn()
          .mockImplementationOnce(() => courseMockData)
          .mockImplementationOnce(() => accountMockData)
          .mockImplementationOnce(() => sharedTemplatesResponseOne)
          .mockImplementationOnce(() => sharedTemplatesResponse);
      });
      test('retrieving shared templates', async () => {
        expect((await customBlockActions.listingSharedTemplates()).length).toEqual(2);
      });
    });
  });

  describe('#fetching blocks', () => {
    beforeEach(() => {
      global.sessionStorage = new StorageMock() as any;
    });
    describe('my rows without nextToken', () => {
      const listBlocksResponse = {
        data: {
          blockByOwner: {
            items: [templateMockData],
            nextToken: null,
          },
        },
      };
      beforeEach(() => {
        Amplify.configure(awsconfig);
        global.sessionStorage = new StorageMock() as any;
        sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
        const user = {
          attributes: userMockData,
        };
        Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
        API.graphql = jest.fn().mockImplementation(() => listBlocksResponse);
      });
      test('retrieving my blocks', async () => {
        expect((await customBlockActions.listingCustomBlocks()).length).toEqual(1);
      });
    });
    describe('my rows with nextToken', () => {
      const listBlocksResponseOne = {
        data: {
          blockByOwner: {
            items: [templateMockData],
            nextToken: 'kjdshfkjdshfj',
          },
        },
      };
      const listBlocksResponse = {
        data: {
          blockByOwner: {
            items: [
              {
                id: '1',
                title: 'test',
                content: '<p>hello</p>',
              },
            ],
            nextToken: null,
          },
        },
      };
      beforeEach(() => {
        Amplify.configure(awsconfig);
        global.sessionStorage = new StorageMock() as any;
        sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
        const user = {
          attributes: userMockData,
        };
        Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
        API.graphql = jest
          .fn()
          .mockImplementationOnce(() => listBlocksResponseOne)
          .mockImplementationOnce(() => listBlocksResponse);
      });
      test('retrieving my blocks', async () => {
        expect((await customBlockActions.listingCustomBlocks()).length).toEqual(2);
      });
    });
    describe('#global Blocks', () => {
      const globalblocksResponse = {
        data: {
          listGlobalBlockss: {
            items: [templateMockData],
            nextToken: null,
          },
        },
      };
      beforeEach(() => {
        sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
        API.graphql = jest.fn().mockImplementation(() => globalblocksResponse);
      });
      test('retrieving global blocks', async () => {
        expect((await customBlockActions.listingGlobalBlocks()).length).toEqual(1);
      });
    });
    describe('#global Blocks without nextToken', () => {
      const globalblocksResponseOne = {
        data: {
          listGlobalBlockss: {
            items: [templateMockData],
            nextToken: 'hdsfkjshfjdskjfhds',
          },
        },
      };
      const globalblocksResponse = {
        data: {
          listGlobalBlockss: {
            items: [templateMockData],
            nextToken: null,
          },
        },
      };
      beforeEach(() => {
        sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
        API.graphql = jest
          .fn()
          .mockImplementationOnce(() => globalblocksResponseOne)
          .mockImplementationOnce(() => globalblocksResponse);
      });
      test('retrieving global blocks', async () => {
        expect((await customBlockActions.listingGlobalBlocks()).length).toEqual(2);
      });
    });
    describe('#shared Blocks', () => {
      describe('Shared blocks without next token', () => {
        const courseMockData = {
          data: {
            courseDetails: courseMockDataValue,
          },
        };
        const accountMockData = {
          data: {
            accountByCourse: accountByCourseMockDataValue,
          },
        };
        const sharedBlocksResponse = {
          data: {
            listSharedBlockss: {
              items: [templateMockData],
              nextToken: null,
            },
          },
        };
        beforeEach(() => {
          sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
          API.graphql = jest
            .fn()
            .mockImplementationOnce(() => courseMockData)
            .mockImplementationOnce(() => accountMockData)
            .mockImplementationOnce(() => sharedBlocksResponse);
        });
        test('retrieving shared blocks', async () => {
          expect((await customBlockActions.listingSharedBlocks()).length).toEqual(1);
        });
      });
      describe('Shared blocks with next token', () => {
        const courseMockData = {
          data: {
            courseDetails: courseMockDataValue,
          },
        };
        const accountMockData = {
          data: {
            accountByCourse: accountByCourseMockDataValue,
          },
        };
        const sharedBlocksResponseOne = {
          data: {
            listSharedBlockss: {
              items: [templateMockData],
              nextToken: 'djfkjdsfjkdsfkjdhsjfkhdsjk',
            },
          },
        };
        const sharedBlocksResponse = {
          data: {
            listSharedBlockss: {
              items: [
                {
                  id: '1',
                  title: 'test',
                  content: '<p>hello</p>',
                },
              ],
              nextToken: null,
            },
          },
        };
        beforeEach(() => {
          sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
          API.graphql = jest
            .fn()
            .mockImplementationOnce(() => courseMockData)
            .mockImplementationOnce(() => accountMockData)
            .mockImplementationOnce(() => sharedBlocksResponseOne)
            .mockImplementationOnce(() => sharedBlocksResponse);
        });
        test('retrieving shared blocks', async () => {
          expect((await customBlockActions.listingSharedBlocks()).length).toEqual(2);
        });
      });
    });
  });
});

describe('#query', () => {
  let data: {};
  beforeEach(() => {
    data = getMyTemplateQueryContext();
  });
  describe('# templateQuery', () => {
    const listTemplatesResponse = {
      data: {
        templateByOwner: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest.fn().mockImplementation(() => listTemplatesResponse);
    });
    test('retrieving my templates', async () => {
      expect(await customBlockActions.templateQuery(data)).not.toBeNull();
    });
  });
  describe('#globalTemplatesQuery', () => {
    const globalTemplatesResponse = {
      data: {
        listGlobalTemplatess: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      API.graphql = jest.fn().mockImplementation(() => globalTemplatesResponse);
    });
    test('retrieving global templates', async () => {
      expect(await customBlockActions.globalTemplateQuery(data)).not.toBeNull();
    });
  });
  describe('sharedTemplatesQuery', () => {
    const courseMockData = {
      data: {
        courseDetails: courseMockDataValue,
      },
    };
    const accountMockData = {
      data: {
        accountByCourse: accountByCourseMockDataValue,
      },
    };
    const sharedTemplatesResponse = {
      data: {
        listSharedTemplatess: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => courseMockData)
        .mockImplementationOnce(() => accountMockData)
        .mockImplementationOnce(() => sharedTemplatesResponse);
    });
    test('retrieving shared templates', async () => {
      expect(await customBlockActions.sharedTemplateQuery(data)).not.toBeNull();
    });
  });
});
describe('#sharedBlocksQuery', () => {
  const sharedTemplatesResponse = {
    data: {
      listSharedBlockss: {
        items: sharedBlockData,
        nextToken: null,
      },
    },
  };
  beforeEach(() => {
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    API.graphql = jest.fn().mockImplementation(() => sharedTemplatesResponse);
  });
  test('retrieving shared blocks', async () => {
    expect(
      await customBlockActions.getSelectedSharedBlocks('', '80785345-2501-4cfe-a081-fb4e0454e1d3'),
    ).not.toBeNull();
  });
  test('retrieving shared blocks count', async () => {
    const data: _Any = await customBlockActions.getSelectedSharedBlocks(
      '',
      '80785345-2501-4cfe-a081-fb4e0454e1d3',
    );
    expect(data?.data.listSharedBlockss.items.length).toEqual(2);
  });
});
