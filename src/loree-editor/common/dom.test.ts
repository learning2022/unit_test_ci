import CONSTANTS from '../../loree-editor/constant';
import {
  createDiv,
  createElement,
  createIframeElement,
  getElementById,
  getIFrame,
  getIFrameElementById,
  getIFrameInputElementById,
  getInputElementById,
  createButton,
  getButtonById,
  createTooltipButton,
  addClassToElement,
  elementHasClass,
  removeElement,
  queryDirectChild,
  isElementOneOf,
  findUpwardsByTag,
  getAsElementWithTag,
  enforceAsElementWithTag,
  getAsElement,
} from './dom';

function temporaryHackToForceLoadIframe(iframe: HTMLIFrameElement, html: string) {
  // https://stackoverflow.com/questions/45934713/jsdom-access-divs-inside-iframe
  // https://stackoverflow.com/questions/65795866/is-it-possible-to-access-an-iframe-contents-within-jest-test
  // https://github.com/jsdom/jsdom#executing-scripts
  // while jsdom supports it create react app does not support  adding
  //     "testEnvironmentOptions": { "resources": "usable" }
  // work around due to this issue
  iframe.contentDocument?.open();
  iframe.contentDocument?.write(html);
  iframe.contentDocument?.close();
}

describe('dom utils', () => {
  it('createDiv with id only', () => {
    const lazyDevsDiv = createDiv('lazyDevDiv');
    expect(lazyDevsDiv.id).toEqual('lazyDevDiv');
    expect(lazyDevsDiv.className).toEqual('');
    expect(lazyDevsDiv).toMatchSnapshot();
  });

  it('createDiv with id and classes', () => {
    const lazyDevsDiv = createDiv('lazyDevDiv', 'great success');
    expect(lazyDevsDiv.id).toEqual('lazyDevDiv');
    expect(lazyDevsDiv.className).toEqual('great success');
    expect(lazyDevsDiv).toMatchSnapshot();
  });

  it('createIframe with id only', () => {
    const lazyDevsDiv = createIframeElement('lazyDevDiv');
    expect(lazyDevsDiv.id).toEqual('lazyDevDiv');
    expect(lazyDevsDiv.className).toEqual('');
    expect(lazyDevsDiv).toMatchSnapshot();
  });

  it('createIframe with id and classes', () => {
    const lazyDevsDiv = createIframeElement('lazyDevDiv', 'great success');
    expect(lazyDevsDiv.id).toEqual('lazyDevDiv');
    expect(lazyDevsDiv.className).toEqual('great success');
    expect(lazyDevsDiv).toMatchSnapshot();
  });

  test('create button element with id and class', () => {
    const buttonElement = createButton('test-id', 'test-class');
    expect(buttonElement.tagName).toEqual('BUTTON');
    expect(buttonElement.id).toEqual('test-id');
    expect(buttonElement.className).toEqual('test-class');
    expect(buttonElement).toMatchSnapshot();
  });

  test('create button element with id only', () => {
    const buttonElement = createButton('test-id');
    expect(buttonElement.tagName).toEqual('BUTTON');
    expect(buttonElement.id).toEqual('test-id');
    expect(buttonElement.className).toEqual('');
    expect(buttonElement).toMatchSnapshot();
  });

  test('create button element with class only', () => {
    const buttonElement = createButton('', 'test-class');
    expect(buttonElement.tagName).toEqual('BUTTON');
    expect(buttonElement.id).toEqual('');
    expect(buttonElement.className).toEqual('test-class');
    expect(buttonElement).toMatchSnapshot();
  });

  test('create tooltip button element with all attributes', () => {
    const tooltipButton = createTooltipButton('title', 'bottom', 'tooltipBtn', 'tooltip-style');
    expect(tooltipButton.tagName).toEqual('BUTTON');
    expect(tooltipButton.id).toEqual('tooltipBtn');
    expect(tooltipButton.className).toEqual('tooltip-style');
    expect(tooltipButton.title).toEqual('title');
    expect(tooltipButton).toHaveAttribute('data-placement', 'bottom');
  });

  test('create tooltip button element by skipping optional attributes', () => {
    const tooltipButton = createTooltipButton('title', 'bottom');
    expect(tooltipButton.tagName).toEqual('BUTTON');
    expect(tooltipButton.title).toEqual('title');
    expect(tooltipButton).toHaveAttribute('data-placement', 'bottom');
  });

  describe('dom retrieve utils', () => {
    beforeEach(() => {
      document.body.appendChild(createDiv('testdivid'));
      document.body.appendChild(createElement('button', 'thisIsaButtonId'));
      document.body.appendChild(createElement('a', 'thisIsNotaButtonId'));
      const iframe = createElement('iframe', CONSTANTS.LOREE_IFRAME) as HTMLIFrameElement;
      document.body.appendChild(iframe);
      const html = `
        <div id='testiframedivid' />
        <div>
          <input type='text' id='testiframeinputid' name='test' value='blah' />
        </div>
      `;
      iframe.src = 'data:text/html;charset=utf-8,' + encodeURI(html);
      temporaryHackToForceLoadIframe(iframe, html);
    });

    it('gets an element that exists', () => {
      expect(getElementById('testdivid').id).toEqual('testdivid');
    });

    // didnt work as expected :) - still got the div!?!?!
    it.skip('throws error when attempting to get a non input element with getInputElementById', () => {
      expect(getInputElementById('testdivid')).toBeNull();
    });

    it('getInputElementById does not get an input element as this one lives in the iframe', () => {
      expect(getInputElementById('testiframeinputid')).toBeNull();
    });

    it('gets an element that exists in an iframe using convience mtehod', () => {
      expect(getIFrameElementById('testiframedivid').id).toEqual('testiframedivid');
    });

    it('gets an element that exists in an iframe', () => {
      expect(getIFrame().getElementById('testiframedivid')?.id).toEqual('testiframedivid');
    });

    it('gets an input that exists in an iframe', () => {
      expect(getIFrameInputElementById('testiframeinputid').id).toEqual('testiframeinputid');
      expect(getIFrameInputElementById('testiframeinputid').getAttribute('name')).toEqual('test');
    });
    it('get button element in the HTML', () => {
      expect(getButtonById('thisIsaButtonId').id).toBe('thisIsaButtonId');
      expect(getButtonById('thisIsaButtonId').nodeName).toBe('BUTTON');
    });
    it('handle empty element in the HTML', () => {
      expect(() => getButtonById('thisIsNotanElementId')).toThrow();
    });
    it('handle elements rather than button', () => {
      expect(() => getButtonById('thisIsNotaButtonId')).toThrow();
    });
  });

  test('elementHasClass', () => {
    const el = document.createElement('div');
    el.classList.add('fred');
    expect(elementHasClass(el, 'fred')).toBeTruthy();
  });

  test('addClassToElement', () => {
    const el = document.createElement('div');

    addClassToElement(el, 'fred');
    expect(el.classList.contains('fred')).toBeTruthy();
  });

  describe('removeElement', function () {
    test('removes element', () => {
      const div = document.createElement('div');
      div.innerHTML = '<div id="y"><div id="x"><span>a</span><span>b</span></div></div>';

      const el = div.querySelector('#x');
      if (el) {
        removeElement(el);
      }
      expect(div.innerHTML).toEqual('<div id="y"><span>a</span><span>b</span></div>');
    });

    test('should work with nodes', () => {
      const div = document.createElement('div');
      div.innerHTML = '<div id="y">ijk<div id="x">abc<span>a</span>xyz<span>b</span></div></div>';

      const el = div.querySelector('#x');
      if (el) {
        removeElement(el);
      }
      expect(div.innerHTML).toEqual('<div id="y">ijkabc<span>a</span>xyz<span>b</span></div>');
    });
  });

  describe('queryDirectChild', function () {
    test('returns direct children', () => {
      const div = document.createElement('div');
      div.innerHTML = '<h3 id="y">ijk<a id="x">abc<span>a</span>xyz<span>b</span></a></h3>';

      const el = queryDirectChild(div, 'h3');

      expect(el).toBeTruthy();
      expect(el?.tagName).toEqual('H3');
    });

    test('does not find indirect children', () => {
      const div = document.createElement('div');
      div.innerHTML = '<h3 id="y">ijk<a id="x">abc<span>a</span>xyz<span>b</span></a></h3>';

      const el = queryDirectChild(div, 'a');

      expect(el).toBeNull();
    });
  });

  describe('isElementOneOf', function () {
    test('checks list for element - returns true when found', () => {
      const div = document.createElement('div');
      expect(isElementOneOf(div, ['A', 'H1', 'DIV'])).toBeTruthy();
    });

    test('checks list for element - returns false when not found', () => {
      const div = document.createElement('div');
      expect(isElementOneOf(div, ['A', 'H1', 'H2'])).toBeFalsy();
    });
  });

  describe('findUpwardsByTag', () => {
    test('find element if it is an ancestor', () => {
      document.body.innerHTML =
        '<div id="x"><h1><a><span>abc<b id="z">xyz</b></span></a></h1></div>';

      const el = document.body.querySelector('b');

      const el2 = findUpwardsByTag(el, 'div');
      expect(el2).toBeTruthy();
      expect(el2?.id).toEqual('x');
    });

    test('return null if it is not an ancestor', () => {
      document.body.innerHTML =
        '<div id="x"><h1><a><span>abc<b id="z">xyz</b></span></a></h1></div>';

      const el = document.body.querySelector('b');

      const el2 = findUpwardsByTag(el, 'h4');
      expect(el2).toBeFalsy();
    });

    test('accepts node as a parameter', () => {
      document.body.innerHTML =
        '<div id="x"><h1><a><span>abc<b id="z">xyz</b></span></a></h1></div>';

      const el = document.body.querySelector('b') as HTMLElement;
      const n: Node = el.childNodes[0];
      expect(n).not.toBeNull();
      const el2 = findUpwardsByTag(n, 'div');
      expect(n.nodeType).toEqual(n.TEXT_NODE);
      expect(el2).toBeTruthy();
      expect(el2?.id).toEqual('x');
    });
  });
});

describe('getAsElement', () => {
  test('returns element when it is an element', () => {
    const div = document.createElement('div');
    expect(getAsElement(div)).toBe(div);
  });

  test('throws when it not is an element', () => {
    const div = document.createTextNode('fred');
    expect(() => getAsElement(div)).toThrow();
  });
});

describe('getAsElementWithTag', () => {
  test('returns null an element', async () => {
    const div = document.createTextNode('fred');
    expect(getAsElementWithTag('a', div)).toBeNull();
  });

  test('returns element if tag matches', async () => {
    const div = document.createElement('div');
    expect(getAsElementWithTag('div', div)).toBe(div);
  });

  test('returns null if tag does not match', async () => {
    const div = document.createElement('a');
    expect(getAsElementWithTag('div', div)).toBeNull();
  });
});

describe('enforceAsElementWithTag', () => {
  test('throws if not an element', async () => {
    const div = document.createTextNode('fred');
    expect(() => enforceAsElementWithTag('a', div)).toThrow();
  });

  test('returns element if tag matches', async () => {
    const div = document.createElement('div');
    expect(enforceAsElementWithTag('div', div)).toBe(div);
  });

  test('throws if tag does not match', async () => {
    const div = document.createElement('a');
    expect(() => enforceAsElementWithTag('div', div)).toThrow();
  });
});
