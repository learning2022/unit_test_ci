import CONSTANTS from '../../loree-editor/constant';

export const createElement = (
  elementType: keyof HTMLElementTagNameMap,
  id?: string,
  classes?: string,
): HTMLElement => {
  const thing = document.createElement(elementType);
  if (id) thing.id = id;
  if (classes) thing.className = classes;
  return thing;
};

export const createinputElement = (
  id?: string,
  classes?: string,
  type?: string,
): HTMLInputElement => {
  const thing = document.createElement('input');
  if (id) thing.id = id;
  if (classes) thing.className = classes;
  if (type) thing.type = type;
  return thing;
};

export const createSelectElement = (
  id?: string,
  classes?: string,
  name?: string,
): HTMLSelectElement => {
  const select = document.createElement('select');
  if (id) select.id = id;
  if (classes) select.className = classes;
  if (name) select.name = name;
  return select;
};

export const createIframeElement = (id?: string, classes?: string): HTMLIFrameElement => {
  const element = document.createElement('iframe');
  if (id) element.id = id;
  if (classes) element.className = classes;
  return element;
};

export const createDiv = (id?: string, classes?: string): HTMLElement =>
  createElement('div', id, classes);

export const createButton = (id?: string, classes?: string): HTMLElement =>
  createElement('button', id, classes);

export const createTooltipButton = (
  title: string,
  placement: string,
  id?: string,
  classes?: string,
): HTMLElement => {
  const button = document.createElement('button');
  if (id) button.id = id;
  if (classes) button.className = classes;
  button.setAttribute('data-toggle', 'tooltip');
  button.setAttribute('title', title);
  button.setAttribute('data-placement', placement);
  return button;
};

export const createParagraph = (id?: string, classes?: string): HTMLElement =>
  createElement('p', id, classes);

// use this when know an element will exist
export const getElementById = (id: string, doc: Document = document): HTMLElement =>
  // we know this element will exist
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  doc.getElementById(id)!;

// use this when know an element will exist
export const getInputElementById = (id: string, doc: Document = document): HTMLInputElement =>
  // we know this element will exist
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  doc.getElementById(id)! as HTMLInputElement;

export const getButtonById = (id: string, doc: Document = document): HTMLButtonElement => {
  const documentElement = doc.getElementById(id);
  if (documentElement?.nodeName === 'BUTTON') {
    return documentElement as HTMLButtonElement;
  } else {
    throw new Error('this was supposed to be a button');
  }
};

// use this when know an element will exist and we dont want the current document
// we want to get data out of an iframe - defaults to the editor iframe
export const getIFrameElementById = (id: string, iframe: Document = getIFrame()): HTMLElement =>
  iframe.getElementById(id) as HTMLElement;

export const getIFrameElementByClassName = (
  className: string,
  iframe: Document = getIFrame(),
): HTMLCollectionOf<Element> => iframe.getElementsByClassName(className);

// use this when know an element will exist and we dont want the current document
// we want to get data out of an iframe - defaults to the editor iframe
export const getIFrameInputElementById = (
  id: string,
  iframe: Document = getIFrame(),
): HTMLElement => iframe.getElementById(id) as HTMLElement;

// gets our editor iframe
export const getIFrame = (id: string = CONSTANTS.LOREE_IFRAME): Document => {
  const iframe: HTMLIFrameElement = document.getElementById(id) as HTMLIFrameElement;
  // we know iframe exists
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return iframe.contentDocument!;
};

// gets our editor iframe
export const getIFrameElement = (id: string = CONSTANTS.LOREE_IFRAME): HTMLIFrameElement => {
  const iframe: HTMLIFrameElement = document.getElementById(id) as HTMLIFrameElement;
  // we know iframe exists
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return iframe;
};

export const getNElementByClassName = (className: string, index: number = 0): HTMLElement => {
  return document.getElementsByClassName(className)[index] as HTMLElement;
};

export const getElementsByClassName = (
  className: string,
  doc: Document = document,
): HTMLCollectionOf<Element> =>
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  doc.getElementsByClassName(className)!;

/**
 * Insert a new node into the hierarchy between currentParent and all its children
 * @param currentParent
 * @param newWrapper
 */
export function wrapChildElements(currentParent: Node, newWrapper: Node) {
  while (currentParent.hasChildNodes()) {
    newWrapper.appendChild(currentParent.childNodes[0]);
  }

  currentParent.appendChild(newWrapper);
}

/**
 * Wrap an element in a new element
 * @param elementToWrap
 * @param wrapper
 */
export function wrapElement(elementToWrap: HTMLElement, wrapper: HTMLElement) {
  if (elementToWrap.parentElement) {
    elementToWrap.parentElement.appendChild(wrapper);
  }

  wrapper.appendChild(elementToWrap);
}

/**
 * Remove an element, moving all its children up to being owned by this element's parent
 * @param currentParent
 */
export function removeElement(currentParent: Element) {
  const parent = currentParent.parentElement;
  if (parent) {
    while (currentParent.hasChildNodes()) {
      parent.insertBefore(currentParent.childNodes[0], currentParent);
    }
  }
  currentParent.remove();
}

/**
 * Get a direct child element via tag selector, returns null if it doesnt exist
 * @param element
 * @param selectors
 * @returns
 */
export function queryDirectChild<K extends keyof HTMLElementTagNameMap>(
  element: Element,
  selectors: K,
): HTMLElementTagNameMap[K] | null {
  const v = element.querySelector(selectors);
  return v?.parentElement === element ? v : null;
}

export function elementHasClass(element: HTMLElement, className: string): boolean {
  return element?.classList?.contains(className);
}

export function addClassToElement(element: HTMLElement, className: string) {
  element?.classList.add(className);
}

/**
 * From a node, navigate upward until you hit the matching tag, may match original node
 * if it is of the right tag
 * @param node Node
 * @param tag string
 */
export function findUpwardsByTag<K extends keyof HTMLElementTagNameMap>(
  node: Node | null,
  tag: K,
): HTMLElementTagNameMap[K] | null {
  let e: Node | null = node;
  while (e && (e.nodeType !== e.ELEMENT_NODE || (e as HTMLElement).tagName !== tag.toUpperCase())) {
    e = e.parentElement;
  }
  return e as HTMLElementTagNameMap[K];
}

export function isElementOneOf(element: HTMLElement, tags: string[]) {
  return tags.includes(element.tagName);
}

export const getElementByQuerySelector = (selector: string): HTMLElement | null =>
  document.querySelector(selector);

export const getElementsByQuerySelectorAll = (
  selector: string,
  doc: Document | Element | HTMLElement = document,
): NodeListOf<Element> => doc.querySelectorAll(selector);

/**
 * Get the Element from a Node.  If the Node is not an Element an exception
 * will be thrown
 * @param node - Node to check.
 */
export function getAsElement(node: Node): HTMLElement {
  if (node.nodeType !== Node.ELEMENT_NODE) {
    throw new TypeError('Node is not an Element');
  }
  return node as HTMLElement;
}

/**
 * Get the Element from a node as tag.  If the node is not an element
 * or does not match the tag, an exception will be thrown
 * @param tag name of the tag e.g. 'div', 'a', etc
 * @param node the node to turn into an element
 */
export function enforceAsElementWithTag<K extends keyof HTMLElementTagNameMap>(
  tag: K,
  node: Node,
): HTMLElementTagNameMap[K] {
  if (node.nodeType !== Node.ELEMENT_NODE) {
    throw new TypeError('Node is not an Element');
  }
  if ((node as HTMLElement).tagName !== tag.toUpperCase()) {
    throw new TypeError('Element does not have tag: ' + tag);
  }
  return node as HTMLElementTagNameMap[K];
}

/**
 * Get the Element from a node as tag.  If the node is not an element null
 * will be returned.
 * if the tag doesn't match, null will be returned.
 * @param tag name of the tag e.g. 'div', 'a', etc
 * @param node the node to turn into an element
 */
export function getAsElementWithTag<K extends keyof HTMLElementTagNameMap>(
  tag: K,
  node: Node | null,
): HTMLElementTagNameMap[K] | null {
  if (!node) {
    return null;
  }
  if (node.nodeType !== Node.ELEMENT_NODE) {
    return null;
  }
  if ((node as HTMLElement).tagName !== tag.toUpperCase()) {
    return null;
  }
  return node as HTMLElementTagNameMap[K];
}
