import React from 'react';
import { mount } from 'enzyme';
import ItemList from './topicList';
import { BrowserRouter as Router } from 'react-router-dom';

describe('#topicList', () => {
  describe('when topic list has result', () => {
    const topicListProps = {
      hierarchyLevel: '',
      itemList: [
        {
          IsExempt: false,
          IsHidden: false,
          SortOrder: 452,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          IsBroken: false,
          ActivityId: 'https://ids.brightspace.com/activities/contenttopic/higherednce-236',
          CompletionType: 2,
          TopicId: 1065,
          Identifier: '1065',
          TypeIdentifier: 'File',
          Title: 'Brightspace is Mobile!',
          Bookmarked: false,
          Unread: true,
          Url: '/content/Brightspace is Mobile!.html',
          ToolId: null,
          ToolItemId: null,
          ActivityType: 1,
          GradeItemId: null,
          LastModifiedDate: '2018-07-20T21:20:45.953Z',
          Description: {
            Text: '',
            Html: '',
          },
        },
        {
          IsExempt: false,
          IsHidden: false,
          SortOrder: 446,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          IsBroken: false,
          ActivityId:
            'https://ids.brightspace.com/activities/dropbox/4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
          CompletionType: 2,
          TopicId: 12801,
          Identifier: '12801',
          TypeIdentifier: 'Link',
          Title: 'Untitled',
          Bookmarked: false,
          Unread: true,
          Url: '/d2l/common/dialogs/quickLink/quickLink.d2l?ou=6606&type=dropbox&rCode=4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
          ToolId: 2000,
          ToolItemId: 542,
          ActivityType: 3,
          GradeItemId: null,
          LastModifiedDate: '2020-12-10T07:24:34.810Z',
          Description: {
            Text: '',
            Html: '',
          },
        },
        {
          IsExempt: false,
          IsHidden: false,
          SortOrder: 446,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          IsBroken: false,
          ActivityId:
            'https://ids.brightspace.com/activities/dropbox/4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
          CompletionType: 2,
          TopicId: 12801,
          Identifier: '12801',
          TypeIdentifier: 'Link',
          Title: 'Untitled',
          Bookmarked: false,
          Unread: true,
          Url: '/d2l/common/dialogs/quickLink/quickLink.d2l?ou=6606&type=dropbox&rCode=4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
          ToolId: 2000,
          ToolItemId: 542,
          ActivityType: 5,
          GradeItemId: null,
          LastModifiedDate: '2020-12-10T07:24:34.810Z',
          Description: {
            Text: '',
            Html: '',
          },
        },
      ],
      moduleList: [
        {
          IsHidden: false,
          SortOrder: 395,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          PacingStartDate: null,
          PacingEndDate: null,
          DefaultPath: '/content/',
          ModuleId: 1051,
          Title: 'Course In Progress',
          Modules: [],
          Topics: [
            {
              IsExempt: false,
              IsHidden: false,
              SortOrder: 452,
              StartDateTime: null,
              EndDateTime: null,
              IsLocked: false,
              IsBroken: false,
              ActivityId: 'https://ids.brightspace.com/activities/contenttopic/higherednce-236',
              CompletionType: 2,
              TopicId: 1065,
              Identifier: '1065',
              TypeIdentifier: 'File',
              Title: 'Brightspace is Mobile!',
              Bookmarked: false,
              Unread: true,
              Url: '/content/Brightspace is Mobile!.html',
              ToolId: null,
              ToolItemId: null,
              ActivityType: 1,
              GradeItemId: null,
              LastModifiedDate: '2018-07-20T21:20:45.953Z',
              Description: {
                Text: '',
                Html: '',
              },
            },
            {
              IsExempt: false,
              IsHidden: false,
              SortOrder: 446,
              StartDateTime: null,
              EndDateTime: null,
              IsLocked: false,
              IsBroken: false,
              ActivityId:
                'https://ids.brightspace.com/activities/dropbox/4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
              CompletionType: 2,
              TopicId: 12801,
              Identifier: '12801',
              TypeIdentifier: 'Link',
              Title: 'Untitled',
              Bookmarked: false,
              Unread: true,
              Url: '/d2l/common/dialogs/quickLink/quickLink.d2l?ou=6606&type=dropbox&rCode=4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
              ToolId: 2000,
              ToolItemId: 542,
              ActivityType: 3,
              GradeItemId: null,
              LastModifiedDate: '2020-12-10T07:24:34.810Z',
              Description: {
                Text: '',
                Html: '',
              },
            },
            {
              IsExempt: false,
              IsHidden: false,
              SortOrder: 446,
              StartDateTime: null,
              EndDateTime: null,
              IsLocked: false,
              IsBroken: false,
              ActivityId:
                'https://ids.brightspace.com/activities/dropbox/4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
              CompletionType: 2,
              TopicId: 12801,
              Identifier: '12801',
              TypeIdentifier: 'Link',
              Title: 'Untitled',
              Bookmarked: false,
              Unread: true,
              Url: '/d2l/common/dialogs/quickLink/quickLink.d2l?ou=6606&type=dropbox&rCode=4527DE4C-056A-414A-AEA2-B0AEC6B26387-3216',
              ToolId: 2000,
              ToolItemId: 542,
              ActivityType: 5,
              GradeItemId: null,
              LastModifiedDate: '2020-12-10T07:24:34.810Z',
              Description: {
                Text: '',
                Html: '',
              },
            },
          ],
          LastModifiedDate: '2018-06-29T15:13:54.980Z',
          Description: {
            Text: '',
            Html: '',
          },
        },
        {
          IsHidden: false,
          SortOrder: 609,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          PacingStartDate: null,
          PacingEndDate: null,
          DefaultPath: '/content/',
          ModuleId: 9630,
          Title: 'BSBWOR301- Organise personal work priorities',
          Modules: [
            {
              IsHidden: false,
              SortOrder: 610,
              StartDateTime: null,
              EndDateTime: null,
              IsLocked: false,
              PacingStartDate: null,
              PacingEndDate: null,
              DefaultPath: '/content/',
              ModuleId: 9631,
              Title: 'eLearning Activities',
              Modules: [],
              Topics: [
                {
                  IsExempt: false,
                  IsHidden: false,
                  SortOrder: 617,
                  StartDateTime: null,
                  EndDateTime: null,
                  IsLocked: false,
                  IsBroken: false,
                  ActivityId: 'https://ids.brightspace.com/activities/contenttopic/VU-466425',
                  CompletionType: 3,
                  TopicId: 9635,
                  Identifier: '9635',
                  TypeIdentifier: 'SCORM_1_2',
                  Title: 'Organise Personal Work Priorities - Part 3 (BSBWOR301)',
                  Bookmarked: false,
                  Unread: true,
                  Url: 'BSBWOR301/SCORM Files/Part 3/index_lms.html',
                  ToolId: null,
                  ToolItemId: null,
                  ActivityType: 22,
                  GradeItemId: null,
                  LastModifiedDate: '2020-12-03T06:09:41.977Z',
                  Description: {
                    Text: '',
                    Html: '',
                  },
                },
              ],
              LastModifiedDate: '2020-12-12T09:49:14.590Z',
              Description: {
                Text: '',
                Html: '',
              },
            },
          ],
          Topics: [],
          LastModifiedDate: '2020-12-03T06:09:41.677Z',
          Description: {
            Text: ' Organise Personal Work Priorities\nThis unit describes the skills and knowledge required to organise own work schedules, to monitor and obtain feedback on work performance and to maintain required levels of competence.\nThis unit applies to individuals who exercise discretion and judgement and apply a broad range of competencies in various work contexts.\n',
            Html: '<h2 style="text-align: left;background-color: #900c3f;"><span style="color: white;">&#160;Organise Personal Work Priorities<br/></span></h2>\n<p class="ait4">This unit describes the skills and knowledge required to organise own work schedules, to monitor and obtain feedback on work performance and to maintain required levels of competence.</p>\n<p class="ait4">This unit applies to individuals who exercise discretion and judgement and apply a broad range of competencies in various work contexts.</p>\n<p></p>',
          },
        },
      ],
    };
    const topicList = mount(
      <Router>
        <ItemList {...topicListProps} />
      </Router>,
    );
    test('topic list is rendered', () => {
      expect(topicList.find('div.list-group-item')).toHaveLength(3);
    });
  });
  describe('when topic list is empty', () => {
    const topicListProps = {
      hierarchyLevel: '',
      itemList: [],
      moduleList: [],
    };

    const moduleList = mount(
      <Router>
        <ItemList {...topicListProps} />
      </Router>,
    );
    test('does not render topic list', () => {
      expect(moduleList.find('div.list-group-item')).toHaveLength(0);
    });
  });
});
