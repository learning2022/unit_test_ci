/* eslint-disable */ // Remove this line when editing this file
import React, { Component } from 'react';
import { Accordion, Card, Row, Col, Button } from 'react-bootstrap';
import ItemLists from './topicList';
import editIcon from '../../../assets/images/edit.svg';
import { Link } from 'react-router-dom';

interface moduleProps {
  moduleList: any;
  hierarchyLevel: any;
  handlePageModal?: (moduleId: any) => void;
  handleFolderModal?: (moduleId: any, type: any) => void;
}

class ModulesList extends Component<moduleProps> {
  state = {
    moduleList: [],
    topicList: [],
    hierarchyLevel: '',
  };

  componentDidMount() {
    this.setState({
      moduleList: this.props.moduleList,
      topicList: this.props.moduleList.Topics,
      hierarchyLevel: this.props.hierarchyLevel,
    });
  }

  getSiblings = (e: any) => {
    // for collecting siblings
    const siblings: any[] = [];
    // if no parent, return no sibling
    if (!e.parentNode) {
      return siblings;
    }
    // first child of the parent node
    let sibling = e.parentNode.firstChild;
    // collecting siblings
    while (sibling) {
      if (sibling.nodeType === 1 && sibling !== e) {
        siblings.push(sibling);
      }
      sibling = sibling.nextSibling;
    }
    return siblings;
  };

  handleToggleAccordionIcon = (e: any) => {
    const currentClickedElement = e.target.closest('.card') as HTMLElement;
    const siblings = this.getSiblings(currentClickedElement);
    siblings.forEach((e: any) => {
      const collapseElement = e.getElementsByClassName('icon-down-arrow')[0];
      if (collapseElement !== undefined)
        collapseElement.className = collapseElement?.className.replace('icon-down-arrow', 'icon-right-arrow');
    });
    const collapsed = e.target.parentNode.parentNode.parentNode.parentNode.childNodes[1].className === 'collapse';
    !collapsed
      ? (e.target.className = e.target.className.replace('icon-down-arrow', 'icon-right-arrow'))
      : (e.target.className = e.target.className.replace('icon-right-arrow', 'icon-down-arrow'));
  };

  render() {
    return this.state.moduleList.length > 0 ? (
      <Accordion className='mt-2 d2l-landing-dashboard'>
        {this.state.moduleList.map((module: any, index: any) => (
          <Card key={index} className='mb-3'>
            <Card.Header className='p-1'>
              <Row>
                <Col>
                  <Accordion.Toggle
                    as={Button}
                    variant='link'
                    eventKey={`${index}`}
                    className='icon-right-arrow text-decoration-none'
                    onClick={e => this.handleToggleAccordionIcon(e)}
                  >
                    {module['Title']}
                  </Accordion.Toggle>
                </Col>
                <Col>
                  <div className='d-flex flex-row-reverse px-2 py-1'>
                    <Link
                      to={`/lti/editor/${module['ModuleId'] ? module['ModuleId'] : module['Id']}/modules/${encodeURIComponent(
                        module['Title'],
                      )}`}
                    >
                      {' '}
                      <img src={editIcon} alt='edit' title='Edit' style={{ width: '15px' }} />{' '}
                    </Link>
                  </div>
                </Col>
              </Row>
            </Card.Header>
            <Accordion.Collapse eventKey={`${index}`}>
              <Card.Body className='p-0'>
                <ItemLists
                  itemList={this.state.moduleList[index]['Topics']}
                  moduleList={this.props.moduleList[index]}
                  hierarchyLevel={this.state.hierarchyLevel}
                  handlePageModal={this.props.handlePageModal}
                  handleFolderModal={this.props.handleFolderModal}
                />
                <div className='d-flex flex-row py-2'>
                  {this.state.hierarchyLevel !== 'FOLDER' ? (
                    <Button
                      variant='primary'
                      className='mr-3'
                      onClick={() =>
                        this.props.handleFolderModal
                          ? this.props.handleFolderModal(
                              module['ModuleId'] ? module['ModuleId'] : module['Id'],
                              this.state.hierarchyLevel === 'UNIT' ? 'LESSON' : this.state.hierarchyLevel === 'LESSON' ? 'FOLDER' : '',
                            )
                          : ''
                      }
                    >
                      {this.state.hierarchyLevel === 'UNIT'
                        ? 'Add New Lesson'
                        : this.state.hierarchyLevel === 'LESSON'
                        ? 'Add New Folder'
                        : ''}
                    </Button>
                  ) : (
                    ''
                  )}
                  <Button
                    variant='primary'
                    onClick={() =>
                      this.props.handlePageModal ? this.props.handlePageModal(module['ModuleId'] ? module['ModuleId'] : module['Id']) : ''
                    }
                  >
                    Add New Page
                  </Button>
                </div>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        ))}
      </Accordion>
    ) : (
      <div className='border'>
        <div className='p-3 font-italic'>No modules found</div>
      </div>
    );
  }
}

export default ModulesList;
