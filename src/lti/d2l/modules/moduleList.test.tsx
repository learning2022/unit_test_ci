import React from 'react';
import { mount } from 'enzyme';
import ModulesList from './moduleList';
import { BrowserRouter as Router } from 'react-router-dom';

describe('#moduleList', () => {
  describe('when module list is present', () => {
    const moduleListProps = {
      hierarchyLevel: '',
      moduleList: [
        {
          IsHidden: false,
          SortOrder: 395,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          PacingStartDate: null,
          PacingEndDate: null,
          DefaultPath: '/content/',
          ModuleId: 1051,
          Title: 'Course In Progress',
          Modules: [],
          Topics: [
            {
              IsExempt: false,
              IsHidden: false,
              SortOrder: 452,
              StartDateTime: null,
              EndDateTime: null,
              IsLocked: false,
              IsBroken: false,
              ActivityId: 'https://ids.brightspace.com/activities/contenttopic/higherednce-236',
              CompletionType: 2,
              TopicId: 1065,
              Identifier: '1065',
              TypeIdentifier: 'File',
              Title: 'Brightspace is Mobile!',
              Bookmarked: false,
              Unread: true,
              Url: '/content/Brightspace is Mobile!.html',
              ToolId: null,
              ToolItemId: null,
              ActivityType: 1,
              GradeItemId: null,
              LastModifiedDate: '2018-07-20T21:20:45.953Z',
              Description: {
                Text: '',
                Html: '',
              },
            },
          ],
          LastModifiedDate: '2018-06-29T15:13:54.980Z',
          Description: {
            Text: '',
            Html: '',
          },
        },
        {
          IsHidden: false,
          SortOrder: 609,
          StartDateTime: null,
          EndDateTime: null,
          IsLocked: false,
          PacingStartDate: null,
          PacingEndDate: null,
          DefaultPath: '/content/',
          ModuleId: 9630,
          Title: 'BSBWOR301- Organise personal work priorities',
          Modules: [
            {
              IsHidden: false,
              SortOrder: 610,
              StartDateTime: null,
              EndDateTime: null,
              IsLocked: false,
              PacingStartDate: null,
              PacingEndDate: null,
              DefaultPath: '/content/',
              ModuleId: 9631,
              Title: 'eLearning Activities',
              Modules: [],
              Topics: [
                {
                  IsExempt: false,
                  IsHidden: false,
                  SortOrder: 617,
                  StartDateTime: null,
                  EndDateTime: null,
                  IsLocked: false,
                  IsBroken: false,
                  ActivityId: 'https://ids.brightspace.com/activities/contenttopic/VU-466425',
                  CompletionType: 3,
                  TopicId: 9635,
                  Identifier: '9635',
                  TypeIdentifier: 'SCORM_1_2',
                  Title: 'Organise Personal Work Priorities - Part 3 (BSBWOR301)',
                  Bookmarked: false,
                  Unread: true,
                  Url: 'BSBWOR301/SCORM Files/Part 3/index_lms.html',
                  ToolId: null,
                  ToolItemId: null,
                  ActivityType: 22,
                  GradeItemId: null,
                  LastModifiedDate: '2020-12-03T06:09:41.977Z',
                  Description: {
                    Text: '',
                    Html: '',
                  },
                },
              ],
              LastModifiedDate: '2020-12-12T09:49:14.590Z',
              Description: {
                Text: '',
                Html: '',
              },
            },
          ],
          Topics: [],
          LastModifiedDate: '2020-12-03T06:09:41.677Z',
          Description: {
            Text: ' Organise Personal Work Priorities\nThis unit describes the skills and knowledge required to organise own work schedules, to monitor and obtain feedback on work performance and to maintain required levels of competence.\nThis unit applies to individuals who exercise discretion and judgement and apply a broad range of competencies in various work contexts.\n',
            Html: '<h2 style="text-align: left;background-color: #900c3f;"><span style="color: white;">&#160;Organise Personal Work Priorities<br/></span></h2>\n<p class="ait4">This unit describes the skills and knowledge required to organise own work schedules, to monitor and obtain feedback on work performance and to maintain required levels of competence.</p>\n<p class="ait4">This unit applies to individuals who exercise discretion and judgement and apply a broad range of competencies in various work contexts.</p>\n<p></p>',
          },
        },
      ],
    };

    const moduleList = mount(
      <Router>
        <ModulesList {...moduleListProps} />
      </Router>,
    );
    test('module list is rendered', () => {
      expect(moduleList.find('div.accordion')).toHaveLength(2);
    });
    describe('when click any accordion', () => {
      // todo
      test.skip('icon for clicked accordion is changed', () => {
        const handleToggleAccordionIcon = jest.fn();
        moduleList
          .find('button.icon-right-arrow')
          .at(0)
          .simulate('click', handleToggleAccordionIcon);
        expect(handleToggleAccordionIcon.mock).toBeCalledTimes(1);
      });
    });
  });
  describe('when module list is empty', () => {
    const moduleListProps = {
      moduleList: [],
      hierarchyLevel: '',
    };

    const moduleList = mount(
      <Router>
        <ModulesList {...moduleListProps} />
      </Router>,
    );
    test('does not render module list', () => {
      expect(moduleList.find('div.accordion')).toHaveLength(0);
    });
  });
});
