/* eslint-disable */ // Remove this line when editing this file
import React, { Component, Fragment } from 'react';
import { ListGroup, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileAlt, faRocket, faThList, faComments } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import editIcon from '../../../assets/images/edit.svg';
import ModuleList from './moduleList';
interface ItemProps {
  itemList: any;
  moduleList: any;
  hierarchyLevel: any;
  handlePageModal?: (moduleId: any) => void;
  handleFolderModal?: (moduleId: any, type: any) => void;
}

class ItemList extends Component<ItemProps> {
  render() {
    return (
      <Fragment>
        <ListGroup variant='flush' className='module-item hierarchy-sub-level'>
          {this.props.moduleList['Modules'] && this.props.moduleList['Modules'].length > '0' ? (
            <ModuleList
              moduleList={this.props.moduleList['Modules']}
              hierarchyLevel={this.props.hierarchyLevel === 'UNIT' ? 'LESSON' : this.props.hierarchyLevel === 'LESSON' ? 'FOLDER' : ''}
              handlePageModal={this.props.handlePageModal}
              handleFolderModal={this.props.handleFolderModal}
            />
          ) : (
            ''
          )}
          {this.props.itemList !== undefined
            ? (this.props.itemList as Array<any>).length > 0
              ? (this.props.itemList as Array<any>).map((item: any, index: any) => (
                  <ListGroup.Item key={index}>
                    <Row>
                      {item.DefaultPath || item.ActivityType === 1 ? (
                        <Fragment>
                          <Col>
                            <FontAwesomeIcon icon={faFileAlt} className='item-icon mr-3' /> {item.Title}
                          </Col>
                          <Col>
                            <div className='d-flex justify-content-end'>
                              <Link to={`/lti/editor/${item['TopicId']}/topics/${encodeURIComponent(item['Title'])}`}>
                                <img src={editIcon} alt='edit' title='Edit' style={{ width: '15px' }} />{' '}
                              </Link>
                            </div>
                          </Col>
                        </Fragment>
                      ) : item.ActivityType === 3 ? (
                        <Fragment>
                          <Col>
                            <FontAwesomeIcon icon={faThList} className='mr-2' /> {item.Title}
                          </Col>
                          <Col>
                            <div className='d-flex flex-row-reverse'>
                              <Link to={`/lti/editor/${item['TopicIdd']}/topics/${encodeURIComponent(item['Title'])}`}>
                                <img src={editIcon} alt='edit' title='Edit' style={{ width: '15px' }} />{' '}
                              </Link>
                            </div>
                          </Col>
                        </Fragment>
                      ) : item.ActivityType === 5 || item.ActivityType === 6 ? (
                        <Fragment>
                          <Col>
                            <FontAwesomeIcon icon={faComments} className='mr-2' /> {item.Title}
                          </Col>
                          <Col>
                            <div className='d-flex flex-row-reverse'>
                              <Link to={`/lti/editor/${item['TopicId']}/topics/${encodeURIComponent(item['Title'])}`}>
                                <img src={editIcon} alt='edit' title='Edit' style={{ width: '15px' }} />{' '}
                              </Link>
                            </div>
                          </Col>
                        </Fragment>
                      ) : (
                        <Fragment>
                          <Col>
                            <FontAwesomeIcon icon={faRocket} className='item-icon mr-3' />
                            {item.Title}
                          </Col>
                        </Fragment>
                      )}
                    </Row>
                  </ListGroup.Item>
                ))
              : ''
            : ''}
        </ListGroup>
      </Fragment>
    );
  }
}

export default ItemList;
