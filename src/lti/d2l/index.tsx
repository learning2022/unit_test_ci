/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { Component } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import LandingPageHeader from '../components/landingPageHeader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCodeBranch } from '@fortawesome/free-solid-svg-icons';
import ModuleLists from './modules/moduleList';
import { d2lModules } from '../../graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import Loading from '../../components/loader/loading';
import CreateRenameModules from './components/d2lCreateRenameModules';
import { AddModule, createPage, createFolder } from './components/actions';
import ToastComponent from '../components/ToastComponent';
import HelpSection from '../helpSection';
import Space from '../../loree-editor/modules/space/space';
import CONSTANTS from '../../loree-editor/constant';
import { translate } from '../../i18n/translate';
class D2lHome extends Component {
  state = {
    d2lItem: 'modules',
    brightSpaceData: [],
    loading: true,
    showCreateRenameModel: false,
    isCreateType: '',
    moduleName: '',
    btnLoading: false,
    showToast: false,
    toastMessage: '',
    hierarchyLevel: '',
    moduleId: '',
  };

  // Updating Dropdown value
  selectD2lItem = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({
      d2lItem: event.target.value,
    });
  };

  async componentDidMount() {
    const course_id = sessionStorage.getItem('course_id');
    const modulesResponse: any = await API.graphql(
      graphqlOperation(d2lModules, { courseId: course_id }),
    );
    const modulesData = JSON.parse(modulesResponse.data.d2lModules);
    this.setState({
      brightSpaceData: modulesData.body.Modules,
      loading: false,
      hierarchyLevel: 'UNIT',
    });
  }

  // For creating D2l modules
  moduleNameHandler = (event: any) => {
    this.setState({
      moduleName: event.target.value,
    });
  };

  handleCreate = (createType: any, event: any) => {
    if (createType === 'LESSON') createType = 'FOLDER';
    switch (createType) {
      case 'UNIT':
        this.handleModuleAdd(event);
        return;
      case 'PAGE':
        this.handleAddPage(event);
        return;
      case 'FOLDER':
        this.handleAddFolder(event);
    }
  };

  isEmptyOrSpaces = (str: any) => {
    return str === null || str.match(/^ *$/) !== null;
  };

  handleModuleAdd = async (event: any) => {
    if (!this.isEmptyOrSpaces(this.state.moduleName)) {
      const name = this.state.moduleName.trim();
      this.setState({ btnLoading: true });
      await AddModule(event, name, this.state.brightSpaceData);
      const toastMsg = translate('d2l.unitaddsuccessdetail', { detail: name });
      this.closeCreateRenameModel();
      this.setState({ showToast: true, toastMessage: toastMsg, moduleName: '', btnLoading: false });
    }
  };

  closeCreateRenameModel = () => {
    this.setState({ showCreateRenameModel: false });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  handleAddPage = async (event: any) => {
    if (!this.isEmptyOrSpaces(this.state.moduleName)) {
      let toastMsg = '';
      const name = this.state.moduleName.trim();
      this.setState({ btnLoading: true });
      const newModuleList = await createPage(event, name, this.state.moduleId);
      if (newModuleList === 'error') {
        toastMsg = translate('d2l.pageaddfaildetail', { detail: name });
      } else {
        this.setState({ brightSpaceData: [], loading: true });
        toastMsg = translate('d2l.pageaddsuccessdetail', { detail: name });
      }
      this.closeCreateRenameModel();
      this.setState({
        brightSpaceData: newModuleList === 'error' ? this.state.brightSpaceData : newModuleList,
        loading: false,
        hierarchyLevel: 'UNIT',
        showToast: true,
        toastMessage: toastMsg,
        moduleName: '',
        btnLoading: false,
      });
    }
  };

  handleAddFolder = async (event: any) => {
    if (!this.isEmptyOrSpaces(this.state.moduleName)) {
      let toastMsg = '';
      const name = this.state.moduleName.trim();
      this.setState({ btnLoading: true });
      const newModuleList = await createFolder(event, name, this.state.moduleId);
      if (newModuleList === 'error') {
        toastMsg = translate('d2l.folderaddsuccessdetail', { detail: name });
      } else {
        this.setState({ brightSpaceData: [], loading: true });
        toastMsg = translate('d2l.folderaddsuccessdetail', { detail: name });
      }
      this.closeCreateRenameModel();
      this.setState({
        brightSpaceData: newModuleList === 'error' ? this.state.brightSpaceData : newModuleList,
        loading: false,
        hierarchyLevel: 'UNIT',
        showToast: true,
        toastMessage: toastMsg,
        moduleName: '',
        btnLoading: false,
      });
    }
  };

  handleFolderModal = (moduleId: any, type: any) => {
    this.setState({
      showCreateRenameModel: true,
      moduleName: '',
      isCreateType: type,
      moduleId: moduleId,
    });
  };

  handlePageModal = (moduleId: any) => {
    this.setState({
      showCreateRenameModel: true,
      moduleName: '',
      isCreateType: 'PAGE',
      moduleId: moduleId,
    });
  };

  render() {
    return (
      <>
        <LandingPageHeader btnText='Admin' buttonUrl='/lti/editor/admin' />
        <div className='d-flex justify-content-end help-section top-padding-pos'>
          <p>
            <FontAwesomeIcon icon={faCodeBranch} /> {CONSTANTS.LOREE_APP_VERSION}
          </p>
          <HelpSection space={new Space()} />
        </div>
        <Container fluid className='loree-frame p-10'>
          {this.state.loading ? (
            <Loading />
          ) : (
            <>
              <Row>
                <Col className='mb-3'>
                  <Button
                    type='submit'
                    className='float-right'
                    onClick={() =>
                      this.setState({
                        showCreateRenameModel: true,
                        moduleName: '',
                        isCreateType: 'UNIT',
                      })
                    }
                  >
                    {translate('unit.plusnewunit')}
                  </Button>
                </Col>
              </Row>
              {this.state.d2lItem === 'modules' ? (
                <ModuleLists
                  moduleList={this.state.brightSpaceData}
                  hierarchyLevel={this.state.hierarchyLevel}
                  handlePageModal={this.handlePageModal}
                  handleFolderModal={this.handleFolderModal}
                />
              ) : null}
            </>
          )}
        </Container>
        <div className='d-flex justify-content-end help-section'>{translate('powered')}</div>
        <CreateRenameModules
          show={this.state.showCreateRenameModel}
          hide={this.closeCreateRenameModel}
          isCreateType={this.state.isCreateType}
          saveMod={this.handleCreate}
          moduleName={this.state.moduleName}
          nameHandler={this.moduleNameHandler}
          btnLoad={this.state.btnLoading}
          isEmptyOrSpaces={this.isEmptyOrSpaces}
        />
        <ToastComponent
          showToast={this.state.showToast}
          toastMessage={this.state.toastMessage}
          closeToast={this.closeToast}
        />
      </>
    );
  }
}

export default D2lHome;
