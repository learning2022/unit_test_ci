import React from 'react';
import { shallow } from 'enzyme';
import D2lCreateRenameModules from './d2lCreateRenameModules';

describe('#CreateRenameModal', () => {
  describe('when click rename', () => {
    const renameModuleProps = {
      show: true,
      hide: false,
      isCreate: false,
      saveMod: jest.fn(),
      moduleName: 'test module',
      nameHandler: jest.fn(),
      btnLoad: false,
      isEmptyOrSpaces: jest.fn(),
      isCreateType: undefined,
    };
    const renameModal = shallow(<D2lCreateRenameModules {...renameModuleProps} />);
    test('rename modal is rendered', () => {
      expect(renameModal.find('ModalTitle').text()).toEqual('');
    });
    describe('when click update button', () => {
      test('default event is prevented', () => {
        const preventDefault = jest.fn();
        renameModal.find('Form').at(0).simulate('submit', { preventDefault });
      });
    });
  });
  describe('when click add module', () => {
    const createModuleProps = {
      show: true,
      hide: false,
      isCreate: true,
      saveMod: jest.fn(),
      moduleName: 'test module',
      nameHandler: jest.fn(),
      btnLoad: true,
      isCreateType: undefined,
      isEmptyOrSpaces: jest.fn(),
    };
    const addModal = shallow(<D2lCreateRenameModules {...createModuleProps} />);
    test('add module modal is rendered', () => {
      expect(addModal.find('ModalTitle').text()).toEqual('');
    });
  });
});
