/* eslint-disable */ // Remove this line when editing this file
import React, { Component, Fragment } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

interface CreateRenameProps {
  show: boolean;
  hide: any;
  isCreateType: any;
  saveMod(createType: any, event: any): void;
  moduleName: any;
  nameHandler: any;
  btnLoad: boolean;
  isEmptyOrSpaces(str: any): boolean;
}

class CreateRenameModules extends Component<CreateRenameProps> {
  render() {
    return (
      <Fragment>
        <Modal show={this.props.show} onHide={this.props.hide} backdrop='static' keyboard={false} centered>
          <Modal.Header closeButton>
            <Modal.Title id='create-new-module'>
              <h5 className='text-primary'>
                {this.props.isCreateType === 'UNIT'
                  ? 'Add Unit'
                  : this.props.isCreateType === 'LESSON'
                  ? 'Add New Lesson'
                  : this.props.isCreateType === 'FOLDER'
                  ? 'Add New Folder'
                  : this.props.isCreateType === 'PAGE'
                  ? 'Add New Page'
                  : ''}
              </h5>
            </Modal.Title>
          </Modal.Header>
          <Form onSubmit={event => event.preventDefault()}>
            <Modal.Body>
              <Form.Group controlId='formPageName'>
                <Form.Label>
                  {this.props.isCreateType === 'UNIT'
                    ? 'Unit Name'
                    : this.props.isCreateType === 'LESSON'
                    ? 'Lesson Name'
                    : this.props.isCreateType === 'FOLDER'
                    ? 'Folder Name'
                    : this.props.isCreateType === 'PAGE'
                    ? 'Page Name'
                    : ''}
                </Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder={
                    this.props.isCreateType === 'UNIT'
                      ? 'Unit Name'
                      : this.props.isCreateType === 'LESSON'
                      ? 'Lesson Name'
                      : this.props.isCreateType === 'FOLDER'
                      ? 'Folder Name'
                      : this.props.isCreateType === 'PAGE'
                      ? 'Page Name'
                      : ''
                  }
                  autoComplete='off'
                  maxLength={100}
                  value={this.props.moduleName}
                  onChange={this.props.nameHandler}
                />
              </Form.Group>
            </Modal.Body>
            <Modal.Footer className='d-flex justify-content-center'>
              <Button variant='outline-primary' onClick={this.props.hide} size='sm'>
                Cancel
              </Button>
              <Button
                variant='primary'
                size='sm'
                onClick={e => this.props.saveMod(this.props.isCreateType, e)}
                disabled={this.props.btnLoad || this.props.isEmptyOrSpaces(this.props.moduleName)}
              >
                {this.props.btnLoad ? 'Adding' : 'Add'}
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </Fragment>
    );
  }
}

export default CreateRenameModules;
