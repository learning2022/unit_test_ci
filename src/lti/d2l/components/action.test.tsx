import API from '@aws-amplify/api';
import * as action from '../components/actions';

describe('testcase for getCourseLable functionality', () => {
  let moduleId: string;
  let moduleName: string;
  let clickEvent: MouseEvent;
  beforeEach(() => {
    moduleId = '8905';
    moduleName = 'Loree';
    clickEvent = new MouseEvent('click');
    Object.assign(clickEvent, { preventDefault: jest.fn() });
    Object.assign(clickEvent, { stopPropagation: jest.fn() });
    sessionStorage.setItem('courseLabel', 'LOREE V2');
  });

  test('removespace function called onetimes and remove space in course lable', async () => {
    const spyremovespace = jest.spyOn(action, 'getCourseLabel');
    action.createPage(clickEvent, moduleName, moduleId);
    expect(action.getCourseLabel()).toBe('LOREEV2');
    expect(spyremovespace).toBeCalledTimes(1);
  });
  describe('create page', () => {
    const res1 = {
      data: {
        createD2LSubContent:
          '{"statusCode":200,"body":{"IsExempt":false,"DueDate":null,"Description":{"Text":"","Html":""},"ParentModuleId":34517,"OpenAsExternalResource":null,"TopicType":1,"Url":"/content/enforced/6799-LTI-V2/testpage.html","StartDate":null,"EndDate":null,"IsHidden":true,"IsLocked":false,"ActivityType":1,"ToolId":null,"ToolItemId":null,"GradeItemId":null,"Id":91670,"Title":"testpage*","ShortTitle":"","Type":1,"LastModifiedDate":"2022-02-27T02:28:22.440Z"}}',
      },
    };
    const res2 = {
      data: {
        d2lModules:
          '{"statusCode":200,"body":{"Modules":[{"IsHidden":true,"SortOrder":111,"StartDateTime":null,"EndDateTime":null,"IsLocked":false,"PacingStartDate":null,"PacingEndDate":null,"DefaultPath":"/content/enforced/6799-LTI-V2/","ModuleId":34517,"Title":"Module 1","Modules":[],"Topics":[{"IsExempt":false,"IsHidden":true,"SortOrder":112,"StartDateTime":null,"EndDateTime":null,"IsLocked":false,"IsBroken":false,"ActivityId":"https://ids.brightspace.com/activities/contenttopic/4527DE4C-056A-414A-AEA2-B0AEC6B26387-25129","CompletionType":2,"TopicId":91670,"Identifier":"91670","TypeIdentifier":"File","Title":"testpage*","Bookmarked":false,"Unread":true,"Url":"/content/enforced/6799-LTI-V2/testpage.html","ToolId":null,"ToolItemId":null,"ActivityType":1,"GradeItemId":null,"LastModifiedDate":"2022-02-27T02:28:22.440Z","Description":{"Text":"","Html":""}}],"LastModifiedDate":"2021-01-26T13:09:12.677Z","Description":{"Text":"\\n\\n\\n\\n\\n\\n","Html":"\\n\\n\\n\\n\\n\\n\\n"}}]}}',
      },
    };
    beforeEach(() => {
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => res1)
        .mockImplementationOnce(() => res2);
    });
    test('create page response checking', async () => {
      const data = await action.createPage(clickEvent, moduleName, moduleId);
      expect(data[0].Title).toEqual('Module 1');
    });
    describe('creating page using *', () => {
      test('* is present in title', async () => {
        const data = await action.createPage(clickEvent, moduleName, moduleId);
        expect(data[0].Topics[0].Title).toEqual('testpage*');
      });
      test('* is not present in htmlfile path', async () => {
        const data = await action.createPage(clickEvent, moduleName, moduleId);
        expect(data[0].Topics[0].Url).toEqual('/content/enforced/6799-LTI-V2/testpage.html');
      });
    });
  });
});
