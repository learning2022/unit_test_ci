import { API, graphqlOperation } from 'aws-amplify';
import {
  CreateD2lModulesMutation,
  CreateD2LSubContentMutation,
  D2lModulesQuery,
} from '../../../API';
import { createD2lModules, createD2LSubContent } from '../../../graphql/mutations';
import { d2lModules } from '../../../graphql/queries';

export const AddModule = async (event: Event, moduleName: string, modulesList: _Any) => {
  event.preventDefault();
  event.stopPropagation();
  const courseId = sessionStorage.getItem('course_id');
  const formData = {
    Title: moduleName,
    ShortTitle: moduleName,
    Type: 0,
    ModuleStartDate: null,
    ModuleEndDate: null,
    ModuleDueDate: null,
    IsHidden: true,
    IsLocked: true,
    Description: null,
    Duration: null,
  };
  const createModules = await API.graphql<CreateD2lModulesMutation>(
    graphqlOperation(createD2lModules, {
      courseId: courseId,
      formData: JSON.stringify(formData),
    }),
  );
  const createModulesResponse = JSON.parse(createModules?.data?.createD2lModules as string);
  if (createModulesResponse?.statusCode === 200) {
    if (createModulesResponse?.body.items === undefined) {
      createModulesResponse.body.items = [];
    }
    modulesList.push(createModulesResponse?.body);
    return modulesList;
  } else {
    return 'error';
  }
};

const fetchModuleList = async () => {
  const courseId = sessionStorage.getItem('course_id');
  const modulesResponse = await API.graphql<D2lModulesQuery>(
    graphqlOperation(d2lModules, { courseId: courseId }),
  );
  const modulesData = JSON.parse(modulesResponse?.data?.d2lModules as string);
  return modulesData?.body?.Modules;
};

export const createPage = async (
  event: Event,
  moduleName: string,
  moduleId: string,
): Promise<_Any> => {
  event.preventDefault();
  event.stopPropagation();
  const courseId = sessionStorage.getItem('course_id');
  const courseLabel = getCourseLabel();
  const pageFileName = `/content/enforced/${courseId}-${courseLabel}/${moduleName.replace(
    /[^a-zA-Z0-9]/g,
    '',
  )}.html`;
  const formData = {
    pageDetails: `--xxBOUNDARYxx\r\nContent-Type: application/json\r\n\r\n{"Title":"${moduleName}","ShortTitle":"${moduleName}","Type":1,"TopicType":1,"Url":"${pageFileName}","StartDate":null,"EndDate":null,"DueDate":null,"IsHidden":true,"IsLocked":false,"OpenAsExternalResource":false,"Description":null}\r\n--xxBOUNDARYxx\r\nContent-Disposition: form-data; name:""; filename="${moduleName}.html"\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE html>\n\r\n--xxBOUNDARYxx--`,
  };
  const createSubContent = await API.graphql<CreateD2LSubContentMutation>(
    graphqlOperation(createD2LSubContent, {
      courseId: courseId,
      folderId: moduleId,
      formData: JSON.stringify(formData),
    }),
  );
  const createSubContentResponse = JSON.parse(
    createSubContent?.data?.createD2LSubContent as string,
  );
  if (createSubContentResponse?.statusCode === 200) {
    if (createSubContentResponse?.body.items === undefined) {
      createSubContentResponse.body.items = [];
    }
    return await fetchModuleList();
  } else {
    return 'error';
  }
};
export const getCourseLabel = (): string => {
  const sessionCourseLabel = sessionStorage.getItem('courseLabel') as string;
  return sessionCourseLabel?.replace(/\s/g, '');
};
export const createFolder = async (event: Event, moduleName: string, moduleId: string) => {
  event.preventDefault();
  event.stopPropagation();
  const courseId = sessionStorage.getItem('course_id');
  const formData = {
    Title: moduleName,
    ShortTitle: moduleName,
    Type: 0,
    ModuleStartDate: null,
    ModuleEndDate: null,
    ModuleDueDate: null,
    IsHidden: true,
    IsLocked: true,
    Description: null,
    Duration: null,
  };
  const createSubContent = await API.graphql<CreateD2LSubContentMutation>(
    graphqlOperation(createD2LSubContent, {
      courseId: courseId,
      folderId: moduleId,
      formData: JSON.stringify(formData),
    }),
  );
  const createSubContentResponse = JSON.parse(
    createSubContent?.data?.createD2LSubContent as string,
  );
  if (createSubContentResponse.statusCode === 200) {
    if (createSubContentResponse.body.items === undefined) {
      createSubContentResponse.body.items = [];
    }
    return await fetchModuleList();
  } else {
    return 'error';
  }
};
