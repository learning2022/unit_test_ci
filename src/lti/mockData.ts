export const canvasCourseDetailMock = {
  data: {
    courseDetails:
      '{"statusCode":200,"body":{"id":178,"name":"Deva sandbox-EEE350","account_id":52,"root_account_id":1,"uuid":"YG54DKLiPo9gbACrvgyWrAkxE2Ur89UpgN6SbXZa","start_at":"2022-01-13T13:41:30Z","grading_standard_id":null,"is_public":false,"created_at":"2019-05-06T07:05:57Z","course_code":"Deva sandbox-1","default_view":"wiki","enrollment_term_id":1,"license":"private","grade_passback_setting":null,"end_at":null,"public_syllabus":false,"public_syllabus_to_auth":false,"storage_quota_mb":2500,"is_public_to_auth_users":false,"homeroom_course":false,"course_color":null,"friendly_name":null,"hide_final_grades":false,"apply_assignment_group_weights":true,"account":{"id":52,"name":"Migration Training","workflow_state":"active","parent_account_id":1,"root_account_id":1,"uuid":"UvgT2ZQi7eHXNnGAvzstJL4rzXXGYCs13FdnlXhE","default_storage_quota_mb":2000,"default_user_storage_quota_mb":50,"default_group_storage_quota_mb":2000,"default_time_zone":"Asia/Kolkata","sis_account_id":null,"sis_import_id":null,"integration_id":null,"course_template_id":null},"calendar":{"ics":"https://crystaldelta.instructure.com/feeds/calendars/course_YG54DKLiPo9gbACrvgyWrAkxE2Ur89UpgN6SbXZa.ics"},"time_zone":"Australia/Melbourne","blueprint":false,"template":false,"sis_course_id":null,"sis_import_id":null,"integration_id":null,"enrollments":[{"type":"teacher","role":"TeacherEnrollment","role_id":4,"user_id":242,"enrollment_state":"active","limit_privileges_to_course_section":false}],"workflow_state":"available","restrict_enrollments_to_course_dates":false,"overridden_course_visibility":""}}',
  },
};

export const blackBoardCourseEnrollmentMock = {
  data: {
    courseEnrollment:
      '{"statusCode":200,"body":{"id":"_227_1","userId":"_144_1","courseId":"_19_1","dataSourceId":"_2_1","created":"2021-12-07T10:43:20.833Z","modified":"2021-12-07T10:43:26.610Z","availability":{"available":"Yes"},"courseRoleId":"CourseBuilder","lastAccessed":"2022-05-07T16:51:06.535Z"}}',
  },
};

export const canvasAdminCourseDetailMock = {
  data: {
    courseDetails:
      '{"statusCode":200,"body":{"id":178,"name":"Deva sandbox-EEE350","account_id":52,"root_account_id":1,"uuid":"YG54DKLiPo9gbACrvgyWrAkxE2Ur89UpgN6SbXZa","start_at":"2022-01-13T13:41:30Z","grading_standard_id":null,"is_public":false,"created_at":"2019-05-06T07:05:57Z","course_code":"Deva sandbox-1","default_view":"wiki","enrollment_term_id":1,"license":"private","grade_passback_setting":null,"end_at":null,"public_syllabus":false,"public_syllabus_to_auth":false,"storage_quota_mb":2500,"is_public_to_auth_users":false,"homeroom_course":false,"course_color":null,"friendly_name":null,"hide_final_grades":false,"apply_assignment_group_weights":true,"account":{"id":52,"name":"Migration Training","workflow_state":"active","parent_account_id":1,"root_account_id":1,"uuid":"UvgT2ZQi7eHXNnGAvzstJL4rzXXGYCs13FdnlXhE","default_storage_quota_mb":2000,"default_user_storage_quota_mb":50,"default_group_storage_quota_mb":2000,"default_time_zone":"Asia/Kolkata","sis_account_id":null,"sis_import_id":null,"integration_id":null,"course_template_id":null},"calendar":{"ics":"https://crystaldelta.instructure.com/feeds/calendars/course_YG54DKLiPo9gbACrvgyWrAkxE2Ur89UpgN6SbXZa.ics"},"time_zone":"Australia/Melbourne","blueprint":false,"template":false,"sis_course_id":null,"sis_import_id":null,"integration_id":null,"enrollments":[],"workflow_state":"available","restrict_enrollments_to_course_dates":false,"overridden_course_visibility":""}}',
  },
};

export const blackBoardadminCourseEnrollmentMock = {
  data: {
    courseEnrollment:
      '{"statusCode":404,"body":{"status":404,"message":"The specified object was not found."}}',
  },
};

export const canvasListLmsLoreeRoleMock = {
  data: {
    listLmsLoreeRoles: {
      items: [
        {
          id: 'c6a14985-5f65-45e4-9463-0c8eea45a087',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          loreeRoleID: {
            id: '3c39c5c0-c710-4ffa-a9d8-3de38a55adc7',
            ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
            name: 'Teacher',
            description: null,
            featureList:
              '[{"id":"customrowfiltersearch","type":"Custom Blocks"},{"id":"customelementsfiltersearch","type":"Custom Blocks"},{"id":"templatesfiltersearch","type":"Custom Blocks"},{"id":"saveastemplate","type":"Custom Blocks"},{"id":"saveascustomrow","type":"Custom Blocks"},{"id":"saveascustomelement","type":"Custom Blocks"},{"id":"duplicaterow","type":"Advanced"},{"id":"deleterow","type":"Advanced"},{"id":"changerow","type":"Advanced"},{"id":"marginrow","type":"Advanced"},{"id":"paddingrow","type":"Advanced"},{"id":"backgroundcoloroftherow","type":"Advanced"},{"id":"rowstructure","type":"Advanced"},{"id":"moverow","type":"Advanced"},{"id":"copyandpaste","type":"Advanced"},{"id":"deletecolumn","type":"Advanced"},{"id":"changecolumn","type":"Advanced"},{"id":"margincolumn","type":"Advanced"},{"id":"paddingcolumn","type":"Advanced"},{"id":"backgroundcolorofthecolumn","type":"Advanced"},{"id":"movecolumn","type":"Advanced"},{"id":"duplicateelement","type":"Advanced"},{"id":"deleteelement","type":"Advanced"},{"id":"moveelement","type":"Advanced"},{"id":"image","type":"Advanced"},{"id":"banner","type":"Advanced"},{"id":"uploadimage","type":"Advanced"},{"id":"insertlink","type":"Advanced"},{"id":"imagedesign","type":"Advanced"},{"id":"video","type":"Advanced"},{"id":"uploadnewvideo","type":"Advanced"},{"id":"youtube","type":"Advanced"},{"id":"vimeo","type":"Advanced"},{"id":"insertbyurl","type":"Advanced"},{"id":"videodesign","type":"Advanced"},{"id":"divider","type":"Advanced"},{"id":"dividerdesign","type":"Advanced"},{"id":"table","type":"Advanced"},{"id":"tabledesign","type":"Advanced"},{"id":"specialblocks","type":"Advanced"},{"id":"myinteractive","type":"Interactives"},{"id":"embedurl","type":"Advanced"},{"id":"outline","type":"Basic"},{"id":"undoredo","type":"Basic"},{"id":"codeproperties","type":"Basic"},{"id":"preview","type":"Basic"},{"id":"accessibilitychecker","type":"Basic"},{"id":"textblock","type":"Basic"},{"id":"container","type":"Advanced"},{"id":"navigationMenu","type":"Advanced"},{"id":"h5p","type":"H5P"},{"id":"editorganisationh5p","type":"H5P"},{"id":"deleteorganisationh5p","type":"H5P"},{"id":"fontstyles","type":"Basic"},{"id":"customBlockImage","type":"Custom Blocks performance Improvement"},{"id":"loree-en","label":"English","type":"Languages"},{"id":"loree-zh","label":"Chinese","type":"Languages"}]',
            lmsLoreeRoles: {
              nextToken: null,
            },
            createdAt: '2020-12-01T11:46:21.543Z',
            updatedAt: '2022-04-25T07:05:41.215Z',
          },
          lmsRole: 'Teacher',
          lmsBaseRoleType: 'TeacherEnrollment',
          lmsRoleId: '4',
          createdAt: '2020-12-08T09:15:32.547Z',
          updatedAt: '2020-12-08T09:15:32.547Z',
        },
      ],
      nextToken: null,
    },
  },
};

export const blackBoardListLmsLoreeRoleMock = {
  data: {
    listLmsLoreeRoles: {
      items: [
        {
          id: 'fd986dd3-16d2-4d9a-90c6-39d66e19788e',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: null,
          lmsRole: 'Teaching Assistant',
          lmsBaseRoleType: 'Teaching Assistant',
          lmsRoleId: 'TeachingAssistant',
          createdAt: '2022-03-03T05:06:27.272Z',
          updatedAt: '2022-03-03T05:06:27.272Z',
        },
        {
          id: 'c40823ff-cb55-4a6f-b410-cd15abe97a7c',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: null,
          lmsRole: 'Guest',
          lmsBaseRoleType: 'Guest',
          lmsRoleId: 'Guest',
          createdAt: '2022-03-03T05:06:34.368Z',
          updatedAt: '2022-03-03T05:06:34.368Z',
        },
        {
          id: 'f666ffc5-8cca-4884-90c0-1b7542af1794',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: null,
          lmsRole: 'Student',
          lmsBaseRoleType: 'Student',
          lmsRoleId: 'Student',
          createdAt: '2022-02-24T06:30:03.559Z',
          updatedAt: '2022-02-24T06:30:03.559Z',
        },
        {
          id: '8cf4e83b-336e-4de0-b8fc-5a5af25462e6',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: {
            id: '6607a3ed-be68-430b-8a73-e23944315811',
            ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
            name: 'Course Builder',
            description: null,
            featureList:
              '[{"id":"outline","label":"Outline","type":"Basic"},{"id":"undoredo","label":"Undo/Redo","type":"Basic"},{"id":"codeproperties","label":"Code Properties","type":"Basic"},{"id":"preview","label":"Preview","type":"Basic"},{"id":"accessibilitychecker","label":"Accessibility checker","type":"Basic"},{"id":"textblock","label":"Text Block","type":"Basic"},{"id":"fontstyles","label":"Font styles","type":"Basic"},{"id":"duplicaterow","label":"Duplicate row","type":"Advanced"},{"id":"deleterow","label":"Delete row","type":"Advanced"},{"id":"changerow","label":"Change row","type":"Advanced"},{"id":"marginrow","label":"Margin row","type":"Advanced"},{"id":"paddingrow","label":"Padding row","type":"Advanced"},{"id":"backgroundcoloroftherow","label":"Background color of the row","type":"Advanced"},{"id":"rowstructure","label":"Row structure","type":"Advanced"},{"id":"moverow","label":"Move row","type":"Advanced"},{"id":"copyandpaste","label":"copy and paste","type":"Advanced"},{"id":"deletecolumn","label":"Delete column","type":"Advanced"},{"id":"changecolumn","label":"Change column","type":"Advanced"},{"id":"margincolumn","label":"Margin - column","type":"Advanced"},{"id":"paddingcolumn","label":"Padding - column","type":"Advanced"},{"id":"backgroundcolorofthecolumn","label":"Background color of the column","type":"Advanced"},{"id":"movecolumn","label":"Move column","type":"Advanced"},{"id":"duplicateelement","label":"Duplicate - Element","type":"Advanced"},{"id":"deleteelement","label":"Delete - Element","type":"Advanced"},{"id":"moveelement","label":"Move - Element","type":"Advanced"},{"id":"image","label":"Image","type":"Advanced"},{"id":"banner","label":"Banner","type":"Advanced"},{"id":"uploadimage","label":"Upload image","type":"Advanced"},{"id":"insertlink","label":"Insert Link","type":"Advanced"},{"id":"imagedesign","label":"Image - Design","type":"Advanced"},{"id":"video","label":"Video","type":"Advanced"},{"id":"uploadnewvideo","label":"Upload new video","type":"Advanced"},{"id":"youtube","label":"Youtube","type":"Advanced"},{"id":"vimeo","label":"Vimeo","type":"Advanced"},{"id":"insertbyurl","label":"Insert by URL","type":"Advanced"},{"id":"videodesign","label":"Video - Design","type":"Advanced"},{"id":"divider","label":"Divider","type":"Advanced"},{"id":"dividerdesign","label":"Divider - Design","type":"Advanced"},{"id":"table","label":"Table","type":"Advanced"},{"id":"tabledesign","label":"Table - Design","type":"Advanced"},{"id":"customrowfiltersearch","label":"Custom Row (Filter | Search)","type":"Custom Blocks"},{"id":"customelementsfiltersearch","label":"Custom Elements (Filter | Search)","type":"Custom Blocks"},{"id":"templatesfiltersearch","label":"Templates (Filter | Search)","type":"Custom Blocks"},{"id":"saveastemplate","label":"Save as template","type":"Custom Blocks"},{"id":"saveascustomrow","label":"Save as custom row","type":"Custom Blocks"},{"id":"saveascustomelement","label":"Save as custom element","type":"Custom Blocks"},{"id":"myinteractive","label":"My Interactives","type":"Interactives"},{"id":"h5p","label":"H5p","type":"H5p"},{"id":"imageUpload","label":"Image upload/save enhancement","type":"File upload performance Improvement"},{"id":"language-zh","label":"Chinese","type":"Languages"}]',
            lmsLoreeRoles: {
              nextToken: null,
            },
            createdAt: '2022-05-06T08:54:59.395Z',
            updatedAt: '2022-05-07T09:20:57.028Z',
          },
          lmsRole: 'Course Builder',
          lmsBaseRoleType: 'Course Builder',
          lmsRoleId: 'CourseBuilder',
          createdAt: '2022-02-24T06:30:11.293Z',
          updatedAt: '2022-05-06T08:55:17.333Z',
        },
        {
          id: 'b4422c55-8209-476a-9cdd-cf77719cc7bf',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: null,
          lmsRole: 'Instructor',
          lmsBaseRoleType: 'Instructor',
          lmsRoleId: 'Instructor',
          createdAt: '2022-02-24T06:30:02.472Z',
          updatedAt: '2022-02-24T06:30:02.472Z',
        },
        {
          id: 'dea943ef-0327-4663-a6e5-d2a40ee60a16',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: null,
          lmsRole: 'Grader',
          lmsBaseRoleType: 'Grader',
          lmsRoleId: 'Grader',
          createdAt: '2022-03-03T05:06:32.644Z',
          updatedAt: '2022-03-03T05:06:32.644Z',
        },
        {
          id: 'e5c8520f-d95d-4cae-aac0-3a381db83811',
          ltiPlatformID: '37212122-2c37-462f-8a61-d68632463bd7',
          loreeRoleID: null,
          lmsRole: 'Facilitator',
          lmsBaseRoleType: 'Facilitator',
          lmsRoleId: 'BbFacilitator',
          createdAt: '2022-03-03T05:06:40.718Z',
          updatedAt: '2022-03-03T05:06:40.718Z',
        },
      ],
      nextToken: null,
    },
  },
};

export const loreeFeaturesMock = {
  data: {
    listLoreeFeatures: {
      items: [
        {
          id: '22bccf27-dc8b-45df-bfb6-0b78619b54fc',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          featureList:
            '[{"featureList":[{"id":"outline","label":"Outline"},{"id":"undoredo","label":"Undo/Redo"},{"id":"codeproperties","label":"Code Properties"},{"id":"preview","label":"Preview"},{"id":"accessibilitychecker","label":"Accessibility checker"},{"id":"textblock","label":"Text Block"},{"id":"fontstyles","label":"Font styles"}],"name":"Basic"},{"featureList":[{"id":"duplicaterow","label":"Duplicate row"},{"id":"deleterow","label":"Delete row"},{"id":"changerow","label":"Change row"},{"id":"marginrow","label":"Margin row"},{"id":"paddingrow","label":"Padding row"},{"id":"backgroundcoloroftherow","label":"Background color of the row"},{"id":"rowstructure","label":"Row structure"},{"id":"moverow","label":"Move row"},{"id":"copyandpaste","label":"copy and paste"},{"id":"deletecolumn","label":"Delete column"},{"id":"changecolumn","label":"Change column"},{"id":"margincolumn","label":"Margin - column"},{"id":"paddingcolumn","label":"Padding - column"},{"id":"backgroundcolorofthecolumn","label":"Background color of the column"},{"id":"movecolumn","label":"Move column"},{"id":"duplicateelement","label":"Duplicate - Element"},{"id":"deleteelement","label":"Delete - Element"},{"id":"moveelement","label":"Move - Element"},{"id":"image","label":"Image"},{"id":"banner","label":"Banner"},{"id":"uploadimage","label":"Upload image"},{"id":"insertlink","label":"Insert Link"},{"id":"imagedesign","label":"Image - Design"},{"id":"video","label":"Video"},{"id":"uploadnewvideo","label":"Upload new video"},{"id":"youtube","label":"Youtube"},{"id":"vimeo","label":"Vimeo"},{"id":"insertbyurl","label":"Insert by URL"},{"id":"videodesign","label":"Video - Design"},{"id":"divider","label":"Divider"},{"id":"dividerdesign","label":"Divider - Design"},{"id":"table","label":"Table"},{"id":"tabledesign","label":"Table - Design"},{"id":"specialblocks","label":"Special Blocks"},{"id":"container","label":"Container"},{"id":"embedurl","label":"Embed URL"},{"id":"navigationMenu","label":"Navigation menu"}],"name":"Advanced"},{"featureList":[{"id":"customrowfiltersearch","label":"Custom Row (Filter Search)"},{"id":"customelementsfiltersearch","label":"Custom Elements (Filter Search)"},{"id":"templatesfiltersearch","label":"Templates (Filter Search)"},{"id":"saveastemplate","label":"Save as template"},{"id":"saveascustomrow","label":"Save as custom row"},{"id":"saveascustomelement","label":"Save as custom element"}],"name":"Custom Blocks"},{"featureList":[{"id":"myinteractive","label":"My Interactives"}],"name":"Interactives"},{"featureList":[{"id":"h5p","label":"H5P"},{"id":"editorganisationh5p","label":"Can Edit"},{"id":"deleteorganisationh5p","label":"Can Delete"}],"name":"H5P"},{"featureList":[{"id":"tiptap","label":"Tip-Tap Editor"}],"name":"Tip-Tap"},{"featureList":[{"id":"language-en","label":"English"},{"id":"language-zh","label":"Chinese"},{"id":"language-ja","label":"Japanese"},{"id":"language-nl","label":"Dutch"}],"name":"Languages"},{"featureList":[{"id":"customBlockImage","label":"Image upload/save custom block"}],"name":"Custom Blocks performance Improvement"}]',
          createdAt: '2020-12-01T09:37:59.047Z',
          updatedAt: '2020-12-04T04:53:21.477Z',
        },
      ],
      nextToken: null,
    },
  },
};
