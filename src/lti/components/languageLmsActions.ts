import { API, graphqlOperation } from 'aws-amplify';
import { ListLoreeFeaturesQuery, LmsLoreeRole } from '../../API';
import { listLoreeFeatures } from '../../graphql/queries';
import { getCurrentPlatform, getLmsRoleId, isCanvas } from '../../lmsConfig';
import { LanguageInfo } from '../../loree-editor/interface';
import {
  setBlockTypeAndSelectedBlock,
  createOrUpdateCustomBlock,
} from '../../loree-editor/modules/customBlocks/customBlockEvents';
import { updateEditorContentBeforEditMode } from '../../loree-editor/modules/customBlocks/customBlockHandler';
import { captureErrorLog } from '../../loree-editor/utils';
import {
  fetchPageDetails,
  updateBBEditorContent,
  updateCurrentTemplate,
} from '../../utils/saveContent';

export const savePageToLMS = async () => {
  const pageDetails = fetchPageDetails();
  isCanvas()
    ? await updateEditorContentBeforEditMode(pageDetails)
    : await updateBBEditorContent(
        pageDetails.pageId,
        pageDetails.title ?? '',
        pageDetails.contentType,
      );
};

export const updateCustomBlock = async (
  customBlockType: string,
  customBlockTitle: string,
  customBlockSelectedCategoryId: string,
) => {
  switch (customBlockType) {
    case 'Row':
    case 'Elements':
      setBlockTypeAndSelectedBlock(customBlockType);
      await createOrUpdateCustomBlock(customBlockTitle, customBlockSelectedCategoryId);
      break;
    case 'My Template':
      await updateCurrentTemplate(customBlockTitle, customBlockSelectedCategoryId);
      break;
  }
};

export const fetchLanguagesForAdmin = async () => {
  const { ltiPlatformID } = await getCurrentPlatform();
  try {
    const features = await API.graphql<ListLoreeFeaturesQuery>(
      graphqlOperation(listLoreeFeatures, {
        filter: {
          ltiPlatformID: {
            eq: ltiPlatformID,
          },
        },
      }),
    );
    return (
      features?.data?.listLoreeFeatures?.items
        ?.map((item) => JSON.parse(item?.featureList ?? '[]'))
        .flatMap((nestedData) => nestedData)
        .find(({ name }) => name === 'Languages')?.featureList || []
    );
  } catch (err) {
    captureErrorLog('Fetching data for Language Dropdown', err);
  }
};

export const fetchLanguagesForEnrolledUsers = (
  roleInfo: LmsLoreeRole[],
  courseEnrollmentID: string,
) => {
  return (
    roleInfo
      ?.map((featureCategory) => {
        const featuresList = featureCategory?.loreeRoleID?.featureList;
        const lmsRoleID = getLmsRoleId(featureCategory);
        if (lmsRoleID !== courseEnrollmentID) return [];
        return JSON.parse(featuresList ?? '[]');
      })
      ?.filter((list) => (list.length ? list : null))[0]
      ?.filter((lang: LanguageInfo) => lang?.type === 'Languages') || []
  );
};
