/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import ToastComponent from './ToastComponent';

describe('#Toaster', () => {
  describe('when rendering toaster', () => {
    const ConfirmAlertProps = {
      showToast: true,
      toastMessage: 'Modules created successfully',
      closeToast: jest.fn(),
    };
    const toastAlert = mount(<ToastComponent {...ConfirmAlertProps} />);
    test('displaying toast component', () => {
      expect(toastAlert.find('.toast')).toHaveLength(1);
    });
    test('displaying toaster message', () => {
      console.log(toastAlert.debug());
      expect(toastAlert.find('strong').children().at(0).text()).toEqual('Modules created successfully');
    });
    describe('after onclick close button', () => {
      const closeToast = jest.fn();
      test('close button is triggered', () => {
        toastAlert.find('button').children().at(1).simulate('click', closeToast);
        expect(closeToast.mock.calls.length).toBeCalledWith;
      });
    });
  });

  describe('when not rendering toaster', () => {
    const ConfirmAlertProps = {
      showToast: false,
      toastMessage: '',
      closeToast: jest.fn(),
    };
    const toastAlert = mount(<ToastComponent {...ConfirmAlertProps} />);
    test('not displaying toaster', () => {
      expect(toastAlert.find('.toast')).toHaveLength(0);
    });
  });
});
