import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Dropdown } from 'react-bootstrap';
import { ReactComponent as LanguageIcon } from '../../assets/Icons/_editor/localisation_icon.svg';
import { LmsAccess } from '../../utils/lmsAccess';
import { API, graphqlOperation } from 'aws-amplify';
import { CourseDetailsQuery, CourseEnrollmentQuery, LmsLoreeRole } from '../../API';
import { courseDetails, courseEnrollment } from '../../graphql/queries';
import {
  captureErrorLog,
  getCurrentI18nLanguage,
  setCurrentI18nLanguage,
} from '../../loree-editor/utils';
import { listLMSLoreeRoles } from '../admin/rolesAndFeature/rolesAndFeaturesActions';
import { LanguageProps, LanguageInfo } from '../../loree-editor/interface';
import { translate } from '../../i18n/translate';
import CustomModal from '../../components/customModal/customModal';
import { browserPromptHandler, getEditBlockDetails } from '../../utils/saveContent';
import LmsLoader from '../../views/editor/saveIcon/saveToLmsLoader';
import { isCanvas, isD2l } from '../../lmsConfig';
import { GraphQLResult } from '@aws-amplify/api-graphql';
import {
  fetchLanguagesForAdmin,
  fetchLanguagesForEnrolledUsers,
  savePageToLMS,
  updateCustomBlock,
} from './languageLmsActions';

export const LanguageDropdown = (props: LanguageProps) => {
  const { isEditor, updateLanguageEnableStatus } = props;
  const history = useHistory();
  const isAdmin = sessionStorage.getItem('isAdmin') !== 'false';
  const defaultLanguage = {
    id: 'en',
    label: 'English',
  };
  const [languages, setLanguages] = useState<LanguageInfo[]>([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState<LanguageInfo>(defaultLanguage);
  const [prevSelectedLanguage, setPrevSelectedLanguage] = useState<LanguageInfo>(defaultLanguage);
  const [lmsLoader, setLmsLoader] = useState<boolean>(false);
  const [saveTitle, setSaveTitle] = useState<string>('global.lmsLoaderTitle');
  const mounted = useRef(false);
  const fetchLanguageBasedOnRole = useCallback(
    async (courseData, roleInfo: LmsLoreeRole[]) => {
      const userDefaultLanguage = getCurrentI18nLanguage();
      let availableLangauages = [];
      const isEnrolled =
        isCanvas() && courseData?.body?.enrollments
          ? courseData?.body.enrollments.length
          : courseData.body.courseRoleId;
      if (!isEnrolled) {
        availableLangauages = await fetchLanguagesForAdmin();
      } else {
        const courseEnrollmentID = isCanvas()
          ? courseData?.body.enrollments[0]?.role_id
          : courseData.body.courseRoleId;
        availableLangauages = fetchLanguagesForEnrolledUsers(roleInfo, courseEnrollmentID);
      }
      if (availableLangauages?.length > 0 && mounted.current) {
        const enabledLang = availableLangauages.findIndex(
          (list: LanguageInfo) => list.id.split('-')[0] === userDefaultLanguage,
        );
        enabledLang < 0
          ? setSelectedLanguage({
              id: 'en',
              label: 'English',
            })
          : setSelectedLanguage(availableLangauages[enabledLang]);
        availableLangauages.unshift(availableLangauages?.splice(enabledLang, 1)[0]);
        if (updateLanguageEnableStatus) updateLanguageEnableStatus(true);
      }
      mounted.current && setLanguages(availableLangauages);
    },
    [updateLanguageEnableStatus],
  );

  useEffect(() => {
    mounted.current = true;
    if (isD2l()) {
      mounted.current && setLanguages([]);
      return;
    }
    const canvasCourseSchema = courseDetails as CourseDetailsQuery;
    const blackBoardCourseSchema = courseEnrollment as CourseEnrollmentQuery;
    const currentLms = isCanvas() ? canvasCourseSchema : blackBoardCourseSchema;
    try {
      void Promise.all([
        API.graphql(
          graphqlOperation(currentLms, {
            courseId: sessionStorage.getItem('course_id'),
          }),
        ),
        listLMSLoreeRoles(),
      ]).then(([courseInfo, roleInfo]) => {
        const courseData = isCanvas()
          ? JSON.parse(
              (courseInfo as GraphQLResult<typeof canvasCourseSchema>).data?.courseDetails ??
                'null',
            )
          : JSON.parse(
              (courseInfo as GraphQLResult<typeof blackBoardCourseSchema>).data?.courseEnrollment ??
                'null',
            );
        void fetchLanguageBasedOnRole(courseData, roleInfo as LmsLoreeRole[]);
      });
    } catch (err) {
      captureErrorLog('Fetching data for Language Dropdown', err);
    }
    return () => {
      mounted.current = false;
    };
  }, [fetchLanguageBasedOnRole]);

  const handleSelectedLanguage = (id: string, label: string) => {
    if (id === selectedLanguage.id) return;
    setPrevSelectedLanguage(selectedLanguage);
    if (isEditor) setShowModal(true);
    setSelectedLanguage({
      id: id.split('-')[0],
      label: label,
    });
    if (!isEditor) {
      setCurrentI18nLanguage(id.split('-')[0]);
      history.go(0);
    }
  };

  const handleRefresh = () => {
    const lms = new LmsAccess();
    browserPromptHandler(false);
    setCurrentI18nLanguage(selectedLanguage.id);
    if (isEditor && !lmsLoader) {
      const domainName = sessionStorage.getItem('domainName')?.toLowerCase();
      const redirectUrl = lms.getAccess() ? `/lti/${domainName}/home` : '/dashboard';
      window.location.replace(redirectUrl);
    }
  };

  const handleCancel = () => {
    setShowModal(false);
    setSelectedLanguage(prevSelectedLanguage);
  };

  const handleSaveAndContinue = async () => {
    setLmsLoader(true);
    setShowModal(false);
    const { isCustomBlock, customBlockType, customBlockTitle, customBlockSelectedCategoryId } =
      getEditBlockDetails();
    if (!isCustomBlock) {
      await savePageToLMS();
      setLmsLoader(false);
      handleRefresh();
      return;
    }
    setSaveTitle(
      customBlockType === 'My Template'
        ? 'global.lmsLoaderTemplateTitle'
        : customBlockType === 'Row'
        ? 'global.lmsLoaderRowTitle'
        : 'global.lmsLoaderElementTitle',
    );
    await updateCustomBlock(customBlockType, customBlockTitle, customBlockSelectedCategoryId);
    setLmsLoader(false);
    customBlockType === 'My Template' && handleRefresh();
  };

  const closeSaveModal = () => {
    setLmsLoader(false);
  };

  return (
    <>
      {languages.length > 0 && (
        <Dropdown
          className={`${
            !isEditor && isAdmin
              ? 'ml-auto mt-3 mb-2 mr-0'
              : !isEditor && !isAdmin
              ? 'ml-auto mt-3 mb-2 mr-4'
              : ''
          }`}
          id='loree_navbar_language_dropdown'
        >
          <Dropdown.Toggle className={`p-0 border-0 ${!isEditor ? 'mr-2' : 'mr-n3'}`}>
            <LanguageIcon className={`${isEditor ? 'editorLangIcon' : 'homePageLangIcon'}`} />
            <span className={`${!isEditor ? '' : 'editorLabelAlignment'}`}>
              {translate('global.language')}
            </span>
          </Dropdown.Toggle>
          <Dropdown.Menu
            className={`loree-language-menu ${isEditor ? 'dropdown-menu-editor-alignment' : ''}`}
          >
            {languages?.map((lang: LanguageInfo) => (
              <Dropdown.Item
                key={lang.id}
                className={lang.id === selectedLanguage.id ? 'activeLanguage' : ''}
                onClick={() => handleSelectedLanguage(lang.id, lang.label)}
                disabled={lang.id === selectedLanguage.id}
              >
                {lang.label}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      )}

      {showModal && (
        <CustomModal
          show={showModal}
          modalBody={translate('language.refreshmodalcontent')}
          hide={handleCancel}
          loadingStatus={false}
          modalTitle={translate('global.confirm')}
          showAddOrContinueButton
          showCancelButton
          addOrContinueText={translate('global.continue')}
          saveAndContinueText={translate('global.save&continue')}
          showSaveContinueButton
          events={{
            addOrContinue: handleRefresh,
            saveContinue: handleSaveAndContinue,
          }}
        />
      )}

      <LmsLoader
        manualSaving={lmsLoader}
        closeLoaderModal={closeSaveModal}
        type='LanguageSave'
        action={translate(`${saveTitle}`)}
        bodyContent=''
      />
    </>
  );
};
