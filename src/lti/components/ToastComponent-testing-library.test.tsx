import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import axe from '../../axeHelper';
import ToastComponent from './ToastComponent';

describe('Canvas Toast Component', () => {
  let toast: RenderResult;
  beforeEach(() => {
    toast = render(
      <BrowserRouter>
        <ToastComponent showToast toastMessage='Test toast message' closeToast={jest.fn()} />
      </BrowserRouter>,
    );
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(toast.container);
    expect(results).toHaveNoViolations();
  });

  test('should have role=`alert`', () => {
    const toastElement = toast.getByText('Test toast message');
    expect(toastElement).toHaveAttribute('role', 'alert');
  });
});
