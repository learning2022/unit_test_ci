import React from 'react';
import { render, RenderResult, screen } from '@testing-library/react';
import { LanguageDropdown } from './language';
import { API, Auth } from 'aws-amplify';
import {
  canvasAdminCourseDetailMock,
  blackBoardadminCourseEnrollmentMock,
  blackBoardCourseEnrollmentMock,
  blackBoardListLmsLoreeRoleMock,
  canvasCourseDetailMock,
  canvasListLmsLoreeRoleMock,
  loreeFeaturesMock,
} from '../mockData';

import * as LmsConfig from '../../lmsConfig';

jest.mock('../../i18n/translate.ts', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#LanguageDropdown', () => {
  let dropdownElement: RenderResult;
  beforeEach(() => {
    dropdownElement = render(<LanguageDropdown />);
  });
  test('initial render with no api call has no children', () => {
    expect(dropdownElement.container.children).toHaveLength(0);
  });
});

describe('#LanguageDropdownforCanvasEnrolledUsers', () => {
  let props = true;
  beforeEach(() => {
    localStorage.setItem('loreeLng', 'en');
    sessionStorage.setItem('domainName', 'canvas');

    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasCourseDetailMock)
      .mockImplementationOnce(() => canvasListLmsLoreeRoleMock);
    render(<LanguageDropdown isEditor={props} />);
  });
  test('initial dropdownElement with Language text', () => {
    expect(screen.getByText('global.language')).toBeDefined();
  });
  test('dropdown list toggle', () => {
    screen.getByText('global.language').click();
    expect(screen.getByText('Chinese')).toBeInTheDocument();
  });
  props = false;
  test.skip('modal to be rendered', () => {
    screen.getByText('global.language').click();
    screen.getByText('Chinese').click();
    expect(screen.getByText('global.confirm')).toBeInTheDocument();
    const modalButtons = ['global.cancel', 'global.continue', 'global.save&continue'];
    modalButtons.filter((btn) => expect(screen.getByText(btn)).toBeInTheDocument());
  });
});

describe('#LanguageForCanvasAdminRole', () => {
  beforeAll(() => {
    document.body.innerHTML = '';
  });
  beforeEach(() => {
    localStorage.setItem('loreeLng', 'en');
    sessionStorage.setItem('domainName', 'canvas');
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'deva',
        sub: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    LmsConfig.getCurrentPlatform();
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasAdminCourseDetailMock)
      .mockImplementationOnce(() => canvasListLmsLoreeRoleMock)
      .mockImplementationOnce(() => loreeFeaturesMock);
    render(<LanguageDropdown />);
  });
  test('initial dropdownElement with Language text', () => {
    expect(screen.getByText('global.language')).toBeDefined();
  });
  test('dropdown list with all languages', () => {
    screen.getByText('global.language').click();
    const adminLanguages = ['English', 'Chinese', 'Japanese', 'Dutch'];
    adminLanguages.filter((lang) => expect(screen.getByText(lang)).toBeInTheDocument());
  });
});

describe('#LanguageDropdownforBBEnrolledUsers', () => {
  let props = true;
  beforeEach(() => {
    localStorage.setItem('loreeLng', 'en');
    sessionStorage.setItem('domainName', 'BB');

    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => blackBoardCourseEnrollmentMock)
      .mockImplementationOnce(() => blackBoardListLmsLoreeRoleMock);
    render(<LanguageDropdown isEditor={props} />);
  });
  test('initial dropdownElement with Language text', () => {
    expect(screen.getByText('global.language')).toBeDefined();
  });
  test('dropdown list toggle', () => {
    screen.getByText('global.language').click();
    expect(screen.getByText('Chinese')).toBeInTheDocument();
  });
  props = false;
  test.skip('modal to be rendered', () => {
    screen.getByText('global.language').click();
    screen.getByText('Chinese').click();
    expect(screen.getByText('global.confirm')).toBeInTheDocument();
    const modalButtons = ['global.cancel', 'global.continue', 'global.save&continue'];
    modalButtons.filter((btn) => expect(screen.getByText(btn)).toBeInTheDocument());
  });
});

describe('#LanguageForBBAdmins', () => {
  beforeAll(() => {
    document.body.innerHTML = '';
  });
  beforeEach(() => {
    localStorage.setItem('loreeLng', 'en');
    sessionStorage.setItem('domainName', 'BB');
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'deva',
        sub: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    LmsConfig.getCurrentPlatform();
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => blackBoardadminCourseEnrollmentMock)
      .mockImplementationOnce(() => blackBoardListLmsLoreeRoleMock)
      .mockImplementationOnce(() => loreeFeaturesMock);
    render(<LanguageDropdown />);
  });
  test('initial dropdownElement with Language text', () => {
    expect(screen.getByText('global.language')).toBeDefined();
  });
  test('dropdown list with all languages', () => {
    screen.getByText('global.language').click();
    const adminLanguages = ['English', 'Chinese', 'Japanese', 'Dutch'];
    adminLanguages.filter((lang) => expect(screen.getByText(lang)).toBeInTheDocument());
  });
});

describe('#LanguageDropdownForD2L', () => {
  let dropdownElement: RenderResult;
  beforeEach(() => {
    localStorage.setItem('loreeLng', 'en');
    sessionStorage.setItem('domainName', 'D2l');

    dropdownElement = render(<LanguageDropdown />);
  });
  test('initial render with no api call has no children', () => {
    expect(dropdownElement.container.children).toHaveLength(0);
  });
});
