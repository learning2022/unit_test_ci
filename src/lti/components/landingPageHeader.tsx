import React, { useState } from 'react';
import { Navbar } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import LoreeLogo from '../../assets/images/loree-header-logo.png';
import { LanguageDropdown } from './language';
interface HeaderProps {
  btnText: string;
  buttonUrl: string;
  isAdmin?: boolean;
  isModuleListUpdated?: boolean;
}

const LandingPageHeader = (props: HeaderProps): JSX.Element => {
  const [isLanguageEnabled, setIsLanguageEnabled] = useState(false);
  return (
    <>
      <div role='presentation'>
        <Navbar expand='md' style={{ zIndex: 2 }} className='loreeNavBar fixed-top py-0'>
          <div className='loreeHeaderLogo my-2'>
            <img alt='' src={LoreeLogo} />
          </div>
          {process.env.REACT_APP_ENABLE_LOCALISATION === 'true' && props.isModuleListUpdated && (
            <LanguageDropdown
              isAdmin={props.isAdmin}
              updateLanguageEnableStatus={(status: boolean) => setIsLanguageEnabled(status)}
            />
          )}
          {sessionStorage.getItem('isAdmin') === 'true' && (
            <NavLink
              to={props.buttonUrl}
              className={`text-primary btn btn-light ml-4 mt-1 ${
                !isLanguageEnabled ? 'ml-auto' : ''
              }`}
            >
              {props.btnText}
            </NavLink>
          )}
        </Navbar>
      </div>
    </>
  );
};

const MemoizedLandingPageHeader = React.memo(LandingPageHeader);
export default MemoizedLandingPageHeader;
