import React from 'react';
import { shallow } from 'enzyme';
import Header from './landingPageHeader';

describe('#Header', () => {
  describe('when click admin button', () => {
    const headerProps = {
      btnText: 'Module',
      buttonUrl: '/lti/canvas/home',
    };
    test('header is rendered in Canvas Modules page', () => {
      const header = shallow(<Header {...headerProps} />);
      expect(header.find('NavLink')).toHaveLength(0);
    });
  });
});
