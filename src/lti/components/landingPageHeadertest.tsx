import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import axe from '../../axeHelper';
import Header from './landingPageHeader';
import { translate } from '../../i18n/translate';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));
// // NOTE: There is another testing file 'index.test.tsx' which is using enzyme.
// // Please add test cases created after Nov 2021 here.

describe('Canvas Modules Header', () => {
  let header: RenderResult;
  beforeEach(() => {
    process.env.REACT_APP_ENABLE_LOCALISATION = 'false';
    header = render(
      <BrowserRouter>
        <Header btnText={translate('global.admin')} buttonUrl='/lti/editor/admin' isAdmin />
      </BrowserRouter>,
    );
  });
  test('should pass accessibility tests', async () => {
    const results = await axe(header.container);
    expect(results).toHaveNoViolations();
  });
});
