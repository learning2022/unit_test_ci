import React, { Component, Fragment } from 'react';
import { Toast } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

interface ToastProps {
  showToast: boolean;
  toastMessage: string;
  closeToast: () => void;
}

class ToastComponent extends Component<ToastProps> {
  render() {
    return (
      <>
        <Toast
          onClose={this.props.closeToast}
          show={this.props.showToast}
          delay={3000}
          autohide
          style={{
            position: 'fixed',
            top: '10px',
            left: '40%',
            zIndex: 2,
          }}
          className='loree-toast'
        >
          <Toast.Header data-testid='toast-component'>
            <FontAwesomeIcon icon={faInfoCircle} className='mr-1' />
            <strong role='alert' className='mr-auto'>
              {this.props.toastMessage}
            </strong>
          </Toast.Header>
        </Toast>
      </>
    );
  }
}

export default ToastComponent;
