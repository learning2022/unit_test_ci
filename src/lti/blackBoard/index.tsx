/* eslint-disable */ // Remove this line when editing this file
import React, { useEffect, useRef, useState } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import Loading from '../../components/loader/loading';
import { viewBbContents, viewBbCourses } from '../../graphql/queries';
import LandingPage from './landingPage';

const BBHome = () => {
  const [loading, setLoading] = useState(true);
  const [course_id, setCourse_id] = useState('');
  const [bbContentList, setBBContentList] = useState([]);
  const [initialContent, setInitialContent] = useState({});
  const initialRender = useRef(true);

  useEffect(() => {
    async function getContentList() {
      const course_id = sessionStorage.getItem('course_id');
      setCourse_id(sessionStorage.getItem('course_id') as string);
      const courseData: any = await API.graphql(
        graphqlOperation(viewBbCourses, {
          courseId: course_id,
        }),
      );
      sessionStorage.setItem('BBCourseStatus', JSON.parse(courseData.data.viewBbCourses).body.ultraStatus);
      const response: any = await API.graphql(
        graphqlOperation(viewBbContents, {
          courseId: course_id,
        }),
      );
      setBBContentList(JSON.parse(response.data.viewBbContents).body.results);
    }
    getContentList();
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false;
    } else {
      if (bbContentList.length) {
        setInitialContent({ id: bbContentList[0]['id'], title: bbContentList[0]['title'] });
      } else {
        setInitialContent([]);
      }
      setLoading(false);
    }
    //eslint-disable-next-line
  }, [bbContentList]);

  return (
    <React.Fragment>
      {loading ? (
        <Loading />
      ) : (
        <LandingPage initialContent={initialContent} sessionData={{course_id }} content={bbContentList} />
      )}
    </React.Fragment>
  );
};

export default BBHome;
