/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Card, Col, Dropdown, Row } from 'react-bootstrap';
import ReactHtmlParser from 'react-html-parser';
import { API, graphqlOperation } from 'aws-amplify';
import { viewBbContentsById } from '../../../graphql/queries';
import { ReactComponent as CreateProject } from '../../../assets/Icons/create_project.svg';
import { ReactComponent as Exclusion } from '../../../assets/Icons/Exclusion 5.svg';
import { ReactComponent as Union } from '../../../assets/Icons/Union 6.svg';
import editIcon from '../../../assets/images/edit.svg';
import Loading from '../../../components/loader/loading';
import { translate } from '../../../i18n/translate';
import { ContentType } from './updateModal';

const CourseContent = (props: _Any) => {
  const history = useHistory();
  const folder = 'resource/x-bb-folder';
  const item = 'resource/x-bb-document';

  const handleContentData = (data: _Any) => {
    if (data.contentHandler.id === 'resource/x-bb-module-page') props.setIsModule(true);
    if (data.contentHandler.id !== 'resource/x-bb-module-page') props.setIsModule(false);
    props.course(data);
    props.setActivePath(data.id);
  };

  const handleUltraDocument = async (contentId: string, contentTitle: string) => {
    const response: _Any = await API.graphql(
      graphqlOperation(viewBbContentsById, {
        courseId: sessionStorage.getItem('course_id'),
        contentId: contentId,
      }),
    );
    history.push(
      `/lti/editor/${
        JSON.parse(response.data.viewBbContentsById).body.results[0].id
      }/page/${encodeURIComponent(contentTitle)}`,
    );
  };

  return (
    <div className='course-content-wrapper'>
      <nav aria-label={translate('course.breadcrumb_aria')}>
        <ol className='breadcrumb pl-0'>
          {props.path.length > 0 &&
            props.path.map((data: _Any, idx: _Any) => (
              <li className='breadcrumb-item' key={idx}>
                <span
                  onClick={() => props.course(data)}
                  className={
                    data.id === props.activePath || props.path.length < 2 ? 'active-path' : ''
                  }
                >
                  {ReactHtmlParser(data.title)}
                </span>
              </li>
            ))}
        </ol>
      </nav>
      {props.content !== '' && !props.isModule ? (
        <Dropdown id='BB-Add-Content'>
          <Dropdown.Toggle variant='primary' className='add-button btn-sm'>
            {translate('course.plusadd')}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <ul className='p-0 m-0'>
              <li
                className='px-3 py-1'
                onClick={() => props.handleModalSettings(ContentType.Folder)}
              >
                {translate('course.folder')}
              </li>
              {/* <li className='px-3 py-1' onClick={() => props.handleModalSettings('Module')}>
              Module
            </li> */}
              <li className='px-3 py-1' onClick={() => props.handleModalSettings(ContentType.Item)}>
                {translate('global.item')}
              </li>
            </ul>
          </Dropdown.Menu>
        </Dropdown>
      ) : (
        ''
      )}
      <div className='bb-landing-course-list pr-2 mt-3 overflow-auto'>
        {props.load ? (
          <Loading />
        ) : props.data !== undefined && props.data.length > 0 ? (
          props.data.map(
            (data: _Any, index: number) =>
              data.contentHandler && (
                <Card className='mt-3 border-bottom-0' key={index}>
                  <Card.Header
                    className={
                      data.contentHandler.id === folder && data.contentHandler.isBbPage
                        ? 'card-border-page'
                        : data.contentHandler.id === folder
                        ? 'card-border-folder'
                        : data.contentHandler.id === item
                        ? 'card-border-page'
                        : 'card-border-module'
                    }
                  >
                    <Row>
                      <Col>
                        {data.contentHandler.id === folder && data.contentHandler.isBbPage ? (
                          <span
                            onClick={() => {
                              void handleUltraDocument(data.id, data.title);
                            }}
                          >
                            <Exclusion width={22} height={24} className='mb-1 mr-2' />
                            {ReactHtmlParser(data.title)}
                          </span>
                        ) : data.contentHandler.id === folder ? (
                          <span onClick={() => handleContentData(data)}>
                            <CreateProject width={23} height={24} className='mb-1 mr-2' />
                            {ReactHtmlParser(data.title)}
                          </span>
                        ) : data.contentHandler.id === item ? (
                          <span>
                            <Link
                              to={`/lti/editor/${data.id}/page/${encodeURIComponent(data.title)}`}
                            >
                              <Exclusion width={22} height={24} className='mb-1 mr-2' />
                              {ReactHtmlParser(data.title)}
                            </Link>
                          </span>
                        ) : (
                          <span>
                            <Union width={22} height={22} className='mb-1 mr-2' />
                            {ReactHtmlParser(data.title)}
                          </span>
                        )}
                      </Col>
                      <Col className='d-flex flex-row-reverse'>
                        {sessionStorage.getItem('BBCourseStatus') === 'Ultra' ? (
                          data.contentHandler.id === folder &&
                          data.contentHandler.isBbPage && (
                            <img
                              src={editIcon}
                              alt={translate('global.edit')}
                              title={translate('global.edit')}
                              onClick={() => {
                                void handleUltraDocument(data.id, data.title);
                              }}
                              style={{ width: '15px' }}
                            />
                          )
                        ) : data.contentHandler.id === folder || data.contentHandler.id === item ? (
                          <Link
                            to={`/lti/editor/${data.id}/page/${encodeURIComponent(data.title)}`}
                          >
                            <img
                              src={editIcon}
                              alt={translate('global.edit')}
                              title={translate('global.edit')}
                              style={{ width: '15px' }}
                            />{' '}
                          </Link>
                        ) : (
                          ''
                        )}
                      </Col>
                    </Row>
                  </Card.Header>
                </Card>
              ),
          )
        ) : (
          <span className='font-italic d-flex justify-content-center' style={{ color: '#b7b5b5' }}>
            {translate('course.itstime')}
          </span>
        )}
      </div>
    </div>
  );
};

export default CourseContent;
