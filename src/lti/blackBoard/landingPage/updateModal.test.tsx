import React from 'react';
import { configure, fireEvent, render, screen } from '@testing-library/react';
import UpdateModal, { ModalType } from './updateModal';
configure({ testIdAttribute: 'class' });

describe('#Updatemodal', () => {
  describe('show modal false', () => {
    beforeEach(() => {
      const props = {
        show: false,
        modalType: ModalType.AddContentArea,
        setShow: jest.fn(),
        submitHandler: jest.fn(),
      };
      render(<UpdateModal content={[]} courseData={[]} {...props} />);
    });
    test('without modal render', () => {
      expect(screen.queryAllByTestId('modal-open').length).toBe(0);
    });
  });
  describe('show modal true', () => {
    beforeEach(() => {
      const props = {
        show: true,
        modalType: ModalType.AddContentArea,
        setShow: jest.fn(),
        submitHandler: jest.fn(),
      };
      render(<UpdateModal content={[]} courseData={[]} {...props} />);
    });
    test('with modal render', () => {
      screen.debug();
      expect(screen.queryAllByTestId('modal').length).toBe(1);
    });
    test('with modal header class', () => {
      expect(screen.queryAllByTestId('modal-header').length).toEqual(1);
    });
    test('input value check', () => {
      function hasInputValue(e: _Any, inputValue: string) {
        return screen.getByDisplayValue(inputValue) === e;
      }
      const input = screen.queryAllByTestId('form-control')[0] as HTMLInputElement;
      fireEvent.change(input, { target: { value: 'loree-area' } });
      expect(hasInputValue(input, 'loree-area')).toBe(true);
    });
    test('getting input value and submit button', () => {
      const deviceNameInput = screen.queryAllByTestId('form-control')[0] as HTMLInputElement;
      fireEvent.change(deviceNameInput, { target: { value: 'loree-area' } });
      fireEvent.click(screen.getByText(/submit/i));
    });
  });
});
