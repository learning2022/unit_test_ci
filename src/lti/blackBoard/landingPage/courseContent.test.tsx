import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { configure, fireEvent, render, screen } from '@testing-library/react';
import CourseContent from './courseContent';
configure({ testIdAttribute: 'class' });

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#coursecontent', () => {
  describe('#inital state', () => {
    beforeEach(() => {
      const props = {
        load: true,
        path: [{ id: '1' }],
      };
      render(<CourseContent {...props} />);
    });
    test('loading screen', () => {
      expect(screen.getByTitle('Loading, please wait')).toBeInTheDocument();
    });
  });
  describe('#after loading', () => {
    beforeEach(() => {
      const props = {
        load: false,
        path: [{ id: '1' }],
        data: [
          { id: '1', title: 'hello title', contentHandler: { id: 'resource/x-bb-document' } },
          { id: '2', title: 'hello title2', contentHandler: { id: 'resource/x-bb-folder' } },
          { id: '3', title: 'hello title3', contentHandler: { id: '' } },
        ],
        isModule: false,
      };
      render(
        <BrowserRouter>
          <CourseContent {...props} />
        </BrowserRouter>,
      );
    });
    test('Add button', async () => {
      expect(screen.getByRole('button')).toHaveTextContent('course.plusadd');
    });
    test('document type tile', async () => {
      expect(screen.queryAllByTestId('col')[0].innerHTML).toEqual(
        `<span><a href="/lti/editor/1/page/hello title"><svg width="22" height="24" class="mb-1 mr-2">Exclusion 5.svg</svg>hello title</a></span>`,
      );
    });
    test('folder type tile', async () => {
      expect(screen.queryAllByTestId('col')[1].innerHTML).toEqual(
        `<span><svg width="23" height="24" class="mb-1 mr-2">create_project.svg</svg>hello title2</span>`,
      );
    });
    test('content list is rendered', () => {
      expect(screen.queryAllByTestId('col')).toHaveLength(3);
    });
    test('edit icon', () => {
      const eventData = screen.getAllByTitle('global.edit');
      fireEvent.click(eventData[0]);
      expect(eventData).toHaveLength(2);
    });
  });
});
