/* eslint-disable */ // Remove this line when editing this file
import CONSTANTS from '../../../loree-editor/constant';
import { API, graphqlOperation } from 'aws-amplify';
import { v4 as uuidv4 } from 'uuid';
import { uploadBbUltraFileToS3, updateUltraContent, createBbChildContent } from '../../../graphql/mutations';

//For updating html content into s3
export const ultraContentUpdate = async (contentId: string, title: string, editorContent: any) => {
  let contentUrl = '';
  const response: any = await API.graphql(
    graphqlOperation(uploadBbUltraFileToS3, {
      platformId: sessionStorage.getItem('ltiPlatformId'),
      file: `<html><head><meta name="viewport" content="width=device-width, initial-scale=1"><script src="${CONSTANTS.LOREE_JS_URL}"></script><script class="bb-embedded-html bb-auto-resize" type="text/javascript">
      window.addEventListener('load', () => {
        var height = document.documentElement.scrollHeight;
        var source = new URL(document.referrer).origin;
        var w = window;
        while (w !== top) {
          w = w.parent;
          w.postMessage({ type: 'bb-auto-resize', height }, source);
        }
      });
    </script><link rel="stylesheet" media="all" href="${CONSTANTS.LOREE_CSS_URL}"></head>${editorContent}</html>`,
      title: `${contentId}/${title}.html`,
      newTitle: `${contentId}/${title}.html`,
    }),
  );
  contentUrl = response.data.uploadBBUltraFileToS3;
  return contentUrl;
};

// For updating content url to BB Content
export const UpdateS3SourceBody = async (fileName: string, fileUrl: string, pageId: string) => {
  const course_id = sessionStorage.getItem('course_id');
  const updateData = {
    fileName: `${fileName}.html`,
    fileUrl: fileUrl,
    uuid: uuidv4(),
  };
  const createContBody = await API.graphql(
    graphqlOperation(updateUltraContent, {
      courseId: course_id,
      contentId: pageId,
      updateContentData: JSON.stringify(updateData),
    }),
  );
  return createContBody;
};

// For fetching content from s3 url
export const ultraContentFetch = async (contentUrl: any) => {
  return await fetch(contentUrl)
    .then(async res => {
      return await res.text();
    })
    .then(function (html: any) {
      return html;
    })
    .catch(function (err) {
      console.log('Failed to fetch page: ', err);
    });
};

export const handleUltraPageCreation = async (contentId: string, title: string) => {
  const course_id = sessionStorage.getItem('course_id');
  const content = {
    title: 'ultraDocumentBody',
    contentHandler: { id: 'resource/x-bb-document' },
    body: '',
  };
  const resp: any = await API.graphql(
    graphqlOperation(createBbChildContent, {
      courseId: course_id,
      contentId: contentId,
      contentData: JSON.stringify(content),
    }),
  );
  const fileUrl: any = await ultraContentUpdate(JSON.parse(resp.data.createBbChildContent).body.id, title, '');
  await UpdateS3SourceBody(title, fileUrl, JSON.parse(resp.data.createBbChildContent).body.id);
  return JSON.parse(resp.data.createBbChildContent).body.id;
};
