import React from 'react';
import { mount } from 'enzyme';
import ContentArea from './contentArea';
const samplePropData = [{ title: 'sample title', contentHandler: { isBbPage: true } }];
const onShowAddContentModal = jest.fn();
const setActiveArea = jest.fn();

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('blackboard sidenav content', () => {
  let bbsideNav: any;
  let bbSideNavWrap: any;
  beforeEach(async () => {
    bbsideNav = mount(
      <ContentArea
        content={samplePropData}
        active={{ activeArea: 'sample title', setActiveArea }}
        handleModalSettings={onShowAddContentModal}
        isModule
      />,
    );
    bbSideNavWrap = bbsideNav.find('.profile-side-nav-wrapper');
  });
  test('Verifying bb side nav is rendered or not', () => {
    expect(bbSideNavWrap).not.toBeNull();
  });
  test('Verifying bb side nav children is rendered or not', () => {
    expect(bbSideNavWrap.children().length).toBe(5);
  });
  test('Verifying Content area anchor block is rendered or not', () => {
    expect(bbSideNavWrap.children().at(2).html()).toEqual(
      `<a href="#" class="loree-nav-brand text-center mb-2 mb-md-4 navbar-brand"><h4 class="mx-1 text-primary">content.contentarea <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus-circle" class="svg-inline--fa fa-plus-circle ml-1 fa-sm fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path></svg></h4><hr class="mx-2 mt-3"></a>`,
    );
  });
  test('Verifying side nav list has the class or not', () => {
    expect(bbSideNavWrap.children().at(3).prop('className')).toContain('side-nav-link-wrapper');
  });
  test('Verifying Powered By text is rendered or not', () => {
    expect(bbSideNavWrap.children().at(4).prop('className')).toContain('bb-poweredBy-section');
    // expect(bbSideNavWrap.children().at(4).text()).toContain('powered | A2202.6');
  });
});
