import React, { useEffect, useState } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { Container, Col, Row } from 'react-bootstrap';
import { viewBbContentsById } from '../../../graphql/queries';
import { createBbChildContent, createBbRootContent } from '../../../graphql/mutations';
import ContentArea from './contentArea';
import CourseContent from './courseContent';
import UpdateModal, { ModalType, ContentType } from './updateModal';
import { handleUltraPageCreation } from './bbUltra';
import '../blackboard.scss';
import { translate } from '../../../i18n/translate';
import LandingPageHeader from '../../components/landingPageHeader';

const LandingPage = (props: _Any) => {
  const [contentId, setContentId] = useState(props.initialContent.id);
  const [activeArea, setActiveArea] = useState(
    props.content.length > 0 ? props.initialContent.title : '',
  );
  const [contentList, setContentList] = useState(props.content);
  const [courseData, setCourseData] = useState([] as _Any);
  const [folderPath, setFolderPath] = useState([props.initialContent] as _Any);
  const [loading, setLoading] = useState(true);
  const [activeBreadcrumb, setActiveBreadcrumb] = useState('');
  const [modalType, setModalType] = useState(ModalType.None);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [isModule, setIsModule] = useState(false);

  useEffect(() => {
    const handleCourseId = async () => {
      const response: _Any = await API.graphql(
        graphqlOperation(viewBbContentsById, {
          courseId: props.sessionData.course_id,
          contentId: contentId,
        }),
      );
      setCourseData(JSON.parse(response.data.viewBbContentsById).body.results);
      setLoading(false);
    };
    void handleCourseId();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contentId]);

  const handleCourseDetails = (data: _Any) => {
    setLoading(true);
    setContentId(data.id);
    if (folderPath.length > 1) {
      for (let i = 0; i < folderPath.length; i++) {
        if (folderPath[i].title === data.title) {
          folderPath.length = i + 1;
          setFolderPath(folderPath);
          setLoading(false);
          return;
        }
      }
    }
    const isPathExists = folderPath.some(function (el: _Any) {
      return el.title === data.title;
    });
    if (!isPathExists) setFolderPath([...folderPath, data]);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  };

  const onShowAddContentAreaModal = () => {
    setModalType(ModalType.AddContentArea);
    setShowUpdateModal(true);
  };

  const onShowAddContentModal = (contentType: ContentType) => {
    if (contentType === ContentType.Folder) {
      setModalType(ModalType.AddFolder);
      setShowUpdateModal(true);
    }
    if (contentType === ContentType.Item) {
      setModalType(ModalType.AddItem);
      setShowUpdateModal(true);
    }
  };

  const getCreateContentPath = (modalType: ModalType) => {
    if (modalType === ModalType.AddFolder) {
      return 'resource/x-bb-folder';
    }
    if (modalType === ModalType.AddItem) {
      return 'resource/x-bb-document';
    }
    return 'resource/x-bb-module-page';
  };

  const updateModalSubmitHandler = (modalType: ModalType, name: string) => {
    if (modalType === ModalType.AddContentArea) {
      void handleCreateContentArea(name);
    } else {
      void handleCreateCourseData(modalType, name);
    }
  };

  const handleCreateContentArea = async (name: string) => {
    const content = {
      title: name,
      contentHandler: {
        id: 'resource/x-bb-folder',
      },
    };
    const response: _Any = await API.graphql(
      graphqlOperation(createBbRootContent, {
        courseId: props.sessionData.course_id,
        contentId: contentId,
        contentData: JSON.stringify(content),
      }),
    );
    setContentList([...contentList, JSON.parse(response.data.createBbRootContent).body]);
    setShowUpdateModal(false);
  };

  const handleCreateCourseData = async (modalType: ModalType, name: string) => {
    const courseType = sessionStorage.getItem('BBCourseStatus');
    let contentHandlerData;
    courseType === 'Ultra'
      ? (contentHandlerData = {
          id:
            modalType === ModalType.AddFolder || modalType === ModalType.AddItem
              ? 'resource/x-bb-folder'
              : 'resource/x-bb-module-page',
          isBbPage: modalType === ModalType.AddItem,
        })
      : (contentHandlerData = {
          id: getCreateContentPath(modalType),
        });
    const content = {
      title: name,
      contentHandler: contentHandlerData,
    };
    const response: _Any = await API.graphql(
      graphqlOperation(createBbChildContent, {
        courseId: props.sessionData.course_id,
        contentId: contentId,
        contentData: JSON.stringify(content),
      }),
    );
    courseType === 'Ultra' &&
      handleUltraPageCreation(
        JSON.parse(response.data.createBbChildContent).body.id,
        JSON.parse(response.data.createBbChildContent).body.title,
      );
    const result = JSON.parse(response.data.createBbChildContent).body;
    setCourseData([...courseData, result]);
    setShowUpdateModal(false);
  };

  return (
    <>
      <LandingPageHeader btnText={translate('global.admin')} buttonUrl='/lti/editor/admin' />
      <ContentArea
        content={contentList}
        path={setFolderPath}
        active={{ activeArea, setActiveArea }}
        course={{ setLoading, setContentId }}
        handleModalSettings={onShowAddContentAreaModal}
        isModule={setIsModule}
      />
      <Container fluid className='recent-section'>
        <Row>
          <Col>
            <CourseContent
              load={loading}
              data={courseData}
              path={folderPath}
              course={handleCourseDetails}
              handleModalSettings={onShowAddContentModal}
              activePath={activeBreadcrumb}
              setActivePath={setActiveBreadcrumb}
              content={activeArea}
              isModule={isModule}
              setIsModule={setIsModule}
            />
          </Col>
        </Row>
        <UpdateModal
          content={contentList}
          courseData={courseData}
          show={showUpdateModal}
          setShow={setShowUpdateModal}
          modalType={modalType}
          submitHandler={updateModalSubmitHandler}
        />
      </Container>
    </>
  );
};

export default LandingPage;
