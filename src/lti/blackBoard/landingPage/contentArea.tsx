/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import { useHistory } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import ReactHtmlParser from 'react-html-parser';
import { Navbar, Nav } from 'react-bootstrap';
import { viewBbContentsById } from '../../../graphql/queries';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import editIcon from '../../../assets/images/edit.svg';
import CONSTANTS from '../../../loree-editor/constant';
import { handleUltraPageCreation } from './bbUltra';
import '../blackboard.scss';
import HelpSection from '../../helpSection';
import Space from '../../../loree-editor/modules/space/space';
import { translate } from '../../../i18n/translate';
import { ModalType } from './updateModal';

interface BBContentData {
  id: string;
  title: string;
  contentHandler: Record<string, string>;
}

const ContentArea: React.FC<_Any> = (props: _Any): JSX.Element => {
  const history = useHistory();
  const handleContentData = (data: { id: string; title: string }) => {
    props.course.setLoading(true);
    props.course.setContentId(data.id);
    props.isModule(false);
    props.path([]);
    props.path([data]);
    props.active.setActiveArea(data.title);
  };

  const handleUltraDocument = async (contentId: string, contentTitle: string) => {
    const response: _Any = await API.graphql(
      graphqlOperation(viewBbContentsById, {
        courseId: sessionStorage.getItem('course_id'),
        contentId: contentId,
      }),
    );
    if (JSON.parse(response.data.viewBbContentsById).body.results[0]) {
      history.push(
        `/lti/editor/${
          JSON.parse(response.data.viewBbContentsById).body.results[0].id
        }/page/${encodeURIComponent(contentTitle)}`,
      );
    } else {
      void handleUltraPageCreation(contentId, contentTitle);
      const childContentResponse: _Any = await API.graphql(
        graphqlOperation(viewBbContentsById, {
          courseId: sessionStorage.getItem('course_id'),
          contentId: contentId,
        }),
      );
      history.push(
        `/lti/editor/${
          JSON.parse(childContentResponse.data.viewBbContentsById).body.results[0].id
        }/page/${encodeURIComponent(contentTitle)}`,
      );
    }
  };

  const handleData = async (data: _Any) => {
    data?.contentHandler?.isBbPage
      ? await handleUltraDocument(data.id, data.title)
      : handleContentData(data);
  };
  return (
    <Navbar
      className={`profile-side-nav-wrapper bb-nav-section p-0 ${
        !props.visible ? 'd-none d-md-flex' : 'd-md-flex'
      }`}
      style={{ top: '73px', minWidth: '200px' }}
    >
      <span onClick={props.onClick} className='d-md-none fa-1x fa-fw align-self-end'>
        <FontAwesomeIcon icon={faWindowClose} />
      </span>
      <Navbar.Brand href='#' className='loree-nav-brand text-center mb-2 mb-md-4'>
        <h4 className='mx-1 text-primary'>
          {translate('content.contentarea')}{' '}
          <FontAwesomeIcon
            className='ml-1 fa-sm fa-w-16'
            icon={faPlusCircle}
            onClick={() => props.handleModalSettings(ModalType.AddContentArea)}
          />
        </h4>
        <hr className='mx-2 mt-3' />
      </Navbar.Brand>
      <Nav className='side-nav-link-wrapper d-flex flex-column ml-1'>
        {props.content.length
          ? props.content.map((data: BBContentData, index: number) => (
              <Nav.Item className='my-2' key={index} onClick={async () => await handleData(data)}>
                <Nav.Link
                  className={props.active.activeArea === data.title ? 'active-area' : ''}
                  style={{ color: props.active.activeArea === data.title ? '#000d9c' : '#000' }}
                >
                  <span className='text-truncate bb-side-nav-title mr-2 d-inline-block'>
                    {ReactHtmlParser(data.title)}
                  </span>
                  {data.contentHandler?.isBbPage && (
                    <img
                      src={editIcon}
                      alt='edit'
                      title={translate('global.edit')}
                      style={{ width: '17px' }}
                      className='float-right'
                    />
                  )}
                </Nav.Link>
              </Nav.Item>
            ))
          : ''}
      </Nav>
      <p className='bb-poweredBy-section mt-auto ml-2'>
        {translate('powered')} | {CONSTANTS.LOREE_BB_APP_VERSION}{' '}
        <HelpSection space={new Space()} />
      </p>
    </Navbar>
  );
};

export default ContentArea;
