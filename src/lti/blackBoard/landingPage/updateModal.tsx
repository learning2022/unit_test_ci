import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { useLoreeTranslation } from '../../../i18n/translate';

export enum ModalType {
  AddContentArea = 0,
  Rename = 1,
  AddFolder = 2,
  AddItem = 3,
  None = 4,
}

export enum ContentType {
  Folder = 0,
  Item = 1,
}
interface UpdateModalProps {
  show: boolean;
  modalType: ModalType;
  setShow: (value: boolean) => void;
  submitHandler: (type: ModalType, name: string) => void;
  content: [];
  courseData: [];
}

const UpdateModal = (props: UpdateModalProps) => {
  const [name, setName] = useState('');
  const [error, setError] = useState('');
  const [btnStatus, setBtnStatus] = useState(true);

  const { t: translate } = useLoreeTranslation();

  useEffect(() => {
    if (!props.show) {
      setName('');
      setError('');
      setBtnStatus(true);
    }
  }, [props.show]);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (error !== '') setError('');
    setBtnStatus(true);
    setName(e.target.value.trim());
    if (e.target.value.trim() !== '') {
      setBtnStatus(false);
    }
  };

  const handleSubmit = (props: UpdateModalProps) => {
    setBtnStatus(true);
    if (name === '') {
      setError(translate('error.cannotbeempty'));
      return;
    }
    if (name !== '') {
      if (props.modalType === ModalType.AddContentArea) {
        if (isNameExist(props.content)) props.submitHandler(props.modalType, name);
        else setError(translate('error.nametaken'));
      } else {
        if (isNameExist(props.courseData)) props.submitHandler(props.modalType, name);
        else setError(translate('error.nametaken'));
      }
    }
  };

  const getModalTypeTitle = (inType: ModalType) => {
    if (inType === ModalType.Rename) {
      return translate('global.rename');
    }
    if (inType === ModalType.AddContentArea) {
      return translate('course.addcontentarea');
    }
    return translate('global.add');
  };

  const getModalTypeSubmitLabel = (inType: ModalType) => {
    if (inType === ModalType.Rename) {
      return translate('global.save');
    }
    if (inType === ModalType.AddContentArea) {
      return translate('global.submit');
    }
    return translate('global.add');
  };

  const isNameExist = (arr: []) => {
    if (arr.length !== undefined) {
      const found = arr.some((el: { title: string }) => el.title === name);
      if (!found) {
        return true;
      } else {
        return false;
      }
    } else return true;
  };
  return (
    <>
      <Modal
        show={props.show}
        centered
        backdrop='static'
        onHide={() => props.setShow(false)}
        keyboard={false}
        animation={false}
      >
        <Modal.Header closeButton>
          <Modal.Title className='text-primary font-weight-bold'>
            <h5 className='text-primary'>{getModalTypeTitle(props.modalType)}</h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='pb-0'>
          <Form>
            <Form.Group>
              <Form.Control
                className={error ? 'border-danger' : ''}
                type='text'
                placeholder={
                  props.modalType !== ModalType.Rename
                    ? translate('course.typenamedetail', {
                        detail: getModalTypeTitle(props.modalType),
                      })
                    : translate('course.entername')
                }
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeHandler(e)}
              />
              {error !== '' ? <small className='text-danger ml-1'>{error}</small> : ''}
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-center'>
          <Button
            variant='outline-primary'
            size='sm'
            className='px-3 rounded-3'
            onClick={() => props.setShow(false)}
          >
            {translate('global.cancel')}
          </Button>
          <Button
            variant='primary'
            size='sm'
            className='px-3 rounded-3'
            onClick={() => handleSubmit(props)}
            disabled={btnStatus}
          >
            {getModalTypeSubmitLabel(props.modalType)}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default UpdateModal;
