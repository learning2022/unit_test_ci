/* eslint-disable @typescript-eslint/no-explicit-any */

import { useEffect } from 'react';
import History from 'history';

interface UnauthProps {
  match: {
    path: '';
    params: {
      lmsUrl: string;
      course_id: string;
    };
  };
  location: History.Location;
  history: any;
}

const UnAuth = (props: UnauthProps) => {
  const userQuery: any = new URLSearchParams(props.location.search);
  const lmsUrl: any = userQuery.get('lmsUrl');
  useEffect(() => {
    if (!window?.top) {
      // Should be impossible when run in a browser
      throw new Error();
    }
    if (sessionStorage.getItem('lmsUrl') && sessionStorage.getItem('course_id')) {
      if (sessionStorage.getItem('domainName') === 'canvas')
        window.top.location.href = `${sessionStorage.getItem(
          'lmsUrl',
        )}/courses/${sessionStorage.getItem('course_id')}`;
      if (sessionStorage.getItem('domainName') === 'D2l')
        window.top.location.href = `${sessionStorage.getItem(
          'lmsUrl',
        )}/d2l/le/lessons/${sessionStorage.getItem('course_id')}`;
    } else if (lmsUrl) {
      if (lmsUrl.includes('instructure'))
        window.top.location.href = `${userQuery.get('lmsUrl')}/courses/${userQuery.get(
          'course_id',
        )}`;
      if (lmsUrl.includes('d2l-partners.brightspace'))
        window.top.location.href = `${userQuery.get('lmsUrl')}/d2l/le/lessons/${userQuery.get(
          'course_id',
        )}`;
    }
  });
  return null;
};

export default UnAuth;
