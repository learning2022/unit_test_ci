import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import Space from '.././loree-editor/modules/space/space';
import { isBB, isCanvas } from '../lmsConfig';
import CONSTANTS from '../loree-editor/constant';

function ListItem(props: { url: string; onClick: () => void; isVisible: boolean; text: string }) {
  return (
    <Dropdown.Item
      role='menuitem'
      className={
        isBB()
          ? `bb-dropdown-item ${!props.isVisible && 'd-none'}`
          : `dropdown-item ${!props.isVisible && 'd-none'}`
      }
      key='whats-new'
      onClick={props.onClick}
      href={props.url}
      target='_blank'
    >
      {props.text}
    </Dropdown.Item>
  );
}

export default function HelpSection(props: { space: Space }) {
  const { space } = props;
  return (
    <Dropdown className='ml-2 dropdown dropdown-layout'>
      <Dropdown.Toggle
        variant='link'
        className='no-caret helpDropdown dropdown-toggle'
        id={CONSTANTS.LOREE_SPACE_HELP_ICON_BUTTON}
        title='Help'
      >
        <FontAwesomeIcon icon={faQuestionCircle} />
      </Dropdown.Toggle>
      <Dropdown.Menu className={isBB() ? 'bb-help-menu' : 'canvas-help-menu'} role='menu'>
        <ListItem
          url={space.getHelpLink('user-guide')}
          text='User guide'
          isVisible
          onClick={() => space.helpLink('user-guide')}
        />
        <ListItem
          url={space.getHelpLink('loree-support')}
          text='Loree support'
          isVisible
          onClick={() => space.helpLink('loree-support')}
        />
        {(isCanvas() || isBB()) && (
          <ListItem
            url={space.getHelpLink(isBB() ? 'bb-whats-new' : 'whats-new')}
            text={`What's new`}
            isVisible
            onClick={() => space.helpLink(isBB() ? 'bb-whats-new' : 'whats-new')}
          />
        )}
      </Dropdown.Menu>
    </Dropdown>
  );
}
