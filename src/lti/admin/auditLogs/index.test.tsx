/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { shallow } from 'enzyme';
import AuditLog from './index';
import { API } from 'aws-amplify';

describe('auditLogs component', () => {
  describe('when LtiLogs API returns results', () => {
    let auditLog: any;

    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(arg => {
        return {
          data: {
            byLtiPlatformByGeneratedAt: {
              items: [
                {
                  clientInfo: JSON.stringify({
                    browserAgent: 'Mozilla/5.0',
                    userIpAddress: '103.254.59.78',
                    browserDomain: 'https://crystaldelta.instructure.com',
                  }),
                  createdAt: '2020-10-13T07:00:32.704Z',
                  generatedAt: '2020-10-13T07:00:32.704Z',
                  id: '765da151-1ff9-409f-ba46-aad94f4f0e5c',
                  ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                  userInfo: JSON.stringify({
                    course_id: 804,
                    ltiClientID: '123670000000000296…anga',
                    email: 'priyanga.alagar@crystaldelta.com',
                  }),
                  userGeoLocation: '106.211.203.85',
                },
                {
                  clientInfo: JSON.stringify({
                    browserAgent: 'Mozilla/5.0',
                    userIpAddress: '103.254.59.78',
                    browserDomain: 'https://crystaldelta.instructure.com',
                  }),
                  createdAt: '2020-10-13T07:00:32.704Z',
                  generatedAt: '2020-10-13T07:00:32.704Z',
                  id: '865da151-1ff9-409f-ba46-aad94f4f0e5c',
                  ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                  userInfo: JSON.stringify({
                    course_id: 804,
                    ltiClientID: '123670000000000296…anga',
                    email: 'priyanga.alagar@crystaldelta.com',
                  }),
                  userGeoLocation: '106.211.203.85',
                },
              ],
            },
          },
        };
      });

      auditLog = shallow(<AuditLog />);
    });

    test('page header is rendered', () => {
      expect(auditLog.find('h3').text()).toEqual('Auditability logs');
    });

    describe('getLogTime', () => {
      describe('when valid input time', () => {
        test.skip('return human readable time format', () => {
          expect(auditLog.instance().getLogTime('2020-10-13T07:00:32.704Z')).toEqual('12:30:32');
        });
      });
      describe('when invalid input time', () => {
        test('return NaN error', () => {
          expect(auditLog.instance().getLogTime('')).toEqual('Invalid date');
        });
      });
    });
    test('audit logs data is rendered', (done) => {
      setImmediate(() => {
        const userLogData: Array<any> = auditLog.state('userLogData');
        expect(userLogData.length).toBe(2);
        done();
      });
    });
  });

  describe('when LtiLogs API returns no results', () => {
    let auditLog: any;

    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(arg => {
        return {
          data: {
            byLtiPlatformByGeneratedAt: {
              items: [],
            },
          },
        };
      });

      auditLog = shallow(<AuditLog />);
    });

    test('render logs table with no result', (done) => {
      setImmediate(() => {
        const userLogData: Array<any> = auditLog.state('userLogData');
        expect(userLogData.length).toBe(0);
        done();
      });
    });

    test('display no logs found message', () => {
      expect(auditLog.find('.p-3').text()).toEqual('No logs found');
    });
  });
});
