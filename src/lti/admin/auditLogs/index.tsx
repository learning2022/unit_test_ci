/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment } from 'react';
import { Table } from 'react-bootstrap';
import { byLtiPlatformByGeneratedAt } from '../../../../src/graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import Loading from '../../../components/loader/loading';
import moment from 'moment';

class AuditLogs extends React.Component {
  state = {
    userLogData: [],
    loading: true,
  };
  async componentDidMount() {
    const ltiPlatformId = sessionStorage.getItem('ltiPlatformId');
    const userLogs: any = await API.graphql(
      graphqlOperation(byLtiPlatformByGeneratedAt, {
        ltiPlatformID: ltiPlatformId,
        sortDirection: 'DESC',
      }),
    );
    const logData = userLogs.data.byLtiPlatformByGeneratedAt.items;
    this.setState({
      userLogData: logData,
      loading: false,
    });
  }

  getLogTime = (timeString: string) => {
    return moment(timeString).format('HH:mm:ss');
  };

  render() {
    return (
      <Fragment>
        <div className='text-primary mb-3'>
          <h3>Auditability logs</h3>
        </div>
        <div>
          {this.state.loading ? (
            <Loading />
          ) : (this.state.userLogData as Array<any>).length > 0 ? (
            <Table responsive borderless>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Course ID</th>
                  <th>User ID</th>
                  <th>User Name</th>
                  <th>Email ID</th>
                  <th>Log Stream</th>
                </tr>
              </thead>
              <tbody>
                {(this.state.userLogData as Array<any>).map((log: any, index: any) => (
                  <tr key={index} className='admin-table'>
                    <td>{new Date(log.generatedAt).toDateString()}</td>
                    <td>{this.getLogTime(log.generatedAt)}</td>
                    <td>{JSON.parse(log.userInfo).course_id}</td>
                    <td>{JSON.parse(log.userInfo).user_id}</td>
                    <td>{JSON.parse(log.userInfo).user_name}</td>
                    <td>{JSON.parse(log.userInfo).email}</td>
                    <td>{log.userAction}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className='container'>
              <div className='p-3'>No logs found</div>
            </div>
          )}
        </div>
      </Fragment>
    );
  }
}
export default AuditLogs;
