import React, { useState, useRef, useEffect, ChangeEvent } from 'react';
import { Table, Button } from 'react-bootstrap';
import { ReactComponent as SearchIcon } from '../../../assets/Icons/search.svg';
import { fontFamilyList } from './fonts';
import { API, graphqlOperation } from 'aws-amplify';
import { listCustomStyles } from '../../../graphql/queries';
import Loading from '../../../components/loader/loading';
import { updateCustomStyle } from '../../../graphql/mutations';
import './fontmanager.scss';
import ToastComponent from '../../components/ToastComponent';
import { ListCustomStylesQuery } from '../../../API';
import { FontList } from '../../../loree-editor/interface';
import { translate } from '../../../i18n/translate';

const initialFontList = () => {
  const VIC_FONT_USERS = [
    'https://vetsharedresources.instructure.com',
    'https://crystaldelta.instructure.com',
    'https://lms.educationapps.vic.gov.au',
  ];

  const PROXIMA_FONT_USERS = [
    'https://byroncollege.instructure.com/',
    'https://crystaldelta.instructure.com',
  ];

  const EVALUATION_ASSOCIATES_FONT = [
    'https://crystaldelta.instructure.com',
    'https://evaluationassociates.instructure.com',
  ];

  const STARWBERRY_SOLUTIONS_FONT = [
    'https://crystaldelta.instructure.com',
    'https://strawberrysolutions.instructure.com',
  ];

  const PROVIDER_PLUS = [
    'https://providerplus.instructure.com',
    'https://crystaldelta.instructure.com',
  ];
  const filteredList = fontFamilyList.filter((item) => {
    if (!VIC_FONT_USERS.includes(sessionStorage.getItem('lmsUrl') as string)) {
      return !item.name.includes('VIC');
    }
    if (!PROXIMA_FONT_USERS.includes(sessionStorage.getItem('lmsUrl') as string)) {
      return !item.name.includes('Proxima');
    }
    if (!EVALUATION_ASSOCIATES_FONT.includes(sessionStorage.getItem('lmsUrl') as string)) {
      return !item.name.includes('Brown');
    }
    if (!STARWBERRY_SOLUTIONS_FONT.includes(sessionStorage.getItem('lmsUrl') as string)) {
      return !item.name.includes('NB');
    }
    if (!PROVIDER_PLUS.includes(sessionStorage.getItem('lmsUrl') as string)) {
      return !item.name.includes('IVYPRESTO');
    }
    return item;
  });
  return filteredList;
};

export const FontManager = () => {
  const [loading, setLoading] = useState(true);
  const [initialState, setInitialState] = useState(() => initialFontList());
  const [customSelectedFont, setCustomSelectedFont]: _Any = useState([]);
  const [search, setSearch] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [applyText, setApplyText] = useState('Save');
  const [id, setId] = useState('');
  const inputFocus = useRef<HTMLInputElement>(null);
  const defaultFontList = [
    'Arial Regular',
    'Barlow',
    'Be Vietnam',
    'Dancing Script',
    'Helvetica',
    'Lato',
    'Open Sans',
    'Source Sans Pro',
    'Times New Roman',
  ];
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState('');

  useEffect(() => {
    void (async function () {
      try {
        const list = await API.graphql<ListCustomStylesQuery>(
          graphqlOperation(listCustomStyles, {
            filter: {
              ltiPlatformID: {
                eq: sessionStorage.getItem('ltiPlatformId'),
              },
            },
          }),
        );
        const data = list.data?.listCustomStyles?.items
          ? JSON.parse(list.data?.listCustomStyles?.items[0]?.customFont as string)
          : '';
        const fontListData: {
          url?: string | undefined;
          name: string;
          fontFamily: string;
          selected: boolean;
        }[] = initialFontList();
        let clonedArray: FontList[] = [];
        const initialFontArray: string[] = [];
        if (data.length > 0) {
          data.forEach((font: { name: string }) => {
            const index = fontListData.findIndex((x: { name: string }) => x.name === font.name);
            if (index !== -1) {
              clonedArray = fontListData.slice();
              clonedArray[index].selected = true;
              initialFontArray.push(clonedArray[index].name);
            }
          });
        }
        fontListData.forEach((font: { name: string; selected: boolean }) => {
          const n = initialFontArray.includes(font.name);
          if (!n) {
            font.selected = false;
          }
        });
        if (list.data?.listCustomStyles?.items) setId(list.data.listCustomStyles.items[0]?.id);
        setCustomSelectedFont(sortSelectedFontList(clonedArray));
        setInitialState(sortSelectedFontList(clonedArray));
        setLoading(false);
      } catch (error) {
        setCustomSelectedFont(initialFontList());
        setLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    if (inputFocus.current) inputFocus.current.focus();
  }, [search]);

  const filterData = (value: string) => {
    const excludeColumns = ['url', 'fontFamily'];
    const lowercasedValue = value.toLowerCase().trim();
    if (lowercasedValue === '') setCustomSelectedFont(initialState);
    else {
      const filteredData = initialState.filter((item: FontList) => {
        return Object.keys(item).some((key) =>
          excludeColumns.includes(key)
            ? false
            : item[key]?.toString().toLowerCase().includes(lowercasedValue),
        );
      });
      setCustomSelectedFont(filteredData);
    }
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
    filterData(event.target.value);
  };

  const handleSearch = () => {
    setSearch(!search);
    if (search) {
      inputFocus?.current?.focus();
    }
  };

  const updateCustomFonts = (e: ChangeEvent<HTMLInputElement>, name: string) => {
    const checkStatus = (e.target as HTMLInputElement).checked;
    const index = customSelectedFont.findIndex((x: { name: string }) => x.name === name);
    const indexNumber = initialState.findIndex((x: { name: string }) => x.name === e.target.id);
    if (index !== -1) {
      const clonedArray = initialState;
      clonedArray[indexNumber].selected = checkStatus;
      setInitialState(clonedArray);
      setCustomSelectedFont((prevState: { name: string }[]) =>
        prevState.map((d: { name: string }) => {
          if (d.name === name) {
            return {
              ...d,
              selected: checkStatus,
            };
          }
          return d;
        }),
      );
    }
  };

  const sortSelectedFontList = (data: FontList[]) => {
    return data.sort((a, b) => {
      return Number(b.selected) - Number(a.selected);
    });
  };

  const handleApplyEvent = async () => {
    setApplyText(translate('global.saving'));
    const selectedFonts: { selected: boolean }[] = [];
    initialState.forEach((font: { selected: boolean }) => {
      if (font.selected) {
        selectedFonts.push(font);
      }
    });
    const userInput = {
      id: id,
      customFont: JSON.stringify(selectedFonts),
    };
    await API.graphql(graphqlOperation(updateCustomStyle, { input: userInput }));
    setShowToast(true);
    setToastMessage(translate('global.savesuccess'));
    setApplyText(translate('global.save'));
  };

  const closeToast = () => {
    setShowToast(false);
  };

  return (
    <>
      <div className='text-primary'>
        <h3>{translate('global.fonts')}</h3>
      </div>
      <div>
        {loading ? (
          <Loading />
        ) : (
          <>
            <div className='row'>
              <div className='col-12'>
                <Button
                  className='float-right'
                  variant='primary'
                  size='sm'
                  onClick={handleApplyEvent}
                >
                  {applyText}
                </Button>
                <div className='d-inline-block float-right pr-3 mt-2 position-relative'>
                  <input
                    id='search'
                    name='search'
                    value={searchText}
                    className={!search ? 'd-none' : 'search pr-5'}
                    type='text'
                    autoComplete='off'
                    placeholder={translate('global.search')}
                    onChange={handleChange}
                    ref={inputFocus}
                  />
                  <SearchIcon
                    width={!search ? 24 : 15}
                    height={!search ? 20 : 12}
                    className={!search ? 'mx-2 search-icon-normal' : 'mx-2 search-icon iconActive'}
                    data-test='searchbtn'
                    onClick={handleSearch}
                  />
                </div>
              </div>
            </div>
            <Table responsive borderless size='sm' className='table-page'>
              <thead>
                <tr>
                  <th>{translate('global.fontfamily')}</th>
                  <th>{translate('global.fontpreview')}</th>
                  <th className='text-center' />
                </tr>
              </thead>
              <tbody>
                {customSelectedFont.map(
                  (font: { name: string; fontFamily: string; selected: boolean }, idx: number) => {
                    return (
                      <tr key={idx} className='admin-table'>
                        <td> {font.name}</td>
                        <td style={{ fontFamily: `${font.fontFamily}` }} className=''>
                          {translate('global.loremipsum')}
                        </td>
                        <td>
                          <input
                            disabled={defaultFontList.includes(font.name)}
                            id={`${font.name}`}
                            checked={defaultFontList.includes(font.name) ? true : font.selected}
                            onChange={(e: ChangeEvent<HTMLInputElement>) =>
                              updateCustomFonts(e, font.name)
                            }
                            type='checkbox'
                            className='mr-1'
                          />
                        </td>
                      </tr>
                    );
                  },
                )}
                {customSelectedFont.length === 0 && (
                  <tr>
                    <td>{translate('global.nofontfamily')}</td>
                    <td>{translate('global.nofontpreview')}</td>
                  </tr>
                )}
              </tbody>
            </Table>
            {showToast && (
              <ToastComponent
                showToast={showToast}
                toastMessage={toastMessage}
                closeToast={closeToast}
              />
            )}
          </>
        )}
      </div>
    </>
  );
};
