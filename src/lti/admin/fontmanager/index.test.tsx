import React from 'react';
import { FontManager } from './index';
import { API } from 'aws-amplify';
import { fontFamilyList } from './fonts';
import { StorageMock } from '../../../utils/storageMock';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';

global.sessionStorage = new StorageMock() as any;

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#fontManagerIndex', () => {
  beforeEach(() => {
    API.graphql = jest.fn().mockImplementation((arg) => {
      return {
        data: {
          listCustomStyles: {
            items: [
              {
                id: '5ab18de7-62ca-4623-a44f-56419a3a4c71',
                ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                customColor: '[{"color":"#1c29b2"}]',
                customHeader:
                  '[{"h1":{"size":"66px","font":"Arial"}},{"h2":{"size":"54px","font":"Arial"}},{"h3":{"size":"50px","font":"Average Sans"}},{"h4":{"size":"34px","font":"SourceSansPro"}},{"h5":{"size":"28px","font":"Arial"}},{"h6":{"size":"22px","font":"Arial"}},{"paragraph":{"size":"20px","font":"Dancing Script"}}]',
                customFont:
                  '[{"fontFamily":"ABeeZee, sans-serif","name":"ABeeZee","url":"https://fonts.googleapis.com/css2?family=ABeeZee","selected":true},{"fontFamily":"Barlow, sans-serif","name":"Barlow","url":"https://fonts.googleapis.com/css2?family=Barlow","selected":true},{"fontFamily":"Be Vietnam, sans-serif","name":"Be Vietnam","url":"https://fonts.googleapis.com/css2?family=Be+Vietnam","selected":true},{"fontFamily":"Dancing Script, handwriting","name":"Dancing Script","url":"https://fonts.googleapis.com/css2?family=Dancing+Script","selected":true},{"fontFamily":"Lato, sans-serif","name":"Lato","url":"https://fonts.googleapis.com/css2?family=Lato","selected":true},{"fontFamily":"Open Sans, sans-serif","name":"Open Sans","url":"https://fonts.googleapis.com/css2?family=Open+Sans","selected":true},{"fontFamily":"Source Sans Pro, sans-serif","name":"Source Sans Pro","url":"https://fonts.googleapis.com/css2?family=Source+Sans+Pro","selected":true},{"fontFamily":"Abel, sans-serif","name":"Abel","url":"https://fonts.googleapis.com/css2?family=Abel","selected":true},{"fontFamily":"Abril Fatface, display","name":"Abril Fatface","url":"https://fonts.googleapis.com/css2?family=Abril+Fatface","selected":true},{"fontFamily":"Acme, sans-serif","name":"Acme","url":"https://fonts.googleapis.com/css2?family=Acme","selected":true},{"fontFamily":"Astloch, display","name":"Astloch","url":"https://fonts.googleapis.com/css2?family=Astloch","selected":true},{"fontFamily":"Average Sans, sans-serif","name":"Average Sans","url":"https://fonts.googleapis.com/css2?family=Average+Sans","selected":true},{"fontFamily":"Barlow Semi Condensed, sans-serif","name":"Barlow Semi Condensed","url":"https://fonts.googleapis.com/css2?family=Barlow+Semi+Condensed","selected":true},{"fontFamily":"Bitter, serif","name":"Bitter","url":"https://fonts.googleapis.com/css2?family=Bitter","selected":true}]',
                customLink:
                  '[{"color":"#b01a8b","text-decoration":"Overline","text-decoration-style":"Dotted","text-style":"Italic","font":"Arial"}]',
                createdAt: '2020-11-30T08:56:56.008Z',
                updatedAt: '2020-12-30T04:27:44.627Z',
              },
            ],
            nextToken: null,
          },
        },
      };
    });
  });

  test('font manager is rendered', () => {
    render(<FontManager />);
    expect(screen.getByRole('heading')).toHaveTextContent('global.fonts');
    expect(document.querySelectorAll('svg').length).toEqual(1);
  });

  describe('VIC Font Supported LMS', () => {
    beforeEach(() => {
      sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
    });
    test('font list rendered with VIC Fonts', async () => {
      const container = render(<FontManager />);
      await waitFor(() =>
        expect(container.getByRole('table').querySelectorAll('tbody tr').length).toEqual(
          fontFamilyList.length,
        ),
      );
    });
  });

  describe('! VIC Font Supported LMS', () => {
    beforeEach(() => {
      sessionStorage.setItem('lmsUrl', '');
    });
    test('font list rendered without VIC Fonts', async () => {
      const filteredList = fontFamilyList.filter((item) => {
        return !item.name.includes('VIC');
      });
      const container = render(<FontManager />);
      await waitFor(() =>
        expect(container.getByRole('table').querySelectorAll('tbody tr').length).toEqual(
          filteredList.length,
        ),
      );
    });
  });
  test('click on search icon should focus input box', async () => {
    render(<FontManager />);
    await waitFor(() => document.getElementsByTagName('svg'));
    fireEvent.click(document.getElementsByTagName('svg')[0]);
    expect(document.getElementsByTagName('input')[0]).toHaveFocus();
  });

  test('font manager is rendered based on search', async () => {
    const fontManager = render(<FontManager />);
    await waitFor(() => document.getElementsByTagName('input'));
    fireEvent.change(document.getElementsByTagName('input')[0], { target: { value: 'Arial' } });
    expect(fontManager.getByRole('table').querySelectorAll('tbody tr').length).toEqual(5);
  });

  test('click on save button should save the updated fonts', async () => {
    render(<FontManager />);
    await waitFor(() => document.getElementsByTagName('button'));
    fireEvent.click(document.getElementsByTagName('button')[0], { target: { value: 'Arial' } });
    expect(document.getElementsByTagName('button')[0].textContent).toEqual('global.saving');
  });
});
describe('Brown Font Supported LMS', () => {
  beforeEach(() => {
    sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
  });
  test('font list rendered with Brown Fonts', async () => {
    const container = render(<FontManager />);
    await waitFor(() =>
      expect(container.getByRole('table').querySelectorAll('tbody tr').length).toEqual(
        fontFamilyList.length,
      ),
    );
  });
});
describe('! Brown Font Supported LMS', () => {
  beforeEach(() => {
    sessionStorage.setItem('lmsUrl', '');
  });
  test('font list rendered without Brown Fonts', async () => {
    const filteredList = fontFamilyList.filter((item) => {
      return !item.name.includes('Brown');
    });
    const container = render(<FontManager />);
    await waitFor(() =>
      expect(container.getByRole('table').querySelectorAll('tbody tr').length).not.toEqual(
        filteredList.length,
      ),
    );
  });
});
describe('NB Font Supported LMS', () => {
  beforeEach(() => {
    sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
  });
  test('font list rendered with NB Fonts', async () => {
    const container = render(<FontManager />);
    await waitFor(() =>
      expect(container.getByRole('table').querySelectorAll('tbody tr').length).toEqual(
        fontFamilyList.length,
      ),
    );
  });
});
describe('! NB Font Supported LMS', () => {
  beforeEach(() => {
    sessionStorage.setItem('lmsUrl', '');
  });
  test('font list rendered without NB Fonts', async () => {
    const filteredList = fontFamilyList.filter((item) => {
      return !item.name.includes('NB');
    });
    const container = render(<FontManager />);
    await waitFor(() =>
      expect(container.getByRole('table').querySelectorAll('tbody tr').length).not.toEqual(
        filteredList.length,
      ),
    );
  });
});
describe('IVYPRESTO Font Supported LMS', () => {
  beforeEach(() => {
    sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
  });
  test('font list rendered with IVYPRESTO Fonts', async () => {
    const container = render(<FontManager />);
    await waitFor(() =>
      expect(container.getByRole('table').querySelectorAll('tbody tr').length).toEqual(
        fontFamilyList.length,
      ),
    );
  });
});
describe('! IVYPRESTO Font Supported LMS', () => {
  beforeEach(() => {
    sessionStorage.setItem('lmsUrl', '');
  });
  test('font list rendered without IVYPRESTO Fonts', async () => {
    const filteredList = fontFamilyList.filter((item) => {
      return !item.name.includes('IVYPRESTO');
    });
    const container = render(<FontManager />);
    await waitFor(() =>
      expect(container.getByRole('table').querySelectorAll('tbody tr').length).not.toEqual(
        filteredList.length,
      ),
    );
  });
});
