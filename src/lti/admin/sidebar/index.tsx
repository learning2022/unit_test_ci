import React from 'react';
import { NavLink } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import { ReactComponent as DashboardIcon } from '../../../assets/Icons/dashboard.svg';
import { ReactComponent as RoleFeatureIcon } from '../../../assets/Icons/roles_features.svg';
import { ReactComponent as MyTemplatesIcon } from '../../../assets/Icons/_editor/template.svg';
import { ReactComponent as FontIcon } from '../../../assets/Icons/elements/text.svg';
import { ReactComponent as ConfigureIcon } from '../../../assets/Icons/configure.svg';
import { ReactComponent as StyleManagerIcon } from '../../../assets/Icons/style_manager.svg';
import { ReactComponent as AuditabilityLogIcon } from '../../../assets/Icons/auditability_logs.svg';
import { ReactComponent as CollapseArrowIcon } from '../../../assets/Icons/collapseArrowIcon.svg';
import { FeatureInterface } from '../../../loree-editor/interface';
import Loreelogo from '../../../assets/images/Loree-logo.png';
import H5pIcon from '../../../assets/images/h5p.png';
import LoreeLogoCollapsed from '../../../assets/images/loree-header-logo.png';
import CONSTANTS from '../../../loree-editor/constant';
import './sidebar.scss';

interface SideNavProps {
  path: string;
  featureFlags?: FeatureInterface;
}

const Sidenav: React.FC<SideNavProps> = (props: SideNavProps): JSX.Element => {
  const useThemeManager = !!props.featureFlags?.thememanager;
  const expandCollapseSideNav = () => {
    const adminSideNavWrap = document.getElementById('adminSideNavWrap');
    adminSideNavWrap?.classList.toggle('collapsedView');
    const adminSideNavExpanded = document.getElementById('adminSideNavExpanded');
    adminSideNavExpanded?.classList.toggle('admin-sidenav-container-fluid');
    const adminMainExpanded = document.getElementById('adminMainExpanded');
    adminMainExpanded?.classList.toggle('admin-main-container-fluid');
  };

  return (
    <>
      <Navbar className='admin-side-nav-wrapper m-0 p-0 d-none d-md-flex' id='adminSideNavWrap'>
        <div
          className='position-absolute collapseExpandBtn'
          onClick={expandCollapseSideNav}
          onKeyPress={expandCollapseSideNav}
          aria-hidden='true'
        >
          <CollapseArrowIcon width={15} height={12} />
        </div>
        <img alt='Loree' src={Loreelogo} className='admin-header-logo mx-auto mt-3' />
        <img
          alt='Loree'
          src={LoreeLogoCollapsed}
          width='40'
          height='40'
          className='collapsed-header-logo'
        />
        <hr />
        <Nav className='side-nav-link-wrapper d-flex flex-column'>
          <NavLink
            exact
            to={`${props.path}/dash`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <DashboardIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Dashboard</span>
          </NavLink>
          <NavLink
            exact
            to={`${props.path}/roles`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <RoleFeatureIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Roles &amp; Features </span>
          </NavLink>
          <NavLink
            exact
            to={`${props.path}/customblock`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <MyTemplatesIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Custom Blocks</span>
          </NavLink>
          <NavLink
            exact
            to={`${props.path}/fonts`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <FontIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Fonts</span>
          </NavLink>
          <NavLink
            exact
            to={`${props.path}/configure`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <ConfigureIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Configure</span>
          </NavLink>
          <NavLink
            exact
            to={`${props.path}/stylemanager`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <StyleManagerIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Style Manager</span>
          </NavLink>
          {useThemeManager && (
            <NavLink
              exact
              to={`${props.path}/thememanager`}
              className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
            >
              <StyleManagerIcon width={20} height={15} />
              <span className='font-weight-light pl-2'>Theme Manager</span>
            </NavLink>
          )}
          <NavLink
            exact
            to={`${props.path}/h5ps`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center d-none'
          >
            <img alt='H5p logo' src={H5pIcon} className='admin-h5p-logo' />
            <span className='font-weight-light pl-2'>H5P Interactives</span>
          </NavLink>
          <NavLink
            exact
            to={`${props.path}/logs`}
            className='mb-3 pl-md-2 pl-xl-3 d-flex align-items-center'
          >
            <AuditabilityLogIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>Auditability Logs</span>
          </NavLink>
        </Nav>
        <small className='mt-auto d-flex px-2 text-muted py-1'>
          Powered by Crystal Delta | {CONSTANTS.LOREE_APP_VERSION}
        </small>
      </Navbar>
    </>
  );
};

export default Sidenav;
