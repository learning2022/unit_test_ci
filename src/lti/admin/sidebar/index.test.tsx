import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import Sidenav from './index';

const sideBarProps = {
  path: '/lti/editor/admin',
  featureFlags: { thememanager: false },
};

const sideNav = mount(
  <Router>
    <Sidenav {...sideBarProps} />
  </Router>,
);

describe('admin sidebar', () => {
  test('nav tag is rendered', () => {
    expect(sideNav.find('nav')).toHaveLength(1);
  });

  test('div tag is rendered', () => {
    expect(sideNav.find('div')).toHaveLength(2);
  });
  test('navBar options', () => {
    const closeIcon = sideNav.find('div').at(0).html();
    expect(closeIcon).toEqual(
      `<div class="position-absolute collapseExpandBtn" aria-hidden="true"><svg width="15" height="12">collapseArrowIcon.svg</svg></div>`,
    );
  });
  test('navLink tag is rendered', () => {
    const navBarArray = sideNav.find('NavLink');
    expect(navBarArray.length).toBe(8);
    expect(navBarArray.at(6).html()).not.toContain('Theme Manager');
  });

  test('navLink tag is rendered with Theme Manager', () => {
    const sideNavWithTheme = mount(
      <Router>
        <Sidenav path='/lti/editor/admin' featureFlags={{ thememanager: true }} />
      </Router>,
    );
    const navBarArray = sideNavWithTheme.find('NavLink');
    expect(navBarArray.length).toBe(9);
    expect(navBarArray.at(5).html()).toContain('Style Manager');
    expect(navBarArray.at(6).html()).toContain('Theme Manager');
  });

  describe('sidebar options', () => {
    test('roles and features option is present', () => {
      const option = sideNav.find('NavLink').at(1).html();
      expect(option).toContain('Roles &amp; Features');
    });
    test('custom Blocks option is present', () => {
      const option = sideNav.find('NavLink').at(2).html();
      expect(option).toContain('Custom Blocks');
    });
    test('Dashboard option is present', () => {
      const option = sideNav.find('NavLink').at(0).html();
      expect(option).toContain('Dashboard');
    });
  });

  describe('admin dashboard sidebar footer', () => {
    test('Checking `small` tag is rendered', () => {
      expect(sideNav.find('small')).toHaveLength(1);
    });
  });
});
