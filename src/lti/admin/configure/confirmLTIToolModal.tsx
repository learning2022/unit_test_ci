import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { translate } from '../../../i18n/translate';

export interface AlertModalProps {
  modalBody: string;
  show: boolean;
  hide(): void;
  confirmCancel(): void;
}

function AlertModal(props: AlertModalProps) {
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.hide}
        backdrop='static'
        centered
        className='border-primary'
        size='lg'
      >
        <Modal.Header closeButton data-testid='close'>
          <Modal.Title>
            <h5 className='text-primary' aria-level={1}>
              {translate('global.confirm')}
            </h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>{props.modalBody}</Modal.Body>
        <Modal.Footer className='d-flex justify-content-center'>
          <Button
            variant='outline-primary'
            className='modal-footer-cancel-btn editor-custom-btn mx-1'
            onClick={props.hide}
          >
            {translate('global.no')}
          </Button>
          <Button
            variant='primary'
            className='editor-custom-btn mx-1'
            type='submit'
            onClick={props.confirmCancel}
          >
            {translate('global.yes')}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default AlertModal;
