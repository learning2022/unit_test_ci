export interface ExternalToolField {
  key: string;
  label: string;
  isHidden: boolean;
  isMandatory: boolean;
  isEditable: boolean;
}

export const externalToolFieldList: ExternalToolField[] = [
  {
    key: 'toolName',
    label: 'Tool Name',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },
  {
    key: 'oidcUrl',
    label: 'OIDC Url',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },
  {
    key: 'jwksUrl',
    label: 'JWKS Url',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },
  {
    key: 'redirectURI',
    label: 'Redirect Url',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },
  {
    key: 'clientId',
    label: 'Client ID',
    isHidden: false,
    isMandatory: false,
    isEditable: false,
  },
  {
    key: 'issuerUrl',
    label: 'Issuer Url',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },

  {
    key: 'clientSecret',
    label: 'Client Secret',
    isHidden: true,
    isMandatory: false,
    isEditable: false,
  },
  {
    key: 'targetLinkURI',
    label: 'Target Link Url',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },
  {
    key: 'domainName',
    label: 'Domain Name',
    isHidden: false,
    isMandatory: true,
    isEditable: true,
  },
  {
    key: 'deploymentId',
    label: 'Deployment ID',
    isHidden: false,
    isMandatory: false,
    isEditable: false,
  },
];
