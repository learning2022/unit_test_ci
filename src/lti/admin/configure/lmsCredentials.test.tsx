/* eslint-disable jest/no-done-callback */
import React from 'react';
import { shallow } from 'enzyme';
import LmsCredentials from './lmsCredentials';
import { API } from 'aws-amplify';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#lmsCredentials', () => {
  let lmsCredentials: any;
  describe('when canvas client API returns result', () => {
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation((arg) => {
        return {
          data: {
            canvasClient:
              '{"statusCode":200,"body":{"platformId":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","clientId":"123670000000000300","platformUrl":"https://canvas.instructure.com","apiClientId":"123670000000000301","apiSecretKey":"tEKdnC98yy0PIX17vTIlacoDCbwJxTu8zZLuKBVS4UaDrIRjvMvZ46HlOsEcwcK7"}}',
          },
        };
      });
      lmsCredentials = shallow(<LmsCredentials />);
    });
    test('lms credentials rendered', (done) => {
      setImmediate(() => {
        expect(lmsCredentials.find('FormControl').at(0).props().type).toEqual('password');
        done();
      });
    });
  });
  describe('when canvas client API returns error', () => {
    let lmsCredentials: any;
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation((arg) => {
        return {
          data: {
            canvasClient: '{"statusCode":400,"body":""}',
          },
        };
      });
      lmsCredentials = shallow(<LmsCredentials />);
    });
    test('lms credentials is not rendered', (done) => {
      setImmediate(() => {
        expect(lmsCredentials.find('h1').text()).toEqual('error.text');
        done();
      });
    });
  });
  describe('when click canvas domain hide icon', () => {
    test('canvas domain value hided', () => {
      lmsCredentials.find('FontAwesomeIcon').at(0).simulate('click');
      expect(lmsCredentials.find('FormControl').at(0).props().type).toEqual('text');
    });
  });
  describe('when click LTI clientID hide icon', () => {
    test('LTI clientID value hided', () => {
      lmsCredentials.find('FontAwesomeIcon').at(1).simulate('click');
      expect(lmsCredentials.find('FormControl').at(1).props().type).toEqual('text');
    });
  });
  describe('when click LTI clientSecret hide icon', () => {
    test('LTI clientSecret value hided', () => {
      lmsCredentials.find('FontAwesomeIcon').at(2).simulate('click');
      expect(lmsCredentials.find('FormControl').at(2).props().type).toEqual('text');
    });
  });
  describe('when click API clientID hide icon', () => {
    test('API clientID value hided', () => {
      lmsCredentials.find('FontAwesomeIcon').at(3).simulate('click');
      expect(lmsCredentials.find('FormControl').at(3).props().type).toEqual('text');
    });
  });
  describe('when click API clientSecret hide icon', () => {
    test('API clientSecret value hided', () => {
      lmsCredentials.find('FontAwesomeIcon').at(4).simulate('click');
      expect(lmsCredentials.find('FormControl').at(4).props().type).toEqual('text');
    });
  });
});
