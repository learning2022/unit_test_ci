/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { Form, Row, Col, Button } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import { listKalturaConfigs } from '../../../graphql/queries';
import { createKalturaConfig, updateKalturaConfig } from '../../../graphql/mutations';
import ToastComponent from '../../components/ToastComponent';
import Loading from '../../../components/loader/loading';
import { translate } from '../../../i18n/translate';

class KalturaCredentials extends React.Component {
  state = {
    data: null as any,
    partnerId: '',
    appTokenInput: '',
    tokenInput: '',
    showToast: false,
    toastMessage: '',
    loading: true,
    error: false,
    saveText: `${translate('global.save')}`,
    saveBtnActiveState: false,
  };

  async componentDidMount() {
    const list: any = await API.graphql(
      graphqlOperation(listKalturaConfigs, {
        filter: {
          ltiPlatformID: {
            eq: sessionStorage.getItem('ltiPlatformId'),
          },
        },
        limit: 1,
      }),
    );
    this.setState({ data: list.data.listKalturaConfigs.items[0], loading: false });
  }

  closeToast = () => {
    this.setState({ showToast: false });
  };

  partnerIdInput = (e: any): any => {
    e.target.value === ''
      ? this.setState({ saveBtnActiveState: true })
      : this.setState({ saveBtnActiveState: false });
    this.setState({ partnerId: e.target.value });
  };

  appTokenIdInput = (e: any): any => {
    e.target.value === ''
      ? this.setState({ saveBtnActiveState: true })
      : this.setState({ saveBtnActiveState: false });
    this.setState({ appTokenInput: e.target.value });
  };

  tokenIdInput = (e: any): any => {
    e.target.value === ''
      ? this.setState({ saveBtnActiveState: true })
      : this.setState({ saveBtnActiveState: false });
    this.setState({ tokenInput: e.target.value });
  };

  handleUpdate = async (id: any) => {
    this.setState({ saveText: translate('global.saving') });
    const userInput = {
      id: id,
      partnerID: this.state.partnerId ? this.state.partnerId : this.state.data.partnerID,
      appToken: this.state.appTokenInput ? this.state.appTokenInput : this.state.data.appToken,
      tokenID: this.state.tokenInput ? this.state.tokenInput : this.state.data.tokenID,
    };
    try {
      await API.graphql(graphqlOperation(updateKalturaConfig, { input: userInput }));
      this.setState({ showToast: true, toastMessage: 'Updated Successfully' });
    } catch (error) {
      this.setState({ error: true });
      console.log(error);
    }
    this.setState({ saveText: translate('global.save') });
  };

  handleCreate = async () => {
    this.setState({ saveText: translate('global.saving') });
    const userInput = {
      ltiPlatformID: sessionStorage.getItem('ltiPlatformId'),
      partnerID: this.state.partnerId,
      appToken: this.state.appTokenInput,
      tokenID: this.state.tokenInput,
    };
    try {
      await API.graphql(graphqlOperation(createKalturaConfig, { input: userInput }));
      this.setState({ showToast: true, toastMessage: 'Created Successfully' });
    } catch (error) {
      this.setState({ error: true });
      console.log(error);
    }
    this.setState({ saveText: translate('toast.save') });
  };

  render() {
    return (
      <>
        {this.state.loading ? (
          <Loading />
        ) : this.state.error ? (
          <h1>{translate('error.text')}</h1>
        ) : (
          <Form className='mt-3 ml-2'>
            <>
              <Form.Group as={Row}>
                <Form.Label column sm='2' className='pr-0 font-weight-bold'>
                  {translate('admin.partnerid')}
                </Form.Label>
                <Col sm='8'>
                  <Form.Control
                    type='text'
                    placeholder={translate('admin.partnerid_placeholder')}
                    defaultValue={
                      this.state.data ? this.state.data.partnerID : this.state.partnerId
                    }
                    onChange={this.partnerIdInput}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm='2' className='pr-0 font-weight-bold'>
                  {translate('admin.apptoken')}
                </Form.Label>
                <Col sm='8'>
                  <Form.Control
                    type='text'
                    placeholder={translate('admin.apptoken_placeholder')}
                    defaultValue={
                      this.state.data ? this.state.data.appToken : this.state.appTokenInput
                    }
                    onChange={this.appTokenIdInput}
                  />
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm='2' className='pr-0 font-weight-bold'>
                  {translate('admin.tokenid')}
                </Form.Label>
                <Col sm='8'>
                  <Form.Control
                    type='text'
                    placeholder={translate('admin.tokenid_placeholder')}
                    defaultValue={this.state.data ? this.state.data.tokenID : this.state.tokenInput}
                    onChange={this.tokenIdInput}
                  />
                </Col>
              </Form.Group>
            </>
          </Form>
        )}
        <div className='style-manager-button-wrapper-section d-flex justify-content-center mt-5'>
          <Button
            className='add-button'
            size='sm'
            onClick={() => {
              this.state.data ? this.handleUpdate(this.state.data.id) : this.handleCreate();
            }}
            disabled={this.state.saveBtnActiveState}
          >
            {this.state.saveText}
          </Button>
        </div>
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
      </>
    );
  }
}

export default KalturaCredentials;
