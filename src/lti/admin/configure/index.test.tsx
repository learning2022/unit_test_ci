import React from 'react';
import API from '@aws-amplify/api';
import { Configure } from './index';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './../../../../src/aws-exports';
import { render, screen, fireEvent } from '@testing-library/react';
import {
  listLtiToolMockData,
  listLtiToolsMockResponse,
  canvasClientMockResponse,
  listKalturaConfigsMockResponse,
  listLtiToolsNoLtiToolMockResponse,
} from './mockData';
import axe from '../../../axeHelper';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#configureIndex', () => {
  beforeAll(() => {
    window.sessionStorage.setItem('domainName', 'canvas');
    window.sessionStorage.setItem('course_id', '848');
    window.sessionStorage.setItem('lmsEmail', 'andiswamy.raja@crystaldelta.com');
    window.sessionStorage.setItem('lmsUrl', 'https://crystaldelta.instructure.com');
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
  });

  beforeEach(() => {
    Amplify.configure(awsconfig);
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'andiswamy',
        sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);

    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasClientMockResponse)
      .mockImplementationOnce(() => listKalturaConfigsMockResponse)
      .mockImplementationOnce(() => listLtiToolsMockResponse);
  });

  describe('when click configure tab', () => {
    test('configure page is rendered', () => {
      render(<Configure />);
      expect(screen.getByText('Configure')).toBeInTheDocument();
      expect(screen.getByText('Kaltura')).toBeInTheDocument();
      // expect(screen.getByText('Configure External LTI Tools')).toBeInTheDocument();
    });
  });
  describe('when click external tool accordion', () => {
    beforeEach(() => {
      process.env.REACT_APP_ENABLE_EXTERNAL_LTI_TOOL = 'true';
      render(<Configure />);
    });
    test.skip('should pass accessibility tests', async () => {
      expect(await axe(document.body)).toHaveNoViolations();
    });

    test('should display relevant external tool fields and reflect mocked data correctly', async () => {
      expect(await screen.findByTestId('external-tool-accordion-toggle')).toBeVisible();
      const accordion = await screen.getByTestId('external-tool-accordion-toggle');
      fireEvent.click(accordion);

      // verify input fields are displayed
      expect(screen.getByText('Tool Name *')).toBeInTheDocument();
      expect(screen.getByText('Issuer Url *')).toBeInTheDocument();
      expect(screen.getByText('Client ID')).toBeInTheDocument();
      expect(screen.getByText('Client Secret')).toBeInTheDocument();
      expect(screen.getByText('OIDC Url *')).toBeInTheDocument();
      expect(screen.getByText('Redirect Url *')).toBeInTheDocument();
      expect(screen.getByText('Target Link Url *')).toBeInTheDocument();
      expect(screen.getByText('JWKS Url *')).toBeInTheDocument();

      // verify field values are correct
      expect(screen.getByPlaceholderText('Tool Name')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.toolName,
      );
      expect(screen.getByPlaceholderText('Issuer Url')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.issuerUrl,
      );
      // expect(screen.getByPlaceholderText('Client ID')).toHaveAttribute('value', '1000000000001');
      expect(screen.getByPlaceholderText('Client Secret')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.clientSecret,
      );
      expect(screen.getByPlaceholderText('OIDC Url')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.oidcUrl,
      );
      expect(screen.getByPlaceholderText('Redirect Url')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.redirectURI,
      );
      expect(screen.getByPlaceholderText('Target Link Url')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.targetLinkURI,
      );
      expect(screen.getByPlaceholderText('JWKS Url')).toHaveAttribute(
        'value',
        listLtiToolMockData[0]?.jwksUrl,
      );
    });

    test('should display create lti tool modal on click of button', async () => {
      expect(await screen.findByTestId('external-tool-accordion-toggle')).toBeVisible();

      // verify add external tool button is present
      expect(screen.getByText('externaltools.newexternaltool')).toBeInTheDocument();
      const addButton = await screen.getByText('externaltools.newexternaltool');
      fireEvent.click(addButton);
      expect(screen.getByText('Pick your Tool Type')).toBeInTheDocument();
      expect(screen.getByText('Other')).toBeInTheDocument();
      expect(screen.getByText('Add')).toBeInTheDocument();
    });

    test('should display no external tool accordion when no external tool results are returned', async () => {
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => canvasClientMockResponse)
        .mockImplementationOnce(() => listKalturaConfigsMockResponse)
        .mockImplementationOnce(() => listLtiToolsNoLtiToolMockResponse);

      const accordion = screen.queryByText('External Tool');
      expect(accordion).toBeNull(); // it doesn't exist
    });
  });
});

describe('#configureIndex for BB', () => {
  beforeAll(() => {
    window.sessionStorage.setItem('domainName', 'BB');
  });

  describe('when click configure tab in BB', () => {
    test('configure page is rendered with kaltura tab in BB', () => {
      render(<Configure />);
      expect(screen.getByText('Configure')).toBeInTheDocument();
      expect(screen.getByText('Kaltura')).toBeInTheDocument();
    });
  });
});
