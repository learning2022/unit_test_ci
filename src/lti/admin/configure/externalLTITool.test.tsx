import React from 'react';
import { render, screen } from '@testing-library/react';
import { LtiTool } from '../../../loree-editor/interface';
import axe from '../../../axeHelper';

import ExternalLTITool from './externalLTITool';
import userEvent from '@testing-library/user-event';
import { API } from 'aws-amplify';
import {
  createLtiToolMock,
  createLtiToolDeploymentMock,
  canvasClientMockResponse,
  listKalturaConfigsMockResponse,
  listLtiToolsMockResponse,
} from './mockData';
import { Configure } from './index';

const props = {
  isActiveAccordion: true,
  handleActiveAccordion: jest.fn(),
  setIsAddExternalToolDisabled: jest.fn(),
  externalToolConfig: {
    id: 'id',
    ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
    toolName: 'H5P',
    issuerUrl: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
    clientId: '1000000000001',
    clientSecret: '123456',
    oidcUrl: 'https://limon.h5p.com/lti/login',
    redirectURI: 'https://limon.h5p.com/lti/launch',
    targetLinkURI: 'https://limon.h5p.com/lti/launch',
    jwksUrl: 'https://limon.h5p.com/lti/jwks/1291484793007326309.json',
    domainName: 'https://limon.h5p.com',
    toolDeployments: {
      items: [
        {
          id: 'beb3443c-3f4f-4e88-911f-4c8a91bdd43b',
        },
      ],
    },
  } as LtiTool,
  index: 0,
  handleCancelExternalLTITool: jest.fn(),
};
jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('externalLTITool', () => {
  beforeEach(() => {
    render(<ExternalLTITool {...props} />);
  });

  describe('when rendered', () => {
    test('should loads mandatory fields correctly', async () => {
      // verify field labels exist
      expect(screen.getByText('Tool Name *')).toBeInTheDocument();
      expect(screen.getByText('Issuer Url *')).toBeInTheDocument();
      expect(screen.getByText('Client ID')).toBeInTheDocument();
      expect(screen.getByText('Client Secret')).toBeInTheDocument();
      expect(screen.getByText('OIDC Url *')).toBeInTheDocument();
      expect(screen.getByText('Redirect Url *')).toBeInTheDocument();
      expect(screen.getByText('Target Link Url *')).toBeInTheDocument();
      expect(screen.getByText('JWKS Url *')).toBeInTheDocument();
      expect(screen.getByText('Domain Name *')).toBeInTheDocument();
      expect(screen.getByText('Deployment ID')).toBeInTheDocument();
    });

    test('should contain mandatory fields input values', async () => {
      // verify field values
      expect(screen.getByPlaceholderText('Tool Name')).toHaveAttribute('value', 'H5P');
      expect(screen.getByPlaceholderText('Issuer Url')).toHaveAttribute(
        'value',
        'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
      );
      expect(screen.getByPlaceholderText('Client ID')).toHaveAttribute('value', '1000000000001');
      expect(screen.getByPlaceholderText('Client Secret')).toHaveAttribute('value', '123456');
      expect(screen.getByPlaceholderText('OIDC Url')).toHaveAttribute(
        'value',
        'https://limon.h5p.com/lti/login',
      );
      expect(screen.getByPlaceholderText('Redirect Url')).toHaveAttribute(
        'value',
        'https://limon.h5p.com/lti/launch',
      );
      expect(screen.getByPlaceholderText('Target Link Url')).toHaveAttribute(
        'value',
        'https://limon.h5p.com/lti/launch',
      );
      expect(screen.getByPlaceholderText('JWKS Url')).toHaveAttribute(
        'value',
        'https://limon.h5p.com/lti/jwks/1291484793007326309.json',
      );
      expect(screen.getByPlaceholderText('Domain Name')).toHaveAttribute(
        'value',
        'https://limon.h5p.com',
      );
      expect(screen.getByPlaceholderText('Deployment ID')).toHaveAttribute(
        'value',
        'beb3443c-3f4f-4e88-911f-4c8a91bdd43b',
      );
    });

    test('should should reflect defaultHidden property in input fields type', async () => {
      // verify ishidden value defined in externaltoolfield is reflect in type
      expect(screen.getByPlaceholderText('Tool Name')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Issuer Url')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Client ID')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Client Secret')).toHaveAttribute('type', 'password');
      expect(screen.getByPlaceholderText('OIDC Url')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Redirect Url')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Target Link Url')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('JWKS Url')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Domain Name')).toHaveAttribute('type', 'text');
      expect(screen.getByPlaceholderText('Deployment ID')).toHaveAttribute('type', 'text');
    });

    test('deployment id, client id and secret inputs should not be editable', () => {
      expect(screen.getByPlaceholderText('Tool Name')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Issuer Url')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Client ID')).toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Client Secret')).toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('OIDC Url')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Redirect Url')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Target Link Url')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('JWKS Url')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Domain Name')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Deployment ID')).toHaveAttribute('disabled');
    });
    test('should be able to edit configuration', async () => {
      expect(screen.getByTestId('external-tool-accordion-toggle')).toBeVisible();
      expect(screen.getByText('global.edit')).toBeInTheDocument();
      screen.getByText('global.edit').click();
      expect(screen.getByPlaceholderText('Tool Name')).not.toHaveAttribute('disabled');
      expect(screen.getByPlaceholderText('Client ID')).toHaveAttribute('disabled');
      expect(screen.getByText('global.update')).toBeInTheDocument();
      screen.getByText('global.update').click();
      expect(screen.getByText('global.updating')).toBeInTheDocument();
    });
    test('should be able to cancel the edit configuration', async () => {
      expect(screen.getByTestId('external-tool-accordion-toggle')).toBeVisible();
      expect(screen.getByText('global.edit')).toBeInTheDocument();
      screen.getByText('global.edit').click();
      userEvent.type(screen.getByPlaceholderText('Tool Name'), 'Test');
      expect(screen.getByText('global.cancel')).toBeInTheDocument();
      screen.getByText('global.cancel').click();
      expect(screen.getByText('global.confirm')).toBeInTheDocument();
      screen.getByText('global.yes').click();
      expect(screen.getByPlaceholderText('Tool Name')).toHaveAttribute('value', 'H5P');
    });
    test('should be able to cancel the edit configuration and click on no', async () => {
      expect(screen.getByTestId('external-tool-accordion-toggle')).toBeVisible();
      expect(screen.getByText('global.edit')).toBeInTheDocument();
      screen.getByText('global.edit').click();
      userEvent.type(screen.getByPlaceholderText('Tool Name'), 'Test');
      expect(screen.getByText('global.cancel')).toBeInTheDocument();
      screen.getByText('global.cancel').click();
      expect(screen.getByText('global.confirm')).toBeInTheDocument();
      screen.getByText('global.no').click();
      expect(screen.getByPlaceholderText('Tool Name')).toHaveAttribute('value', 'H5PTest');
    });
  });
});

describe('Accessibility', () => {
  test('should pass accessibility tests', async () => {
    const externalLtiTool = render(<ExternalLTITool {...props} />);
    const results = await axe(externalLtiTool.container);
    expect(results).toHaveNoViolations();
  });
});

describe('Create externaltool', () => {
  beforeEach(() => {
    process.env.REACT_APP_ENABLE_EXTERNAL_LTI_TOOL = 'true';
    sessionStorage.setItem('domainName', 'canvas');
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasClientMockResponse)
      .mockImplementationOnce(() => listKalturaConfigsMockResponse)
      .mockImplementationOnce(() => listLtiToolsMockResponse)
      .mockImplementationOnce(() => createLtiToolMock)
      .mockImplementationOnce(() => createLtiToolDeploymentMock);
    render(<Configure />);
  });
  test('should display create lti tool modal on click of button', async () => {
    // verify add external tool button is present
    const addExternalToolButton = screen.getByText('externaltools.newexternaltool');
    expect(addExternalToolButton).toBeInTheDocument();
    addExternalToolButton.click();
    // add external tool
    screen.getByText('Add').click();
    expect(screen.queryByText('External Tool -')).toBeInTheDocument();
    expect(screen.queryAllByText('global.save')[0]).toBeDisabled();
    // fill in external tool details
    userEvent.type(screen.queryAllByPlaceholderText('Tool Name')[1], 'H5P');
    userEvent.type(
      screen.queryAllByPlaceholderText('Issuer Url')[1],
      'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
    );
    userEvent.type(
      screen.queryAllByPlaceholderText('OIDC Url')[1],
      'https://limon.h5p.com/lti/login',
    );
    userEvent.type(
      screen.queryAllByPlaceholderText('Redirect Url')[1],
      'https://limon.h5p.com/lti/launch',
    );
    userEvent.type(
      screen.queryAllByPlaceholderText('Target Link Url')[1],
      'https://limon.h5p.com/lti/launch',
    );
    userEvent.type(
      screen.queryAllByPlaceholderText('JWKS Url')[1],
      'https://limon.h5p.com/lti/jwks/1291484793007326309.json',
    );
    userEvent.type(screen.getByPlaceholderText('Domain Name'), 'https://limon.h5p.com');
    screen.queryAllByText('global.save')[1].click();
    expect(screen.getByText('global.saving')).toBeInTheDocument();
  });

  test('cancel the creation and click yes on modal', () => {
    const addExternalToolButton = screen.getByText('externaltools.newexternaltool');
    expect(addExternalToolButton).toBeInTheDocument();
    addExternalToolButton.click();
    // add external tool
    screen.getByText('Add').click();
    expect(screen.queryByText('global.cancel')).toBeInTheDocument();
    screen.getByText('global.cancel').click();
    expect(screen.getByText('global.confirm')).toBeInTheDocument();
    screen.getByText('global.yes').click();
    expect(screen.queryAllByPlaceholderText('Tool Name')[1]).toBeUndefined();
  });

  test('cancel the creation and click no on modal', () => {
    const addExternalToolButton = screen.getByText('externaltools.newexternaltool');
    expect(addExternalToolButton).toBeInTheDocument();
    addExternalToolButton.click();
    // add external tool
    screen.getByText('Add').click();
    expect(screen.queryByText('global.cancel')).toBeInTheDocument();
    screen.getByText('global.cancel').click();
    expect(screen.getByText('global.confirm')).toBeInTheDocument();
    screen.getByText('global.no').click();
    expect(screen.queryAllByPlaceholderText('Tool Name')[1]).not.toBeUndefined();
  });
});
