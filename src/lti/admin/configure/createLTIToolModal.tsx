import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface CreateLTIToolModalProps {
  showCreateLTIToolModal: boolean;
  toggleCreateLTIToolModal: () => void;
  handleAddExternalLTIToolsConfiguration: () => void;
}

const CreateLTIToolModal: React.FC<CreateLTIToolModalProps> = (props: CreateLTIToolModalProps) => {
  const LTI_TOOL_TYPES: string[] = ['Other'];

  const [ltiToolType, setltiToolType] = useState(LTI_TOOL_TYPES[0]);

  return (
    <Modal
      show={props.showCreateLTIToolModal}
      onHide={props.toggleCreateLTIToolModal}
      backdrop='static'
      keyboard={false}
      centered
      size='sm'
    >
      <Modal.Header closeButton>
        <Modal.Title id='create-lti-tool-modal-header'>
          <h5 className='text-primary'>Pick your Tool Type</h5>
        </Modal.Title>
      </Modal.Header>
      <Form onSubmit={(event) => event.preventDefault()}>
        <Modal.Body>
          <Form.Group
            id='create-lti-tool-modal-body'
            className='canvasAddPageTemplateDropdown position-relative d-flex align-items-center mb-0 mr-2'
            style={{ width: '100%' }}
          >
            <Form.Control
              as='select'
              defaultValue={LTI_TOOL_TYPES[0]}
              value={ltiToolType}
              onChange={(e) => setltiToolType(e.target.value)}
            >
              <option value={LTI_TOOL_TYPES[0]}>{LTI_TOOL_TYPES[0]}</option>
            </Form.Control>
            <span className='position-absolute'>
              <FontAwesomeIcon icon={faCaretDown} />
            </span>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-center'>
          <Button
            variant='outline-primary'
            data-testid='button'
            onClick={() => {
              props.handleAddExternalLTIToolsConfiguration();
              props.toggleCreateLTIToolModal();
            }}
            size='sm'
            className='mx-1'
          >
            Add
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default CreateLTIToolModal;
