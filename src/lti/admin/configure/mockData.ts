export const listLtiToolMockData = [
  {
    id: 'cf59bce5-a779-4696-b604-5a380a25f10f',
    ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
    toolName: 'H5PInteractive',
    issuerUrl: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
    clientId: '1000000000001',
    clientSecret: '123456',
    oidcUrl: 'https://limon.h5p.com/lti/login',
    redirectURI: 'https://limon.h5p.com/lti/launch',
    targetLinkURI: 'https://limon.h5p.com/lti/launch',
    jwksUrl: 'https://limon.h5p.com/lti/jwks/1291484793007326309.json',
    createdAt: '2022-01-17T02:47:56.029Z',
    updatedAt: '2022-02-01T06:49:03.591Z',
  },
];

export const listLtiToolsMockResponse = {
  data: {
    listLtiTools: {
      items: listLtiToolMockData,
      nextToken: null,
    },
  },
};

export const listLtiToolsNoLtiToolMockResponse = {
  data: {
    listLtiTools: {
      items: [],
      nextToken: null,
    },
  },
};

export const canvasClientMockResponse = {
  data: {
    canvasClient:
      '{"statusCode":200,"body":{"platformId":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","clientId":"123670000000000300","platformUrl":"https://canvas.instructure.com","apiClientId":"123670000000000301","apiSecretKey":"tEKdnC98yy0PIX17vTIlacoDCbwJxTu8zZLuKBVS4UaDrIRjvMvZ46HlOsEcwcK7"}}',
  },
};

export const listKalturaConfigsMockResponse = {
  data: {
    listKalturaConfigs: {
      items: [
        {
          id: '55be43c9-6654-44e7-b2b9-dfeb9e57528a',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          partnerID: '2503821',
          appToken: 'a874d20102438b678951e99977dad590',
          tokenID: '0_9pqjpyw0',
          createdAt: '2020-09-17T18:02:00.457Z',
          updatedAt: '2020-09-17T18:02:00.457Z',
        },
      ],
      nextToken: null,
    },
  },
};

export const createLtiToolMock = {
  data: {
    createLtiTool: {
      id: 'a38d740a-7515-4c5c-8f64-29db1a2d8b39',
      ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
      toolName: 'test',
      issuerUrl: 'test',
      oidcUrl: 'test',
      redirectURI: 'test',
      targetLinkURI: 'test',
      jwksUrl: 'test',
      domainName: 'test',
      clientId: 'aa425b96-9a70-4c5a-aeeb-5b67df8eb5bb',
      clientSecret: '74b513e9-ff7f-48d9-b324-f4fc8fed5831',
      toolDeployments: {
        items: [],
        nextToken: null,
      },
      createdAt: '2022-04-18T10:21:29.484Z',
      updatedAt: '2022-04-18T10:21:29.484Z',
    },
  },
};

export const createLtiToolDeploymentMock = {
  data: {
    createLtiToolDeployment: {
      id: 'a7b51076-3193-44c0-abfa-fcab046518a3',
      toolId: {
        id: 'a38d740a-7515-4c5c-8f64-29db1a2d8b39',
        ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
        toolName: 'test',
        issuerUrl: 'test',
        oidcUrl: 'test',
        redirectURI: 'test',
        targetLinkURI: 'test',
        jwksUrl: 'test',
        domainName: 'test',
        clientId: 'aa425b96-9a70-4c5a-aeeb-5b67df8eb5bb',
        clientSecret: '74b513e9-ff7f-48d9-b324-f4fc8fed5831',
        toolDeployments: {
          items: [
            {
              id: 'a7b51076-3193-44c0-abfa-fcab046518a3',
              createdAt: '2022-04-18T10:21:30.128Z',
              updatedAt: '2022-04-18T10:21:30.128Z',
            },
          ],
          nextToken: null,
        },
        createdAt: '2022-04-18T10:21:29.484Z',
        updatedAt: '2022-04-18T10:21:29.484Z',
      },
      createdAt: '2022-04-18T10:21:30.128Z',
      updatedAt: '2022-04-18T10:21:30.128Z',
    },
  },
};
