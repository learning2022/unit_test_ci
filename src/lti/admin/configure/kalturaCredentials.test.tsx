/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { shallow } from 'enzyme';
import KalturaCredentials from './kalturaCredentials';
import { API } from 'aws-amplify';

describe('#kalturaCredntials', () => {
  let kalturaCredentials: any;
  describe('when kaltura configuration API returns result', () => {
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(arg => {
        return {
          data: {
            listKalturaConfigs: {
              items: [
                {
                  id: '55be43c9-6654-44e7-b2b9-dfeb9e57528a',
                  ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                  partnerID: '2503821',
                  appToken: 'a874d20102438b678951e99977dad590',
                  tokenID: '0_9pqjpyw0',
                  createdAt: '2020-09-17T18:02:00.457Z',
                  updatedAt: '2020-09-17T18:02:00.457Z',
                },
              ],
              nextToken:
                'eyJ2ZXJzaW9uIjoyLCJ0b2tlbiI6IkFRSUNBSGcxTUkwMVp2VjlkTHF1STdtTkkvN2Nocm1GbStXSjNMM2NHLzAyamNqMGRRR0xCWEJkbGFVWmxCaU1BdXdZaTgyRUFBQUNLekNDQWljR0NTcUdTSWIzRFFFSEJxQ0NBaGd3Z2dJVUFnRUFNSUlDRFFZSktvWklodmNOQVFjQk1CNEdDV0NHU0FGbEF3UUJMakFSQkF6OWdqWlJIVmNndGFBbHZ1a0NBUkNBZ2dIZVlZYkRmaXVUMXpQZHRobll3am40MkhVRjlrOXN5THVMeHpvSWRsMEY4NElJYnpRTkZaL2YyZnREdXhld3Y2K2VVZUFlZTFBdEJFTng5aTF6Q3AwdStiUHllTDBqMWUyYWttZVh6bzRzK1AyMnREUVlEMHZTMkl1djh6UGNHUkc3VFBicmVOSWN2V3pLaStxSmk2ZjVnYU0yWCtUM3R1M0dpOTZPdm9NSjBKTnorcFBaY3lCNWdjZ1hBc0NqVXFzZk1MTDdOUmhjQS9KZkdiVXU4VWhUbzZGdERtMUxsNVFBdFpFSklnUkFYYWlyS1owVGRtQWdyT2Z3aHpVM1lpaE1sZktaYUhqaDBja2NGM3JxOFczeE1RVmFFQjFUak1zdGs5WElKRjdlMEZmc3VhYlNqc2UxdXFkb040dnJWNDMrWDZ6OXlwUWlGdVFkYWNTamRVdy8vUHFPS3ZIWjBlRVgrN1FxWDdnWC9KNGJSY2JZV2djOGpTRHpHY1haQUNGa2NwT0U0SnJDS0c0S3VSTnRhZkw2aElFMy80TEdSeVVmRWF2amxEaEdTenNFRWd3WWl2UC85SEYrMXUxOURXYXVYQ1NJSGxaYUlZc0hndlZEYkpORjFZUnJEN2RrSE1IN1lEY0xxK0dZWFRmem40RXhyWktHS1crWnNUMDhvS2hsRmpsQ3pOVEwwUmVkZ1dxUnB6YkZaVEluY09jS0FxTXN6UVc5MVFIYldoUTl6YzVqL0RjQzhidmo0ZTdCZm11S0MzUVVmMG9ybVpWbC9jTFZwdWJRVm1FTmZwZ3I5c1JEUmk0N3M4bEZDRFZyU0ovRmhTWlNGOUw3ZjJTOGxRPT0ifQ==',
            },
          },
        };
      });
      kalturaCredentials = shallow(<KalturaCredentials />);
    });
    test('kaltura credentials rendered', (done) => {
      const partnerIdInput = jest.fn();
      setImmediate(() => {
        kalturaCredentials
          .find('FormControl')
          .at(0)
          .simulate('change', { partnerIdInput, target: { value: '2503822' } });
        expect(partnerIdInput.mock).toBeCalled;
        done();
      });
    });
    describe('when click save button', () => {
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(arg => {
          return {
            data: {
              updateKalturaConfig: {
                id: '55be43c9-6654-44e7-b2b9-dfeb9e57528a',
                ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                partnerID: '2503822',
                appToken: 'a874d20102438b678951e99977dad590',
                tokenID: '0_9pqjpyw0',
                createdAt: '2020-09-17T18:02:00.457Z',
                updatedAt: '2020-12-22T09:20:35.395Z',
              },
            },
          };
        });
      });
      test('updated credentials saved', () => {
        const handleUpdate = jest.fn();
        kalturaCredentials.find('Button').simulate('click', handleUpdate);
        expect(handleUpdate.mock).toBeCalled;
      });
    });
  });
  describe('when kaltura credentials API returns empty result', () => {
    let kalturaCredentials: any;
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(arg => {
        return {
          data: {
            listKalturaConfigs: {
              items: [],
            },
          },
        };
      });
      kalturaCredentials = shallow(<KalturaCredentials />);
    });
    test('kaltura credentials rendered', () => {
      expect(kalturaCredentials.find('FormControl').at(0).props().value).toEqual(undefined);
    });
    describe('when click save button', () => {
      test('new credentials created', () => {
        const handleCreate = jest.fn();
        kalturaCredentials.find('Button').simulate('click', handleCreate);
        expect(handleCreate.mock).toBeCalled;
      });
    });
  });
  describe('when changes partner id', () => {
    test('partner id is changed', () => {
      const partnerIdInput = jest.fn();
      kalturaCredentials
        .find('FormControl')
        .at(0)
        .simulate('change', { partnerIdInput, target: { value: '2503822' } });
      expect(partnerIdInput.mock).toBeCalled;
    });
  });
  describe('when changes app token', () => {
    test('app token is changed', () => {
      const appTokenIdInput = jest.fn();
      kalturaCredentials
        .find('FormControl')
        .at(1)
        .simulate('change', { appTokenIdInput, target: { value: 'a874d20102438b678951e99977dad591' } });
      expect(appTokenIdInput.mock).toBeCalled;
    });
  });
  describe('when changes token id', () => {
    test('token id is changed', () => {
      const tokenIdInput = jest.fn();
      kalturaCredentials
        .find('FormControl')
        .at(2)
        .simulate('change', { tokenIdInput, target: { value: '0_9pqjpyw1' } });
      expect(tokenIdInput.mock).toBeCalled;
    });
  });
});
