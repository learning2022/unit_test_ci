/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { canvasClient } from '../../../graphql/queries';
import ToastComponent from '../../components/ToastComponent';
import Loading from '../../../components/loader/loading';
import { Form, Row, Col, Alert, InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { translate } from '../../../i18n/translate';

class LmsUserCredentials extends React.Component {
  state = {
    data: null as any,
    apiData: null,
    platformUrl: '',
    clientId: '',
    clientSecret: '',
    apiClientId: '',
    apiSecretKey: '',
    showToast: false,
    hiddenCanvasDomain: true,
    hiddenLtiClientId: true,
    hiddenLtiClientSecret: true,
    hiddenApiClientId: true,
    hiddenApiClientSecret: true,
    toastMessage: '',
    loading: true,
    saveText: `${translate('global.save')}`,
    error: false,
  };

  async componentDidMount() {
    let response: any = await API.graphql(
      graphqlOperation(canvasClient, { platformId: sessionStorage.getItem('ltiPlatformId') }),
    );
    response = JSON.parse(response.data.canvasClient);
    if (response.statusCode === 200) {
      this.setState({ data: response.body, loading: false });
    } else {
      this.setState({ error: true, loading: false });
    }
  }

  handleLabelName = () => {
    if (sessionStorage.getItem('domainName') === 'canvas') {
      return translate('admin.canvasdomain');
    } else if (sessionStorage.getItem('domainName') === 'D2l') {
      return translate('admin.brightspacedomain');
    } else if (sessionStorage.getItem('domainName') === 'BB') {
      return translate('admin.blackboarddomain');
    }
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  render() {
    return (
      <>
        {this.state.loading ? (
          <Loading />
        ) : this.state.error ? (
          <h1>{translate('error.text')}</h1>
        ) : (
          <Form className='mt-3'>
            <Alert variant='info'>{translate('error.noeditmessage')}</Alert>
            <Form.Group className='pl-2' as={Row}>
              <Form.Label column sm='3'>
                {this.handleLabelName()}
              </Form.Label>
              <Col sm='8'>
                <InputGroup className='mb-2'>
                  <Form.Control
                    type={this.state.hiddenCanvasDomain ? 'password' : 'text'}
                    disabled
                    placeholder={translate('admin.canvasdomain_placeholder')}
                    defaultValue={
                      this.state.data ? this.state.data.platformUrl : this.state.platformUrl
                    }
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text>
                      <FontAwesomeIcon
                        icon={this.state.hiddenCanvasDomain ? faEyeSlash : faEye}
                        onClick={() =>
                          this.setState({ hiddenCanvasDomain: !this.state.hiddenCanvasDomain })
                        }
                      />
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group className='pl-2' as={Row}>
              <Form.Label column sm='3'>
                {translate('admin.lticlientid')}
              </Form.Label>
              <Col sm='8'>
                <InputGroup className='mb-2'>
                  <Form.Control
                    type={this.state.hiddenLtiClientId ? 'password' : 'text'}
                    disabled
                    placeholder={translate('admin.lticlientid_placeholder')}
                    defaultValue={this.state.data ? this.state.data.clientId : this.state.clientId}
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text>
                      <FontAwesomeIcon
                        icon={this.state.hiddenLtiClientId ? faEyeSlash : faEye}
                        onClick={() =>
                          this.setState({ hiddenLtiClientId: !this.state.hiddenLtiClientId })
                        }
                      />
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group className='pl-2' as={Row}>
              <Form.Label column sm='3'>
                {translate('admin.lticlientsecret')}
              </Form.Label>
              <Col sm='8'>
                <InputGroup className='mb-2'>
                  <Form.Control
                    type={this.state.hiddenLtiClientSecret ? 'password' : 'text'}
                    disabled
                    placeholder={translate('admin.lticlientsecret_placeholder')}
                    defaultValue={
                      this.state.data ? this.state.data.clientSecret : this.state.clientSecret
                    }
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text>
                      <FontAwesomeIcon
                        icon={this.state.hiddenLtiClientSecret ? faEyeSlash : faEye}
                        onClick={() =>
                          this.setState({
                            hiddenLtiClientSecret: !this.state.hiddenLtiClientSecret,
                          })
                        }
                      />
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group className='pl-2' as={Row}>
              <Form.Label column sm='3'>
                {translate('admin.apiclientid')}
              </Form.Label>
              <Col sm='8'>
                <InputGroup className='mb-2'>
                  <Form.Control
                    type={this.state.hiddenApiClientId ? 'password' : 'text'}
                    disabled
                    placeholder={translate('admin.apiclientid_placeholder')}
                    defaultValue={
                      this.state.data ? this.state.data.apiClientId : this.state.apiClientId
                    }
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text>
                      <FontAwesomeIcon
                        icon={this.state.hiddenApiClientId ? faEyeSlash : faEye}
                        onClick={() =>
                          this.setState({ hiddenApiClientId: !this.state.hiddenApiClientId })
                        }
                      />
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group className='pl-2' as={Row}>
              <Form.Label column sm='3'>
                {translate('admin.apiclientsecret')}
              </Form.Label>
              <Col sm='8'>
                <InputGroup className='mb-2'>
                  <Form.Control
                    type={this.state.hiddenApiClientSecret ? 'password' : 'text'}
                    disabled
                    placeholder={translate('admin.apiclientid_placeholder')}
                    defaultValue={
                      this.state.data ? this.state.data.apiSecretKey : this.state.apiSecretKey
                    }
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text>
                      <FontAwesomeIcon
                        icon={this.state.hiddenApiClientSecret ? faEyeSlash : faEye}
                        onClick={() =>
                          this.setState({
                            hiddenApiClientSecret: !this.state.hiddenApiClientSecret,
                          })
                        }
                      />
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                </InputGroup>
              </Col>
            </Form.Group>
          </Form>
        )}

        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
      </>
    );
  }
}

export default LmsUserCredentials;
