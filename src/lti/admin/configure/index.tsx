import React, { useState, useEffect } from 'react';
import { Button, Tabs, Tab, Accordion } from 'react-bootstrap';
import { isEmpty } from 'lodash';

import LmsUserCredentials from './lmsCredentials';
import KalturaCredentials from './kalturaCredentials';
import '../adminDashboard.scss';
import CreateLTIToolModal from './createLTIToolModal';
import ExternalLTITool from './externalLTITool';
import { LtiTool } from '../../../loree-editor/interface';
import { isCanvas, isBB } from '../../../lmsConfig';
import { translate } from '../../../i18n/translate';
import { captureErrorLog } from '../../../loree-editor/utils';
import { API, graphqlOperation } from 'aws-amplify';
import { ListLtiToolsQuery } from '../../../API';
import { listLtiTools } from '../../../graphql/queries';

export const Configure = () => {
  const [activeAccordion, setActiveAccordion] = useState<number | null>(null);

  const [showCreateLTIToolModal, setShowCreateLTIToolModal] = useState(false);

  const [externalLTIToolsConfigurationList, setExternalLTIToolsConfigurationList] = useState<
    LtiTool[]
  >([]);

  const [isAddExternalToolDisabled, setIsAddExternalToolDisabled] = useState(false);

  const handleSetIsAddExternalToolDisabled = (val: boolean) => setIsAddExternalToolDisabled(val);

  const handleAddExternalLTIToolsConfiguration = () => {
    const newExternalLTIToolsConfigurationList = [
      ...externalLTIToolsConfigurationList,
      {
        toolName: '',
        issuerUrl: '',
        oidcUrl: '',
        redirectURI: '',
        targetLinkURI: '',
        jwksUrl: '',
        domainName: '',
      } as LtiTool,
    ];
    setExternalLTIToolsConfigurationList(newExternalLTIToolsConfigurationList);
    setIsAddExternalToolDisabled(true);
  };

  const handleCancelExternalLTITool = () => {
    externalLTIToolsConfigurationList.pop();
    setExternalLTIToolsConfigurationList(externalLTIToolsConfigurationList);
    setIsAddExternalToolDisabled(false);
  };

  const toggleCreateLTIToolModal = () => {
    setShowCreateLTIToolModal((prev: boolean) => !prev);
  };

  const handleActiveAccordion = (index: number) => {
    const active = activeAccordion === index ? null : index;
    setActiveAccordion(active);
  };

  const handleLabelName = () => {
    if (sessionStorage.getItem('domainName') === 'canvas') {
      return 'Canvas User Credentials';
    } else if (sessionStorage.getItem('domainName') === 'D2l') {
      return 'Brightspace User Credentials';
    } else if (sessionStorage.getItem('domainName') === 'BB') {
      return 'Blackboard User Credentials';
    }
  };

  const getInitialExternalLTIToolsConfigurationList = async (): Promise<LtiTool[]> => {
    const result = await API.graphql<ListLtiToolsQuery>(graphqlOperation(listLtiTools, {}));
    setExternalLTIToolsConfigurationList(result.data?.listLtiTools?.items as LtiTool[]);
    return result.data?.listLtiTools?.items as LtiTool[];
  };

  useEffect(() => {
    getInitialExternalLTIToolsConfigurationList().catch((error) => {
      captureErrorLog('fetching lti tool list', error);
    });
  }, []);

  const showExternalLtiTool = () => {
    if (process.env.REACT_APP_ENABLE_EXTERNAL_LTI_TOOL === 'true' && isCanvas()) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <>
      <h3 className='text-primary mb-3'>Configure</h3>
      <Tabs defaultActiveKey='0'>
        <Tab eventKey='0' className='pl-0 mx-3' title={handleLabelName()}>
          <LmsUserCredentials />
        </Tab>
        {showExternalLtiTool() && (
          <Tab eventKey='1' title='External Tools'>
            <>
              <Button
                className='mx-1 px-3 mt-3 add-external-tool-button'
                size='sm'
                onClick={toggleCreateLTIToolModal}
                disabled={isAddExternalToolDisabled}
              >
                {translate('externaltools.newexternaltool')}
              </Button>
              <p className='mx-1 pt-2 text-muted external-tool-support-note'>
                {translate('externaltools.ltiversion')}
              </p>
              <CreateLTIToolModal
                showCreateLTIToolModal={showCreateLTIToolModal}
                toggleCreateLTIToolModal={toggleCreateLTIToolModal}
                handleAddExternalLTIToolsConfiguration={handleAddExternalLTIToolsConfiguration}
              />
              <Accordion className='admin-accordion tab-container'>
                {!isEmpty(externalLTIToolsConfigurationList) &&
                  externalLTIToolsConfigurationList.map(
                    (externalToolConfig: LtiTool, index: number) => {
                      return (
                        <ExternalLTITool
                          key={`${index}-${externalToolConfig.toolName}`}
                          isActiveAccordion={activeAccordion === index}
                          handleActiveAccordion={() => handleActiveAccordion(index)}
                          externalToolConfig={externalToolConfig}
                          setIsAddExternalToolDisabled={handleSetIsAddExternalToolDisabled}
                          index={index}
                          handleCancelExternalLTITool={handleCancelExternalLTITool}
                        />
                      );
                    },
                  )}
              </Accordion>
            </>
          </Tab>
        )}
        {(isCanvas() || isBB()) && ( // For Now we enable kaltura only for canvas and BB lms
          <Tab eventKey='2' title='Kaltura'>
            <KalturaCredentials />
          </Tab>
        )}
      </Tabs>
    </>
  );
};
