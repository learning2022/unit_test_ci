import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import axe from '../../../axeHelper';

import CreateLTIToolModal from './createLTIToolModal';

describe('createLTIToolModal', () => {
  const props = {
    showCreateLTIToolModal: true,
    toggleCreateLTIToolModal: jest.fn(),
    handleAddExternalLTIToolsConfiguration: jest.fn(),
  };
  test('loads and displays modal for selecting external LTI tool type', async () => {
    render(<CreateLTIToolModal {...props} />);
    expect(screen.getByText('Pick your Tool Type')).toBeInTheDocument();
    expect(screen.getByText('Other')).toBeInTheDocument();
    expect(screen.getByTestId('button')).toBeInTheDocument();
  });

  test('should pass accessibility tests', async () => {
    const createLTIToolModal = render(<CreateLTIToolModal {...props} />);
    const results = await axe(createLTIToolModal.container);
    expect(results).toHaveNoViolations();
  });

  test('should call handling for creating external tool', async () => {
    render(<CreateLTIToolModal {...props} />);
    const button = screen.getByTestId('button');
    screen.getByRole('button', {
      name: /Add/i,
    });
    fireEvent.click(button);
    expect(props.handleAddExternalLTIToolsConfiguration).toHaveBeenCalledTimes(1);
    expect(props.toggleCreateLTIToolModal).toHaveBeenCalledTimes(1);
  });
});
