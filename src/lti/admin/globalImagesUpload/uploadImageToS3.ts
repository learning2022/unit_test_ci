import { API, graphqlOperation } from 'aws-amplify';
import { imageS3Upload } from '../../../graphql/mutations';
import { refreshLoreeImage } from '../../../loree-editor/modules/imageModal/imageModalUtils';
import { saveToCanvas, getImageUrl, uploadLmsCourseImageToBB } from '../../../utils/imageUpload';
import { LmsAccess } from '../../../utils/lmsAccess';
import { ImageS3UploadMutation } from '../../../API';
import { getS3Key } from '../../../utils/imageUtil';

export const uploadToCanvas = async (globalHtmlContent: string) => {
  const s3ImageUrls = getS3ImageSource(globalHtmlContent);
  if (s3ImageUrls.length > 0) {
    const imageTitles = getS3ImageTitle(globalHtmlContent);
    const canvasImageUrls = await getCanvasImageUrls(s3ImageUrls, imageTitles);
    const updatedContent = parseHtmlString(globalHtmlContent, s3ImageUrls, canvasImageUrls, [
      `${sessionStorage.getItem('ltiPlatformId')}`,
    ]);
    return updatedContent;
  } else {
    return globalHtmlContent;
  }
};

export const getS3ImageSource = (HtmlContent: string) => {
  let image;
  const imageSrcUrls = [];
  const rex = /<img.*?src="(.*?)"/g;
  while ((image = rex.exec(HtmlContent))) {
    const updateImage = image[1].replace(/&amp;/g, '&');
    const platformId = image[1].includes(`${sessionStorage.getItem('ltiPlatformId')}`);
    if (platformId) {
      imageSrcUrls.push(updateImage);
    }
  }
  return imageSrcUrls;
};

export const getS3ImageTitle = (HtmlContent: string) => {
  let image;
  const imageTitles: Array<string> = [];
  const rex = /<img.*?alt="(.*?)"[^>]+>/g;
  while ((image = rex.exec(HtmlContent))) {
    const platformId = image[0].includes(`${sessionStorage.getItem('ltiPlatformId')}`);
    if (platformId) {
      imageTitles.push(image[1]);
    }
  }
  return imageTitles;
};
export const getCanvasImageUrls = async (images: Array<string>, imageTitles: Array<string>) => {
  const s3UrlArray: Array<string> = [];
  for (const [index, image] of images.entries()) {
    let separateName = `${imageTitles[index]}`;
    let imageUrl: _Any;
    if (sessionStorage.getItem('domainName') === 'canvas') {
      const newS3Url = await getNewImageS3URL(image);
      imageUrl = await saveToCanvas('image/jpeg', newS3Url, separateName);
      imageUrl = JSON.parse(imageUrl.data.imageUpload);
      if (imageUrl.url) {
        s3UrlArray.push(imageUrl.url);
      }
    } else {
      if (image.includes('https://loreev2storage')) {
        const finalKey: Array<string> | null = getS3Key(image);
        if (finalKey) separateName = finalKey[4];
      }
      imageUrl = await uploadLmsCourseImageToBB(image, separateName, 'globalUpload');
      s3UrlArray.push(imageUrl);
    }
  }
  return s3UrlArray;
};
export const uploadToS3 = async (globalHtmlContent: string, type: string, platformId?: string) => {
  const platform = sessionStorage.getItem('ltiPlatformId')
    ? sessionStorage.getItem('ltiPlatformId')
    : platformId;
  const domainName = sessionStorage.getItem('lmsUrl') as string;
  const canvasImageUrls = await getImageSource(globalHtmlContent, type, platform, domainName);
  if (canvasImageUrls.length > 0) {
    const lms = new LmsAccess();
    const imageTitles = getImageTitle(globalHtmlContent, type);
    const s3ImageUrls = await getS3ImageUrls(canvasImageUrls, imageTitles, type, platformId);
    const lmsIdentifier = !lms.getAccess() ? ['wysiwyg'] : [domainName];
    if (type.includes('Global-')) {
      lmsIdentifier.push(platform as string);
    }
    const updatedContent = parseHtmlString(
      globalHtmlContent,
      canvasImageUrls,
      s3ImageUrls,
      lmsIdentifier,
    );
    await refreshLoreeImage();
    return updatedContent;
  } else {
    return globalHtmlContent;
  }
};

export const getImageSource = async (
  HtmlContent: string,
  type: string,
  platformId: string | null | undefined,
  domainName: string,
) => {
  let image;
  let imageSrcUrls: _Any = [];
  const rex = /<img.*?src="(.*?)"/g;
  while ((image = rex.exec(HtmlContent))) {
    const updateImage = image[1].replace(/&amp;/g, '&');
    if (image[1].includes(domainName) || image[1].includes('wysiwyg')) {
      imageSrcUrls.push(updateImage);
    } else if (type.includes('Global-')) {
      imageSrcUrls = await getImageSrcBasedOnUrl(image[1], updateImage, type, platformId);
    }
  }
  return imageSrcUrls;
};

export const getImageSrcBasedOnUrl = async (
  image: string,
  updateImage: string,
  type: string,
  platformId: string | null | undefined,
) => {
  const imageSrcUrls = [];
  let imageUrl;
  if (image.includes('https://loreev2storage') && !image[1].includes(type)) {
    const s3Key: Array<string> | null = getS3Key(updateImage);
    const imageKey = s3Key ? `${s3Key[2]}/${s3Key[3]}/${s3Key[4]}` : '';
    imageUrl = await getImageUrl(decodeURI(imageKey));
    imageSrcUrls.push(imageUrl);
  } else if (!image.includes('https://loreev2storage') && image.includes(platformId as string)) {
    imageUrl = await getImageUrl(updateImage);
    imageSrcUrls.push(imageUrl);
  }
  return imageSrcUrls;
};

export const getNewImageS3URL = async (image: string) => {
  let imageUrl: Object | null;
  if (image.includes('https://loreev2storage')) {
    const s3Key: Array<string> | null = getS3Key(image);
    const imageKey = s3Key ? `${s3Key[2]}/${s3Key[3]}/${s3Key[4]}` : '';
    imageUrl = await getImageUrl(decodeURI(imageKey));
    return imageUrl;
  }
  return image;
};

export const getImageTitle = (HtmlContent: string, type: string) => {
  let image;
  let imageTitles: Array<string> = [];
  if (HtmlContent.includes('wysiwyg')) {
    const imageName = getMigratedTemplateImageTitle(HtmlContent) as _Any;
    imageTitles = imageName;
  } else {
    const rex = /<img.*?alt="(.*?)"[^>]+>/g;
    while ((image = rex.exec(HtmlContent))) {
      if (
        image[0].includes('instructure') ||
        image[0].includes('lms.educationapps.vic.gov.au') ||
        image[0].includes('blackboard')
      ) {
        imageTitles.push(image[1]);
      } else if (type.includes('Global-') && !image[1].includes(type)) {
        imageTitles.push(image[1]);
      }
    }
  }
  return imageTitles;
};

export const getMigratedTemplateImageTitle = (HtmlContent: string) => {
  const rex = /<img.*?src="(.*?)"/g;
  let image;
  const imageTitles: Array<string> = [];
  while ((image = rex.exec(HtmlContent))) {
    const name = image[1]?.split('/')?.pop();
    imageTitles.push(name as string);
  }
  return imageTitles;
};

const getS3ImageUrls = async (
  images: Array<string>,
  imageTitles: Array<string>,
  type: string,
  platform?: string,
) => {
  const s3UrlArray: Array<string> = [];
  const platformId: string | null | undefined = sessionStorage.getItem('ltiPlatformId')
    ? sessionStorage.getItem('ltiPlatformId')
    : platform;
  for (const [index, image] of images.entries()) {
    const imageMetaElements = imageTitles[index].split('.');
    const imageNameElements: _Any = imageMetaElements[0].split('-');
    const timestamp = Date.now();
    let validImageNametoBeUploaded = getImageName(
      imageMetaElements[1],
      timestamp,
      imageNameElements,
    );

    for (const [index, element] of imageNameElements.entries()) {
      if (element?.length === 13 && new Date(parseInt(element)).getTime() > 0) {
        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        imageNameElements.slice(index + 1);
        imageNameElements[index] = timestamp;
        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        validImageNametoBeUploaded = imageNameElements.join('-') + '.' + imageMetaElements[1];
      }
    }

    const list = await API.graphql<ImageS3UploadMutation>(
      graphqlOperation(imageS3Upload, {
        platformId: platformId,
        path: image,
        title: validImageNametoBeUploaded,
        type: type,
      }),
    );
    s3UrlArray.push((list.data?.imageS3Upload as string).replace(/"/g, ''));
  }

  return s3UrlArray;
};

export const getImageName = (
  imageMetaElements: string | null,
  timestamp: string | number,
  imageNameElements: string[],
) => {
  let name = imageNameElements.join('-') + '-' + timestamp;
  if (imageMetaElements) {
    name += '.' + imageMetaElements;
  }
  return name;
};

export const getS3urlFromKey = async (globalHtmlContent: string) => {
  const s3ImageKeys = getS3ImageSource(globalHtmlContent);
  if (s3ImageKeys.length > 0) {
    const s3ImageUrls = await getS3SignedUrls(s3ImageKeys);
    const updatedContent = parseHtmlString(globalHtmlContent, s3ImageKeys, s3ImageUrls, [
      `${sessionStorage.getItem('ltiPlatformId')}`,
    ]);
    return updatedContent;
  } else {
    return globalHtmlContent;
  }
};

export const getS3SignedUrls = async (s3ImageKeys: Array<string>) => {
  const s3UrlArray = [];
  let imageUrl: Object | null;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  for (const [index, image] of s3ImageKeys.entries()) {
    if (image.includes('https://loreev2storage')) {
      const s3Key: Array<string> | null = getS3Key(image);
      const imageKey = s3Key ? `${s3Key[2]}/${s3Key[3]}/${s3Key[4]}` : '';
      imageUrl = await getImageUrl(decodeURI(imageKey));
    } else {
      imageUrl = await getImageUrl(image);
    }
    s3UrlArray.push(imageUrl);
  }
  return s3UrlArray;
};

export const parseHtmlString = (
  yourHtmlString: string,
  canvasImageUrls: Array<string>,
  s3ImageUrls: _Any,
  includedStrings: string[],
) => {
  const element = document.createElement('div');
  element.innerHTML = yourHtmlString;
  const imgSrcUrls = element.getElementsByTagName('img');
  const replaceUrls: HTMLImageElement[] = [];
  for (const includedString of includedStrings) {
    for (const url of imgSrcUrls) {
      if (url.hasAttribute('src')) {
        const replaceUrl = url.getAttribute('src');
        if (replaceUrl?.includes(includedString)) {
          replaceUrls.push(url);
        }
      }
    }
  }
  canvasImageUrls.forEach((canvasImage: string, index: number) => {
    const urlValue = replaceUrls[index]?.getAttribute('src')?.replace(/&amp;/g, '&');
    canvasImage = canvasImage.replace(/&amp;/g, '&');
    if (
      urlValue?.split('?')[0] === canvasImage.split('?')[0] ||
      canvasImage.includes(urlValue as string)
    ) {
      replaceUrls[index].setAttribute('src', s3ImageUrls[index]);
    }
  });
  return element.innerHTML;
};
