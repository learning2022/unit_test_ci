import * as uploadImageToS3 from './uploadImageToS3';
import { StorageMock } from '../../../utils/storageMock';
import {
  expectedHtmlData,
  migratedTempalteDataWithImage,
  migratedTempalteDataWithOutImage,
  sampleHtmlData,
  sampleImageUrl,
  storageResultMockData,
  globalHtmlContentMockData,
  pageContentInputDET,
  pageContentOutputDET,
  uploadToS3Mock,
  globalHtmlContentMockDataWithS3Url,
  uploadImageResultMockData,
  s3ToCanvasFrontendApiResult,
  s3ToCanvasFrontendCodeResult,
} from './uploadImageMockData';
import { Storage } from 'aws-amplify';
import API from '@aws-amplify/api';

describe('#upload image to s3', () => {
  beforeEach(async () => {
    global.sessionStorage = new StorageMock() as any;
    sessionStorage.setItem('ltiPlatformId', '37212122-2c37-462f-8a61-d68632463bd7');
  });
  describe('#getS3ImageSrc', () => {
    const data =
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div>';
    test('image is not present in content', () => {
      expect(uploadImageToS3.getS3ImageSource('<p>hello</p>')).toEqual([]);
    });
    test('image is present and returns image keys', () => {
      expect(uploadImageToS3.getS3ImageSource(data)).toEqual([
        '37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png',
      ]);
    });
  });
  describe('#getS3ImageTitle', () => {
    const data =
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div>';
    test('image is not present in content', () => {
      expect(uploadImageToS3.getS3ImageTitle('<p>hello</p>')).toEqual([]);
    });
    test('image is present and returns image title', () => {
      expect(uploadImageToS3.getS3ImageTitle(data)).toEqual(['images.png']);
    });
  });
  describe('#parseHtmlString', () => {
    const data =
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png"></div></div></div> ';
    const imgSrc = [
      '37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png',
    ];
    const includedString = ['37212122-2c37-462f-8a61-d68632463bd7'];
    const imgUrl = [
      'https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Credential=ASIATHBL7ES3I4IXZZ4U%2F20210624%2Fap-southeast-2%2Fs3%2Faws4_request&X-Amz-Date=20210624T062046Z&X-Amz-Expires=900&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDYaDmFwLXNvdXRoZWFzdC0yIkcwRQIhAOI1Nsq80ANt7CPV8%2Bf%2F4ZHj2yiOpXYukOJdq2QjLyviAiBAjaY3NbWC5%2BbQFioAutOA7wmynH%2F%2F0ZAijtnEav9sBSrrBAj%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAIaDDIyMTI4MzAzNDI5NCIM4hvHy2MUfAvvn6AlKr8E5efcNBmi7i6Z1ocl6zUPRwi6NIdnf9ZIW%2BJSED7lEG7hEpDTWl3bsoQvg%2BxzZpoKWTriP8laSXxN%2BhRtEoIm9MXyIJrHm%2Be4Ry%2BXYR9C3Sjb4hucitmh%2FTuDSUe%2F88O00%2B3UPaUHjdHONKjwOkWT9dgZXrTG2jD8BVL8HZfZ14PxnI%2BPpFeb3oUsZFrNcaquEpf81pFFvo98v094qRWLXyj2ZrI9tABRyUlWxkL%2FbIU8Xdy7FBBg9Yts%2FOPu1qx99GA%2FWMaQT2hhKiFWpQBxpdAGriXdK4cVKRgSXBm%2FfC2OIu7%2BuNaffdBsuXkmlJSXJDgX2wKIvbGNwmbaZhuX3d4nN0vQj98oW2POdv2xrJ%2Bn%2Bri8KItV7O1ixQUr1pXZPpzqwgcTsGE7NqGO8qv4yy8Tk5KY6zv8%2FAX%2FieGoFqMZ6ylwYFF%2B%2FNFbyW3FsZxosgIhJ8dgeXEVEgIRzpcQIzoLgn9lgReGHAXGXm3gbZ44WBRnjPnp1qikEZu2aEpQtHNIoHIWpgt5gz7fk4vmwWzXU%2BZPX75gbwKm%2BXreIIToKx6FFObPMAOqPs%2BleZRiV3mPId%2B9EDQ9D34zW5c4NFfeO4tL9MiCywaoRtSjHanFvi09Oyr5yF6OSYNbiQqpiSpxMxs4TBUcbCPGyAmwTOtXsL1WbAkBM6AaO07U%2F51JTMLdvVCJKsi8m0mAFXjO32L2bvyXhmUJnMs%2BUWpC8T9zWCQXBuSbRpiFFeCtOiNHOZQKIYd5PjQ%2FBJCgQcMwqcjQhgY6hQI3FgV0YbQNYXaBWBKLhUqy344yX3a4UVcUNhjrluid5ZP6hYTtyfLnUPWNYSX1JZY9yNYBCN3Rx80US%2BRfuxP9LkHJ4sapxsoLU8jQUFaxap9I4JwluxDRtGzzPM1D%2Fxee2SSiuMwrmWgO8V329VJffqI%2FzRY17vEhNryC3vUNh1prtUX6PoQSBgDCPKkjqxWSaDQfxylRcKU014xX6VOP%2FngiuiorBDwDiy1Rb2C61v9AJYAYP2yhWVeK2D4Ul5vpNgf8jeHGn%2FH%2BacIN%2FI95%2BLUE9DZ6veTqgQrf4JrKmTwzm%2F2wJeql5xDAsRm5i8byH0e4JXJRy0T62BJx3HMVvfvmENM%3D&X-Amz-Signature=030ccff22fcd920f87efffdb86a6ec1b685bc51256b01fbb0f68c0dd8f20cc7a&X-Amz-SignedHeaders=host&x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Linux%20x86_64%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F90.0.4430.93%20Safari%2F537.36%20aws-amplify%2F3.8.1%20js&x-id=GetObject',
    ];
    test('parse html content', () => {
      expect(uploadImageToS3.parseHtmlString(data, imgSrc, imgUrl, includedString)).toEqual(
        '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper"><img alt="images.png" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://loreev2storage24cd6295054c4df4b5661676008c768e160442-lmsdev.s3.ap-southeast-2.amazonaws.com/public/37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Credential=ASIATHBL7ES3I4IXZZ4U%2F20210624%2Fap-southeast-2%2Fs3%2Faws4_request&amp;X-Amz-Date=20210624T062046Z&amp;X-Amz-Expires=900&amp;X-Amz-Security-Token=IQoJb3JpZ2luX2VjEDYaDmFwLXNvdXRoZWFzdC0yIkcwRQIhAOI1Nsq80ANt7CPV8%2Bf%2F4ZHj2yiOpXYukOJdq2QjLyviAiBAjaY3NbWC5%2BbQFioAutOA7wmynH%2F%2F0ZAijtnEav9sBSrrBAj%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAIaDDIyMTI4MzAzNDI5NCIM4hvHy2MUfAvvn6AlKr8E5efcNBmi7i6Z1ocl6zUPRwi6NIdnf9ZIW%2BJSED7lEG7hEpDTWl3bsoQvg%2BxzZpoKWTriP8laSXxN%2BhRtEoIm9MXyIJrHm%2Be4Ry%2BXYR9C3Sjb4hucitmh%2FTuDSUe%2F88O00%2B3UPaUHjdHONKjwOkWT9dgZXrTG2jD8BVL8HZfZ14PxnI%2BPpFeb3oUsZFrNcaquEpf81pFFvo98v094qRWLXyj2ZrI9tABRyUlWxkL%2FbIU8Xdy7FBBg9Yts%2FOPu1qx99GA%2FWMaQT2hhKiFWpQBxpdAGriXdK4cVKRgSXBm%2FfC2OIu7%2BuNaffdBsuXkmlJSXJDgX2wKIvbGNwmbaZhuX3d4nN0vQj98oW2POdv2xrJ%2Bn%2Bri8KItV7O1ixQUr1pXZPpzqwgcTsGE7NqGO8qv4yy8Tk5KY6zv8%2FAX%2FieGoFqMZ6ylwYFF%2B%2FNFbyW3FsZxosgIhJ8dgeXEVEgIRzpcQIzoLgn9lgReGHAXGXm3gbZ44WBRnjPnp1qikEZu2aEpQtHNIoHIWpgt5gz7fk4vmwWzXU%2BZPX75gbwKm%2BXreIIToKx6FFObPMAOqPs%2BleZRiV3mPId%2B9EDQ9D34zW5c4NFfeO4tL9MiCywaoRtSjHanFvi09Oyr5yF6OSYNbiQqpiSpxMxs4TBUcbCPGyAmwTOtXsL1WbAkBM6AaO07U%2F51JTMLdvVCJKsi8m0mAFXjO32L2bvyXhmUJnMs%2BUWpC8T9zWCQXBuSbRpiFFeCtOiNHOZQKIYd5PjQ%2FBJCgQcMwqcjQhgY6hQI3FgV0YbQNYXaBWBKLhUqy344yX3a4UVcUNhjrluid5ZP6hYTtyfLnUPWNYSX1JZY9yNYBCN3Rx80US%2BRfuxP9LkHJ4sapxsoLU8jQUFaxap9I4JwluxDRtGzzPM1D%2Fxee2SSiuMwrmWgO8V329VJffqI%2FzRY17vEhNryC3vUNh1prtUX6PoQSBgDCPKkjqxWSaDQfxylRcKU014xX6VOP%2FngiuiorBDwDiy1Rb2C61v9AJYAYP2yhWVeK2D4Ul5vpNgf8jeHGn%2FH%2BacIN%2FI95%2BLUE9DZ6veTqgQrf4JrKmTwzm%2F2wJeql5xDAsRm5i8byH0e4JXJRy0T62BJx3HMVvfvmENM%3D&amp;X-Amz-Signature=030ccff22fcd920f87efffdb86a6ec1b685bc51256b01fbb0f68c0dd8f20cc7a&amp;X-Amz-SignedHeaders=host&amp;x-amz-user-agent=aws-sdk-js-v3-%40aws-sdk%2Fclient-s3%2F1.0.0-gamma.8%20Mozilla%2F5.0%20%28X11%3B%20Linux%20x86_64%29%20AppleWebKit%2F537.36%20%28KHTML%2C%20like%20Gecko%29%20Chrome%2F90.0.4430.93%20Safari%2F537.36%20aws-amplify%2F3.8.1%20js&amp;x-id=GetObject"></div></div></div> ',
      );
    });
  });
  describe('#parseHtmlString with user based data', () => {
    const data = sampleHtmlData;
    const imgSrc = [
      'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Global-Templates/-1637744141937.undefined',
    ];
    const includedString = ['instructure', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a'];
    const imgUrl = [sampleImageUrl];
    test('parse html content', () => {
      expect(uploadImageToS3.parseHtmlString(data, imgSrc, imgUrl, includedString)).toEqual(
        expectedHtmlData,
      );
    });
  });
  describe('#getImageSrc', () => {
    test('image is not present in content', async () => {
      expect(
        await uploadImageToS3.getImageSource(
          '<p>hello</p>',
          'My-Templates',
          '456546546565',
          'https://crystaldelta.instructure.com',
        ),
      ).toEqual([]);
    });
    describe('image is present in content with external url', () => {
      const data =
        '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper "><img alt="download.jpeg" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg"></div></div></div>';
      beforeEach(() => {
        sessionStorage.setItem('ltiPlatformId', '37212122-2c37-462f-8a61-d68632463bd7');
      });
      test('returns image course image src', async () => {
        expect(
          await uploadImageToS3.getImageSource(
            data,
            'My-Templates',
            'i694695939234329',
            'https://crystaldelta.instructure.com',
          ),
        ).toEqual([]);
      });
    });
    describe('image is present in content', () => {
      const data =
        '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper "><img alt="download.jpeg" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://crystaldelta-eval.blackboard.com/bbcswebdav/xid-380431_1?VxJw3wfC56=1624521519&amp;3cCnGYSz89=704rOr1cqVqDhhA0E3xfMT5%2F%2FiBFHR%2FVC0dijzwaaus%3D"></div></div></div>';
      beforeEach(() => {
        sessionStorage.setItem('ltiPlatformId', '37212122-2c37-462f-8a61-d68632463bd7');
      });
      test('returns image course image src', async () => {
        expect(
          await uploadImageToS3.getImageSource(
            data,
            'My-Templates',
            '546465465465475655',
            'https://crystaldelta-eval.blackboard.com',
          ),
        ).toEqual([
          'https://crystaldelta-eval.blackboard.com/bbcswebdav/xid-380431_1?VxJw3wfC56=1624521519&3cCnGYSz89=704rOr1cqVqDhhA0E3xfMT5%2F%2FiBFHR%2FVC0dijzwaaus%3D',
        ]);
      });
    });
  });
  describe('#getImageTitle', () => {
    const data =
      '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding: 10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper "><img alt="download.jpeg" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image undefined" src="https://crystaldelta-eval.blackboard.com/bbcswebdav/xid-380431_1?VxJw3wfC56=1624521519&amp;3cCnGYSz89=704rOr1cqVqDhhA0E3xfMT5%2F%2FiBFHR%2FVC0dijzwaaus%3D"></div></div></div>';
    test('image is not present in content', () => {
      expect(uploadImageToS3.getImageTitle('<p>hello</p>', 'MY-Templates')).toEqual([]);
    });
    test('image is present and returns image course image title', () => {
      expect(uploadImageToS3.getImageTitle(data, 'My-Templates')).toEqual(['download.jpeg']);
    });
  });
  describe('#gets3SignedURl', () => {
    const data = ['37212122-2c37-462f-8a61-d68632463bd7/Global-Templates/images-1624442408092.png'];
    test('image is not present in content', async () => {
      expect(await uploadImageToS3.getS3SignedUrls([])).toEqual([]);
    });
    test('image is present but storage is not configure', async () => {
      expect(await uploadImageToS3.getS3SignedUrls(data)).toEqual([null]);
    });
  });
  describe('#getMigrateTemplateImage', () => {
    test('#imageTitle', () => {
      // need to fix this test case
      expect(uploadImageToS3.getMigratedTemplateImageTitle(migratedTempalteDataWithImage)).toEqual(
        [],
      );
    });
    test('#without image', () => {
      expect(
        uploadImageToS3.getMigratedTemplateImageTitle(migratedTempalteDataWithOutImage),
      ).toEqual([]);
    });
  });
  describe('#getNewImageS3URL', () => {
    const imgUrl = sampleImageUrl;
    const nons3url = 'https://picsum.photos/200';
    test('when it is s3 url', async () => {
      expect(await uploadImageToS3.getNewImageS3URL(imgUrl)).toEqual(null);
    });
    test('when it is not s3 url', async () => {
      expect(await uploadImageToS3.getNewImageS3URL(nons3url)).toEqual(nons3url);
    });
  });
  describe('Upload to S3 functionality current implementation', () => {
    const storageResult = storageResultMockData;
    beforeAll(() => {
      jest.clearAllMocks();
      const listImprovedApiResult = {
        data: { imageS3Upload: 'http://link1,http://link2,http://limk3' },
      };
      API.graphql = jest.fn().mockImplementation(() => listImprovedApiResult);
      Storage.list = jest.fn().mockImplementation(() => storageResult);
    });
    test('#While performing the editor content to save as templates', async () => {
      const globalHtmlContent = globalHtmlContentMockData;
      const type = `d0f5eb54-d676-4d7c-a647-e0fe8298c471`;
      const s3Result = await uploadImageToS3.uploadToS3(globalHtmlContent, type, '');
      expect(s3Result).toBe(globalHtmlContent);
    });
  });
  describe('S3 image title check', () => {
    beforeAll(() => {
      jest.clearAllMocks();
      const listImprovedApiResult = {
        data: { imageS3Upload: 'http://link1,http://link2,http://link3' },
      };
      API.graphql = jest.fn().mockImplementation(() => listImprovedApiResult);
    });
    test('Title scenario for empty file names', async () => {
      const titleResults1 = uploadImageToS3.getImageName('', 54635261, ['dummyTitle']);
      expect(titleResults1).toBe('dummyTitle-54635261');
    });
    test('Title scenario for non-empty file names', async () => {
      const titleResults2 = uploadImageToS3.getImageName('sample_names', 54635261, ['dummyTitle']);
      expect(titleResults2).toBe('dummyTitle-54635261.sample_names');
    });
  });
  describe('upload to S3 for DET instance', () => {
    beforeAll(() => {
      sessionStorage.setItem('lmsUrl', 'https://lms.educationapps.vic.gov.au');
      API.graphql = jest.fn().mockImplementation(() => uploadToS3Mock);
    });
    test('should upload course images into S3', async () => {
      const type = `30d624b5-7bc3-4c50-830e-39cbc4668526`;
      const s3Result = await uploadImageToS3.uploadToS3(pageContentInputDET, type, '');
      expect(s3Result).toBe(pageContentOutputDET);
    });
  });
  describe('S3 to canvas functionality in Save to LMS process current process', () => {
    const globalHtmlContent = globalHtmlContentMockDataWithS3Url;
    beforeAll(() => {
      jest.clearAllMocks();
      sessionStorage.setItem('domainName', 'canvas');
      Storage.list = jest.fn().mockImplementation(() => uploadImageResultMockData);
      API.graphql = jest.fn().mockImplementationOnce(() => s3ToCanvasFrontendApiResult);
    });
    test('While performing the editor content as save to LMS current process', async () => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const updatedContent = await uploadImageToS3.uploadToCanvas(globalHtmlContent);
      expect(updatedContent).toBe(s3ToCanvasFrontendCodeResult);
    });
  });
});
