import React, { useEffect, useState } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCaretUp, faUndo } from '@fortawesome/free-solid-svg-icons';
import Loading from '../../../components/loader/loading';
import { createGlobalBlocks, updateGlobalBlocks } from '../../../graphql/mutations';
import EditModal from './editTemplateModal';
import { ReactComponent as EditPencil } from '../../../assets/Icons/edit.svg';
import { uploadToS3 } from '../globalImagesUpload/uploadImageToS3';
import LmsLoader from '../../../views/editor/saveIcon/saveToLmsLoader';
import { isCanvasAndBB } from '../../../lmsConfig';
import { customBlockMessage } from '../../../loree-editor/modules/customBlocks/customBlockHandler';
import { handleUpdateCustomBlockMutation } from './customTemplateActions';
import { listingGlobalBlocks } from '../../../loree-editor/modules/customBlocks/customBlocksAction';
import { translate } from '../../../i18n/translate';
import { CustomElement } from '../../../loree-editor/customTypes';
import { BlockInfo, CustomElementsProps, StateP } from '../../../loree-editor/interface';

const CustomElements = (props: CustomElementsProps) => {
  const [elementState, setElementState] = useState<StateP>({
    loading: true,
    customElements: [],
    showEditModal: false,
    elementName: '',
    categoryName: '',
    categoryId: '',
    elementId: '',
    filteredTemplate: [],
    disableEditButton: true,
    showToast: false,
    toastMessage: '',
    global: false,
    index: 0,
    active: false,
    savingLoader: false,
    elementNames: 'asc',
    category: 'asc',
    createdBy: 'asc',
    createdAt: 'asc',
    lastUpdated: 'asc',
  });

  useEffect(() => {
    const updatevalue = {
      customElements: props.customElements,
      filteredTemplate: props.filteredTemplate,
      loading: false,
    };
    setElementState({
      ...elementState,
      ...updatevalue,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    props.customElements,
    props.filteredTemplate,
    props.searchValue,
    props.filterValue,
    props.globalFilter,
  ]);

  const openEditModal = async (
    elementId: string,
    elementTitle: string,
    categoryName: string,
    categoryId: string,
    status: boolean,
    index: number,
    active: boolean,
  ) => {
    const updateVal = {
      showEditModal: true,
      elementName: elementTitle,
      elementId: elementId,
      categoryId: categoryId,
      categoryName: categoryName,
      global: status,
      index: index,
      active: active,
    };
    setElementState({
      ...elementState,
      ...updateVal,
    });
  };

  const setEditElementShow = () => {
    setElementState({
      ...elementState,
      disableEditButton: true,
      showEditModal: false,
      categoryId: '',
    });
  };

  const handleEditElement = async () => {
    try {
      const updateInput = {
        id: elementState.elementId,
        title: elementState.elementName,
        categoryID: elementState.categoryId,
        isGlobal: elementState.global,
        active: elementState.active,
      };
      const updatedStatus = await handleUpdateCustomBlockMutation(updateInput);
      const elementData = {
        id: elementState.elementId,
        title: elementState.elementName,
        categoryId: elementState.categoryId,
        isGlobal: elementState.global,
      };
      if (updatedStatus) {
        setEditElementShow();
        if (props.filterValue === '' && props.globalFilter === '' && props.searchValue === '')
          elementFilter(elementData, elementState.categoryName, elementState.index, false);
        else {
          const filterArray = [...props.filteredTemplate];
          filterArray[elementState.index].title = elementData.title;
          filterArray[elementState.index].category.id = elementData.categoryId;
          filterArray[elementState.index].categoryID = elementData.categoryId;
          filterArray[elementState.index].category.name = elementState.categoryName;
          setElementState({
            ...elementState,
            filteredTemplate: filterArray,
          });
          elementFilter(elementData, elementState.categoryName, elementState.index, true);
        }
      }
    } catch (e) {
      console.error('error on updating custom elements');
    }
  };

  const handleCategory = (
    event: React.ChangeEvent<{ value: number; options: [{ innerText: string }] }>,
  ) => {
    event.preventDefault();
    setElementState({
      ...elementState,
      categoryId: event.target.value,
      categoryName: event.target.options[event.target.value].innerText,
      disableEditButton: false,
    });
  };

  const elementFilter = (
    updatedElement: { id: string; title: string; categoryId: string | number; isGlobal: boolean },
    categoryName: string,
    index: number,
    isFiltered: boolean,
  ) => {
    const elementsArray = isFiltered ? elementState.filteredTemplate : elementState.customElements;
    elementsArray[index].title = updatedElement.title;
    elementsArray[index].category.id = updatedElement.categoryId;
    elementsArray[index].categoryID = updatedElement.categoryId;
    elementsArray[index].category.name = categoryName;
    isFiltered
      ? setElementState({
          ...elementState,
          filteredTemplate: elementsArray,
        })
      : setElementState({
          ...elementState,
          customElements: elementsArray,
        });
    setElementState({
      ...elementState,
      showToast: true,
      toastMessage: 'Element updated successfully',
    });
  };

  const rowNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setElementState({ ...elementState, elementName: event.target?.value });
    validateElementSaveButton();
  };

  const validateElementSaveButton = () => {
    if (elementState.elementName.trim().length > 0)
      setElementState({ ...elementState, disableEditButton: false });
    else setElementState({ ...elementState, disableEditButton: true });
  };

  const closeToast = () => {
    setElementState({ ...elementState, showToast: false });
  };

  const changeElementGlobalStatus = async (
    rowId: string,
    elementTitle: string,
    categoryId: string,
    status: boolean,
    index: number,
    active: boolean,
  ) => {
    try {
      setElementState({ ...elementState, savingLoader: true, global: status });
      const updateBlockInput = {
        id: rowId,
        title: elementTitle,
        categoryID: categoryId,
        isGlobal: !status,
        active: active,
      };
      const updatedStatus = await handleUpdateCustomBlockMutation(updateBlockInput);
      if (updatedStatus) {
        let elementsArray;
        if (props.filterValue !== '' || props.globalFilter !== '' || props.searchValue !== '') {
          elementsArray = props.filteredTemplate;
          elementsArray[index].isGlobal = !status;
          elementsArray[index].updatedAt = new Date().toISOString();
          void handleGlobalData(status, elementsArray[index], 'Global-Elements');
          setElementState({
            ...elementState,
            filteredTemplate: elementsArray,
          });
          refreshGlobalFilteredData(index);
        } else {
          elementsArray = elementState.customElements;
          elementsArray[index].isGlobal = !status;
          elementsArray[index].updatedAt = new Date().toISOString();
          void handleGlobalData(status, elementsArray[index], 'Global-Elements');
          setElementState({
            ...elementState,
            customElements: elementsArray,
            filteredTemplate: elementsArray,
          });
        }
        const toastMessage = customBlockMessage('Element', elementState.global);
        setElementState({
          ...elementState,
          savingLoader: false,
          showToast: true,
          toastMessage: toastMessage,
        });
      } else {
        setElementState({
          ...elementState,
          savingLoader: false,
          showToast: true,
          toastMessage: 'Element not updated. Please contact admin',
        });
        console.error('Error in updating loree element global status');
      }
    } catch (e) {
      console.log('Error in updating global status', e);
    }
  };

  const refreshGlobalFilteredData = (index: number) => {
    if (props.globalFilter !== '') {
      const templatesArray = elementState.filteredTemplate;
      templatesArray.splice(index, 1);
      setElementState({
        ...elementState,
        filteredTemplate: templatesArray,
      });
    }
  };

  const handleGlobalData = async (
    status: boolean,
    block: {
      id: string | undefined;
      content: string;
      ltiPlatformID: string;
      title: string;
      type: string;
      categoryID: string;
      thumbnail: string;
      createdBy: string;
    },
    type: string,
  ) => {
    const globalBlocksLists = await listingGlobalBlocks();
    let isAlreadyExists = false;
    let globalBlockId: string | undefined = '';
    for (const global of globalBlocksLists) {
      if (global?.customBlockID === block.id) {
        isAlreadyExists = true;
        globalBlockId = global?.id;
      }
    }
    if (!status) {
      const globalisedContent: string = isCanvasAndBB()
        ? await uploadToS3(block.content, type)
        : block.content;
      if (!isAlreadyExists) {
        const globalBlockPayload = {
          customBlockID: block.id,
          ltiPlatformID: block.ltiPlatformID,
          title: block.title,
          type: block.type,
          categoryID: block.categoryID,
          thumbnail: block.thumbnail,
          content: globalisedContent,
          createdBy: block.createdBy,
          active: true,
        };
        await API.graphql(graphqlOperation(createGlobalBlocks, { input: globalBlockPayload }));
      } else {
        const globalBlockPayload = {
          id: globalBlockId,
          content: globalisedContent,
          active: true,
        };
        await API.graphql(graphqlOperation(updateGlobalBlocks, { input: globalBlockPayload }));
      }
    } else {
      const globalBlockPayload = {
        id: globalBlockId,
        active: false,
      };
      await API.graphql(graphqlOperation(updateGlobalBlocks, { input: globalBlockPayload }));
    }
  };

  const handleRestore = async (
    templateId: string,
    categoryId: string,
    title: string,
    isGlobal: boolean,
    index: number,
  ) => {
    const updateInput = {
      id: templateId,
      title: title,
      isGlobal: isGlobal,
      categoryID: categoryId,
      active: true,
    };
    const updatedStatus = await handleUpdateCustomBlockMutation(updateInput);
    if (updatedStatus) {
      setElementState({
        ...elementState,
        showToast: true,
        toastMessage: 'Element restored successfully',
      });
      let rowsArray;
      if (props.filterValue !== '' || props.globalFilter !== '' || props.searchValue !== '') {
        rowsArray = props.filteredTemplate;
        rowsArray[index].active = true;
        rowsArray[index].updatedAt = new Date().toISOString();
        setElementState({
          ...elementState,
          filteredTemplate: rowsArray,
        });
      } else {
        rowsArray = elementState.customElements;
        rowsArray[index].active = true;
        rowsArray[index].updatedAt = new Date().toISOString();
        setElementState({
          ...elementState,
          customElements: rowsArray,
          filteredTemplate: rowsArray,
        });
      }
    }
  };

  const handleCloseLmsLoader = () => {
    setElementState({ ...elementState, savingLoader: false });
  };

  const customElementSortingData = (
    ordertype: string,
    sortableorder: string,
    orderingElements: CustomElement,
  ) => {
    const orderTypes = ordertype === 'asc' ? 'desc' : 'asc';
    const toggleTypes = toggleOrderType(sortableorder, orderTypes);
    const dataFetch =
      props.searchValue === '' && props.filterValue === '' && props.globalFilter === ''
        ? elementState.customElements
        : elementState.filteredTemplate;
    const data = [...dataFetch];
    if (orderingElements === 'category') {
      data.sort((firstSortCustomElement, secondSortCustomElement) =>
        (
          firstSortCustomElement.category as { id: string | number; name: string }
        ).name.localeCompare(secondSortCustomElement.category.name),
      );
    } else {
      data.sort((firstSortCustomElement, secondSortCustomElement) =>
        firstSortCustomElement[orderingElements].localeCompare(
          secondSortCustomElement[orderingElements],
        ),
      );
    }

    if (ordertype !== 'asc') {
      data.reverse();
    }

    setElementState({
      ...elementState,
      customElements: data,
      filteredTemplate: data,
      ...toggleTypes,
    });
  };

  const toggleOrderType = (sortableorder: string, orderType: string) => {
    switch (sortableorder) {
      case 'customElement_name':
        return {
          elementNames: orderType,
        };
      case 'category':
        return {
          category: orderType,
        };
      case 'created_by':
        return {
          createdBy: orderType,
        };
      case 'created_at':
        return {
          createdAt: orderType,
        };
      case 'last_updated':
        return {
          lastUpdated: orderType,
        };
      default:
        return null;
    }
  };

  return (
    <>
      <EditModal
        templateName={elementState.elementName}
        showEditModal={elementState.showEditModal}
        categoryId={elementState.categoryId}
        categories={props.categories}
        setEditTemplateShow={setEditElementShow}
        templateNameHandler={rowNameHandler}
        handleCategory={handleCategory}
        handleEditTemplate={handleEditElement}
        disableEditButton={elementState.disableEditButton}
        showToast={elementState.showToast}
        toastMessage={elementState.toastMessage}
        closeToast={closeToast}
        tabType={props.tabType}
      />
      <LmsLoader
        manualSaving={elementState.savingLoader}
        closeLoaderModal={handleCloseLmsLoader}
        type='global'
        action={
          elementState.global
            ? translate('global.removingfromglobal')
            : translate('global.addingtoglobal')
        }
      />
      {props.loading ? (
        <Loading />
      ) : (
          (props.filterValue === '' && props.globalFilter === '' && props.searchValue === ''
            ? elementState.customElements
            : elementState.filteredTemplate) as Array<Object>
        ).length > 0 ? (
        <Table responsive borderless size='sm' className='table-page'>
          <thead>
            <tr>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customElementSortingData(
                      elementState.elementNames,
                      'customElement_name',
                      CustomElement.title,
                    );
                  }}
                  aria-hidden='true'
                >
                  {translate('global.elementname')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={elementState.elementNames === 'asc' ? faCaretDown : faCaretUp}
                    />
                  </span>
                </span>
              </th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customElementSortingData(
                      elementState.category,
                      'category',
                      CustomElement.category,
                    );
                  }}
                  aria-hidden='true'
                >
                  {translate('global.category')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={elementState.category === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              {/* Added d-none class for temporary , once D2l file management is completed, will remove it. */}
              <th className={!isCanvasAndBB() ? 'd-none' : ''}>{translate('global.global')}</th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customElementSortingData(
                      elementState.createdBy,
                      'created_by',
                      CustomElement.createdBy,
                    );
                  }}
                  aria-hidden='true'
                >
                  {translate('global.createdby')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={elementState.createdBy === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customElementSortingData(
                      elementState.createdAt,
                      'created_at',
                      CustomElement.createdAt,
                    );
                  }}
                  aria-hidden='true'
                >
                  {translate('global.createdat')}
                  <span className='position-absolute ,ml-2'>
                    <FontAwesomeIcon
                      icon={elementState.createdAt === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customElementSortingData(
                      elementState.lastUpdated,
                      'last_updated',
                      CustomElement.updatedAt,
                    );
                  }}
                  aria-hidden='true'
                >
                  {translate('global.lastupdated')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={elementState.lastUpdated === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              <th>{translate('global.edit')}</th>
              <th>{translate('global.restore')}</th>
            </tr>
          </thead>
          <tbody>
            {(props.searchValue === '' && props.filterValue === '' && props.globalFilter === ''
              ? elementState.customElements
              : elementState.filteredTemplate
            ).map((element: BlockInfo, index: number) => (
              <tr key={index} className='admin-table'>
                <td className='font-weight-bold text-truncate overflowText'>{element.title}</td>
                <td className='text-truncate overflowText'>{element.category?.name}</td>
                <td className={!isCanvasAndBB() ? 'd-none' : ''}>
                  <input
                    type='checkbox'
                    id={element.id}
                    onChange={async (e) =>
                      await changeElementGlobalStatus(
                        element.id,
                        element.title,
                        element.category.id,
                        element.isGlobal,
                        index,
                        element.active,
                      )
                    }
                    checked={element.isGlobal}
                  />
                </td>
                <td>{element.createdBy}</td>
                <td>{new Date(element.createdAt).toDateString()}</td>
                <td>{new Date(element.updatedAt).toDateString()}</td>
                <td>
                  <button
                    onClick={async () =>
                      await openEditModal(
                        element.id,
                        element.title,
                        element.category.name,
                        element.category.id,
                        element.isGlobal,
                        index,
                        element.active,
                      )
                    }
                    style={{ background: '#eee', border: '#eee' }}
                  >
                    <EditPencil />
                  </button>
                </td>
                <td>
                  {!element.active && (
                    <FontAwesomeIcon
                      icon={faUndo}
                      onClick={async () =>
                        await handleRestore(
                          element.id,
                          element.category.id,
                          element.title,
                          element.isGlobal,
                          index,
                        )
                      }
                      className='mr-2'
                    />
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      ) : (
        <div className='container'>
          <div className='p-3'>{translate('element.nocustomelementfound')}</div>
        </div>
      )}
    </>
  );
};
export default React.memo(CustomElements);
