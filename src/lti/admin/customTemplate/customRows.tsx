import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCaretUp, faRedo } from '@fortawesome/free-solid-svg-icons';
import Loading from '../../../components/loader/loading';
import { createGlobalBlocks, updateGlobalBlocks } from '../../../graphql/mutations';
import EditModal from './editTemplateModal';
import { ReactComponent as EditPencil } from '../../../assets/Icons/edit.svg';
import { uploadToS3 } from '../globalImagesUpload/uploadImageToS3';
import LmsLoader from '../../../views/editor/saveIcon/saveToLmsLoader';
import { isCanvasAndBB } from '../../../lmsConfig';
import { customBlockMessage } from '../../../loree-editor/modules/customBlocks/customBlockHandler';
import { handleUpdateCustomBlockMutation } from './customTemplateActions';
import { listingGlobalBlocks } from '../../../loree-editor/modules/customBlocks/customBlocksAction';
import { translate } from '../../../i18n/translate';
import { CustomElement } from '../../../loree-editor/customTypes';
import { BlockInfo, CustomRowProps, StateR } from '../../../loree-editor/interface';

const CustomRows = (props: CustomRowProps) => {
  const [rowState, setRowState] = useState<StateR>({
    loading: true,
    customElements: [],
    customRows: [],
    filteredTemplate: [],
    rowName: '',
    disableEditButton: false,
    showEditModal: false,
    showToast: false,
    toastMessage: '',
    rowId: '',
    categoryId: '',
    global: false,
    index: 0,
    active: false,
    categoryName: '',
    savingLoader: false,
    customRowTitle: 'asc',
    category: 'asc',
    createdBy: 'asc',
    createdAt: 'asc',
    lastUpdated: 'asc',
  });
  useEffect(() => {
    const updateVal = {
      customRows: props.customRows,
      filteredTemplate: props.filteredTemplate,
      loading: false,
    };
    setRowState({
      ...rowState,
      ...updateVal,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    props.customRows,
    props.filteredTemplate,
    props.globalFilter,
    props.searchValue,
    props.filterValue,
  ]);

  const openEditModal = async (
    rowId: string,
    rowTitle: string,
    categoryName: string,
    categoryId: string,
    status: boolean,
    index: number,
    active: boolean,
  ) => {
    const updateValue = {
      showEditModal: true,
      rowName: rowTitle,
      rowId: rowId,
      categoryId: categoryId,
      categoryName: categoryName,
      global: status,
      active: active,
      index: index,
    };
    setRowState({
      ...rowState,
      ...updateValue,
    });
  };

  const setEditRowShow = () => {
    setRowState({
      ...rowState,
      disableEditButton: true,
      showEditModal: false,
      categoryId: '',
    });
  };

  const handleEditRow = async () => {
    try {
      const updateInput = {
        id: rowState.rowId,
        title: rowState.rowName,
        categoryID: rowState.categoryId,
        isGlobal: rowState.global,
        active: rowState.active,
      };
      const updatedStatus = await handleUpdateCustomBlockMutation(updateInput);
      const rowData = {
        id: rowState.rowId,
        title: rowState.rowName,
        categoryId: rowState.categoryId,
        isGlobal: rowState.global,
      };
      if (updatedStatus) {
        setEditRowShow();
        if (props.filterValue === '' && props.globalFilter === '' && props.searchValue === '')
          rowFilter(rowData, rowState.categoryName, rowState.index, false);
        else {
          const filterArray = rowState.filteredTemplate;
          filterArray[rowState.index].title = rowData.title;
          filterArray[rowState.index].category.id = rowData.categoryId;
          filterArray[rowState.index].categoryID = rowData.categoryId;
          filterArray[rowState.index].category.name = rowState.categoryName;
          setRowState({
            ...rowState,
            filteredTemplate: filterArray,
          });
          rowFilter(rowData, rowState.categoryName, rowState.index, true);
        }
      }
    } catch (e) {
      console.error('error on updating custom rows');
    }
  };

  const handleCategory = (
    event: React.ChangeEvent<{ value: number; options: [{ innerText: string }] }>,
  ) => {
    event.preventDefault();
    setRowState({
      ...rowState,
      categoryId: event.target.value,
      categoryName: event.target.options[event.target.value].innerText,
      disableEditButton: false,
    });
  };

  const rowFilter = (
    updatedTemplate: { id: string; title: string; categoryId: string | number; isGlobal?: boolean },
    categoryName: string,
    index: number,
    isFiltered: boolean,
  ) => {
    const rowsArray = isFiltered ? rowState.filteredTemplate : rowState.customRows;
    rowsArray[index].title = updatedTemplate.title;
    rowsArray[index].category.id = updatedTemplate.categoryId;
    rowsArray[index].categoryID = updatedTemplate.categoryId;
    rowsArray[index].category.name = categoryName;
    isFiltered
      ? setRowState({ ...rowState, filteredTemplate: rowsArray })
      : setRowState({ ...rowState, customRows: rowsArray });
    setRowState({
      ...rowState,
      showToast: true,
      toastMessage: 'Row updated successfully',
    });
  };

  const rowNameHandler = (event: Event) => {
    const target = event.target as HTMLInputElement;
    setRowState({ ...rowState, rowName: target?.value });
    validateRowSaveButton();
  };

  const validateRowSaveButton = () => {
    if (rowState.rowName.trim().length > 0) setRowState({ ...rowState, disableEditButton: false });
    else setRowState({ ...rowState, disableEditButton: true });
  };

  const closeToast = () => {
    setRowState({ ...rowState, showToast: false });
  };

  const changeRowGlobalStatus = async (
    rowId: string,
    templateTitle: string,
    categoryId: string,
    status: boolean,
    index: number,
    active: boolean,
  ) => {
    try {
      setRowState({ ...rowState, savingLoader: true, global: status });
      const updateInput = {
        id: rowId,
        title: templateTitle,
        categoryID: categoryId,
        isGlobal: !status,
        active: active,
      };
      const updatedStatus = await handleUpdateCustomBlockMutation(updateInput);
      if (updatedStatus) {
        let rowsArray: Array<{
          id: string;
          content: string;
          createdBy: string;
          ltiPlatformID: string;
          title: string;
          categoryID: string;
          thumbnail: string;
          isGlobal: boolean;
          updatedAt: string;
          type: string;
        }>;
        if (props.filterValue !== '' || props.globalFilter !== '' || props.searchValue !== '') {
          rowsArray = props.filteredTemplate;
          rowsArray[index].isGlobal = !status;
          rowsArray[index].updatedAt = new Date().toISOString();
          await handleGlobalData(status, rowsArray[index], 'Global-Rows');
          setRowState({
            ...rowState,
            filteredTemplate: rowsArray,
          });
          refreshGlobalFilteredData(index);
        } else {
          rowsArray = rowState.customRows;
          rowsArray[index].isGlobal = !status;
          rowsArray[index].updatedAt = new Date().toISOString();
          await handleGlobalData(status, rowsArray[index], 'Global-Rows');
          setRowState({
            ...rowState,
            customRows: rowsArray,
          });
        }
        const toastMessage = customBlockMessage('Row', rowState.global);
        setRowState({
          ...rowState,
          savingLoader: false,
          showToast: true,
          toastMessage: toastMessage,
        });
      } else {
        setRowState({
          ...rowState,
          savingLoader: false,
          showToast: true,
          toastMessage: 'Row not updated, Please contact admin',
        });
        console.error('Error in updating loree row global status');
      }
    } catch (e) {
      console.log('Error in updating global status', e);
    }
  };

  const refreshGlobalFilteredData = (index: number) => {
    if (props.globalFilter !== '') {
      const templatesArray = rowState.filteredTemplate;
      templatesArray.splice(index, 1);
      setRowState({
        ...rowState,
        filteredTemplate: templatesArray,
      });
    }
  };

  const handleGlobalData = async (
    status: boolean,
    block: {
      id: string;
      content: string;
      createdBy: string;
      ltiPlatformID: string;
      title: string;
      categoryID: string;
      thumbnail: string;
      type: string;
    },
    type: string,
  ) => {
    const globalBlocksLists = await listingGlobalBlocks();
    let isAlreadyExists = false;
    let globalBlockId = '';
    for (const global of globalBlocksLists) {
      if (global?.customBlockID === block.id) {
        isAlreadyExists = true;
        globalBlockId = global?.id;
      }
    }

    if (!status) {
      const globalisedContent: string = isCanvasAndBB()
        ? await uploadToS3(block.content, type)
        : block.content;
      if (!isAlreadyExists) {
        const globalBlockPayload = {
          customBlockID: block.id,
          ltiPlatformID: block.ltiPlatformID,
          title: block.title,
          type: block.type,
          categoryID: block.categoryID,
          thumbnail: block.thumbnail,
          content: globalisedContent,
          createdBy: block.createdBy,
          active: true,
        };
        await API.graphql(graphqlOperation(createGlobalBlocks, { input: globalBlockPayload }));
      } else {
        const globalBlockPayload = {
          id: globalBlockId,
          content: globalisedContent,
          active: true,
        };
        await API.graphql(graphqlOperation(updateGlobalBlocks, { input: globalBlockPayload }));
      }
    } else {
      const globalBlockPayload = {
        id: globalBlockId,
        active: false,
      };
      await API.graphql(graphqlOperation(updateGlobalBlocks, { input: globalBlockPayload }));
    }
  };

  const handleRestore = async (
    templateId: string,
    categoryId: string,
    title: string,
    isGlobal: boolean,
    index: number,
  ) => {
    const updateInput = {
      id: templateId,
      title: title,
      isGlobal: isGlobal,
      categoryID: categoryId,
      active: true,
    };
    const updatedStatus = await handleUpdateCustomBlockMutation(updateInput);
    if (updatedStatus) {
      setRowState({ ...rowState, showToast: true, toastMessage: 'Row restored successfully' });
      let rowsArray: BlockInfo[];
      if (props.filterValue !== '' || props.globalFilter !== '' || props.searchValue !== '') {
        rowsArray = props.filteredTemplate;
        rowsArray[index].active = true;
        rowsArray[index].updatedAt = new Date().toISOString();
        setRowState({
          ...rowState,
          filteredTemplate: rowsArray,
        });
      } else {
        rowsArray = rowState.customRows;
        rowsArray[index].active = true;
        rowsArray[index].updatedAt = new Date().toISOString();
        setRowState({
          ...rowState,
          customRows: rowsArray,
        });
      }
    }
  };

  const handleCloseLmsLoader = () => {
    setRowState({ ...rowState, savingLoader: false });
  };

  const customRowsSorted = (
    ordertype: string,
    sortableOrder: string,
    orderingElements: CustomElement,
  ) => {
    const orderTypes = ordertype === 'asc' ? 'desc' : 'asc';
    const toggleTypes = toggleOrderType(sortableOrder, orderTypes);
    const fetchCustomRowsData =
      props.searchValue === '' && props.filterValue === '' && props.globalFilter === ''
        ? rowState.customRows
        : rowState.filteredTemplate;
    const customRowsData = [...fetchCustomRowsData];

    if (orderingElements === 'category') {
      customRowsData.sort((firstCustomRowElement, secondCustomRowElement) =>
        (
          firstCustomRowElement.category as { id: string | number; name: string }
        ).name.localeCompare(secondCustomRowElement.category.name),
      );
    } else {
      customRowsData.sort((firstCustomRowElement, secondCustomRowElement) =>
        firstCustomRowElement[orderingElements].localeCompare(
          secondCustomRowElement[orderingElements],
        ),
      );
    }
    if (ordertype !== 'asc') {
      customRowsData.reverse();
    }
    setRowState({
      ...rowState,
      customRows: customRowsData,
      customElements: customRowsData,
      filteredTemplate: customRowsData,
      ...toggleTypes,
    });
  };

  const toggleOrderType = (sortableOrder: string, orderType: string) => {
    switch (sortableOrder) {
      case 'customRow_Title':
        return {
          customRowTitle: orderType,
        };
      case 'category':
        return {
          category: orderType,
        };
      case 'created_By':
        return {
          createdBy: orderType,
        };
      case 'created_At':
        return {
          createdAt: orderType,
        };
      case 'updated_At':
        return {
          lastUpdated: orderType,
        };
      default:
        return null;
    }
  };

  return (
    <>
      <EditModal
        templateName={rowState.rowName}
        showEditModal={rowState.showEditModal}
        categoryId={rowState.categoryId}
        categories={props.categories}
        setEditTemplateShow={setEditRowShow}
        templateNameHandler={rowNameHandler}
        handleCategory={handleCategory}
        handleEditTemplate={handleEditRow}
        disableEditButton={rowState.disableEditButton}
        showToast={rowState.showToast}
        toastMessage={rowState.toastMessage}
        closeToast={closeToast}
        tabType={props.tabType}
      />
      <LmsLoader
        manualSaving={rowState.savingLoader}
        closeLoaderModal={handleCloseLmsLoader}
        type='global'
        action={
          rowState.global
            ? translate('global.removingfromglobal')
            : translate('global.addingtoglobal')
        }
      />
      {props.loading ? (
        <Loading />
      ) : (
          (props.filterValue === '' && props.globalFilter === '' && props.searchValue === ''
            ? rowState.customRows
            : rowState.filteredTemplate) as Array<Object>
        ).length > 0 ? (
        <Table responsive borderless size='sm' className='table-page'>
          <thead>
            <tr>
              <th>
                <span
                  className='clickableOrderContent'
                  data-testid='custom-row'
                  onClick={() => {
                    customRowsSorted(
                      rowState.customRowTitle,
                      'customRow_Title',
                      CustomElement.title,
                    );
                  }}
                  aria-hidden='true'
                >
                  {translate('global.rowname')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={rowState.customRowTitle === 'asc' ? faCaretDown : faCaretUp}
                    />
                  </span>
                </span>
              </th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customRowsSorted(rowState.category, 'category', CustomElement.category);
                  }}
                  aria-hidden='true'
                >
                  {translate('global.category')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon icon={rowState.category === 'asc' ? faCaretUp : faCaretDown} />
                  </span>
                </span>
              </th>
              {/* Added d-none class for temporary , once D2l file management is completed, will remove it. */}
              <th className={!isCanvasAndBB() ? 'd-none' : ''}>{translate('global.global')}</th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customRowsSorted(rowState.createdBy, 'created_By', CustomElement.createdBy);
                  }}
                  aria-hidden='true'
                >
                  {translate('global.createdby')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={rowState.createdBy === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customRowsSorted(rowState.createdAt, 'created_At', CustomElement.createdAt);
                  }}
                  aria-hidden='true'
                >
                  {translate('global.createdat')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={rowState.createdAt === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              <th>
                <span
                  className='clickableOrderContent'
                  onClick={() => {
                    customRowsSorted(rowState.lastUpdated, 'updated_At', CustomElement.updatedAt);
                  }}
                  aria-hidden='true'
                >
                  {translate('global.lastupdated')}
                  <span className='position-absolute ml-2'>
                    <FontAwesomeIcon
                      icon={rowState.lastUpdated === 'asc' ? faCaretUp : faCaretDown}
                    />
                  </span>
                </span>
              </th>
              <th>{translate('global.edit')}</th>
              <th>{translate('global.restore')}</th>
            </tr>
          </thead>
          <tbody>
            {(props.searchValue === '' && props.filterValue === '' && props.globalFilter === ''
              ? rowState.customRows
              : rowState.filteredTemplate
            ).map((row: BlockInfo, index: number) => (
              <tr key={index} className='admin-table'>
                <td className='font-weight-bold'>{row.title}</td>
                <td className='text-truncate overflowText'>{row.category.name}</td>
                <td className={!isCanvasAndBB() ? 'd-none' : ''}>
                  <input
                    type='checkbox'
                    id={row.id}
                    onChange={async (e) =>
                      await changeRowGlobalStatus(
                        row.id,
                        row.title,
                        row.category.id,
                        row.isGlobal,
                        index,
                        row.active,
                      )
                    }
                    checked={row.isGlobal}
                  />
                </td>
                <td>{row.createdBy}</td>
                <td>{new Date(row.createdAt).toDateString()}</td>
                <td>{new Date(row.updatedAt).toDateString()}</td>
                <td>
                  <button
                    onClick={async () =>
                      await openEditModal(
                        row.id,
                        row.title,
                        row.category.name,
                        row.category.id,
                        row.isGlobal,
                        index,
                        row.active,
                      )
                    }
                    style={{ background: '#eee', border: '#eee' }}
                  >
                    <EditPencil />
                  </button>
                </td>
                <td>
                  {!row.active && (
                    <FontAwesomeIcon
                      icon={faRedo}
                      id='restore-icon'
                      onClick={async () =>
                        await handleRestore(row.id, row.category.id, row.title, row.isGlobal, index)
                      }
                      className='mr-2'
                    />
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      ) : (
        <div className='container'>
          <div className='p-3'>No Custom Rows found</div>
        </div>
      )}
    </>
  );
};

export default React.memo(CustomRows);
