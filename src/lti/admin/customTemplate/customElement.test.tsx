import React from 'react';
import { mount } from 'enzyme';
import CustomElements from './customElements';
import {
  customElementMockData,
  customElementSortedMockDatas,
  customElementReverseMockData,
} from './customBlockMockData';
import { screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import { translate } from '../../../i18n/translate';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('custom Element ascending and descending checking', () => {
  let elementProps: any;
  beforeEach(() => {
    elementProps = customElementMockData;
    render(<CustomElements {...elementProps} />);
  });
  test('to check the table header present or not in the document', () => {
    const customElementHeaderTitleElement = screen.getByText(translate('global.elementname'));
    expect(customElementHeaderTitleElement).toBeInTheDocument();
    const customElementHeaderCategoryElement = screen.getByText(translate('global.category'));
    expect(customElementHeaderCategoryElement).toBeInTheDocument();
    const customElementHeaderCreatedByElement = screen.getByText(translate('global.createdby'));
    expect(customElementHeaderCreatedByElement).toBeInTheDocument();
    const customElementHeaderCreatedAtElement = screen.getByText(translate('global.createdat'));
    expect(customElementHeaderCreatedAtElement).toBeInTheDocument();
    const customElementHeaderLastUpdatedElement = screen.getByText(translate('global.lastupdated'));
    expect(customElementHeaderLastUpdatedElement).toBeInTheDocument();
  });
  test('check the length of the customElementlist', () => {
    expect(customElementMockData.customElements.length).toEqual(5);
  });
  test('check to the fuction is called ,sorted and reverse', () => {
    const customElementHeaderTitleElement = screen.getByText(translate('global.elementname'));
    const dataProperty = [...customElementMockData.customElements];
    customElementHeaderTitleElement.click();
    const customElementsortedData = dataProperty.sort((a, b) => a.title.localeCompare(b.title));
    expect(customElementsortedData).toEqual(customElementSortedMockDatas.customElements);
    const customElementReverseData = customElementsortedData.map((data) => data);
    expect(customElementReverseData.reverse()).toEqual(customElementReverseMockData.customElements);
  });
});

describe('custom Elements', () => {
  let elementList: any;
  let elementProps: any;
  beforeEach(() => {
    elementProps = customElementMockData;
    elementList = mount(<CustomElements {...elementProps} />);
  });
  describe('when display the element details', () => {
    test('rendered the list', () => {
      expect(elementList.find('div')).toHaveLength(1);
    });
    test('rendered the list in table', () => {
      expect(elementList.find('table')).toHaveLength(1);
    });
    test('element name shows in table definition', () => {
      expect(elementList.find('td').at(0).text()).toEqual(elementProps.customElements[0].title);
    });
    describe('when edit modal is clicked', () => {
      test.skip('the onclick function is triggered and modal is opened', () => {
        const openEditModal = jest.fn();
        elementList.find('button').simulate('click', openEditModal);
        expect(openEditModal).toHaveBeenCalled();
      });
    });
  });
  describe('when element is inactive', () => {
    test.skip('shows restore icon', () => {
      elementProps.customElements[0].active = false;
      elementList = mount(<CustomElements {...elementProps} />);
      const handleRestore = jest.fn();
      elementList.find('FontAwesomeIcon').simulate('click', handleRestore);
      expect(handleRestore).toHaveBeenCalled();
    });
  });
  describe('when global is enable', () => {
    test.skip('the onchange function triggered', () => {
      elementProps.customElements[0].isGlobal = true;
      elementList = mount(<CustomElements {...elementProps} />);
      const changeElementGlobalStatus = jest.fn();
      elementList.find('input').simulate('change', changeElementGlobalStatus);
      expect(changeElementGlobalStatus).toHaveBeenCalled();
    });
  });
  describe('when user search a value', () => {
    test('searching value is not found', () => {
      elementProps.searchValue = 'test-elements';
      elementProps.filteredTemplate = [];
      elementList = mount(<CustomElements {...elementProps} />);
      expect(elementList.find('div').at(1).text()).toEqual('element.nocustomelementfound');
    });
  });
});
