import React from 'react';
import { mount } from 'enzyme';
import CustomRows from './customRows';
import {
  customRowMockData,
  customRowReverseMockData,
  customRowSortingMockData,
} from './customBlockMockData';
import { render } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import { translate } from '../../../i18n/translate';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));
describe('custom Element ascending and descending checking', () => {
  let rowProps: any;
  beforeEach(() => {
    rowProps = customRowMockData;
    render(<CustomRows {...rowProps} />);
  });
  test('to check the table custom Row header present or not in the document', () => {
    const customRowHeaderTitleElement = screen.getByText(translate('global.rowname'));
    expect(customRowHeaderTitleElement).toBeInTheDocument();
    const customRowHeaderCategoryElement = screen.getByText(translate('global.category'));
    expect(customRowHeaderCategoryElement).toBeInTheDocument();
    const customRowHeaderCreatedByElement = screen.getByText(translate('global.createdby'));
    expect(customRowHeaderCreatedByElement).toBeInTheDocument();
    const customRowHeaderCreatedAtElement = screen.getByText(translate('global.createdat'));
    expect(customRowHeaderCreatedAtElement).toBeInTheDocument();
    const customRowHeaderLastUpdatedElement = screen.getByText(translate('global.lastupdated'));
    expect(customRowHeaderLastUpdatedElement).toBeInTheDocument();
  });
  test('check the length of the customRowlist', () => {
    expect(customRowMockData.customRows.length).toEqual(4);
  });
  test('check to the fuction is called ,sorted and reverse', () => {
    const customRowHeaderTitleElement = screen.getByText(translate('global.rowname'));
    customRowHeaderTitleElement.click();
    const dataProperty = [...customRowMockData.customRows];
    const customRowSortedData = dataProperty.sort((a, b) => a.title.localeCompare(b.title));
    expect(customRowSortedData).toEqual(customRowSortingMockData.customRows);
    const customRowReverseData = customRowSortedData.map((data) => data);
    expect(customRowReverseData.reverse()).toEqual(customRowReverseMockData.customRows);
  });
});

describe('custom rows', () => {
  let rowList: any;
  let rowProps: any;
  beforeEach(() => {
    rowProps = customRowMockData;
    rowList = mount(<CustomRows {...rowProps} />);
  });
  describe('when display the row details', () => {
    test('rendered the list', () => {
      expect(rowList.find('div')).toHaveLength(1);
    });
    test('rendered the list in table', () => {
      expect(rowList.find('table')).toHaveLength(1);
    });
    test('table heading contains a row name', () => {
      expect(rowList.find('th').at(0).text()).toEqual('global.rowname');
    });
    test('row name shows in table definition', () => {
      expect(rowList.find('td').at(0).text()).toEqual(rowProps.customRows[0].title);
    });
    describe('when edit modal is clicked', () => {
      test.skip('the onclick function is triggered and modal is opened', () => {
        const openEditModal = jest.fn();
        rowList.find('button').simulate('click', openEditModal);
        expect(openEditModal).toHaveBeenCalled();
      });
    });
  });
  describe('when row is inactive', () => {
    test.skip('shows restore icon', () => {
      rowProps.customRows[0].active = false;
      rowList = mount(<CustomRows {...rowProps} />);
      const handleRestore = jest.fn();
      rowList.find('FontAwesomeIcon').simulate('click', handleRestore);
      expect(handleRestore).toHaveBeenCalled();
    });
  });
  describe('when global is enable', () => {
    test.skip('the onchange function triggered', () => {
      rowProps.customRows[0].isGlobal = true;
      rowList = mount(<CustomRows {...rowProps} />);
      const changeElementGlobalStatus = jest.fn();
      rowList.find('input').simulate('change', changeElementGlobalStatus);
      expect(changeElementGlobalStatus).toHaveBeenCalled();
    });
  });
  describe('when user search a value', () => {
    test('searching value is not found', () => {
      rowProps.searchValue = 'test-elements';
      rowProps.filteredTemplate = [];
      rowList = mount(<CustomRows {...rowProps} />);
      expect(rowList.find('div').at(1).text()).toEqual('No Custom Rows found');
    });
  });
});
