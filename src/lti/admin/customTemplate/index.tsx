/* eslint-disable array-callback-return */
import React from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import TemplateLists from './templateHandler';
import CustomRows from './customRows';
import CustomElements from './customElements';
import {
  handleCustomElementsAPI,
  handleCustomRowAPI,
  handleCustomTemplateAPI,
  setLtiPlatformId,
  handleCategoryAPI,
} from './customTemplateActions';
import FilterOptions from './filterOptions';
import { TabType } from './editTemplateModal';

class CustomTemplate extends React.Component {
  state = {
    categoryLists: [],
    customTemplateLists: [],
    loading: true,
    customRows: [],
    customElements: [],
    categoryId: '',
    categories: [],
    filterValue: '',
    filteredTemplate: [],
    globalFilter: '',
    searchValue: '',
    search: false,
    tabType: TabType.Template,
  };

  async componentDidMount() {
    setLtiPlatformId(sessionStorage.getItem('ltiPlatformId') as string);
    try {
      void Promise.all([await handleCustomTemplateAPI(), await handleCategoryAPI()]).then(
        (response) => {
          this.setState({
            customTemplateLists: response[0],
            categories: response[1],
            loading: false,
          });
        },
      );
    } catch (err) {
      console.error('Error in getting custom templates', err);
    }
  }

  getCustomRowList = async () => {
    if (this.state.customRows.length <= 0)
      this.setState({ customRows: await handleCustomRowAPI() });
    this.setState({ loading: false });
    return this.state.customRows;
  };

  getCustomElementList = async () => {
    if (this.state.customElements.length <= 0)
      this.setState({ customElements: await handleCustomElementsAPI() });
    this.setState({ loading: false });
    return this.state.customElements;
  };

  handleSelectTab = async (key: string | null) => {
    let selectedDataList;
    if (key === 'customTemplate') {
      selectedDataList = this.state.customTemplateLists;
      this.setState({ tabType: TabType.Template });
    } else if (key === 'customRows') {
      this.setState({ tabType: TabType.Row, loading: true });
      selectedDataList = await this.getCustomRowList();
    } else if (key === 'customElements') {
      this.setState({ tabType: TabType.Element, loading: true });
      selectedDataList = await this.getCustomElementList();
    }
    this.setState({
      search: false,
      searchValue: '',
      filteredTemplate: selectedDataList,
    });
    const search = document.getElementById('search') as HTMLElement;
    (search as HTMLInputElement).value = '';
    const filter = document.getElementById('categoryfilter') as HTMLInputElement;
    filter.value = '';
    const globalCheckBox = document.getElementById('global-filter') as HTMLInputElement;
    globalCheckBox.checked = false;
  };

  handleCustomSelectedTabData = () => {
    let selectedTabData;
    if (this.state.tabType === TabType.Template) {
      selectedTabData = this.state.customTemplateLists;
    } else if (this.state.tabType === TabType.Row) {
      selectedTabData = this.state.customRows;
    } else if (this.state.tabType === TabType.Element) {
      selectedTabData = this.state.customElements;
    }
    return selectedTabData;
  };

  handleCategoryFilter = (event: { target: { value: string } }) => {
    this.applyCatFilter(event?.target?.value);
  };

  handleGlobalFilter = (event: { target: { checked: boolean } }) => {
    const value = event.target.checked ? 'global' : '';
    this.applyGlobalFilter(value);
  };

  handleSearchChange = (event: { target: { value: string } }) => {
    this.applySearchFilter(event.target.value);
  };

  applyCatFilter = (value: string) => {
    let filterId: Array<string> = [];
    let finalArray: Array<{}> | undefined = [];
    if (value !== '') {
      filterId = this.commonFilter('', this.state.searchValue, this.state.globalFilter);
      if (filterId.length > 0) {
        this.handleCustomSelectedTabData()?.map((item: { id?: string; categoryID?: string }) => {
          filterId.map((id) => {
            if (item.id === id && item.categoryID === value) finalArray?.push(item);
          });
        });
      } else {
        finalArray = this.getDefaultCategoryFilterData(value);
      }
    } else {
      filterId = this.commonFilter('', this.state.searchValue, this.state.globalFilter);
      if (filterId.length > 0) {
        this.handleCustomSelectedTabData()?.map((item: { id: string }) => {
          filterId.map((id: string) => {
            if (item.id === id) finalArray?.push(item);
          });
        });
      } else {
        finalArray = this.getDefaultCategoryFilterData(value);
      }
    }
    this.setState({
      filterValue: value,
      filteredTemplate: finalArray,
    });
  };

  getDefaultCategoryFilterData = (value: string) => {
    if (this.state.searchValue === '' && this.state.globalFilter === '') {
      return this.handleCustomSelectedTabData()?.filter((template: { categoryID: string }) => {
        return template.categoryID === value;
      });
    }
  };

  applyGlobalFilter = (value: string) => {
    let filterId: Array<string> = [];
    let finalArray: Array<{}> | undefined = [];
    if (value !== '') {
      filterId = this.commonFilter(this.state.filterValue, this.state.searchValue, '');
      if (filterId.length > 0) {
        this.handleCustomSelectedTabData()?.map((item: { id: string; isGlobal: boolean }) => {
          filterId.map((id: string) => {
            if (item.id === id && item.isGlobal) finalArray?.push(item);
          });
        });
      } else {
        finalArray = this.getDefaultGlobalFilterData();
      }
    } else {
      filterId = this.commonFilter(this.state.filterValue, this.state.searchValue, '');
      if (filterId.length > 0) {
        this.handleCustomSelectedTabData()?.map((item: { id: string }) => {
          filterId.map((id: string) => {
            if (item.id === id) finalArray?.push(item);
          });
        });
      } else {
        finalArray = this.getDefaultGlobalFilterData();
      }
    }
    this.setState({
      globalFilter: value,
      filteredTemplate: finalArray,
    });
  };

  getDefaultGlobalFilterData = () => {
    if (this.state.filterValue === '' && this.state.searchValue === '') {
      return this.handleCustomSelectedTabData()?.filter((template: { isGlobal: boolean }) => {
        return template.isGlobal;
      });
    }
  };

  applySearchFilter = (value: string) => {
    let filterId: Array<string> = [];
    let finalArray: Array<{}> | undefined = [];
    if (value !== '') {
      filterId = this.commonFilter(this.state.filterValue, '', this.state.globalFilter);
      if (filterId.length > 0) {
        this.handleCustomSelectedTabData()?.map((item: { title: string; id: string }) => {
          const name = item.title.toLowerCase();
          const comparingValue = value.toLowerCase();
          filterId.map((id: string) => {
            if (item.id === id && name.includes(comparingValue)) {
              finalArray?.push(item);
            }
          });
        });
      } else {
        finalArray = this.getDefaultSearchFilterArray(value);
      }
    } else {
      filterId = this.commonFilter(this.state.filterValue, '', this.state.globalFilter);
      if (filterId.length > 0) {
        this.handleCustomSelectedTabData()?.map((item: { id: string }) => {
          filterId.map((id: string) => {
            if (item.id === id) finalArray?.push(item);
          });
        });
      } else {
        finalArray = this.getDefaultSearchFilterArray(value);
      }
    }
    this.setState({
      searchValue: value,
      filteredTemplate: finalArray,
    });
  };

  getDefaultSearchFilterArray = (value: string) => {
    if (this.state.filterValue === '' && this.state.globalFilter === '') {
      return this.handleCustomSelectedTabData()?.filter((template: { title: string }) => {
        const name = template.title.toLowerCase();
        const comparingValue = value.toLowerCase();
        return name.includes(comparingValue);
      });
    }
  };

  commonFilter = (filterValue: string, searchValue: string, globalFilter: string) => {
    const filterId: Array<string> = [];
    let index: number = 0;
    if (filterValue !== '') {
      index++;
      this.handleCustomSelectedTabData()?.map((item: { categoryID: string; id: string }) => {
        if (item.categoryID === filterValue) filterId.push(item.id);
      });
    }
    if (searchValue !== '') {
      index++;
      this.handleCustomSelectedTabData()?.map((item: { title: string; id: string }) => {
        const name = item.title.toLowerCase();
        const comparingValue = searchValue.toLowerCase();
        if (name.includes(comparingValue)) filterId.push(item.id);
      });
    }
    if (globalFilter !== '') {
      index++;
      this.handleCustomSelectedTabData()?.map((item: { isGlobal: boolean; id: string }) => {
        if (item.isGlobal) filterId.push(item.id);
      });
    }
    if (index > 1) {
      const result = this.findDuplicates(filterId);
      return result;
    } else {
      return filterId;
    }
  };

  findDuplicates = (arr: Array<string>) => {
    const sortedArr = arr.slice().sort();
    const results = sortedArr.filter((value, index, data) => data.indexOf(value) !== index);
    return results;
  };

  handleSearch = () => {
    this.setState({
      search: !this.state.search,
    });
  };

  render() {
    return (
      <>
        <h3 className='text-primary'>Custom Blocks</h3>
        <div className='my-templates-wrapper'>
          <div className='templates-tab-section'>
            <FilterOptions
              search={this.state.search}
              handleSearchChange={this.handleSearchChange}
              handleSearch={this.handleSearch}
              handleGlobalFilter={this.handleGlobalFilter}
              handleCategoryFilter={this.handleCategoryFilter}
              categories={this.state.categories}
            />
            <Tabs
              defaultActiveKey='customTemplate'
              id='uncontrolled-tab-example'
              onSelect={async (key: string | null) => await this.handleSelectTab(key)}
            >
              <Tab eventKey='customTemplate' title='Custom Templates'>
                {this.state.tabType === TabType.Template && (
                  <TemplateLists
                    loading={this.state.loading}
                    filterValue={this.state.filterValue}
                    globalFilter={this.state.globalFilter}
                    customTemplateLists={this.state.customTemplateLists}
                    filteredTemplate={this.state.filteredTemplate}
                    categories={this.state.categories}
                    searchValue={this.state.searchValue}
                    tabType={TabType.Template}
                  />
                )}
              </Tab>
              <Tab eventKey='customRows' title='Custom Rows'>
                {this.state.tabType === TabType.Row && (
                  <CustomRows
                    loading={this.state.loading}
                    globalFilter={this.state.globalFilter}
                    searchValue={this.state.searchValue}
                    filteredTemplate={this.state.filteredTemplate}
                    filterValue={this.state.filterValue}
                    categories={this.state.categories}
                    customRows={this.state.customRows}
                    tabType={TabType.Row}
                  />
                )}
              </Tab>
              <Tab eventKey='customElements' title='Custom Elements'>
                {this.state.tabType === TabType.Element && (
                  <CustomElements
                    loading={this.state.loading}
                    globalFilter={this.state.globalFilter}
                    searchValue={this.state.searchValue}
                    filteredTemplate={this.state.filteredTemplate}
                    filterValue={this.state.filterValue}
                    categories={this.state.categories}
                    customElements={this.state.customElements}
                    tabType={TabType.Element}
                  />
                )}
              </Tab>
            </Tabs>
          </div>
        </div>
      </>
    );
  }
}

export default CustomTemplate;
