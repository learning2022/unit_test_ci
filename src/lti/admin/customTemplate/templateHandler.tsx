import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCaretUp, faUndo } from '@fortawesome/free-solid-svg-icons';
import Loading from '../../../components/loader/loading';
import { createGlobalTemplates, updateGlobalTemplates } from '../../../graphql/mutations';
import EditTemplate from './editTemplateModal';
import { ReactComponent as EditPencil } from '../../../assets/Icons/edit.svg';
import { uploadToS3 } from '../globalImagesUpload/uploadImageToS3';
import LmsLoader from '../../../views/editor/saveIcon/saveToLmsLoader';
import { isCanvasAndBB } from '../../../lmsConfig';
import { customBlockMessage } from '../../../loree-editor/modules/customBlocks/customBlockHandler';
import { handleUpdateCustomTemplateMutation } from './customTemplateActions';
import { listingGlobalTemplates } from '../../../loree-editor/modules/customBlocks/customBlocksAction';
import { translate } from '../../../i18n/translate';
import { CustomElement } from '../../../loree-editor/customTypes';
import { BlockInfo, TemplateProps, StateT } from '../../../loree-editor/interface';

const TemplateLists = (props: TemplateProps) => {
  const [templateListData, setTemplateListData] = useState<StateT>({
    loading: true,
    showEditModal: false,
    templateName: '',
    categoryName: '',
    categoryId: '',
    templateId: '',
    customTemplateLists: [],
    filteredTemplate: [],
    disableEditButton: true,
    showToast: false,
    toastMessage: '',
    global: false,
    active: false,
    index: 0,
    savingLoader: false,
    elementNames: 'asc',
    category: 'asc',
    createdBy: 'asc',
    createdAt: 'asc',
    lastUpdated: 'asc',
  });

  useEffect(() => {
    const updateValues = {
      customTemplateLists: props.customTemplateLists,
      filteredTemplate: props.filteredTemplate,
      loading: false,
    };
    setTemplateListData({
      ...templateListData,
      ...updateValues,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    props.customTemplateLists,
    props.filteredTemplate,
    props.globalFilter,
    props.searchValue,
    props.filterValue,
  ]);

  const setEditTemplateShow = () => {
    setTemplateListData({
      ...templateListData,
      disableEditButton: true,
      showEditModal: false,
      categoryId: '',
    });
  };

  const openEditModal = async (
    templateId: string,
    templateTitle: string,
    categoryName: string,
    categoryId: string,
    status: boolean,
    active: boolean,
    index: number,
  ) => {
    const updateValue = {
      showEditModal: true,
      templateName: templateTitle,
      templateId: templateId,
      categoryId: categoryId,
      categoryName: categoryName,
      global: status,
      active: active,
      index: index,
    };
    setTemplateListData({
      ...templateListData,
      ...updateValue,
    });
  };

  const changeTemplateGlobalStatus = async (
    templateId: string,
    templateTitle: string,
    categoryId: string,
    status: boolean,
    index: number,
    active: boolean,
  ) => {
    try {
      setTemplateListData({ ...templateListData, savingLoader: true, global: status });
      const updateTemplateInput = {
        id: templateId,
        title: templateTitle,
        categoryID: categoryId,
        isGlobal: !status,
        active: active,
      };
      const updatedStatus = await handleUpdateCustomTemplateMutation(updateTemplateInput);
      if (updatedStatus) {
        let templatesArray;
        if (props.filterValue !== '' || props.globalFilter !== '' || props.searchValue !== '') {
          templatesArray = props.filteredTemplate;
          templatesArray[index].isGlobal = !status;
          templatesArray[index].updatedAt = new Date().toISOString();
          await handleGlobalData(status, templatesArray[index], 'Global-Templates');
          setTemplateListData({
            ...templateListData,
            filteredTemplate: templatesArray,
          });
          refreshGlobalFilteredData(index);
        } else {
          templatesArray = templateListData.customTemplateLists;
          templatesArray[index].isGlobal = !status;
          templatesArray[index].updatedAt = new Date().toISOString();
          await handleGlobalData(status, templatesArray[index], 'Global-Templates');
          setTemplateListData({
            ...templateListData,
            customTemplateLists: templatesArray,
          });
        }
        const toastMessage = customBlockMessage('Template', templateListData.global);
        setTemplateListData({
          ...templateListData,
          showToast: true,
          savingLoader: false,
          toastMessage: toastMessage,
        });
      } else {
        setTemplateListData({
          ...templateListData,
          showToast: true,
          savingLoader: false,
          toastMessage: 'Template not updated. Please contact admin',
        });
        console.error('Error in updating loree template global status');
      }
    } catch (e) {
      console.log('Error in updating global status', e);
    }
  };

  const refreshGlobalFilteredData = (index: number) => {
    if (props.globalFilter !== '') {
      const templatesArray = templateListData.filteredTemplate;
      templatesArray.splice(index, 1);
      setTemplateListData({
        ...templateListData,
        filteredTemplate: templatesArray,
      });
    }
  };

  const handleGlobalData = async (
    status: boolean,
    template: {
      id: string;
      content: string;
      createdBy: string;
      ltiPlatformID: string;
      title: string;
      categoryID: string;
      thumbnail: string;
    },
    type: string,
  ) => {
    const globalTemplateLists = await listingGlobalTemplates();
    let isAlreadyExists = false;
    let globalTempateId = '';
    for (const global of globalTemplateLists) {
      if (global?.customTemplateID === template.id) {
        isAlreadyExists = true;
        globalTempateId = global?.id;
      }
    }
    if (!status) {
      const globalisedContent: string = isCanvasAndBB()
        ? await uploadToS3(template.content, type)
        : template.content;
      if (!isAlreadyExists) {
        const globalTemplatePayload = {
          customTemplateID: template.id,
          ltiPlatformID: template.ltiPlatformID,
          title: template.title,
          categoryID: template.categoryID,
          thumbnail: template.thumbnail,
          content: globalisedContent,
          createdBy: template.createdBy,
          active: true,
        };
        await API.graphql(
          graphqlOperation(createGlobalTemplates, { input: globalTemplatePayload }),
        );
      } else {
        const globalTemplatePayload = {
          id: globalTempateId,
          content: globalisedContent,
          active: true,
        };
        await API.graphql(
          graphqlOperation(updateGlobalTemplates, { input: globalTemplatePayload }),
        );
      }
    } else {
      const globalTemplatePayload = {
        id: globalTempateId,
        active: false,
      };
      await API.graphql(graphqlOperation(updateGlobalTemplates, { input: globalTemplatePayload }));
    }
  };

  const closeToast = () => {
    setTemplateListData({ ...templateListData, showToast: false });
  };

  const templateNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTemplateListData({ ...templateListData, templateName: event.target.value });
    validateTempSaveButton();
  };

  const validateTempSaveButton = () => {
    if (templateListData.templateName.trim().length > 0)
      setTemplateListData({ ...templateListData, disableEditButton: false });
    else setTemplateListData({ ...templateListData, disableEditButton: true });
  };

  const handleCategory = (
    event: React.ChangeEvent<{ value: number; options: [{ innerText: string }] }>,
  ) => {
    event.preventDefault();
    setTemplateListData({
      ...templateListData,
      categoryId: event.target.value,
      categoryName: event.target.options[event.target.value].innerText,
      disableEditButton: false,
    });
  };

  const handleEditTemplate = async () => {
    try {
      const updateTemplateInput = {
        id: templateListData.templateId,
        title: templateListData.templateName,
        categoryID: templateListData.categoryId,
        isGlobal: templateListData.global,
        active: templateListData.active,
      };
      const updatedStatus = await handleUpdateCustomTemplateMutation(updateTemplateInput);
      const templateData = {
        id: templateListData.templateId,
        title: templateListData.templateName,
        categoryId: templateListData.categoryId,
        isGlobal: templateListData.global,
      };
      if (updatedStatus) {
        setEditTemplateShow();
        if (props.filterValue === '' && props.globalFilter === '' && props.searchValue === '')
          templateFilter(
            templateData,
            templateListData.categoryName,
            templateListData.index,
            false,
          );
        else {
          const filterArray = props.filteredTemplate;
          filterArray[templateListData.index].title = templateData.title;
          filterArray[templateListData.index].category.id = templateData.categoryId;
          filterArray[templateListData.index].categoryID = templateData.categoryId;
          filterArray[templateListData.index].category.name = templateListData.categoryName;
          setTemplateListData({
            ...templateListData,
            filteredTemplate: filterArray,
          });
          templateFilter(templateData, templateListData.categoryName, templateListData.index, true);
        }
      }
    } catch (e) {
      console.error('error on updating custom template');
    }
  };

  const templateFilter = (
    updatedTemplate: { id: string; title: string; categoryId: string | number; isGlobal: boolean },
    categoryName: string,
    index: number,
    isFiltered: boolean,
  ) => {
    const templatesArray = isFiltered
      ? templateListData.filteredTemplate
      : templateListData.customTemplateLists;
    templatesArray[index].title = updatedTemplate.title;
    templatesArray[index].category.id = updatedTemplate.categoryId;
    templatesArray[index].categoryID = updatedTemplate.categoryId;
    templatesArray[index].category.name = categoryName;
    isFiltered
      ? setTemplateListData({ ...templateListData, filteredTemplate: templatesArray })
      : setTemplateListData({ ...templateListData, customTemplateLists: templatesArray });
    setTemplateListData({
      ...templateListData,
      showToast: true,
      toastMessage: 'Template updated successfully',
    });
  };

  const handleRestore = async (
    templateId: string,
    categoryId: string,
    title: string,
    isGlobal: boolean,
    index: number,
  ) => {
    const updateTemplateInput = {
      id: templateId,
      title: title,
      isGlobal: isGlobal,
      categoryID: categoryId,
      active: true,
    };
    const updatedStatus = await handleUpdateCustomTemplateMutation(updateTemplateInput);
    if (updatedStatus) {
      let templatesArray;
      if (props.filterValue !== '' || props.globalFilter !== '' || props.searchValue !== '') {
        templatesArray = props.filteredTemplate;
        templatesArray[index].active = true;
        templatesArray[index].updatedAt = new Date().toISOString();
        setTemplateListData({
          ...templateListData,
          filteredTemplate: templatesArray,
        });
      } else {
        templatesArray = templateListData.customTemplateLists;
        templatesArray[index].active = true;
        templatesArray[index].updatedAt = new Date().toISOString();
        setTemplateListData({
          ...templateListData,
          customTemplateLists: templatesArray,
        });
      }
      setTemplateListData({
        ...templateListData,
        showToast: true,
        toastMessage: 'Template restored successfully',
      });
    }
  };

  const handleCloseLmsLoader = () => {
    setTemplateListData({ ...templateListData, savingLoader: false });
  };

  const templateListSorted = (
    ordertype: string,
    sortableOrder: string,
    orderingElements: CustomElement,
  ) => {
    const orderTypes = ordertype === 'asc' ? 'desc' : 'asc';
    const toggleTypes = toggleOrderType(sortableOrder, orderTypes);
    const fetchTemplateListData =
      props.searchValue === '' && props.filterValue === '' && props.globalFilter === ''
        ? templateListData.customTemplateLists
        : templateListData.filteredTemplate;

    const TemplateListDatas = [...fetchTemplateListData];

    if (orderingElements === 'category') {
      TemplateListDatas.sort((firstCustomElement, secondCustomElement) =>
        firstCustomElement.category.name.localeCompare(secondCustomElement.category.name),
      );
    } else {
      TemplateListDatas.sort((firstCustomElement, secondCustomElement) =>
        firstCustomElement[orderingElements].localeCompare(secondCustomElement[orderingElements]),
      );
    }

    if (ordertype !== 'asc') {
      TemplateListDatas.reverse();
    }
    setTemplateListData({
      ...templateListData,
      filteredTemplate: TemplateListDatas,
      customTemplateLists: TemplateListDatas,
      ...toggleTypes,
    });
  };
  const toggleOrderType = (sortableOrder: string, orderType: string) => {
    switch (sortableOrder) {
      case 'templateList_name':
        return {
          elementNames: orderType,
        };
      case 'category':
        return {
          category: orderType,
        };
      case 'created_By':
        return {
          createdBy: orderType,
        };
      case 'created_At':
        return {
          createdAt: orderType,
        };
      case 'last_updated':
        return {
          lastUpdated: orderType,
        };
      default:
        return null;
    }
  };

  return templateListData.loading ? (
    <Loading />
  ) : templateListData.loading ? (
    <Loading />
  ) : (props.filterValue === '' && props.globalFilter === '' && props.searchValue === ''
      ? templateListData.customTemplateLists
      : templateListData.filteredTemplate
    ).length > 0 ? (
    <div>
      <EditTemplate
        templateName={templateListData.templateName}
        showEditModal={templateListData.showEditModal}
        categoryId={templateListData.categoryId}
        categories={props.categories}
        setEditTemplateShow={setEditTemplateShow}
        templateNameHandler={templateNameHandler}
        handleCategory={handleCategory}
        handleEditTemplate={handleEditTemplate}
        disableEditButton={templateListData.disableEditButton}
        showToast={templateListData.showToast}
        toastMessage={templateListData.toastMessage}
        closeToast={closeToast}
        tabType={props.tabType}
      />
      <LmsLoader
        manualSaving={templateListData.savingLoader}
        closeLoaderModal={handleCloseLmsLoader}
        type='global'
        action={
          templateListData.global
            ? translate('global.removingfromglobal')
            : translate('global.addingtoglobal')
        }
      />
      <Table responsive borderless size='sm' className='table-page'>
        <thead>
          <tr>
            <th>
              <span
                className='clickableOrderContent'
                onClick={() => {
                  templateListSorted(
                    templateListData.elementNames,
                    'templateList_name',
                    CustomElement.title,
                  );
                }}
                aria-hidden='true'
              >
                {translate('global.templatename')}
                <span className='position-absolute ml-2'>
                  <FontAwesomeIcon
                    icon={templateListData.elementNames === 'asc' ? faCaretDown : faCaretUp}
                  />
                </span>
              </span>
            </th>
            <th>
              <span
                className='clickableOrderContent'
                onClick={() => {
                  templateListSorted(templateListData.category, 'category', CustomElement.category);
                }}
                aria-hidden='true'
              >
                {translate('global.category')}{' '}
                <span className='position-absolute ml-2'>
                  <FontAwesomeIcon
                    icon={templateListData.category === 'asc' ? faCaretUp : faCaretDown}
                  />
                </span>
              </span>
            </th>
            {/* Added d-none class for temporary , once D2l file management is completed, will remove it. */}
            <th className={!isCanvasAndBB() ? 'd-none' : ''}>{translate('global.global')}</th>
            <th>
              <span
                className='clickableOrderContent'
                onClick={() => {
                  templateListSorted(
                    templateListData.createdBy,
                    'created_By',
                    CustomElement.createdBy,
                  );
                }}
                aria-hidden='true'
              >
                {translate('global.createdby')}
                <span className='position-absolute ml-2'>
                  <FontAwesomeIcon
                    icon={templateListData.createdBy === 'asc' ? faCaretUp : faCaretDown}
                  />
                </span>
              </span>
            </th>
            <th>
              <span
                className='clickableOrderContent'
                onClick={() => {
                  templateListSorted(
                    templateListData.createdAt,
                    'created_At',
                    CustomElement.createdAt,
                  );
                }}
                aria-hidden='true'
              >
                {translate('global.createdat')}{' '}
                <span className='position-absolute ml-2'>
                  <FontAwesomeIcon
                    icon={templateListData.createdAt === 'asc' ? faCaretUp : faCaretDown}
                  />
                </span>
              </span>
            </th>
            <th>
              <span
                className='clickableOrderContent'
                onClick={() => {
                  templateListSorted(
                    templateListData.lastUpdated,
                    'last_updated',
                    CustomElement.updatedAt,
                  );
                }}
                aria-hidden='true'
              >
                {translate('global.lastupdated')}
                <span className='position-absolute ml-2'>
                  <FontAwesomeIcon
                    icon={templateListData.lastUpdated === 'asc' ? faCaretUp : faCaretDown}
                  />
                </span>
              </span>
            </th>
            <th>{translate('global.edit')}</th>
            <th>{translate('global.restore')}</th>
          </tr>
        </thead>
        <tbody>
          {(props.filterValue === '' && props.globalFilter === '' && props.searchValue === ''
            ? templateListData.customTemplateLists
            : templateListData.filteredTemplate
          ).map((template: BlockInfo, index: number) => (
            <tr key={index} className='admin-table'>
              <td className='font-weight-bold text-truncate overflowText'>{template.title}</td>
              <td className='text-truncate overflowText'>{template.category?.name}</td>
              <td className={!isCanvasAndBB() ? 'd-none' : ''}>
                <input
                  type='checkbox'
                  id={template.id}
                  onChange={async () =>
                    await changeTemplateGlobalStatus(
                      template.id,
                      template.title,
                      template.category.id,
                      template.isGlobal,
                      index,
                      template.active,
                    )
                  }
                  checked={template.isGlobal}
                />
              </td>
              <td>{template.createdBy}</td>
              <td>{new Date(template.createdAt).toDateString()}</td>
              <td>{new Date(template.updatedAt).toDateString()}</td>
              <td>
                <button
                  onClick={async () =>
                    await openEditModal(
                      template.id,
                      template.title,
                      template.category.name,
                      template.category.id,
                      template.isGlobal,
                      template.active,
                      index,
                    )
                  }
                  style={{ background: '#eee', border: '#eee' }}
                >
                  <EditPencil />
                </button>
              </td>
              <td className='text-center'>
                {!template.active && (
                  <FontAwesomeIcon
                    icon={faUndo}
                    onClick={async () =>
                      await handleRestore(
                        template.id,
                        template.category.id,
                        template.title,
                        template.isGlobal,
                        index,
                      )
                    }
                    className='mr-2'
                  />
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  ) : (
    <div className='container'>
      <div className='p-3'>{translate('template.nocustomtemplatefound')}</div>
    </div>
  );
};

export default React.memo(TemplateLists);
