import React from 'react';
import { mount } from 'enzyme';
import EditTemplateModal, { TabType } from './editTemplateModal';

const EditTemplateModalProps = {
  templateName: 'test-block',
  showEditModal: true,
  categoryId: 'fae3e9ec-a86b-4cdd-ace9-4e4b31a38100',
  categories: [{ id: 'fae3e9ec-a86b-4cdd-ace9-4e4b31a38100', name: 'coloring' }],
  setEditTemplateShow: jest.fn(),
  templateNameHandler: jest.fn(),
  handleCategory: jest.fn(),
  handleEditTemplate: jest.fn(),
  disableEditButton: false,
  showToast: true,
  toastMessage: 'Updated successfully',
  closeToast: jest.fn(),
  tabType: TabType.Element,
};

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

let EditBlocksModalComponent = mount(<EditTemplateModal {...EditTemplateModalProps} />);
describe('edit Blocks', () => {
  describe('when display the popup modal', () => {
    test('title as Edit element', () => {
      expect(EditBlocksModalComponent.find('.modal-title').at(0).text()).toEqual(
        'template.editelement',
      );
    });
    test('with close Button', () => {
      expect(EditBlocksModalComponent.find('.modal-header').children().at(1).text()).toEqual(
        '×Close',
      );
    });
    describe('when user clicks on close button', () => {
      const setEditTemplateShow = jest.fn();
      test.skip('modal is closed', () => {
        EditBlocksModalComponent.find('.modal-header')
          .children()
          .at(1)
          .simulate('click', setEditTemplateShow);
        expect(setEditTemplateShow).toHaveBeenCalled();
      });
    });
    describe('when label is rendered on tab type', () => {
      test('field label as Element Name', () => {
        expect(EditBlocksModalComponent.find('.modal-body').children().at(0).text()).toEqual(
          'template.elementname',
        );
      });
      test('field label as Template Name', () => {
        EditTemplateModalProps.tabType = TabType.Template;
        EditTemplateModalProps.showToast = true;
        EditTemplateModalProps.toastMessage = 'Updated successfully';
        EditBlocksModalComponent = mount(<EditTemplateModal {...EditTemplateModalProps} />);
        expect(EditBlocksModalComponent.find('.modal-body').children().at(0).text()).toEqual(
          'template.templatename',
        );
      });
      test('field label as Row Name', () => {
        EditTemplateModalProps.tabType = TabType.Row;
        EditTemplateModalProps.showToast = true;
        EditTemplateModalProps.toastMessage = 'Updated successfully';
        EditBlocksModalComponent = mount(<EditTemplateModal {...EditTemplateModalProps} />);
        expect(EditBlocksModalComponent.find('.modal-body').children().at(0).text()).toEqual(
          'global.rowname',
        );
      });
    });

    describe('form onSubmit is triggered', () => {
      test('onsubmit function is called', () => {
        const onSubmitFn = jest.fn();
        EditBlocksModalComponent.find('form').simulate('submit', onSubmitFn);
        expect(onSubmitFn).toHaveBeenCalledTimes(0);
      });

      describe('when a input field', () => {
        const input = EditBlocksModalComponent.find('input');
        test('with type as text', () => {
          expect(input.prop('type')).toEqual('text');
        });
        test('with placeholder', () => {
          expect(input.prop('placeholder')).toEqual('template.elementname');
        });
        test('entering the text, onchange function triggers', () => {
          // Testing the onchange functionlaity of input field
          const title = EditBlocksModalComponent.find('input').at(0);
          title.simulate('change');
        });
      });
    });

    describe('when a modal-footer', () => {
      const setEditTemplateShow = jest.fn();
      const handleEditTemplate = jest.fn();
      // Testing the cancel button & it's functionality.
      describe('with cancel button', () => {
        test('with the label', () => {
          expect(EditBlocksModalComponent.find('.modal-footer').children().at(0).text()).toEqual(
            'global.cancel',
          );
        });
        test.skip('when the cancel button is triggered', () => {
          EditBlocksModalComponent.find('.modal-footer')
            .children()
            .at(0)
            .simulate('click', setEditTemplateShow);
          expect(handleEditTemplate).toHaveBeenCalled();
        });
      });

      // Testing the update button & it's functionality
      describe('update button', () => {
        test('with the label', () => {
          expect(EditBlocksModalComponent.find('.modal-footer').children().at(1).text()).toEqual(
            'global.update',
          );
        });
        test.skip('onclick function is triggered', () => {
          EditBlocksModalComponent.find('.modal-footer')
            .children()
            .at(1)
            .simulate('click', handleEditTemplate);
          expect(handleEditTemplate).toHaveBeenCalled();
        });
      });
    });
  });
  describe('toast component', () => {
    describe('with triggered toaster', () => {
      test('when updated message', () => {
        expect(EditBlocksModalComponent.find('strong').text()).toEqual('Updated successfully');
      });
      describe('when the close button is triggered', () => {
        test.skip('toast is closed', () => {
          const closeToast = jest.fn();
          EditBlocksModalComponent.find('.close').children().at(0).simulate('click', closeToast);
          expect(closeToast).toHaveBeenCalled();
        });
      });
    });

    describe('without triggered toaster', () => {
      EditTemplateModalProps.showToast = false;
      EditTemplateModalProps.toastMessage = '';
      const wrapper = mount(<EditTemplateModal {...EditTemplateModalProps} />);
      test('render with the error text', () => {
        expect(wrapper.find('strong')).toHaveLength(0);
      });
    });
  });
});
