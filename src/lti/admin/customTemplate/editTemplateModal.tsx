/* eslint-disable react/jsx-no-bind */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { Fragment } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { translate } from '../../../i18n/translate';
import ToastComponent from '../../components/ToastComponent';

export enum TabType {
  Template = 0,
  Row = 1,
  Element = 2,
}
interface EditTemplateProps {
  templateName: string;
  showEditModal: boolean;
  categoryId: string | number;
  categories: any;
  setEditTemplateShow: () => void;
  templateNameHandler: (event: any) => void;
  handleCategory: (event: any) => void;
  handleEditTemplate: () => void;
  disableEditButton: boolean;
  showToast: boolean;
  toastMessage: string;
  closeToast: () => void;
  tabType: TabType;
}

class EditTemplate extends React.Component<EditTemplateProps> {
  getTabTypeName = (tabType: TabType) => {
    if (tabType === TabType.Template) {
      return translate('template.templatename');
    }
    if (tabType === TabType.Row) {
      return translate('global.rowname');
    }
    return translate('template.elementname');
  };

  getTabTypeEdit = (tabType: TabType) => {
    if (tabType === TabType.Template) {
      return translate('template.edittemplate');
    }
    if (tabType === TabType.Row) {
      return translate('global.editrow');
    }
    return translate('template.editelement');
  };

  render() {
    return (
      <>
        <Modal
          animation={false}
          aria-labelledby='create-new-page'
          backdrop='static'
          centered
          onHide={() => this.props.setEditTemplateShow()}
          show={this.props.showEditModal}
          size='sm'
        >
          <Modal.Header closeButton>
            <Modal.Title id='create-new-page' className='pageBlock'>
              <h5 className='text-primary'>{this.getTabTypeEdit(this.props.tabType)}</h5>
            </Modal.Title>
          </Modal.Header>
          <Form onSubmit={(event) => event.preventDefault()}>
            <Modal.Body className='create-page-form'>
              <Form.Group controlId='formPageName'>
                <Form.Label>{this.getTabTypeName(this.props.tabType)}</Form.Label>
                <Form.Control
                  required
                  type='text'
                  placeholder={this.getTabTypeName(this.props.tabType)}
                  autoComplete='off'
                  value={this.props.templateName}
                  onChange={this.props.templateNameHandler.bind(this)}
                  maxLength={100}
                />
              </Form.Group>
              <Form.Group controlId='formTemplateCategory'>
                <Form.Label>{translate('template.selectcategory')}</Form.Label>
                <Form.Control
                  as='select'
                  className='template-category-dropdown'
                  onChange={this.props.handleCategory.bind(this)}
                  defaultValue={this.props.categoryId}
                >
                  {this.props.categories.map((category: any) => (
                    <option value={category.id} id={category.id} key={category.id}>
                      {category.name}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Modal.Body>
            <Modal.Footer style={{ justifyContent: 'center' }}>
              <Button
                variant='outline-primary'
                className='mx-1'
                onClick={() => this.props.setEditTemplateShow()}
              >
                {translate('global.cancel')}
              </Button>
              <Button
                variant='primary'
                className='mx-1'
                onClick={() => this.props.handleEditTemplate()}
                disabled={this.props.disableEditButton}
              >
                {translate('global.update')}
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
        {this.props.showToast && (
          <ToastComponent
            showToast={this.props.showToast}
            toastMessage={this.props.toastMessage}
            closeToast={this.props.closeToast}
          />
        )}
      </>
    );
  }
}

export default EditTemplate;
