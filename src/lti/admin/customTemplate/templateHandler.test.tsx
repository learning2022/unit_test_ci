import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import CustomTemplates from './templateHandler';
import { StorageMock } from '../../../utils/storageMock';
import { screen } from '@testing-library/dom';
import {
  customTemplateGlobalList,
  customTemplateGlobalUpdateResponse,
  customTemplateList,
  customTemplateUpdateResponse,
  customTemplateGlobalSortedList,
  customTemplateGlobalReverseList,
} from './customBlockMockData';
import { getElementsByClassName } from '../../../loree-editor/common/dom';
import { getElementsByTagName } from '../../../loree-editor/utils';
import API from '@aws-amplify/api';
import { globalTemplatesResponse } from '../../../loree-editor/modules/customBlocks/customBlockMockData';
import { translate } from '../../../i18n/translate';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('custom Element ascending and descending checking', () => {
  let templateProps: _Any;
  beforeEach(() => {
    templateProps = customTemplateGlobalList;
    render(<CustomTemplates {...templateProps} />);
  });
  test('to check the table customTemplateheader present or not in the document', () => {
    const customTemplateHeaderTitleElement = screen.getByText(translate('global.templatename'));
    expect(customTemplateHeaderTitleElement).toBeInTheDocument();
    const customTemplateHeaderTitleElementHeaderCategoryElement = screen.getByText(
      translate('global.category'),
    );
    expect(customTemplateHeaderTitleElementHeaderCategoryElement).toBeInTheDocument();
    const customTemplateHeaderTitleElementHeaderCreatedByElement = screen.getByText(
      translate('global.createdby'),
    );
    expect(customTemplateHeaderTitleElementHeaderCreatedByElement).toBeInTheDocument();
    const customTemplateHeaderTitleElementHeaderCreatedAtElement = screen.getByText(
      translate('global.createdat'),
    );
    expect(customTemplateHeaderTitleElementHeaderCreatedAtElement).toBeInTheDocument();
    const customTemplateHeaderTitleElementHeaderLastUpdatedElement = screen.getByText(
      translate('global.lastupdated'),
    );
    expect(customTemplateHeaderTitleElementHeaderLastUpdatedElement).toBeInTheDocument();
  });
  test('check the length of the customTemplatelist', () => {
    expect(customTemplateGlobalList.customTemplateLists.length).toEqual(4);
  });
  test('check to the fuction is called ,sorted and reverse', () => {
    const customTemplateHeaderTitleElement = screen.getByText(translate('global.templatename'));
    const dataProperty = [...customTemplateGlobalList.customTemplateLists];
    customTemplateHeaderTitleElement.click();
    const customTemplateSortedData = dataProperty.sort((a, b) => a.title.localeCompare(b.title));
    expect(customTemplateSortedData).toEqual(customTemplateGlobalSortedList.customTemplateLists);
    const customTemplateReverseData = customTemplateSortedData.map((data) => data);
    expect(customTemplateReverseData.reverse()).toEqual(
      customTemplateGlobalReverseList.customTemplateLists,
    );
  });
});

describe('#custom template', () => {
  let templateProps: _Any;
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as _Any;
    sessionStorage.setItem('domainName', 'canvas');
    templateProps = customTemplateGlobalList;
    render(<CustomTemplates {...templateProps} />);
  });

  test('rendered the list', () => {
    expect(getElementsByTagName('div')).toHaveLength(3);
  });

  test('rendered the list in table', () => {
    expect(getElementsByTagName('table')).toHaveLength(1);
  });

  test('table heading contains a template name', () => {
    expect(getElementsByTagName('span')[0].innerHTML).toEqual(
      'global.templatename<span class="position-absolute ml-2"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="caret-down" class="svg-inline--fa fa-caret-down fa-w-10 " role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M31.3 192h257.3c17.8 0 26.7 21.5 14.1 34.1L174.1 354.8c-7.8 7.8-20.5 7.8-28.3 0L17.2 226.1C4.6 213.5 13.5 192 31.3 192z"></path></svg></span>',
    );
  });

  test('template name shows in table definition', () => {
    expect(getElementsByTagName('td')[0].innerHTML).toEqual(
      templateProps.customTemplateLists[0].title,
    );
  });

  test('global option enabled for canvas', () => {
    expect(getElementsByTagName('th')[2].outerHTML).toEqual(`<th class="">global.global</th>`);
  });

  test('the onclick function is triggered and modal is opened', () => {
    fireEvent.click(getElementsByTagName('button')[0]);
    expect(getElementsByClassName('modal-backdrop')[0]).toBeInTheDocument();
  });

  test('trigger global', () => {
    fireEvent.change(getElementsByTagName('input')[0]);
    const element = getElementsByTagName('input')[0] as HTMLInputElement;
    expect(element.checked).toEqual(true);
  });
});

describe('#remove global functionality for templates', () => {
  let templateProps: _Any;
  beforeEach(() => {
    jest.clearAllMocks();
    global.sessionStorage = new StorageMock() as _Any;
    sessionStorage.setItem('domainName', 'canvas');
  });

  test('template should be removed from global on click', () => {
    templateProps = customTemplateGlobalList;
    render(<CustomTemplates {...templateProps} />);
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => customTemplateUpdateResponse)
      .mockImplementation(() => globalTemplatesResponse);
    fireEvent.click(getElementsByTagName('input')[0]);
    const isGlobal = getElementsByTagName('p')[0];
    expect(isGlobal.innerHTML).toBe('global.removingfromglobal');
  });

  test('template should be made global on click', () => {
    templateProps = customTemplateList;
    render(<CustomTemplates {...templateProps} />);
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => customTemplateGlobalUpdateResponse)
      .mockImplementation(() => globalTemplatesResponse);
    fireEvent.click(getElementsByTagName('input')[0]);
    const isGlobal = getElementsByTagName('p')[0];
    expect(isGlobal.innerHTML).toBe('global.addingtoglobal');
  });

  test('whether global popup modal class is added', () => {
    templateProps = customTemplateList;
    render(<CustomTemplates {...templateProps} />);
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => customTemplateGlobalUpdateResponse)
      .mockImplementation(() => globalTemplatesResponse);
    fireEvent.click(getElementsByTagName('input')[0]);
    const isGlobalClass = getElementsByClassName('lms-loader-folder-icon-global')[0];
    expect(isGlobalClass).toBeDefined();
  });
});
