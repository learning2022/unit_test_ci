/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Form } from 'react-bootstrap';
import { ReactComponent as SearchIcon } from '../../../assets/Icons/search.svg';
import { isCanvasAndBB } from '../../../lmsConfig';

interface FilterOptionProps {
  search: any;
  handleSearchChange: (event: any) => void;
  handleSearch: () => void;
  handleGlobalFilter: (event: any) => void;
  handleCategoryFilter: (event: any) => void;
  categories: any;
}

class FilterOptions extends React.Component<FilterOptionProps> {
  render() {
    return (
      <div className='d-flex template-tools align-items-center float-right'>
        <div className='d-inline-block float-right pr-2 mb-3 position-relative'>
          <input
            id='search'
            name='search'
            className={!this.props.search ? 'd-none' : 'search pr-5'}
            type='text'
            autoComplete='off'
            placeholder='Search'
            onChange={e => this.props.handleSearchChange(e)}
            autoFocus
          />
          <SearchIcon
            width={!this.props.search ? 24 : 15}
            height={!this.props.search ? 20 : 12}
            className={!this.props.search ? 'mx-2 search-icon-normal' : 'mx-2 search-icon iconActive'}
            data-test='searchbtn'
            onClick={this.props.handleSearch}
          />
        </div>
        {/* Added d-none class for temporary , once D2l file management is completed, will remove it. */}
        <div className={!isCanvasAndBB() ? 'd-none' : 'form-check'}>
          <input
            type='checkbox'
            id='global-filter'
            onChange={event => this.props.handleGlobalFilter(event)}
            value='global-filter'
            className='form-check-input'
          />
          <label className='form-check-label' htmlFor='global-filter'>
            Global
          </label>
        </div>
        <Form.Group>
          <Form.Control as='select' onChange={(event: any) => this.props.handleCategoryFilter(event)} id='categoryfilter'>
            <option value=''>Select Category</option>
            {this.props.categories.map((category: any) => (
              <option value={category.id} key={category.id}>
                {category.name}
              </option>
            ))}
          </Form.Control>
          <div className='arrow-down'></div>
        </Form.Group>
      </div>
    );
  }
}

export default FilterOptions;
