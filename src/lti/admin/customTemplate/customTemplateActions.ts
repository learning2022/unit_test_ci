import {
  categoryByPlatform,
  listCustomTemplates,
  listCustomBlocks,
} from '../../../graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import { updateCustomBlock, updateCustomTemplate } from '../../../graphql/mutations';
import {
  CategoryByPlatformQuery,
  ListCustomBlocksQuery,
  ListCustomTemplatesQuery,
  UpdateCustomBlockMutation,
  UpdateCustomTemplateMutation,
} from '../../../API';
import { getGlobalTemplateQueryContext } from '../../../loree-editor/modules/customBlocks/blocksQueryUtils';
let ltiPlatformId = '';

export const setLtiPlatformId = (id: string) => {
  ltiPlatformId = id;
};

export const handleCategoryAPI = async () => {
  const categoryLists = await API.graphql<CategoryByPlatformQuery>(
    graphqlOperation(categoryByPlatform, { ltiPlatformID: ltiPlatformId }),
  );
  return categoryLists?.data?.categoryByPlatform?.items ?? [];
};

const handlingTemplateQuery = async (graphqlOperationData: {} | undefined) => {
  return await API.graphql<ListCustomTemplatesQuery>(
    graphqlOperation(listCustomTemplates, graphqlOperationData),
  );
};

export const handleCustomTemplateAPI = async () => {
  const listTemplates = [];
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getGlobalTemplateQueryContext();
  const listTemplatesResponse = await handlingTemplateQuery(graphqlOperationData);
  listTemplates.push(...(listTemplatesResponse?.data?.listCustomTemplates?.items ?? []));
  let nextToken = listTemplatesResponse?.data?.listCustomTemplates?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const listTemplatesResponseLoop = await handlingTemplateQuery(graphqlOperationData);
    listTemplates.push(...(listTemplatesResponseLoop?.data?.listCustomTemplates?.items ?? []));
    nextToken = listTemplatesResponseLoop?.data?.listCustomTemplates?.nextToken as string | null;
  }
  return listTemplates;
};

const handlingBlocksQuery = async (graphqlOperationData: {} | undefined) => {
  return await API.graphql<ListCustomBlocksQuery>(
    graphqlOperation(listCustomBlocks, graphqlOperationData),
  );
};

export const handleCustomBlocks = async () => {
  const listBlocks = [];
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getGlobalTemplateQueryContext();
  const listRowsResponse = await handlingBlocksQuery(graphqlOperationData);
  listBlocks.push(...(listRowsResponse?.data?.listCustomBlocks?.items ?? []));
  let nextToken = listRowsResponse?.data?.listCustomBlocks?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const listTemplatesResponseLoop = await handlingBlocksQuery(graphqlOperationData);
    listBlocks.push(...(listTemplatesResponseLoop?.data?.listCustomBlocks?.items ?? []));
    nextToken = listTemplatesResponseLoop?.data?.listCustomBlocks?.nextToken as string | null;
  }
  return listBlocks;
};

export const handleCustomRowAPI = async () => {
  return (await handleCustomBlocks()).filter((rowData) => {
    return rowData?.type === 'Row';
  });
};

export const handleCustomElementsAPI = async () => {
  return (await handleCustomBlocks()).filter((elementsData) => {
    return elementsData?.type === 'Elements';
  });
};

export const handleUpdateCustomBlockMutation = async (updateInput: {}) => {
  const updateResponse = await API.graphql<UpdateCustomBlockMutation>(
    graphqlOperation(updateCustomBlock, { input: updateInput }),
  );
  return updateResponse?.data?.updateCustomBlock ?? [];
};

export const handleUpdateCustomTemplateMutation = async (updateInput: {}) => {
  const updateResponse = await API.graphql<UpdateCustomTemplateMutation>(
    graphqlOperation(updateCustomTemplate, { input: updateInput }),
  );
  return updateResponse?.data?.updateCustomTemplate;
};
