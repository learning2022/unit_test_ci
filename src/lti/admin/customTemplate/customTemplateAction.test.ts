import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from '../../../aws-exports';
import { StorageMock } from '../../../utils/storageMock';
import {
  blocksMockData,
  templateMockData,
  userMockData,
} from '../../../loree-editor/modules/customBlocks/customBlockMockData';
import { categoryMockData } from './customBlockMockData';
import {
  handleCategoryAPI,
  handleCustomBlocks,
  handleCustomElementsAPI,
  handleCustomRowAPI,
  handleCustomTemplateAPI,
} from './customTemplateActions';

const categorySampleData = {
  data: {
    categoryByPlatform: { items: categoryMockData },
  },
};

describe('when calling category api function', () => {
  beforeEach(() => {
    API.graphql = jest.fn().mockImplementation(() => categorySampleData);
  });
  test('it returns the empty value', async () => {
    expect((await handleCategoryAPI()).length).toEqual(3);
  });
  test('if name value is present', async () => {
    expect((await handleCategoryAPI())[0]?.name).toEqual('byplatform-canvas21');
  });
  test('if name value is not present', async () => {
    expect((await handleCategoryAPI())[1]?.name).toEqual(undefined);
  });
});

describe('#templates', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
  });
  describe('templates without nextToken', () => {
    const listTemplatesResponse = {
      data: {
        listCustomTemplates: {
          items: [templateMockData],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest.fn().mockImplementation(() => listTemplatesResponse);
    });
    test('retrieving my templates', async () => {
      expect((await handleCustomTemplateAPI()).length).toEqual(1);
    });
  });
  describe('templates with nextToken', () => {
    const listTemplatesResponseOne = {
      data: {
        listCustomTemplates: {
          items: [templateMockData],
          nextToken: 'dfheriuwfhjsdkshf',
        },
      },
    };
    const listTemplatesResponse = {
      data: {
        listCustomTemplates: {
          items: [
            {
              id: '1',
              title: 'test',
              content: '<p>hello</p>',
            },
          ],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => listTemplatesResponseOne)
        .mockImplementationOnce(() => listTemplatesResponse);
    });
    test('retrieving templates', async () => {
      expect((await handleCustomTemplateAPI()).length).toEqual(2);
    });
  });
});

describe('#blocks', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
  });
  describe('blocks without nextToken', () => {
    const listBlocksResponse = {
      data: {
        listCustomBlocks: {
          items: blocksMockData,
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest.fn().mockImplementation(() => listBlocksResponse);
    });
    test('retrieving my blocks', async () => {
      expect((await handleCustomBlocks()).length).toEqual(2);
    });
  });
  describe('blocks with nextToken', () => {
    const listBlocksResponse = {
      data: {
        listCustomBlocks: {
          items: blocksMockData,
          nextToken: 'dfheriuwfhjsdkshf',
        },
      },
    };
    const listBlocksResponseWithToken = {
      data: {
        listCustomBlocks: {
          items: [
            {
              id: '3',
              title: 'test-3',
              content: '<p>hello test-3</p>',
            },
          ],
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => listBlocksResponse)
        .mockImplementationOnce(() => listBlocksResponseWithToken);
    });
    test('retrieving blocks', async () => {
      expect((await handleCustomBlocks()).length).toEqual(3);
    });
  });
  describe('#blocks type', () => {
    const listBlocksResponse = {
      data: {
        listCustomBlocks: {
          items: blocksMockData,
          nextToken: null,
        },
      },
    };
    beforeEach(() => {
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest.fn().mockImplementation(() => listBlocksResponse);
    });
    test('row type', async () => {
      const rowsData = await handleCustomRowAPI();
      expect(rowsData[0]?.type).toEqual('Row');
    });
    test('element type', async () => {
      const elementsData = await handleCustomElementsAPI();
      expect(elementsData[0]?.type).toEqual('Elements');
    });
  });
});
