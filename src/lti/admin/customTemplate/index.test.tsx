/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import CustomTemplate from './index';
import { API } from 'aws-amplify';
import { filteredTemplateMockData, templateMockData } from './customBlockMockData';
API.graphql = jest.fn().mockImplementation((arg) => {
  if (arg.query.includes('LoreeTemplates')) {
    return templateMockData;
  } else {
    return {
      data: {
        listCategorys: {
          items: [],
        },
      },
    };
  }
});
describe('Custom template component', () => {
  describe('Global filter enabled', () => {
    let mockFilteredTemplateData: {};
    beforeEach(() => {
      mockFilteredTemplateData = filteredTemplateMockData;
    });
    test.skip('results filtered by global', () => {
      const wrapper = mount(<CustomTemplate />);
      const filter = wrapper.find('#global-filter');
      filter.simulate('change', { target: { checked: true } });
      wrapper.setState({ filteredTemplate: [mockFilteredTemplateData] });
      const filteredTemplates: Array<{}> = wrapper.state('filteredTemplate');
      expect(wrapper.state('globalFilter')).toBeTruthy();
      expect(filteredTemplates.length).toBe(1);
    });
  });
});
