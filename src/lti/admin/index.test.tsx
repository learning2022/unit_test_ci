import React from 'react';
import { AdminDashboard } from './index';
import { adminDashboardPropsMockData } from './adminDashboardMockData';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { getElementsByClassName } from '../../loree-editor/common/dom';

describe('rendering admin dahsboard', () => {
  test('verifying navlink text', () => {
    sessionStorage.setItem('isAdmin', 'true');
    render(
      <BrowserRouter>
        <AdminDashboard {...adminDashboardPropsMockData} />
      </BrowserRouter>,
    );
    expect(getElementsByClassName('btn')[0].innerHTML).toBe('Exit Admin');
  });
});
