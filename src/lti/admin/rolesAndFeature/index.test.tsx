import React from 'react';
import { shallow } from 'enzyme';
import RolesAndFeature from './index';
import { StorageMock } from '../../../utils/storageMock';
import { API } from 'aws-amplify';
import { listLoreeFeatures, listLoreeRoles } from './rolesAndFeature.mock';
import {
  adminRolesMock,
  courseDetailsMock,
  loreeLmsRoleWithoutTokenMockData,
  loreeLmsRoleWithTokenMockData,
  loreeRoleWithoutTokenMockData,
  loreeRoleWithTokenMockData,
} from './rolesAndFeatureMockData';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('RolesAndFeature', () => {
  let rolesAndFeature: _Any;

  beforeEach(() => {
    rolesAndFeature = shallow(<RolesAndFeature />);
    global.sessionStorage = new StorageMock() as any;
    sessionStorage.setItem('domainName', 'canvas');
    sessionStorage.setItem('course_id', '48');
  });
  test('should check fetchCourseDetails', () => {
    API.graphql = jest.fn().mockImplementation(() => courseDetailsMock);
    const fetchCourseDetailSpy = jest.spyOn(RolesAndFeature.prototype, 'fetchCourseDetails');
    RolesAndFeature.prototype.fetchCourseDetails();
    expect(fetchCourseDetailSpy).toHaveBeenCalled();
  });

  test('render with header', () => {
    expect(rolesAndFeature.find('h3').text()).toEqual('global.rolesfeatures');
  });

  test('render with button', () => {
    expect(rolesAndFeature.find('Button').at(0).text()).toEqual('Create New role');
    rolesAndFeature.find('Button').at(0).simulate('click');
  });

  describe('render with the tab', () => {
    test('with default key', () => {
      expect(rolesAndFeature.find('.mt-3').props().defaultActiveKey).toEqual('loreeroles');
    });

    describe('with child nodes', () => {
      test('with the label of the first child', () => {
        expect(rolesAndFeature.find('.mt-3').children().at(0).props().title).toEqual('Loree Roles');
      });
      test('with label of the second child', () => {
        expect(rolesAndFeature.find('.mt-3').children().at(1).props().title).toEqual(
          'Canvas Roles',
        );
      });
    });
  });

  test('should check navigation to canvas roles', () => {
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => adminRolesMock)
      .mockImplementationOnce(() => loreeLmsRoleWithTokenMockData)
      .mockImplementationOnce(() => loreeLmsRoleWithoutTokenMockData)
      .mockImplementationOnce(() => loreeRoleWithTokenMockData)
      .mockImplementationOnce(() => loreeRoleWithoutTokenMockData);
    rolesAndFeature.find('Tabs').simulate('select', 'canvasroles');
    expect(rolesAndFeature.find('Button').at(0).text()).toEqual('sub-acc');
  });

  describe('tab access on initial render', () => {
    beforeAll(() => {
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => listLoreeRoles)
        .mockImplementationOnce(() => listLoreeFeatures);
    });
    test('lms role tab must be disabled on initial', () => {
      expect(rolesAndFeature.find('.mt-3').children().at(1).props().disabled).toBe(true);
    });
    test('lms role tab disabled after initial data is fetched', () => {
      rolesAndFeature.instance().updateLmsTabAccess();
      expect(rolesAndFeature.find('.mt-3').children().at(1).props().disabled).toBe(false);
    });
  });
});
