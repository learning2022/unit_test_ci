export const listLoreeRoles = {
  data: {
    listLoreeRoles: {
      items: [
        {
          id: '44e04c22-e813-4d2d-9a39-9e2e2d0ebcfb',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          name: 'Role-1',
          description: null,
          featureList: '[]',
          lmsLoreeRoles: {
            nextToken: null,
          },
          createdAt: '2021-12-28T09:29:21.661Z',
          updatedAt: '2021-12-28T09:29:21.661Z',
        },
        {
          id: 'eb81e890-80d9-4539-8dae-b3736136aa6e',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          name: 'Role-52',
          description: null,
          featureList: '[]',
          lmsLoreeRoles: {
            nextToken: null,
          },
          createdAt: '2021-12-28T09:39:46.725Z',
          updatedAt: '2021-12-28T09:39:46.725Z',
        },
        {
          id: '6d0bbcef-9b2a-4de4-b201-4921737bc124',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          name: 'Role-15',
          description: null,
          featureList: '[]',
          lmsLoreeRoles: {
            nextToken: null,
          },
          createdAt: '2021-12-28T09:31:01.060Z',
          updatedAt: '2021-12-28T09:31:01.060Z',
        },
        {
          id: '0b5539cd-1a4e-403b-9df0-1465c7a95378',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          name: 'Role-10',
          description: null,
          featureList: '[]',
          lmsLoreeRoles: {
            nextToken: null,
          },
          createdAt: '2021-12-28T09:30:24.425Z',
          updatedAt: '2021-12-28T09:30:24.425Z',
        },
        {
          id: '4f866cdc-bc14-456e-b990-c7cb7d2a16ee',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          name: 'Role-9',
          description: null,
          featureList: '[]',
          lmsLoreeRoles: {
            nextToken: null,
          },
          createdAt: '2021-12-28T09:30:17.231Z',
          updatedAt: '2021-12-28T09:30:17.231Z',
        },
      ],
      nextToken: null,
    },
  },
};

export const listLoreeFeatures = {
  data: {
    listLoreeFeatures: {
      items: [
        {
          id: '4064e9e9-3aa9-4d34-9eb9-c55e1c57e953',
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          featureList:
            '[{"featureList":[{"id":"outline","label":"Outline"},{"id":"undoredo","label":"Undo/Redo"},{"id":"codeproperties","label":"Code Properties"},{"id":"preview","label":"Preview"},{"id":"accessibilitychecker","label":"Accessibility checker"},{"id":"textblock","label":"Text Block"},{"id":"fontstyles","label":"Font styles"}],"name":"Basic"},{"featureList":[{"id":"duplicaterow","label":"Duplicate row"},{"id":"deleterow","label":"Delete row"},{"id":"changerow","label":"Change row"},{"id":"marginrow","label":"Margin row"},{"id":"paddingrow","label":"Padding row"},{"id":"backgroundcoloroftherow","label":"Background color of the row"},{"id":"rowstructure","label":"Row structure"},{"id":"moverow","label":"Move row"},{"id":"copyandpaste","label":"copy and paste"},{"id":"deletecolumn","label":"Delete column"},{"id":"changecolumn","label":"Change column"},{"id":"margincolumn","label":"Margin - column"},{"id":"paddingcolumn","label":"Padding - column"},{"id":"backgroundcolorofthecolumn","label":"Background color of the column"},{"id":"movecolumn","label":"Move column"},{"id":"duplicateelement","label":"Duplicate - Element"},{"id":"deleteelement","label":"Delete - Element"},{"id":"moveelement","label":"Move - Element"},{"id":"image","label":"Image"},{"id":"banner","label":"Banner"},{"id":"uploadimage","label":"Upload image"},{"id":"insertlink","label":"Insert Link"},{"id":"imagedesign","label":"Image - Design"},{"id":"video","label":"Video"},{"id":"uploadnewvideo","label":"Upload new video"},{"id":"youtube","label":"Youtube"},{"id":"vimeo","label":"Vimeo"},{"id":"insertbyurl","label":"Insert by URL"},{"id":"videodesign","label":"Video - Design"},{"id":"divider","label":"Divider"},{"id":"dividerdesign","label":"Divider - Design"},{"id":"table","label":"Table"},{"id":"tabledesign","label":"Table - Design"},{"id":"specialblocks","label":"Special Blocks"},{"id":"container","label":"Container"},{"id":"navigationMenu","label":"Navigation Menu"}],"name":"Advanced"},{"featureList":[{"id":"customrowfiltersearch","label":"Custom Row (Filter Search)"},{"id":"customelementsfiltersearch","label":"Custom Elements (Filter Search)"},{"id":"templatesfiltersearch","label":"Templates (Filter Search)"},{"id":"saveastemplate","label":"Save as template"},{"id":"saveascustomrow","label":"Save as custom row"},{"id":"saveascustomelement","label":"Save as custom element"}],"name":"Custom Blocks"},{"featureList":[{"id":"myinteractive","label":"My Interactives"}],"name":"Interactives"},{"featureList":[{"id":"h5p","label":"H5P"}],"name":"H5P"},{"featureList":[{"id":"tiptap","label":"Tip-Tap Editor"}],"name":"Tip-Tap"}]',
          createdAt: '2021-05-28T11:20:34.778Z',
          updatedAt: '2021-05-28T11:20:34.778Z',
        },
      ],
      nextToken: null,
    },
  },
};
