import React from 'react';
import { Card, Col, Form, Row } from 'react-bootstrap';
import Loading from '../../../components/loader/loading';
import { API, Auth, graphqlOperation } from 'aws-amplify';
import {
  adminRoles,
  adminSubAccounts,
  d2lAdminRoles,
  fetchInstitutionRoles,
} from '../../../graphql/queries';
import {
  createLmsLoreeRole,
  deleteLmsLoreeRole,
  updateLmsLoreeRole,
} from '../../../graphql/mutations';
import RootAccount from './rootAccount';
import Error from '../../canvas/components/Error';
import {
  AdminRolesQuery,
  AdminSubAccountsQuery,
  D2lAdminRolesQuery,
  FetchInstitutionRolesQuery,
} from '../../../API';
import { RoleListType } from '../../../loree-editor/interface';
import { isCanvas, isD2l, isBB } from '../../../lmsConfig/index';
import { listLMSLoreeRoles, listingLoreeRoles } from './rolesAndFeaturesActions';
interface LmsRoleProps {
  rootAccountModal: boolean;
  closeRootAccountModal: _Any;
  accountId: number;
  accountName: string;
}
class LmsRoles extends React.Component<LmsRoleProps> {
  state = {
    loading: true,
    rolesList: [],
    canvasRoles: [],
    allAccount: [],
    loreeRoles: [],
    error: false,
    selectedSubAccount: 'Root Account',
  };

  async componentDidMount() {
    try {
      if (isCanvas()) {
        const user = await Auth.currentAuthenticatedUser();
        const roles = await API.graphql<AdminRolesQuery>(
          graphqlOperation(adminRoles, {
            loreeUserEmail: user.attributes.email,
            accountId: this.props.accountId,
          }),
        );
        const adminRolesData = JSON.parse(roles?.data?.adminRoles as string);
        if (adminRolesData.statusCode === 200) {
          const isCheck = adminRolesData.body.filter((data: { base_role_type: string }) => {
            return data.base_role_type !== 'AccountMembership';
          });
          this.setState({
            canvasRoles: isCheck,
            selectedSubAccount: this.props.accountName,
          });
        }
      }
      if (isD2l()) {
        const D2lRoles = await API.graphql<D2lAdminRolesQuery>(graphqlOperation(d2lAdminRoles));
        this.setState({
          canvasRoles: JSON.parse(D2lRoles?.data?.d2lAdminRoles as string).body,
        });
      }
      if (isBB()) {
        const BBRoles = await API.graphql<FetchInstitutionRolesQuery>(
          graphqlOperation(fetchInstitutionRoles),
        );
        this.setState({
          canvasRoles: JSON.parse(BBRoles?.data?.fetchInstitutionRoles as string).body.results,
        });
      }
      const lmsRoleRetrieve = await listLMSLoreeRoles();
      const loreeRole = await listingLoreeRoles();
      this.setState({
        rolesList: loreeRole,
        loading: false,
        loreeRoles: lmsRoleRetrieve,
      });
    } catch (err) {
      console.log('error fetching canvasAccounts...', err);
      this.setState({
        loading: false,
        error: true,
      });
    }
  }

  handleSubAccount = async (subAccountId: number, subAccountName: string) => {
    const user = await Auth.currentAuthenticatedUser();
    const subAccounts = await API.graphql<AdminSubAccountsQuery>(
      graphqlOperation(adminSubAccounts, {
        accountId: subAccountId,
      }),
    );
    const roles = await API.graphql<AdminRolesQuery>(
      graphqlOperation(adminRoles, {
        loreeUserEmail: user.attributes.email,
        accountId: subAccountId,
      }),
    );
    if (this.state.allAccount.length > 0) {
      let selectedIndex: number | undefined;
      this.state.allAccount.map((account: _Any, idx) => {
        account.name.filter((data: _Any) => {
          if (data.id === subAccountId) selectedIndex = idx;
          return null;
        });
        return null;
      });
      this.state.allAccount.length = selectedIndex !== undefined ? selectedIndex + 1 : 0;
    }
    let subAccountData = JSON.parse(subAccounts?.data?.adminSubAccounts as string).body;
    subAccountData = subAccountData.sort(
      (blockTitle1: { name: string }, blockTitle2: { name: string }) =>
        blockTitle1.name.toUpperCase() > blockTitle2.name.toUpperCase() ? 1 : -1,
    );
    const isCheckAdmin = JSON.parse(roles?.data?.adminRoles as string).body.filter(
      (data: { base_role_type: string }) => {
        return data.base_role_type !== 'AccountMembership';
      },
    );
    this.setState({
      allAccount: [
        ...this.state.allAccount,
        {
          id: subAccountId,
          name: subAccountData,
        },
      ],
      selectedSubAccount: subAccountName,
      canvasRoles: isCheckAdmin,
    });
  };

  handleStateUpdateLoreeRole = async () => {
    const lmsRoleRetrieve = await listLMSLoreeRoles();
    this.setState({
      loreeRoles: lmsRoleRetrieve,
    });
  };

  handleLoreeRoleChange = async (e: _Any, lmsRole: string, roleType: string, lmsRoleId: number) => {
    e.persist();
    const select = e.target;
    const selectedId = select.children[select.selectedIndex].id;
    const isCheck: _Any = this.state.loreeRoles.filter((data: { lmsRole: string }) => {
      return data.lmsRole === lmsRole;
    });
    if (isCheck.length === 0) {
      const createRoleInput = {
        ltiPlatformID: sessionStorage.getItem('ltiPlatformId'),
        lmsLoreeRoleLoreeRoleIDId: selectedId,
        lmsRole: lmsRole,
        lmsBaseRoleType: roleType,
        lmsRoleId: lmsRoleId,
      };
      await API.graphql(graphqlOperation(createLmsLoreeRole, { input: createRoleInput }));
      await this.handleStateUpdateLoreeRole();
    } else {
      if (selectedId === '1') {
        const deleteInput = {
          id: isCheck[0].id,
        };
        await API.graphql(graphqlOperation(deleteLmsLoreeRole, { input: deleteInput }));
        await this.handleStateUpdateLoreeRole();
      } else {
        const updateRoleInput = {
          id: isCheck[0].id,
          ltiPlatformID: sessionStorage.getItem('ltiPlatformId'),
          lmsLoreeRoleLoreeRoleIDId: selectedId,
          lmsRole: lmsRole,
          lmsBaseRoleType: roleType,
          lmsRoleId: lmsRoleId,
        };
        await API.graphql(graphqlOperation(updateLmsLoreeRole, { input: updateRoleInput }));
        await this.handleStateUpdateLoreeRole();
      }
    }
  };

  handleDomainChange = (roleList: RoleListType) => {
    const roleData = {
      id: 0,
      display_name: '',
      base_role: '',
    };
    const currentDomainName = sessionStorage.getItem('domainName');
    switch (currentDomainName) {
      case 'D2l':
        roleData.display_name = roleList.DisplayName;
        roleData.id = roleList.Identifier;
        roleData.base_role = roleList.DisplayName;
        return roleData;
      case 'BB':
        roleData.display_name = roleList.nameForCourses;
        roleData.id = roleList.roleId;
        roleData.base_role = roleList.nameForCourses;
        return roleData;
      case 'canvas':
        roleData.display_name = roleList.label;
        roleData.id = roleList.id;
        roleData.base_role = roleList.base_role_type;
        return roleData;
      default:
        return roleData;
    }
  };

  handleDefaultValue = (canvasRole: string) => {
    let selectedValue = 'Select role';
    this.state.loreeRoles.filter((loreeRole: _Any) => {
      if (loreeRole.lmsRole === canvasRole && loreeRole.loreeRoleID !== null) {
        selectedValue = loreeRole.loreeRoleID.name;
      }
      return null;
    });
    return selectedValue;
  };

  render() {
    return (
      <>
        {this.state.loading ? (
          <Loading />
        ) : this.state.error ? (
          <Error errorMessage='lmsRoleError' />
        ) : (
          <div className='mt-3'>
            <p>Map specific Loree role, if not find anything suitable create new Loree role.</p>
            <p className='font-weight-bold p-1 text-primary'>{this.state.selectedSubAccount}</p>
            {this.state.canvasRoles.map((roleList: RoleListType, idx: number) => (
              <Card.Header key={idx} className='pb-2 mb-3 rounded'>
                <Row>
                  <Col>
                    <h6 className='font-weight-bold'>
                      {this.handleDomainChange(roleList).display_name !== null
                        ? this.handleDomainChange(roleList).display_name
                        : ''}
                    </h6>
                  </Col>
                  <Col>
                    <Form.Control
                      style={{ width: '200px' }}
                      as='select'
                      defaultValue={this.handleDefaultValue(
                        this.handleDomainChange(roleList).display_name !== null
                          ? this.handleDomainChange(roleList).display_name
                          : '',
                      )}
                      onChange={(e) => {
                        void this.handleLoreeRoleChange(
                          e,
                          this.handleDomainChange(roleList).display_name !== null
                            ? this.handleDomainChange(roleList).display_name
                            : '',
                          this.handleDomainChange(roleList).base_role !== null
                            ? this.handleDomainChange(roleList).base_role
                            : '',
                          this.handleDomainChange(roleList).id !== null
                            ? this.handleDomainChange(roleList).id
                            : 0,
                        );
                      }}
                      className='mb-0'
                    >
                      <option value='Select role' id='1'>
                        Select role
                      </option>
                      {this.state.rolesList.map(
                        (role: { name: string; id: string }, index: number) => (
                          <option key={index} value={role.name} id={role.id}>
                            {role.name}
                          </option>
                        ),
                      )}
                    </Form.Control>
                  </Col>
                </Row>
              </Card.Header>
            ))}
            {this.props.rootAccountModal && isCanvas() && (
              <RootAccount
                rootAccountModal={this.props.rootAccountModal}
                closeRootAccountModal={this.props.closeRootAccountModal}
                allAccount={this.state.allAccount}
                handleSubAccount={this.handleSubAccount}
                selectedAccountName={this.state.selectedSubAccount}
                accountId={this.props.accountId}
                accountName={this.props.accountName}
              />
            )}
          </div>
        )}
      </>
    );
  }
}

export default LmsRoles;
