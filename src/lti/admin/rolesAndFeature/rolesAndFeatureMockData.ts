export const userMockData = {
  email: 'canvas_123670000000000245@example.com',
  name: 'Krishnaveni',
  sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
};

const loreeRoleItems = {
  createdAt: '2021-12-28T09:41:16.799Z',
  description: null,
  featureList: '[]',
  id: '95f990d0-8341-4a63-ac3c-9818bdf7f236',
  ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
  name: 'Role-63',
  updatedAt: '2021-12-28T09:41:16.799Z',
};

export const loreeRoleWithoutTokenMockData = {
  data: {
    listLoreeRoles: {
      items: [loreeRoleItems],
      nextToken: null,
    },
  },
};

export const loreeRoleWithTokenMockData = {
  data: {
    listLoreeRoles: {
      items: [loreeRoleItems],
      nextToken: 'dfheriuwfhjsdkshf',
    },
  },
};

const loreeLmsRoleItems = {
  createdAt: '2021-12-28T09:41:16.799Z',
  id: '90f990d0-8341-4a63-ac3c-9818bdf7f236',
  lmsBaseRoleType: 'DesignerEnrollment',
  lmsRole: 'Course Designer',
  lmsRoleId: '6',
  loreeRoleID: [],
  ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
  updatedAt: '2021-12-28T09:41:16.799Z',
};

export const loreeLmsRoleWithoutTokenMockData = {
  data: {
    listLmsLoreeRoles: {
      items: [loreeLmsRoleItems],
      nextToken: null,
    },
  },
};

export const loreeLmsRoleWithTokenMockData = {
  data: {
    listLmsLoreeRoles: {
      items: [loreeLmsRoleItems],
      nextToken: 'dfheriuwfhjsdkshf',
    },
  },
};

export const adminRolesMock = {
  data: {
    adminRoles:
      '{"statusCode":200,"body":[{"id":1,"role":"AccountAdmin","label":"Account Admin","last_updated_at":"2022-01-06T01:13:01Z"}]}',
  },
};

export const courseDetailsMock = {
  data: {
    courseDetails:
      '{"statusCode":200,"body":{"id":1,"name":"sub-acc","account_id":"1","account":{"id":52,"root_account_id":1,"name":"sub-acc"}}}',
  },
};
