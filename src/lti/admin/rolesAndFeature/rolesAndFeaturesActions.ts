import { API, graphqlOperation } from 'aws-amplify';
import { ListLmsLoreeRolesQuery, ListLoreeRolesQuery } from '../../../API';
import { listLmsLoreeRoles, listLoreeRoles } from '../../../graphql/queries';

const getListLoreeRoleQueryFilter = () => ({
  filter: { ltiPlatformID: { eq: sessionStorage.getItem('ltiPlatformId') } },
});

const listLoreeRoleQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<ListLoreeRolesQuery>(
    graphqlOperation(listLoreeRoles, graphqlOperationData),
  );
};

export const listingLoreeRoles = async () => {
  const loreeRole = [];
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getListLoreeRoleQueryFilter();
  const loreeRoles = await listLoreeRoleQuery(graphqlOperationData);
  loreeRole.push(...(loreeRoles?.data?.listLoreeRoles?.items ?? []));
  let nextToken = loreeRoles?.data?.listLoreeRoles?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const loreeRolesLoop = await listLoreeRoleQuery(graphqlOperationData);
    loreeRole.push(...(loreeRolesLoop?.data?.listLoreeRoles?.items ?? []));
    nextToken = loreeRolesLoop?.data?.listLoreeRoles?.nextToken as string;
  }
  return loreeRole;
};

const listLMSLoreeRoleQuery = async (graphqlOperationData: {}) => {
  return await API.graphql<ListLmsLoreeRolesQuery>(
    graphqlOperation(listLmsLoreeRoles, graphqlOperationData),
  );
};

export const listLMSLoreeRoles = async () => {
  const loreeLMSRole = [];
  const graphqlOperationData: { filter: {}; nextToken?: string | null } =
    getListLoreeRoleQueryFilter();
  const loreeLMSRoles = await listLMSLoreeRoleQuery(graphqlOperationData);
  loreeLMSRole.push(...(loreeLMSRoles?.data?.listLmsLoreeRoles?.items ?? []));
  let nextToken = loreeLMSRoles?.data?.listLmsLoreeRoles?.nextToken as string | null;
  while (nextToken !== null) {
    graphqlOperationData.nextToken = nextToken;
    const loreeLmsRolesLoop = await listLMSLoreeRoleQuery(graphqlOperationData);
    loreeLMSRole.push(...(loreeLmsRolesLoop?.data?.listLmsLoreeRoles?.items ?? []));
    nextToken = loreeLmsRolesLoop?.data?.listLmsLoreeRoles?.nextToken as string;
  }
  return loreeLMSRole;
};
