/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import { Accordion, Button, Card, Form } from 'react-bootstrap';
import Loading from '../../../components/loader/loading';
import { API, graphqlOperation } from 'aws-amplify';
import { listLoreeFeatures } from '../../../graphql/queries';
import { listingLoreeRoles } from './rolesAndFeaturesActions';
import {
  CreateLoreeRoleMutation,
  ListLoreeFeaturesQuery,
  UpdateLoreeRoleMutation,
} from '../../../API';
import { updateLoreeRole, createLoreeRole, deleteLoreeRole } from '../../../graphql/mutations';
import CreateLoreeRole from './createLoreeRole';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleDown,
  faAngleRight,
  faCaretDown,
  faCaretRight,
  faTrash,
} from '@fortawesome/free-solid-svg-icons';
import ToastComponent from '../../components/ToastComponent';
import { ReactComponent as LoaderIcon } from '../../../views/editor/saveIcon/icons/loaderIcon.svg';
import { translate } from '../../../i18n/translate';
interface LoreeRoleProps {
  createLoreeRoleModal: boolean;
  closeCreateLoreeRoleModal: any;
  intialLmsTabAccess: boolean;
  updateLmsTabAccess: () => void;
}
class LoreeRoles extends React.Component<LoreeRoleProps> {
  state = {
    loading: true,
    featureList: [] as any[],
    rolesList: [] as any[],
    roleName: '',
    createState: true,
    showToast: false,
    toastMessage: '',
    activeAccordion: 0,
    activeSubAccordion: '0',
    saveLoader: false,
    roleNameError: '',
    pointerEvents: false,
    subAccordion: '',
  };

  async componentDidMount() {
    try {
      const loreeRole = await listingLoreeRoles();
      const features = await API.graphql<ListLoreeFeaturesQuery>(
        graphqlOperation(listLoreeFeatures, {
          filter: {
            ltiPlatformID: {
              eq: sessionStorage.getItem('ltiPlatformId'),
            },
          },
        }),
      );
      this.setState({
        rolesList: loreeRole,
        loading: false,
        featureList: features?.data?.listLoreeFeatures?.items ?? [],
      });
      this.setState({ subAccordion: `${this.state.rolesList[0].name}` });
      if (!this.props.intialLmsTabAccess) this.props.updateLmsTabAccess();
    } catch (err) {
      console.log('error fetching canvasAccounts...', err);
    }
  }

  handleRoleName = (value: string) => {
    value !== ''
      ? this.setState({ roleName: value, createState: false, roleNameError: '' })
      : this.setState({ roleName: value, createState: true, roleNameError: '' });
  };

  handleCreateNewRole = async () => {
    const roleNames: Array<string> = [];
    for (const loreeRoleName of this.state.rolesList) {
      roleNames.push(loreeRoleName.name);
    }
    if (roleNames.includes(this.state.roleName.trim())) {
      this.setState({ roleNameError: translate('role.rolenametaken') });
    } else {
      this.setState({ roleNameError: '' });
      const roleInput = {
        ltiPlatformID: sessionStorage.getItem('ltiPlatformId'),
        name: this.state.roleName,
        featureList: JSON.stringify([]),
      };
      const createLoreeRoles = await API.graphql<CreateLoreeRoleMutation>(
        graphqlOperation(createLoreeRole, { input: roleInput }),
      );
      this.setState({
        rolesList: [...this.state.rolesList, createLoreeRoles?.data?.createLoreeRole],
        createState: true,
        showToast: true,
        toastMessage: 'Role created successfully',
      });
      this.props.closeCreateLoreeRoleModal();
    }
  };

  handleDelete = async (id: string) => {
    await API.graphql(graphqlOperation(deleteLoreeRole, { input: { id: id } }));
    const updatedRoleList = this.state.rolesList;
    updatedRoleList.map((role: { id: string }, index: number) => {
      if (role.id === id) updatedRoleList.splice(index, 1);
      return null;
    });
    this.setState({ rolesList: updatedRoleList });
  };

  handleSubTypeOnchange = (
    e: { target: { checked: Boolean } },
    index: number,
    role: any,
    category: any,
    featureList: any,
    subTypeIdx: number,
  ) => {
    const inputElements = document.getElementsByClassName(
      `loree-role-checkbox-${index}${subTypeIdx}`,
    ) as any;
    const unCheckedElement: any = [];
    featureList.map((feature: any) => {
      for (const element of inputElements) {
        if (element.childNodes[1].innerText === feature.label) {
          if (e.target.checked) {
            if (!element.childNodes[0].checked) {
              unCheckedElement.push(feature);
              element.childNodes[0].checked = true;
            }
          } else {
            unCheckedElement.push(feature);
            element.childNodes[0].checked = false;
          }
        }
      }
      return null;
    });
    this.handleOnchange(e, index, unCheckedElement, role, category, '', subTypeIdx);
  };

  handleSubTypeChecked = (index: number, featureList: any, category: string): boolean => {
    let isChecked = false;
    const newRoleList: any = [];
    const selectedFeatureList = JSON.parse(this.state.rolesList[index].featureList);
    selectedFeatureList.map((feature: { type: string }) => {
      if (category === feature.type) newRoleList.push(feature);
      return null;
    });
    if (newRoleList.length === featureList.length) isChecked = true;
    return isChecked;
  };

  checkFeatureListChecked = (index: number, subType: string, subTypeIdx: number) => {
    const inputElements = document.getElementsByClassName(
      `loree-role-checkbox-${index}${subTypeIdx}`,
    ) as any;
    const subTypeCheckBox = document.getElementById(
      `loree-role-checkbox-${index}-${subType}`,
    ) as HTMLInputElement;
    const isAllChecked: Array<Boolean> = [];
    for (const element of inputElements) {
      isAllChecked.push(element.childNodes[0].checked);
    }
    if (isAllChecked.includes(false)) subTypeCheckBox.checked = false;
    else subTypeCheckBox.checked = true;
  };

  handleOnchange = async (
    e: any,
    index: number,
    selectedFeature: any,
    type: string,
    subType: string,
    categoryList: any,
    subTypeIdx: number,
  ) => {
    if (categoryList) this.checkFeatureListChecked(index, subType, subTypeIdx);
    const updateRoleList = this.state.rolesList;
    let inputVal = {};
    let inputId = '';
    updateRoleList.map((role) => {
      if (role.name === type) {
        inputId = role.id;
        let updateFeatureList = JSON.parse(role.featureList);
        if (e.target.checked) {
          if (selectedFeature.constructor === Array) {
            for (const featureItem of selectedFeature) {
              const newRole = { id: featureItem.id, label: featureItem.label, type: subType };
              updateFeatureList = [...updateFeatureList, newRole];
            }
          } else {
            const newRole = { id: selectedFeature, type: subType };
            updateFeatureList = [...updateFeatureList, newRole];
          }
        } else {
          if (selectedFeature.constructor === Array) {
            for (const featureItem of selectedFeature) {
              updateFeatureList.map((feature: { id: string }) => {
                if (feature.id === featureItem.id)
                  updateFeatureList.splice(updateFeatureList.indexOf(feature), 1);
                return null;
              });
            }
          } else {
            updateFeatureList.map((feature: { id: string }) => {
              if (feature.id === selectedFeature)
                updateFeatureList.splice(updateFeatureList.indexOf(feature), 1);
              return null;
            });
          }
        }
        inputVal = {
          id: inputId,
          featureList: JSON.stringify(updateFeatureList),
        };
      }
      return null;
    });
    this.setState({ saveLoader: true, pointerEvents: true });
    const updateRoleListResponse = await API.graphql<UpdateLoreeRoleMutation>(
      graphqlOperation(updateLoreeRole, { input: inputVal }),
    );
    updateRoleList.map((roleData: { id: string }, index: number) => {
      if (roleData.id === inputId)
        (this.state.rolesList as any)[index] = updateRoleListResponse?.data?.updateLoreeRole;
      return null;
    });
    this.setState({ saveLoader: false, pointerEvents: false });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  handleActiveAccordion = (name: string, index: number): void => {
    const role = document.getElementsByClassName(`${name} collapse show`);
    const active = this.state.activeAccordion === index ? '' : index;
    const activeSubAccordion = role[0] ? role[0].id : '';
    this.setState({
      activeSubAccordion: activeSubAccordion,
      activeAccordion: active,
      subAccordion: `${name}`,
    });
  };

  handleSubAccordion = (name: string, index: number): void => {
    const active = this.state.activeSubAccordion === `${index}` ? '' : `${index}`;
    this.setState({ activeSubAccordion: active, subAccordion: `${name}` });
  };

  render() {
    return (
      <>
        {this.state.loading ? (
          <Loading />
        ) : (
          <Accordion
            defaultActiveKey='0'
            className='mt-4 loree-role admin-accordion'
            style={{ pointerEvents: this.state.pointerEvents ? 'none' : 'auto' }}
          >
            {this.state.rolesList.map((role: any, rIdx: number) => (
              <Card
                className={`mb-4  ${this.state.activeAccordion === rIdx ? 'active' : ''}`}
                key={rIdx}
              >
                <Accordion.Toggle
                  as={Card.Header}
                  eventKey={`${rIdx}`}
                  id='role-accordion-header'
                  className='border-0'
                  onClick={() => this.handleActiveAccordion(role.name, rIdx)}
                >
                  {role.name}
                  <FontAwesomeIcon
                    icon={this.state.activeAccordion === rIdx ? faCaretDown : faCaretRight}
                    className='float-right'
                  />
                  <FontAwesomeIcon
                    icon={faTrash}
                    className='float-right d-none'
                    onClick={async () => await this.handleDelete(role.id)}
                  />
                </Accordion.Toggle>
                {this.state.featureList.map((featureCategory) => (
                  <Accordion.Collapse eventKey={`${rIdx}`} key={rIdx + 1}>
                    <Card.Body className='pl-3 pr-3 pt-1 pb-0'>
                      <Accordion defaultActiveKey='0' className='border-bottom mb-2'>
                        {(JSON.parse(featureCategory.featureList) as Array<any>).map(
                          (category: any, idx: any) => (
                            <Card className='border-0 mb-0' key={idx}>
                              <Card.Header className='border-top bg-white pt-1 pb-1 border-bottom-0'>
                                <div className='d-inline-block ml-4'>
                                  <Form.Check
                                    type='checkbox'
                                    id={`loree-role-checkbox-${rIdx}-${category.name}`}
                                    className='font-weight-bold'
                                    defaultChecked={this.handleSubTypeChecked(
                                      rIdx,
                                      category.featureList,
                                      category.name,
                                    )}
                                    onChange={(e: any) => {
                                      this.handleSubTypeOnchange(
                                        e,
                                        rIdx,
                                        role.name,
                                        category.name,
                                        category.featureList,
                                        idx,
                                      );
                                    }}
                                  />
                                </div>
                                <div className='d-inline-block'>
                                  <Accordion.Toggle
                                    as={Button}
                                    variant='link'
                                    eventKey={`${idx}`}
                                    className='pt-2 pb-2 mb-1 text-decoration-none'
                                    id='feature-accordion-toggle'
                                    onClick={() => this.handleSubAccordion(role.name, idx)}
                                  >
                                    <div
                                      className='d-inline-block mr-2'
                                      style={{ marginLeft: '-50px' }}
                                    >
                                      <FontAwesomeIcon
                                        icon={
                                          this.state.activeSubAccordion === `${idx}` &&
                                          `${role.name}` === this.state.subAccordion
                                            ? faAngleDown
                                            : faAngleRight
                                        }
                                        className='float-right text-dark'
                                        onClick={() => this.handleSubAccordion(role.name, idx)}
                                      />
                                    </div>
                                    <span className='ml-4'>{category.name}</span>
                                  </Accordion.Toggle>
                                </div>
                              </Card.Header>
                              <Accordion.Collapse
                                eventKey={`${idx}`}
                                id={`${idx}`}
                                className={`${role.name}`}
                              >
                                <Card.Body className='pt-2 pb-2 border-left feature-list-vertical-line'>
                                  <ul style={{ columns: '3' }} className='list-unstyled pl-3 pt-2'>
                                    {category.featureList.map((feature: any, fIdx: number) => (
                                      <li key={fIdx}>
                                        <Form.Check
                                          type='checkbox'
                                          label={feature.label}
                                          className={`loree-role-checkbox-${rIdx}${idx}`}
                                          defaultChecked={
                                            !!JSON.parse(role.featureList).find(
                                              (f: any) => f.id === feature.id,
                                            )
                                          }
                                          onChange={(e: any) => {
                                            this.handleOnchange(
                                              e,
                                              rIdx,
                                              [feature],
                                              role.name,
                                              category.name,
                                              category.featureList,
                                              idx,
                                            );
                                          }}
                                        />
                                      </li>
                                    ))}
                                  </ul>
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          ),
                        )}
                      </Accordion>
                    </Card.Body>
                  </Accordion.Collapse>
                ))}
              </Card>
            ))}
          </Accordion>
        )}
        <CreateLoreeRole
          createLoreeRoleModal={this.props.createLoreeRoleModal}
          closeCreateLoreeRoleModal={this.props.closeCreateLoreeRoleModal}
          roleNameHandler={(value: string) => this.handleRoleName(value)}
          handleCreateLoreeRole={this.handleCreateNewRole}
          createState={this.state.createState}
          roleNameError={this.state.roleNameError}
        />
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
        {this.state.saveLoader && (
          <div className='save-loader save-role-loader position-absolute justify-content-center'>
            <div className='d-flex px-3 py-2 saving-loader-block'>
              <div className='icon rotating'>
                <LoaderIcon />
              </div>
              <div className='title ml-3'>Saving...</div>
            </div>
          </div>
        )}
      </>
    );
  }
}
export default LoreeRoles;
