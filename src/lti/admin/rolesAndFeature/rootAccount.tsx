import { Modal, Row, Col, ListGroup, Button } from 'react-bootstrap';
import Loading from '../../../components/loader/loading';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import React from 'react';

interface RootAccountProps {
  rootAccountModal: boolean;
  closeRootAccountModal: _Any;
  handleSubAccount(id: number, name: string): void;
  allAccount: _Any;
  selectedAccountName: string;
  accountId: number;
  accountName: string;
}

class RootAccount extends React.Component<RootAccountProps> {
  state = {
    loading: false,
  };

  handleSelectedSubAccount = (subAccountId: number, subAccountName: string) => {
    this.setState({ loading: true });
    this.props.handleSubAccount(subAccountId, subAccountName);
    this.setState({ loading: false });
  };

  render() {
    return (
      <>
        <Modal
          show={this.props.rootAccountModal}
          onHide={this.props.closeRootAccountModal}
          size='lg'
          aria-labelledby='contained-modal-title-vcenter'
          centered
          className='border-primary root-account-modal'
          backdrop='static'
        >
          <Modal.Header closeButton>
            <Modal.Title>
              <h5 className='text-primary'>Fetch Roles</h5>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.loading && <Loading />}
            <Row>
              <Col className='border-right'>
                <ListGroup className='subaccount-list'>
                  <ListGroup.Item
                    className={`border-0 p-0 ${
                      this.props.selectedAccountName === this.props.accountName
                        ? 'text-primary'
                        : ''
                    }`}
                    action
                    onClick={async () =>
                      this.handleSelectedSubAccount(this.props.accountId, this.props.accountName)
                    }
                  >
                    {this.props.accountName}{' '}
                    <FontAwesomeIcon icon={faCaretRight} className='float-right mr-2 mt-1' />
                  </ListGroup.Item>
                </ListGroup>
              </Col>
              {this.props.allAccount.map((adminaccount: _Any, adminIdx: number) => (
                <Col className='border-right' key={adminIdx}>
                  {this.state.loading && <Loading />}
                  <ListGroup className='subaccount-list'>
                    {adminaccount.name.length > 0 ? (
                      adminaccount.name.map((subaccount: { id: number; name: string }) => (
                        <ListGroup.Item
                          key={adminIdx}
                          className={`border-0 p-0 ${
                            this.props.selectedAccountName === subaccount.name ? 'text-primary' : ''
                          }`}
                          action
                          onClick={async () =>
                            this.handleSelectedSubAccount(subaccount.id, subaccount.name)
                          }
                        >
                          {subaccount.name}{' '}
                          <FontAwesomeIcon icon={faCaretRight} className='float-right mr-2 mt-1' />
                        </ListGroup.Item>
                      ))
                    ) : (
                      <p>No Sub-Accounts found</p>
                    )}
                  </ListGroup>
                </Col>
              ))}
            </Row>
          </Modal.Body>
          <Modal.Footer className='d-flex justify-content-center'>
            <Button onClick={this.props.closeRootAccountModal}>Fetch roles</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default RootAccount;
