/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Button, Form, Modal } from 'react-bootstrap';

interface CreateLoreeRoleProps {
  createLoreeRoleModal: boolean;
  closeCreateLoreeRoleModal: any;
  roleNameHandler: any;
  handleCreateLoreeRole: any;
  createState: boolean;
  roleNameError: any;
}

class CreateLoreeRoles extends React.Component<CreateLoreeRoleProps> {
  render() {
    return (
      <Modal
        show={this.props.createLoreeRoleModal}
        onHide={this.props.closeCreateLoreeRoleModal}
        backdrop='static'
        keyboard={false}
        centered
        animation={false}
      >
        <Modal.Header closeButton>
          <Modal.Title id='create-new-module'>
            <h5 className='text-primary'>Create Role</h5>
          </Modal.Title>
        </Modal.Header>
        <Form onSubmit={event => event.preventDefault()}>
          <Modal.Body>
            <Form.Group controlId='formPageName'>
              <Form.Label>Role Name</Form.Label>
              <Form.Control
                required
                data-test='moduleName'
                type='text'
                placeholder='Role Name'
                autoComplete='off'
                maxLength={100}
                onChange={(e: any) => this.props.roleNameHandler(e.target.value)}
              />
              {this.props.roleNameError && <small className='text-danger ml-2'>{this.props.roleNameError}</small>}
            </Form.Group>
          </Modal.Body>
          <Modal.Footer className='d-flex justify-content-center'>
            <Button variant='outline-primary' className='mx-1 modal-footer-button ' onClick={this.props.closeCreateLoreeRoleModal}>
              Cancel
            </Button>
            <Button
              variant='primary'
              className='mx-1 modal-footer-create-button'
              onClick={this.props.handleCreateLoreeRole}
              disabled={this.props.createState}
            >
              Create
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    );
  }
}

export default CreateLoreeRoles;
