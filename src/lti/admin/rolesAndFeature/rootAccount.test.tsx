import React from 'react';
import { mount } from 'enzyme';
import RootAccount from './rootAccount';

const accountDetails = [
  {
    id: '1',
    name: [
      {
        default_group_storage_quota_mb: 2000,
        default_storage_quota_mb: 2000,
        default_time_zone: 'Asia/Kolkata',
        default_user_storage_quota_mb: 50,
        id: 106,
        integration_id: null,
        name: 'LOREE-TEST',
        parent_account_id: 1,
        root_account_id: 1,
        sis_account_id: null,
        sis_import_id: null,
        uuid: 'FCy0EPYCpJLN0Q3wBJFXK0bZKZP0vsp5hVkt3Dyu',
        workflow_state: 'active',
      },
      {
        default_group_storage_quota_mb: 2000,
        default_storage_quota_mb: 2000,
        default_time_zone: 'Asia/Kolkata',
        default_user_storage_quota_mb: 50,
        id: 52,
        integration_id: null,
        name: 'Migration Training',
        parent_account_id: 1,
        root_account_id: 1,
        sis_account_id: null,
        sis_import_id: null,
        uuid: 'UvgT2ZQi7eHXNnGAvzstJL4rzXXGYCs13FdnlXhE',
        workflow_state: 'active',
      },
    ],
  },
];

const componentToTest = (
  <RootAccount
    rootAccountModal
    closeRootAccountModal={jest.fn()}
    allAccount={accountDetails}
    handleSubAccount={jest.fn()}
    selectedAccountName='Root Account'
    accountId={0}
    accountName='Root Account'
  />
);

describe('RootAccount', () => {
  const wrapper = mount(componentToTest);

  test('render without crashing', () => {
    mount(componentToTest);
  });

  describe('display popup modal', () => {
    describe('with header', () => {
      test('with the title as Fetch Roles', () => {
        expect(wrapper.find('.modal-title').at(0).text()).toEqual('Fetch Roles');
      });
      test('with close button', () => {
        expect(wrapper.find('.modal-header').children().at(1).text()).toEqual('×Close');
      });
    });

    describe('with body', () => {
      describe('render with Root Account', () => {
        test('with the label', () => {
          expect(wrapper.find('button').at(1).text()).toEqual('Root Account ');
        });

        describe('When the root account button triggers', () => {
          describe('render with sub accounts', () => {
            test('with the label', () => {
              expect(wrapper.find('button').at(2).text()).toEqual('LOREE-TEST ');
            });
            test('with the label Migration Training', () => {
              expect(wrapper.find('button').at(3).text()).toEqual('Migration Training ');
            });
          });
        });
      });
    });

    describe('with footer', () => {
      describe('render with a button', () => {
        test('with label as Fetch roles', () => {
          expect(wrapper.find('.modal-footer').children().at(0).text()).toEqual('Fetch roles');
        });
      });
    });
  });
});
