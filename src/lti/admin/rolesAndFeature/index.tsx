import React from 'react';
import { apm } from '@elastic/apm-rum';
import { Tab, Tabs, Button } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import LoreeRoles from './loreeRole';
import LmsRoles from './lmsRole';
import './rolesandfeature.scss';
import { courseDetails } from '../../../graphql/queries';
import { CourseDetailsQuery } from '../../../API';
import { isCanvas, isD2l, isBB } from '../../../lmsConfig/index';
import { translate } from '../../../i18n/translate';

class RolesAndFeature extends React.Component {
  state = {
    loading: true,
    isLoreeRoleTab: true,
    rootAccountModal: false,
    createLoreeRoleModal: false,
    roleType: { loreeRole: true, lmsRole: false },
    account_id: 0,
    account_name: '',
    lmsTabInitialAccess: false,
  };

  async componentDidMount() {
    if (isCanvas()) {
      const courseDetail = await this.fetchCourseDetails();
      this.setState({
        account_id: courseDetail?.account_id,
        account_name: courseDetail?.account_name,
      });
    }
  }

  async fetchCourseDetails() {
    try {
      const courses = await API.graphql<CourseDetailsQuery>(
        graphqlOperation(courseDetails, {
          courseId: sessionStorage.getItem('course_id'),
        }),
      );
      return {
        account_id: JSON.parse(courses?.data?.courseDetails as string).body.account.id,
        account_name: JSON.parse(courses?.data?.courseDetails as string).body.account.name,
      };
    } catch (error) {
      console.error(error);
      apm.captureError(error as Error);
    }
  }

  handleSelectTab = (key: string | null) => {
    if (key === 'loreeroles') {
      this.setState({ isLoreeRoleTab: true, roleType: { loreeRole: true, lmsRole: false } });
    } else {
      this.setState({ isLoreeRoleTab: false, roleType: { loreeRole: false, lmsRole: true } });
    }
  };

  closeRootAccountModal = async () => {
    this.setState({ rootAccountModal: false });
  };

  closeCreateLoreeRoleModal = async () => {
    this.setState({ createLoreeRoleModal: false });
  };

  updateLmsTabAccess = () => {
    this.setState({ lmsTabInitialAccess: true });
  };

  render() {
    return (
      <>
        <h3 className='text-primary'>{translate('global.rolesfeatures')}</h3>
        <Button
          variant='primary'
          className={this.state.isLoreeRoleTab || isCanvas() ? 'float-right' : 'd-none'}
          onClick={
            this.state.isLoreeRoleTab
              ? () => this.setState({ createLoreeRoleModal: true })
              : () => this.setState({ rootAccountModal: true })
          }
        >
          {this.state.isLoreeRoleTab ? 'Create New role' : this.state.account_name}
        </Button>
        <Tabs
          defaultActiveKey='loreeroles'
          className='mt-3'
          onSelect={(key: string | null) => this.handleSelectTab(key)}
        >
          <Tab eventKey='loreeroles' title='Loree Roles'>
            {this.state.roleType.loreeRole && (
              <LoreeRoles
                createLoreeRoleModal={this.state.createLoreeRoleModal}
                closeCreateLoreeRoleModal={this.closeCreateLoreeRoleModal}
                intialLmsTabAccess={this.state.lmsTabInitialAccess}
                updateLmsTabAccess={this.updateLmsTabAccess}
              />
            )}
          </Tab>
          <Tab
            eventKey='canvasroles'
            title={isD2l() ? 'Brightspace Roles' : isBB() ? 'Blackboard Roles' : 'Canvas Roles'}
            disabled={!this.state.lmsTabInitialAccess}
          >
            {this.state.roleType.lmsRole && (
              <LmsRoles
                rootAccountModal={this.state.rootAccountModal}
                closeRootAccountModal={this.closeRootAccountModal}
                accountId={this.state.account_id}
                accountName={this.state.account_name}
              />
            )}
          </Tab>
        </Tabs>
      </>
    );
  }
}

export default RolesAndFeature;
