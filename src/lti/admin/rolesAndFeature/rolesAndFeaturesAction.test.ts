import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from '../../../aws-exports';
import { StorageMock } from '../../../utils/storageMock';
import {
  loreeLmsRoleWithoutTokenMockData,
  loreeLmsRoleWithTokenMockData,
  loreeRoleWithoutTokenMockData,
  loreeRoleWithTokenMockData,
  userMockData,
} from './rolesAndFeatureMockData';
import * as rolesAndFeaturesActions from './rolesAndFeaturesActions';

describe('#fetching loree roles and lms roles', () => {
  beforeEach(() => {
    Amplify.configure(awsconfig);
    global.sessionStorage = new StorageMock() as _Any;
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    const user = {
      attributes: userMockData,
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
  });

  test('retrieving loree roles without next token', async () => {
    const listLoreeRolesResponse = loreeRoleWithoutTokenMockData;
    API.graphql = jest.fn().mockImplementation(() => listLoreeRolesResponse);
    expect((await rolesAndFeaturesActions.listingLoreeRoles()).length).toEqual(1);
  });

  test('retrieving loree roles', async () => {
    const listLoreeRolesResponseOne = loreeRoleWithTokenMockData;
    const listLoreeRolesResponse = loreeRoleWithoutTokenMockData;
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => listLoreeRolesResponseOne)
      .mockImplementationOnce(() => listLoreeRolesResponse);
    expect((await rolesAndFeaturesActions.listingLoreeRoles()).length).toEqual(2);
  });

  test('retrieving loree lms roles without next token', async () => {
    const listLoreeLmsRolesResponse = loreeLmsRoleWithoutTokenMockData;
    API.graphql = jest.fn().mockImplementation(() => listLoreeLmsRolesResponse);
    expect((await rolesAndFeaturesActions.listLMSLoreeRoles()).length).toEqual(1);
  });

  test('retrieving lms loree roles with next token', async () => {
    const listLoreeLmsRolesResponse = loreeLmsRoleWithTokenMockData;
    const listLoreeLmsRolesResponseNext = loreeLmsRoleWithoutTokenMockData;
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => listLoreeLmsRolesResponse)
      .mockImplementationOnce(() => listLoreeLmsRolesResponseNext);
    expect((await rolesAndFeaturesActions.listLMSLoreeRoles()).length).toEqual(2);
  });
});
