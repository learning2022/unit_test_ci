/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import CreateLoreeRole from './createLoreeRole';

describe('CreateLoreeRole', () => {
  const componentToTest = (
    <CreateLoreeRole
      createLoreeRoleModal={true}
      closeCreateLoreeRoleModal={jest.fn()}
      roleNameHandler={(value: any) => jest.fn()}
      handleCreateLoreeRole={jest.fn()}
      createState={true}
      roleNameError=''
    />
  );

  const wrapper = mount(componentToTest);

  test('render without crashing', () => {
    mount(componentToTest);
  });

  describe('display the popup modal', () => {
    describe('with a header', () => {
      test('with title as Create Role', () => {
        expect(wrapper.find('.modal-title').at(0).text()).toEqual('Create Role');
      });
      test('with close Button', () => {
        expect(wrapper.find('.modal-header').children().at(1).text()).toEqual('×Close');
      });

      describe('When onclick button is triggered', () => {
        const closeCreateLoreeRoleModal = jest.fn();
        test('it should close the modal', () => {
          wrapper.find('.modal-header').children().at(1).simulate('click', closeCreateLoreeRoleModal);
          expect(closeCreateLoreeRoleModal.mock.calls.length).toBeCalledWith;
        });
      });
    });

    describe('with a body', () => {
      test('with a field label as Role Name', () => {
        expect(wrapper.find('.modal-body').children().at(0).text()).toEqual('Role Name');
      });

      describe('it should have a form ', () => {
        test('', () => {
          const onSubmitFn = jest.fn();
          wrapper.find('form').simulate('submit', onSubmitFn);
          expect(onSubmitFn).toHaveBeenCalledTimes(0);
        });
        describe('with a input field', () => {
          const input = wrapper.find('input');
          test('with type as text', () => {
            expect(input.prop('type')).toEqual('text');
          });
          test('with placeholder', () => {
            expect(input.prop('placeholder')).toEqual('Role Name');
          });
          describe('When entering the text, onchange function triggers', () => {
            // Testing the onchange functionlaity of input field
            const title = wrapper.find('input').at(0);
            title.simulate('change');
          });
        });
      });
    });

    describe('with a footer', () => {
      const closeCreateLoreeRoleModal = jest.fn(),
        handleCreateLoreeRole = jest.fn();

      // Testing the cancel button & it's functionality.
      describe('with cancel button', () => {
        test('with the label', () => {
          expect(wrapper.find('.modal-footer').children().at(0).text()).toEqual('Cancel');
        });
        test('when the cancel button is triggered', () => {
          wrapper.find('.modal-footer').children().at(0).simulate('click', closeCreateLoreeRoleModal);
          expect(handleCreateLoreeRole.mock.calls.length).toBeCalledWith;
        });
      });

      // Testing the create button & it's functionality
      describe('with create button', () => {
        test('with the label', () => {
          expect(wrapper.find('.modal-footer').children().at(1).text()).toEqual('Create');
        });
        test('when the create button is triggered', () => {
          wrapper.find('.modal-footer').children().at(1).simulate('click', handleCreateLoreeRole);
          expect(handleCreateLoreeRole.mock.calls.length).toBeCalledWith;
        });
      });
    });
  });
  describe('When role name already exists', () => {
    const componentToTest = (
      <CreateLoreeRole
        createLoreeRoleModal={true}
        closeCreateLoreeRoleModal={jest.fn()}
        roleNameHandler={(value: any) => jest.fn()}
        handleCreateLoreeRole={jest.fn()}
        createState={true}
        roleNameError='Role name already exist'
      />
    );
    const wrapper = mount(componentToTest);

    test('render with the error text', () => {
      expect(wrapper.find('small').text()).toEqual('Role name already exist');
    });
  });
});
