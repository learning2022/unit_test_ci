import React from 'react';
import API from '@aws-amplify/api';
import { mount, ReactWrapper } from 'enzyme';
import Dashboard from './index';
import { act } from 'react-dom/test-utils';
import {
  mockUserData,
  interactiveData,
  loreeFeaturesData,
  loreeTemplatesData,
  customBlocksData,
} from './mockData';

const waitForComponentToUpdate = async (wrapper: ReactWrapper) => {
  await act(async () => {
    await new Promise((resolve) => setTimeout(resolve, 0));
    wrapper.update();
  });
};

describe('analytics dashboard charts', () => {
  let analyticsDb: ReactWrapper;
  let chartSubWrapper: ReactWrapper;
  beforeEach(async () => {
    API.graphql = jest
      .fn()
      .mockReturnValueOnce(mockUserData)
      .mockReturnValueOnce(interactiveData)
      .mockReturnValueOnce(loreeFeaturesData)
      .mockReturnValueOnce(loreeTemplatesData)
      .mockReturnValueOnce(customBlocksData);
    analyticsDb = mount(<Dashboard />);
    await waitForComponentToUpdate(analyticsDb);
    chartSubWrapper = analyticsDb
      .find('.analytics-chart-block')
      .at(0)
      .children()
      .at(0)
      .children()
      .at(0)
      .children();
  });

  test('Verifying chart section is rendered or not', () => {
    expect(analyticsDb.find('.analytics-chart-block')).not.toBeNull();
  });
  test('Verifying chart parent div has all child div or not', () => {
    expect(analyticsDb.find('.analytics-chart-block')).toHaveLength(1);
    expect(
      analyticsDb.find('.analytics-chart-block').at(0).children().at(0).prop('test-id'),
    ).toEqual('charts-sub-wrapper');
  });
  test('Verifying whether session by country and Templates blocks has their children components or not', () => {
    expect(chartSubWrapper.at(0).children().at(0).prop('test-id')).toEqual('by-country');
    expect(chartSubWrapper.at(1).children().at(0).prop('test-id')).toEqual('by-templates');
  });
  test('Verifying whether session by country has its children components or not', () => {
    expect(chartSubWrapper.find('CardTitle').at(0).text()).toEqual('Users Details');
    expect(chartSubWrapper.find('.by-country-chart-render').at(0).children()).not.toBeNull();
  });
  test('Verifying whether session by Templates has its children components or not', () => {
    expect(chartSubWrapper.find('CardTitle').at(1).text()).toEqual('Build Details');
    expect(chartSubWrapper.find('.by-template-chart-render').at(1).children()).not.toBeNull();
  });
  test('total users count based on mock data', () => {
    expect(analyticsDb.find('CardText').at(1).text()).toEqual('Users');
    expect(analyticsDb.find('CardTitle').at(0).text()).toEqual('13');
  });
  test('total templates count based on mock data', () => {
    expect(analyticsDb.find('CardText').at(3).text()).toEqual('Templates');
    expect(analyticsDb.find('CardTitle').at(1).text()).toEqual('99');
  });
  test('total blocks count based on mock data', async () => {
    expect(analyticsDb.find('CardText').at(5).text()).toEqual('Rows + Elements');
    expect(analyticsDb.find('CardTitle').at(2).text()).toEqual('114');
  });
});
