export const mockUserData = {
  data: {
    dashboardStatistics:
      '{"statusCode":200,"body":{"UserData":{"Items":[{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000027@example.com","userInfo":{"name":"Edina Cejvan","global_id":"123670000000000027","id":27,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-02-15T06:39:15.772Z","loreePassword":"ySTcA1nX5SDFNPiLse235&#","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":27,"isAdmin":true,"generatedAt":"2021-02-15T08:01:42.909Z","expiresAt":3600,"updatedAt":"2021-02-15T06:39:15.772Z","refreshToken":"12367~Gw8W0Kjba3FMpijpzbYkuFeckvDmuq6nPcwhAVS55fBeKcDn3xDCG8GtCedxRxVJ","id":"1065cd59-8d1b-4ed0-8610-6120d5860e98","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"edina@crystaldelta.com","accessToken":"12367~THpH0bGScOim0h2u74OMnYca7N2Td0A3dwVxv0JKIw4NZteQwKFGYyQyS7bBi9xs"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000242@example.com","userInfo":{"name":"Devanand Ravi","global_id":"123670000000000242","id":242,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2020-12-03T05:06:17.631Z","loreePassword":"bReU1dZXA6&ypCuzjZjQyN996&!","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":242,"isAdmin":true,"generatedAt":"2021-05-20T04:36:21.676Z","expiresAt":3600,"updatedAt":"2020-12-03T05:06:17.631Z","refreshToken":"12367~SWLGmp8KFvi2b3QUnDGMPBG3DvuGsnt6HmjSTx4DfYzyUsr9ibZoKvUf3BD0HEIO","id":"9779111b-0101-441f-b5fa-c52369fa9a82","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"devanand.ravi@crystaldelta.com","accessToken":"12367~AYwTZ4nfSP4GNYbpr2mk2Ccs7Vrf56xkAzZdm6p28TZ9HUY5MVCUJlYruZ8Q0ZCM"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000897@example.com","userInfo":{"name":"Admin","global_id":"123670000000000897","id":897,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-02-10T12:18:09.689Z","loreePassword":"mW!CEhMdqYDEePzC429#%","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":897,"isAdmin":true,"generatedAt":"2021-03-04T12:33:48.502Z","expiresAt":3600,"updatedAt":"2021-02-10T12:18:09.689Z","refreshToken":"12367~PjPc2CxUl1DSJzrb26KITrLloqSILGsnqNiqzH5ro5wqAfXsyJ5h8jFY0sR9610h","id":"476552c7-41c4-40a6-93af-78e631451acc","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"admin@loree.io","accessToken":"12367~AhKpXMKHHwbPiIhT4GlZtAHuKnF2N4fR20RKHxuFt2Gak5iIC01RJB4CXerskJ8s"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000245@example.com","userInfo":{"name":"Krishnaveni","global_id":"123670000000000245","id":245,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-03-04T06:42:01.320Z","loreePassword":"UWZotXqkearrVoInvrDliT198!@","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":245,"isAdmin":true,"generatedAt":"2021-05-19T07:29:37.050Z","expiresAt":3600,"updatedAt":"2021-03-04T06:42:01.320Z","refreshToken":"12367~0xWCxfBSaJrmnswZUGOMd97PRa1WRHA11CzHotZEDdjkl0K0QcjPC1eYhsBumBAl","id":"584a6e1d-3dea-4d82-8918-761b0e19bd97","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"krishnaveni.vela@crystaldelta.com","accessToken":"12367~Dx7GhnE5AUXx6lCxw5q5OAvQuwCY6bqw15JQcyEeZRTCp7AqnWmHZ77XbsRdmm3j"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000244@example.com","userInfo":{"name":"Keerthanashri Raj","global_id":"123670000000000244","id":244,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-02-24T04:26:00.602Z","loreePassword":"Ai5sBLUaJeaxcZXAgo211!@","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":244,"isAdmin":true,"generatedAt":"2021-05-20T04:16:47.615Z","expiresAt":3600,"updatedAt":"2021-02-24T04:26:00.602Z","refreshToken":"12367~aLmHUS4SUC9Ws65c5ok84g9OEUZskA2HGChKJSZt8zoW1zSx5nsWN8rUfCPaGq8t","id":"2c2038a0-0bb6-474a-a9d0-825541bc90a5","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"keerthanashri.raj@crystaldelta.com","accessToken":"12367~OCs6wpIitYpRwnUJ4me93EPfL2Id5Bd7ev3JeG81ypM3ngxlrjxOd0aClyOXptpZ"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000955@example.com","userInfo":{"name":"Aishwarya Prabha","global_id":"123670000000000955","id":955,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-05-18T07:29:31.208Z","loreePassword":"WgdSd785QOSAKUpqLPnu363@&","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":955,"isAdmin":true,"generatedAt":"2021-05-18T11:09:05.916Z","expiresAt":3600,"updatedAt":"2021-05-18T07:29:31.208Z","refreshToken":"12367~DORWpTXAJRRR6w9tx9meNyWhGB7qwNmm6w3W8i0q06hC7qLd1ohLEeFs01TkcxOL","id":"59128b93-570f-493c-9316-18dd61c9578e","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"aishwaryap@crystaldelta.com","accessToken":"12367~snmz0ynLC3tJCOU6m68jY30noxo0jxjF1m6CRgVoSPN3TJZ6aSkZBAq8PzSqB8Qd"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000782@example.com","userInfo":{"name":"Hanuman Choudhary","global_id":"123670000000000782","id":782,"effective_locale":"en"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2020-12-08T06:51:33.614Z","loreePassword":"kb#X6wRf6j0ynzolyJCDFE963%&","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":782,"isAdmin":false,"generatedAt":"2020-12-08T06:51:33.614Z","expiresAt":3600,"updatedAt":"2020-12-08T06:51:33.614Z","refreshToken":"12367~Jwf64Au7j4E1HTM9sUy3YVHTrIfhiYLKEWywKigGDy9G0HkZjvaO1A6UYQzzquvk","id":"ad116bd2-9b82-449b-8e9f-edffadc443d4","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"hanuman.choudhary@crystaldelta.com","accessToken":"12367~IzzTvE0SO28MMGyXvtlAHs10mP0Mn2KXeooSFrA7NQ1ILNPM5KPCvpHeanyPdwjt"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000004@example.com","userInfo":{"name":"Nick Gray","global_id":"123670000000000004","id":4,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-04-29T10:26:37.993Z","loreePassword":"xlns!qg6tFEQDnPDRK174&%","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":4,"isAdmin":false,"generatedAt":"2021-05-06T10:33:52.220Z","expiresAt":3600,"updatedAt":"2021-04-29T10:26:37.993Z","refreshToken":"12367~pZlWKzF7vzx9UhJ1T8EaakJKiajTXDu2LcO3qbhekNwiyo1bYuFFhMSKJTl3XtTm","id":"93cc7e08-5dc1-42d7-a494-3e85984b8a84","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"nick@crystaldelta.com","accessToken":"12367~wgFyeo9g6cETCPw65P0JBswYr1HJOl5DsdDuHMpFiOAOZL2OW3OS24dPc5Da2S3n"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000001180@example.com","userInfo":{"name":"Priyanga","global_id":"123670000000001180","id":1180,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-04-09T07:27:39.016Z","loreePassword":"jAFaA7G0SgINfopjkAPDlp640%#","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":1180,"isAdmin":true,"generatedAt":"2021-05-20T03:52:13.910Z","expiresAt":3600,"updatedAt":"2021-04-09T07:27:39.016Z","refreshToken":"12367~V8PO8Jy6vNhiZx7U71SNl1ppxp44mCGm48T2V7xdl1B2DPd4ENvawPEvT2NtGlyv","id":"9936dc92-4dfd-4bee-a3cb-e6528177ab95","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"priyanga.alagar@crystaldelta.com","accessToken":"12367~b2Vq0oG65OWOZJgAiqE3TY2sIE86j0LCb9NRIpaWwuBWISfDzK4ei3SNjHIxgXCJ"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000900@example.com","userInfo":{"name":"Student","global_id":"123670000000000900","id":900,"effective_locale":"en"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-02-11T08:17:12.999Z","loreePassword":"VH2YuyhJ#dTqpiuwVHML203&!","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":900,"isAdmin":false,"generatedAt":"2021-02-11T08:17:12.999Z","expiresAt":3600,"updatedAt":"2021-02-11T08:17:12.999Z","refreshToken":"12367~LbwCVM6jee22QRpM3R0L18M3s7SoOdgVulgNM7NCKDb1QBV39JmtpytW26sjs4Qi","id":"aa4ff927-b519-4ab4-ac74-b5933ff7b277","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"student@loree.io","accessToken":"12367~pF3m9Ca9njXpUlp6Uw6RBqDcpuOjZzmzSpHBOpoazidSXhrGLPQIxjkp4ULzScW9"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000001182@example.com","userInfo":{"name":"Gunaseelan","global_id":"123670000000001182","id":1182,"effective_locale":"en-GB"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-02-24T05:48:25.947Z","loreePassword":"K2NjgQezXijfgelpiX332%#","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":1182,"isAdmin":true,"generatedAt":"2021-04-21T15:10:49.261Z","expiresAt":3600,"updatedAt":"2021-02-24T05:48:25.947Z","refreshToken":"12367~3VRW12BWQhdnnDmUtiL81vTOu19ZibLE5Q94mn9NMMAAqMHwDZTQvGEhwvGID1MV","id":"a6e12753-f0c3-4d35-a2c3-900915b83407","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"gunaseelan.p@crystaldelta.com","accessToken":"12367~CLC8S0KQW7yxH4wDw0GkWD8YSU3q6RpOeEJGV7FeVJQ6f1ZnODSQGfrZbgdilSXe"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000001324@example.com","userInfo":{"name":"Arun Shankar","global_id":"123670000000001324","id":1324,"effective_locale":"en"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","createdAt":"2021-04-12T11:05:57.770Z","loreePassword":"eJ6N32S@OdyOByVx506%&","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":1324,"isAdmin":true,"generatedAt":"2021-05-20T04:07:00.704Z","expiresAt":3600,"updatedAt":"2021-04-12T11:05:57.770Z","refreshToken":"12367~ug1ZS0wm7i1SdROm8TaeUYavIgIzaYhD6tSYOIh7OcDkrAQQHiT5AMOvt1YZlhz8","id":"e7ed0ca4-dc98-4b48-9497-4d82ee482834","lmsApiUrl":"https://crystaldelta.instructure.com","lms_email":"arunshankar.maheshwaran@crystaldelta.com","accessToken":"12367~D0lg32z0TbUb3k7rQxawY1WJCrrKvirthopG4VDKE02BZnN4ytg8YOXtTFMJpK3C"},{"ltiClientID":"123670000000000300","loreeUsername":"canvas_123670000000000162@example.com","userInfo":{"name":"Andiswamy","global_id":"123670000000000162","id":162,"effective_locale":"en"},"ltiPlatformID":"d66b1309-ac2b-48e5-b96f-c0098e8baf0a","loreePassword":"cF%1h87tApekSUfjidLE917#!","createdAt":"2021-02-15T06:28:03.634Z","ltiApiKeyID":"a028ccb1-e1be-4019-95b0-445e99f3738b","user":162,"isAdmin":true,"generatedAt":"2021-02-15T06:28:03.634Z","expiresAt":3600,"updatedAt":"2021-02-15T06:28:03.634Z","refreshToken":"12367~1g2liKcRk9NpDhoU33haLpXM0S3XoLHnHWHhfyrQY75ujs7tuu7ymuKccYaq6FdW","id":"908d9f76-716f-48ef-9bbe-7850304d20c6","lms_email":"andiswamy.raja@crystaldelta.com","lmsApiUrl":"https://crystaldelta.instructure.com","accessToken":"12367~xJN7s78WNuObq3ry17mvaqD7QMgPDob8RMbl1KQylCH5gRBXhmmyPMSnQlWewu9v"}],"Count":13,"ScannedCount":13},"UserCount":13,"TemplatesCount":99,"BlocksCount":114}}',
  },
};

export const interactiveData = {
  data: {
    AdminDashboardInteractive: '{"statusCode":200,"body":{"InteractiveData":1268,"H5pData":75}}',
  },
};

export const loreeFeaturesData = {
  data: {
    listLoreeFeatures: {
      items: [
        {
          featureList:
            '[{"featureList":[{"id":"myinteractive","label":"My Interactives"}],"name":"Interactives"},{"featureList":[{"id":"h5p","label":"H5p"},{"id":"editorganisationh5p","label":"Can Edit"},{"id":"deleteorganisationh5p","label":"Can Delete"}],"name":"H5p"}]',
        },
      ],
    },
  },
};

export const loreeTemplatesData = {
  data: {
    listCustomTemplates: {
      items: [
        {
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          createdAt: '2021-02-03T12:03:01.766Z',
          createdBy: 'Krishnaveni',
          active: true,
          isGlobal: false,
          categoryID: '458e9135-2c73-4eb9-82c8-fbf0d4e19b18',
          updatedAt: '2021-07-30T07:13:25.668Z',
          owner: 'e9e7bb6b-8171-4dfd-b563-b88fc520f9d2',
          thumbnail: {
            bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox',
            region: 'ap-southeast-2',
            key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/template/test-8',
          },
          id: '9d7b55d5-40fc-4607-8c96-5fb43c755837',
          title: 'test-8',
        },
      ],
      nextToken: null,
    },
  },
};

export const customBlocksData = {
  data: {
    listCustomBlocks: {
      items: [
        {
          content: '',
          status: true,
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          createdAt: '2021-03-15T08:10:09.050Z',
          createdBy: 'Aishwarya Prabha',
          active: true,
          isGlobal: false,
          categoryID: '44999503-9fe2-4ce2-b9f6-ac4ac1ae715f',
          updatedAt: '2021-03-15T08:10:09.050Z',
          owner: '20a4e682-fe99-4fa6-8748-880697e553fc',
          thumbnail: {
            bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox',
            region: 'ap-southeast-2',
            key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/blocks/Smiley-20',
          },
          id: '4f93017d-96b5-4405-a3bb-555984843f33',
          title: 'Smiley-20',
          type: 'Row',
        },
        {
          content: '',
          status: true,
          ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
          createdAt: '2021-03-15T08:10:09.050Z',
          createdBy: 'Aishwarya Prabha',
          active: true,
          isGlobal: false,
          categoryID: '44999503-9fe2-4ce2-b9f6-ac4ac1ae715f',
          updatedAt: '2021-03-15T08:10:09.050Z',
          owner: '20a4e682-fe99-4fa6-8748-880697e553fc',
          thumbnail: {
            bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e114211-andibox',
            region: 'ap-southeast-2',
            key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/blocks/Smiley-20',
          },
          id: '4f93017d-96b5-4405-a3bb-555984843f33',
          title: 'Smiley-20',
          type: 'Elements',
        },
      ],
      nextToken: null,
    },
  },
};

export const userData = [
  [
    {
      templates: [
        {
          createdAt: '2021-08-13:14:30:01.766Z',
          title: 'test-8',
        },
        {
          createdAt: '2021-08-12T15:53:25.390Z',
          title: 'worstt000002',
        },
        {
          createdAt: '2021-07-22T12:03:01.766Z',
          title: 'test-8',
        },
        {
          createdAt: '2021-08-07T12:03:01.766Z',
          title: 'test-9',
        },
        {
          createdAt: '2021-08-12T02:53:01.766Z',
          title: 'test-9',
        },
        {
          createdAt: '2021-08-12T18:30:00.808Z',
          title: 'test-9',
        },
        {
          createdAt: '2021-08-11T18:30:00.766Z',
          title: 'test-9',
        },
      ],
    },
  ],
  [
    {
      row: [
        {
          createdAt: '2021-08-12T08:10:09.050Z',
          title: 'Smiley-20',
          type: 'Row',
        },
      ],
    },
    {
      row: [
        {
          createdAt: '2021-08-13T07:59:29.990Z',
          title: 'Smiley-8',
          type: 'Row',
        },
      ],
    },
    {
      row: [
        {
          createdAt: '2021-07-20T08:51:05.495Z',
          title: 'Smiley-92',
          type: 'Row',
        },
      ],
    },
    {
      row: [
        {
          createdAt: '2021-08-02T08:51:05.495Z',
          title: 'Smiley-93',
          type: 'Row',
        },
      ],
    },
    {
      row: [
        {
          createdAt: '2021-08-12T02:51:05.495Z',
          title: 'Smiley-94',
          type: 'Row',
        },
      ],
    },
    {
      row: [
        {
          createdAt: '2021-08-12T18:30:00.495Z',
          title: 'Smiley-93',
          type: 'Row',
        },
      ],
    },
    {
      row: [
        {
          createdAt: '2021-08-11T18:30:00.495Z',
          title: 'Smiley-96',
          type: 'Row',
        },
      ],
    },
  ],
  [
    {
      element: [
        {
          createdAt: '2021-07-23T04:29:13.922Z',
          title: 'new element demo',
          type: 'Elements',
        },
      ],
    },
    {
      element: [
        {
          createdAt: '2021-08-12T11:05:46.808Z',
          title: 'new column',
          type: 'Elements',
        },
      ],
    },
    {
      element: [
        {
          createdAt: '2021-07-27T11:05:46.808Z',
          title: 'new element',
          type: 'Elements',
        },
      ],
    },
    {
      element: [
        {
          createdAt: '2021-08-12T02:05:46.808Z',
          title: 'new element 1',
          type: 'Elements',
        },
      ],
    },
    {
      element: [
        {
          createdAt: '2021-08-12T18:30:00.808Z',
          title: 'new element 2',
          type: 'Elements',
        },
      ],
    },
    {
      element: [
        {
          createdAt: '2021-08-13T14:30:00.808Z',
          title: 'new element 3',
          type: 'Elements',
        },
      ],
    },
    {
      element: [
        {
          createdAt: '2021-08-11T18:30:00.808Z',
          title: 'new element 4',
          type: 'Elements',
        },
      ],
    },
  ],
];

export const filteredData = [
  [
    { Elements: 1, Rows: 1, Templates: 1, name: '00:00AM' },
    { Elements: 1, Rows: 1, Templates: 0, name: '12:00PM' },
    { Elements: 1, Rows: 1, Templates: 1, name: '11:49PM' },
  ],
  [
    { Elements: 1, Rows: 1, Templates: 1, name: '00:00AM' },
    { Elements: 3, Rows: 2, Templates: 2, name: '12:00PM' },
    { Elements: 0, Rows: 1, Templates: 1, name: '11:49PM' },
  ],
  [
    { Elements: 0, Rows: 0, Templates: 0, name: 'Fri' },
    { Elements: 0, Rows: 0, Templates: 1, name: 'Sat' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Sun' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Mon' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Tue' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Wed' },
    { Elements: 3, Rows: 3, Templates: 3, name: 'Thu' },
  ],
  [
    { Elements: 0, Rows: 0, Templates: 0, name: '07/13' },
    { Elements: 0, Rows: 0, Templates: 0, name: '07/18' },
    { Elements: 1, Rows: 1, Templates: 1, name: '07/23' },
    { Elements: 1, Rows: 0, Templates: 0, name: '07/28' },
    { Elements: 0, Rows: 1, Templates: 0, name: '08/02' },
    { Elements: 0, Rows: 0, Templates: 1, name: '08/07' },
    { Elements: 3, Rows: 3, Templates: 3, name: '08/12' },
  ],
  [
    { Elements: 0, Rows: 0, Templates: 0, name: 'May' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'June' },
    { Elements: 2, Rows: 1, Templates: 1, name: 'July' },
  ],
  [
    { Elements: 0, Rows: 0, Templates: 0, name: 'February' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'March' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'April' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'May' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'June' },
    { Elements: 2, Rows: 1, Templates: 1, name: 'July' },
  ],
  [
    { Elements: 5, Rows: 6, Templates: 6, name: 'Aug' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Sep' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Oct' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Nov' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Dec' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Jan' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Feb' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Mar' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Apr' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'May' },
    { Elements: 0, Rows: 0, Templates: 0, name: 'Jun' },
    { Elements: 2, Rows: 1, Templates: 1, name: 'Jul' },
  ],
];
