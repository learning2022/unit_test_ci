/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file

import React from 'react';
import { Container, Card, Row, Dropdown } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import {
  adminDashboardInteractive,
  dashboardStatistics,
  listLoreeFeatures,
} from '../../../graphql/queries';
import Loader from '../../../components/loader/loading';
import { ReactComponent as ExportIcon } from '../../../assets/Icons/export.svg';
import NewUserChart from './analyticsCharts/newUser';
import NewUsageChart from './analyticsCharts/usageCharts';
import { handlingFilterData } from './analyticsCharts/filterUserDetail';
import './dashboard.scss';
import {
  DashboardStatisticsQuery,
  AdminDashboardInteractiveQuery,
  ListLoreeFeaturesQuery,
} from '../../../API';
interface DashboardState {
  newUser?: string[] | undefined;
  userData?: { createdAt: string; id: string }[] | undefined;
  usersCount?: number | undefined;
  customTemplateCount?: number;
  customBlockCount?: number;
  interactivesCount?: number;
  h5pCount?: number;
  filterDuration?: string | undefined;
  loading?: boolean;
  featureList?: [];
}

class Dashboard extends React.Component<{}, DashboardState> {
  state: Readonly<DashboardState> = {
    newUser: [],
    userData: [],
    usersCount: 0,
    customTemplateCount: 0,
    customBlockCount: 0,
    interactivesCount: 0,
    h5pCount: 0,
    filterDuration: 'today',
    loading: true,
    featureList: [],
  };

  componentDidMount() {
    try {
      const organization = sessionStorage.getItem('lmsUrl')?.replace(/^https?:\/\//i, '');
      void Promise.all([
        API.graphql<DashboardStatisticsQuery>(graphqlOperation(dashboardStatistics, {})),
        API.graphql<AdminDashboardInteractiveQuery>(
          graphqlOperation(adminDashboardInteractive, {
            organization: organization,
            email: sessionStorage.getItem('lmsEmail'),
            platformId: sessionStorage.getItem('ltiPlatformId'),
          }),
        ),
        API.graphql<ListLoreeFeaturesQuery>(
          graphqlOperation(listLoreeFeatures, {
            filter: {
              ltiPlatformID: {
                eq: sessionStorage.getItem('ltiPlatformId'),
              },
            },
          }),
        ),
      ]).then((response) => {
        this.setState({
          userData: JSON.parse(response[0].data?.dashboardStatistics as string).body.UserData.Items,
          usersCount: JSON.parse(response[0].data?.dashboardStatistics as string).body.UserCount,
          customTemplateCount: JSON.parse(response[0].data?.dashboardStatistics as string).body
            .TemplatesCount,
          customBlockCount: JSON.parse(response[0].data?.dashboardStatistics as string).body
            .BlocksCount,
          interactivesCount: JSON.parse(response[1].data?.AdminDashboardInteractive as string).body
            .InteractiveData,
          h5pCount: JSON.parse(response[1].data?.AdminDashboardInteractive as string).body.H5pData,
          featureList: response[2]?.data?.listLoreeFeatures?.items
            ? JSON.parse(response[2].data.listLoreeFeatures.items[0]?.featureList as string)
            : [],
          loading: false,
        });
        this.getNewUser('today');
      });
    } catch (err) {
      console.error('Error:', err);
    }
  }

  getNewUser = (type: string) => {
    const filteredUserData = handlingFilterData(type, this.state.userData);
    this.setState({
      newUser: filteredUserData,
      filterDuration: type,
    });
  };

  isHaveAccess = (featureName: string) => {
    for (const feature of this.state.featureList as { name: string }[]) {
      if (feature.name === featureName) return true;
    }
    return false;
  };

  render() {
    return this.state.loading ? (
      <Loader />
    ) : (
      <Container fluid className='px-0'>
        <div className=' d-flex justify-content-between'>
          <h3 className='text-primary'>Dashboard</h3>
          <Dropdown
            drop='down'
            alignRight
            className='card-export-button'
            style={{ padding: '0px !important' }}
          >
            <Dropdown.Toggle
              disabled
              id='dropdown-menu-align-right'
              style={{ background: 'white', borderColor: 'white' }}
            >
              <ExportIcon width={25} height={25} className='export-icon' />
            </Dropdown.Toggle>
            <Dropdown.Menu className='dropdown-content'>
              <Dropdown.Item>PDF</Dropdown.Item>
              <Dropdown.Item>HTML</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
        <div className=' d-flex justify-content-between'>
          <h5 className='stats-label mb-3'>The below fields indicates the total numbers</h5>
        </div>
        <div className='d-flex mt-3 justify-content-between'>
          <Card className='shadow bg-white rounded analytics-stats'>
            <Card.Body className='px-4 py-3'>
              <Card.Text className='text-right stats-percent positive-count'>59.2%</Card.Text>
              <Card.Title className='text-center stats-count font-weight-bold mb-0'>
                {this.state.usersCount ? this.state.usersCount : '0'}
              </Card.Title>
              <Card.Text className='text-center stats-label'>Users</Card.Text>
            </Card.Body>
          </Card>
          <Card className='shadow bg-white rounded analytics-stats'>
            <Card.Body className='px-4 py-3'>
              <Card.Text className='text-right stats-percent positive-count'>44.2%</Card.Text>
              <Card.Title className='text-center stats-count font-weight-bold mb-0'>
                {this.state.customTemplateCount ? this.state.customTemplateCount : '0'}
              </Card.Title>
              <Card.Text className='text-center stats-label'>Templates</Card.Text>
            </Card.Body>
          </Card>
          <Card className='shadow bg-white rounded analytics-stats'>
            <Card.Body className='px-4 py-3'>
              <Card.Text className='text-right stats-percent positive-count'>60%</Card.Text>
              <Card.Title className='text-center stats-count font-weight-bold mb-0'>
                {this.state.customBlockCount ? this.state.customBlockCount : '0'}
              </Card.Title>
              <Card.Text className='text-center stats-label'>Rows + Elements</Card.Text>
            </Card.Body>
          </Card>
          {this.isHaveAccess('Interactives') ? (
            <Card className='shadow bg-white rounded analytics-stats'>
              <Card.Body className='px-4 py-3'>
                <Card.Text className='text-right stats-percent negative-count'>24.5%</Card.Text>
                <Card.Title className='text-center stats-count font-weight-bold mb-0'>
                  {this.state.interactivesCount ? this.state.interactivesCount : '0'}
                </Card.Title>
                <Card.Text className='text-center stats-label'>Interactives</Card.Text>
              </Card.Body>
            </Card>
          ) : (
            ''
          )}
          {this.isHaveAccess('H5P') ? (
            <Card className='shadow bg-white rounded analytics-stats'>
              <Card.Body className='px-4 py-3'>
                <Card.Text className='text-right stats-percent negative-count'>16%</Card.Text>
                <Card.Title className='text-center stats-count font-weight-bold mb-0'>
                  {this.state.h5pCount ? this.state.h5pCount : '0'}
                </Card.Title>
                <Card.Text className='text-center stats-label'>H5P</Card.Text>
              </Card.Body>
            </Card>
          ) : (
            ''
          )}
        </div>
        <div className='analytics-chart-block'>
          <Row className='mt-3' test-id='charts-sub-wrapper'>
            <NewUserChart
              getNewUser={this.getNewUser}
              newUser={this.state.newUser}
              totalUser={this.state.usersCount}
              filterDurationType={this.state.filterDuration}
            />
            <NewUsageChart />
          </Row>
        </div>
      </Container>
    );
  }
}
export default Dashboard;
