import React from 'react';
import { Card, Col } from 'react-bootstrap';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from 'recharts';
import DurationRangeForChart from './durationRangeChart';
import { handlingUsageFilterData } from '../analyticsCharts/usageFilterChart';
import {
  handleCustomBlocks,
  handleCustomTemplateAPI,
} from '../../customTemplate/customTemplateActions';

interface UsageChartState {
  templateUserData:
    | { name: string; Templates: number; Rows: number; Elements: number }[]
    | undefined;
  responses: {}[];
  loading: boolean;
  type: string;
}

class NewUsageChart extends React.Component<{}, UsageChartState> {
  state = {
    templateUserData: [],
    responses: [],
    loading: true,
    type: 'today',
  };

  async componentDidMount() {
    try {
      void Promise.all([await handleCustomTemplateAPI(), await handleCustomBlocks()]).then(
        (response) => {
          this.setState({
            responses: [response[0], response[1]],
            loading: false,
          });
          this.handleRangeChange('today');
        },
      );
    } catch (err) {
      console.error('Error:', err);
    }
  }

  getstate = () => {
    const templatedate: { templates: [] }[] = [];
    templatedate.push({
      templates: this.state.responses[0],
    });
    const elementdata: { element: [{}] }[] = [];
    const rowdata: { row: [{}] }[] = [];
    const blocksData: { type: string }[] = this.state.responses[1];
    if (blocksData) {
      for (let i = 0; i < blocksData.length; i++) {
        if (blocksData[i].type === 'Elements') {
          elementdata.push({
            element: [this.state.responses[1][i]],
          });
        }
        if (blocksData[i].type === 'Row') {
          rowdata.push({ row: [this.state.responses[1][i]] });
        }
      }
    }
    return [templatedate, rowdata, elementdata];
  };

  handleRangeChange = (type: string) => {
    const stateloop: _Any = this.getstate();
    const filteredUserData:
      | { name: string; Templates: number; Rows: number; Elements: number }[]
      | undefined = handlingUsageFilterData(type, stateloop);
    this.setState({
      templateUserData: filteredUserData,
      type: type,
    });
    return filteredUserData;
  };

  render() {
    return (
      <Col xs={12} lg={6} test-id='by-templates'>
        <Card className='shadow bg-white rounded'>
          <Card.Body className='p-3'>
            <div className='d-flex justify-content-between'>
              <Card.Title className='text-primary'>Build Details</Card.Title>
            </div>
            <DurationRangeForChart
              getNewUser={this.handleRangeChange}
              filterDurationType={this.state.type}
            />
            <div className='by-template-chart-render'>
              <ResponsiveContainer>
                <LineChart
                  data={this.state.templateUserData}
                  margin={{
                    top: 10,
                    right: 10,
                    left: 10,
                    bottom: 10,
                  }}
                >
                  <CartesianGrid strokeDasharray='3 3' />
                  <XAxis dataKey='name' />
                  <YAxis />
                  <Tooltip />
                  <Legend verticalAlign='bottom' align='center' iconType='square' />
                  <Line type='monotone' dataKey='Templates' stroke='#001996' activeDot={{ r: 5 }} />
                  <Line type='monotone' dataKey='Rows' stroke='#559d3f' />
                  <Line type='monotone' dataKey='Elements' stroke='#d1342b' />
                </LineChart>
              </ResponsiveContainer>
            </div>
          </Card.Body>
        </Card>
      </Col>
    );
  }
}

export default NewUsageChart;
