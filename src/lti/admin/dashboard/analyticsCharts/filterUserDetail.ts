import moment from 'moment';

export const handlingFilterData = (
  type: string,
  userData: { createdAt: string; id: string }[] | undefined,
) => {
  if (!userData) return;
  const today = new Date();
  const yearCount = moment().isLeapYear() ? 365 : 366;
  const newUsersDetails: string[] = [];
  for (const userDetail of userData) {
    switch (type) {
      case 'today':
        moment(new Date()).format('YYYY-MM-DD') ===
          moment(userDetail.createdAt).format('YYYY-MM-DD') && newUsersDetails.push(userDetail.id);
        break;
      case 'yesterday':
        moment(new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1)).format(
          'YYYY-MM-DD',
        ) === moment(userDetail.createdAt).format('YYYY-MM-DD') &&
          newUsersDetails.push(userDetail.id);
        break;
      case 'week':
        moment(new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7)).format(
          'YYYY-MM-DD',
        ) < moment(userDetail.createdAt).format('YYYY-MM-DD') &&
          newUsersDetails.push(userDetail.id);
        break;
      case 'month':
        moment(
          new Date(
            today.getFullYear(),
            today.getMonth(),
            today.getDate() - moment(today).daysInMonth(),
          ),
        ).format('YYYY-MM-DD') < moment(userDetail.createdAt).format('YYYY-MM-DD') &&
          newUsersDetails.push(userDetail.id);
        break;
      case 'threemonth':
        today.setMonth(today.getMonth() + -3);
        moment(today).format('yyyy-MM-DD') < moment(userDetail.createdAt).format('YYYY-MM-DD') &&
          newUsersDetails.push(userDetail.id);
        break;
      case 'sixmonth':
        today.setMonth(today.getMonth() + -6);
        moment(today).format('yyyy-MM-DD') < moment(userDetail.createdAt).format('YYYY-MM-DD') &&
          newUsersDetails.push(userDetail.id);
        break;
      case 'year':
        moment(new Date(today.getFullYear(), today.getMonth(), today.getDate() - yearCount)).format(
          'YYYY-MM-DD',
        ) < moment(userDetail.createdAt).format('YYYY-MM-DD') &&
          newUsersDetails.push(userDetail.id);
        break;
    }
  }
  return newUsersDetails;
};
