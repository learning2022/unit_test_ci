import React from 'react';
import { Button } from 'react-bootstrap';

interface DurationProps {
  getNewUser(type: string): void;
  filterDurationType?: string;
}
class DurationRangeForChart extends React.Component<DurationProps> {
  render() {
    return (
      <div className='d-flex'>
        <Button
          variant={this.props.filterDurationType === 'today' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('today')}
        >
          Today
        </Button>
        <Button
          variant={this.props.filterDurationType === 'yesterday' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('yesterday')}
        >
          Yesterday
        </Button>
        <Button
          variant={this.props.filterDurationType === 'week' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('week')}
        >
          1 Week
        </Button>
        <Button
          variant={this.props.filterDurationType === 'month' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('month')}
        >
          1 Month
        </Button>
        <Button
          variant={this.props.filterDurationType === 'threemonth' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('threemonth')}
        >
          3 Months
        </Button>
        <Button
          variant={this.props.filterDurationType === 'sixmonth' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('sixmonth')}
        >
          6 Months
        </Button>
        <Button
          variant={this.props.filterDurationType === 'year' ? 'primary' : 'outline-primary'}
          onClick={() => this.props.getNewUser('year')}
        >
          1 Year
        </Button>
      </div>
    );
  }
}
export default DurationRangeForChart;
