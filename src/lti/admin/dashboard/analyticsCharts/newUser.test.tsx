import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import NewUser from './newUser';

describe('#user detail chart', () => {
  let newUserProps;
  let newUserChart: ReactWrapper;

  beforeEach(() => {
    newUserProps = {
      getNewUser: jest.fn(),
      newUser: [],
      totalUser: 24,
      filterDurationType: 'today',
    };
    newUserChart = mount(<NewUser {...newUserProps} />);
  });

  describe('#rendering ui', () => {
    test('render user detail chart col', () => {
      expect(newUserChart.find('Col')).toHaveLength(1);
    });
    test('render user detail chart cardbody', () => {
      expect(newUserChart.find('CardBody')).toHaveLength(1);
    });
    test('render filter buttons', () => {
      expect(newUserChart.find('DurationRangeForChart')).toHaveLength(1);
    });
    test('filter buttons length', () => {
      expect(newUserChart.find('Button')).toHaveLength(7);
    });
  });

  describe('#triggering function', () => {
    test('checking button onclick', () => {
      const getNewUser = jest.fn();
      newUserChart.find('button').at(1).simulate('click', getNewUser('week'));
      expect(getNewUser).toBeCalled();
    });
  });
});
