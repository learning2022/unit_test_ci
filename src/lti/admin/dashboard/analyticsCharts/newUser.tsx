import React from 'react';
import { Card, Col } from 'react-bootstrap';
import { PieChart, Pie, Cell, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';
import DurationRangeForChart from './durationRangeChart';

interface UserProps {
  getNewUser(type: string): void;
  newUser: string[] | undefined;
  totalUser: number | undefined;
  filterDurationType: string | undefined;
}

class NewUserChart extends React.Component<UserProps> {
  render() {
    const pieData = [
      { name: 'Total user', value: this.props.totalUser },
      { name: 'New user', value: this.props.newUser?.length },
    ];
    const PIECOLORS = ['#001996', '#bbdc93'];
    return (
      <Col xs={12} lg={6} test-id='by-country'>
        <Card className='shadow bg-white rounded'>
          <Card.Body className='p-3'>
            <div className='d-flex justify-content-between'>
              <Card.Title className='text-primary'>Users Details</Card.Title>
            </div>
            <DurationRangeForChart
              getNewUser={this.props.getNewUser}
              filterDurationType={this.props.filterDurationType}
            />
            <div className='by-country-chart-render'>
              <ResponsiveContainer>
                <PieChart>
                  <Pie
                    data={pieData}
                    innerRadius={75}
                    outerRadius={100}
                    fill='#8884d8'
                    paddingAngle={0}
                    dataKey='value'
                  >
                    {pieData.map((entry, index) => (
                      <Cell key={`cell-${index}`} fill={PIECOLORS[index % PIECOLORS.length]} />
                    ))}
                    <Label
                      value={this.props.newUser?.length}
                      position='centerBottom'
                      className='active-user-value'
                    />
                    <Label value='New Users' position='centerTop' className='active-user-label' />
                  </Pie>
                  <Tooltip />
                  <Legend
                    verticalAlign='middle'
                    align='right'
                    layout='vertical'
                    iconType='square'
                  />
                </PieChart>
              </ResponsiveContainer>
            </div>
          </Card.Body>
        </Card>
      </Col>
    );
  }
}
export default NewUserChart;
