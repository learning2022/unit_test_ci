import moment from 'moment';
export const handlingUsageFilterData = (type: string, userData: []) => {
  const today = new Date();
  const todayDate = moment(new Date()).format('YYYY-MM-DD');
  const yesterdayDate = moment().subtract(1, 'days').startOf('day').format('YYYY-MM-DD');

  switch (type) {
    case 'today': {
      const startOfDay = moment().startOf('day');
      const todayMidDay = moment(startOfDay).add(12, 'hours');
      const todayInterval = [
        { name: '00:00AM', Templates: 0, Rows: 0, Elements: 0 },
        { name: '12:00PM', Templates: 0, Rows: 0, Elements: 0 },
        { name: '11:49PM', Templates: 0, Rows: 0, Elements: 0 },
      ];
      for (const content of userData) {
        for (const userDetail of content as [
          {
            templates: { createdAt: string }[];
            row: { createdAt: string }[];
            element: { createdAt: string }[];
          },
        ]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              const templateDate = moment(userDetail.templates[i].createdAt).format('YYYY-MM-DD');
              if (
                templateDate === todayDate &&
                moment(startOfDay).format('HH:MM') ===
                  moment(userDetail.templates[i].createdAt).format('HH:MM')
              ) {
                todayInterval[0].Templates += 1;
              } else if (
                templateDate === todayDate &&
                moment(todayMidDay).format('HH:MM') >=
                  moment(userDetail.templates[i].createdAt).format('HH:MM')
              ) {
                todayInterval[1].Templates += 1;
              } else if (
                templateDate === todayDate &&
                moment(todayMidDay).format('HH:MM') <
                  moment(userDetail.templates[i].createdAt).format('HH:MM')
              ) {
                todayInterval[2].Templates += 1;
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              const rowDate = moment(userDetail.row[i].createdAt).format('YYYY-MM-DD');
              if (
                rowDate === todayDate &&
                moment(startOfDay).format('HH:MM') ===
                  moment(userDetail.row[i].createdAt).format('HH:MM')
              ) {
                todayInterval[0].Rows += 1;
              }

              if (
                rowDate === todayDate &&
                moment(todayMidDay).format('HH:MM') >=
                  moment(userDetail.row[i].createdAt).format('HH:MM')
              ) {
                todayInterval[1].Rows += 1;
              }
              if (
                rowDate === todayDate &&
                moment(todayMidDay).format('HH:MM') <
                  moment(userDetail.row[i].createdAt).format('HH:MM')
              ) {
                todayInterval[2].Rows += 1;
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              const elementDate = moment(userDetail.element[i].createdAt).format('YYYY-MM-DD');
              if (
                todayDate === elementDate &&
                moment(startOfDay).format('HH:MM') ===
                  moment(userDetail.element[i].createdAt).format('HH:MM')
              ) {
                todayInterval[0].Elements += 1;
              }

              if (
                todayDate === elementDate &&
                moment(todayMidDay).format('HH:MM') >=
                  moment(userDetail.element[i].createdAt).format('HH:MM')
              ) {
                todayInterval[1].Elements += 1;
              }
              if (
                todayDate === elementDate &&
                moment(todayMidDay).format('HH:MM') <
                  moment(userDetail.element[i].createdAt).format('HH:MM')
              ) {
                todayInterval[2].Elements += 1;
              }
            }
          }
        }
      }
      return todayInterval;
    }
    case 'yesterday': {
      const startOfYesterday = moment().subtract(1, 'days').startOf('day');
      const yesterdayMidDay = moment(startOfYesterday).add(12, 'hours');
      const yesterdayInterval = [
        { name: '00:00AM', Templates: 0, Rows: 0, Elements: 0 },
        { name: '12:00PM', Templates: 0, Rows: 0, Elements: 0 },
        { name: '11:49PM', Templates: 0, Rows: 0, Elements: 0 },
      ];
      for (const content of userData) {
        for (const userDetail of content as {
          templates: { createdAt: string }[];
          row: { createdAt: string }[];
          element: { createdAt: string }[];
        }[]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              const templateDate = moment(userDetail.templates[i].createdAt).format('YYYY-MM-DD');
              if (
                templateDate === yesterdayDate &&
                moment(startOfYesterday).format('HH:MM') ===
                  moment(userDetail.templates[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[0].Templates += 1;
              }

              if (
                templateDate === yesterdayDate &&
                moment(yesterdayMidDay).format('HH:MM') >=
                  moment(userDetail.templates[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[1].Templates += 1;
              }
              if (
                templateDate === yesterdayDate &&
                moment(yesterdayMidDay).format('HH:MM') <
                  moment(userDetail.templates[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[2].Templates += 1;
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              const rowDate = moment(userDetail.row[i].createdAt).format('YYYY-MM-DD');
              if (
                rowDate === yesterdayDate &&
                moment(startOfYesterday).format('HH:MM') ===
                  moment(userDetail.row[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[0].Rows += 1;
              }

              if (
                rowDate === yesterdayDate &&
                moment(yesterdayMidDay).format('HH:MM') >
                  moment(userDetail.row[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[1].Rows += 1;
              }
              if (
                rowDate === yesterdayDate &&
                moment(yesterdayMidDay).format('HH:MM') <
                  moment(userDetail.row[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[2].Rows += 1;
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              const elementDate = moment(userDetail.element[i].createdAt).format('YYYY-MM-DD');
              if (
                elementDate === yesterdayDate &&
                moment(startOfYesterday).format('HH:MM') ===
                  moment(userDetail.element[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[0].Elements += 1;
              }

              if (
                elementDate === yesterdayDate &&
                moment(yesterdayMidDay).format('HH:MM') >
                  moment(userDetail.element[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[1].Elements += 1;
              }
              if (
                elementDate === yesterdayDate &&
                moment(yesterdayMidDay).format('HH:MM') <
                  moment(userDetail.element[i].createdAt).format('HH:MM')
              ) {
                yesterdayInterval[1].Elements += 1;
              }
            }
          }
        }
      }
      return yesterdayInterval;
    }
    case 'week': {
      const lastWeekData: { name: string; Templates: number; Rows: number; Elements: number }[] =
        [];
      for (let i = 7; i > 0; i--) {
        const monthName = moment(
          new Date(today.getFullYear(), today.getMonth(), today.getDate() - i),
        );
        lastWeekData.push({
          name: moment(monthName).format('ddd'),
          Templates: 0,
          Rows: 0,
          Elements: 0,
        });
      }
      const weekBefore = moment(
        new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7),
      );
      for (const content of userData) {
        for (const userDetail of content as {
          templates: { createdAt: string }[];
          row: { createdAt: string }[];
          element: { createdAt: string }[];
        }[]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              const templateDate = moment(userDetail.templates[i].createdAt).format('YYYY-MM-DD');
              if (
                templateDate !== todayDate &&
                moment(weekBefore).format('YYYY-MM-DD') <=
                  moment(userDetail.templates[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (const date of lastWeekData) {
                  if (moment(userDetail.templates[i].createdAt).format('ddd') === date.name) {
                    date.Templates += 1;
                  }
                }
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              const rowDate = moment(userDetail.row[i].createdAt).format('YYYY-MM-DD');
              if (
                rowDate !== todayDate &&
                moment(weekBefore).format('YYYY-MM-DD') <=
                  moment(userDetail.row[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (const date of lastWeekData) {
                  if (moment(userDetail.row[i].createdAt).format('ddd') === date.name) {
                    date.Rows += 1;
                  }
                }
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              const elementDate = moment(userDetail.element[i].createdAt).format('YYYY-MM-DD');
              if (
                elementDate !== todayDate &&
                moment(weekBefore).format('YYYY-MM-DD') <=
                  moment(userDetail.element[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (const date of lastWeekData) {
                  if (moment(userDetail.element[i].createdAt).format('ddd') === date.name) {
                    date.Elements += 1;
                  }
                }
              }
            }
          }
        }
      }
      return lastWeekData;
    }
    case 'month': {
      const lastMonthData: { name: string; Templates: number; Rows: number; Elements: number }[] =
        [];
      for (let i = 31; i > 0; i -= 5) {
        const monthName = moment(
          new Date(today.getFullYear(), today.getMonth(), today.getDate() - i),
        );
        lastMonthData.push({
          name: moment(monthName).format('MM/DD'),
          Templates: 0,
          Rows: 0,
          Elements: 0,
        });
      }
      const monthBefore = moment(
        new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30),
      );

      for (const content of userData) {
        for (const userDetail of content as {
          templates: { createdAt: string }[];
          row: { createdAt: string }[];
          element: { createdAt: string }[];
        }[]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              if (
                moment(monthBefore).format('YYYY-MM-DD') <=
                moment(userDetail.templates[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (let j = 1; j < lastMonthData.length; j++) {
                  if (
                    lastMonthData[j - 1].name ===
                    moment(userDetail.templates[i].createdAt).format('MM/DD')
                  ) {
                    lastMonthData[j - 1].Templates += 1;
                  }
                  if (
                    lastMonthData[j - 1].name <
                      moment(userDetail.templates[i].createdAt).format('MM/DD') &&
                    lastMonthData[j]?.name >
                      moment(userDetail.templates[i].createdAt).format('MM/DD')
                  ) {
                    lastMonthData[j].Templates += 1;
                  }
                }
              }
              if (
                lastMonthData[lastMonthData.length - 1].name ===
                moment(userDetail.templates[i].createdAt).format('MM/DD')
              ) {
                lastMonthData[lastMonthData.length - 1].Templates += 1;
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              if (
                moment(monthBefore).format('YYYY-MM-DD') <=
                moment(userDetail.row[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (let j = 1; j < lastMonthData.length; j++) {
                  if (
                    lastMonthData[j - 1].name ===
                    moment(userDetail.row[i].createdAt).format('MM/DD')
                  ) {
                    lastMonthData[j - 1].Rows += 1;
                  }
                  if (
                    lastMonthData[j - 1].name <
                      moment(userDetail.row[i].createdAt).format('MM/DD') &&
                    lastMonthData[j].name > moment(userDetail.row[i].createdAt).format('MM/DD')
                  ) {
                    lastMonthData[j].Rows += 1;
                  }
                }
              }
              if (
                lastMonthData[lastMonthData.length - 1].name ===
                moment(userDetail.row[i].createdAt).format('MM/DD')
              ) {
                lastMonthData[lastMonthData.length - 1].Rows += 1;
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              if (
                moment(monthBefore).format('YYYY-MM-DD') <=
                moment(userDetail.element[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (let j = 1; j < lastMonthData.length; j++) {
                  if (
                    lastMonthData[j - 1].name ===
                    moment(userDetail.element[i].createdAt).format('MM/DD')
                  ) {
                    lastMonthData[j - 1].Elements += 1;
                  }
                  if (
                    lastMonthData[j - 1].name <
                      moment(userDetail.element[i].createdAt).format('MM/DD') &&
                    lastMonthData[j]?.name > moment(userDetail.element[i].createdAt).format('MM/DD')
                  ) {
                    lastMonthData[j].Elements += 1;
                  }
                }
              }
              if (
                lastMonthData[lastMonthData.length - 1].name ===
                moment(userDetail.element[i].createdAt).format('MM/DD')
              ) {
                lastMonthData[lastMonthData.length - 1].Elements += 1;
              }
            }
          }
        }
      }
      return lastMonthData;
    }
    case 'threemonth': {
      const lastThreeMonthsData: {
        name: string;
        Templates: number;
        Rows: number;
        Elements: number;
      }[] = [];
      for (let i = 3; i > 0; i--) {
        const monthName = today.getMonth() + 1 - i;
        const formattedMonthName = moment(monthName, 'MM').format('MMMM');
        lastThreeMonthsData.push({ name: formattedMonthName, Templates: 0, Rows: 0, Elements: 0 });
      }
      const threeMonthsbefore = moment(new Date(today.getFullYear(), today.getMonth() - 3));

      for (const content of userData) {
        for (const userDetail of content as {
          templates: { createdAt: string }[];
          row: { createdAt: string }[];
          element: { createdAt: string }[];
        }[]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              if (
                moment(threeMonthsbefore).format('YYYY-MM') <=
                moment(userDetail.templates[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastThreeMonthsData) {
                  if (month.name === moment(userDetail.templates[i].createdAt).format('MMMM')) {
                    month.Templates += 1;
                  }
                }
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              if (
                moment(threeMonthsbefore).format('YYYY-MM-DD') <=
                moment(userDetail.row[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (const month of lastThreeMonthsData) {
                  if (month.name === moment(userDetail.row[i].createdAt).format('MMMM')) {
                    month.Rows += 1;
                  }
                }
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              if (
                moment(threeMonthsbefore).format('YYYY-MM-DD') <=
                moment(userDetail.element[i].createdAt).format('YYYY-MM-DD')
              ) {
                for (const month of lastThreeMonthsData) {
                  if (month.name === moment(userDetail.element[i].createdAt).format('MMMM')) {
                    month.Elements += 1;
                  }
                }
              }
            }
          }
        }
      }
      return lastThreeMonthsData;
    }
    case 'sixmonth': {
      const lastSixMonths: { name: string; Templates: number; Rows: number; Elements: number }[] =
        [];
      for (let i = 6; i > 0; i--) {
        const monthName = today.getMonth() + 1 - i;
        lastSixMonths.push({
          name: moment(monthName, 'MM').format('MMMM'),
          Templates: 0,
          Rows: 0,
          Elements: 0,
        });
      }
      const beforeSixMonths = moment(new Date(today.getFullYear(), today.getMonth() - 6));

      for (const content of userData) {
        for (const userDetail of content as {
          templates: { createdAt: string }[];
          row: { createdAt: string }[];
          element: { createdAt: string }[];
        }[]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              if (
                moment(beforeSixMonths).format('YYYY-MM') <=
                moment(userDetail.templates[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastSixMonths) {
                  if (month.name === moment(userDetail.templates[i].createdAt).format('MMMM')) {
                    month.Templates += 1;
                  }
                }
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              if (
                moment(beforeSixMonths).format('YYYY-MM') <=
                moment(userDetail.row[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastSixMonths) {
                  if (month.name === moment(userDetail.row[i].createdAt).format('MMMM')) {
                    month.Rows += 1;
                  }
                }
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              if (
                moment(beforeSixMonths).format('YYYY-MM') <=
                moment(userDetail.element[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastSixMonths) {
                  if (month.name === moment(userDetail.element[i].createdAt).format('MMMM')) {
                    month.Elements += 1;
                  }
                }
              }
            }
          }
        }
      }
      return lastSixMonths;
    }
    case 'year': {
      const lastYearMonths: { name: string; Templates: number; Rows: number; Elements: number }[] =
        [];
      for (let i = 12; i > 0; i--) {
        const monthName = moment(new Date(today.getFullYear(), today.getMonth() - i));
        lastYearMonths.push({
          name: moment(monthName).format('MMM'),
          Templates: 0,
          Rows: 0,
          Elements: 0,
        });
      }
      const beforeOneYear = moment(new Date(today.getFullYear(), today.getMonth() - 12));

      for (const content of userData) {
        for (const userDetail of content as {
          templates: { createdAt: string }[];
          row: { createdAt: string }[];
          element: { createdAt: string }[];
        }[]) {
          if (userDetail.templates) {
            for (let i = 0; i < userDetail.templates.length; i++) {
              if (
                moment(beforeOneYear).format('YYYY-MM') <=
                moment(userDetail.templates[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastYearMonths) {
                  if (month.name === moment(userDetail.templates[i].createdAt).format('MMM')) {
                    month.Templates += 1;
                  }
                }
              }
            }
          }

          if (userDetail.row) {
            for (let i = 0; i < userDetail.row.length; i++) {
              if (
                moment(beforeOneYear).format('YYYY-MM') <=
                moment(userDetail.row[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastYearMonths) {
                  if (month.name === moment(userDetail.row[i].createdAt).format('MMM')) {
                    month.Rows += 1;
                  }
                }
              }
            }
          }

          if (userDetail.element) {
            for (let i = 0; i < userDetail.element.length; i++) {
              if (
                moment(beforeOneYear).format('YYYY-MM') <=
                moment(userDetail.element[i].createdAt).format('YYYY-MM')
              ) {
                for (const month of lastYearMonths) {
                  if (month.name === moment(userDetail.element[i].createdAt).format('MMM')) {
                    month.Elements += 1;
                  }
                }
              }
            }
          }
        }
      }
      return lastYearMonths;
    }
  }
};
