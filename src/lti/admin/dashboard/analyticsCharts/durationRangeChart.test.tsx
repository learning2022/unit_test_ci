import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import DurationRangers from './durationRangeChart';

describe('#duration rangers for filter', () => {
  let durationfilterProps;
  let durationRangerButtons: ReactWrapper;

  beforeEach(() => {
    durationfilterProps = {
      getNewUser: jest.fn(),
      filterDurationType: 'today',
    };
    durationRangerButtons = mount(<DurationRangers {...durationfilterProps} />);
  });

  describe('#rendering ui', () => {
    test('rendering filter buttons parent div', () => {
      expect(durationRangerButtons.find('div')).toHaveLength(1);
    });
    test('filter buttons length', () => {
      expect(durationRangerButtons.find('button')).toHaveLength(7);
    });
  });

  describe('#triggering function', () => {
    test('triggering today button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(0).simulate('click', getNewUser('today'));
      expect(durationRangerButtons.find('button').at(0).text()).toEqual('Today');
      expect(getNewUser).toBeCalled();
    });
    test('triggering yesterday button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(1).simulate('click', getNewUser('yesterday'));
      expect(durationRangerButtons.find('button').at(1).text()).toEqual('Yesterday');
      expect(getNewUser).toBeCalled();
    });
    test('triggering week button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(2).simulate('click', getNewUser('week'));
      expect(durationRangerButtons.find('button').at(2).text()).toEqual('1 Week');
      expect(getNewUser).toBeCalled();
    });
    test('triggering one month button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(3).simulate('click', getNewUser('month'));
      expect(durationRangerButtons.find('button').at(3).text()).toEqual('1 Month');
      expect(getNewUser).toBeCalled();
    });
    test('triggering three month button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(4).simulate('click', getNewUser('threemonth'));
      expect(durationRangerButtons.find('button').at(4).text()).toEqual('3 Months');
      expect(getNewUser).toBeCalled();
    });
    test('triggering six month button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(5).simulate('click', getNewUser('sixmonth'));
      expect(durationRangerButtons.find('button').at(5).text()).toEqual('6 Months');
      expect(getNewUser).toBeCalled();
    });
    test('triggering 1 year button', async () => {
      const getNewUser = jest.fn();
      durationRangerButtons.find('button').at(6).simulate('click', getNewUser('year'));
      expect(durationRangerButtons.find('button').at(6).text()).toEqual('1 Year');
      expect(getNewUser).toBeCalled();
    });
  });
});
