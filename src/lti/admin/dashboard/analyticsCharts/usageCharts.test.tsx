import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import UsageChart from './usageCharts';
import mockDate from 'mockdate';

describe('#user detail chart', () => {
  let usagefilterProps;
  let templatesUsageChart: ReactWrapper;

  beforeEach(() => {
    usagefilterProps = {
      getNewUser: jest.fn(),
    };
    templatesUsageChart = mount(<UsageChart {...usagefilterProps} />);
    mockDate.set(new Date('8/13/2021'));
  });

  describe('#rendering ui', () => {
    test('render usage detail chart col', () => {
      expect(templatesUsageChart.find('Col')).toHaveLength(1);
    });
    test('render usage detail chart cardbody', () => {
      expect(templatesUsageChart.find('CardBody')).toHaveLength(1);
    });
    test('filter buttons length', () => {
      expect(templatesUsageChart.find('Button')).toHaveLength(7);
    });
  });

  describe('#triggering function', () => {
    test('checking button onclick', () => {
      const getNewUser = jest.fn();
      templatesUsageChart.find('button').at(1).simulate('click', getNewUser('month'));
      expect(getNewUser).toBeCalled();
    });
  });
});
