import mockDate from 'mockdate';
import { handlingFilterData } from './filterUserDetail';

describe('#filter user detail', () => {
  let userData: { createdAt: string; id: string }[] | undefined = [];
  beforeEach(() => {
    userData = [
      {
        createdAt: '2021-05-18T06:39:15.772Z',
        id: '1065cd59-8d1b-4ed0-8610-6120d5860e98',
      },
      {
        createdAt: '2021-05-17T05:06:17.631Z',
        id: '9779111b-0101-441f-b5fa-c52369fa9a82',
      },
      {
        createdAt: '2021-05-10T05:06:17.631Z',
        id: '9779111b-0101-441f-b5fa-c52369fa9a82',
      },
    ];
    mockDate.set(new Date('5/18/2021'));
  });
  test('new user for today', async () => {
    expect(handlingFilterData('today', userData)?.length).toEqual(1);
  });

  test('new user for yesterday', async () => {
    expect(handlingFilterData('yesterday', userData)?.length).toEqual(1);
  });

  test('new user for month', async () => {
    expect(handlingFilterData('month', userData)?.length).toEqual(3);
  });

  test('new user for week', async () => {
    expect(handlingFilterData('week', userData)?.length).toEqual(2);
  });

  test('new user for three month', async () => {
    expect(handlingFilterData('threemonth', userData)?.length).toEqual(3);
  });

  test('new user for six month', async () => {
    expect(handlingFilterData('sixmonth', userData)?.length).toEqual(3);
  });

  test('new user for year', async () => {
    expect(handlingFilterData('year', userData)?.length).toEqual(3);
  });
});
