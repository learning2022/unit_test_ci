import React, { useState } from 'react';
import { Container, Col, Row, Form } from 'react-bootstrap';
import { Auth } from 'aws-amplify';
import { Switch, useRouteMatch, NavLink } from 'react-router-dom';
import Sidebar from './sidebar';
import RoleFeature from './rolesAndFeature/index';
import CustomTemplate from './customTemplate/index';
import { FontManager } from './fontmanager/index';
import { StyleManager } from './stylemanager/index';
import ThemeManager from './thememanager/index';
import { Configure } from './configure/index';
import PrivateRoute from '../../router/PrivateRoute';
import LtiLogs from './auditLogs/index';
import Dashboard from './dashboard/index';
import H5pInteractives from './h5p/index';

import { lmsUrlRedirection } from '../../lmsConfig';
import { FeatureInterface } from '../../loree-editor/interface';

type AdminDashboardProps = {
  featureFlags: FeatureInterface | undefined;
};

export const AdminDashboard = (props: AdminDashboardProps): JSX.Element => {
  const { path, url } = useRouteMatch();
  const [userName, setUserName] = useState('');
  const useThemeManager: boolean = !!props.featureFlags?.thememanager;
  void Auth.currentAuthenticatedUser().then((user) => {
    setUserName(user.attributes.name);
  });
  return (
    <>
      <Container fluid>
        <Row>
          <Col xs={0} md={2} className='px-0' id='adminSideNavExpanded'>
            <Sidebar path={url} featureFlags={props.featureFlags} />
          </Col>
          <Col
            xs={12}
            md={10}
            className='mt-0 main-section my-templates-wrapper'
            id='adminMainExpanded'
          >
            <div className='d-flex justify-content-between my-4 admin-header-right'>
              <h2 className='mb-0 font-weight-bold'>
                Hello, <span>{userName}</span>
              </h2>
              <div className='d-flex align-items-center'>
                <div>
                  {sessionStorage.getItem('isAdmin') === 'true' && (
                    <NavLink
                      to={lmsUrlRedirection()}
                      className='ml-auto text-white btn btn-primary'
                    >
                      Exit Admin
                    </NavLink>
                  )}
                </div>
                <Form.Group className='ml-2 mb-0 d-none'>
                  <Form.Control as='select' id='clientFilter'>
                    <option value=''>All Clients</option>
                    <option>UTS</option>
                    <option>Strawberry</option>
                  </Form.Control>
                  <div className='arrow-down' />
                </Form.Group>
              </div>
            </div>
            <hr />
            <Switch>
              <PrivateRoute exact path={`${path}/`}>
                <Dashboard />
              </PrivateRoute>
              <PrivateRoute exact path={`${path}/dash`}>
                <Dashboard />
              </PrivateRoute>
              <PrivateRoute exact path={`${path}/roles`}>
                <RoleFeature />
              </PrivateRoute>
              <PrivateRoute path={`${path}/customblock`}>
                <CustomTemplate />
              </PrivateRoute>
              <PrivateRoute path={`${path}/stylemanager`}>
                <StyleManager />
              </PrivateRoute>
              {useThemeManager && (
                <PrivateRoute path={`${path}/thememanager`}>
                  <ThemeManager />
                </PrivateRoute>
              )}
              <PrivateRoute path={`${path}/fonts`}>
                <FontManager />
              </PrivateRoute>

              <PrivateRoute path={`${path}/configure`}>
                <Configure />
              </PrivateRoute>
              <PrivateRoute path={`${path}/h5ps`}>
                <H5pInteractives />
              </PrivateRoute>
              <PrivateRoute path={`${path}/logs`}>
                <LtiLogs />
              </PrivateRoute>
            </Switch>
          </Col>
        </Row>
      </Container>
    </>
  );
};
