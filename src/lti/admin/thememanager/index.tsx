import React, { useState, useEffect } from 'react';
import { apm } from '@elastic/apm-rum';
import { Accordion, Card } from 'react-bootstrap';
import { faCaretRight, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import ToastComponent from '../../components/ToastComponent';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ThemeInfo, getThemeList } from './service';
import Loading from '../../../components/loader/loading';
import { translate } from '../../../i18n/translate';

export default function ThemeManager() {
  const [loading, setLoading] = useState(true);
  const [themeList, setThemeList] = useState<ThemeInfo[]>([]);
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState('');
  const [activeAccordion, setActiveAccordion] = useState(0);
  const closeToast = () => {
    setShowToast(false);
    setToastMessage('');
  };

  useEffect(() => {
    const load = async () => {
      try {
        const list = await getThemeList();
        setThemeList(list);
      } catch (error) {
        setShowToast(true);
        setToastMessage(translate('theme.error'));
        console.error(error);
        apm.captureError(error as Error);
      } finally {
        setLoading(false);
      }
    };
    void load();
  }, []);

  return (
    <>
      <div className='text-primary mb-3'>
        <h3>{translate('theme.thememanager')}</h3>
      </div>
      {showToast && (
        <ToastComponent showToast={showToast} toastMessage={toastMessage} closeToast={closeToast} />
      )}
      <div>
        {loading ? (
          <>
            <Loading />
          </>
        ) : (
          <>
            <Accordion
              data-testid='LoadingComplete'
              defaultActiveKey='0'
              className='mt-3 admin-accordion'
            >
              {themeList.length < 1 ? (
                <h3>{translate('theme.nothemefound')}</h3>
              ) : (
                themeList.map((theme, i) => (
                  <Accordion.Toggle
                    key={i}
                    as={Card.Header}
                    eventKey={theme.id}
                    onClick={() => setActiveAccordion(activeAccordion === i ? -1 : i)}
                  >
                    {theme.name}
                    <span className='right-actions'>
                      <FontAwesomeIcon icon={activeAccordion === i ? faCaretDown : faCaretRight} />
                    </span>
                  </Accordion.Toggle>
                ))
              )}
            </Accordion>
          </>
        )}
      </div>
    </>
  );
}
