// TODO replace with GraphQL autogen thingy
export interface ThemeInfo {
  id: string; // ID!
  ltiPlatformID: string;
  active: boolean; // Boolean;
  name: string;
  description?: string;
  createdAt: string; // AWSDateTime;
  createdBy: string;
  updatedAt?: string; // AWSDateTime!
}

export async function getThemeList(): Promise<ThemeInfo[]> {
  return await new Promise((resolve, reject) => {
    resolve([
      {
        id: 'hello',
        active: true,
        ltiPlatformID: 'chris1',
        name: "chris's first theme",
        description: "I love the nice colours, pity we can't name them",
        createdAt: 'now',
        createdBy: 'me',
        updatedAt: 'later',
      },
      {
        id: 'hello2',
        ltiPlatformID: 'chris1',
        active: true,
        name: "chris's second theme",
        createdAt: 'now',
        createdBy: 'me',
      },
      {
        id: 'hello3',
        active: true,
        ltiPlatformID: 'baz3',
        name: "baz's only theme",
        createdAt: 'now',
        createdBy: 'baz',
      },
    ]);
  });
}
