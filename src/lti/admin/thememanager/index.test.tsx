import React from 'react';
import { render, screen } from '@testing-library/react';
import { apm } from '@elastic/apm-rum';

import { getThemeList } from './service';
import ThemeManager from './index';
import axe from '../../../axeHelper';

jest.mock('./service');
jest.mock('@elastic/apm-rum');

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#themeManagerIndex', () => {
  describe('when themes query returns result', () => {
    beforeEach(() => {
      const mockGetThemeList = getThemeList as jest.MockedFunction<typeof getThemeList>;
      mockGetThemeList.mockResolvedValue([
        {
          id: '1234',
          ltiPlatformID: 'a platform',
          active: true,
          name: 'Woot Theme',
          description: 'A theme to make you excited',
          createdAt: '2020-02-02T12:00:00Z',
          createdBy: 'chris',
          updatedAt: '2020-02-02T12:00:00Z',
        },
      ]);
    });

    test('Loading Spinner is displayed', async () => {
      render(<ThemeManager />);
      expect(screen.getByTitle(/loading, please wait/i)).toBeDefined();
      expect(await axe(document.body)).toHaveNoViolations();
    });

    test('Main section is displayed', async () => {
      render(<ThemeManager />);
      expect(screen.getByTitle(/loading, please wait/i)).not.toBeVisible();
      expect(await screen.findByTestId('LoadingComplete')).toBeVisible();
      expect(await axe(document.body)).toHaveNoViolations();
    });
  });
  describe('when themes query returns empty result', () => {
    beforeEach(async () => {
      const mockGetThemeList = getThemeList as jest.MockedFunction<typeof getThemeList>;
      mockGetThemeList.mockResolvedValue([]);
    });

    test('No themes message is displayed', async () => {
      render(<ThemeManager />);
      expect(await screen.findByText(/theme.nothemefound/)).toBeVisible();
      expect(await axe(document.body)).toHaveNoViolations();
    });
  });

  describe('when themes error, show error', () => {
    beforeEach(() => {
      const mockGetThemeList = getThemeList as jest.MockedFunction<typeof getThemeList>;
      mockGetThemeList.mockRejectedValue(new Error('Oh no!'));
    });

    test('Unknown toast message is displayed, message logged to APM', async () => {
      const mockCaptureError = apm.captureError as jest.MockedFunction<typeof apm.captureError>;

      render(<ThemeManager />);

      expect(await screen.findByText(/theme.error/i)).toBeVisible();
      expect(mockCaptureError.mock.calls.length).toBe(1);
      expect(await axe(document.body)).toHaveNoViolations();
    });
  });
});
