import React from 'react';
import { StyleManager } from './index';
import { API } from 'aws-amplify';
import { render, screen } from '@testing-library/react';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#styleManagerIndex', () => {
  describe('when custom styles query returns result', () => {
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation((arg) => {
        return {
          data: {
            listCustomStyles: {
              items: [
                {
                  id: '5ab18de7-62ca-4623-a44f-56419a3a4c71',
                  ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                  customColor: '[{"color":"#1c29b2"}]',
                  customHeader:
                    '[{"h1":{"size":"66px","font":"Arial"}},{"h2":{"size":"54px","font":"Arial"}},{"h3":{"size":"46px","font":"Arial"}},{"h4":{"size":"34px","font":"SourceSansPro"}},{"h5":{"size":"28px","font":"Arial"}},{"h6":{"size":"22px","font":"Arial"}},{"paragraph":{"size":"20px","font":"SourceSansPro"}}]',
                  customFont:
                    '[{"fontFamily":"ABeeZee, sans-serif","name":"ABeeZee","url":"https://fonts.googleapis.com/css2?family=ABeeZee"}]',
                  customLink:
                    '[{"color":"#b01a8b","text-decoration":"Overline","text-decoration-style":"Dotted","text-style":"Italic","font":"Arial"}]',
                  createdAt: '2020-11-30T08:56:56.008Z',
                  updatedAt: '2020-11-30T08:56:56.008Z',
                },
              ],
              nextToken: null,
            },
          },
        };
      });
      render(<StyleManager />);
    });
    test('style manager is rendered', () => {
      expect(screen.getByText('stylemanager.stylemanager')).toBeInTheDocument();
      expect(screen.getByText('stylemanager.customcolors')).toBeInTheDocument();
      expect(screen.getByText('stylemanager.addedcolor')).toBeInTheDocument();
      expect(screen.getByText('stylemanager.customheaders')).toBeInTheDocument();
      expect(screen.getByText('stylemanager.customlink')).toBeInTheDocument();
    });
  });
});
