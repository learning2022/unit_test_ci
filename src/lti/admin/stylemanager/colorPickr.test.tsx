import React from 'react';
import { shallow } from 'enzyme';
import { ColorPicker } from './colorPickr';

describe('#colorPickr', () => {
  describe('when click custom color', () => {
    const colorPickerProps = {
      addedColor: ['#b01a8b'],
      handleAddedColor: jest.fn(),
    };
    test('color picker is rendered', () => {
      const colorPicker = shallow(<ColorPicker {...colorPickerProps} />);
      expect(colorPicker.find('#color-picker')).toHaveLength(1);
    });
  });
});
