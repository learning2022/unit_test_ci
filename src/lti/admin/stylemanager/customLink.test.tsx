import React from 'react';
import { shallow } from 'enzyme';
import { CustomLinkComponent } from './customLink';

describe('#customLink', () => {
  describe('when custom style has result', () => {
    const customLinkProps = {
      customHeaderStyles: {
        id: '5ab18de7-62ca-4623-a44f-56419a3a4c71',
        ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
        customColor: '[{"color":"#1c29b2"}]',
        customHeader:
          '[{"h1":{"size":"66px","font":"Arial"}},{"h2":{"size":"54px","font":"Arial"}},{"h3":{"size":"46px","font":"Arial"}},{"h4":{"size":"34px","font":"SourceSansPro"}},{"h5":{"size":"28px","font":"Arial"}},{"h6":{"size":"22px","font":"Arial"}},{"paragraph":{"size":"20px","font":"SourceSansPro"}}]',
        customFont:
          '[{"fontFamily":"ABeeZee, sans-serif","name":"ABeeZee","url":"https://fonts.googleapis.com/css2?family=ABeeZee"}]',
        customLink:
          '[{"color":"#b01a8b","text-decoration":"Overline","text-decoration-style":"Dotted","text-style":"Italic","font":"Arial"}]',
        createdAt: '2020-11-30T08:56:56.008Z',
        updatedAt: '2020-11-30T08:56:56.008Z',
      },
      handleSave: jest.fn(),
    };
    const customLink = shallow(<CustomLinkComponent {...customLinkProps} />);
    test('custom link is rendered', () => {
      expect(customLink.find('FormControl')).toHaveLength(5);
    });
    describe('when changes link color', () => {
      test('link color is updated', () => {
        customLink
          .find('FormControl')
          .at(0)
          .simulate('change', { target: { value: '#b01a8c' }, type: 'color' });
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'Overline',
            'text-decoration-style': 'Dotted',
            'text-style': 'Italic',
            font: 'Arial',
          },
        ]);
      });
    });
    describe('when changes text decoration', () => {
      test('text decoration is updated', () => {
        customLink
          .find('FormControl')
          .at(1)
          .simulate('change', { target: { value: 'Underline' }, type: 'text-decoration' });
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'Underline',
            'text-decoration-style': 'Dotted',
            'text-style': 'Italic',
            font: 'Arial',
          },
        ]);
      });
    });

    describe('when changes text decoration style', () => {
      test('text decoration style is updated', () => {
        customLink
          .find('FormControl')
          .at(2)
          .simulate('change', { target: { value: 'Dashed' }, type: 'text-decoration-style' });
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'Underline',
            'text-decoration-style': 'Dashed',
            'text-style': 'Italic',
            font: 'Arial',
          },
        ]);
      });
    });

    describe('when changes font family', () => {
      test('font family is updated', () => {
        customLink
          .find('FormControl')
          .at(3)
          .simulate('change', { target: { value: 'Times New Roman' }, type: 'font' });
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'Underline',
            'text-decoration-style': 'Dashed',
            'text-style': 'Italic',
            font: 'Times New Roman',
          },
        ]);
      });
    });

    describe('when changes link text style', () => {
      test('link text style is updated', () => {
        customLink
          .find('FormControl')
          .at(4)
          .simulate('change', { target: { value: 'Bold' }, type: 'text-style' });
        const customLinkStyle = customLink.find('FormControl').at(2);
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'Underline',
            'text-decoration-style': 'Dashed',
            'text-style': 'Bold',
            font: 'Times New Roman',
          },
        ]);
        expect(customLinkStyle.props().disabled).toBeFalsy();
      });
    });

    describe('when click save button', () => {
      test('updated styles were saved', () => {
        customLink.find('Button').at(0).simulate('click');
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'Underline',
            'text-decoration-style': 'Dashed',
            'text-style': 'Bold',
            font: 'Times New Roman',
          },
        ]);
      });
    });

    describe('when changes text decoration to none', () => {
      test('text decoration style is disabled', () => {
        customLink
          .find('FormControl')
          .at(1)
          .simulate('change', { target: { value: 'None' }, type: 'text-decoration' });
        const customLinkStyle = customLink.find('FormControl').at(2);
        expect(customLink.state('customLinkStyle')).toEqual([
          {
            color: '#b01a8b',
            'text-decoration': 'None',
            'text-decoration-style': 'None',
            'text-style': 'Bold',
            font: 'Times New Roman',
          },
        ]);
        expect(customLinkStyle.props().disabled).toBeTruthy();
      });
    });

    describe('when link style is changed and text decoration is none', () => {
      test('text decoration dropdown should be disabled', () => {
        customLink
          .find('FormControl')
          .at(4)
          .simulate('change', { target: { value: 'Bold' }, type: 'text-style' });
        const customLinkStyle = customLink.find('FormControl').at(2);
        expect(customLinkStyle.props().disabled).toBeTruthy();
      });
    });
  });

  describe('when custom style has empty result', () => {
    const customLinkProps = {
      customHeaderStyles: { id: '1', customFont: '', customLink: '' },
      handleSave: jest.fn(),
    };
    const customLink = shallow(<CustomLinkComponent {...customLinkProps} />);
    test('custom link is not rendered', () => {
      expect(customLink.find('FormControl')).toHaveLength(0);
    });
  });
});
