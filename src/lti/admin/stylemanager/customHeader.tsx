import React from 'react';
import Loading from '../../../components/loader/loading';
import { Form, Button, Col, Row } from 'react-bootstrap';
import Error from '../../canvas/components/Error';
import { checkFontFamily } from '../../../loree-editor/utils';

interface CustomHeaderInterface {
  customHeaderStyles: { id: string; customFont: string; customHeader: string };
  handleSave: (input: { id: string; customHeader: string }) => void;
}

interface State {
  loading: boolean;
  customStyleId: string;
  headerStyle: _Any;
  customFontList: { name: string }[];
  error: boolean;
  saveText: string;
}

export class HeaderDropdownComponent extends React.Component<CustomHeaderInterface> {
  state: State = {
    loading: true,
    customStyleId: '',
    headerStyle: [],
    customFontList: [],
    error: false,
    saveText: 'Save',
  };

  handleFontStyleChange = (value: string, idx: number, headerType: string, styleType: string) => {
    const updateFont: Array<_Any> = this.state.headerStyle;
    updateFont[idx][headerType][styleType] = value;
    this.setState({
      headerStyle: updateFont,
    });
  };

  handleCustomHeaderSave = () => {
    this.setState({ saveText: 'Saving...' });
    const userInput = {
      id: this.state.customStyleId,
      customHeader: JSON.stringify(this.state.headerStyle),
    };
    this.props.handleSave(userInput);
    this.setState({ saveText: 'Save' });
  };

  async componentDidMount() {
    try {
      const customStyleData = this.props.customHeaderStyles;
      if (customStyleData) {
        let fontList = JSON.parse(customStyleData.customFont);
        fontList = fontList.sort((a: { name: string }, b: { name: string }) =>
          a.name.localeCompare(b.name),
        );
        const headerList = JSON.parse(customStyleData.customHeader);
        checkFontFamily(headerList, fontList, 'admin');
        this.setState({
          loading: false,
          customStyleId: customStyleData.id,
          headerStyle: headerList,
          customFontList: fontList,
        });
      } else {
        this.setState({
          loading: false,
          headerStyle: [],
        });
      }
    } catch (e) {
      this.setState({
        loading: false,
        error: true,
      });
    }
  }

  render() {
    const { headerStyle } = this.state;
    const headerManager = [
      {
        key: 'h1',
        label: 'Header 1',
      },
      {
        key: 'h2',
        label: 'Header 2',
      },
      {
        key: 'h3',
        label: 'Header 3',
      },
      {
        key: 'h4',
        label: 'Header 4',
      },
      {
        key: 'h5',
        label: 'Header 5',
      },
      {
        key: 'h6',
        label: 'Header 6',
      },
      {
        key: 'paragraph',
        label: 'Paragraph',
      },
    ];

    const getFontWeightList = () => {
      const fontSizeList: { value: string }[] = [];
      for (let i = 2; i <= 70; i += 2) {
        fontSizeList.push({ value: `${i}px` });
      }
      return fontSizeList;
    };

    const fontweightDropdownList = getFontWeightList();
    if (this.state.loading) {
      return <Loading />;
    }
    if (this.state.error) {
      return <Error />;
    }
    return (
      <>
        {headerManager.map((val, idx) => {
          return (
            <React.Fragment key={idx}>
              <Form as={Row}>
                <Form.Label column sm='2'>
                  {val.label}
                </Form.Label>
                <Col sm='3'>
                  <Form.Group>
                    <Form.Control
                      as='select'
                      defaultValue={this.state.headerStyle[idx][val.key].font}
                      onChange={(event) =>
                        this.handleFontStyleChange(event.target.value, idx, val.key, 'font')
                      }
                    >
                      <option value=''>Select Font</option>
                      {this.state.customFontList.map((value: { name: string }, index: number) => {
                        return (
                          <option key={index} value={`${value.name}`}>{`${value.name}`}</option>
                        );
                      })}
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col sm='2'>
                  <Form.Group>
                    <Form.Control
                      as='select'
                      defaultValue={this.state.headerStyle[idx][val.key].size}
                      onChange={(event) =>
                        this.handleFontStyleChange(event.target.value, idx, val.key, 'size')
                      }
                    >
                      {fontweightDropdownList.map((value: { value: string }, index: number) => {
                        return (
                          <option key={index} value={`${value.value}`}>{`${value.value}`}</option>
                        );
                      })}
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col sm='5' className=''>
                  <div
                    className='text-center'
                    style={{
                      padding: '5px',
                      border: '1px solid black',
                      fontFamily: `${
                        headerStyle[idx][val.key].font ? headerStyle[idx][val.key].font : ''
                      }`,
                      fontSize: `${headerStyle[idx][val.key].size}`,
                    }}
                  >
                    {val.label}
                  </div>
                  <h6 className='text-center mt-2'>Preview</h6>
                </Col>
              </Form>
              <hr />
            </React.Fragment>
          );
        })}
        <div className='style-manager-button-wrapper-section d-flex justify-content-center mt-5'>
          <Button className='add-button' size='sm' onClick={this.handleCustomHeaderSave}>
            {this.state.saveText}
          </Button>
        </div>
      </>
    );
  }
}
