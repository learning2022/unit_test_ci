/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
import React, { useState, useEffect } from 'react';
import { Accordion, Card, Button, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ColorPicker } from './colorPickr';
import { HeaderDropdownComponent } from './customHeader';
import { CustomLinkComponent } from './customLink';
import { faTimes, faCaretRight, faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { listCustomStyles } from '../../../graphql/queries';
import { API, graphqlOperation } from 'aws-amplify';
import { updateCustomStyle } from '../../../graphql/mutations';
import Loading from '../../../components/loader/loading';
import ToastComponent from '../../components/ToastComponent';
import './stylemanager.scss';
import '../adminDashboard.scss';
import { translate } from '../../../i18n/translate';

export const StyleManager = () => {
  const [addedColor, setAddedColor]: any = useState(null);
  const [loading, setLoading] = useState(true);
  const [saveText, setSaveText] = useState('Save');
  const [id, setId] = useState('');
  const [customStyleList, setCustomStyleList]: _Any = useState(null);
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState('');
  const [activeAccordion, setActiveAccordion]: any = useState(0);

  useEffect(() => {
    (async function () {
      try {
        const list: any = await API.graphql(
          graphqlOperation(listCustomStyles, {
            filter: {
              ltiPlatformID: {
                eq: sessionStorage.getItem('ltiPlatformId'),
              },
            },
          }),
        );
        setId(list.data.listCustomStyles.items[0].id);
        setCustomStyleList(list.data.listCustomStyles.items[0]);
        updateState(list.data.listCustomStyles.items[0].customColor);
      } catch (error) {
        console.log('Error in fetching custom styles', error);
      }
    })();
  }, []);

  const handleStyleManagerSave = async (userInput: any) => {
    await API.graphql(graphqlOperation(updateCustomStyle, { input: userInput }));
    setShowToast(true);
    setToastMessage(translate('global.savesuccess'));
  };

  const updateState = (colorList: any) => {
    const parsedCustomColor = JSON.parse(colorList);
    const userColors: any = [];
    parsedCustomColor.forEach((val: any) => {
      userColors.push(val.color);
    });
    if (colorList) {
      setAddedColor(userColors);
      setLoading(false);
    }
  };

  const handleAddedColor = (data: string[], hexColor: string) => {
    setLoading(true);
    if (addedColor!.indexOf(hexColor) === -1) {
      setAddedColor(data);
    }
    setLoading(false);
  };

  const handleDelete = async (e: React.MouseEvent<HTMLSpanElement, MouseEvent>, idx: React.Key) => {
    const deletedSwatch = addedColor;
    deletedSwatch.splice(idx, 1);
    setLoading(true);
    await setAddedColor(deletedSwatch);
    setLoading(false);
  };

  const closeToast = () => {
    setShowToast(false);
  };

  const handleCustomColorSave = async () => {
    setSaveText(translate('global.saving'));
    const userColor: any = [];
    addedColor.forEach((element: string) => {
      const colorObj = { color: element };
      userColor.push(colorObj);
    });
    const userInput = {
      id: id,
      customColor: JSON.stringify(userColor),
    };
    await handleStyleManagerSave(userInput);
    setSaveText(translate('global.save'));
  };

  const handleActiveAccordion = (index: number) => {
    const active = activeAccordion === index ? '' : index;
    setActiveAccordion(active);
  };

  return (
    <React.Fragment>
      <div className='text-primary mb-3'>
        <h3>{translate('stylemanager.stylemanager')}</h3>
      </div>
      <div>
        {loading ? (
          <Loading />
        ) : (
          <React.Fragment>
            <Accordion defaultActiveKey='0' className='mt-3 admin-accordion'>
              <Card className={`${activeAccordion === 0 ? 'active' : ''}`}>
                <Accordion.Toggle as={Card.Header} eventKey='0' onClick={() => handleActiveAccordion(0)} className=''>
                  {translate('stylemanager.customcolors')}
                  <span className='right-actions'>
                    <FontAwesomeIcon icon={activeAccordion === 0 ? faCaretDown : faCaretRight} />
                  </span>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey='0'>
                  <React.Fragment>
                    <Card.Body>
                      <Row>
                        <Col sm={2}>{translate('stylemanager.addcustomcolor')}</Col>
                        <Col sm={10}>
                          <ColorPicker addedColor={addedColor} handleAddedColor={handleAddedColor} />
                        </Col>
                      </Row>
                      <h6 className='mt-5'>{translate('stylemanager.addedcolor')}</h6>
                      <Row>
                        {addedColor.map((color: string, idx: any) => (
                          <Col sm={2} key={idx} className='card-color-view'>
                            <div className='d-flex flex-row-reverse '>
                              <Button variant='link' onClick={e => handleDelete(e, idx)} className='btn-delete'>
                                <FontAwesomeIcon icon={faTimes} />
                              </Button>
                            </div>
                            <Row>
                              <Col xs={3}>
                                <div
                                  style={{
                                    backgroundColor: `${color}`,
                                    padding: '5px',
                                    height: 25,
                                    width: 25,
                                    borderWidth: '1px',
                                    borderStyle: 'solid',
                                    borderColor: '#e2e2e2',
                                    borderRadius: '4px',
                                  }}
                                ></div>
                              </Col>
                              <Col xs={9}>
                                <span className='text-uppercase'>{color}</span>
                              </Col>
                            </Row>
                          </Col>
                        ))}
                      </Row>
                    </Card.Body>
                    <Card.Footer className='bg-white text-center'>
                      <Button className='ml-2' size='sm' onClick={handleCustomColorSave}>
                        {saveText}
                      </Button>
                    </Card.Footer>
                  </React.Fragment>
                </Accordion.Collapse>
              </Card>

              <Card className={`${activeAccordion === 1 ? 'active' : ''}`}>
                <Accordion.Toggle as={Card.Header} onClick={() => handleActiveAccordion(1)} eventKey='1'>
                  {translate('stylemanager.customheaders')}
                  <span className='right-actions'>
                    <FontAwesomeIcon icon={activeAccordion === 1 ? faCaretDown : faCaretRight} />
                  </span>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey='1'>
                  <Card.Body>
                    <HeaderDropdownComponent customHeaderStyles={customStyleList} handleSave={handleStyleManagerSave} />
                  </Card.Body>
                </Accordion.Collapse>
              </Card>

              <Card className={`${activeAccordion === 2 ? 'active' : ''}`}>
                <Accordion.Toggle as={Card.Header} onClick={() => handleActiveAccordion(2)} eventKey='2'>
                  {translate('stylemanager.customlink')}
                  <span className='right-actions'>
                    <FontAwesomeIcon icon={activeAccordion === 2 ? faCaretDown : faCaretRight} />
                  </span>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey='2'>
                  <Card.Body>
                    <CustomLinkComponent customHeaderStyles={customStyleList} handleSave={handleStyleManagerSave} />
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
            {showToast && <ToastComponent showToast={showToast} toastMessage={toastMessage} closeToast={closeToast} />}
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
};
