export const mockHeaderList = [
  { h1: { font: 'Arial', size: '66px' } },
  { h2: { font: '', size: '54px' } },
  { h3: { font: '', size: '46px' } },
  { h4: { font: '', size: '34px' } },
  { h5: { font: '', size: '28px' } },
  { h6: { font: '', size: '22px' } },
  { paragraph: { font: '', size: '20px' } },
];

export const changeHeaderFontSize = [
  { h1: { font: 'Arial', size: '20px' } },
  { h2: { font: '', size: '54px' } },
  { h3: { font: '', size: '46px' } },
  { h4: { font: '', size: '34px' } },
  { h5: { font: '', size: '28px' } },
  { h6: { font: '', size: '22px' } },
  { paragraph: { font: '', size: '20px' } },
];

export const changeHeaderFontFamily = [
  { h1: { font: 'Times New Roman', size: '66px' } },
  { h2: { font: '', size: '54px' } },
  { h3: { font: '', size: '46px' } },
  { h4: { font: '', size: '34px' } },
  { h5: { font: '', size: '28px' } },
  { h6: { font: '', size: '22px' } },
  { paragraph: { font: '', size: '20px' } },
];
