import React, { Fragment, useEffect } from 'react';
import Pickr from '@simonwep/pickr';
import '@simonwep/pickr/dist/themes/classic.min.css';

interface ColorPickerProps {
  addedColor?: string[];
  handleAddedColor: (data: string[], color: string) => void;
  linkColor?: string;
}

export const ColorPicker = (props: ColorPickerProps) => {
  useEffect(() => {
    const picker = Pickr.create({
      el: '.color-picker',
      theme: 'nano',
      default: props.linkColor ? props.linkColor : '#42445a',
      defaultRepresentation: 'HEXA',
      components: {
        preview: true,
        opacity: false,
        hue: true,
        interaction: {
          hsva: false,
          hsla: false,
          hex: true,
          rgba: false,
          input: true,
          clear: false,
          cancel: true,
          save: true,
        },
      },
      i18n: {
        'btn:save': 'Apply',
        'btn:cancel': 'Cancel',
      },
    });
    picker.on('save', (color: Pickr.HSVaColor) => {
      const hexColor = `#${color.toHEXA().join('')}`;
      if (props.addedColor) props.handleAddedColor([...props.addedColor, hexColor], hexColor);
      if (picker.getColor()) picker.hide();
    });
    picker.on('cancel', () => {
      picker.hide();
    });
    return () => {
      picker.destroyAndRemove();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.addedColor]);
  return (
    <>
      <div id='color-picker' className='color-picker d-inline-flex' />
    </>
  );
};
