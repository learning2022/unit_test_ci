import React from 'react';
import { Form, Button, Row, Col, InputGroup } from 'react-bootstrap';
import { ColorPicker } from './colorPickr';
import Loading from '../../../components/loader/loading';
import Error from '../../canvas/components/Error';
interface ColorPickerProps {
  customHeaderStyles: { id: string; customFont: string; customLink: string };
  handleSave: (input: { id: string; customLink: string }) => void;
}

interface State {
  loading: boolean;
  error: boolean;
  customStyleId: string;
  customLinkStyle: Array<_Any>;
  customFontList: { name: string }[];
  customLinkTextColor: string;
  saveText: string;
  addedLinkColor: string[];
  disableCustomLinkStyle: boolean;
}

export class CustomLinkComponent extends React.Component<ColorPickerProps> {
  state: State = {
    loading: true,
    error: false,
    customStyleId: '',
    customLinkStyle: [],
    customFontList: [],
    customLinkTextColor: '',
    saveText: 'Save',
    addedLinkColor: [],
    disableCustomLinkStyle: false,
  };

  handleCustomLinkSave = () => {
    this.setState({ saveText: 'Saving...' });
    const userInput = {
      id: this.state.customStyleId,
      customLink: JSON.stringify(this.state.customLinkStyle),
    };
    this.props.handleSave(userInput);
    this.setState({ saveText: 'Save' });
  };

  handleCustomLinkStyleChange = (value: string, type: string) => {
    const updateCustomLinkStyle: Array<_Any> = this.state.customLinkStyle;
    if (type === 'color') updateCustomLinkStyle[0][type] = this.state.customLinkTextColor;
    else updateCustomLinkStyle[0][type] = value;

    if (type === 'text-decoration') {
      const customLinkStyleState = value === 'None';
      if (customLinkStyleState) updateCustomLinkStyle[0]['text-decoration-style'] = value;
      this.setState({
        disableCustomLinkStyle: customLinkStyleState,
      });
    }

    this.setState({
      customLinkStyle: updateCustomLinkStyle,
    });
  };

  handleAddedLinkColor = (data: string[], hex: string) => {
    this.setState({ customLinkTextColor: hex });
    this.handleCustomLinkStyleChange('', 'color');
  };

  async componentDidMount() {
    try {
      const customStyleData = this.props.customHeaderStyles;
      if (customStyleData) {
        let fontList = JSON.parse(customStyleData.customFont);
        fontList = fontList.sort((a: { name: string }, b: { name: string }) =>
          a.name.localeCompare(b.name),
        );
        const customLinkStyleValue = JSON.parse(customStyleData.customLink)[0]['text-decoration'];
        this.setState({
          loading: false,
          customStyleId: customStyleData.id,
          customLinkTextColor: JSON.parse(customStyleData.customLink)[0].color,
          customLinkStyle: JSON.parse(customStyleData.customLink),
          customFontList: fontList,
          disableCustomLinkStyle: customLinkStyleValue === 'None',
        });
      } else {
        this.setState({
          loading: false,
          customLinkStyle: [],
        });
      }
    } catch (e) {
      this.setState({
        loading: false,
        error: true,
      });
    }
  }

  render() {
    const textDecorationList = [{ value: 'None' }, { value: 'Underline' }];
    const textDecorationStyleList = [
      { value: 'Solid' },
      { value: 'Dashed' },
      { value: 'Dotted' },
      { value: 'Double' },
      { value: 'Initial' },
      { value: 'Wavy' },
    ];
    const textLinkStyle = [{ value: 'Bold' }, { value: 'Italic' }, { value: 'None' }];
    if (this.state.loading) {
      return <Loading />;
    }
    if (this.state.error) {
      return <Error />;
    }
    return (
      <>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group as={Row}>
                <Form.Label column sm='4'>
                  Link Color
                </Form.Label>
                <Col sm='6'>
                  <InputGroup className='mb-3 custom-links-color-pickr'>
                    <Form.Control
                      value={this.state.customLinkTextColor}
                      onChange={(event) =>
                        this.handleCustomLinkStyleChange(event.target.value, 'color')
                      }
                    />
                    <InputGroup.Append>
                      <ColorPicker
                        addedColor={this.state.addedLinkColor}
                        handleAddedColor={this.handleAddedLinkColor}
                        linkColor={this.state.customLinkTextColor}
                      />
                    </InputGroup.Append>
                  </InputGroup>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm='4'>
                  {' '}
                  Text Decoration{' '}
                </Form.Label>
                <Col sm='6'>
                  <Form.Control
                    as='select'
                    defaultValue={this.state.customLinkStyle[0]['text-decoration']}
                    onChange={(event) =>
                      this.handleCustomLinkStyleChange(event.target.value, 'text-decoration')
                    }
                  >
                    {textDecorationList.map((value: { value: string }, index: number) => {
                      return (
                        <option key={index} value={`${value.value}`}>{`${value.value}`}</option>
                      );
                    })}
                  </Form.Control>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm='4'>
                  {' '}
                  Text Decoration Style{' '}
                </Form.Label>
                <Col sm='6'>
                  <Form.Control
                    as='select'
                    value={this.state.customLinkStyle[0]['text-decoration-style']}
                    disabled={this.state.disableCustomLinkStyle}
                    defaultValue={this.state.customLinkStyle[0]['text-decoration-style']}
                    onChange={(event) =>
                      this.handleCustomLinkStyleChange(event.target.value, 'text-decoration-style')
                    }
                  >
                    {textDecorationStyleList.map((value: { value: string }, index: number) => {
                      return (
                        <option key={index} value={`${value.value}`}>{`${value.value}`}</option>
                      );
                    })}
                  </Form.Control>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm='4'>
                  {' '}
                  Font Family{' '}
                </Form.Label>
                <Col sm='6'>
                  <Form.Control
                    as='select'
                    defaultValue={this.state.customLinkStyle[0].font}
                    onChange={(event) =>
                      this.handleCustomLinkStyleChange(event.target.value, 'font')
                    }
                  >
                    <option value=''>Select Font</option>
                    {this.state.customFontList.map((value: { name: string }, index: number) => {
                      return <option key={index} value={`${value.name}`}>{`${value.name}`}</option>;
                    })}
                  </Form.Control>
                </Col>
              </Form.Group>
              <Form.Group as={Row}>
                <Form.Label column sm='4'>
                  {' '}
                  Link Text Style{' '}
                </Form.Label>
                <Col sm='6'>
                  <Form.Control
                    as='select'
                    defaultValue={
                      this.state.customLinkStyle[0]['text-style']
                        ? this.state.customLinkStyle[0]['text-style']
                        : 'Bold'
                    }
                    onChange={(event) =>
                      this.handleCustomLinkStyleChange(event.target.value, 'text-style')
                    }
                  >
                    {textLinkStyle.map((value: { value: string }, index: number) => {
                      return (
                        <option key={index} value={`${value.value}`}>{`${value.value}`}</option>
                      );
                    })}
                  </Form.Control>
                </Col>
              </Form.Group>
            </Form>
          </Col>
          <Col md={4}>
            <div style={{ border: '1px solid black', padding: '20px' }}>
              <h5
                className='text-center'
                style={{
                  color: this.state.customLinkStyle[0].color,
                  textDecoration: this.state.customLinkStyle[0]['text-decoration'],
                  textDecorationStyle: this.state.customLinkStyle[0]['text-decoration-style'],
                  fontStyle:
                    this.state.customLinkStyle[0]['text-style'] === 'Italic' ? 'italic' : '',
                  fontFamily: this.state.customLinkStyle[0].font,
                  fontWeight:
                    this.state.customLinkStyle[0]['text-style'] === 'Bold'
                      ? this.state.customLinkStyle[0]['text-style']
                      : 'normal',
                }}
              >
                Custom link
              </h5>
            </div>
            <h6 className='text-center mt-2'>Preview</h6>
          </Col>
        </Row>
        <div className='style-manager-button-wrapper-section d-flex justify-content-center'>
          <Button className='add-button' size='sm' onClick={this.handleCustomLinkSave}>
            {this.state.saveText}
          </Button>
        </div>
      </>
    );
  }
}
