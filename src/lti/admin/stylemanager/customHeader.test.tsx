import React from 'react';
import { shallow } from 'enzyme';
import { HeaderDropdownComponent } from './customHeader';
import { changeHeaderFontFamily, changeHeaderFontSize, mockHeaderList } from './mockData';

describe('#customHeader', () => {
  describe('when custom header has result', () => {
    const customHeaderProps = {
      customHeaderStyles: {
        id: '5ab18de7-62ca-4623-a44f-56419a3a4c71',
        ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
        customColor: '[{"color":"#1c29b2"}]',
        customHeader:
          '[{"h1":{"size":"66px","font":"Arial"}},{"h2":{"size":"54px","font":"Arial"}},{"h3":{"size":"46px","font":"Arial"}},{"h4":{"size":"34px","font":"SourceSansPro"}},{"h5":{"size":"28px","font":"Arial"}},{"h6":{"size":"22px","font":"Arial"}},{"paragraph":{"size":"20px","font":"SourceSansPro"}}]',
        customFont:
          '[{"fontFamily":"ABeeZee, sans-serif","name":"ABeeZee","url":"https://fonts.googleapis.com/css2?family=ABeeZee"}]',
        customLink:
          '[{"color":"#b01a8b","text-decoration":"Overline","text-decoration-style":"Dotted","text-style":"Italic","font":"Arial"}]',
        createdAt: '2020-11-30T08:56:56.008Z',
        updatedAt: '2020-11-30T08:56:56.008Z',
      },
      handleSave: jest.fn(),
    };
    const customHeader = shallow(<HeaderDropdownComponent {...customHeaderProps} />);
    test('custom header is rendered', () => {
      expect(customHeader.find('Form')).toHaveLength(7);
    });
    describe('when changes font family', () => {
      test('font family is updated', () => {
        customHeader
          .find('FormControl')
          .at(0)
          .simulate('change', {
            target: { value: 'Arial' },
            index: '0',
            headerType: 'h1',
            type: 'font',
          });
        expect(customHeader.state('headerStyle')).toEqual(mockHeaderList);
      });
    });
    describe('when changes font size', () => {
      test('font size is updated', () => {
        customHeader
          .find('FormControl')
          .at(1)
          .simulate('change', {
            target: { value: '20px' },
            index: '0',
            headerType: 'h1',
            type: 'size',
          });
        expect(customHeader.state('headerStyle')).toEqual(changeHeaderFontSize);
      });
    });
    describe('when click save button', () => {
      test('updated styles saved', () => {
        customHeader.find('Button').at(0).simulate('click');
        expect(customHeader.state('headerStyle')).toEqual(changeHeaderFontSize);
      });
    });

    describe('when uncheck the font from fonts section, then set default font family to the style manager', () => {
      const customHeader = shallow(<HeaderDropdownComponent {...customHeaderProps} />);
      test('check dropdown fonts', () => {
        customHeader
          .find('FormControl')
          .at(0)
          .simulate('change', {
            target: { value: 'Times New Roman' },
            index: '0',
            headerType: 'h1',
            type: 'font',
          });
        expect(customHeader.state('headerStyle')).toEqual(changeHeaderFontFamily);
      });
    });
  });
  describe('when custom header result is empty', () => {
    const customHeaderProps = {
      customHeaderStyles: { id: '1', customFont: '', customHeader: '' },
      handleSave: jest.fn(),
    };
    const customHeader = shallow(<HeaderDropdownComponent {...customHeaderProps} />);
    test('custom header is not rendered', () => {
      expect(customHeader.find('Form')).toHaveLength(0);
    });
  });
});
