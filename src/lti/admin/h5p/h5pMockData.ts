/* eslint-disable */ // Remove this line when editing this file
export const h5pMockData = {
  h5pData: {
    data: {
      getAdminH5PContent: `{
        "body":{
          "status":1,
          "message":"Data fetched", "h5pList":[{"id": "164","h5p_type": "Image Hotspots","title": "National Quality Framework - hotspot","created_user": "66","created_by": "Andiswamy Rajagopal","account_id": "18","created_at": "2021-06-02 13:51:18","updated_at": "2021-06-02 13:51:18"}]
        }
      }`,
    },
  },
  tableHeadings: ['', 'Title', 'Name', 'H5P Type', 'Department', 'Updated At'],
};
