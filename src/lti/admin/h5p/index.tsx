/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment } from 'react';
import { Table, Accordion, Card, Button, Dropdown, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShareAlt, faGlobeAmericas } from '@fortawesome/free-solid-svg-icons';
import { API, graphqlOperation } from 'aws-amplify';
import moment from 'moment';
import { getAdminH5PContent } from '../../../../src/graphql/queries';
import Loading from '../../../components/loader/loading';
import { ReactComponent as EllipsisIcon } from '../../../assets/Icons/ellipsis_normal.svg';

class H5pInteractives extends React.Component {
  state = {
    isLoadingH5p: true,
    h5pContents: [],
  };
  componentDidMount = async () => {
    const h5pContents: any = await API.graphql(
      graphqlOperation(getAdminH5PContent, {
        platformId: sessionStorage.getItem('ltiPlatformId'),
      }),
    );
    const parsedH5pData = JSON.parse(h5pContents.data.getAdminH5PContent);
    this.setState({
      h5pContents: parsedH5pData.body.h5pList,
      isLoadingH5p: false,
    });
  };

  handleToggleAccordionIcon = (e: any) => {
    const collapsed = e.target.parentNode.childNodes[1].className === 'collapse';
    !collapsed
      ? (e.target.className = e.target.className.replace('icon-down-arrow', 'icon-right-arrow'))
      : (e.target.className = e.target.className.replace('icon-right-arrow', 'icon-down-arrow'));
  };

  render() {
    return (
      <Fragment>
        <h3 className='text-primary'>H5P Interactives</h3>
        <div className='admin-h5p-wrapper'>
          <div className='h5p-tab-section'>
            {this.state.isLoadingH5p ? (
              <Loading />
            ) : (this.state.h5pContents as Array<any>).length > 0 ? (
              <Table responsive borderless>
                <thead className='h5p-table-head'>
                  <tr>
                    <th>
                      <Form.Check />
                    </th>
                    <th>Title</th>
                    <th>Name</th>
                    <th>H5P Type</th>
                    <th>Department</th>
                    <th>Updated At</th>
                  </tr>
                </thead>
                <tbody>
                  {(this.state.h5pContents as Array<any>).map((h5p: any, index: any) => (
                    <tr key={index} className='h5p-admin-table'>
                      <td>
                        <Form.Check />
                      </td>
                      <td>
                        <Accordion>
                          <Card>
                            <Accordion.Toggle
                              as={Card}
                              eventKey='0'
                              className='icon-right-arrow text-decoration-none'
                              onClick={e => this.handleToggleAccordionIcon(e)}
                            >
                              {h5p.title}
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey='0'>
                              <Card.Body>
                                <Button variant='success' className='m-1' size='sm'>
                                  Meta data1
                                </Button>
                                <Button variant='secondary' className='m-1' size='sm'>
                                  Meta data2
                                </Button>
                              </Card.Body>
                            </Accordion.Collapse>
                          </Card>
                        </Accordion>
                      </td>
                      <td>
                        <p className='h5pTextWrap'>{h5p.created_by}</p>
                      </td>
                      <td>{h5p.h5p_type}</td>
                      <td>{h5p.account_id}</td>
                      <td>{moment(h5p.updated_at).format('ddd, ll')}</td>
                      <td>
                        <FontAwesomeIcon icon={faShareAlt} test-id='share-icon' color='gray' />
                      </td>
                      <td>
                        <FontAwesomeIcon icon={faGlobeAmericas} test-id='global-icon' color='gray' />
                      </td>
                      <td>
                        <Dropdown drop='down' alignRight className='edit-meta-data-icon' style={{ padding: '0px !important' }}>
                          <Dropdown.Toggle id='dropdown-menu-align-left' className='noPadding'>
                            <EllipsisIcon width={3} height={14} />
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            <Dropdown.Item>Edit Meta data</Dropdown.Item>
                            <Dropdown.Item>Delete</Dropdown.Item>
                            <Dropdown.Item>Lock</Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            ) : (
              <div className='container'>
                <div className='p-3'>No H5P Interactives found</div>
              </div>
            )}
          </div>
          <Button className='m-2'>Global</Button>
          <Button>Share</Button>
        </div>
      </Fragment>
    );
  }
}

export default H5pInteractives;
