/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import API from '@aws-amplify/api';
import { shallow } from 'enzyme';
import H5pInteractives from './index';
import { act } from 'react-dom/test-utils';
import { h5pMockData } from './h5pMockData';

const waitForComponentToUpdate = async (wrapper: any) => {
  await act(async () => {
    await new Promise(resolve => setTimeout(resolve, 0));
    wrapper.update();
  });
};

describe('Given a user is in Loree Admin Dashbard ', () => {
  describe('When they are in H5P page', () => {
    let h5pInteractives: any;
    beforeAll(async () => {
      API.graphql = jest.fn().mockReturnValueOnce(h5pMockData.h5pData);
      h5pInteractives = shallow(<H5pInteractives />);
      await waitForComponentToUpdate(h5pInteractives);
    });
    test('It should render the list of H5P components', () => {
      expect(h5pInteractives.find('h3').text()).toEqual('H5P Interactives');
      expect(h5pInteractives.find('.admin-h5p-wrapper')).not.toBeNull();
    });
    test('should have one primary child under h5p wrapper', () => {
      const h5pTableSection = h5pInteractives.find('.admin-h5p-wrapper');
      expect(h5pTableSection.children().length).toBe(3);
      expect(h5pTableSection.childAt(0)).not.toBeNull();
    });
    test('should contain table headers elements', () => {
      const h5pTableSection = h5pInteractives.find('.admin-h5p-wrapper').childAt(0);
      expect(h5pTableSection.find('thead').find('th').length).toBe(6);
      h5pMockData.tableHeadings.forEach((head: any, index: number) => {
        expect(h5pTableSection.find('thead').find('th').at(index).text()).toEqual(head);
      });
    });
    test('should contain tabble row h5p elements', () => {
      const tableRowElements = h5pInteractives.find('.h5p-admin-table');
      expect(tableRowElements.find('td').length).toBe(9);
      //First element as Accordian
      expect(tableRowElements.find('td').at(1).children().type().displayName).toEqual('Accordion');
      expect(tableRowElements.find('.icon-right-arrow').text()).toEqual('National Quality Framework - hotspot');

      expect(tableRowElements.find('td').at(2).children().text()).toEqual('Andiswamy Rajagopal');
      expect(tableRowElements.find('td').at(3).children().text()).toEqual('Image Hotspots');
      expect(tableRowElements.find('td').at(4).children().text()).toEqual('18');
      expect(tableRowElements.find('td').at(5).children().text()).toEqual('Wed, Jun 2, 2021');
      expect(tableRowElements.find('td').at(6).children().at(0).props()['test-id']).toEqual('share-icon');
      expect(tableRowElements.find('td').at(7).children().at(0).props()['test-id']).toEqual('global-icon');
      // last element as dropdown
      expect(tableRowElements.find('td').at(8).children().type().displayName).toEqual('Dropdown');
      expect(tableRowElements.find('DropdownItem').length).toBe(3);
      expect(tableRowElements.find('DropdownItem').at(0).text()).toEqual('Edit Meta data');
      expect(tableRowElements.find('DropdownItem').at(1).text()).toEqual('Delete');
      expect(tableRowElements.find('DropdownItem').at(2).text()).toEqual('Lock');
    });
  });
});
