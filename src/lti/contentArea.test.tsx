import { render } from '@testing-library/react';
import React from 'react';
import { getElementsByClassName } from '../loree-editor/common/dom';
import ContentArea from './blackBoard/landingPage/contentArea';
describe('rendering content area', () => {
  const samplePropData = [{ title: 'sample title', contentHandler: { isBbPage: true } }];
  const onShowAddContentAreaModal = jest.fn();
  const setActiveArea = jest.fn();

  beforeAll(() => {
    render(
      <ContentArea
        content={samplePropData}
        active={{ activeArea: 'sample title', setActiveArea }}
        handleModalSettings={onShowAddContentAreaModal}
        isModule
      />,
    );
  });
  test('displaying help icon', () => {
    expect(getElementsByClassName('helpDropdown')[0]).toBeTruthy();
  });
});
