import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import { Auth } from 'aws-amplify';
import History from 'history';
import Loading from '../components/loader/loading';
import '../components/loader/loading.scss';
import { lmsUrlRedirection } from '../lmsConfig';
import { RouteComponentProps } from 'react-router-dom';
import { translate } from '../i18n/translate';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const jwt = require('jsonwebtoken');

interface StateVal {
  match: {
    path: '';
    params: {
      id_token: string;
      course_id: string;
    };
  };
  location: History.Location;
  history: RouteComponentProps['history'];
}

class LtiLogin extends Component<StateVal> {
  async componentDidMount() {
    const userQuery = new URLSearchParams(this.props.location.search);
    if (userQuery.get('token') !== null) {
      try {
        const decodeToken = jwt.verify(userQuery.get('token'), userQuery.get('publicKey'));
        sessionStorage.setItem('course_id', decodeToken.course_id);
        sessionStorage.setItem('courseLabel', decodeToken.course_label);
        Auth.signIn(decodeToken.userCogId, decodeToken.userToken)
          .then((user) => {
            sessionStorage.setItem('lmsUrl', decodeToken.lmsApiUrl);
            sessionStorage.setItem('lmsEmail', decodeToken.lms_email); // For H5p user register(temporary update), will remove this field from session storage, once details are updated.
            sessionStorage.setItem('ltiPlatformId', decodeToken.platform_id);
            sessionStorage.setItem('isAdmin', decodeToken.roleAdmin);
            sessionStorage.setItem('trackingId', decodeToken.trackingId);
            sessionStorage.setItem('envName', decodeToken.envName);
            sessionStorage.setItem('domainName', decodeToken.domainName);
            localStorage.setItem(
              'ReactAmplify.TokenKey',
              user.signInUserSession.accessToken.jwtToken,
            );
            this.props.history.push({
              pathname: lmsUrlRedirection(),
            });
          })
          .catch((error) => {
            console.log('Error in LTI login', error);
            this.props.history.push({
              pathname: '/lti/unauth',
            });
          });
      } catch (err) {
        this.props.history.push({
          pathname: '/lti/unauth',
        });
      }
    } else {
      this.props.history.push({
        pathname: '/lti/unauth',
      });
    }
  }

  render() {
    return (
      <>
        <Container>
          <Loading />
          <p className='loaderViewText mt-2'>{translate('landing.holdontoyourhats')}</p>
        </Container>
      </>
    );
  }
}

export default LtiLogin;
