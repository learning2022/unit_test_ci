import React from 'react';
import { fireEvent, render, RenderResult, screen, waitFor } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import axe from '../axeHelper';
import HelpSection from './helpSection';
import Space from '../loree-editor/modules/space/space';
jest.mock('../loree-editor/modules/space/space.ts', () => {
  return jest.fn().mockImplementation(() => {
    return { getHelpLink: jest.fn(), helpLink: jest.fn() };
  });
});
describe('Help section', () => {
  sessionStorage.setItem('domainName', 'BB');
  let helpSection: RenderResult;
  let spaceInstance: Space;
  beforeEach(() => {
    spaceInstance = new Space();
    helpSection = render(
      <BrowserRouter>
        <HelpSection space={spaceInstance} />
      </BrowserRouter>,
    );
  });
  test('should pass accessibility tests', async () => {
    const results = await axe(helpSection.container);
    expect(results).toHaveNoViolations();
  });
  test('click user guide', async () => {
    const helpButton = helpSection.getByRole('button', { name: 'Help' });
    userEvent.click(helpButton);
    await waitFor(() => userEvent.click(screen.getByRole('menuitem', { name: 'User guide' })));
    expect(spaceInstance.helpLink).toHaveBeenCalled();
  });
  test('calls onClick for loree support', async () => {
    const helpButton = helpSection.getByRole('button', { name: 'Help' });
    userEvent.click(helpButton);
    await waitFor(() => userEvent.click(screen.getByRole('menuitem', { name: 'Loree support' })));
    expect(spaceInstance.helpLink).toHaveBeenCalled();
  });
  // TODO: mock lmsConfig to make 'What's new' links display
  test('calls onClick for whats new', async () => {
    const helpButton = helpSection.getByRole('button', { name: 'Help' });
    userEvent.click(helpButton);
    await waitFor(() =>
      userEvent.click(helpSection.getAllByRole('menuitem', { name: `What's new` })[0]),
    );
    expect(spaceInstance.helpLink).toHaveBeenCalled();
  });
  test('right click to open in a new tab', async () => {
    const helpButton = helpSection.getByRole('button', { name: 'Help' });
    userEvent.click(helpButton);
    await waitFor(() =>
      fireEvent.contextMenu(screen.getByRole('menuitem', { name: 'Loree support' })),
    );
    await waitFor(() => {
      expect(spaceInstance.getHelpLink).toHaveBeenCalled();
    });
  });
  test.each([
    {
      domainName: 'BB',
    },
    {
      domainName: 'canvas',
    },
  ])('whats new for different lms', ({ domainName }) => {
    sessionStorage.setItem('domainName', domainName);
    const helpButton = helpSection.getByRole('button', { name: 'Help' });
    userEvent.click(helpButton);
    expect(screen.getByText(`What's new`)).toBeInTheDocument();
  });
});
