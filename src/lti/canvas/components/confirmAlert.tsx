import React, { MouseEventHandler } from 'react';
import { Button, Modal } from 'react-bootstrap';
import FocusLock from 'react-focus-lock';
import { translate } from '../../../i18n/translate';

export interface ConfirmAlertProps {
  show?: boolean;
  type: string;
  confirm: MouseEventHandler;
  onCancel: () => void;
  deleteBtn?: boolean;
}

class ConfirmAlert extends React.Component<ConfirmAlertProps> {
  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onCancel} centered backdrop='static'>
        <FocusLock>
          <Modal.Header closeButton closeLabel={translate('global.close')}>
            <Modal.Title>
              <h5 className='text-primary'>{translate('global.confirm')}</h5>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>{translate('global.deleteconfirmdetail', { detail: this.props.type })}</h5>
          </Modal.Body>
          <Modal.Footer className='d-flex justify-content-center'>
            <Button variant='outline-primary' onClick={this.props.onCancel} size='sm'>
              {translate('global.cancel')}
            </Button>
            <Button
              variant='primary'
              onClick={this.props.confirm}
              disabled={this.props.deleteBtn}
              size='sm'
            >
              {this.props.deleteBtn ? translate('global.deleting') : translate('global.delete')}
            </Button>
          </Modal.Footer>
        </FocusLock>
      </Modal>
    );
  }
}

export default ConfirmAlert;
