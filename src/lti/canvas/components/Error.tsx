import React from 'react';
import { Jumbotron } from 'react-bootstrap';
import { translate } from '../../../i18n/translate';

interface ErrorProps {
  errorMessage?: string;
}
const Error: React.FC<ErrorProps> = (props: ErrorProps) => {
  return (
    <Jumbotron>
      <h1>{props.errorMessage ? translate('error.noauth') : translate('error.error')}</h1>
      <p>{translate('error.errormessage')}</p>
    </Jumbotron>
  );
};

export default Error;
