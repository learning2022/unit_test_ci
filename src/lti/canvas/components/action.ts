/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { API, graphqlOperation } from 'aws-amplify';
import {
  createToCanvas,
  updateCanvasModule,
  deleteCanvasModule,
  publishToCanvas,
  deleteModuleItem,
  updateModuleItem,
  duplicatePageFromModule,
} from '../../../graphql/mutations';
import { canvasPages, canvasModuleItems } from '../../../graphql/queries';

export const AddModule = async (event: any, moduleName: any, modulesList: any) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(createToCanvas, {
      courseId: course_id,
      Name: escape(moduleName),
      type: 'MODULE',
    }),
  );
  response = JSON.parse(response.data.createToCanvas);
  if (response.statusCode === 200) {
    if (response.body.items === undefined) {
      response.body.items = [];
    }
    modulesList.push(response.body);
    return modulesList;
  } else {
    return 'error';
  }
};

export const UpdateModule = async (
  event: any,
  moduleId: any,
  moduleName: any,
  modulesList: any,
  modulePosition: any,
) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(updateCanvasModule, {
      courseId: course_id,
      moduleId: moduleId,
      moduleName: escape(moduleName),
    }),
  );
  response = JSON.parse(response.data.updateCanvasModule);
  if (response.statusCode === 200) {
    modulesList[modulePosition].name = response.body.name;
    return modulesList;
  } else {
    return 'error';
  }
};

export const DeleteModule = async (moduleId: string, modulesList: any, modulePosition: any) => {
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(deleteCanvasModule, {
      courseId: course_id,
      moduleId: moduleId,
    }),
  );
  response = JSON.parse(response.data.deleteCanvasModule);
  if (response.statusCode === 200) {
    modulesList.splice(modulePosition, 1);
    return modulesList;
  } else {
    return 'error';
  }
};

export const RemoveItemFromModule = async (
  moduleId: string,
  itemId: any,
  itemList: any,
  itemPosition: any,
) => {
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(deleteModuleItem, {
      courseId: course_id,
      moduleId: moduleId,
      itemId: itemId,
    }),
  );
  response = JSON.parse(response.data.deleteModuleItem);
  if (response.statusCode === 200) {
    itemList.splice(itemPosition, 1);
    return itemList;
  } else {
    return 'error';
  }
};

export const ModuleTogglePublish = async (
  event: any,
  moduleId: string,
  isPublised: boolean,
  modulesList: any,
  modulePosition: any,
  type: any,
) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(publishToCanvas, {
      courseId: course_id,
      moduleId: moduleId,
      status: isPublised,
      type: type,
    }),
  );
  response = JSON.parse(response.data.publishToCanvas);
  if (response.statusCode === 200) {
    modulesList[modulePosition].published = response.body.published;
    return modulesList;
  } else {
    return 'error';
  }
};

export const GetPages = async (event: any) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(canvasPages, {
      courseId: course_id,
      pageIndex: 1,
      pageItems: 300,
    }),
  );
  response = JSON.parse(response.data.canvasPages);
  if (response.statusCode === 200) {
    return response.body;
  } else {
    return 'error';
  }
};

export const getModuleItems = async (moduleId: any) => {
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(canvasModuleItems, {
      courseId: course_id,
      moduleId: moduleId,
    }),
  );
  response = JSON.parse(response.data.canvasModuleItems);
  if (response.statusCode === 200) {
    return response.body;
  } else {
    return 'error';
  }
};

export const CreatePage = async (event: any, pageName: any) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(createToCanvas, {
      courseId: course_id,
      Name: escape(pageName),
      type: 'PAGE',
    }),
  );
  response = JSON.parse(response.data.createToCanvas);
  if (response.statusCode === 200) {
    return response.body;
  } else {
    return 'error';
  }
};

export const AddPageToModule = async (
  moduleId: string,
  pageUrl: any,
  moduleList: any,
  modulePosition: any,
) => {
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(createToCanvas, {
      courseId: course_id,
      moduleId: moduleId,
      Name: pageUrl,
      type: 'ITEM',
    }),
  );
  response = JSON.parse(response.data.createToCanvas);
  if (response.statusCode === 200) {
    if (moduleList[modulePosition].items) moduleList[modulePosition].items.push(response.body);
    return moduleList;
  } else {
    return 'error';
  }
};

export const ItemTogglePublish = async (
  event: any,
  moduleId: string,
  isPublised: boolean,
  pageList: any,
  itemId: any,
  itemPosition: any,
  type: any,
  moduleArr: any,
) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(publishToCanvas, {
      courseId: course_id,
      moduleId: moduleId,
      itemId: itemId,
      status: isPublised,
      type: 'ITEM',
    }),
  );
  response = JSON.parse(response.data.publishToCanvas);
  if (response.statusCode === 200) {
    const moduleArray = moduleArr;
    moduleArray.forEach((module: any, modIndex: any) => {
      if (module.items.length > 0) {
        module.items.forEach((item: any, index: any) => {
          if (
            item.type === 'Page' ||
            item.type === 'Assignment' ||
            item.type === 'Discussion' ||
            item.type === 'Quiz'
          ) {
            let itemId = '';
            let responseId = '';
            if (item.type === 'Page') {
              itemId = item.url;
              responseId = response.body.url;
            } else {
              itemId = item.id;
              responseId = response.body.id;
            }
            if (itemId === responseId) {
              const thisModule: any = moduleArray[modIndex];
              if (thisModule.items !== undefined)
                thisModule.items[index].published = response.body.published;
              return moduleArray;
            }
          }
        });
      }
    });
  } else {
    return 'error';
  }
};

export const handleRenameItem = async (
  event: any,
  selectedItemId: any,
  title: any,
  moduleId: any,
  moduleArr: any,
) => {
  event.preventDefault();
  event.stopPropagation();
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(updateModuleItem, {
      courseId: course_id,
      itemId: selectedItemId,
      title: escape(title),
      moduleId: moduleId,
    }),
  );
  response = JSON.parse(response.data.updateModuleItem);
  if (response.statusCode === 200) {
    const moduleArray = moduleArr;
    moduleArray.forEach((module: any, modIndex: any) => {
      if (module.items.length > 0) {
        module.items.forEach((item: any, index: any) => {
          if (item.type === 'Page') {
            if (item.id === response.body.id) {
              const thisModule: any = moduleArray[modIndex];
              thisModule.items[index] = response.body;
            }
          }
        });
      }
    });
  } else {
    return 'error';
  }
};

export const pageItemDuplicate = async (
  moduleId: string,
  pageId: string,
  pageUrl: string,
  moduleArr: any,
) => {
  const course_id = sessionStorage.getItem('course_id');
  let response: any = await API.graphql(
    graphqlOperation(duplicatePageFromModule, {
      courseId: course_id,
      moduleId: moduleId,
      pageUrl: pageUrl,
    }),
  );
  response = JSON.parse(response.data.duplicatePageFromModule);
  if (response.statusCode === 200) {
    const moduleArray = moduleArr;
    moduleArray.forEach((module: any, modIndex: any) => {
      if (module.items.length > 0) {
        module.items.forEach((item: any, index: any) => {
          if (item.type === 'Page') {
            const itemId = item.id;
            if (itemId === pageId) {
              const thisModule: any = moduleArray[modIndex];
              thisModule.items.splice(index + 1, 0, response.body);
              return moduleArray;
            }
          }
        });
      }
    });
  } else {
    return 'error';
  }
};
