/* eslint-disable unicorn/no-abusive-eslint-disable */
import React from 'react';
import { shallow, mount } from 'enzyme';
import { ModalProps, Modal } from 'react-bootstrap';
import AddItem, { AddItemProps } from './AddModuleItem';
import AddTemplateItem, { AddTemplateItemProps, TemplateType } from './AddTemplatesItem';
import {
  addModuleItemProps,
  shareTemplatelistProps,
  globalTemplatelistProps,
} from '../../../_test/canvasMockData';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#AddModuleItem', () => {
  describe('when click add module item button', () => {
    const withoutTemplatelistProps: AddTemplateItemProps = {
      show: true,
      hide: jest.fn().mockImplementation(() => false),
      moduleName: 'Test module',
      pageList: [],
      isTemplatePageLoading: false,
      handleAdd: jest.fn(),
      handleExistingAdd: jest.fn(),
      addItemLoading: true,
      itemType: 'Templates',
      onSelectedTemplateItemChange: jest.fn(),
      TemplatesLists: [],
      handleTemplatesFetch: jest.fn(),
      globalTemplatesLists: [],
      sharedTemplatesLists: [],
      templateType: TemplateType.MyTemplate,
      pageNamingModal: false,
      selectedTemplateItem: [],
      handleTemplateRename: jest.fn(),
      viewType: 'list',
    };

    const withoutlistProps: AddTemplateItemProps | AddItemProps = {
      show: true,
      hide: jest.fn().mockImplementation(() => false),
      moduleName: 'Test module',
      pageList: [],
      getPageLoading: false,
      isTemplatePageLoading: false,
      handleAdd: jest.fn(),
      handleExistingAdd: jest.fn(),
      addItemLoading: true,
      selectItemType: jest.fn(),
      itemType: 'Pages',
      TemplatesLists: [],
      handleTemplateAdd: jest.fn(),
      fetchTemplates: jest.fn(),
      globalTemplatesLists: [],
      sharedTemplatesLists: [],
      templateType: TemplateType.MyTemplate,
    };

    const addModuleItem = shallow(<AddItem {...addModuleItemProps} />);
    const handleSave = jest.fn();
    const onSelectedItemChange = jest.fn();
    test('add module modal is rendered', () => {
      expect(addModuleItem.find<ModalProps>(Modal).props().show).toBe(true);
    });
    describe('when page list has result', () => {
      test('page list options rendered', () => {
        expect(addModuleItem.find('option')).toHaveLength(3);
      });
      describe('when grid view is chosen', () => {
        const addModuleItem = shallow(<AddItem {...addModuleItemProps} />);
        test('page list renders in grid view', () => {
          addModuleItem.find('#templatGridIcon').simulate('click');
          expect(addModuleItem.find('.page-grid')).toHaveLength(3);
        });
      });
    });
    describe('when search page name', () => {
      test('search result displayed', () => {
        addModuleItem
          .find('FormControl')
          .at(0)
          .simulate('change', { target: { value: 'test' } });
        expect(addModuleItem.find('option')).toHaveLength(3);
      });
    });
    // todo
    describe.skip('when selects new page option', () => {
      test('change selected option', () => {
        addModuleItem
          .find('FormControl')
          .at(1)
          .simulate('change', { target: { selectedOptions: [{ value: 'new' }] } });
        expect(onSelectedItemChange.mock).toBeCalled();
      });
      describe('when click save button', () => {
        test('new module item is created', () => {
          addModuleItem.find('Button').at(1).simulate('click', handleSave);
          expect(handleSave.mock).toBeCalled();
        });
      });
    });
    // todo
    describe.skip('when types page name', () => {
      test('typed page name is updated in state', () => {
        const pageNameHandler = jest.fn();
        addModuleItem
          .find('FormControl')
          .at(2)
          .simulate('change', { target: { value: 'page name' } });
        expect(pageNameHandler.mock).toBeCalled();
      });
    });
    describe('when select option from page list', () => {
      // todo
      test.skip('change selected option', () => {
        addModuleItem
          .find('FormControl')
          .at(1)
          .simulate('change', { target: { selectedOptions: [{ value: 'test-page' }] } });
        expect(onSelectedItemChange.mock).toBeCalled();
      });
      describe('when click on save button', () => {
        test('module item is duplicated', () => {
          addModuleItem.find('Button').at(1).simulate('click');
          expect(addModuleItem.find('Button').at(1).text()).toEqual('module.addingitem');
        });
      });
    });
    describe('when page list is empty', () => {
      const addModuleItem = shallow(<AddItem {...withoutlistProps} />);
      test('page list option is empty', () => {
        expect(addModuleItem.find('.p-1').at(0).text()).toEqual('page.nopagesfound');
      });
    });

    describe('When Modal Body fetched', () => {
      test('Check [ New Page From Loree Templates ] option is available', () => {
        expect(addModuleItem.find('option').at(1).text()).toEqual('[ module.newpagefromloree ]');
      });

      // todo
      test.skip('Click [ New Page From Loree Templates ] option', () => {
        addModuleItem
          .find('FormControl')
          .at(1)
          .simulate('change', {
            target: { selectedOptions: [{ value: 'new loree page' }], pageNamingModal: false },
          });
        const onSelectedTemplateItemChange = jest.fn();
        expect(onSelectedTemplateItemChange.mock).toBeCalled();
      });

      test('Check Next button is Available', () => {
        addModuleItem
          .find('FormControl')
          .at(1)
          .simulate('change', {
            target: { selectedOptions: [{ value: 'new loree page' }], pageNamingModal: false },
          });
        expect(addModuleItem.find('Button')).toHaveLength(2);
        expect(addModuleItem.find('Button').at(1).text()).toEqual('global.next');
      });

      // todo
      test.skip('Change itemType to Templates by cicking Next', () => {
        addModuleItem
          .find('Button')
          .at(1)
          .simulate('click', { target: { itemType: [{ value: 'Templates' }] } });
        const onSelectedTemplateItemChange = jest.fn();
        expect(onSelectedTemplateItemChange.mock).toBeCalled();
      });
    });

    describe('When templates Modal is navigated', () => {
      test('Check the Template key words present', () => {
        const addModuleItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        expect(addModuleItem.find('FormText').at(0).text()).toEqual('module.addtemplatedetail');
        expect(addModuleItem.find('FormText').at(1).text()).toEqual('module.newpagefromloree');
      });

      test('Check the dropdown has three template option', () => {
        const addModuleItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        expect(addModuleItem.find('option')).toHaveLength(3);
      });

      test('Check the dropdown names of Templates', () => {
        const addModuleItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        expect(addModuleItem.find('option').at(0).text()).toEqual('template.mytemplates');
        expect(addModuleItem.find('option').at(1).text()).toEqual('template.globaltemplates');
        expect(addModuleItem.find('option').at(2).text()).toEqual('template.sharedtemplates');
      });
    });

    describe('When Template is empty', () => {
      test('It should show no template found in a screen', () => {
        withoutTemplatelistProps.TemplatesLists = [];
        const addModuleItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        expect(addModuleItem.render().text()).toContain('template.notemplatetypefound');
      });
      test('Next button should be disabled', () => {
        withoutlistProps.itemType = 'Templates';
        const addModuleItem = shallow(<AddItem {...withoutlistProps} />);
        expect(addModuleItem.find('Button')).toHaveLength(3);
        expect(addModuleItem.find('Button').at(2).props().disabled).toBeTruthy();
      });
    });

    describe('When Template is not an empty list', () => {
      test('It should not show No template found in a screen', () => {
        withoutTemplatelistProps.TemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile template',
            updatedAt: '12134',
          },
        ];
        const addModuleItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        expect(addModuleItem.render().text()).not.toContain('No My Templates Found');
        expect(addModuleItem.find('option')).toHaveLength(3);
      });

      test('Next button should be active and not disabled on selecting template', () => {
        withoutlistProps.TemplatesLists = [
          {
            active: true,
            content:
              '<div class="loree-iframe-content-row row" style="padding: 20px; position: relative; margin: 0px;"></div>',
            createdBy: 'Andiswamy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
            status: true,
            thumbnail: {
              bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e105225-local',
              key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/template/andy',
              region: 'ap-southeast-2',
            },
            title: 'Agile template',
            category: {
              id: '1',
              ltiPlatformID: '2',
              name: '3',
              createdAt: '12',
              updatedAt: '14',
            },
            categoryID: '123',
            createdAt: '12445',
            owner: '65657',
            updatedAt: '5478',
          },
        ];
        withoutlistProps.itemType = 'Templates';

        const addModuleTemplateItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        expect(addModuleTemplateItem.render().text()).toContain('Agile template');
      });

      // todo
      describe.skip('when search templates', () => {
        const addModuleTemplateItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);
        test('searched template displayed', () => {
          const handletTemplateSearch = jest.fn();
          addModuleTemplateItem
            .find('FormControl')
            .at(1)
            .simulate('change', { target: { value: 'agile' } });
          expect(handletTemplateSearch.mock).toBeCalled();
        });
      });

      describe('when click on search icon', () => {
        const addModuleTemplateItem = shallow(<AddTemplateItem {...withoutTemplatelistProps} />);

        // todo
        test.skip('search box displayed', () => {
          const templateSearchFilter = jest.fn();
          addModuleTemplateItem.find('.template-search-icon').simulate('click');
          expect(templateSearchFilter.mock).toBeCalled();
        });
      });
      describe('when tab view is chosen', () => {
        withoutTemplatelistProps.TemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile template',
            updatedAt: '12134',
          },
        ];
        withoutTemplatelistProps.viewType = 'tab';
        const addModuleItem = mount(<AddTemplateItem {...withoutTemplatelistProps} />);
        test('displays template list in grid view', () => {
          expect(addModuleItem.find('.template-grid')).toHaveLength(1);
        });
        describe('when click on template grid item', () => {
          // todo
          test.skip('selects the template', () => {
            const handleTemplateClick = jest.fn();
            addModuleItem.find('.template-grid').at(0).simulate('click');
            expect(handleTemplateClick.mock).toBeCalled();
          });
        });
      });
    });

    describe('When Global Template is chosen', () => {
      test('Show No Global Templates found whlile its empty', () => {
        const addModuleItem = shallow(<AddTemplateItem {...globalTemplatelistProps} />);
        expect(addModuleItem.render().text()).toContain('template.notemplatetypefound');
      });
      test('Show fetched templates if not an empty', () => {
        globalTemplatelistProps.globalTemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile Global template',
            updatedAt: '12134',
          },
        ];
        const addModuleItem = shallow(<AddTemplateItem {...globalTemplatelistProps} />);
        expect(addModuleItem.render().text()).toContain('Agile Global template');
      });
    });

    describe('When Shared Template is chosen', () => {
      test('Show No Global Templates found whlile its empty', () => {
        const addModuleItem = shallow(<AddTemplateItem {...shareTemplatelistProps} />);
        expect(addModuleItem.render().text()).toContain('template.notemplatetypefound');
      });
      test('Show fetched templates if not an empty', () => {
        shareTemplatelistProps.sharedTemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile Shared template',
            updatedAt: '12134',
          },
        ];
        const addModuleItem = shallow(<AddTemplateItem {...shareTemplatelistProps} />);
        expect(addModuleItem.render().text()).toContain('Agile Shared template');
      });
    });

    describe('When pageRenaming Modal is navigated', () => {
      test('it should have the default Template Name 1 and Page Name column', () => {
        shareTemplatelistProps.sharedTemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile Shared template',
            updatedAt: '12134',
          },
        ];
        shareTemplatelistProps.pageNamingModal = true;
        shareTemplatelistProps.selectedTemplateItem = [
          {
            id: '84e0a188-fc8e-4106-b134-96364766a964',
            name: 'Agile Shared template',
          },
        ];
        const addModuleItem = shallow(<AddTemplateItem {...shareTemplatelistProps} />);
        expect(addModuleItem.find('.text-primary')).toHaveLength(2);
        expect(addModuleItem.find('.text-primary').at(0).text()).toBe('global.template');
        expect(addModuleItem.find('.text-primary').at(1).text()).toBe('module.newpage');
      });
      test('Rendered Modal should conatin follwing Template field properties', () => {
        shareTemplatelistProps.sharedTemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile Shared template',
            updatedAt: '12134',
          },
        ];
        shareTemplatelistProps.pageNamingModal = true;
        shareTemplatelistProps.selectedTemplateItem = [
          {
            id: '84e0a188-fc8e-4106-b134-96364766a964',
            name: 'Agile Shared template',
          },
        ];
        const addModuleItem = shallow(<AddTemplateItem {...shareTemplatelistProps} />);
        expect(addModuleItem.find('.page-naming-form')).toHaveLength(1);
        expect(addModuleItem.find('.text-muted')).toHaveLength(1);
        expect(addModuleItem.find('.text-muted').at(0).text()).toBe('template.templatenameindex');
        expect(addModuleItem.find('FormControl').at(0).props().disabled).toBeTruthy();
      });
      test('Rendered Modal should conatin follwing Page field properties', () => {
        shareTemplatelistProps.sharedTemplatesLists = [
          {
            active: true,
            category: {
              id: '12',
              ltiPlatformID: '1234',
              name: 'andy',
              createdAt: '12344',
              updatedAt: '243435',
            },
            categoryID: '12',
            content: 'html',
            createdAt: '1234',
            createdBy: 'andy',
            id: 'd6033f8a-4846-403d-8840-82a1c418547f',
            isGlobal: false,
            isShared: null,
            ltiPlatformID: '1234',
            owner: 'andy',
            status: true,
            thumbnail: { bucket: 'asasas', key: 'fff123', region: 'aws' },
            title: 'Agile Shared template',
            updatedAt: '12134',
          },
        ];
        shareTemplatelistProps.pageNamingModal = true;
        shareTemplatelistProps.selectedTemplateItem = [
          {
            id: '84e0a188-fc8e-4106-b134-96364766a964',
            name: 'Agile Shared template',
          },
        ];
        const addModuleItem = shallow(<AddTemplateItem {...shareTemplatelistProps} />);
        expect(addModuleItem.find('FormText')).toHaveLength(5);
        expect(addModuleItem.find('FormText').at(4).text()).toBe('template.templatepagename*');
      });
    });
  });
});
