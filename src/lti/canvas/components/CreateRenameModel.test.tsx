import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import CreateRename from './CreateRenameModel';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#CreateRenameModal', () => {
  describe('when click rename', () => {
    const renameModuleProps = {
      show: true,
      hide: jest.fn(),
      isCreate: false,
      saveMod: jest.fn(),
      moduleName: 'test module',
      nameHandler: jest.fn(),
      btnLoad: false,
    };
    const renameModal = shallow(<CreateRename {...renameModuleProps} />);
    test('rename modal is rendered', () => {
      expect(renameModal.find('ModalTitle').text()).toEqual('module.renamemodule');
    });
    describe('when click update button', () => {
      test('default event is prevented', () => {
        const preventDefault = jest.fn();
        renameModal.find('Form').at(0).simulate('submit', { preventDefault });
      });
    });
  });
  describe('when click add module', () => {
    const createModuleProps = {
      show: true,
      hide: jest.fn(),
      isCreate: true,
      saveMod: jest.fn(),
      moduleName: 'test module',
      nameHandler: jest.fn(),
      btnLoad: true,
    };
    const addModal = shallow(<CreateRename {...createModuleProps} />);
    test('add module modal is rendered', () => {
      expect(addModal.find('ModalTitle').text()).toEqual('module.addmodule');
    });
  });

  describe('When creating new Module with empty name', () => {
    let addModal: ShallowWrapper;
    beforeEach(() => {
      // having an empty module name to create
      const createModuleProps = {
        show: true,
        hide: jest.fn(),
        isCreate: true,
        saveMod: jest.fn(),
        moduleName: '',
        nameHandler: jest.fn(),
        btnLoad: false,
      };
      addModal = shallow(<CreateRename {...createModuleProps} />);
    });
    test('it should not allow ADD button to be disabled by default', () => {
      expect(addModal.find('Button').at(1).text()).toEqual('global.add');
      expect(addModal.find('Button').at(1).props().disabled).toBeTruthy();
    });
  });
  describe('When creating new Module with some strings', () => {
    let addModal: ShallowWrapper;
    beforeEach(() => {
      // having valid module name to create
      const createModuleProps = {
        show: true,
        hide: jest.fn(),
        isCreate: true,
        saveMod: jest.fn(),
        moduleName: 'Lesson1',
        nameHandler: jest.fn(),
        btnLoad: false,
      };
      addModal = shallow(<CreateRename {...createModuleProps} />);
    });
    test('it should enable ADD button as having valid module name', () => {
      expect(addModal.find('Button').at(1).props().disabled).toBeFalsy();
    });
  });
  describe('When creating new Module with space keys', () => {
    let addModal: ShallowWrapper;
    beforeEach(() => {
      // having a space key filled module name to create
      const createModuleProps = {
        show: true,
        hide: jest.fn(),
        isCreate: true,
        saveMod: jest.fn(),
        moduleName: '                    ',
        nameHandler: jest.fn(),
        btnLoad: false,
      };
      addModal = shallow(<CreateRename {...createModuleProps} />);
    });
    test('it should enable ADD button on having valid module name', () => {
      expect(addModal.find('Button').at(1).props().disabled).toBeTruthy();
    });
  });
  describe('When giving input field value of less than or equal to 255 chars', () => {
    let addModal: ShallowWrapper;
    beforeEach(() => {
      // having 255 char value of module name
      const createModuleProps = {
        show: true,
        hide: jest.fn(),
        isCreate: true,
        saveMod: jest.fn(),
        moduleName:
          'PBUG/pages/790659188/LoreeV2FeaturesAdmindashboardhttps://crystaldelta.atlassian.net/wiki/spaces/PBUG/pages/790659188/LoreeV2FeaturesAdmindasddsdsdssasdadsfsfdsfdddddddddddddddddddeeeeeeeeeeeeeeeeeeep ffffffffffffffffffffffffffffffffffffffffffffffffffsddd',
        nameHandler: jest.fn(),
        btnLoad: false,
      };
      addModal = shallow(<CreateRename {...createModuleProps} />);
    });
    test('it should enable ADD button', () => {
      expect(addModal.find('Button').at(1).props().disabled).not.toBeTruthy();
    });
  });
  describe('When giving input field value of greater than or equal to 255 chars', () => {
    let addModal: ShallowWrapper;
    beforeEach(() => {
      // having 256 char value of module name
      const createModuleProps = {
        show: true,
        hide: jest.fn(),
        isCreate: true,
        saveMod: jest.fn(),
        moduleName:
          'PBUG/pages/790659188/LoreeV2FeaturesAdmindashboardhttps://crystaldelta.atlassian.net/wiki/spaces/PBUG/pages/790659188/LoreeV2FeaturesAdmindasddsdsdssasdadsfsfdsfdddddddddddddddddddeeeeeeeeeeeeeeeeeeep ffffffffffffffffffffffffffffffffffffffffffffffffffsddd@',
        nameHandler: jest.fn(),
        btnLoad: false,
      };
      addModal = shallow(<CreateRename {...createModuleProps} />);
    });
    test('it should give an alert message', () => {
      expect(addModal.find('small').text()).toEqual('global.charlimitnote');
    });
  });
});
