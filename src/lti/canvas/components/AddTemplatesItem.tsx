import React, { Component, ChangeEventHandler } from 'react';
import { Form, Col } from 'react-bootstrap';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Loading from '../../../components/loader/loading';
import { LtiIcons } from '../../icons';
import { PageLists, TemplateList } from './AddModuleItem';
import { translate } from '../../../i18n/translate';

export enum TemplateType {
  MyTemplate = 0,
  GlobalTemplate = 1,
  SharedTemplate = 2,
}
export interface AddTemplateItemProps {
  show: boolean;
  hide(): void;
  moduleName: string;
  pageList: Array<PageLists>;
  isTemplatePageLoading: boolean;
  handleAdd(event: React.MouseEvent<HTMLElement>, pageName: string): void;
  handleExistingAdd(pageUrl: never[], isMultiplePage: boolean): void;
  addItemLoading: boolean;
  itemType: string;
  onSelectedTemplateItemChange(event: React.ChangeEvent<HTMLSelectElement>): void;
  TemplatesLists: TemplateList[];
  handleTemplatesFetch(event: React.ChangeEvent<HTMLInputElement>): void;
  globalTemplatesLists: TemplateList[];
  sharedTemplatesLists: TemplateList[];
  templateType: TemplateType;
  pageNamingModal: boolean;
  selectedTemplateItem: { name: string; id: string }[];
  handleTemplateRename(event: React.ChangeEvent<HTMLInputElement>): void;
  viewType: string;
}

class AddTemplateItem extends Component<AddTemplateItemProps> {
  state = {
    templateType: TemplateType.MyTemplate,
    templateList: [],
    globalTemplateList: [],
    sharedTemplateList: [],
    viewType: this.props.viewType,
  };

  componentDidMount() {
    this.setState({
      templateList: this.props.TemplatesLists,
      globalTemplateList: this.props.globalTemplatesLists,
      sharedTemplateList: this.props.sharedTemplatesLists,
    });
  }

  componentDidUpdate(
    prevProps: Readonly<AddTemplateItemProps> & Readonly<{ children?: React.ReactNode }>,
  ) {
    if (
      this.props.TemplatesLists !== prevProps.TemplatesLists ||
      this.props.globalTemplatesLists !== prevProps.globalTemplatesLists ||
      this.props.sharedTemplatesLists !== prevProps.sharedTemplatesLists
    )
      // eslint-disable-next-line  react/no-did-update-set-state
      this.setState({
        templateList: this.props.TemplatesLists,
        globalTemplateList: this.props.globalTemplatesLists,
        sharedTemplateList: this.props.sharedTemplatesLists,
      });
  }

  handleTemplateDropdown: ChangeEventHandler<HTMLInputElement> = async (event) => {
    const templateSearchBar = document.getElementById('templateSearch');
    (templateSearchBar as HTMLInputElement).value = '';
    await this.props.handleTemplatesFetch(event);
  };

  handlePageNaming = (event: React.ChangeEvent<HTMLInputElement>, index: number) => {
    event.preventDefault();
    this.handleFieldAlertsDisplay(index, event.target.value);
    this.props.handleTemplateRename(event);
  };

  handleFieldAlertsDisplay = (index: number, value: string): void => {
    const alertElements = document.getElementsByClassName('char-alert') as HTMLCollection;
    Array.prototype.forEach.call(alertElements, (alertElement, elementIndex) => {
      if (elementIndex === index)
        value.length > 254
          ? alertElement.classList.remove('d-none')
          : alertElement.classList.add('d-none');
    });
  };

  checkIfAlertsExists = () => {
    const alertElements = document.getElementsByClassName('char-alert') as HTMLCollection;
    const alertElementsArray = Array.from(alertElements);
    const isExists = alertElementsArray.every((element: Element) =>
      element.classList.contains('d-none'),
    );
    return !isExists;
  };

  handletTemplateSearch: ChangeEventHandler<HTMLInputElement> = (event) => {
    const searchText = event.target.value.toLowerCase();
    const templateData: TemplateList[] | never[] =
      this.props.templateType === TemplateType.MyTemplate
        ? this.props.TemplatesLists
        : this.props.templateType === TemplateType.GlobalTemplate
        ? this.props.globalTemplatesLists
        : this.props.sharedTemplatesLists;
    const searchList: TemplateList[] = [];
    if (searchText !== '') {
      for (let i = 0; i < templateData.length; i++) {
        const pageTitle = templateData[i].title;
        if (pageTitle.toLowerCase().includes(searchText)) searchList.push(templateData[i]);
      }
      switch (this.props.templateType) {
        case TemplateType.MyTemplate:
          this.setState({ templateList: searchList });
          break;
        case TemplateType.GlobalTemplate:
          this.setState({ globalTemplateList: searchList });
          break;
        case TemplateType.SharedTemplate:
          this.setState({ sharedTemplateList: searchList });
          break;
      }
    } else {
      this.setState({
        templateList: this.props.TemplatesLists,
        globalTemplateList: this.props.globalTemplatesLists,
        sharedTemplateList: this.props.sharedTemplatesLists,
      });
    }
  };

  render() {
    return (
      <div>
        <Form.Text className='h6' role='heading' aria-level={2}>
          {translate('module.addtemplatedetail', { detail: this.props.moduleName })}
        </Form.Text>
        {this.props.pageNamingModal && this.props.selectedTemplateItem.length > 0 ? (
          <div className='page-template-rename-container' role='presentation'>
            <Form className='mb-2' role='presentation'>
              <Form.Row>
                <Col>
                  <Form.Text className='text-primary font-weight-bold'>
                    {translate('global.template')}
                  </Form.Text>
                </Col>
                <Col>
                  <Form.Text className='text-primary font-weight-bold'>
                    {translate('module.newpage')}
                  </Form.Text>
                </Col>
              </Form.Row>
            </Form>

            {this.props.selectedTemplateItem.map(
              (template: { name: string; id: string }, index: number) => (
                <Form key={index}>
                  <Form.Row className='page-naming-form'>
                    <Col>
                      <Form.Text
                        data-testid='selected-template'
                        aria-label={translate('template.selectedtemplate_aria', {
                          detail: index + 1,
                        })}
                        className='text-muted'
                      >
                        {translate('template.templatenameindex', { detail: index + 1 })}
                      </Form.Text>
                      <Form.Control
                        key={'Template Name ' + index}
                        value={template.name}
                        size='sm'
                        disabled
                      />
                    </Col>
                    <Col>
                      <Form.Text
                        aria-label={translate('template.typenewpage_aria')}
                        id='page-name'
                        data-testid='new-page-name'
                      >
                        {translate('template.templatepagename', { detail: index + 1 })}
                        <span className='h6 text-danger font-weight-bold'>*</span>
                      </Form.Text>
                      <Form.Control
                        aria-labelledby='page-name'
                        data-key={template.id}
                        placeholder={translate('page.pagename')}
                        size='sm'
                        maxLength={255}
                        autoComplete='off'
                        required
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          this.handlePageNaming(e, index)
                        }
                      />
                      <small className='text-danger d-none char-alert' role='alert'>
                        {translate('global.charlimitnote')}
                      </small>
                    </Col>
                  </Form.Row>
                </Form>
              ),
            )}
          </div>
        ) : (
          <div className='canvas-list-modal-container'>
            <div className='d-flex justify-content-between'>
              <Form.Text className='h6 font-weight-bold m-2'>
                {translate('module.newpagefromloree')}
              </Form.Text>
              <div className='d-flex justify-content-end'>
                <Form.Group
                  id='canvasAddPageTemplateDropdown'
                  className='canvasAddPageTemplateDropdown position-relative d-flex align-items-center mb-0 mr-2'
                >
                  <Form.Control
                    aria-label={translate('template.templatetype_aria')}
                    as='select'
                    data-testid='template-types'
                    className='templates-dropdown'
                    value={this.props.templateType}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      this.handleTemplateDropdown(e)
                    }
                  >
                    <option value={TemplateType.MyTemplate}>
                      {translate('template.mytemplates')}
                    </option>
                    <option value={TemplateType.GlobalTemplate}>
                      {translate('template.globaltemplates')}
                    </option>
                    <option value={TemplateType.SharedTemplate}>
                      {translate('template.sharedtemplates')}
                    </option>
                  </Form.Control>
                  <span className='position-absolute'>
                    <FontAwesomeIcon icon={faCaretDown} />
                  </span>
                </Form.Group>
                <Form.Group className='mb-0'>
                  <Form.Control
                    id='templateSearch'
                    className='module-item-search-bar d-block mr-2'
                    type='text'
                    autoComplete='off'
                    placeholder={translate('global.search')}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      this.handletTemplateSearch(e)
                    }
                  />
                </Form.Group>
                <LtiIcons.SearchIcon
                  id='templateSearchIcon'
                  width='20px'
                  height='30px'
                  fill='#909090'
                  className='template-search-icon mr-2 template-search-bar-active position-absolute'
                  data-test='searchbtn'
                />
                <LtiIcons.ListIcon
                  id='templateListIcon'
                  width='20px'
                  height='30px'
                  fill={this.state.viewType === 'list' ? '#000d9c' : '#909090'}
                  className='mr-2'
                  onClick={() => this.setState({ viewType: 'list' })}
                />
                <LtiIcons.TabIcon
                  id='templatGridIcon'
                  width='20px'
                  height='30px'
                  fill={this.state.viewType === 'tab' ? '#000d9c' : '#909090'}
                  onClick={() => this.setState({ viewType: 'tab' })}
                />
              </div>
            </div>
            <Form.Text className='text-muted m-2' id='add_template_instructions' role='status'>
              {this.state.viewType === 'list'
                ? translate('template.templateadddescribedetail')
                : translate('template.templateadddescribe')}
            </Form.Text>

            {this.props.templateType === TemplateType.MyTemplate ? (
              this.props.isTemplatePageLoading ? (
                <Loading />
              ) : (this.state.templateList as Array<TemplateList>).length > 0 ? (
                <TemplatesListsView
                  templatesData={this.state.templateList}
                  onSelectedTemplateItemChange={this.props.onSelectedTemplateItemChange}
                  viewType={this.state.viewType}
                />
              ) : (
                <NoTemplateElement templateType={this.props.templateType} />
              )
            ) : null}

            {this.props.templateType === TemplateType.GlobalTemplate ? (
              this.props.isTemplatePageLoading ? (
                <Loading />
              ) : (this.state.globalTemplateList as Array<TemplateList>).length > 0 ? (
                <TemplatesListsView
                  templatesData={this.state.globalTemplateList}
                  onSelectedTemplateItemChange={this.props.onSelectedTemplateItemChange}
                  viewType={this.state.viewType}
                />
              ) : (
                <NoTemplateElement templateType={this.props.templateType} />
              )
            ) : null}

            {this.props.templateType === TemplateType.SharedTemplate ? (
              this.props.isTemplatePageLoading ? (
                <Loading />
              ) : (this.state.sharedTemplateList as Array<TemplateList>).length > 0 ? (
                <TemplatesListsView
                  templatesData={this.state.sharedTemplateList}
                  onSelectedTemplateItemChange={this.props.onSelectedTemplateItemChange}
                  viewType={this.state.viewType}
                />
              ) : (
                <NoTemplateElement templateType={this.props.templateType} />
              )
            ) : null}
          </div>
        )}
      </div>
    );
  }
}

interface NoTemplateProps {
  templateType: TemplateType;
}
class NoTemplateElement extends Component<NoTemplateProps> {
  render() {
    return (
      <div>
        <div className='p-1 border'>
          <div className='p-1'>
            {translate('template.notemplatetypefound', {
              detail: translate(`global.templatetype${this.props.templateType}`),
            })}
          </div>
        </div>
      </div>
    );
  }
}

interface ListViewProps {
  templatesData: TemplateList[];
  onSelectedTemplateItemChange(event: React.ChangeEvent<HTMLSelectElement>): void;
  viewType: string;
}
class TemplatesListsView extends Component<ListViewProps> {
  handleTemplateClick = (event: _Any) => {
    const templateWrapper = event.target.parentElement;
    templateWrapper?.classList.toggle('template-grid-active');
    if (templateWrapper?.classList?.contains('template-grid-active')) {
      templateWrapper.style.color = '#000d9c';
      templateWrapper.firstChild.style.borderColor = '#000d9c';
    } else {
      templateWrapper.style.color = '#000000';
      templateWrapper.firstChild.style.borderColor = '#909090';
    }
    this.props.onSelectedTemplateItemChange(event);
  };

  render() {
    return (
      <div>
        {this.props.viewType === 'list' ? (
          <div>
            <Form.Control
              data-testid='select-template-list'
              aria-labelledby='add_template_instructions'
              as='select'
              multiple
              htmlSize={9}
              custom
              onChange={this.props.onSelectedTemplateItemChange}
            >
              {this.props.templatesData.map((template: TemplateList, index: number) => (
                <option value={template.id} key={index}>
                  {template.title}
                </option>
              ))}
            </Form.Control>
          </div>
        ) : (
          <div className='d-flex flex-row flex-wrap template-grid-container'>
            {this.props.templatesData.map((template: TemplateList, index: number) => (
              <div
                key={index}
                id={template.id}
                className='m-2 template-grid'
                onClick={(e) => this.handleTemplateClick(e)}
                onKeyDown={() => {}}
                role='presentation'
              >
                <div className='mb-2 template-grid-item' />
                <p className='text-center text-truncate template-grid-text' title={template.title}>
                  {template.title}
                </p>
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default AddTemplateItem;
