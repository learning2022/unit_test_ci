import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import ConfirmAlert from './confirmAlert';

const ConfirmAlertProps = {
  show: true,
  type: 'modules',
  confirm: jest.fn(),
  onCancel: jest.fn(),
  deleteBtn: false,
};
jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

// let confirmAlert = mount(<ConfirmAlert {...ConfirmAlertProps} />);
describe('<ConfirmAlert>', () => {
  test('renders', () => {
    const { container } = render(<ConfirmAlert {...ConfirmAlertProps} />);
    expect(container).toBeInTheDocument();
  });
  describe('close button', () => {
    test('exists', () => {
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} />);
      const button = getByText('global.close', { exact: false });
      expect(button).toBeInTheDocument();
    });
    test('calls onCancel when clicked', () => {
      const onCancel = jest.fn();
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} onCancel={onCancel} />);
      const button = getByText('global.close', { exact: false });
      fireEvent.click(button);
      expect(onCancel).toHaveBeenCalled();
    });
  });
  describe('delete button', () => {
    test('exists', () => {
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} />);
      const button = getByText('global.delete');
      expect(button).toBeInTheDocument();
    });
    test('call confirm when clicked', () => {
      const confirm = jest.fn();
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} confirm={confirm} />);
      const button = getByText('global.delete');
      fireEvent.click(button);
      expect(confirm).toHaveBeenCalled();
    });
    test('has correct initalised text', () => {
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} />);
      const button = getByText('global.delete');
      expect(button).toBeEnabled();
    });
    test('changes presented text when clicked', () => {
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} deleteBtn />);
      const button = getByText('global.deleting');
      expect(button).toBeDisabled();
    });
  });
  describe('cancel button', () => {
    test('exists', () => {
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} />);
      const button = getByText('global.cancel', { exact: false });
      expect(button).toBeInTheDocument();
    });
    test('calls onCancel when clicked', () => {
      const onCancel = jest.fn();
      const { getByText } = render(<ConfirmAlert {...ConfirmAlertProps} onCancel={onCancel} />);
      const button = getByText('global.cancel', { exact: false });
      fireEvent.click(button);
      expect(onCancel).toHaveBeenCalled();
    });
  });
});
