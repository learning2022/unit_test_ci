import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';

import axe from '../../../axeHelper';
import CreateRename from './CreateRenameModel';
// // NOTE: There is another testing file 'index.test.tsx' which is using enzyme.
// // Please add test cases created after Nov 2021 here.

describe('Canvas Create Rename Modal', () => {
  const renameModuleProps = {
    show: true,
    hide: jest.fn(),
    isCreate: false,
    saveMod: jest.fn(),
    moduleName: 'test module',
    nameHandler: jest.fn(),
    btnLoad: false,
  };
  let createRename: RenderResult;
  beforeEach(() => {
    createRename = render(
      <BrowserRouter>
        <CreateRename {...renameModuleProps} />
      </BrowserRouter>,
    );
  });
  test('should pass accessibility tests', async () => {
    const results = await axe(createRename.container);
    expect(results).toHaveNoViolations();
  });
});
