import React from 'react';
import { render, RenderResult, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

import axe from '../../../axeHelper';
import AddItem from './AddModuleItem';
import { addModuleItemProps } from '../../../_test/canvasMockData';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Canvas Add Item Modal', () => {
  let addItem: RenderResult;

  beforeEach(() => {
    addItem = render(
      <BrowserRouter>
        <AddItem {...addModuleItemProps} />
      </BrowserRouter>,
    );
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(addItem.container);
    expect(results).toHaveNoViolations();
  });

  test('should tab wrap focus in order', () => {
    // 1. Close Button
    const closeButton = screen.getByText('global.close', { exact: false });
    expect(closeButton.parentElement).toHaveFocus();
    // 2. Search Input
    userEvent.tab();
    const searchInput = screen.getByPlaceholderText('global.search');
    expect(searchInput).toHaveFocus();
    // 3. Custom Select
    userEvent.tab();
    const customSelect = screen.getByTestId('select-list');
    expect(customSelect).toHaveFocus();
    // 4. Cancel Button
    userEvent.tab();
    const cancelButton = screen.getByText('global.cancel');
    expect(cancelButton).toHaveFocus();
    // 1. Close Button - back to the first focusable element
    userEvent.tab();
    expect(closeButton.parentElement).toHaveFocus();
  });

  test('should have search input displayed without user interaction', () => {
    const searchInput = screen.getByPlaceholderText('global.search');
    expect(searchInput).toBeTruthy();
  });

  test('should have modal header role=heading', () => {
    const modalHeader = screen.getByRole('heading', { level: 1 });
    expect(modalHeader.innerHTML).toBe('module.addmoduleitemdetail');

    const secondModalHeader = screen.getByRole('heading', { level: 2 });
    expect(secondModalHeader.innerHTML).toBe('module.addmodulepagedetail');

    const thirdModalHeader = screen.getByRole('heading', { level: 3 });
    expect(thirdModalHeader.innerHTML).toBe('module.defaultcanvaspages');
  });
});
