import React, { Component, MouseEventHandler } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import FocusLock from 'react-focus-lock';
import { FormControlProps } from 'react-bootstrap/FormControl';
import { translate } from '../../../i18n/translate';

export interface ItemRenameProps {
  show: boolean;
  hide: () => void;
  renameItemHandler: MouseEventHandler;
  itemName: string;
  nameHandler: FormControlProps['onChange'];
  btnLoad: boolean;
}

class ItemRename extends Component<ItemRenameProps> {
  render() {
    return (
      <>
        <Modal
          show={this.props.show}
          onHide={this.props.hide}
          backdrop='static'
          keyboard={false}
          centered
        >
          <FocusLock className='focus-lock'>
            <Modal.Header closeButton closeLabel={translate('global.close')}>
              <Modal.Title id='create-new-module'>
                <h5 className='text-primary'>{translate('pageitem.renameitem')}</h5>
              </Modal.Title>
            </Modal.Header>
            <Form onSubmit={(event) => event.preventDefault()}>
              <Modal.Body>
                <Form.Group controlId='formPageName'>
                  <Form.Label>{translate('pageitem.itemname')}</Form.Label>
                  <Form.Control
                    required
                    data-test='moduleName'
                    type='text'
                    maxLength={255}
                    placeholder=' Item name'
                    autoComplete='off'
                    value={this.props.itemName}
                    onChange={this.props.nameHandler}
                  />
                  {this.props.itemName.length > 254 ? (
                    <small className='text-danger'>{translate('global.charlimitnote')}</small>
                  ) : (
                    ''
                  )}
                </Form.Group>
              </Modal.Body>
              <Modal.Footer className='d-flex justify-content-center'>
                <Button
                  variant='outline-primary'
                  onClick={this.props.hide}
                  size='sm'
                  className='mx-1'
                >
                  {translate('global.cancel')}
                </Button>
                <Button
                  variant='primary'
                  size='sm'
                  className='mx-1'
                  onClick={this.props.renameItemHandler}
                  disabled={
                    this.props.btnLoad ||
                    this.props.itemName === '' ||
                    this.props.itemName.replace(/\s/g, '').length === 0
                  }
                >
                  {this.props.btnLoad
                    ? translate('global.updatingdot')
                    : translate('global.update')}
                </Button>
              </Modal.Footer>
            </Form>
          </FocusLock>
        </Modal>
      </>
    );
  }
}

export default ItemRename;
