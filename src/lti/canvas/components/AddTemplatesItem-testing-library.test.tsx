import React from 'react';
import { render, RenderResult, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';

import axe from '../../../axeHelper';
import AddTemplateItem from './AddTemplatesItem';
import {
  addTemplateItemProps,
  addTemplateItemWithLoaderProps,
} from '../../../_test/canvasMockData';
import { getElementsByClassName } from '../../../loree-editor/common/dom';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Canvas Add Item Modal', () => {
  let addTemplateItem: RenderResult;

  beforeEach(() => {
    addTemplateItem = render(
      <BrowserRouter>
        <AddTemplateItem {...addTemplateItemProps} />
      </BrowserRouter>,
    );
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(addTemplateItem.container);
    expect(results).toHaveNoViolations();
  });

  test('should have search input displayed without user interaction', () => {
    const searchInput = screen.getByPlaceholderText('global.search');
    expect(searchInput.className).toBe('module-item-search-bar d-block mr-2 form-control');
  });

  test('should have modal header role=heading', () => {
    const modalHeader = addTemplateItem.getByRole('heading', { level: 2 });
    expect(modalHeader.innerHTML).toBe('module.addtemplatedetail');
  });

  test('dropdown menu should have aria-label', () => {
    const dropdownMenu = screen.getByTestId('template-types');
    expect(dropdownMenu).toHaveAttribute('aria-label');
  });
});
describe('add template design checkup with loader', () => {
  beforeEach(() => {
    render(<AddTemplateItem {...addTemplateItemWithLoaderProps} />);
  });
  test('my templates page loader', () => {
    expect(getElementsByClassName('loader-icon')[0]).toBeInTheDocument();
  });
});
describe('add template design checkup without loader', () => {
  beforeEach(() => {
    render(<AddTemplateItem {...addTemplateItemProps} />);
  });
  test('my templates page loader', () => {
    expect(getElementsByClassName('loader-icon')[0]).toBeFalsy();
  });
});
