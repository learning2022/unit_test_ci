import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { shallow } from 'enzyme';
import Published from './Published';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#published', () => {
  describe('when page is published', () => {
    const publishProps = {
      isPublished: true,
      id: '12344',
      type: 'PAGE',
      togglePublishStatus: jest.fn(),
      modulePosition: 2,
      itemId: '',
    };

    const publishedComponent = shallow(<Published {...publishProps} />);
    test('publish status displayed', () => {
      expect(publishedComponent.find('Button').props().title).toEqual('page.unpublishbutton');
    });
  });
  test('page is unpublished', () => {
    const publishProps = {
      isPublished: true,
      id: '12344',
      type: 'PAGE',
      togglePublishStatus: jest.fn(),
      modulePosition: 2,
      itemId: '',
    };
    const togglePublishStatus = jest.fn();
    const { getByTitle } = render(
      <Published {...publishProps} togglePublishStatus={togglePublishStatus} />,
    );
    const pubButton = getByTitle('page.unpublishbutton');
    fireEvent.click(pubButton);
    expect(togglePublishStatus).toHaveBeenCalled();
  });
  describe('when page is not published', () => {
    const puplishProps = {
      isPublished: false,
      id: '12344',
      type: 'PAGE',
      togglePublishStatus: jest.fn(),
      modulePosition: 2,
      itemId: '',
    };
    const publishedComponent = shallow(<Published {...puplishProps} />);
    test('publish status displayed', () => {
      expect(publishedComponent.find('Button').props().title).toEqual('page.publishbutton');
    });
  });
});
