import React from 'react';
import { shallow } from 'enzyme';
import Error from './Error';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#Error', () => {
  describe('when unauthorized error', () => {
    const errorProps = {
      errorMessage: 'lmsRoleError',
    };
    const error = shallow(<Error {...errorProps} />);
    test('displays unauthorized meesage', () => {
      expect(error.find('h1').text()).toEqual('error.noauth');
    });
  });
  describe('when any other error', () => {
    const error = shallow(<Error />);
    test('displays something went wrong message', () => {
      expect(error.find('h1').text()).toEqual('error.error');
    });
  });
});
