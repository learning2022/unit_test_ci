import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

import axe from '../../../axeHelper';
import ItemRename from './ItemUpdateModal';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Item Rename Modal', () => {
  let itemRename: RenderResult;
  const itemRenameProps = {
    show: true,
    hide: jest.fn(),
    renameItemHandler: jest.fn(),
    itemName: 'page name',
    nameHandler: jest.fn(),
    btnLoad: false,
  };

  beforeEach(() => {
    itemRename = render(
      <BrowserRouter>
        <ItemRename {...itemRenameProps} />
      </BrowserRouter>,
    );
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(itemRename.container);
    expect(results).toHaveNoViolations();
  });

  test('should tab to focus on UI buttons in order of visual appearance', () => {
    const closeButton = itemRename.getByRole('button', {
      name: 'global.close',
    });
    closeButton.focus();
    expect(closeButton).toHaveFocus();
    userEvent.tab();

    const itemNameInput = itemRename.getByRole('textbox', {
      name: 'pageitem.itemname',
    });
    expect(itemNameInput).toHaveFocus();
    userEvent.tab();

    const cancelButton = itemRename.getByRole('button', {
      name: 'global.cancel',
    });
    expect(cancelButton).toHaveFocus();
    userEvent.tab();

    const updateButton = itemRename.getByRole('button', {
      name: 'global.update',
    });
    expect(updateButton).toHaveFocus();
    userEvent.tab();

    expect(closeButton).toHaveFocus();
  });
});
