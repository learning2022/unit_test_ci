import React, { Component, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';
import { translate } from '../../../i18n/translate';

interface PublishedProps {
  isPublished: boolean;
  id: _Any;
  type: _Any;
  togglePublishStatus(
    event: _Any,
    moduleId: _Any,
    publishedStatus: _Any,
    modulePosition: _Any,
    type: _Any,
    itemId: _Any,
  ): _Any;
  modulePosition: _Any;
  itemId?: _Any;
}

class Published extends Component<PublishedProps> {
  render() {
    return (
      <>
        <Button
          variant='link'
          title={
            this.props.isPublished
              ? translate('page.unpublishbutton')
              : translate('page.publishbutton')
          }
          onClick={(e) =>
            this.props.togglePublishStatus(
              e,
              this.props.id,
              !this.props.isPublished,
              this.props.modulePosition,
              this.props.type,
              this.props.itemId,
            )
          }
          className='pt-0 pb-0'
        >
          <FontAwesomeIcon
            icon={this.props.isPublished ? faCheckCircle : faBan}
            className={`${this.props.isPublished ? 'text-success' : 'text-muted'}`}
          />
        </Button>
      </>
    );
  }
}

export default Published;
