import React from 'react';
import { shallow } from 'enzyme';
import ItemRename from './ItemUpdateModal';
import { Modal, ModalProps } from 'react-bootstrap';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#itemRenameModal', () => {
  describe('when click on rename', () => {
    const itemRenameProps = {
      show: true,
      hide: jest.fn(),
      renameItemHandler: jest.fn(),
      itemName: 'page name',
      nameHandler: jest.fn(),
      btnLoad: false,
    };
    const updateModal = shallow(<ItemRename {...itemRenameProps} />);
    test('rename modal opened', () => {
      expect(updateModal.find<ModalProps>(Modal).props().show).toBe(true);
    });
    describe('when form submitted', () => {
      test('default action prevented', () => {
        const preventDefault = jest.fn();
        updateModal.find('Form').at(0).simulate('submit', { preventDefault });
      });
    });
    describe('when click on update', () => {
      const itemRenameProps = {
        show: true,
        hide: jest.fn(),
        renameItemHandler: jest.fn(),
        itemName: 'page name',
        nameHandler: jest.fn(),
        btnLoad: true,
      };
      const updateModal = shallow(<ItemRename {...itemRenameProps} />);
      test('update text changed to updating', () => {
        expect(updateModal.find('Button').at(1).text()).toEqual('global.updatingdot');
      });
    });
  });
});
