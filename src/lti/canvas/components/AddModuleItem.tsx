import React, { Component, ChangeEventHandler } from 'react';
import { Button, Modal, Form, Row, Col } from 'react-bootstrap';
import FocusLock from 'react-focus-lock';
import Loading from '../../../components/loader/loading';
import AddTemplateItem, { TemplateType } from './AddTemplatesItem';
import { LtiIcons } from '../../icons';
import { translate } from '../../../i18n/translate';

export type PageLists = {
  created_at: string;
  editing_roles: string;
  front_page: boolean;
  hide_from_students: boolean;
  html_url: string;
  last_edited_by: {
    id: number | string;
    display_name: string;
    avatar_image_url: string;
    html_url: string;
  };
  pronouns: null | string;
  locked_for_user: boolean;
  page_id: number | string;
  published: boolean;
  title: string;
  todo_date: null;
  updated_at: string;
  url: string;
};
type CategoryData = {
  id: string;
  ltiPlatformID: string;
  name: string;
  createdAt: string;
  updatedAt: string;
};
export type TemplateList = {
  active: boolean;
  category: CategoryData;
  categoryID: string;
  content: string;
  createdAt: string;
  createdBy: string;
  id: string;
  isGlobal: boolean;
  isShared: null | boolean;
  ltiPlatformID: string;
  owner: string;
  status: boolean;
  thumbnail: { bucket: string; key: string; region: string };
  title: string;
  updatedAt: string;
};

type RenamedTemplatesToAdd = {
  id: string;
  name: string;
  pageName: string;
};
export interface AddItemProps {
  show: boolean;
  hide(): void;
  moduleName: string;
  pageList: Array<PageLists>;
  getPageLoading: boolean;
  isTemplatePageLoading: boolean;
  handleAdd(event: React.MouseEvent<HTMLElement>, pageName: string): void;
  handleExistingAdd(pageUrl: never[], isMultiplePage: boolean): void;
  addItemLoading: boolean;
  selectItemType(itemTypeChange: string): void;
  itemType: string;
  TemplatesLists: TemplateList[];
  handleTemplateAdd(event: React.MouseEvent<HTMLElement>, selectedTemplates: _Any): void;
  fetchTemplates(type: TemplateType): void;
  globalTemplatesLists: never[];
  sharedTemplatesLists: never[];
  templateType: TemplateType;
}

type AddItemStateTypes = {
  pageList: Array<PageLists>;
  selectedItem: _Any;
  isCreatePage: boolean;
  pageName: string;
  selectedTemplateItem: _Any;
  pageNamingModal: boolean;
  pageRenameArray: RenamedTemplatesToAdd[];
  allowEdit: boolean;
  viewType: string;
};

class AddItem extends Component<AddItemProps, AddItemStateTypes> {
  state: AddItemStateTypes = {
    pageList: [],
    selectedItem: [],
    isCreatePage: false,
    pageName: '',
    selectedTemplateItem: [],
    pageNamingModal: false,
    pageRenameArray: [],
    allowEdit: false,
    viewType: 'list',
  };

  componentDidMount() {
    this.setState({ pageList: this.props.pageList });
  }

  componentDidUpdate(prevProps: AddItemProps) {
    if (this.props.pageList !== prevProps.pageList) {
      // eslint-disable-next-line  react/no-did-update-set-state
      this.setState({ pageList: this.props.pageList });
    }
  }

  pageNameHandler: ChangeEventHandler<HTMLInputElement> = (event) => {
    this.setState({
      pageName: event.target.value,
    });
  };

  onSelectedItemChange = (event: _Any) => {
    const selected: string[] = [];
    const selectedPage =
      this.state.viewType === 'tab'
        ? document.getElementsByClassName('page-grid-active')
        : event.target.selectedOptions;
    for (let i = 0; i < selectedPage.length; i++) {
      selected.push(this.state.viewType === 'tab' ? selectedPage[i].id : selectedPage[i].value);
    }
    this.setState({
      selectedItem: selected,
      pageName: '',
    });
  };

  handleTemplatesBack = () => {
    if (this.state.pageNamingModal) {
      this.setState({
        pageNamingModal: false,
      });
      this.resetToDefaultStateValues();
      this.props.selectItemType('Templates');
    } else {
      this.resetToDefaultStateValues();
      this.props.selectItemType('Pages');
    }
  };

  onSelectedTemplateItemChange: ChangeEventHandler<HTMLSelectElement> = (event) => {
    const isTemplateGrid = event.target.parentElement?.classList.contains('template-grid');
    const selected: { name: string; id: number | string }[] = [];
    const selectedTemplates = isTemplateGrid
      ? document.getElementsByClassName('template-grid-active')
      : event.target.selectedOptions;
    for (let i = 0; i < selectedTemplates.length; i++) {
      if (isTemplateGrid) {
        selected.push({
          name: selectedTemplates[i].childNodes[1].textContent ?? '',
          id: selectedTemplates[i].id,
        });
      } else {
        const option = selectedTemplates[i] as HTMLOptionElement;
        selected.push({
          name: option.text,
          id: option.value,
        });
      }
    }
    this.setState({
      selectedTemplateItem: selected,
    });
  };

  handleSave = (event: React.MouseEvent<HTMLElement>) => {
    if (this.state.selectedItem.length === 1 && this.state.selectedItem[0] === 'new') {
      this.props.handleAdd(event, this.state.pageName);
    } else {
      this.props.handleExistingAdd(this.state.selectedItem, true);
    }
  };

  handleTemplatesNext = () => {
    if (this.props.itemType === 'Pages' && this.state.selectedTemplateItem.length === 0) {
      this.resetToDefaultStateValues();
      this.props.selectItemType('Templates');
    } else if (this.props.itemType === 'Templates' && this.state.selectedTemplateItem.length > 0) {
      this.setState({
        pageNamingModal: true,
        pageRenameArray: this.state.selectedTemplateItem,
      });
    }
  };

  handleTemplatesFetch = async (event: React.ChangeEvent<HTMLInputElement>) => {
    this.resetToDefaultStateValues();
    await this.props.fetchTemplates(parseInt(event.target.value) as TemplateType);
  };

  resetToDefaultStateValues = () => {
    this.setState({
      selectedItem: [],
      selectedTemplateItem: [],
      pageName: '',
      pageList: this.props.pageList,
      pageRenameArray: [],
    });
  };

  handleTemplateRename = (event: React.ChangeEvent<HTMLInputElement>) => {
    const toRenameData: Array<RenamedTemplatesToAdd> = this.state.pageRenameArray;
    for (const template of toRenameData) {
      const eventTargetDataKeyValue = event.target.getAttribute('data-key');
      if (template.id === eventTargetDataKeyValue) {
        toRenameData[toRenameData.indexOf(template)].pageName = event.target.value;
        const isTAllemplatesRenamed = toRenameData.every(
          (element: RenamedTemplatesToAdd) =>
            element.pageName && element.pageName.replace(/\s/g, '').length !== 0,
        );
        if (isTAllemplatesRenamed)
          this.setState({
            allowEdit: true,
            pageRenameArray: toRenameData,
          });
        else
          this.setState({
            allowEdit: false,
          });
      }
    }
  };

  addTemplateHandler = (event: React.MouseEvent<HTMLElement>) => {
    this.props.handleTemplateAdd(event, this.state.pageRenameArray);
  };

  handleModuleItemSearch = (eventTargetValue: string) => {
    this.setState({ selectedItem: [] });
    const searchText = eventTargetValue.toLowerCase();
    const searchList = [];
    for (let i = 0; i < this.props.pageList.length; i++) {
      const pageTitle = this.props.pageList[i].title;
      if (pageTitle.toLowerCase().includes(searchText)) searchList.push(this.props.pageList[i]);
    }
    this.setState({ pageList: searchList });
  };

  handlePageClick = (event: _Any) => {
    let pageWrapper = event.target;
    while (!pageWrapper.classList.contains('page-grid')) {
      pageWrapper = pageWrapper.parentElement;
    }
    pageWrapper?.classList.toggle('page-grid-active');
    this.onSelectedItemChange(event);
  };

  render() {
    return (
      <>
        <Modal
          className='loree-canvas-additem-modal-content-header'
          size='lg'
          show={this.props.show}
          onHide={this.props.hide}
          backdrop='static'
          centered
          aria-describedby='baby'
        >
          <FocusLock className='focus-lock'>
            <Modal.Header closeButton data-testid='close' closeLabel={translate('global.close')}>
              <Modal.Title id='add-module-item'>
                <h5 className='text-primary' id='modal_header' aria-level={1}>
                  {translate('module.addmoduleitemdetail', { detail: this.props.moduleName })}
                </h5>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body style={{ minHeight: '200px' }}>
              {this.props.getPageLoading ? (
                <Loading />
              ) : this.props.itemType === 'Pages' ? (
                <div>
                  <Form.Text className='h6' role='heading' aria-level={2}>
                    {translate('module.addmodulepagedetail', { detail: this.props.moduleName })}
                  </Form.Text>
                  <div className='canvas-list-modal-container'>
                    <div className='d-flex justify-content-between'>
                      <Form.Text className='h6 font-weight-bold m-2' role='heading' aria-level={3}>
                        {translate('module.defaultcanvaspages')}
                      </Form.Text>
                      <div className='d-flex justify-content-end'>
                        <Form.Group className='mb-0'>
                          <Form.Control
                            id='moduleItemSearch'
                            title={translate('module.addformtitle')}
                            className='module-item-search-bar d-block mr-2'
                            type='text'
                            autoComplete='off'
                            placeholder={translate('global.search')}
                            onChange={(e) => this.handleModuleItemSearch(e.target.value)}
                          />
                        </Form.Group>
                        <LtiIcons.SearchIcon
                          id='moduleItemSearchIcon'
                          width='20px'
                          height='30px'
                          fill='#909090'
                          className='module-item-search-icon mr-2 pages-search-bar-active position-absolute'
                        />
                        <LtiIcons.ListIcon
                          id='templateListIcon'
                          width='20px'
                          height='30px'
                          fill={this.state.viewType === 'list' ? '#000d9c' : '#909090'}
                          className='mr-2'
                          onClick={() => this.setState({ viewType: 'list', selectedItem: [] })}
                        />
                        <LtiIcons.TabIcon
                          id='templatGridIcon'
                          width='20px'
                          height='30px'
                          fill={this.state.viewType === 'tab' ? '#000d9c' : '#909090'}
                          onClick={() => this.setState({ viewType: 'tab', selectedItem: [] })}
                        />
                      </div>
                    </div>
                    <Form.Text className='text-muted m-2' id='add_page_instructions'>
                      {translate('module.adddescribe')}
                    </Form.Text>

                    {this.state.pageList.length > 0 && this.state.viewType === 'list' ? (
                      <div>
                        <Form.Control
                          data-testid='select-list'
                          aria-labelledby='add_page_instructions'
                          as='select'
                          multiple
                          htmlSize={9}
                          custom
                          onChange={this.onSelectedItemChange}
                        >
                          <option value='new'>[ {translate('module.newblankpage')} ]</option>
                          <option value='new loree page'>
                            [ {translate('module.newpagefromloree')} ]
                          </option>
                          {this.state.pageList.map((page: PageLists, index: number) => (
                            <option value={page.url} key={index}>
                              {page.title}
                            </option>
                          ))}
                        </Form.Control>
                      </div>
                    ) : this.state.pageList.length > 0 && this.state.viewType === 'tab' ? (
                      <div className='d-flex flex-row flex-wrap page-grid-container'>
                        <div
                          id='new'
                          className='m-2 page-grid'
                          onClick={(e) => this.handlePageClick(e)}
                          onKeyDown={() => {}}
                          role='presentation'
                        >
                          <div className='mb-2 page-grid-item-new'>
                            <LtiIcons.BlankPageIcon
                              id='blankPageIcon'
                              width='100px'
                              height='100px'
                            />
                          </div>
                          <p className='text-center text-truncate page-grid-text'>
                            {translate('module.blankpage')}
                          </p>
                        </div>
                        <div
                          id='new loree page'
                          className='m-2 page-grid'
                          onClick={(e) => this.handlePageClick(e)}
                          onKeyDown={() => {}}
                          role='presentation'
                        >
                          <div className='mb-2 page-grid-item-new'>
                            <LtiIcons.TemplateIcon id='templateIcon' width='100px' height='100px' />
                          </div>
                          <p className='text-center text-truncate page-grid-text'>
                            {translate('module.newpage')}
                          </p>
                        </div>
                        {this.state.pageList.map((page: PageLists, index: number) => (
                          <div
                            key={index}
                            id={page.url}
                            className='m-2 page-grid'
                            onClick={(e) => this.handlePageClick(e)}
                            onKeyDown={() => {}}
                            role='presentation'
                          >
                            <div className='mb-2 page-grid-item' />
                            <p
                              className='text-center text-truncate page-grid-text'
                              title={page.title}
                            >
                              {page.title}
                            </p>
                          </div>
                        ))}
                      </div>
                    ) : (
                      <div>
                        <Form.Control
                          as='select'
                          multiple
                          htmlSize={9}
                          custom
                          onChange={this.onSelectedItemChange}
                        >
                          <option value='new'>[ {translate('module.newblankpage')} ]</option>
                          <option value='new loree page'>
                            [ {translate('module.newpagefromloree')} ]
                          </option>
                        </Form.Control>
                        <div className='p-1'>
                          <div className='p-1' id='search_result_tooltip'>
                            {translate('page.nopagesfound')}
                          </div>
                        </div>
                      </div>
                    )}
                    {this.state.selectedItem.length === 1 && this.state.selectedItem[0] === 'new' && (
                      <Form.Group as={Row} controlId='formPlaintextPassword' className='mt-4'>
                        <Form.Label column sm='3'>
                          {translate('page.pagename')}:
                        </Form.Label>
                        <Col sm='8'>
                          <Form.Control
                            type='text'
                            placeholder={translate('page.pagename')}
                            maxLength={255}
                            onChange={this.pageNameHandler}
                          />
                          {this.state.pageName.length > 254 ? (
                            <small className='text-danger'>
                              {translate('global.charlimitnote')}
                            </small>
                          ) : (
                            ''
                          )}
                        </Col>
                      </Form.Group>
                    )}
                  </div>
                </div>
              ) : (
                <AddTemplateItem
                  show={this.props.show}
                  hide={this.props.hide}
                  moduleName={this.props.moduleName}
                  pageList={this.props.pageList}
                  isTemplatePageLoading={this.props.isTemplatePageLoading}
                  handleAdd={this.props.handleAdd}
                  handleExistingAdd={this.props.handleExistingAdd}
                  addItemLoading={this.props.addItemLoading}
                  itemType={this.props.itemType}
                  onSelectedTemplateItemChange={this.onSelectedTemplateItemChange}
                  TemplatesLists={this.props.TemplatesLists}
                  handleTemplatesFetch={this.handleTemplatesFetch}
                  globalTemplatesLists={this.props.globalTemplatesLists}
                  sharedTemplatesLists={this.props.sharedTemplatesLists}
                  templateType={this.props.templateType}
                  pageNamingModal={this.state.pageNamingModal}
                  selectedTemplateItem={this.state.selectedTemplateItem}
                  handleTemplateRename={this.handleTemplateRename}
                  viewType={this.state.viewType}
                />
              )}
            </Modal.Body>
            <Modal.Footer className='d-flex justify-content-center loree-canvas-image-modal-content-footer button'>
              {this.props.itemType === 'Templates' ? (
                <Button
                  variant='outline-primary'
                  className='mx-1'
                  size='sm'
                  value='Pages'
                  onClick={() => this.handleTemplatesBack()}
                >
                  {translate('global.back')}
                </Button>
              ) : null}
              <Button
                variant='outline-primary'
                onClick={this.props.hide}
                size='sm'
                className='mx-1'
              >
                {translate('global.cancel')}
              </Button>
              {(this.state.selectedItem.length === 1 &&
                this.state.selectedItem[0] === 'new loree page' &&
                !this.state.pageNamingModal) ||
              (this.props.itemType === 'Templates' && !this.state.pageNamingModal) ? (
                <Button
                  variant='primary'
                  disabled={
                    this.props.itemType === 'Templates' &&
                    this.state.selectedTemplateItem.length === 0
                  }
                  className='mx-1'
                  size='sm'
                  onClick={() => this.handleTemplatesNext()}
                >
                  {translate('global.next')}
                </Button>
              ) : this.props.itemType === 'Pages' ||
                (this.props.itemType === 'Templates' && this.state.pageNamingModal) ? (
                <Button
                  variant='primary'
                  size='sm'
                  className='mx-1'
                  onClick={
                    this.props.itemType === 'Pages'
                      ? (e) => this.handleSave(e)
                      : (e) => this.addTemplateHandler(e)
                  }
                  disabled={
                    this.props.itemType === 'Pages'
                      ? this.state.selectedItem.length <= 0 ||
                        (this.state.selectedItem.length >= 1 &&
                          this.state.selectedItem[0] === 'new' &&
                          this.state.pageName.replace(/\s/g, '').length === 0) ||
                        this.props.addItemLoading ||
                        (this.state.selectedItem.length >= 1 &&
                          (this.state.selectedItem[0] === 'new loree page' ||
                            this.state.selectedItem[1] === 'new loree page'))
                      : this.state.selectedTemplateItem.length <= 0 ||
                        this.props.addItemLoading ||
                        !this.state.pageNamingModal ||
                        !this.state.allowEdit
                  }
                >
                  {this.props.addItemLoading
                    ? translate('module.addingitem')
                    : translate('module.additem')}
                </Button>
              ) : null}
            </Modal.Footer>
          </FocusLock>
        </Modal>
      </>
    );
  }
}

export default AddItem;
