import React, { Component } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import FocusLock from 'react-focus-lock';
import { translate } from '../../../i18n/translate';
interface CreateRenameProps {
  show: boolean;
  hide(): void;
  isCreate: boolean;
  saveMod(event: React.MouseEvent<HTMLElement>): void;
  moduleName: string;
  nameHandler(event: React.ChangeEvent<HTMLInputElement>): void;
  btnLoad: boolean;
}

class CreateRename extends Component<CreateRenameProps> {
  render() {
    return (
      <>
        <Modal
          show={this.props.show}
          onHide={this.props.hide}
          backdrop='static'
          keyboard={false}
          centered
        >
          <FocusLock className='focus-lock'>
            <Modal.Header closeButton>
              <Modal.Title id='create-new-module'>
                <h5 className='text-primary'>
                  {this.props.isCreate
                    ? translate('module.addmodule')
                    : translate('module.renamemodule')}
                </h5>
              </Modal.Title>
            </Modal.Header>
            <Form onSubmit={(event) => event.preventDefault()}>
              <Modal.Body>
                <Form.Group controlId='formPageName'>
                  <Form.Label>{translate('module.modulename')}</Form.Label>
                  <Form.Control
                    required
                    data-test='moduleName'
                    type='text'
                    maxLength={255}
                    placeholder={translate('module.modulenameplaceholder')}
                    autoComplete='off'
                    value={this.props.moduleName}
                    onChange={this.props.nameHandler}
                  />
                  {this.props.moduleName.length > 254 ? (
                    <small className='text-danger'>{translate('global.charlimitnote')}</small>
                  ) : (
                    ''
                  )}
                </Form.Group>
              </Modal.Body>
              <Modal.Footer className='d-flex justify-content-center'>
                <Button
                  variant='outline-primary'
                  onClick={this.props.hide}
                  size='sm'
                  className='mx-1'
                >
                  {translate('global.cancel')}
                </Button>
                <Button
                  variant='primary'
                  className='mx-1'
                  size='sm'
                  onClick={this.props.saveMod}
                  disabled={
                    this.props.btnLoad || this.props.moduleName.replace(/\s/g, '').length === 0
                  }
                >
                  {this.props.btnLoad
                    ? `${
                        this.props.isCreate
                          ? translate('global.adding')
                          : translate('global.updating')
                      }`
                    : `${
                        this.props.isCreate ? translate('global.add') : translate('global.update')
                      }`}
                </Button>
              </Modal.Footer>
            </Form>
          </FocusLock>
        </Modal>
      </>
    );
  }
}

export default CreateRename;
