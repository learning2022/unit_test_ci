import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BrowserRouter } from 'react-router-dom';
import { API } from 'aws-amplify';

import axe from '../../../axeHelper';
import PagesLists from './index';
import { canvasPagesResult } from './mockData';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('Canvas Module(s) Pages Lists', () => {
  let pages: RenderResult;
  beforeEach(() => {
    API.graphql = jest.fn().mockReturnValue(canvasPagesResult);
    pages = render(
      <BrowserRouter>
        <PagesLists />
      </BrowserRouter>,
    );
  });
  test.skip('should pass accessibility tests', async () => {
    const results = await axe(pages.container, {
      rules: {
        'valid-table-caption': { enabled: false },
        tableWhiteSpace: { enabled: false },
      },
    });
    expect(results).toHaveNoViolations();
  });

  test('should tab and wrap focus in create new page modal', () => {
    userEvent.click(pages.getByText('page.pluspage'));
    // 1. Close Button
    const closeButton = pages.getByText('global.close');
    expect(closeButton.parentElement).toHaveFocus();
    // 2. Page name input
    userEvent.tab();
    const searchInput = pages.getByPlaceholderText('page.pagenameplaceholder');
    expect(searchInput).toHaveFocus();
    // 3. Cancel Select
    userEvent.tab();
    const cancelButton = pages.getByText('global.cancel');
    expect(cancelButton).toHaveFocus();
    // 1. Close Button - back to the first focusable element
    userEvent.tab();
    expect(closeButton.parentElement).toHaveFocus();
  });
});
