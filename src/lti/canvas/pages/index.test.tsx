import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import PagesLists from './index';
import { API } from 'aws-amplify';
import { act, fireEvent, render, screen, configure } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  canvasPagesResult,
  createPageResponse,
  publishToCanvasSuccess,
  publishToCanvasError,
  canvasPagesError,
  canvasPagesUnOrderingResult,
  canvasPagesOrderingResult,
  canvasPages404Result,
  errorPageData,
  orderTitleTableResult,
  orderCreatedDateTableResult,
  orderLastEditTableResult,
} from './mockData';
import CONSTANTS from '../../../loree-editor/constant';

configure({ testIdAttribute: 'class' });

const waitForComponentToUpdate = async (wrapper: ReactWrapper) => {
  await act(async () => {
    await new Promise((resolve) => setTimeout(resolve, 0));
    wrapper.update();
  });
};

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#Load more canvas Page content', () => {
  sessionStorage.setItem('course_id', '875');
  CONSTANTS.LOREE_PER_PAGE_ITEM = 1;

  beforeEach(() => {
    document.body.innerHTML = '';
  });
  test('Ordering page content with 404 statusCode', async () => {
    API.graphql = jest.fn().mockImplementation(() => canvasPagesUnOrderingResult);
    await act(async () => {
      await new Promise((resolve) => setTimeout(resolve, 0));
      render(
        <Router>
          <PagesLists />
        </Router>,
      );
      await new Promise((resolve) => setTimeout(resolve, 0));
      fireEvent.click(screen.getByText('global.loadmore'));
    });
    expect(API.graphql).toHaveBeenCalledTimes(2);
  });
});
describe('#Sorting canvas Page content', () => {
  sessionStorage.setItem('course_id', '875');
  beforeEach(() => {
    document.body.innerHTML = '';
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasPagesUnOrderingResult)
      .mockImplementationOnce(() => canvasPagesOrderingResult);
  });

  test.each([
    { columnName: 'pagetable.pagetitle', expectedResult: orderTitleTableResult },
    { columnName: 'pagetable.pagecreationdate', expectedResult: orderCreatedDateTableResult },
    { columnName: 'pagetable.pagelastedit', expectedResult: orderLastEditTableResult },
  ])('Ordering page content', async ({ columnName, expectedResult }) => {
    await act(async () => {
      await new Promise((resolve) => setTimeout(resolve, 0));
      render(
        <Router>
          <PagesLists />
        </Router>,
      );
      await new Promise((resolve) => setTimeout(resolve, 0));
      fireEvent.click(screen.getByText(columnName));
    });
    expect(document.getElementsByTagName('table')[0].outerHTML).toBe(expectedResult);
  });
});
describe('#Sorting canvas Page content with failed case in API', () => {
  sessionStorage.setItem('course_id', '875');
  beforeEach(() => {
    document.body.innerHTML = '';
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasPagesUnOrderingResult)
      .mockImplementationOnce(() => canvasPages404Result);
  });
  test('Ordering page content with 404 statusCode', async () => {
    await act(async () => {
      await new Promise((resolve) => setTimeout(resolve, 0));
      render(
        <Router>
          <PagesLists />
        </Router>,
      );
      await new Promise((resolve) => setTimeout(resolve, 0));
      fireEvent.click(screen.getByText('pagetable.pagetitle'));
    });
    expect(document.getElementsByClassName('jumbotron')[0].innerHTML).toBe(errorPageData);
  });
});

describe('#CanvasPages', () => {
  let pages: ReactWrapper;
  describe('when canvas pages API returns result', () => {
    beforeEach(async () => {
      API.graphql = jest.fn().mockReturnValue(canvasPagesResult);
      pages = mount(
        <Router>
          <PagesLists />
        </Router>,
      );
      await waitForComponentToUpdate(pages);
    });
    test('assignment list is rendered', () => {
      expect(pages.find('tr')).toHaveLength(2);
    });
    describe('when click on + Page button', () => {
      test('open create modal', async () => {
        await pages.find('button').at(0).simulate('click');
        await waitForComponentToUpdate(pages);
        expect(document.getElementsByClassName('modal')).toHaveLength(1);
      }, 15000);
    });
    describe('when type only empty space in page name', () => {
      test('create button is disabled', async () => {
        fireEvent.change(document.getElementsByTagName('input')[0], { target: { value: ' ' } });
        await waitForComponentToUpdate(pages);
        expect(document.getElementsByClassName('btn-primary')[0]).toHaveAttribute('disabled');
      });
    });
    describe('when type valid name in page name', () => {
      test('create button is enabled', async () => {
        fireEvent.change(document.getElementsByTagName('input')[0], {
          target: { value: ' page name' },
        });
        await waitForComponentToUpdate(pages);
        expect(document.getElementsByClassName('btn-primary')[0]).not.toHaveAttribute('disabled');
      });
    });
    describe('when click on create button', () => {
      beforeEach(() => {
        API.graphql = jest.fn().mockReturnValue(createPageResponse);
      });
      test('text is changed to creating', async () => {
        fireEvent.click(document.getElementsByClassName('btn-primary')[0]);
        expect(document.getElementsByClassName('btn-primary')[0].textContent).toEqual(
          'global.creating',
        );
      });
    });
    describe('when click on publish icon and API returns success', () => {
      beforeEach(async () => {
        API.graphql = jest.fn().mockReturnValue(publishToCanvasSuccess);
      });
      test('page is published', async () => {
        pages.find('button').at(1).simulate('click');
        await waitForComponentToUpdate(pages);
        expect(pages.find('button').at(1).props().title).toEqual('page.unpublishbutton');
      });
    });
    describe('when click on publish icon and API got failed', () => {
      beforeEach(async () => {
        API.graphql = jest.fn().mockReturnValue(publishToCanvasError);
      });
      test('page is not published', async () => {
        pages.find('button').at(1).simulate('click');
        await waitForComponentToUpdate(pages);
        expect(pages.find('h1').text()).toEqual('error.error');
      });
    });
  });
  describe('when canvas pages API got failed', () => {
    beforeEach(async () => {
      API.graphql = jest.fn().mockReturnValue(canvasPagesError);
      pages = mount(
        <Router>
          <PagesLists />
        </Router>,
      );
      await waitForComponentToUpdate(pages);
    });
    test('pages component rendered with error', () => {
      expect(pages.find('h1').text()).toEqual('error.error');
    });
  });
});
