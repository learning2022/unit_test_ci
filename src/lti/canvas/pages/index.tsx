/* eslint-disable @typescript-eslint/promise-function-async */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { Dropdown, Modal, Form, Button, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faCheckCircle,
  faBan,
  faEllipsisV,
  faCaretDown,
  faCaretUp,
} from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import FocusLock from 'react-focus-lock';
import { translate } from '../../../i18n/translate';
import ToastComponent from '../../components/ToastComponent';
import Loader from '../../../components/loader/loading';
import { canvasPages } from '../../../graphql/queries';
import {
  createToCanvas,
  updateCanvasPage,
  publishToCanvas,
  deleteCanvasPage,
  duplicateCanvasPage,
} from '../../../graphql/mutations';
import Error from '../components/Error';
import ConfirmAlert from '../components/confirmAlert';
import CONSTANTS from '../../../loree-editor/constant';
import {
  CanvasPagesQuery,
  PublishToCanvasMutation,
  DeleteCanvasPageMutation,
  UpdateCanvasPageMutation,
  CreateToCanvasMutation,
  DuplicateCanvasPageMutation,
} from '../../../API';
class PagesLists extends Component {
  state = {
    pageIndex: 1,
    pageItems: CONSTANTS.LOREE_PER_PAGE_ITEM,
    loading: true,
    loadingContent: false,
    loadingDataContent: false,
    moreContent: false,
    pagesArr: [],
    error: false,
    pageName: '',
    pageUrl: '',
    createPageShow: false,
    updatePage: false,
    disableCreateButton: true,
    showToast: false,
    message: '',
    showDelete: false,
    btnLoading: false,
    deleteBtnLoading: false,
    titleOrder: 'desc', // Orders are in inverted state
    createdAtOrder: 'asc',
    updatedAtOrder: 'asc',
    activeColumn: 'title',
    activeColumnOrder: 'asc',
  };

  async componentDidMount() {
    const courseId = sessionStorage.getItem('course_id');
    const response = await API.graphql<CanvasPagesQuery>(
      graphqlOperation(canvasPages, {
        courseId: courseId,
        pageIndex: this.state.pageIndex,
        pageItems: this.state.pageItems,
        orderType: this.state.activeColumnOrder,
        sortingValue: this.state.activeColumn,
      }),
    );
    const parseResponse = JSON.parse(response.data?.canvasPages as string);
    if (parseResponse.statusCode === 200) {
      if (parseResponse.body.length < this.state.pageItems) {
        this.setState({
          moreContent: false,
        });
      } else {
        this.setState({
          moreContent: true,
        });
      }
      this.setState({
        pageIndex: this.state.pageIndex + 1,
        pagesArr: this.state.pagesArr.concat(parseResponse.body),
        loading: false,
        loadingContent: false,
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  }

  togglePagePublish = async (pageUrl: string, status: boolean) => {
    const courseId = sessionStorage.getItem('course_id');
    const response = await API.graphql<PublishToCanvasMutation>(
      graphqlOperation(publishToCanvas, {
        courseId: courseId,
        pageId: pageUrl,
        status: !status,
        type: 'PAGE',
      }),
    );
    const parseResponse = JSON.parse(response.data?.publishToCanvas as string);
    if (parseResponse.statusCode === 200) {
      const pages: Array<{ page_id: string }> = this.state.pagesArr;
      pages.forEach((page: { page_id: string }, index: number) => {
        if (page.page_id === parseResponse.body.page_id) {
          pages[index] = parseResponse.body;
        }
      });
      this.setState({
        pagesArr: pages,
        showToast: true,
        message: !status
          ? translate('modulepage.publishsuccess')
          : translate('modulepage.unpublishsuccess'),
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  editPageName = (title: string, pageUrl: string) => {
    this.setState({
      createPageShow: true,
      updatePage: true,
      pageName: title,
      pageUrl: pageUrl,
    });
  };

  confirmDeletePage = async () => {
    const courseId = sessionStorage.getItem('course_id');
    this.setState({
      showDelete: false,
      deleteBtnLoading: true,
    });
    const response = await API.graphql<DeleteCanvasPageMutation>(
      graphqlOperation(deleteCanvasPage, {
        courseId: courseId,
        pageUrl: this.state.pageUrl,
      }),
    );
    const parseResponse = JSON.parse(response.data?.deleteCanvasPage as string);
    if (parseResponse.statusCode === 200) {
      // Add item to it
      const pageNew: Array<{ page_id: string }> = this.state.pagesArr;
      pageNew.forEach((page: { page_id: string }, index: number) => {
        if (page.page_id === parseResponse.body.page_id) pageNew.splice(index, 1);
      });
      this.setState({
        deleteBtnLoading: false,
        pagesArr: pageNew,
        showToast: true,
        message: translate('page.deletesuccess'),
        pageUrl: '',
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  setcreateModuleShow = (value: boolean) => {
    this.setState({
      createPageShow: value,
      pageName: '',
      pageUrl: '',
      updatePage: false,
      disableCreateButton: true,
      btnLoading: false,
    });
  };

  pageNameHandler = (event: React.ChangeEvent) => {
    this.setState(
      {
        pageName: (event.target as HTMLInputElement)?.value,
      },
      this.validateCreatePageButton.bind(this),
    );
  };

  validateCreatePageButton() {
    if (this.state.pageName.trim().length > 0) {
      this.setState({ disableCreateButton: false });
    } else {
      this.setState({ disableCreateButton: true });
    }
  }

  handleUpdatePage = async (event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ btnLoading: true });
    const courseId = sessionStorage.getItem('course_id');

    const response = await API.graphql<UpdateCanvasPageMutation>(
      graphqlOperation(updateCanvasPage, {
        courseId: courseId,
        pageUrl: this.state.pageUrl,
        pageName: escape(this.state.pageName),
      }),
    );
    const parseResponse = JSON.parse(response.data?.updateCanvasPage as string);
    if (parseResponse.statusCode === 200) {
      const pageNew: Array<{ page_id: string }> = this.state.pagesArr;
      pageNew.forEach((page: { page_id: string }, index: number) => {
        if (page.page_id === parseResponse.body.page_id) {
          pageNew[index] = parseResponse.body;
        }
      });
      this.setState({
        pagesArr: pageNew,
        pageUrl: '',
        pageName: '',
        createPageShow: false,
        updatePage: false,
        showToast: true,
        message: translate('page.renamesuccess'),
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  openCreateModal = () => {
    this.setState({
      createPageShow: true,
    });
  };

  handleCreatePage = async (event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ btnLoading: true });
    const courseId = sessionStorage.getItem('course_id');

    const response = await API.graphql<CreateToCanvasMutation>(
      graphqlOperation(createToCanvas, {
        courseId: courseId,
        Name: escape(this.state.pageName),
        type: 'PAGE',
      }),
    );
    const parseResponse = JSON.parse(response.data?.createToCanvas as string);
    if (parseResponse.statusCode === 200) {
      const pageNew: Array<{}> = this.state.pagesArr;
      // Add item to it
      pageNew.push(parseResponse.body);
      this.setState({
        pagesArr: pageNew,
        btnLoading: false,
        createPageShow: false,
        pageName: '',
        showToast: true,
        message: translate('page.createsuccess'),
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  duplicatePage = async (pageUrl: string) => {
    const courseId = sessionStorage.getItem('course_id');
    const response = await API.graphql<DuplicateCanvasPageMutation>(
      graphqlOperation(duplicateCanvasPage, {
        courseId: courseId,
        pageUrl: pageUrl,
      }),
    );
    const parseResponse = JSON.parse(response.data?.duplicateCanvasPage as string);
    if (parseResponse.statusCode === 200) {
      const pageNew: Array<{}> = this.state.pagesArr;
      // Add item to it
      pageNew.push(parseResponse.body);
      this.setState({
        pagesArr: pageNew,
        showToast: true,
        message: translate('page.duplicatepage'),
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  loadMore = () => {
    this.setState({
      loadingContent: true,
    });
    void this.componentDidMount();
  };

  orderingPageData = async (sortingSection: string, orderingType: string) => {
    this.setState({
      loadingDataContent: true,
      moreContent: true,
      loadingContent: true,
    });
    const courseId = sessionStorage.getItem('course_id');
    const response = await API.graphql<CanvasPagesQuery>(
      graphqlOperation(canvasPages, {
        courseId: courseId,
        pageIndex: 1,
        pageItems: this.state.pageItems,
        orderType: orderingType,
        sortingValue: sortingSection,
      }),
    );
    const parseResponse = JSON.parse(response.data?.canvasPages as string);
    if (parseResponse.statusCode === 200) {
      this.state.pagesArr = [];
      this.setState({
        moreContent: !(parseResponse.body.length < this.state.pageItems),
        pagesArr: this.state.pagesArr.concat(parseResponse.body),
        activeColumn: sortingSection,
        activeColumnOrder: orderingType,
        pageIndex: 2,
        loading: false,
        loadingContent: false,
        loadingDataContent: false,
      });
      this.toggleOrderType(sortingSection, orderingType);
    } else {
      this.setState({
        error: true,
        loadingDataContent: false,
      });
    }
  };

  toggleOrderType = (sortingSection: string, orderingType: string) => {
    const orderTypeToChange = orderingType === 'asc' ? 'desc' : 'asc';
    switch (sortingSection) {
      case 'title':
        this.setState({
          titleOrder: orderTypeToChange,
        });
        break;
      case 'created_at':
        this.setState({
          createdAtOrder: orderTypeToChange,
        });
        break;
      case 'updated_at':
        this.setState({
          updatedAtOrder: orderTypeToChange,
        });
        break;
    }
  };

  render() {
    return (
      <>
        {this.state.error ? (
          <Error />
        ) : this.state.loading ? (
          <Loader />
        ) : (
          <div>
            <div className='d-flex justify-content-end landing-add-btn'>
              <Button
                aria-label={translate('page.pluspage_aria')}
                type='submit'
                onClick={this.openCreateModal}
              >
                {translate('page.pluspage')}
              </Button>
            </div>
            {(this.state.pagesArr as Array<{}>).length > 0 ? (
              <div>
                <Table hover striped size='sm' summary={translate('pagetable.tablesummary')}>
                  <thead className='pageTheadElement'>
                    <tr>
                      <th scope='col'>
                        <span
                          className='clickableOrderContent'
                          onClick={async () =>
                            await this.orderingPageData('title', this.state.titleOrder)
                          }
                          aria-hidden='true'
                        >
                          {translate('pagetable.pagetitle')}
                          <span className='position-absolute ml-2'>
                            <FontAwesomeIcon
                              icon={this.state.titleOrder === 'asc' ? faCaretDown : faCaretUp}
                            />
                          </span>
                        </span>
                      </th>
                      <th scope='col'>
                        <span
                          className='clickableOrderContent'
                          onClick={async () =>
                            await this.orderingPageData('created_at', this.state.createdAtOrder)
                          }
                          aria-hidden='true'
                        >
                          {translate('pagetable.pagecreationdate')}
                          <span className='position-absolute ml-2'>
                            <FontAwesomeIcon
                              icon={this.state.createdAtOrder === 'asc' ? faCaretDown : faCaretUp}
                            />
                          </span>
                        </span>
                      </th>
                      <th scope='col'>
                        <span
                          className='clickableOrderContent'
                          onClick={async () =>
                            await this.orderingPageData('updated_at', this.state.updatedAtOrder)
                          }
                          aria-hidden='true'
                        >
                          {translate('pagetable.pagelastedit')}
                          <span className='position-absolute ml-2'>
                            <FontAwesomeIcon
                              icon={this.state.updatedAtOrder === 'asc' ? faCaretDown : faCaretUp}
                            />
                          </span>
                        </span>
                      </th>
                      <th className='text-center' scope='col'>
                        {translate('pagetable.pagepublishedstatus')}
                      </th>
                      <th className='text-center' scope='col'>
                        {translate('pagetable.pagesettings')}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {!this.state.loadingDataContent
                      ? (
                          this.state.pagesArr as Array<{
                            title: string;
                            front_page: boolean;
                            created_at: string;
                            updated_at: string;
                            url: string;
                            published: boolean;
                            last_edited_by: { display_name: string };
                          }>
                        ).map(
                          (
                            page: {
                              title: string;
                              front_page: boolean;
                              created_at: string;
                              updated_at: string;
                              url: string;
                              published: boolean;
                              last_edited_by: { display_name: string };
                            },
                            index: number,
                          ) => (
                            <tr key={index}>
                              <td aria-label={`${page.title}`}>
                                <Link
                                  to={`/lti/editor/${page.url}/page/${encodeURIComponent(
                                    page.title,
                                  )}`}
                                  className='canvasTitleWrap'
                                >
                                  {page.title}
                                </Link>
                                {page.front_page ? (
                                  <Button
                                    variant='dark'
                                    size='sm'
                                    className='canvas-front-page-button font-weight-bold'
                                  >
                                    {translate('page.frontpage')}
                                  </Button>
                                ) : (
                                  ''
                                )}
                              </td>
                              <td>{moment(page.created_at).format('MMM Do, YYYY')}</td>
                              <td>
                                {moment(page.updated_at).format('MMM Do, YYYY')}
                                {page.last_edited_by === undefined
                                  ? ''
                                  : translate('global.authoredby', {
                                      detail: page.last_edited_by.display_name,
                                    })}
                              </td>
                              <td
                                className='text-center'
                                aria-label={
                                  page.published
                                    ? translate('page.publishsuccess_aria')
                                    : translate('page.publishfail_aria')
                                }
                              >
                                <Button
                                  variant='link'
                                  title={
                                    page.published
                                      ? translate('page.unpublishbutton')
                                      : translate('page.publishbutton')
                                  }
                                  id='tooltip-top-pubModButton'
                                  className='pt-0 pb-0'
                                  onClick={() => this.togglePagePublish(page.url, page.published)}
                                >
                                  <FontAwesomeIcon
                                    icon={page.published ? faCheckCircle : faBan}
                                    className={`${page.published ? 'text-success' : 'text-muted'}`}
                                  />
                                </Button>
                              </td>
                              <td className='text-center'>
                                <Dropdown
                                  drop='down'
                                  alignRight
                                  className='mr-3'
                                  style={{ padding: '0px !important' }}
                                >
                                  <Dropdown.Toggle
                                    id='dropdown-menu-align-left'
                                    variant='link'
                                    className='no-caret pt-0 pb-0'
                                    aria-label={translate('page.pagesettings_aria', {
                                      page: page.title,
                                    })}
                                  >
                                    <FontAwesomeIcon icon={faEllipsisV} />
                                  </Dropdown.Toggle>
                                  <Dropdown.Menu>
                                    <Dropdown.Item
                                      aria-label={translate('global.renamedetail_aria', {
                                        detail: page.title,
                                      })}
                                      onClick={() => this.editPageName(page.title, page.url)}
                                    >
                                      {translate('global.rename')}
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                      aria-label={translate('global.duplicatedetail_aria', {
                                        detail: page.title,
                                      })}
                                      onClick={() => this.duplicatePage(page.url)}
                                    >
                                      {translate('global.duplicate')}
                                    </Dropdown.Item>
                                    <Dropdown.Item
                                      aria-label={translate('global.deletedetail_aria', {
                                        detail: page.title,
                                      })}
                                      onClick={() =>
                                        this.setState({ showDelete: true, pageUrl: page.url })
                                      }
                                    >
                                      {translate('global.delete')}
                                    </Dropdown.Item>
                                  </Dropdown.Menu>
                                </Dropdown>
                              </td>
                            </tr>
                          ),
                        )
                      : ''}
                  </tbody>
                </Table>
                {this.state.moreContent ? (
                  this.state.loadingContent ? (
                    <h4 className='textAlignmentCenter'>{translate('global.loading')}</h4>
                  ) : (
                    <Button onClick={this.loadMore}>{translate('global.loadmore')}</Button>
                  )
                ) : (
                  <h4 className='textAlignmentCenter'>{translate('global.nomorepages')}</h4>
                )}
              </div>
            ) : (
              <div className='container'>
                <div className='border'>
                  <div className='p-3'>{translate('page.nopagesfound')}</div>
                </div>
              </div>
            )}
          </div>
        )}
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.message}
            closeToast={this.closeToast}
          />
        )}
        <ConfirmAlert
          show={this.state.showDelete}
          type='page'
          confirm={() => this.confirmDeletePage()}
          onCancel={() => this.setState({ showDelete: false })}
          deleteBtn={this.state.deleteBtnLoading}
        />
        <Modal
          animation={false}
          aria-labelledby='create-new-page'
          backdrop='static'
          centered
          onHide={() => this.setcreateModuleShow(false)}
          show={this.state.createPageShow}
          size='sm'
        >
          <FocusLock className='focus-lock'>
            <Modal.Header closeButton closeLabel={translate('global.close')}>
              <Modal.Title id='create-new-page' className='pageBlock'>
                {this.state.updatePage ? translate('page.editpage') : translate('page.createpage')}
              </Modal.Title>
            </Modal.Header>
            <Form onSubmit={(event) => event.preventDefault()}>
              <Modal.Body className='create-page-form'>
                <Form.Group controlId='formPageName'>
                  <Form.Label>{translate('page.pagename')}</Form.Label>
                  <Form.Control
                    required
                    type='text'
                    maxLength={255}
                    placeholder={translate('page.pagenameplaceholder')}
                    autoComplete='off'
                    value={this.state.pageName}
                    onChange={(e) => this.pageNameHandler(e)}
                  />
                  {this.state.pageName.length > 254 ? (
                    <small className='text-danger'>{translate('global.charlimitnote')}</small>
                  ) : (
                    ''
                  )}
                </Form.Group>
              </Modal.Body>
              <Modal.Footer className='d-flex justify-content-center'>
                <Button
                  variant='outline-primary'
                  size='sm'
                  onClick={() => this.setcreateModuleShow(false)}
                >
                  {translate('global.cancel')}
                </Button>
                <Button
                  variant='primary'
                  size='sm'
                  disabled={this.state.btnLoading || this.state.pageName.trim() === ''}
                  onClick={
                    this.state.updatePage
                      ? (e) => this.handleUpdatePage(e)
                      : (e) => this.handleCreatePage(e)
                  }
                >
                  {this.state.btnLoading
                    ? `${
                        this.state.updatePage
                          ? translate('global.updating')
                          : translate('global.creating')
                      }`
                    : `${
                        this.state.updatePage
                          ? translate('global.update')
                          : translate('global.create')
                      }`}
                </Button>
              </Modal.Footer>
            </Form>
          </FocusLock>
        </Modal>
      </>
    );
  }
}

export default PagesLists;
