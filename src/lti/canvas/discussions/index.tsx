/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { Accordion, Card, Button, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faBan, faComments } from '@fortawesome/free-solid-svg-icons';
import Loader from '../../../components/loader/loading';
import { publishToCanvas } from '../../../graphql/mutations';
import { canvasDiscussions } from '../../../graphql/queries';
import Error from '../components/Error';
import ToastComponent from '../../components/ToastComponent';
import { translate } from '../../../i18n/translate';
class DiscussionLists extends Component {
  state = {
    loading: true,
    error: false,
    pinnedDicussion: [],
    unpinnedDiscussion: [],
    showToast: false,
    toastMessage: '',
  };

  async componentDidMount() {
    const courseId = sessionStorage.getItem('course_id');
    let response: any = await API.graphql(
      graphqlOperation(canvasDiscussions, {
        courseId: courseId,
        pageIndex: 1,
        pageItems: 100,
      }),
    );
    response = JSON.parse(response.data.canvasDiscussions);
    if (response.statusCode === 200) {
      this.setState({
        loading: false,
      });
      this.getPinnedUnpinnedDiscussion(response.body);
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  }

  toggleDiscussionPublish = async (discussionId: string, status: boolean) => {
    const courseId = sessionStorage.getItem('course_id');
    let response: any = await API.graphql(
      graphqlOperation(publishToCanvas, {
        courseId: courseId,
        discussionId: discussionId,
        status: !status,
        type: 'DISCUSSION',
      }),
    );
    console.log('publishDiscussionToModule', response);
    response = JSON.parse(response.data.publishToCanvas);
    if (response.statusCode === 200) {
      const pinnedArray = this.state.pinnedDicussion;
      const unpinnedArray = this.state.unpinnedDiscussion;
      if (response.body.pinned) {
        pinnedArray.forEach((item: any) => {
          if (item.id === response.body.id) item.published = response.body.published;
        });
        this.setState({
          pinnedDicussion: pinnedArray,
        });
      } else {
        unpinnedArray.forEach((item: any) => {
          if (item.id === response.body.id) item.published = response.body.published;
        });
        this.setState({
          unpinnedDiscussion: unpinnedArray,
        });
      }
      this.setState({
        showToast: true,
        toastMessage: !status
          ? translate('modulepage.publishsuccess')
          : translate('modulepage.unpublishsuccess'),
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  getPinnedUnpinnedDiscussion = (response: any) => {
    const arrOfDiscussion: any = response;
    if (arrOfDiscussion !== undefined && arrOfDiscussion.length > 0) {
      const pinDiscussion: any = []; // Array of Pinned discussion
      const unPinDiscussion: any = []; // Array of discussion
      arrOfDiscussion.forEach((item: any) => {
        if (item.pinned) {
          pinDiscussion.push(item);
        }
        if (!item.pinned) {
          unPinDiscussion.push(item);
        }
      });
      pinDiscussion.sort((a: any, b: any) => {
        return a.position - b.position;
      });
      this.setState({
        pinnedDicussion: pinDiscussion,
        unpinnedDiscussion: unPinDiscussion,
      });
    }
  };

  handleToggleAccordionIcon = (e: any) => {
    const collapsed =
      e.target.parentNode.parentNode.parentNode.parentNode.childNodes[1].className === 'collapse';
    if (!collapsed) {
      e.target.className = e.target.className.replace('icon-down-arrow', 'icon-right-arrow');
    } else {
      e.target.className = e.target.className.replace('icon-right-arrow', 'icon-down-arrow');
    }
  };

  render() {
    const discussionCategories = [translate('global.discussions'), translate('discussion.pinned')];
    const discussionArr = [this.state.pinnedDicussion, this.state.unpinnedDiscussion];
    return (
      <>
        {this.state.error ? (
          <Error />
        ) : this.state.loading ? (
          <Loader />
        ) : (
          <div>
            {discussionCategories.map((discussionCategoryName: any, index: any) => (
              <Accordion
                defaultActiveKey={`${index}`}
                style={{ cursor: 'pointer' }}
                key={index}
                className='mb-5'
              >
                <Card className='cardDivAccord' style={{ overflow: 'visible' }}>
                  <Card.Header className='p-1'>
                    <Row>
                      <Col>
                        <Accordion.Toggle
                          as={Button}
                          variant='link'
                          eventKey={`${index}`}
                          onClick={(e) => this.handleToggleAccordionIcon(e)}
                          className='icon-down-arrow text-decoration-none'
                        >
                          {discussionCategoryName}
                        </Accordion.Toggle>
                      </Col>
                    </Row>
                  </Card.Header>
                  <Accordion.Collapse eventKey={`${index}`} id='accordion-collapse-check'>
                    <Card.Body key={index} className='p-0'>
                      {discussionArr[index].length === 0 ? (
                        <div className='border p-3' key={index}>
                          {translate('discussion.nodiscussionfound', {
                            detail: discussionCategoryName,
                          })}
                        </div>
                      ) : (
                        discussionArr[index].map((discussion: any, index: any) => (
                          <div className='border' key={index}>
                            <div className={`p-3 ${discussion.published ? 'pageLeftBorder' : ''}`}>
                              <Button
                                variant='link'
                                title={
                                  discussion.published
                                    ? translate('page.unpublishbutton')
                                    : translate('page.publishbutton')
                                }
                                id='tooltip-top-pubModButton'
                                className='float-right mr-2 pt-0 pb-0 modPublishButton'
                                onClick={async () =>
                                  await this.toggleDiscussionPublish(
                                    discussion.id,
                                    discussion.published,
                                  )
                                }
                              >
                                <FontAwesomeIcon
                                  icon={discussion.published ? faCheckCircle : faBan}
                                  className={`${
                                    discussion.published ? 'text-success' : 'text-muted'
                                  }`}
                                />
                              </Button>
                              <FontAwesomeIcon
                                icon={faComments}
                                className={`mr-1 ${
                                  discussion.published ? 'text-success' : 'text-muted'
                                }`}
                              />{' '}
                              <Link
                                to={`/lti/editor/${discussion.id}/discussion/${encodeURIComponent(
                                  discussion.title,
                                )}`}
                              >
                                {discussion.title}
                              </Link>
                            </div>
                          </div>
                        ))
                      )}
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            ))}
          </div>
        )}
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
      </>
    );
  }
}
export default DiscussionLists;
