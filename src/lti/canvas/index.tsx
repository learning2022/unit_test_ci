import React, { useCallback, useState } from 'react';
import { Container, Form } from 'react-bootstrap';
import { translate } from '../../i18n/translate';
import ModuleLists from './modules';
import PagesLists from './pages';
import Assignments from './assignments';
import Discussions from './discussions';
import MemoizedLandingPageHeader from '../components/landingPageHeader';
import './canvas.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown, faCodeBranch } from '@fortawesome/free-solid-svg-icons';
import CONSTANTS from '../../../src/loree-editor/constant';
import HelpSection from '../helpSection';
import Space from '../../loree-editor/modules/space/space';

const CanvasHome = () => {
  const [canvasItem, setCanvasItem] = useState('modules');
  const [initialModuleList, setModuleList] = useState(false);

  // Updating Dropdown value
  const selectCanvasItem = useCallback(
    (event: React.ChangeEvent) => {
      event.preventDefault();
      event.stopPropagation();
      setCanvasItem((event.currentTarget as HTMLSelectElement).value);
    },
    [setCanvasItem],
  );

  return (
    <>
      <MemoizedLandingPageHeader
        btnText={translate('global.admin')}
        buttonUrl='/lti/editor/admin'
        isModuleListUpdated={initialModuleList}
      />
      <div className='d-flex justify-content-end help-section top-padding-pos'>
        <p>
          <FontAwesomeIcon icon={faCodeBranch} /> {CONSTANTS.LOREE_APP_VERSION}
        </p>
        <HelpSection space={new Space()} />
      </div>
      <Container fluid className='loree-frame p-10 pt-0'>
        <Form.Group className='canvasHomeDropdown position-fixed d-flex align-items-center py-3'>
          <Form.Control
            aria-label='course content type'
            as='select'
            onChange={(e) => selectCanvasItem(e)}
            value={canvasItem}
          >
            <option value='modules'>{translate('global.modules')}</option>
            <option value='pages'>{translate('global.pages')}</option>
            <option value='assignments'>{translate('global.assignments')}</option>
            <option value='discussions'>{translate('global.discussions')}</option>
          </Form.Control>
          <span className='position-absolute'>
            <FontAwesomeIcon icon={faCaretDown} />
          </span>
        </Form.Group>
        <div className='landing-page-list'>
          {canvasItem === 'modules' ? <ModuleLists isModuleListUpdated={setModuleList} /> : null}
          {canvasItem === 'pages' ? <PagesLists /> : null}
          {canvasItem === 'assignments' ? <Assignments /> : null}
          {canvasItem === 'discussions' ? <Discussions /> : null}
        </div>
      </Container>
      <div className='d-flex justify-content-end help-section'>{translate('powered')}</div>
    </>
  );
};

export default CanvasHome;
