import React from 'react';
import { fireEvent, render, RenderResult, screen, waitFor, within } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

import axe from '../../../axeHelper';
import ItemList from './items';

import { itemProps } from '../../../_test/canvasMockData';

// mock style modified to accomodate for the variable $detail in translation. This ensures the $detail is correctly checked for
jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v, detail?: any) => {
    if (detail === undefined) {
      return v;
    }
    return `${v}/${detail.detail}`;
  }),
}));

describe('Canvas Module(s) Item List', () => {
  let moduleItemList: RenderResult;

  beforeEach(() => {
    moduleItemList = render(
      <BrowserRouter>
        <ItemList itemList={itemProps.itemList} moduleList={itemProps.moduleList} />
      </BrowserRouter>,
    );
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(moduleItemList.container);
    expect(results).toHaveNoViolations();
  });

  test('should tab to focus on UI buttons in order of visual appearance', () => {
    const addItemButton = moduleItemList.getByRole('link', {
      name: 'a. Branded H Unit Page',
    });
    addItemButton.focus();
    expect(addItemButton).toHaveFocus();
    userEvent.tab();

    const listItem = moduleItemList.getByRole('listitem', { name: 'a. Branded H Unit Page' });
    const publishButton = within(listItem).getByTitle('page.publishbutton');
    expect(publishButton).toHaveFocus();
    userEvent.tab();

    const dropDownMenu = moduleItemList.getByRole('button', {
      name: 'pageitem.dropdowndetail_aria/a. Branded H Unit Page',
    });
    expect(dropDownMenu).toHaveFocus();
  });

  test('should have list item buttons styled without flex-grow-1', () => {
    const listItemButtons = moduleItemList.getByTestId('assignment-buttons');
    expect(listItemButtons).not.toHaveClass('flex-grow-1');
  });

  describe('dropdown menu', () => {
    beforeEach(() => {
      const itemDropdown = moduleItemList.getByTestId('module-item-dropdown');
      userEvent.click(itemDropdown);
    });

    test('should show itemRename modal when select rename', () => {
      userEvent.keyboard('rename a. Branded H Unit Page');
      expect(moduleItemList.getByText('pageitem.renameitem')).toBeVisible();
    });

    test('should show remove modal when select remove', () => {
      const removeItem = moduleItemList.getByLabelText(
        'pageitem.removedetail_aria/a. Branded H Unit Page',
      );
      userEvent.click(removeItem);
      waitFor(() => {
        expect(screen.getByText('global.confirm')).toBeVisible();
      });
    });

    test('should duplicate item when select duplicate', () => {
      const duplicateItem = moduleItemList.getByLabelText(
        'pageitem.duplicatedetail_aria/a. Branded H Unit Page',
      );
      fireEvent.click(duplicateItem);
      waitFor(() => {
        expect(screen.getByTestId('toast-component')).toBeVisible();
      });
    });
  });
});
