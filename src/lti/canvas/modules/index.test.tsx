import CanvasModules from './index';
import * as action from '../components/action';
import API from '@aws-amplify/api';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from '../../../aws-exports';
import { StorageMock } from '../../../utils/storageMock';
import {
  userMockData,
  items,
  sampleItems,
  moduleData,
  listTemplatesResponseOne,
  listTemplatesResponse,
  globalTemplatesResponseOne,
  globalTemplatesResponse,
  courseMockData,
  accountMockData,
  sharedTemplatesResponseOne,
  sharedTemplatesResponse,
} from '../.././../loree-editor/modules/customBlocks/customBlockMockData';
import { TemplateType } from '../components/AddTemplatesItem';

describe('loadModulesFunction testcase', () => {
  const IndexInstance = new CanvasModules(items);

  const result = {
    canvasModules:
      '{"statusCode":200,"body":[{"id":26164,"name":"tcp","position":1,"unlock_at":null,"require_sequential_progress":false,"publish_final_grade":false,"prerequisite_module_ids":[],"published":false,"items_count":1,"items_url":"https://crystaldelta.instructure.com/api/v1/courses/878/modules/26164/items"},{"id":26542,"name":"hanuman","position":2,"unlock_at":null,"require_sequential_progress":false,"publish_final_grade":false,"prerequisite_module_ids":[],"published":false,"items_count":2,"items_url":"https://crystaldelta.instructure.com/api/v1/courses/878/modules/26542/items"},{"id":26543,"name":"andi","position":3,"unlock_at":null,"require_sequential_progress":false,"publish_final_grade":false,"prerequisite_module_ids":[],"published":false,"items_count":2,"items_url":"https://crystaldelta.instructure.com/api/v1/courses/878/modules/26543/items"},{"id":26544,"name":"arun","position":4,"unlock_at":null,"require_sequential_progress":false,"publish_final_grade":false,"prerequisite_module_ids":[],"published":false,"items_count":2,"items_url":"https://crystaldelta.instructure.com/api/v1/courses/878/modules/26544/items"}]}',
  };
  beforeEach(() => {
    const IndexInstance = new CanvasModules(items);
    IndexInstance.handleModulesApi = jest.fn();
    sessionStorage.setItem('course_id', '878');
  });

  test('ModuleApi call response validation while calling loadmodulusFunction', async () => {
    const courseId = '878';
    IndexInstance.handleModulesApi = jest.fn().mockImplementation((token, courseId) => result);
    const response = IndexInstance.handleModulesApi(courseId);
    expect(response).toEqual(result);
  });

  describe('#appendingModuleItems', () => {
    const mockResponse: _Any = [
      {
        id: 76119,
        title: 'Topic 1: Overview',
        position: 1,
        indent: 0,
        quiz_lti: false,
        type: 'Page',
        module_id: 26645,
        html_url: 'https://crystaldelta.instructure.com/courses/178/modules/items/76119',
        page_url: 'topic-1-overview',
        url: 'https://crystaldelta.instructure.com/api/v1/courses/178/pages/topic-1-overview',
        published: false,
      },
      {
        id: 76120,
        title: 'Front Page',
        position: 2,
        indent: 0,
        quiz_lti: false,
        type: 'Page',
        module_id: 26645,
        html_url: 'https://crystaldelta.instructure.com/courses/178/modules/items/76120',
        page_url: 'front-page',
        url: 'https://crystaldelta.instructure.com/api/v1/courses/178/pages/front-page',
        published: true,
      },
    ];
    const responseResult: _Any = [
      {
        id: 26645,
        name: 'Module - 1',
        position: 1,
        unlock_at: null,
        require_sequential_progress: false,
        publish_final_grade: false,
        prerequisite_module_ids: [],
        published: false,
        items_count: 2,
        items_url: 'https://crystaldelta.instructure.com/api/v1/courses/178/modules/26645/items',
        items: [
          {
            id: 76119,
            title: 'Topic 1: Overview',
            position: 1,
            indent: 0,
            quiz_lti: false,
            type: 'Page',
            module_id: 26645,
            html_url: 'https://crystaldelta.instructure.com/courses/178/modules/items/76119',
            page_url: 'topic-1-overview',
            url: 'https://crystaldelta.instructure.com/api/v1/courses/178/pages/topic-1-overview',
            published: false,
          },
          {
            id: 76120,
            title: 'Front Page',
            position: 2,
            indent: 0,
            quiz_lti: false,
            type: 'Page',
            module_id: 26645,
            html_url: 'https://crystaldelta.instructure.com/courses/178/modules/items/76120',
            page_url: 'front-page',
            url: 'https://crystaldelta.instructure.com/api/v1/courses/178/pages/front-page',
            published: true,
          },
        ],
      },
    ];
    test('module items are appended to module array', async () => {
      jest.spyOn(action, 'getModuleItems').mockImplementation(() => mockResponse);
      const response = await IndexInstance.appendModuleItemsToModule(moduleData, 0);
      expect(response).toEqual(responseResult);
    });
  });
});
describe('calling getstate function', () => {
  const IndexInstance = new CanvasModules(items);
  test('templateslist', () => {
    const getstateFunction = IndexInstance.getStateKey(TemplateType.MyTemplate);
    expect(getstateFunction).toBe('templatesLists');
  });
});

describe('Template full listing', () => {
  const indexInstance = new CanvasModules(sampleItems);

  describe('My Templates setup', () => {
    beforeEach(() => {
      global.sessionStorage = new StorageMock() as _Any;
      Amplify.configure(awsconfig);
      global.sessionStorage = new StorageMock() as _Any;
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      const user = {
        attributes: userMockData,
      };
      Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => listTemplatesResponseOne)
        .mockImplementationOnce(() => listTemplatesResponse);
    });
    test('My Templates', async () => {
      const outputList = (await indexInstance.getTemplateLists(TemplateType.MyTemplate)).length;
      expect(outputList).toBe(3);
    });
  });
  describe('Global Templates setup', () => {
    beforeEach(() => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => globalTemplatesResponseOne)
        .mockImplementationOnce(() => globalTemplatesResponse);
    });
    test('Global Templates', async () => {
      const outputList = (await indexInstance.getTemplateLists(TemplateType.GlobalTemplate)).length;
      expect(outputList).toBe(0);
    });
  });
  describe('Shared Templates setup', () => {
    beforeEach(() => {
      sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
      API.graphql = jest
        .fn()
        .mockImplementationOnce(() => courseMockData)
        .mockImplementationOnce(() => accountMockData)
        .mockImplementationOnce(() => sharedTemplatesResponseOne)
        .mockImplementationOnce(() => sharedTemplatesResponse);
    });
    test('Shared Templates', async () => {
      const outputList = (await indexInstance.getTemplateLists(TemplateType.SharedTemplate)).length;
      expect(outputList).toBe(0);
    });
  });
});
