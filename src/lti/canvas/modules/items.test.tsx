import React from 'react';
import { shallow } from 'enzyme';
import ItemList from './items';
import ConfirmAlert, { ConfirmAlertProps } from '../components/confirmAlert';
import { itemProps } from '../../../_test/canvasMockData';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('#itemList', () => {
  describe('when item list have result', () => {
    const itemList = shallow<ItemList>(
      <ItemList itemList={itemProps.itemList} moduleList={itemProps.moduleList} />,
    );
    test('item list is rendered', () => {
      expect(itemList.find('ListGroupItem')).toHaveLength(3);
    });

    describe('click on remove assignment', () => {
      test('remove assignment modal opened', () => {
        itemList.find('ListGroupItem').at(1).find('Button').at(0).simulate('click');
        expect(itemList.find<ConfirmAlertProps>(ConfirmAlert).props().show).toBe(true);
      });
    });
    describe('click on remove discussion', () => {
      test('remove discussion modal opened', () => {
        itemList.find('ListGroupItem').at(2).find('Button').at(0).simulate('click');
        expect(itemList.find<ConfirmAlertProps>(ConfirmAlert).props().show).toBe(true);
      });
    });
  });

  describe('when item list is empty', () => {
    const itemListProps = {
      itemList: [],
      moduleList: [],
    };
    const itemList = shallow(
      <ItemList itemList={itemListProps.itemList} moduleList={itemListProps.moduleList} />,
    );
    test('item list is not rendered', () => {
      expect(itemList.find('.p-3').text()).toEqual('pageitem.noitemfound');
    });
  });
});
