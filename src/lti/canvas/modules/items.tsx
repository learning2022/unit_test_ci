import React, { Component } from 'react';
import { ListGroup, Dropdown, Button } from 'react-bootstrap';
import Published from '../components/Published';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPen,
  faTrash,
  faEllipsisV,
  faFileAlt,
  faRocket,
  faClone,
  faThList,
  faComments,
  faSpinner,
} from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import {
  RemoveItemFromModule,
  ItemTogglePublish,
  handleRenameItem,
  pageItemDuplicate,
} from '../components/action';
import ToastComponent from '../../components/ToastComponent';

import ItemUpdateModal from '../components/ItemUpdateModal';
import ConfirmAlert from '../components/confirmAlert';
import { translate } from '../../../i18n/translate';
export interface ItemProps {
  itemList: _Any;
  moduleList: _Any;
}

class ItemList extends Component<ItemProps> {
  state = {
    isRemove: false,
    itemId: '',
    moduleId: '',
    itemPosition: '',
    showToast: false,
    toastMessage: '',
    itemModelShow: false,
    itemName: '',
    btnLoading: false,
    deleteBtnLoading: false,
  };

  closeItemUpdateModel = () => {
    this.setState({ itemModelShow: false });
  };

  ItemNameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      itemName: event.target.value,
    });
  };

  handlePublishStatus = async (
    event: React.ChangeEvent<HTMLInputElement>,
    moduleId: string,
    publishedStatus: boolean,
    itemPosition: string,
    type: string,
    itemId: string,
  ) => {
    await ItemTogglePublish(
      event,
      moduleId,
      publishedStatus,
      this.props.itemList,
      itemId,
      itemPosition,
      type,
      this.props.moduleList,
    );
    const toastMsg = publishedStatus
      ? translate('modulepage.publishsuccess')
      : translate('modulepage.unpublishsuccess');
    this.setState({ showToast: true, toastMessage: toastMsg });
  };

  handleDelete = async () => {
    this.setState({
      deleteBtnLoading: true,
    });
    await RemoveItemFromModule(
      this.state.moduleId,
      this.state.itemId,
      this.props.itemList,
      this.state.itemPosition,
    );
    this.setState({
      deleteBtnLoading: false,
      isRemove: false,
      showToast: true,
      toastMessage: translate('pageitem.removesuccess'),
    });
  };

  handleRename = async (event: React.MouseEvent<HTMLInputElement>) => {
    this.setState({ btnLoading: true });
    const renameResponse = await handleRenameItem(
      event,
      this.state.itemId,
      this.state.itemName,
      this.state.moduleId,
      this.props.moduleList,
    );
    const toastMsg =
      renameResponse === 'error'
        ? translate('pageitem.renamefailed')
        : translate('pageitem.renamesuccess');
    this.setState({ showToast: true, toastMessage: toastMsg, itemName: '', btnLoading: false });
    this.closeItemUpdateModel();
  };

  handleItemDuplicate = (
    event: React.MouseEvent<Element>,
    moduleId: string,
    pageId: string,
    pageUrl: string,
  ): void => {
    event.preventDefault();
    event.stopPropagation();
    pageItemDuplicate(moduleId, pageId, pageUrl, this.props.moduleList)
      .then(() => {
        this.setState({ showToast: true, toastMessage: translate('pageitem.duplicatesuccess') });
      })
      .catch(() => {
        this.setState({
          showToast: true,
          toastMessage: translate('pageitem.duplicatefailed'),
        });
      });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  render() {
    return (
      <>
        <ListGroup variant='flush' className='module-item' role='list'>
          {this.props.itemList !== undefined ? (
            this.props.itemList !== undefined && (this.props.itemList as Array<{}>).length > 0 ? (
              (this.props.itemList as Array<Object>).map((item: _Any, index: number) => (
                <ListGroup.Item
                  role='listitem'
                  title={`${item.title}`}
                  key={index}
                  className={`pr-1 ${item.published ? 'item-published' : 'item-unpublished'}`}
                >
                  <div className='d-flex'>
                    {item.type === 'Page' ? (
                      <>
                        <div>
                          <FontAwesomeIcon icon={faFileAlt} className='item-icon mr-3' />
                        </div>
                        <div className='flex-grow-1'>
                          {' '}
                          <Link
                            className='canvasTitleWrap'
                            to={`/lti/editor/${item.page_url}/page/${encodeURIComponent(
                              item.title,
                            )}`}
                          >
                            {item.title}
                          </Link>
                        </div>
                        <div>
                          <div className='d-flex flex-row'>
                            <Published
                              isPublished={item.published}
                              id={item.module_id}
                              type='PAGE'
                              itemId={item.id}
                              togglePublishStatus={this.handlePublishStatus}
                              modulePosition={index}
                            />
                            <Dropdown>
                              <Dropdown.Toggle
                                data-testid='module-item-dropdown'
                                variant='link'
                                className='no-caret pt-0 pb-0'
                                aria-label={translate('pageitem.dropdowndetail_aria', {
                                  detail: item.title,
                                })}
                              >
                                <FontAwesomeIcon icon={faEllipsisV} />
                              </Dropdown.Toggle>
                              <Dropdown.Menu role='menu'>
                                <Dropdown.Item
                                  className='dropdown-menu-item'
                                  aria-label={translate('pageitem.renamedetail_aria', {
                                    detail: item.title,
                                  })}
                                  variant='link'
                                  onClick={() =>
                                    this.setState({
                                      itemModelShow: true,
                                      moduleId: item.module_id,
                                      itemId: item.id,
                                      itemPosition: index,
                                      itemName: item.title,
                                    })
                                  }
                                >
                                  <FontAwesomeIcon icon={faPen} /> {translate('global.rename')}
                                </Dropdown.Item>
                                <Dropdown.Item
                                  className='dropdown-menu-item'
                                  aria-label={translate('pageitem.duplicatedetail_aria', {
                                    detail: item.title,
                                  })}
                                  variant='link'
                                  onClick={(e: React.MouseEvent<Element>) =>
                                    this.handleItemDuplicate(
                                      e,
                                      item.module_id,
                                      item.id,
                                      item.page_url,
                                    )
                                  }
                                >
                                  <FontAwesomeIcon icon={faClone} /> {translate('global.duplicate')}
                                </Dropdown.Item>
                                <Dropdown.Item
                                  className='dropdown-menu-item'
                                  aria-label={translate('pageitem.removedetail_aria', {
                                    detail: item.title,
                                  })}
                                  variant='link'
                                  onClick={() =>
                                    this.setState({
                                      isRemove: true,
                                      moduleId: item.module_id,
                                      itemId: item.id,
                                      itemPosition: index,
                                    })
                                  }
                                >
                                  <FontAwesomeIcon icon={faTrash} /> {translate('global.remove')}
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </div>
                      </>
                    ) : item.type === 'Assignment' ? (
                      <>
                        <div>
                          <FontAwesomeIcon
                            icon={faThList}
                            className={`mr-2 ${item.published ? 'text-success' : 'text-muted'}`}
                          />{' '}
                        </div>
                        <div className='flex-grow-1'>
                          <Link
                            className='canvasTitleWrap'
                            to={`/lti/editor/${item.content_id}/assignment/${encodeURIComponent(
                              item.title,
                            )}`}
                          >
                            {item.title}
                          </Link>
                        </div>
                        <div data-testid='assignment-buttons'>
                          <div className='d-flex flex-row'>
                            <Published
                              isPublished={item.published}
                              id={item.module_id}
                              itemId={item.id}
                              type='ASSIGNMENT'
                              togglePublishStatus={this.handlePublishStatus}
                              modulePosition={index}
                            />
                            <Dropdown>
                              <Dropdown.Toggle
                                variant='link'
                                className='no-caret pt-0 pb-0'
                                aria-label={translate('pageitem.dropdowndetail_aria', {
                                  detail: item.title,
                                })}
                              >
                                <FontAwesomeIcon icon={faEllipsisV} />
                              </Dropdown.Toggle>
                              <Dropdown.Menu>
                                <Button
                                  aria-label={translate('pageitem.removedetail_aria', {
                                    detail: item.title,
                                  })}
                                  variant='link'
                                  onClick={() =>
                                    this.setState({
                                      isRemove: true,
                                      moduleId: item.module_id,
                                      itemId: item.id,
                                      itemPosition: index,
                                    })
                                  }
                                >
                                  <FontAwesomeIcon icon={faTrash} /> {translate('global.remove')}
                                </Button>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </div>
                      </>
                    ) : item.type === 'Discussion' ? (
                      <>
                        <div>
                          <FontAwesomeIcon
                            icon={faComments}
                            className={`mr-2 ${item.published ? 'text-success' : 'text-muted'}`}
                          />{' '}
                        </div>
                        <div className='flex-grow-1'>
                          <Link
                            className='canvasTitleWrap'
                            to={`/lti/editor/${item.content_id}/discussion/${encodeURIComponent(
                              item.title,
                            )}`}
                          >
                            {item.title}
                          </Link>
                        </div>
                        <div>
                          <div className='d-flex flex-row'>
                            <Published
                              isPublished={item.published}
                              id={item.module_id}
                              itemId={item.id}
                              type='DISCUSSION'
                              togglePublishStatus={this.handlePublishStatus}
                              modulePosition={index}
                            />
                            <Dropdown>
                              <Dropdown.Toggle
                                variant='link'
                                className='no-caret pt-0 pb-0'
                                aria-label={translate('pageitem.dropdowndetail_aria', {
                                  detail: item.title,
                                })}
                              >
                                <FontAwesomeIcon icon={faEllipsisV} />
                              </Dropdown.Toggle>
                              <Dropdown.Menu>
                                <Button
                                  aria-label={translate('pageitem.removedetail_aria', {
                                    detail: item.title,
                                  })}
                                  variant='link'
                                  onClick={() =>
                                    this.setState({
                                      isRemove: true,
                                      moduleId: item.module_id,
                                      itemId: item.id,
                                      itemPosition: index,
                                    })
                                  }
                                >
                                  <FontAwesomeIcon icon={faTrash} /> {translate('global.remove')}
                                </Button>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </div>
                      </>
                    ) : item.type === 'Quiz' ? (
                      <>
                        <div>
                          <FontAwesomeIcon icon={faRocket} className='item-icon mr-3' />
                        </div>
                        <div className='flex-grow-1'> {item.title}</div>
                      </>
                    ) : (
                      <>
                        <p className='font-weight-bold flex-grow-1'>{item.title}</p>
                      </>
                    )}
                  </div>
                </ListGroup.Item>
              ))
            ) : (
              <div className='border'>
                <div className='p-3'>{translate('pageitem.noitemfound')}</div>
              </div>
            )
          ) : (
            <div className='justify-content-center'>
              <FontAwesomeIcon icon={faSpinner} className='fa-spin itemLoader-icon mt-3 mb-3' />
            </div>
          )}
        </ListGroup>
        <ConfirmAlert
          show={this.state.isRemove}
          type='item'
          confirm={this.handleDelete}
          onCancel={() => this.setState({ isRemove: false })}
          deleteBtn={this.state.deleteBtnLoading}
        />
        <ItemUpdateModal
          show={this.state.itemModelShow}
          hide={this.closeItemUpdateModel}
          nameHandler={this.ItemNameHandler}
          renameItemHandler={this.handleRename}
          itemName={this.state.itemName}
          btnLoad={this.state.btnLoading}
        />
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
      </>
    );
  }
}

export default ItemList;
