import React, { Component } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { Accordion, Card, Button, Row, Col, Dropdown } from 'react-bootstrap';
import {
  canvasModules,
  getCustomTemplate,
  getGlobalTemplates,
  getSharedTemplates,
} from '../../../graphql/queries';
import Loading from '../../../components/loader/loading';
import ItemLists from './items';
import Published from '../components/Published';
import ToastComponent from '../../components/ToastComponent';
import AddItem from '../components/AddModuleItem';
import CreateRenameModel from '../components/CreateRenameModel';
import {
  AddModule,
  UpdateModule,
  DeleteModule,
  ModuleTogglePublish,
  GetPages,
  CreatePage,
  AddPageToModule,
  getModuleItems,
} from '../components/action';
import ConfirmAlert from '../components/confirmAlert';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faTrash, faEllipsisV, faPlus } from '@fortawesome/free-solid-svg-icons';
import Error from '../components/Error';
import { saveCanvasPage } from '../../../graphql/mutations';
import { uploadToCanvas, getS3urlFromKey } from '../../admin/globalImagesUpload/uploadImageToS3';
import CONSTANTS from '../../../loree-editor/constant';
import {
  listingCustomTemplates,
  listingGlobalTemplates,
  listingSharedTemplates,
} from '../../../loree-editor/modules/customBlocks/customBlocksAction';
import { translate } from '../../../i18n/translate';
import { TemplateType } from '../components/AddTemplatesItem';
import { Items, ModuleProps } from '../../../loree-editor/interface';

interface ItemArrayList extends Array<Items> {}
class CanvasModules extends Component<Partial<ModuleProps> & Partial<ItemArrayList>> {
  state = {
    pageIndex: 1,
    pageItems: CONSTANTS.LOREE_PER_PAGE_ITEM,
    modulesList: [],
    loading: true,
    error: false,
    showCreateRenameModel: false,
    isCreate: true,
    moduleName: '',
    moduleId: '',
    btnLoading: false,
    showToast: false,
    toastMessage: '',
    isDelete: false,
    modulePosition: '',
    showAddModuleItem: false,
    pageList: [],
    getPageLoading: false,
    isTemplatePageLoading: false,
    expanded: '',
    deleteBtnLoading: false,
    itemType: 'Pages',
    templatesLists: [],
    isTemplateLoaded: false,
    globalTemplatesLists: [],
    sharedTemplatesLists: [],
    isGlobalTemplateLoaded: false,
    isSharedTemplateLoaded: false,
    templateType: TemplateType.MyTemplate,
  };

  getStateKey = (type: TemplateType) => {
    switch (type) {
      case TemplateType.MyTemplate:
        return 'templatesLists';
      case TemplateType.GlobalTemplate:
        return 'globalTemplatesLists';
      case TemplateType.SharedTemplate:
        return 'sharedTemplatesLists';
    }
  };

  getStateLoadedKey = (type: TemplateType) => {
    switch (type) {
      case TemplateType.MyTemplate:
        return 'isTemplateLoaded';
      case TemplateType.GlobalTemplate:
        return 'isGlobalTemplateLoaded';
      case TemplateType.SharedTemplate:
        return 'isSharedTemplateLoaded';
    }
  };

  closeCreateRenameModel = () => {
    this.setState({ showCreateRenameModel: false });
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  closeAddModuleItemModel = () => {
    this.setState({
      showAddModuleItem: false,
      itemType: 'Pages',
      isTemplateLoaded: false,
      isGlobalTemplateLoaded: false,
      isSharedTemplateLoaded: false,
    });
  };

  handleModuleItemAdd = async (
    event: _Any,
    moduleId: _Any,
    moduleName: _Any,
    modulePosition: _Any,
  ) => {
    this.setState({
      showAddModuleItem: true,
      moduleId: moduleId,
      moduleName: moduleName,
      getPageLoading: true,
      btnLoading: false,
      modulePosition: modulePosition,
    });
    const pageListData = await GetPages(event);
    this.setState({ pageList: pageListData, getPageLoading: false });
  };

  handleModuleAdd = async (event: _Any) => {
    this.setState({ btnLoading: true });
    let toastMsg;
    if (this.state.isCreate) {
      await AddModule(event, this.state.moduleName, this.state.modulesList);
      toastMsg = translate('module.renametoastsuccess', { moduleName: this.state.moduleName });
    } else {
      const updateModuleResponse = await UpdateModule(
        event,
        this.state.moduleId,
        this.state.moduleName,
        this.state.modulesList,
        this.state.modulePosition,
      );
      toastMsg =
        updateModuleResponse === 'error'
          ? translate('module.updatetoastfail', { moduleName: this.state.moduleName })
          : translate('module.updatetoastsuccess', { moduleName: this.state.moduleName });
    }
    this.closeCreateRenameModel();
    this.setState({ showToast: true, toastMessage: toastMsg, moduleName: '', btnLoading: false });
  };

  handleDelete = async () => {
    this.setState({
      deleteBtnLoading: true,
    });
    await DeleteModule(this.state.moduleId, this.state.modulesList, this.state.modulePosition);
    this.setState({
      deleteBtnLoading: false,
      isDelete: false,
      showToast: true,
      toastMessage: translate('module.deletesuccess'),
    });
  };

  handlePublishStatus = async (
    event: _Any,
    moduleId: _Any,
    publishedStatus: boolean,
    modulePosition: _Any,
  ) => {
    await ModuleTogglePublish(
      event,
      moduleId,
      publishedStatus,
      this.state.modulesList,
      modulePosition,
      'MODULE',
    );
    const updatedItems = await getModuleItems(moduleId);
    const moduleLists: _Any = this.state.modulesList;
    moduleLists[modulePosition].items = updatedItems;
    this.setState({
      moduleList: moduleLists,
    });
    const toastMsg = publishedStatus
      ? translate('modulepage.publishsuccess')
      : translate('modulepage.unpublishsuccess');
    this.setState({ showToast: true, toastMessage: toastMsg });
  };

  moduleNameHandler = (event: _Any) => {
    this.setState({
      moduleName: event.target.value,
    });
  };

  handleModuleItemPageAdd = async (event: _Any, pageName: _Any) => {
    this.setState({ btnLoading: true });
    const newPage = await CreatePage(event, pageName);
    await this.existingPageAdd(newPage.url, false);
    this.closeAddModuleItemModel();
    this.setState({ btnLoading: false });
  };

  // NEED TO UPDATE CLOSE MODEL
  existingPageAdd = async (newPageUrl: _Any, isMultiplePage: boolean) => {
    if (!isMultiplePage) {
      await AddPageToModule(
        this.state.moduleId,
        newPageUrl,
        this.state.modulesList,
        this.state.modulePosition,
      );
      this.setState({
        showToast: true,
        toastMessage: translate('module.itemsuccessdetail', { newPageUrl: newPageUrl }),
      });
    } else {
      this.setState({ btnLoading: true });
      const newPageUrls = newPageUrl;
      const addPage = newPageUrls.map(async (pageUrl: _Any) => {
        await AddPageToModule(
          this.state.moduleId,
          pageUrl,
          this.state.modulesList,
          this.state.modulePosition,
        );
      });
      void Promise.all(addPage).then(() => {
        this.closeAddModuleItemModel();
        this.setState({
          showToast: true,
          toastMessage: translate('module.itemsuccess'),
        });
      });
    }
  };

  async componentDidMount() {
    const courseId = sessionStorage.getItem('course_id');
    const modulesResponse: _Any = await this.handleModulesApi(courseId);
    const modulesData = JSON.parse(modulesResponse.data.canvasModules);
    if (modulesData.statusCode === 200) {
      this.setState({
        pageIndex: this.state.pageIndex + 1,
        modulesList: this.state.modulesList.concat(modulesData.body),
        loading: false,
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
    const moduleArray: _Any = this.state.modulesList;
    moduleArray.forEach(async (module: _Any, index: number) => {
      if (module.items === undefined) {
        const moduleItems: _Any = await getModuleItems(module.id);
        const thisModule: _Any = moduleArray[index];
        thisModule.items = moduleItems;
        moduleArray[index] = thisModule;
        this.setState({
          modulesList: moduleArray,
        });
      }
    });
    if (this.props.isModuleListUpdated) this.props.isModuleListUpdated(true);
  }

  appendModuleItemsToModule = async (module: _Any, index: number) => {
    const moduleItems: _Any = await getModuleItems(module[index].id);
    module[index] = { ...module[index], items: moduleItems };
    return module;
  };

  handleModulesApi = async (courseId: string | null) => {
    const modulesResponse = await API.graphql(
      graphqlOperation(canvasModules, {
        courseId: courseId,
        pageIndex: this.state.pageIndex,
        pageItems: 300,
      }),
    );
    return modulesResponse;
  };

  handleToggleAccordionIcon = (e: _Any) => {
    let collapsed = e.target;
    let nodeTrace = 5;
    while (nodeTrace > 0) {
      collapsed = collapsed.parentNode;
      nodeTrace--;
    }
    const accordianChildCollapse = collapsed.childNodes[0].childNodes[1].className === 'collapse';
    if (!accordianChildCollapse) {
      e.target.className = e.target.className.replace('icon-down-arrow', 'icon-right-arrow');
    } else {
      e.target.className = e.target.className.replace('icon-right-arrow', 'icon-down-arrow');
    }
  };

  selectItemType = async (type: string) => {
    this.setState({
      itemType: type,
    });
    await this.fetchTemplates(TemplateType.MyTemplate);
  };

  fetchTemplates = async (type: TemplateType) => {
    this.setState({
      templateType: type,
    });
    if (
      (type === TemplateType.MyTemplate && !this.state.isTemplateLoaded) ||
      (type === TemplateType.GlobalTemplate && !this.state.isGlobalTemplateLoaded) ||
      (type === TemplateType.SharedTemplate && !this.state.isSharedTemplateLoaded)
    ) {
      this.setState({ isTemplatePageLoading: true });
      this.setState({
        [this.getStateKey(type)]: await this.getTemplateLists(type),
        [this.getStateLoadedKey(type)]: true,
        isTemplatePageLoading: false,
      });
    }
  };

  getTemplateLists = async (type: TemplateType) => {
    let templateList;
    switch (type) {
      case TemplateType.MyTemplate:
        templateList = await listingCustomTemplates();
        break;
      case TemplateType.GlobalTemplate:
        templateList = await listingGlobalTemplates();
        break;
      case TemplateType.SharedTemplate:
        templateList = await listingSharedTemplates();
        break;
    }
    return this.getSortedTemplateLists(templateList);
  };

  getSortedTemplateLists = (myTemplatesLists: _Any) => {
    const bundledMyTemplates = this.validateTemplateStatus(myTemplatesLists);
    bundledMyTemplates.sort(this.sortComparision);
    return bundledMyTemplates;
  };

  validateTemplateStatus = (templatesList: _Any) => {
    const templatesArray = [];
    for (const template of templatesList) {
      if (template.active) {
        templatesArray.push(template);
      }
    }
    return templatesArray;
  };

  sortComparision = (arg: _Any, arg2: _Any): _Any => {
    const blockTitle1 = arg.title.toUpperCase();
    const blockTitle2 = arg2.title.toUpperCase();
    let data = 0;
    if (blockTitle1 > blockTitle2) {
      data = 1;
    } else if (blockTitle1 < blockTitle2) {
      data = -1;
    }
    return data;
  };

  handleTemplateModuleItemAdd = async (event: _Any, selectedtemplatetoAdd: _Any) => {
    this.setState({ btnLoading: true });
    let templateContent: _Any;
    const newPage = selectedtemplatetoAdd.map(async (template: _Any) => {
      let contentData: _Any;
      if (this.state.templateType === TemplateType.MyTemplate) {
        templateContent = await API.graphql(
          graphqlOperation(getCustomTemplate, { id: template.id }),
        );
        contentData = await getS3urlFromKey(templateContent.data.getCustomTemplate.content);
      } else if (this.state.templateType === TemplateType.GlobalTemplate) {
        templateContent = await API.graphql(
          graphqlOperation(getGlobalTemplates, { id: template.id }),
        );
        contentData = await getS3urlFromKey(templateContent.data.getGlobalTemplates.content);
      } else if (this.state.templateType === TemplateType.SharedTemplate) {
        templateContent = await API.graphql(
          graphqlOperation(getSharedTemplates, { id: template.id }),
        );
        contentData = await getS3urlFromKey(templateContent.data.getSharedTemplates.content);
      }
      contentData = await uploadToCanvas(contentData);
      const createdPage = await CreatePage(event, template.pageName);
      const courseId = sessionStorage.getItem('course_id');
      await API.graphql(
        graphqlOperation(saveCanvasPage, {
          courseId: courseId,
          pageID: createdPage.url,
          editorContent: contentData,
        }),
      );
      await AddPageToModule(
        this.state.moduleId,
        createdPage.url,
        this.state.modulesList,
        this.state.modulePosition,
      );
    });

    void Promise.all(newPage).then(() => {
      this.closeAddModuleItemModel();
      this.setState({
        showToast: true,
        toastMessage: translate('module.itemsuccess'),
      });
    });
  };

  render() {
    return (
      <>
        {this.state.loading ? (
          <Loading />
        ) : this.state.error ? (
          <Error />
        ) : (
          <>
            <div className='d-flex justify-content-end landing-add-btn'>
              <Button
                aria-label={translate('module.addmodule')}
                type='submit'
                onClick={() =>
                  this.setState({ showCreateRenameModel: true, moduleName: '', isCreate: true })
                }
              >
                {translate('module.plusmodule')}
              </Button>
              <CreateRenameModel
                show={this.state.showCreateRenameModel}
                hide={this.closeCreateRenameModel}
                isCreate={this.state.isCreate}
                saveMod={this.handleModuleAdd}
                moduleName={this.state.moduleName}
                nameHandler={this.moduleNameHandler}
                btnLoad={this.state.btnLoading}
              />
              <ConfirmAlert
                show={this.state.isDelete}
                type='Module'
                confirm={this.handleDelete}
                onCancel={() => this.setState({ isDelete: false })}
                deleteBtn={this.state.deleteBtnLoading}
              />
            </div>
            {this.state.modulesList.length > 0 ? (
              this.state.modulesList.map((module: _Any, index: _Any) => (
                <Accordion defaultActiveKey={`${index}`} key={index} className='mb-5'>
                  <Card
                    style={{ overflow: 'visible' }}
                    role='article'
                    aria-label={`${module.name}`}
                  >
                    <Card.Header className='p-1'>
                      <Row>
                        <Col>
                          <Accordion.Toggle
                            as={Button}
                            variant='link'
                            eventKey={`${index}`}
                            onClick={(e) => this.handleToggleAccordionIcon(e)}
                            className='icon-down-arrow text-decoration-none text-left'
                          >
                            {module.name}
                          </Accordion.Toggle>
                        </Col>
                        <Col>
                          <div className='d-flex flex-row justify-content-end'>
                            <Published
                              isPublished={module.published}
                              id={module.id}
                              type='MODULE'
                              togglePublishStatus={this.handlePublishStatus}
                              modulePosition={index}
                            />
                            <Button
                              aria-label={translate('module.addmoduleitemdetail', {
                                detail: module.name,
                              })}
                              variant='link'
                              onClick={async (event) =>
                                await this.handleModuleItemAdd(event, module.id, module.name, index)
                              }
                            >
                              <FontAwesomeIcon icon={faPlus} />
                            </Button>
                            <Dropdown>
                              <Dropdown.Toggle variant='link' className='no-caret'>
                                <FontAwesomeIcon icon={faEllipsisV} />
                              </Dropdown.Toggle>
                              <Dropdown.Menu>
                                <Dropdown.Item
                                  className='dropdown-menu-item'
                                  aria-label={translate('module.renamemoduledetail_aria', {
                                    detail: module.name,
                                  })}
                                  variant='link'
                                  onClick={() =>
                                    this.setState({
                                      showCreateRenameModel: true,
                                      isCreate: false,
                                      moduleId: module.id,
                                      moduleName: module.name,
                                      modulePosition: index,
                                    })
                                  }
                                >
                                  <FontAwesomeIcon icon={faPen} /> {translate('global.rename')}
                                </Dropdown.Item>
                                <Dropdown.Item
                                  className='dropdown-menu-item'
                                  aria-label={translate('module.deletemoduledetail_aria', {
                                    detail: module.name,
                                  })}
                                  variant='link'
                                  onClick={() =>
                                    this.setState({
                                      isDelete: true,
                                      moduleId: module.id,
                                      modulePosition: index,
                                    })
                                  }
                                >
                                  <FontAwesomeIcon icon={faTrash} /> {translate('global.delete')}
                                </Dropdown.Item>
                              </Dropdown.Menu>
                            </Dropdown>
                          </div>
                        </Col>
                      </Row>
                    </Card.Header>
                    <Accordion.Collapse eventKey={`${index}`} id='accordion-collapse-check'>
                      <Card.Body className='p-0'>
                        <ItemLists itemList={module.items} moduleList={this.state.modulesList} />
                      </Card.Body>
                    </Accordion.Collapse>
                  </Card>
                </Accordion>
              ))
            ) : (
              <div className='border'>
                <div className='p-3'>{translate('module.nomodulesfound')}</div>
              </div>
            )}

            {this.state.showToast && (
              <ToastComponent
                showToast={this.state.showToast}
                toastMessage={this.state.toastMessage}
                closeToast={this.closeToast}
              />
            )}
            {this.state.showAddModuleItem && (
              <AddItem
                show={this.state.showAddModuleItem}
                hide={this.closeAddModuleItemModel}
                moduleName={this.state.moduleName}
                pageList={this.state.pageList}
                getPageLoading={this.state.getPageLoading}
                isTemplatePageLoading={this.state.isTemplatePageLoading}
                handleAdd={this.handleModuleItemPageAdd}
                handleExistingAdd={this.existingPageAdd}
                addItemLoading={this.state.btnLoading}
                selectItemType={this.selectItemType}
                itemType={this.state.itemType}
                TemplatesLists={this.state.templatesLists}
                handleTemplateAdd={this.handleTemplateModuleItemAdd}
                fetchTemplates={this.fetchTemplates}
                globalTemplatesLists={this.state.globalTemplatesLists}
                sharedTemplatesLists={this.state.sharedTemplatesLists}
                templateType={this.state.templateType}
              />
            )}
          </>
        )}
      </>
    );
  }
}

export default CanvasModules;
