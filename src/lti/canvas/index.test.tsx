import React from 'react';
import { fireEvent, render, RenderResult, screen } from '@testing-library/react';
import axe from '../../axeHelper';
import { API } from 'aws-amplify';
import CanvasHome from './index';
import { canvasCourseDetailMock, canvasListLmsLoreeRoleMock } from '../mockData';

jest.mock('./modules', () => () => <div data-testid='modules' />);
jest.mock('./pages', () => () => <div data-testid='pages' />);
jest.mock('./assignments', () => () => <div data-testid='assignments' />);
jest.mock('./discussions', () => () => <div data-testid='discussions' />);
jest.mock('../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `translated:${v}`),
}));

describe('<CanvasHome>', () => {
  test('renders', () => {
    render(<CanvasHome />);
    const header = screen.getByRole('presentation');
    expect(header).toBeInTheDocument();
  });

  test('default value is modules', async () => {
    render(<CanvasHome />);

    const options = screen.getByTestId('modules');
    expect(options).toBeInTheDocument();
  });

  test('can choose pages', async () => {
    render(<CanvasHome />);

    const dropdown = screen.getByRole('combobox');
    fireEvent.change(dropdown, { target: { value: 'pages' } });
    const options = screen.getByTestId('pages');
    expect(options).toBeInTheDocument();
  });

  test('can choose assignments', async () => {
    render(<CanvasHome />);

    const dropdown = screen.getByRole('combobox');
    fireEvent.change(dropdown, { target: { value: 'assignments' } });
    const options = screen.getByTestId('assignments');
    expect(options).toBeInTheDocument();
  });

  test('can choose discussions', async () => {
    render(<CanvasHome />);

    const dropdown = screen.getByRole('combobox');
    fireEvent.change(dropdown, { target: { value: 'discussions' } });
    const options = screen.getByTestId('discussions');
    expect(options).toBeInTheDocument();
  });
  test('text is translated', () => {
    render(<CanvasHome />);
    const p = screen.getByText('translated:powered');
    expect(p).toBeInTheDocument();
  });
});

describe.skip('#CanvasHome Accessibility', () => {
  let canvasHome: RenderResult;
  beforeEach(() => {
    process.env.REACT_APP_ENABLE_LOCALISATION = 'true';
    localStorage.setItem('loreeLng', 'en');
    sessionStorage.setItem('domainName', 'canvas');
    API.graphql = jest
      .fn()
      .mockImplementationOnce(() => canvasCourseDetailMock)
      .mockImplementationOnce(() => canvasListLmsLoreeRoleMock);
    canvasHome = render(<CanvasHome />);
  });
  test('should pass accessibility tests', async () => {
    const results = await axe(canvasHome.container);
    expect(results).toHaveNoViolations();
  });
});
