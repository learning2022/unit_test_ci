/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { shallow } from 'enzyme';
import AssignmentLists from './index';
import { API } from 'aws-amplify';

describe('#assignments', () => {
  describe('when canvas assignment API returns result', () => {
    let assignmentList: any;
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(arg => {
        return {
          data: {
            canvasAssignments: JSON.stringify({
              statusCode: 200,
              body: [
                {
                  id: 2765,
                  name: 'Assignments',
                  position: 1,
                  group_weight: 0,
                  sis_source_id: null,
                  integration_data: {},
                  rules: {},
                  assignments: [
                    {
                      id: 15817,
                      description: '',
                      due_at: null,
                      unlock_at: null,
                      lock_at: null,
                      points_possible: 0,
                      grading_type: 'points',
                      assignment_group_id: 2765,
                      grading_standard_id: null,
                      created_at: '2020-09-29T07:31:51Z',
                      updated_at: '2020-09-29T07:31:51Z',
                      peer_reviews: false,
                      automatic_peer_reviews: false,
                      position: 1,
                      grade_group_students_individually: false,
                      anonymous_peer_reviews: false,
                      group_category_id: null,
                      post_to_sis: false,
                      moderated_grading: false,
                      omit_from_final_grade: false,
                      intra_group_peer_reviews: false,
                      anonymous_instructor_annotations: false,
                      anonymous_grading: false,
                      graders_anonymous_to_graders: false,
                      grader_count: 0,
                      grader_comments_visible_to_graders: true,
                      final_grader_id: null,
                      grader_names_visible_to_final_grader: true,
                      allowed_attempts: -1,
                      secure_params:
                        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsdGlfYXNzaWdubWVudF9pZCI6ImNhYzM0MmUyLTNmYjktNGRkMi1iNTJjLTFlZjBhYWFjZDVkZCJ9.Prcmp_wyX_aMhM-P6jmmJKLsoTX8a5AMH5J7CkNYkos',
                      course_id: 804,
                      name: 'sample assignment',
                      submission_types: ['online_text_entry'],
                      has_submitted_submissions: false,
                      due_date_required: false,
                      max_name_length: 255,
                      is_quiz_assignment: false,
                      can_duplicate: true,
                      original_course_id: null,
                      original_assignment_id: null,
                      original_assignment_name: null,
                      original_quiz_id: null,
                      workflow_state: 'unpublished',
                      muted: true,
                      html_url: 'https://crystaldelta.instructure.com/courses/804/assignments/15817',
                      has_overrides: true,
                      needs_grading_count: 0,
                      sis_assignment_id: null,
                      integration_id: null,
                      integration_data: {},
                      published: false,
                      unpublishable: true,
                      only_visible_to_overrides: true,
                      locked_for_user: false,
                      submissions_download_url: 'https://crystaldelta.instructure.com/courses/804/assignments/15817/submissions?zip=1',
                      post_manually: false,
                      anonymize_students: false,
                      require_lockdown_browser: false,
                      in_closed_grading_period: false,
                    },
                  ],
                  any_assignment_in_closed_grading_period: false,
                },
              ],
            }),
          },
        };
      });
      assignmentList = shallow(<AssignmentLists />);
    });
    test('assignment list is rendered', (done) => {
      setImmediate(() => {
        expect(assignmentList.find('.cardDivAccord')).toHaveLength(1);
        done();
      });
    });
    describe('when click publish and API is success', () => {
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(arg => {
          return {
            data: {
              publishToCanvas:
                '{"statusCode":200,"body":{"id":15817,"description":"","due_at":null,"unlock_at":null,"lock_at":null,"points_possible":0,"grading_type":"points","assignment_group_id":1261,"grading_standard_id":null,"created_at":"2020-08-29T19:24:31Z","updated_at":"2020-12-23T10:32:47Z","peer_reviews":false,"automatic_peer_reviews":false,"position":1,"grade_group_students_individually":false,"anonymous_peer_reviews":false,"group_category_id":null,"post_to_sis":false,"moderated_grading":false,"omit_from_final_grade":false,"intra_group_peer_reviews":false,"anonymous_instructor_annotations":false,"anonymous_grading":false,"graders_anonymous_to_graders":false,"grader_count":0,"grader_comments_visible_to_graders":true,"final_grader_id":null,"grader_names_visible_to_final_grader":true,"allowed_attempts":-1,"secure_params":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsdGlfYXNzaWdubWVudF9pZCI6ImM2ODY3NTAwLTRmMGItNDM4ZS04Y2ZlLTE1Y2VkNTI1OWY0OSJ9.yAkxvDfEBTtaVKi3V-4X5dbY_L-VGr1XCFBn_NCGEI4","course_id":429,"name":"test discussion","submission_types":["discussion_topic"],"has_submitted_submissions":false,"due_date_required":false,"max_name_length":255,"in_closed_grading_period":false,"is_quiz_assignment":false,"can_duplicate":true,"original_course_id":null,"original_assignment_id":null,"original_assignment_name":null,"original_quiz_id":null,"workflow_state":"published","muted":true,"html_url":"https://crystaldelta.instructure.com/courses/429/assignments/15793","has_overrides":false,"needs_grading_count":0,"sis_assignment_id":null,"integration_id":null,"integration_data":{},"discussion_topic":{"assignment_id":15793,"id":6112,"title":"test discussion","discussion_type":"threaded","last_reply_at":"2020-08-29T19:24:31Z","created_at":"2020-08-29T19:24:31Z","delayed_post_at":null,"posted_at":"2020-08-29T19:24:31Z","root_topic_id":null,"position":null,"podcast_has_student_posts":true,"lock_at":null,"allow_rating":true,"only_graders_can_rate":false,"sort_by_rating":false,"is_section_specific":false,"user_name":"tester","discussion_subentry_count":0,"permissions":{"attach":true,"update":true,"reply":true,"delete":true},"require_initial_post":true,"user_can_see_posts":true,"podcast_url":"/feeds/topics/6112/course_NRHJ4MNnCXGSREW5HFGcShi9neSlIq7fp2Nw8zUI.rss","read_state":"unread","unread_count":0,"subscribed":false,"attachments":[],"published":true,"can_unpublish":true,"locked":false,"can_lock":true,"comments_disabled":false,"author":{"id":1170,"display_name":"tester","avatar_image_url":"https://crystaldelta.instructure.com/images/messages/avatar-50.png","html_url":"https://crystaldelta.instructure.com/courses/429/users/1170","pronouns":null},"html_url":"https://crystaldelta.instructure.com/courses/429/discussion_topics/6112","url":"https://crystaldelta.instructure.com/courses/429/discussion_topics/6112","pinned":false,"group_category_id":null,"can_group":true,"topic_children":[],"group_topic_children":[],"locked_for_user":false,"message":"","todo_date":null},"published":true,"unpublishable":true,"only_visible_to_overrides":false,"locked_for_user":false,"submissions_download_url":"https://crystaldelta.instructure.com/courses/429/assignments/15793/submissions?zip=1","post_manually":false,"anonymize_students":false,"require_lockdown_browser":false}}',
            },
          };
        });
      });
      test('assignment is published', () => {
        const toggleAssignmentPublish = jest.fn();
        assignmentList.find('Button').at(0).simulate('click', { assignment_id: '2765', status: false });
        expect(toggleAssignmentPublish.mock).toBeCalled;
      });
    });
    describe('when click publish and API is failed', () => {
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(arg => {
          return {
            data: {
              publishToCanvas: '{"statusCode":404,"body":{}}',
            },
          };
        });
      });
      test('assignment is not published', () => {
        const toggleAssignmentPublish = jest.fn();
        assignmentList.find('Button').at(0).simulate('click', { assignment_id: '2765', status: false });
        expect(toggleAssignmentPublish.mock).toBeCalled;
      });
    });
  });
  describe('when canvas assignment API returns empty result', () => {
    let assignmentList: any;
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(arg => {
        return {
          data: {
            canvasAssignments: JSON.stringify({
              statusCode: 404,
              body: [],
            }),
          },
        };
      });
      assignmentList = shallow(<AssignmentLists />);
    });
    test('assignment list is not rendered', (done) => {
      setImmediate(() => {
        expect(assignmentList.find('.cardDivAccord')).toHaveLength(0);
        done();
      });
    });
  });
});
