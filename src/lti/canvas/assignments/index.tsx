/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { ListGroup, Button, Accordion, Card } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faBan, faEdit } from '@fortawesome/free-solid-svg-icons';
import Loader from '../../../components/loader/loading';
import { publishToCanvas } from '../../../graphql/mutations';
import { canvasAssignments } from '../../../graphql/queries';
import ToastComponent from '../../components/ToastComponent';
import Error from '../components/Error';
import { translate } from '../../../i18n/translate';

class AssignmentLists extends Component {
  state = {
    loading: true,
    assignmentsArr: [],
    error: false,
    showToast: false,
    toastMessage: '',
    assignmentName: '',
  };

  async componentDidMount() {
    const courseId = sessionStorage.getItem('course_id');
    let response: any = await API.graphql(
      graphqlOperation(canvasAssignments, {
        courseId: courseId,
      }),
    );
    response = JSON.parse(response.data.canvasAssignments);
    if (response.statusCode === 200) {
      this.setState({
        assignmentsArr: response.body,
        loading: false,
      });
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  }

  closeToast = () => {
    this.setState({ showToast: false });
  };

  toggleAssignmentPublish = async (assignmentId: string, status: boolean) => {
    const courseId = sessionStorage.getItem('course_id');
    let response: any = await API.graphql(
      graphqlOperation(publishToCanvas, {
        courseId: courseId,
        assignmentId: assignmentId,
        status: !status,
        type: 'ASSIGNMENT',
      }),
    );
    response = JSON.parse(response.data.publishToCanvas);
    if (response.statusCode === 200) {
      const assignmentsList = this.state.assignmentsArr;
      for (const assignment of assignmentsList as any[]) {
        const assignmentData: any = assignment.assignments;
        assignmentData.forEach((data: any, index: any) => {
          if (data.id === response.body.id) {
            assignmentData[index].published = response.body.published;
          }
        });
        const toastMsg = !status
          ? translate('modulepage.publishsuccess')
          : translate('modulepage.unpublishsuccess');

        this.setState({
          assignmentsArr: assignmentsList,
          showToast: true,
          toastMessage: toastMsg,
        });
      }
    } else {
      this.setState({
        error: true,
        loading: false,
      });
    }
  };

  handleToggleAccordionIcon = (e: any) => {
    const collapsed =
      e.target.parentNode.parentNode.parentNode.childNodes[0].childNodes[1].className ===
      'collapse';
    if (!collapsed) {
      e.target.className = e.target.className.replace('icon-down-arrow', 'icon-right-arrow');
    } else {
      e.target.className = e.target.className.replace('icon-right-arrow', 'icon-down-arrow');
    }
  };

  render() {
    return (
      <>
        {this.state.error ? (
          <Error />
        ) : this.state.loading ? (
          <Loader />
        ) : (
          <div>
            {(this.state.assignmentsArr as Array<any>).length > 0 ? (
              <>
                {(this.state.assignmentsArr as Array<any>).map((page: any, index: any) => (
                  <Accordion
                    defaultActiveKey={`${index}`}
                    style={{ cursor: 'pointer' }}
                    key={index}
                    className='mb-5'
                  >
                    <Card className='cardDivAccord' style={{ overflow: 'visible' }}>
                      <Card.Header className='p-1'>
                        <Accordion.Toggle
                          as={Button}
                          variant='link'
                          eventKey={`${index}`}
                          onClick={(e) => this.handleToggleAccordionIcon(e)}
                          className='icon-down-arrow text-decoration-none'
                        >
                          {page.name}
                        </Accordion.Toggle>
                      </Card.Header>
                      <Accordion.Collapse eventKey={`${index}`} id='accordion-collapse-check'>
                        <Card.Body key={index} className='p-0'>
                          <ListGroup variant='flush' className='module-item'>
                            {page.assignments
                              .sort((blockTitle1: any, blockTitle2: any) =>
                                new Date(blockTitle1.created_at) > new Date(blockTitle2.created_at)
                                  ? 1
                                  : -1,
                              )
                              .map((assignmentData: any, index: any) => (
                                <ListGroup.Item
                                  key={index}
                                  className={`pr-1 ${
                                    assignmentData.published ? 'item-published' : 'item-unpublished'
                                  }`}
                                >
                                  <Button
                                    variant='link'
                                    title={
                                      assignmentData.published
                                        ? translate('page.unpublishbutton')
                                        : translate('page.publishbutton')
                                    }
                                    id='tooltip-top-pubModButton'
                                    className='float-right mr-2 pt-0 pb-0'
                                    onClick={async () =>
                                      await this.toggleAssignmentPublish(
                                        assignmentData.id,
                                        assignmentData.published,
                                      )
                                    }
                                  >
                                    <FontAwesomeIcon
                                      icon={assignmentData.published ? faCheckCircle : faBan}
                                      className={`${
                                        assignmentData.published ? 'text-success' : 'text-muted'
                                      }`}
                                    />
                                  </Button>
                                  <FontAwesomeIcon
                                    icon={faEdit}
                                    className={`mr-1 ${
                                      assignmentData.published ? 'text-success' : 'text-muted'
                                    }`}
                                  />{' '}
                                  <Link
                                    to={`/lti/editor/${
                                      assignmentData.id
                                    }/assignment/${encodeURIComponent(assignmentData.name)}`}
                                  >
                                    {assignmentData.name}
                                  </Link>
                                </ListGroup.Item>
                              ))}
                          </ListGroup>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                ))}
              </>
            ) : (
              <div className='container'>
                <div className='border'>
                  <div className='p-3'>{translate('global.noassignmentsfound')}</div>
                </div>
              </div>
            )}
          </div>
        )}
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
      </>
    );
  }
}

export default AssignmentLists;
