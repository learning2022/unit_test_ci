import React from 'react';
import { render } from '@testing-library/react';
import UnAuth from './ltiUnAuth';

describe('For UnAuth', () => {
  it('userQuery type null error handled', () => {
    const props = {
      match: {
        path: '',
        params: {
          lmsUrl: '',
          course_id: '',
        },
      },
      location: {
        pathname: '',
        search: '',
        state: {},
        hash: '',
      },
      history: '',
    } as const;
    render(<UnAuth {...props} />);
    // it renders without any error
    const innerBody = document.body;
    expect(innerBody.firstElementChild?.innerHTML).toBe('');
  });
});
