import { ReactComponent as SearchIcon } from '../assets/Icons/search.svg';
import { ReactComponent as ListIcon } from '../assets/Icons/list.svg';
import { ReactComponent as TabIcon } from '../assets/Icons/tab.svg';
import { ReactComponent as BlankPageIcon } from '../assets/Icons/blank_page.svg';
import { ReactComponent as TemplateIcon } from '../assets/Icons/template.svg';

export const LtiIcons = { SearchIcon, ListIcon, TabIcon, BlankPageIcon, TemplateIcon };
