/* eslint-disable  @typescript-eslint/no-explicit-any */
import CONSTANTS from './loree-editor/constant';
import Bootstrap from './loree-editor/modules/iframe/templates/bootstrap';

export function mocked<F extends (...args: any[]) => any>(f: F): jest.MockedFunction<F> {
  return f as jest.MockedFunction<F>;
}

export function setupLoreeTestIframeData(iframe: HTMLIFrameElement) {
  iframe.id = CONSTANTS.LOREE_IFRAME;
  const wrapper = document.createElement('div');
  wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
  iframe.appendChild(wrapper);
  iframe.innerHTML += Bootstrap;
  document.body.appendChild(iframe);

  const iframeDoc = (iframe.contentWindow as Window).document;
  iframeDoc.open().write(iframe.innerHTML);
  iframeDoc.close();
}
