// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import 'mutationobserver-shim';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { toHaveNoViolations } from 'jest-axe';
import { init as initA11y } from './loree-editor/a11y-client/init';

configure({ adapter: new Adapter() });

jest.setTimeout(15000);
// Accessibility
expect.extend(toHaveNoViolations);
// Initialize our custom rules
initA11y();

global.console.debug = jest.fn();
global.console.log = jest.fn();
global.console.info = jest.fn();
global.console.warn = jest.fn();
global.console.error = jest.fn();
