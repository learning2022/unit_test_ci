/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment, useState, useEffect } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHamburger } from '@fortawesome/free-solid-svg-icons';
import { Auth } from 'aws-amplify';
import NewProject from '../../views/project/newProject';
import BuildPage from '../../views/page/buildPage';
import './dashboardHeader.scss';

interface HeaderProps {
  onClick?: (e: React.MouseEvent<HTMLInputElement>) => void;
}

export const DashboardHeader: React.FC<HeaderProps> = (props: HeaderProps): JSX.Element => {
  const [user, setUsername] = useState('User');
  const history = useHistory();
  const username = async function () {
    await Auth.currentAuthenticatedUser().then(user => {
      setUsername(user.attributes.name);
    });
  };
  useEffect(() => {
    username();
  }, []);
  return (
    <Fragment>
      <Navbar className='dashboard-header-layout py-0 pb-3 pb-md-4 border-bottom'>
        <div className='dashboard-welcome-user mt-md-5'>
          <small className='text-secondary'>Welcome</small>
          <Navbar.Brand href='#' className='text-primary font-weight-bold text-capitalize'>
            {user}
          </Navbar.Brand>
          <span onClick={props.onClick}>
            <FontAwesomeIcon icon={faHamburger} className='d-md-none fa-1x' />
          </span>
        </div>
        <Nav className='ml-auto mr-md-0 mt-3 nav-wrapper'>
          <NewProject history={history} />
          <BuildPage />
        </Nav>
      </Navbar>
    </Fragment>
  );
};
