/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import { DashboardHeader } from './index';
import awsAmplify from 'aws-amplify';
jest.mock('aws-amplify');

it('mock authenticator', () => {
  awsAmplify.Auth.signIn.mockRejectedValue('mock error');
  awsAmplify.Auth.currentAuthenticatedUser.mockResolvedValue('mock user');
});

const dashboardHead = mount(
  <Router>
    <DashboardHeader />
  </Router>,
);

describe('Dashboard header layout', () => {
  test('header rendering', () => {
    expect(dashboardHead.find('nav')).toHaveLength(1);
    expect(dashboardHead.find('.navbar')).toHaveLength(1);
  });

  test('render dashboard-header-layout wrapper class', () => {
    expect(dashboardHead.find('.dashboard-header-layout')).toHaveLength(2);
  });

  test('render dashboard-header-layout wrapper user class', () => {
    expect(dashboardHead.find('.dashboard-welcome-user')).toHaveLength(1);
  });

  test('render dashboard-header with welcome text', () => {
    expect(dashboardHead.text()).toContain('Welcome');
  });

  test('render dashboard nav with navbar brand for username', () => {
    expect(dashboardHead.find('a').at(0).prop('className')).toContain('navbar-brand');
  });

  test('render dashboard nav with Create Project Link', () => {
    expect(dashboardHead.find('a').at(1).prop('id')).toContain('dashboard-create-page');
  });
  test('render dashboard nav with Create Project Link with approp text', () => {
    expect(dashboardHead.find('a').at(1).text()).toEqual('create_page.svgCreate Page');
  });

  test('render dashboard nav with Create Page Link', () => {
    expect(dashboardHead.find('a').at(1).prop('id')).toContain('dashboard-create-page');
  });

  test('render dashboard nav with Create Page Link with approp text', () => {
    expect(dashboardHead.find('a').at(1).text()).toEqual('create_page.svgCreate Page');
  });

  test('render img tag within create link', () => {
    expect(dashboardHead.find('#dashboard-create-page').at(0).html()).toContain('svg');
  });
});
