/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { NavDropdown } from 'react-bootstrap';

interface DropDownInput {
  title: string;
  list: any;
  action: any;
}

const customDropDown: React.FC<DropDownInput> = (prop: DropDownInput) => {
  return (
    <NavDropdown title={prop.title} id='basic-nav-dropdown' onSelect={prop.action}>
      {prop.list.map((item: any, index: any) => (
        <NavDropdown.Item key={index} eventKey={item.value}>
          {item.label}
        </NavDropdown.Item>
      ))}
    </NavDropdown>
  );
};

export default customDropDown;
