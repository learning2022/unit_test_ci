import React from 'react';
import ReactDOM from 'react-dom';
import CustomDropDown from './customDropDown';
import { shallow } from 'enzyme';

const data = [{ title: '', list: '', action: true }];
const DropDownInput = {
  list: data,
  title: '',
  action: true,
};

describe('customDropDown', () => {
  it('renders customDropDown fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CustomDropDown {...DropDownInput} />, div);
  });

  it('renders customDropDown without crashing', () => {
    shallow(<CustomDropDown {...DropDownInput} />);
  });
});
