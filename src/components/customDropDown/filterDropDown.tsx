/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Dropdown, DropdownButton } from 'react-bootstrap';
import SelectableContext from 'react-bootstrap/SelectableContext';
interface DropDownInput {
  title: string;
  list: any;
  action: any;
  dropdownID: string;
}

const FilterDropDown: React.FC<DropDownInput> = (prop: DropDownInput) => {
  return (
    // @ts-ignore
    <SelectableContext.Provider value={false}>
      <DropdownButton alignRight id={prop.dropdownID} className='dropdown-layout' title={prop.title} onSelect={prop.action}>
        {prop.list.map((item: any, index: any) => (
          <Dropdown.Item active={prop.title === item.value} key={index} eventKey={item.value}>
            {item.value}
          </Dropdown.Item>
        ))}
      </DropdownButton>
    </SelectableContext.Provider>
  );
};

export default FilterDropDown;
