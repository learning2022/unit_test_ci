import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Form, InputGroup } from 'react-bootstrap';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

interface SecretTextInputProps {
  keyName: string;
  defaultHidden: boolean;
  label: string;
  value: string;
  isEditable: boolean;
  onChange: (value: string, key: string) => void;
  index: number;
}

function SecretTextInput(props: SecretTextInputProps) {
  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.onChange(event.target?.value, props.keyName);
  };

  const [defaultHidden, setIsHidden] = useState(props.defaultHidden);
  const handleClick = () => setIsHidden(!defaultHidden);

  return (
    <InputGroup className='mb-2'>
      <Form.Control
        id={`SecretTextInput-${props.keyName}-${props.index}`}
        type={defaultHidden ? 'password' : 'text'}
        placeholder={props.label}
        value={props.value}
        onChange={onChange}
        disabled={!props.isEditable}
      />
      <InputGroup.Prepend>
        <InputGroup.Text data-testid={`hide-button-${props.keyName}`} onClick={handleClick}>
          <FontAwesomeIcon icon={defaultHidden ? faEyeSlash : faEye} />
        </InputGroup.Text>
      </InputGroup.Prepend>
    </InputGroup>
  );
}

export default SecretTextInput;
