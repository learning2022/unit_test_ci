/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import SignUp from './signUp';

jest.mock('aws-amplify');

const signUpTrigger = mount(
  <Router>
    <SignUp />
  </Router>,
);

describe('Signup form TDD required fields exists', () => {
  it('whether the button is Signup', () => {
    expect(signUpTrigger.find('Button')).toHaveLength(1);
    expect(signUpTrigger.find('Button').text()).toEqual('Start Loree Journey');
  });

  it('signup modal opened', () => {
    const handleShow = jest.fn();
    signUpTrigger.find('Button').simulate('click', handleShow);
    expect(handleShow.mock.calls.length).toBeCalledWith;
  });

  it('Form existance', () => {
    expect(signUpTrigger.find('Form')).toHaveLength(1);
  });

  it('it should have username and password field', () => {
    expect(signUpTrigger.find('input')).toHaveLength(4);
  });

  it('must have expected type attribs of form fields', () => {
    expect(signUpTrigger.find('input'!).at(0).props()['type']).toBe('text');
    expect(signUpTrigger.find('input'!).at(1).props()['type']).toBe('text');
    expect(signUpTrigger.find('input'!).at(2).props()['type']).toBe('email');
    expect(signUpTrigger.find('input'!).at(3).props()['type']).toBe('password');
  });

  it('must have expected name attribs of form fileds', () => {
    expect(signUpTrigger.find('input'!).at(0).props()['name']).toBe('firstname');
    expect(signUpTrigger.find('input'!).at(1).props()['name']).toBe('lastname');
    expect(signUpTrigger.find('input'!).at(2).props()['name']).toBe('email');
    expect(signUpTrigger.find('input'!).at(3).props()['name']).toBe('password');
  });

  it('submit button and cancel button is present', () => {
    expect(signUpTrigger.find({ 'data-testid': 'submitbutton' }).at(0)).toHaveLength(1);
    expect(signUpTrigger.find({ 'data-testid': 'cancel' }).at(0)).toHaveLength(0);
  });

  it('submit button functionality', () => {
    const handleSubmit = jest.fn();
    signUpTrigger.find({ 'data-testid': 'submitbutton' }).at(0).simulate('click', handleSubmit);
    expect(handleSubmit.mock.calls.length).toBeCalled;
  });
});
