/* eslint-disable */ // Remove this line when editing this file
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { Button, Form, Spinner } from 'react-bootstrap';
import { Auth } from 'aws-amplify';
import { useHistory, Link } from 'react-router-dom';
import { ResetPassword } from './resetPassword';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
interface AuthProps {
  id?: string;
  handleModal?: () => void;
}

const SignInModal: React.FC<AuthProps> = props => {
  const initialState = { loginUsername: '', loginPassword: '' };
  const [formState, setFormState] = useState(initialState as any);
  const [errors, setErrors] = useState(initialState as any);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [alertMsg, setAlertMsg] = useState('');
  const [errormsg, setErrorMsg] = useState(false);
  const [forgotPassword, setForgotPassword] = useState(false);
  const initialForgotpassword = { forgotEmail: '' };
  const [forgotState, setForgotState] = useState<any>(initialForgotpassword);
  const [resetForm, setResetForm] = useState(false);
  const intialResetState = { verificationCode: '', newPassword: '' };
  const [resetState, setResetState] = useState<any>(intialResetState);
  const [isLoading, setIsLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const history = useHistory();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    if (name === 'forgotEmail') {
      setForgotState({
        [name]: value,
      });
    } else if (name === 'verificationCode' || name === 'newPassword') {
      setResetState({
        ...resetState,
        [name]: value,
      });
    } else {
      setFormState({
        ...formState,
        [name]: value,
      });
    }
    if (event.target.name in errors) {
      delete errors[event.target.name];
    }
  };

  function validation(values: any) {
    const errors: any = {};
    const EmailValidation = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g);
    const PasswordCriteriaExp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&])(?=.{8,})');
    const EmptyFieldMessage = 'Field must not be empty';
    const UsernameEmpty = 'Email must not be empty';
    const PasswordEmpty = 'Password must not be empty';
    const EmailIdErrorMessge = 'Email Id is not valid';
    const PasswordCriteria = 'Password doesnt match the following criteria';

    if ('loginUsername' in values && 'loginPassword' in values) {
      if (!values.loginUsername) {
        errors.loginUsername = UsernameEmpty;
      } else if (!EmailValidation.test(values.loginUsername)) {
        errors.loginUsername = EmailIdErrorMessge;
      } else {
        return errors;
      }
      if (!values.loginPassword) {
        errors.loginPassword = PasswordEmpty;
      } else {
        return errors;
      }
    }
    if ('forgotEmail' in values) {
      if (!values.forgotEmail) {
        errors.forgotEmail = UsernameEmpty;
      } else if (!EmailValidation.test(values.forgotEmail)) {
        errors.forgotEmail = EmailIdErrorMessge;
      } else {
        return errors;
      }
    }
    if ('verificationCode' in values && 'newPassword' in values) {
      if (!values.verificationCode) {
        errors.verificationCode = EmptyFieldMessage;
      }
      if (!values.newPassword) {
        errors.newPassword = PasswordEmpty;
      } else if (!PasswordCriteriaExp.test(values.newPassword)) {
        errors.newPassword = PasswordCriteria;
      }
    }
    return errors;
  }

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setErrorMsg(false);
    setErrors(validation(formState));
    if (forgotPassword) {
      setErrors(validation(forgotState));
    }
    if (resetForm) {
      setErrors(validation(resetState));
    }
    setIsSubmitting(true);
  };

  const submit = () => {
    if (!forgotPassword) {
      const email = formState.loginUsername;
      const password = formState.loginPassword;
      if (Object.keys(errors).length === 0) {
        setIsLoading(true);
        Auth.signIn(email, password)
          .then(user => {
            setIsLoading(false);
            localStorage.setItem('ReactAmplify.TokenKey', user.signInUserSession.accessToken.jwtToken);
            history.push('/dashboard');
          })
          .catch(error => {
            setIsLoading(false);
            if (error.code === 'NotAuthorizedException') {
              setAlertMsg(error.message);
              setErrorMsg(true);
            }
            if (error.message === 'User does not exist.') {
              setAlertMsg(error.message);
              setErrorMsg(true);
            }
            if (error.code === 'InvalidParameterException') {
              setAlertMsg(error.message);
              setErrorMsg(true);
            }
            throw error;
          });
      }
    } else if (forgotPassword && !resetForm) {
      if (Object.keys(errors).length === 0) {
        setIsLoading(true);
        const mailId = forgotState.forgotEmail.trim();
        Auth.forgotPassword(mailId)
          .then(() => {
            setResetForm(true);
            setErrorMsg(false);
            setIsLoading(false);
          })
          .catch(error => {
            setIsLoading(false);
            if (error.code === 'UserNotFoundException') {
              setAlertMsg('Email ID not found');
              setErrorMsg(true);
            }
            throw error;
          });
      }
    } else {
      if (Object.keys(errors).length === 0) {
        setIsLoading(true);
        const mailId = forgotState.forgotEmail.trim();
        const verifyCode = resetState.verificationCode.trim();
        const newPassword = resetState.newPassword.trim();
        Auth.forgotPasswordSubmit(mailId, verifyCode, newPassword)
          .then(() => {
            setErrorMsg(false);
            setIsLoading(false);
            setErrorMsg(false);
            setSuccess(true);
            setAlertMsg('Your password has been changed successfully.');
            Auth.signIn(mailId, newPassword)
              .then(user => {
                setIsLoading(false);
                localStorage.setItem('ReactAmplify.TokenKey', user.signInUserSession.accessToken.jwtToken);
                history.push('/dashboard');
              })
              .catch(() => history.push('/'));
          })
          .catch(error => {
            setIsLoading(false);
            if (error.code === 'CodeMismatchException') {
              setAlertMsg(error.message);
              setErrorMsg(true);
            }
            throw error;
          });
      }
    }
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting === true) {
      submit();
    }
    // eslint-disable-next-line
  }, [errors]);

  return (
    <>
      <div className='container-max-width'>
        <div className='text-center'>
          {!forgotPassword ? (
            <div className='container-min-width'>
              <div>
                <FontAwesomeIcon onClick={props.handleModal} className='d-inline-block back-arrow' icon={faAngleLeft} />
                <span>
                  <h5 className='d-inline-block auth-title'>Log in to Loree.</h5>
                </span>
              </div>
            </div>
          ) : forgotPassword && !resetForm ? (
            <div className='container-min-width'>
              <div>
                <FontAwesomeIcon
                  onClick={() => {
                    setForgotPassword(false);
                    setIsLoading(false);
                    setErrorMsg(false);
                  }}
                  icon={faAngleLeft}
                  className='d-inline-block back-arrow'
                />
                <span>
                  <h5 className='d-inline-block auth-title'>Forgotten your password.</h5>
                </span>
              </div>
              <h6 className='message'>Do not fret! Enter your email below and well send you an email to reset your password.</h6>
            </div>
          ) : (
            <div className='container-min-width'>
              <h5 className='auth-title'>Reset Password</h5>
            </div>
          )}
        </div>
        {success ? <p className='alert alert-success'>{alertMsg}</p> : ''}
        {errormsg ? <p className='alert alert-danger'>{alertMsg}</p> : ''}
        <Form>
          {!forgotPassword ? (
            <>
              <Form.Group controlId='loreeEmail'>
                <Form.Label>Email *</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter email'
                  name='loginUsername'
                  onChange={handleChange}
                  value={formState.loginUsername || ''}
                  className={`${errors.loginUsername && 'border-danger'}`}
                  data-testid='loginUsername'
                />
                {errors.loginUsername && (
                  <small className='text-danger'>
                    <span> {errors.loginUsername} </span>
                  </small>
                )}
              </Form.Group>
              <Form.Group controlId='loreePassword'>
                <Form.Label>Password *</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Password'
                  name='loginPassword'
                  onChange={handleChange}
                  value={formState.loginPassword || ''}
                  className={`${errors.loginPassword && 'border-danger'}`}
                  data-testid='loginPassword'
                />
                {errors.loginPassword && (
                  <small className='text-danger'>
                    <span> {errors.loginPassword} </span>
                  </small>
                )}
              </Form.Group>
            </>
          ) : forgotPassword && !resetForm ? (
            <>
              <Form.Group controlId='loreeForgotEmail'>
                <Form.Label>Email Address *</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter email'
                  name='forgotEmail'
                  onChange={handleChange}
                  value={forgotState.forgotEmail || ''}
                  className={`${errors.forgotEmail && 'border-danger'}`}
                  data-testid='forgotEmail'
                />
                {errors.forgotEmail && (
                  <small className='text-danger'>
                    <span>{errors.forgotEmail}</span>
                  </small>
                )}
              </Form.Group>
            </>
          ) : (
            <ResetPassword resetState={resetState} handleChange={handleChange} errors={errors} />
          )}
        </Form>
        <div className='text-center'>
          <Button variant='secondary' onClick={handleSubmit} data-testid='submitbutton' disabled={success ? true : false}>
            {isLoading ? (
              <>
                <Spinner animation='border' size='sm' />
                <span className='ml-2'>{forgotPassword && !resetForm ? 'Sending Code' : resetForm ? 'Resetting' : 'Signing In'}</span>
              </>
            ) : (
              <span>{forgotPassword && !resetForm ? 'Continue' : resetForm ? 'Reset' : 'Log in'}</span>
            )}
          </Button>
        </div>
        {!forgotPassword && !resetForm ? (
          <div className='d-flex flex-column'>
            <div>
              <Link
                to='/'
                className='footer-message'
                onClick={() => {
                  setForgotPassword(true);
                  setErrors(initialState);
                  setForgotState(initialForgotpassword);
                  setErrorMsg(false);
                  setIsLoading(false);
                }}
              >
                <p className='d-inline-block border-bottom footer-link'>Forgot Password?</p>
              </Link>{' '}
            </div>
            <div>
              <p className='footer-message'>
                New to Loree?{' '}
                <Link className='footer-message-text' to='/' onClick={props.handleModal}>
                  Sign Up
                </Link>
              </p>
            </div>
          </div>
        ) : forgotPassword && !resetForm ? (
          <div className='text-center'>
            <Link
              to='/'
              className='footer-message'
              onClick={() => {
                setForgotPassword(false);
                setIsLoading(false);
                setErrorMsg(false);
              }}
            >
              <p className='d-inline-block border-bottom footer-link'>Return to Login</p>
            </Link>{' '}
          </div>
        ) : (
          ''
        )}
      </div>
    </>
  );
};

export default SignInModal;
