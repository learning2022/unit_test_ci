/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { shallow, mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import awsAmplify from 'aws-amplify';
import { Auth } from 'aws-amplify';
import SignIn from './signIn';

jest.mock('aws-amplify');

const loginTrigger = mount(
  <Router>
    <SignIn />
  </Router>,
);
const validInput: any = {
  EMAIL: 'promail211@gmail.com', //Need to give your email and password
  PASSWORD: 'Qwerty@123',
};
const InvalidInput = {
  EMAIL: 'test@gmail.com',
  PASSWORD: 'Testtest@123',
};

describe('Login form TDD required fields exists', () => {
  it('whether the button is login', () => {
    expect(loginTrigger.find('Button')).toHaveLength(1);
    expect(loginTrigger.find('Button').at(0).text()).toEqual('Log in');
  });

  it('modol pop up opened with show class', () => {
    expect(loginTrigger.find('.modal .show')).toHaveLength(0);
  });

  it('bootstrap modal classes existance', () => {
    expect(loginTrigger.find('.modal-content')).toHaveLength(0);
    expect(loginTrigger.find('.modal-header')).toHaveLength(0);
    expect(loginTrigger.find('.modal-body')).toHaveLength(0);
    expect(loginTrigger.find('.modal-footer')).toHaveLength(0);
  });

  it('Form existance', () => {
    expect(loginTrigger.find('Form')).toHaveLength(1);
  });

  it('Form Input custom component existance', () => {
    expect(loginTrigger.find('input')).toHaveLength(2);
  });

  it('must have expected type attribs of form fileds', () => {
    expect(loginTrigger.find('input'!).first().props()['type']).toBe('email');
    expect(loginTrigger.find('input'!).last().props()['type']).toBe('password');
  });

  it('username onchange calling', () => {
    const logSpy = jest.spyOn(console, 'log');
    expect(logSpy).toBeCalledWith;
    loginTrigger
      .find({ 'data-testid': 'loginUsername' })
      .first()
      .simulate('change', { target: { value: 'it user' } });
    loginTrigger.update();
    expect(logSpy).toBeCalledWith;
    logSpy.mockRestore();
  });

  test('password onchange calling', () => {
    const logSpy = jest.spyOn(console, 'log');
    expect(logSpy).toBeCalledWith;
    loginTrigger
      .find({ 'data-testid': 'loginPassword' })
      .last()
      .simulate('change', { target: { value: 'Test password' } });
    loginTrigger.update();
    expect(logSpy).toBeCalledWith;
    logSpy.mockRestore();
  });

  it('submit button and cancel button is present', () => {
    expect(loginTrigger.find({ 'data-testid': 'submitbutton' }).at(0)).toHaveLength(1);
    expect(loginTrigger.find({ 'data-testid': 'cancel' }).at(0)).toHaveLength(0);
  });

  it('submit button functionality', () => {
    const handleSubmit = jest.fn();
    loginTrigger.find({ 'data-testid': 'submitbutton' }).at(0).simulate('click', handleSubmit);
    expect(handleSubmit.mock).toBeCalled;
  });

  it('mock authenticator', () => {
    awsAmplify.Auth.signIn.mockRejectedValue('mock error');
    awsAmplify.Auth.currentAuthenticatedUser.mockResolvedValue('mock user');
  });
  it('mock authenticator auth', () => {
    Auth.signIn(validInput).then(userInput => {
      console.log(userInput);
    });
  });

  it('close button should close modal', () => {
    const handleClose = jest.fn();
    expect(handleClose.mock.calls.length).toBeCalledWith;
  });
});
