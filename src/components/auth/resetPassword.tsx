/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

interface ResetProps {
  resetState?: any;
  errors?: any;
  handleChange?: any;
}

export const ResetPassword: React.FC<ResetProps> = props => {
  return (
    <>
      <Form.Group controlId='loreeVerifyCode'>
        <Form.Label>Confirmation Code</Form.Label>
        <Form.Control
          type='text'
          placeholder='Enter your verification code'
          name='verificationCode'
          onChange={props.handleChange}
          value={props.resetState.verificationCode || ''}
          className={`${props.errors.verificationCode && 'border-danger'}`}
          data-testid='verificationCode'
        />
        {props.errors.verificationCode && <small className='text-danger'>{props.errors.verificationCode}</small>}
      </Form.Group>
      <Form.Group controlId='loreeNewPassword'>
        <Form.Label>New Password</Form.Label>
        <Form.Control
          type='password'
          placeholder='Enter your new password'
          name='newPassword'
          onChange={props.handleChange}
          value={props.resetState.newPassword || ''}
          className={`${props.errors.newPassword && 'border-danger'}`}
          data-testid='newPassword'
        />
        {props.errors.newPassword && <small className='text-danger'>{props.errors.newPassword}</small>}
      </Form.Group>
    </>
  );
};

ResetPassword.propTypes = {
  handleChange: PropTypes.element.isRequired,
  resetState: PropTypes.element.isRequired,
  errors: PropTypes.element.isRequired,
};
