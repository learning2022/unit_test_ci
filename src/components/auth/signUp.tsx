/* eslint-disable */ // Remove this line when editing this file
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Container, Row, Col, Spinner } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Auth } from 'aws-amplify';
import './userAuth.scss';

interface AuthProps {
  id?: string;
  class?: string;
  show?: boolean;
  setSignup?: any;
}

const SignUpModal: React.FC<AuthProps> = props => {
  const [show, setShow] = useState(true);
  const initialState = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    confirmCode: '',
  };
  const [formState, setFormState] = useState(initialState as any);
  const [errors, setErrors] = useState(initialState as any);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [alertMsg, setAlertMsg] = useState('');
  const [success, setSuccess] = useState(false);
  const [errormsg, setErrorMsg] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const history = useHistory();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormState({
      ...formState,
      [name]: value,
    });
    if (event.target.name in errors) {
      delete errors[event.target.name];
    }
  };

  function validation(values: any) {
    const errors: any = {};
    const EmailValidation = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g);
    const PasswordCriteriaExp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&])(?=.{8,})');
    const firstnameEmptyField = 'FirstName must not be empty';
    const lastnameEmptyField = 'LastName must not be empty';
    const emailEmptyField = 'Email must not be empty';
    const PasswordEmptyField = 'Password must not be empty';
    const EmailIdErrorMessge = 'Email ID is not valid';
    const PasswordCriteria = 'Password doesnt match the following criteria';

    if (!values.firstname) {
      errors.firstname = firstnameEmptyField;
    }
    if (!values.lastname) {
      errors.lastname = lastnameEmptyField;
    }
    if (!values.email) {
      errors.email = emailEmptyField;
    } else if (!EmailValidation.test(values.email)) {
      errors.email = EmailIdErrorMessge;
    }
    if (!values.password) {
      errors.password = PasswordEmptyField;
    } else if (!PasswordCriteriaExp.test(values.password)) {
      errors.password = PasswordCriteria;
    }
    return errors;
  }

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    setErrors(validation(formState));
    if (Object.keys(errors).length === 0) {
      setIsSubmitting(true);
      setErrorMsg(false);
      setSuccess(false);
    }
  };

  const submit = () => {
    if (Object.keys(errors).length === 0) {
      setIsLoading(true);
      Auth.signUp({
        username: formState.email,
        password: formState.password,
        attributes: {
          email: formState.email,
          name: formState.firstname + '' + formState.lastname,
        },
      })
        .then(() => {
          setShow(false);
          setAlertMsg('User registered successfully, Please check your mail');
          setSuccess(true);
          setErrorMsg(false);
          setIsSubmitting(false);
          setIsLoading(false);
        })
        .catch(e => {
          setIsSubmitting(false);
          setIsLoading(false);
          setSuccess(false);
          if (e.code === 'UsernameExistsException') {
            setAlertMsg(e.message);
            setErrorMsg(true);
          }
        });
    }
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting === true) {
      submit();
    }
    // eslint-disable-next-line
  }, [errors]);
  const resend = () => {
    Auth.resendSignUp(formState.email)
      .then(() => {
        setAlertMsg('Code resent successfully');
        setSuccess(true);
        setErrorMsg(false);
        setIsSubmitting(false);
      })
      .catch(e => {
        setIsSubmitting(false);
        setAlertMsg('Code not send!');
        setErrorMsg(true);
        setSuccess(false);
        setIsSubmitting(false);
      });
  };
  const handleConfirm = (e: React.FormEvent) => {
    e.preventDefault();
    if (formState.confirmCode !== '') {
      setIsSubmitting(true);
      setSuccess(false);
      setErrorMsg(false);
      Auth.confirmSignUp(formState.email, formState.confirmCode)
        .then(() => {
          Auth.signIn(formState.email, formState.password).then(user => {
            localStorage.setItem('ReactAmplify.TokenKey', user.signInUserSession.accessToken.jwtToken);
            history.push('/dashboard');
          });
        })
        .catch(e => {
          setIsSubmitting(false);
          if (e.code === 'CodeMismatchException') {
            setAlertMsg(e.message);
            setErrorMsg(true);
          }
        });
    } else {
      setSuccess(false);
      setAlertMsg('Confirmation code cannot be empty');
      setErrorMsg(true);
    }
  };

  return (
    <>
      {show ? (
        <div>
          {success ? <p className='alert alert-success'>{alertMsg}</p> : ''}
          {errormsg ? <p className='alert alert-danger'>{alertMsg}</p> : ''}
          <div className='text-center'>
            <div className='container-max-width'>
              <h3 className='header'>“Welcome to Loree, the tool that helps you build entire pages in minutes”</h3>
              <div className='header-divider'></div>
              <h4 className='message-title'>Get Started with Loree.</h4>
              <h6 className='message'>Create an account, its free. Loree is loved by beginners, experts, designers and individuals.</h6>
            </div>
          </div>
          <Container>
            <Form>
              <Row>
                <Col xs={6}>
                  <Form.Group controlId='loreeFirstname'>
                    <Form.Label>FirstName</Form.Label>
                    <Form.Control
                      type='text'
                      placeholder='Enter firstname'
                      name='firstname'
                      onChange={handleChange}
                      value={formState.firstname || ''}
                      className={`${errors.firstname && 'border-danger'}`}
                      data-testid='firstname'
                    />
                    {errors.firstname && (
                      <small className='text-danger'>
                        <span>{errors.firstname}</span>
                      </small>
                    )}
                  </Form.Group>
                </Col>
                <Col xs={6}>
                  <Form.Group controlId='loreeLastname'>
                    <Form.Label>LastName</Form.Label>
                    <Form.Control
                      type='text'
                      placeholder='Enter lastname'
                      name='lastname'
                      onChange={handleChange}
                      value={formState.lastname || ''}
                      className={`${errors.lastname && 'border-danger'}`}
                      data-testid='lastname'
                    />
                    {errors.lastname && (
                      <small className='text-danger'>
                        <span>{errors.lastname}</span>
                      </small>
                    )}
                  </Form.Group>
                </Col>
              </Row>
              <Form.Group controlId='loreeEmail'>
                <Form.Label>Email *</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter email'
                  name='email'
                  onChange={handleChange}
                  value={formState.email || ''}
                  className={`${errors.email && 'border-danger'}`}
                  data-testid='email'
                />
                {errors.email && (
                  <small className='text-danger'>
                    <span>{errors.email}</span>
                  </small>
                )}
              </Form.Group>
              <Form.Group controlId='loreePassword'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Password'
                  name='password'
                  onChange={handleChange}
                  className={`${errors.password && 'border-danger'}`}
                  data-testid='loginPassword'
                />
                {errors.password && (
                  <small className='text-danger'>
                    <span>{errors.password}</span>
                  </small>
                )}
              </Form.Group>
            </Form>
          </Container>
          <div className='d-flex justify-content-center align-items-center flex-column'>
            <div>
              <Button variant='secondary' className='text-capitalize' onClick={handleSubmit} data-testid='submitbutton'>
                {isLoading ? (
                  <>
                    <Spinner animation='border' size='sm' />
                    <span className='ml-2'>Creating account</span>
                  </>
                ) : (
                  <span>Start Loree Journey</span>
                )}
              </Button>
            </div>
            <div>
              <p className='footer-message'>By signing up, you agree to Loree’s Terms of Use and privacy laws.</p>
            </div>
          </div>
        </div>
      ) : (
        <div className='container-min-width'>
          <Container>
            <Form>
              {success ? <p className='alert alert-success'>{alertMsg}</p> : ''}
              {errormsg ? <p className='alert alert-danger'>{alertMsg}</p> : ''}
              <Form.Group>
                <Form.Label>Email Address *</Form.Label>
                <Form.Control readOnly type='text' placeholder='Enter text' value={formState.email} />
              </Form.Group>
              <Form.Group controlId='loreeConfirmCode'>
                <Form.Label>Confirmation Code</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Confirm your code'
                  name='confirmCode'
                  onChange={handleChange}
                  value={formState.confirmCode || ''}
                  className={`${errors.confirmCode && 'border-danger'}`}
                  data-testid='confirmCode'
                />
                <small>
                  Lost your code?{' '}
                  <span onClick={resend} className='resend-text'>
                    Resend code
                  </span>
                </small>
                {errors.confirmCode && <small className='text-danger'>{errors.confirmCode}</small>}
              </Form.Group>
            </Form>
          </Container>
          <div className='d-flex justify-content-center align-items-center'>
            <Button variant='secondary' onClick={handleConfirm} data-testid='submitbutton'>
              {isSubmitting ? (
                <>
                  <Spinner animation='border' size='sm' />
                  <span className='ml-2'>Confirming</span>
                </>
              ) : (
                <span className='ml-2'>Confirm</span>
              )}
            </Button>
          </div>
        </div>
      )}
    </>
  );
};

SignUpModal.propTypes = {
  id: PropTypes.string,
  class: PropTypes.string,
  show: PropTypes.bool,
  setSignup: PropTypes.func,
};
export default SignUpModal;
