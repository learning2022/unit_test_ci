/* eslint-disable */ // Remove this line when editing this file
import React, { useState } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { Navbar, Nav, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowClose, faSitemap, faUserTie, faTachometerAlt } from '@fortawesome/free-solid-svg-icons';
import Loreelogo from '../../assets/images/Loree-logo.png';
import UserPic from '../../assets/images/user-avatar.png';
import { ReactComponent as Speedometer } from '../../assets/Icons/dashboard.svg';
import { ReactComponent as MyTemplatesIcon } from '../../assets/Icons/my_template.svg';
import { Auth } from 'aws-amplify';
import './leftSideNavbar.scss';
import CONSTANTS from '../../loree-editor/constant';

interface SideNavProps {
  visible?: boolean;
  onClick?: (e: React.MouseEvent<HTMLInputElement>) => void;
  superAdminSidebar?: any;
}

const LeftSidenav: React.FC<SideNavProps> = (props: SideNavProps): JSX.Element => {
  const [isSuperAdmin, setIsSuperAdmin] = useState(false);
  const [userName, setUserName] = useState(null);
  const [userEmail, setUserEmail] = useState(null);

  Auth.currentAuthenticatedUser().then(user => {
    setUserName(user.attributes.name);
    setUserEmail(user.attributes.email);
    const userGroup: any = user.signInUserSession.accessToken.payload['cognito:groups'];
    if (userGroup) setIsSuperAdmin(userGroup.includes('LoreeSuperAdmin'));
  });

  const history = useHistory();

  const handleSignOut = async () => {
    await Auth.signOut({ global: true }).then(() => {
      localStorage.removeItem('ReactAmplify.TokenKey');
      history.push('/');
    });
  };

  return (
    <>
      <Navbar className={`profile-side-nav-wrapper ${!props.visible ? 'd-none d-md-flex' : 'd-md-flex'}`}>
        <span onClick={props.onClick} className='d-md-none fa-1x fa-fw align-self-end'>
          <FontAwesomeIcon icon={faWindowClose} />
        </span>
        <Navbar.Brand href='#' className='loree-nav-brand text-center mb-2 mb-md-5'>
          <Image src={Loreelogo} width={152} height={85} /> <hr />
        </Navbar.Brand>
        <Nav className='side-nav-link-wrapper d-flex flex-column mb-auto pr-4'>
          <NavLink to='/dashboard' className='mb-3'>
            <Speedometer width={20} height={15} className='iconActive' />
            <span className='font-weight-light pl-2'>Dashboard</span>
          </NavLink>
          <NavLink to='/templates' className='mb-3 d-flex align-items-center'>
            <MyTemplatesIcon width={20} height={15} />
            <span className='font-weight-light pl-2'>My Templates</span>
          </NavLink>
          <hr></hr>
          {isSuperAdmin && (
            <React.Fragment>
              <NavLink to='/superadmin/clients' className='mb-3 d-flex align-items-center'>
                <FontAwesomeIcon icon={faUserTie} />
                <span className='font-weight-light pl-2'>Super Admin</span>
              </NavLink>
              {props.superAdminSidebar && (
                <div className='ml-3'>
                  <NavLink to={`/superadmin/${localStorage.getItem('clientId')}/statistics`} className='mb-3 d-flex align-items-center'>
                    <FontAwesomeIcon icon={faTachometerAlt} />
                    <span className='font-weight-light pl-2'>Statistics</span>
                  </NavLink>
                  <NavLink to={`/superadmin/${localStorage.getItem('clientId')}/features`} className='mb-3 d-flex align-items-center'>
                    <FontAwesomeIcon icon={faSitemap} />
                    <span className='font-weight-light pl-2'>Features</span>
                  </NavLink>
                  <NavLink to={`/superadmin/${localStorage.getItem('clientId')}/migrate`} className='mb-3 d-flex align-items-center'>
                    <FontAwesomeIcon icon={faTachometerAlt} />
                    <span className='font-weight-light pl-2'>Migrate</span>
                  </NavLink>
                </div>
              )}
            </React.Fragment>
          )}
        </Nav>
        <Nav className='side-nav-user-link mt-auto mb-3'>
          <div className='my-4'>
            <Nav.Link href='#' className='px-0 text-dark'>
              <span className='font-weight-light'>What&apos;s New</span>
            </Nav.Link>
            <Nav.Link href='#' className='px-0 text-dark'>
              <span className='font-weight-light'>Notifications</span>
            </Nav.Link>
            <Nav.Link href='#' className='px-0 text-dark'>
              <span className='font-weight-light'>Support</span>
            </Nav.Link>
          </div>
        </Nav>
        <div className='user-avatar-wrapper'>
          <div className='side-nav-user-avatar mx-auto mb-4'>
            <Image id={CONSTANTS.LOREE_MAIN_AVTAR} className='user-profile-img' src={UserPic} width={80} height={80} />
          </div>
        </div>
      </Navbar>
      <div id={CONSTANTS.LOREE_AVTAR_MODAL} className='profile-modal d-none'>
        <Image id={CONSTANTS.LOREE_USER_PROFILE_AVTAR} className='user-profile-img mb-1 mt-2' src={UserPic} width={30} height={30} />
        <div>
          <p id={CONSTANTS.LOREE_USER_PROFILE_NAME} className='mb-0 font-weight-bold'>
            {userName}
          </p>
          <small id={CONSTANTS.LOREE_USER_PROFILE_EMAIL} className='mb-0'>
            {userEmail}
          </small>
        </div>
        <hr />
        <NavLink to='/settings' className='text-white d-block pb-2 mt-1'>
          Account Settings
        </NavLink>
        <Nav.Link href={CONSTANTS.LOREE_CANVAS_SUPPORT_PORTAL} className='text-white d-block pb-2 mt-1' target='_blank'>
          Contact Support Team
        </Nav.Link>
        <hr />
        <NavLink exact to='/' className='logout-nav text-white mb-1' onClick={handleSignOut}>
          Logout
        </NavLink>
      </div>
    </>
  );
};

export default LeftSidenav;
