/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import ProfileSidenav from './index';
import Sidenav from '../../lti/admin/sidebar';

const sideNav = mount(
  <Router>
    <ProfileSidenav />
  </Router>,
);
describe('Profile Side navigation UI', () => {
  test('render the Side Nav', () => {
    expect(sideNav.find('nav')).toHaveLength(1);
    expect(sideNav.find('.navbar')).toHaveLength(1);
  });

  test('render side nav with side nav wrapper class', () => {
    expect(sideNav.find('.profile-side-nav-wrapper')).toHaveLength(2);
  });

  test('render side nav with loree brand', () => {
    expect(sideNav.find('a').at(0).prop('className')).toContain('loree-nav-brand');
  });

  test('render dashboard link', () => {
    expect(sideNav.find('a').at(1).html()).toEqual(
      '<a class="mb-3" href="/dashboard"><svg width="20" height="15" class="iconActive">dashboard.svg</svg><span class="font-weight-light pl-2">Dashboard</span></a>',
    );
  });

  test('dashboard link navigate to dashboard page', () => {
    expect(sideNav.find('a').at(1).prop('href')).toEqual('/dashboard');
  });

  test('side nav user link existance', () => {
    expect(sideNav.find('.side-nav-user-link')).toHaveLength(3);
  });

  test('render My Template link', () => {
    expect(sideNav.find('a').at(2).text()).toEqual(`my_template.svgMy Templates`);
  });

  test('render Whats new link', () => {
    expect(sideNav.find('a').at(3).text()).toEqual(`What's New`);
  });

  test('render notification link', () => {
    expect(sideNav.find('a').at(4).text()).toEqual('Notifications');
  });

  test('render support link', () => {
    expect(sideNav.find('a').at(5).text()).toEqual('Support');
  });

  test('render side-nav-user-avatar', () => {
    expect(sideNav.find('.side-nav-user-avatar')).toHaveLength(1);
  });

  test('render side-nav-user-avatar with loree logo component', () => {
    expect(sideNav.find('.side-nav-user-avatar').html()).toContain('img');
  });

  test('render the avtar modal d-none by default', () => {
    expect(sideNav.find('.profile-modal').hasClass('d-none')).toBeTruthy();
  });
  test('Should have an option to logout from avtar modal', () => {
    expect(sideNav.find('.logout-nav')).toBeTruthy();
  });

  test('Should have "Log out" as a third navigation option', () => {
    expect(sideNav.find('.logout-nav').at(0).text()).toEqual('Logout');
  });

  test('signout the user on clickig the Logout navigation from avtar modal', () => {
    const handleSignOut = jest.fn();
    sideNav.find('.text-white').at(0).simulate('click');
    expect(handleSignOut.mock).toBeCalled;
  });
});
