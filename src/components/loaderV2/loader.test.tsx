import React from 'react';
import { render } from '@testing-library/react';

import { Loader } from './loader';

test('loader', async () => {
  const { getByText } = render(<Loader />);
  expect(getByText('Loading...')).toBeInTheDocument();
});
