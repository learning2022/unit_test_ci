import React from 'react';
import { ReactComponent as LoadingIcon } from '../../assets/Icons/elements/loader.svg';

export const Loader = (): JSX.Element => {
  return (
    <div id='modal-loader' className='m-auto justify-content-center' role='listitem'>
      <div className='icon rotating' key='loree-loader-icon'>
        <LoadingIcon key='loree-loader' />
      </div>
      <div className='title ml-3' key='loree-loading-text'>
        Loading...
      </div>
    </div>
  );
};
