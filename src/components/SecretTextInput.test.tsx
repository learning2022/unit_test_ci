import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';

import SecretTextInput from './SecretTextInput';
import axe from '../../src/axeHelper';

describe('secretTextInput', () => {
  const props = {
    keyName: 'toolName',
    defaultHidden: false,
    label: 'Tool Name',
    value: 'H5P',
    onChange: jest.fn(),
    index: 1,
    isEditable: true,
  };

  test('loads and displays secret text input with visible text', async () => {
    render(<SecretTextInput {...props} />);
    expect(screen.getByDisplayValue('H5P')).toBeInTheDocument();
  });

  test('changes the input type when clicked', async () => {
    const props = {
      keyName: 'toolName',
      defaultHidden: true,
      label: 'Tool Name',
      value: 'H5P',
      onChange: jest.fn(),
      index: 1,
      isEditable: true,
    };

    render(<SecretTextInput {...props} />);
    expect(screen.getByDisplayValue('H5P')).toBeInTheDocument();
    expect(screen.getByDisplayValue('H5P').id).toBe('SecretTextInput-toolName-1');
    expect(screen.getByDisplayValue('H5P')).toHaveAttribute('type', 'password');

    const button = screen.getByTestId('hide-button-toolName');
    // change type to text
    fireEvent.click(button);
    expect(screen.getByDisplayValue('H5P')).toHaveAttribute('type', 'text');
    // change type to password
    fireEvent.click(button);
    expect(screen.getByDisplayValue('H5P')).toHaveAttribute('type', 'password');
  });

  test('should pass accessibility tests', async () => {
    const secretTextInput = render(<SecretTextInput {...props} />);
    const results = await axe(secretTextInput.container);
    expect(results).toHaveNoViolations();
  });
});
