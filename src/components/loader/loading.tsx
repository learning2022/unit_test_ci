import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import './loading.scss';

interface LoaderProps {
  iconSize?: string;
}

const Loading: React.FC<LoaderProps> = (prop) => {
  return (
    <FontAwesomeIcon
      title='Loading, please wait'
      icon={faSpinner}
      className={`fa-spin text-primary loader-icon ${prop.iconSize ? prop.iconSize : 'fa-3x'}`}
    />
  );
};

export default Loading;
