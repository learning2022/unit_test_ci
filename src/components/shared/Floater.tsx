import { Placement } from 'popper.js';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { usePopper } from 'react-popper';
import styled, { StyleSheetManager } from 'styled-components';

const PopperDiv = styled.div<{ zIndex?: number }>`
  z-index: ${(props) => props.zIndex ?? 4000};
`;

const PopperBackground = styled.div<{ zIndex?: number }>`
  position: fixed;
  opacity: 0;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: ${(props) => props.zIndex ?? 3999};
`;

type FloaterPlacement = Placement;

interface FloaterProps {
  referenceElement: Element;
  iframe?: HTMLIFrameElement;
  component: JSX.Element;
  placement?: FloaterPlacement;
  noBackground?: boolean;
  arrow?: boolean;
  className?: string;
  onClose: () => void;
  zIndex?: number;
}

// Get the type of the 3rd parameter to usePopper
type PopperConfigType = Parameters<typeof usePopper>[2];

export default function Floater(props: FloaterProps) {
  const { referenceElement, onClose, iframe, noBackground } = props;
  const [arrowElement, setArrowElement] = useState<HTMLDivElement | null>(null);

  const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);
  const popperData = useMemo(() => {
    const mods: object[] = [
      {
        name: 'offset',
        enabled: true,
        options: {
          offset: [0, 6],
        },
      },
    ];
    if (props.arrow) {
      mods.push({
        name: 'arrow',
        enabled: true,
        options: {
          element: arrowElement,
        },
      });
    }

    const data: PopperConfigType = {
      placement: props.placement ?? 'bottom-end',
      modifiers: mods,
    };
    return data;
  }, [arrowElement, props.placement, props.arrow]);

  const { styles, attributes } = usePopper(referenceElement, popperElement, popperData);

  const onBackgroundClick = useCallback(() => {
    onClose();
  }, [onClose]);

  const handleKeyDown = useCallback(
    (e: React.KeyboardEvent<HTMLDivElement>) => {
      if (e.key === 'Escape') {
        onClose();
      }
    },
    [onClose],
  );

  useEffect(() => {
    const handleKeyPress = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        onClose();
      }
    };

    (iframe?.contentWindow ?? window).addEventListener('keydown', handleKeyPress);

    return () => {
      (iframe?.contentWindow ?? window).removeEventListener('keydown', handleKeyPress);
    };
  }, [onClose, iframe]);

  // If we are injecting into an iframe, we need to inject the styles into the iframe header
  const header = useMemo(() => {
    if (iframe) {
      return iframe.contentDocument?.head;
    }
    return undefined;
  }, [iframe]);

  return (
    <>
      <StyleSheetManager target={header}>
        <>
          {!noBackground && (
            <PopperBackground
              onClick={onBackgroundClick}
              zIndex={props.zIndex ? props.zIndex - 1 : props.zIndex}
            />
          )}
          <PopperDiv
            ref={setPopperElement}
            style={styles.popper}
            {...attributes.popper}
            onKeyDown={handleKeyDown}
            className={props.className}
          >
            {props.component}
            {props.arrow && (
              <div ref={setArrowElement} className='floater-arrow' style={styles.arrow} />
            )}
          </PopperDiv>
        </>
      </StyleSheetManager>
    </>
  );
}
