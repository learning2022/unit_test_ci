import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import Floater from './Floater';

describe('<FLoater>', () => {
  let div: HTMLDivElement;
  beforeEach(() => {
    div = document.createElement('div');
  });
  afterEach(() => {
    div.remove();
  });

  test('renders', () => {
    const x = jest.fn();
    const { container } = render(
      <Floater component={<div />} referenceElement={div} onClose={x} />,
    );
    expect(container).toBeInTheDocument();
  });

  test('escape on document closes it', () => {
    const x = jest.fn();
    render(<Floater component={<div />} referenceElement={div} onClose={x} />);
    fireEvent.keyDown(document, { key: 'Escape' });
    expect(x).toHaveBeenCalled();
  });

  test('other keys on document dont close it', () => {
    const x = jest.fn();
    render(<Floater component={<div />} referenceElement={div} onClose={x} />);
    fireEvent.keyDown(document, { key: 'A' });
    expect(x).not.toHaveBeenCalled();
  });

  test('escape closes it', () => {
    const x = jest.fn();
    const { container } = render(
      <Floater component={<div />} referenceElement={div} onClose={x} />,
    );
    const d = container.querySelectorAll('div')[1];
    fireEvent.keyDown(d, { key: 'Escape' });
    expect(x).toHaveBeenCalled();
  });

  test('other keys dont close it', () => {
    const x = jest.fn();
    const { container } = render(
      <Floater component={<div />} referenceElement={div} onClose={x} />,
    );
    const d = container.querySelectorAll('div')[1];
    fireEvent.keyDown(d, { key: 'B' });
    expect(x).not.toHaveBeenCalled();
  });

  test('click outside closes it', () => {
    const x = jest.fn();
    const { container } = render(
      <Floater component={<div />} referenceElement={div} onClose={x} />,
    );
    const d = container.querySelector('div');
    expect(d).not.toBeNull();
    fireEvent.click(d!, {});
    expect(x).toHaveBeenCalled();
  });
});
