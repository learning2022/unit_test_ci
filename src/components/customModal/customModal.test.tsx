import React from 'react';
import CustomModal from './customModal';
import axe from '../../axeHelper';
import { render, screen, RenderResult } from '@testing-library/react';
import ExternalToolIframe from '../../loree-editor/modules/sidebar/externalTool/externalToolIframe';
import { API, Auth } from 'aws-amplify';

const createLtiLoginHintMock = {
  data: {
    createLtiToolLoginHint: {
      id: '3cf8925e-57cf-43fd-a88d-1617c531a1d9',
      issuerUrl: 'https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool',
      clientId: '123000000000123',
      loreeUserEmail: 'canvas_123670000000000245@example.com',
      messageType: 'LtiResourceLinkRequest',
      resourceLink: 'https://limon.h5p.com/content/1291590289572747719',
      createdAt: '2022-03-30T03:55:08.108Z',
      updatedAt: '2022-03-30T03:55:08.108Z',
    },
  },
};

jest.mock('../../i18n/translate.ts', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

describe('when external tool modal is rendered', () => {
  let customModal: RenderResult;
  beforeEach(() => {
    jest.clearAllMocks();
    const user = {
      attributes: {
        email: 'canvas_123670000000000245@example.com',
        name: 'krishnaveni',
        sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
      },
    };
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => user);
    API.graphql = jest.fn().mockImplementation(() => createLtiLoginHintMock);
    customModal = render(
      <CustomModal
        show
        hide={jest.fn()}
        loadingStatus={false}
        modalTitle='H5P'
        showAddOrContinueButton
        addOrContinueText='Add'
        showCancelButton
        modalBody={
          <ExternalToolIframe
            issuerUrl='https://lt6mhsx54i.execute-api.ap-southeast-2.amazonaws.com/andibox/lti/tool'
            clientId='123000000000123'
            oidcUrl='https://limon.h5p.com/lti/login'
            targetLinkURI='https://limon.h5p.com/content/1291590289572747719'
            messageType='LtiDeepLinkingRequest'
          />
        }
      />,
    );
  });

  test('should pass accessibility tests', async () => {
    const results = await axe(customModal.container);
    expect(results).toHaveNoViolations();
  });

  test('loads content correctly', () => {
    expect(screen.getByTestId('close')).toBeInTheDocument();
    expect(screen.getByText('global.cancel')).toBeInTheDocument();
    expect(screen.getByText('Add')).toBeInTheDocument();
    expect(screen.getByTitle('loree-external-tool')).toBeInTheDocument();
  });

  test('loads header correctly', () => {
    expect(screen.getByText('H5P')).toBeInTheDocument();
  });
});
