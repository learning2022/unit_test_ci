import React, { ReactElement } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { translate } from '../../i18n/translate';
import CONSTANTS from '../../loree-editor/constant';
import Loading from '../loader/loading';
import './customModal.scss';
export interface CustomModalProps {
  modalBody: ReactElement | string;
  show: boolean;
  hide(): void;
  loadingStatus: boolean;
  modalTitle: string;
  showAddOrContinueButton: boolean;
  showCancelButton: boolean;
  addOrContinueText: string;
  saveAndContinueText?: string;
  showSaveContinueButton?: boolean;
  events?: {
    saveContinue(): void;
    addOrContinue(): void;
  };
}

function CustomModal(props: CustomModalProps) {
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.hide}
        backdrop='static'
        animation={false}
        centered
        className={`border-primary ${props.modalTitle === 'Confirm' ? '' : 'custom-modal'}`}
      >
        <Modal.Header closeButton data-testid='close'>
          <Modal.Title>
            <h5 className='text-primary' id='modal_header' aria-level={1}>
              {props.modalTitle}
            </h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body id={CONSTANTS.LOREE_CUSTOM_MODAL_BODY}>
          {props.loadingStatus ? <Loading /> : props.modalBody}
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-center'>
          {props.showCancelButton && (
            <Button
              variant='outline-primary'
              className='modal-footer-cancel-btn editor-custom-btn mx-1'
              onClick={props.hide}
            >
              {translate('global.cancel')}
            </Button>
          )}
          {props.showAddOrContinueButton && (
            <Button
              variant='primary'
              className='editor-custom-btn mx-1'
              type='submit'
              onClick={props?.events?.addOrContinue}
            >
              {props.addOrContinueText}
            </Button>
          )}
          {props.showSaveContinueButton && (
            <Button
              variant='primary'
              className='editor-custom-btn mx-1'
              type='submit'
              onClick={props?.events?.saveContinue}
            >
              {props.saveAndContinueText}
            </Button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default CustomModal;
