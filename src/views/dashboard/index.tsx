/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment, useState } from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import { DashboardHeader } from '../../components/dashboardHeader';
import LeftSidenav from '../../components/dashboardSideNavbar';
import RecentSection from './recentSection';
import ProfessionModal from './professionModal';
import './dashboard.scss';

const Dashboard = () => {
  const [toggle, setToggle] = useState(false);
  const handleToggle = () => {
    setToggle(true);
  };
  const closeToggle = () => {
    setToggle(false);
  };
  return (
    <Fragment>
      <LeftSidenav visible={toggle} onClick={closeToggle} />
      <Container fluid={true} className='recent-section'>
        <Row>
          <Col>
            <DashboardHeader onClick={handleToggle} />
          </Col>
        </Row>
        <RecentSection />
      </Container>
      <ProfessionModal />
    </Fragment>
  );
};

export default Dashboard;
