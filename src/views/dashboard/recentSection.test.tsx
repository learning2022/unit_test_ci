/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import RecentSection from './recentSection';

const recentSection = mount(
  <Router>
    <RecentSection />
  </Router>,
);

describe('recent section component', () => {
  it('recent section as seprate row', () => {
    expect(recentSection.find('.row')).toHaveLength(2);
  });

  it('recent-section column existance', () => {
    expect(recentSection.find('.recent-section')).toHaveLength(0);
  });

  it('recent-section heading existance', () => {
    expect(recentSection.find('.recent-column-heading')).toHaveLength(1);
  });

  it('recent-section heading existance with Recent-text', () => {
    expect(recentSection.find('.recent-column-heading').text()).toEqual('Recent Pages');
  });

  it('recent-section heading existance with column icons class', () => {
    expect(recentSection.find('.recent-column-icons')).toHaveLength(1);
  });

  it('recent-section heading existance with search input', () => {
    expect(recentSection.find('input')).toHaveLength(1);
  });

  it('recent-section search existance with onchange prop', () => {
    const logSpy = jest.spyOn(console, 'log');
    expect(logSpy).toBeCalledWith;
    const searchInput = recentSection.find('input');
    searchInput.simulate('change', { target: { value: 'search' } });
    recentSection.update();
    expect(logSpy).toBeCalledWith;
    logSpy.mockRestore();
  });

  it('recent-section heading existance with card class', () => {
    expect(recentSection.find('.recent-section-cards')).toHaveLength(2);
  });

  it('check search-icon', () => {
    const handleSearch = jest.fn();
    recentSection.find({ 'data-test': 'searchbtn' }).at(0).simulate('click', handleSearch);
    expect(handleSearch.mock.calls.length).toBeCalledWith;
  });

  it('check filter-icon', () => {
    const onFilterBtnClick = jest.fn();
    recentSection.find({ 'data-test': 'filterbtn' }).at(0).simulate('click', onFilterBtnClick);
    expect(onFilterBtnClick.mock.calls.length).toBeCalledWith;
  });
});
