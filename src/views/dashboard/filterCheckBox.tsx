/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Form } from 'react-bootstrap';

interface CheckBoxInput {
  title: any;
  list: any;
  defaultValue: boolean;
  action: any;
}

const FilterCheckBox: React.FC<CheckBoxInput> = (prop: CheckBoxInput) => {
  return (
    prop.list && (
      <Form>
        <Form.Group>
          <Form.Label>{prop.title}</Form.Label>
          {prop.list.map((item: any, index: any) => (
            <Form.Check
              key={index}
              custom
              className='filter-custom-checkbox'
              type='checkbox'
              id={item.id}
              value={item.name}
              label={item.name}
              defaultChecked={prop.defaultValue}
              onChange={prop.action}
            />
          ))}
        </Form.Group>
      </Form>
    )
  );
};

export default FilterCheckBox;
