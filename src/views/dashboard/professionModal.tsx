/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Modal } from 'react-bootstrap';
import { ReactComponent as Speedometer } from '../../assets/Icons/dashboard.svg';

class ProfessionModal extends React.Component {
  state = {
    showModal: true,
  };
  handleProfessionChange = (e: any) => {
    e.target.style.borderColor = '#000d9c';
    this.setState({ showModal: false });
  };

  render() {
    return (
      <Modal
        animation={false}
        aria-labelledby='profession-modal'
        backdrop={'static'}
        centered
        show={this.state.showModal}
        className='profession-modal'
      >
        <Modal.Body className='profession-modal-form'>
          <h2 className='profession-header'>What will you be using Loree for?</h2>
          <h5> Well use this to recommend templates for you.</h5>
          <div className='row text-center'>
            <div className='col-4'>
              <div className='profession-list' onClick={e => this.handleProfessionChange(e)}>
                <Speedometer width={40} height={40} />
                <h5 className='mb-0 mt-2'>Teacher</h5>
                <p className='mb-0 mt-2'>You are here to enrich students learning experience</p>
              </div>
            </div>
            <div className='col-4'>
              <div className='profession-list' onClick={e => this.handleProfessionChange(e)}>
                <Speedometer width={40} height={40} />
                <h5 className='mb-0 mt-2'>Personal</h5>
                <p className='mb-0 mt-2'>You are here to let your creative juices flow</p>
              </div>
            </div>
            <div className='col-4'>
              <div className='profession-list' onClick={e => this.handleProfessionChange(e)}>
                <Speedometer width={40} height={40} />
                <h5 className='mb-0 mt-2'>Start-up</h5>
                <p className='mb-0 mt-2'>You are here to create or build your brand</p>
              </div>
            </div>
          </div>
          <div className='row text-center'>
            <div className='col-4'>
              <div className='profession-list' onClick={e => this.handleProfessionChange(e)}>
                <Speedometer width={40} height={40} />
                <h5 className='mb-0 mt-2'>Student</h5>
                <p className='mb-0 mt-2'>You are here to learn, create and impress</p>
              </div>
            </div>
            <div className='col-4'>
              <div className='profession-list' onClick={e => this.handleProfessionChange(e)}>
                <Speedometer width={40} height={40} />
                <h5 className='mb-0 mt-2'>Non-profit or Charity</h5>
                <p className='mb-0 mt-2'>You are here to build for the greater good</p>
              </div>
            </div>
            <div className='col-4'>
              <div className='profession-list' onClick={e => this.handleProfessionChange(e)}>
                <Speedometer width={40} height={40} />
                <h5 className='mb-0 mt-2'>Large Business</h5>
                <p className='mb-0 mt-2'>You are here to enrich students learning experience</p>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default ProfessionModal;
