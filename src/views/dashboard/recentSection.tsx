/* eslint-disable */ // Remove this line when editing this file
import React, { useState, useRef, useEffect } from 'react';
import { Col, Row, Button } from 'react-bootstrap';
import { API, graphqlOperation } from 'aws-amplify';
import { ReactComponent as SearchIcon } from '../../assets/Icons/search.svg';
import { ReactComponent as FilterIcon } from '../../assets/Icons/filter.svg';
import { listPages, listProjects, listCategorys } from '../../graphql/queries';
import Project from '../project/index';
import FilterCheckBox from './filterCheckBox';
import FilterDropDown from '../../components/customDropDown/filterDropDown';
import './dashboard.scss';

const RecentSection = () => {
  const initialSearch = '';
  let _timeoutID: any;
  const inputFocus = useRef<any>(null);
  const [search, setSearch] = useState(false);
  const [searchText, setSearchText] = useState<any>(initialSearch);
  const [filterVisibility, setFilterVisibility] = useState<string>('hide');
  const [typeDropdownValue, setTypeDropdownValue] = useState<string>('Pages');
  const [sortDropdownValue, setSortDropdownValue] = useState<string>('Recent');
  const [checked] = useState<boolean>(false);
  const [isFilterFocus, setIsFilterFocus] = useState<boolean>(false);
  const [filterValues, setFilterValues] = useState<any>();
  const [category, setCategory] = useState<any>('');
  const [checkboxValues, setCheckboxValues] = useState<any>([]);
  const [selectedList, setSelectedList] = useState<string>('Pages');

  useEffect(() => {
    inputFocus.current.focus();
  }, [search]);

  async function getFilterCheckbox() {
    const categoryList: any = await API.graphql(graphqlOperation(listCategorys));
    setCategory(categoryList.data.listCategorys.items);
  }

  useEffect(() => {
    getFilterCheckbox();
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(event.target.value);
  };

  const handleSearch = () => {
    setSearch(!search);
    if (search) {
      inputFocus.current.focus();
    }
  };

  const onFilterBtnClick = () => {
    const el: any = document.getElementById('global-filter-container');
    setTimeout(() => {
      if (!isFilterFocus) {
        setFilterVisibility('show');
        setSearch(false);
        el.focus();
      } else {
        setFilterVisibility('hide');
        setIsFilterFocus(false);
      }
    }, 0);
  };
  const _onFocus = () => {
    clearTimeout(_timeoutID);
    if (!isFilterFocus) {
      setIsFilterFocus(true);
    }
  };
  const _onBlur = () => {
    _timeoutID = setTimeout(() => {
      if (isFilterFocus) {
        setFilterVisibility('hide');
        setIsFilterFocus(false);
      }
    }, 150);
  };

  const onClickClear = () => {
    setTypeDropdownValue('Pages');
    setSortDropdownValue('Recent');
    document.querySelectorAll('.custom-checkbox>input[type=checkbox]').forEach((el: any) => (el.checked = false));
    setCheckboxValues([]);
  };

  const handleTypeSelect = (e: any) => {
    setTypeDropdownValue(e);
  };

  const handleSortSelect = (e: any) => {
    setSortDropdownValue(e);
  };

  const handleCheckboxSelect = (e: any) => {
    // current array of options
    const options: any = checkboxValues;
    let index;
    // check if the check box is checked or unchecked
    if (e.target.checked) {
      // add the id of the checkbox to options array
      options.push(e.target.id);
    } else {
      // or remove the id from the unchecked checkbox from the array
      index = options.indexOf(e.target.id);
      options.splice(index, 1);
    }
    // update the state with the new array of options
    setCheckboxValues(options);
  };

  //Filtering Values
  const sortComparision: any = (arg: any, arg2: any) => {
    const ProjectTitle1 = arg.title.toUpperCase();
    const ProjectTitle2 = arg2.title.toUpperCase();
    let data = 0;
    if (ProjectTitle1 > ProjectTitle2) {
      data = 1;
    } else if (ProjectTitle1 < ProjectTitle2) {
      data = -1;
    }
    return data;
  };

  const Sorting: any = async (FilterDataArray: any) => {
    if (sortDropdownValue === 'Ascending') {
      FilterDataArray.sort(sortComparision);
      setFilterValues(FilterDataArray);
    }
    if (sortDropdownValue === 'Descending') {
      const sorting = FilterDataArray.sort(sortComparision);
      const sortDescent = sorting.reverse();
      setFilterValues(sortDescent);
    }
    if (sortDropdownValue === 'Recent') {
      FilterDataArray.sort((arg: any, arg2: any) => {
        const dateOfArg: any = new Date(arg.createdAt);
        const dateOfArg2: any = new Date(arg2.createdAt);
        return dateOfArg2 - dateOfArg;
      });
      setFilterValues(FilterDataArray);
    }
  };

  const checkOptions = (data: any, allData: any) => {
    if (data.project) {
      if (checkboxValues.includes(data.project.categoryID)) {
        allData.push(data);
      }
    } else {
      if (checkboxValues.includes(data.categoryID)) {
        allData.push(data);
      }
    }
  };

  const checkboxFilter = (dataList: any) => {
    const allData: any = [];
    if (checkboxValues && checkboxValues.length) {
      dataList.map((data: any) => {
        checkOptions(data, allData);
        return allData;
      });
    } else {
      for (const list of dataList) {
        allData.push(list);
      }
    }
    return allData;
  };

  const FilterData: any = async () => {
    onFilterBtnClick();
    setSelectedList(typeDropdownValue);
    if (typeDropdownValue === 'Pages') {
      const page: any = await API.graphql(graphqlOperation(listPages, { limit: 100 }));
      const PageData = page.data.listPages.items;
      const filterResult: any = checkboxFilter(PageData);
      return Sorting(filterResult);
    }
    if (typeDropdownValue === 'Projects') {
      const Project: any = await API.graphql(graphqlOperation(listProjects));
      const ProjectData = Project.data.listProjects.items;
      const filterResult: any = checkboxFilter(ProjectData);
      return Sorting(filterResult);
    }
  };

  const typeDropdownList: any[] = [{ value: 'Projects' }, { value: 'Pages' }];
  const sortDropdownList: any[] = [{ value: 'Recent' }, { value: 'Ascending' }, { value: 'Descending' }];

  return (
    <>
      <Row className='mt-3 recent-style'>
        <Col className='recent-block'>
          <span className='recent-column-heading h5 font-weight-bold ml-3'>Recent {selectedList}</span>
          <div className='recent-column-icons d-inline-block float-right pr-3 position-relative'>
            <input
              id='search'
              name='search'
              value={searchText}
              className={!search ? 'd-none' : 'search'}
              type='text'
              autoComplete='off'
              placeholder='Search'
              onChange={handleChange}
              ref={inputFocus}
              autoFocus
            />
            <SearchIcon
              width={!search ? 24 : 15}
              height={!search ? 20 : 12}
              className={!search ? 'mx-2 search-icon-normal' : 'mx-2 search-icon iconActive'}
              data-test='searchbtn'
              onClick={handleSearch}
            />
            <FilterIcon
              width={24}
              height={24}
              className={isFilterFocus ? 'mx-2 filter-icon iconActive' : 'mx-2 filter-icon'}
              data-test='filterbtn'
              onClick={onFilterBtnClick}
            />
          </div>
        </Col>
      </Row>
      <div className='position-relative'>
        <div
          id='global-filter-container'
          tabIndex={0}
          onBlur={_onBlur}
          onFocus={_onFocus}
          className={`filter-container-layout ${filterVisibility}`}
        >
          <div className='d-flex flex-row'>
            <FilterDropDown
              title={typeDropdownValue}
              list={typeDropdownList}
              action={handleTypeSelect}
              dropdownID='type-dropdown'
            ></FilterDropDown>
            <FilterDropDown
              title={sortDropdownValue}
              list={sortDropdownList}
              action={handleSortSelect}
              dropdownID='sort-dropdown'
            ></FilterDropDown>
            <div className='ml-auto mr-0'>
              <Button variant='link' onClick={onClickClear}>
                Clear
              </Button>
            </div>
          </div>
          <div className='session-divider'></div>
          <div className='d-flex flex-row category-title'>
            <div>Choose respective categories</div>
          </div>
          <div className='d-flex flex-row'>
            <div className='d-flex'>
              <div className='category-divider'></div>
              <FilterCheckBox title='Category' list={category} defaultValue={checked} action={handleCheckboxSelect}></FilterCheckBox>
            </div>
          </div>
          <div className='d-flex flex-row'>
            <Button onClick={onFilterBtnClick} variant='outline-primary'>
              Cancel
            </Button>
            <Button variant='primary' onClick={FilterData}>
              Done
            </Button>
          </div>
        </div>
      </div>
      <Row className='mb-4'>
        <Col className='recent-section-cards'>
          <Project filterText={searchText} filterValue={filterValues} typeDropdown={typeDropdownValue} />
        </Col>
      </Row>
    </>
  );
};

export default RecentSection;
