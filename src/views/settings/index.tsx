import React, { useEffect, useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { Auth } from 'aws-amplify';
import './settings.scss';

export default function Settings() {
  const [user, setUsername] = useState('User');
  const [userEmail, setUserEmail] = useState('UserEmail');

  const username = async function () {
    await Auth.currentAuthenticatedUser().then((user) => {
      setUsername(user.attributes.name);
      setUserEmail(user.attributes.email);
    });
  };
  useEffect(() => {
    void username();
  }, []);
  return (
    <Row>
      <Col md={2} className='settings-nav'>
        <h3 className='text-primary font-weight-bold text-center mt-2 mb-4'>User Settings</h3>
        <NavLink to='/settings' className='mb-3'>
          <span className='font-weight-light pl-2'>Account Settings</span>
        </NavLink>
        <NavLink to='/security' className='mb-3'>
          <span className='font-weight-light pl-2'>Login &#38; Security </span>
        </NavLink>
        <NavLink to='/dashboard' className='mb-3'>
          <span className='font-weight-light pl-2'>Dashboard </span>
        </NavLink>
      </Col>
      <Col md={10} className='settings-main'>
        <h3 className='my-3 font-weight-bold'>Your account</h3>
        <Form>
          <Form.Label className='font-weight-bold my-2'>Name</Form.Label>
          <h6>{user}</h6>
          <Button variant='light' className='float-right mt-n5 mr-5'>
            Edit
          </Button>
          <hr className='ml-1 mr-4' />
          <Form.Label className='font-weight-bold my-2'>Email address</Form.Label>
          <h6>{userEmail}</h6>
          <hr className='ml-1 mr-4' />
        </Form>
      </Col>
    </Row>
  );
}
