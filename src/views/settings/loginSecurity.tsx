/* eslint-disable */ // Remove this line when editing this file
import React, { useEffect, useState } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { Auth } from 'aws-amplify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import './settings.scss';

interface ErrorKey {
  [key: string]: string;
}

export default function LoginSecurity() {
  const [currentUser, setCurrentUser] = useState() as any;
  const [currentPassword, setCurrentPassword] = useState('');
  const [verifiedUser, setVerifiedUser] = useState(false);
  const [password, setPassword] = useState('');
  const [cPassword, setCPassword] = useState('');
  const [apiRequest, setApiRequest] = useState(false);
  const [error, setError] = useState({} as ErrorKey);
  const [updated, setUpdated] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const userData = async function () {
    const user = await Auth.currentAuthenticatedUser();
    setCurrentUser(user);
  };
  useEffect(() => {
    userData();
    // eslint-disable-next-line
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setSubmitted(false);
    setError({} as ErrorKey);
    if (name === 'current-password') {
      setCurrentPassword(value);
    } else if (name === 'new-password') {
      setPassword(value);
    } else {
      setCPassword(value);
    }
  };

  function validation(values: any) {
    const errors: any = {};
    const PasswordMatch = 'Password does not match';
    const PasswordCriteriaExp = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&])(?=.{8,})');
    const PasswordEmptyField = 'Password must not be empty';
    const PasswordCriteria = 'Password doesnt match the following criteria';
    if (!values.password || !values.cPassword) {
      errors.password = PasswordEmptyField;
    } else if (!PasswordCriteriaExp.test(values.password) || !PasswordCriteriaExp.test(values.cPassword)) {
      errors.password = PasswordCriteria;
    }
    if (values.password !== values.cPassword) errors.password = PasswordMatch;
    return errors;
  }

  const handleSubmit = async () => {
    const userName = currentUser.attributes.email;
    if (currentPassword.length > 0) {
      setApiRequest(true);
      try {
        await Auth.signIn(userName, currentPassword);
        setVerifiedUser(true);
        setApiRequest(false);
      } catch (err) {
        console.log(err);
        setError({ currentPassword: 'Current password is incorrect' });
        setApiRequest(false);
      }
    } else {
      setError({ currentPassword: 'Field must not be empty' });
    }
    if (verifiedUser) {
      setError(validation({ password, cPassword }));
      setSubmitted(true);
    }
  };

  useEffect(() => {
    if (Object.keys(error).length === 0 && submitted) {
      updatePassword();
    }
    // eslint-disable-next-line
  }, [error, submitted]);

  const updatePassword = async () => {
    try {
      await Auth.changePassword(currentUser, currentPassword, password);
      setUpdated(true);
      setInterval(() => {
        setUpdated(false);
      }, 3000);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div>
      <Row>
        <Col md={2} className='settings-nav'>
          <h3 className='text-primary font-weight-bold text-center mt-2 mb-4'>User Settings</h3>
          <NavLink to='/settings' className='mb-3'>
            <span className='font-weight-light pl-2'>Account Settings</span>
          </NavLink>
          <NavLink to='/security' className='mb-3'>
            <span className='font-weight-light pl-2'>Login &#38; Security </span>
          </NavLink>
          <NavLink to='/dashboard' className='mb-3'>
            <span className='font-weight-light pl-2'>Dashboard </span>
          </NavLink>
        </Col>

        <Col md={10} className='settings-main'>
          <h3 className='mt-3'>Login Password</h3>
          <div className='password-reset-section'>
            <Form>
              {!verifiedUser && (
                <>
                  <Form.Group className='w-25 mb-0'>
                    <Form.Label>Current Password</Form.Label>
                    <Form.Control
                      type='password'
                      name='current-password'
                      placeholder='Enter Current Password'
                      autoComplete='new-password'
                      onChange={handleChange}
                    />
                  </Form.Group>
                  {error.currentPassword && (
                    <small className='text-danger'>
                      <span> {error.currentPassword} </span>
                    </small>
                  )}
                </>
              )}
              {verifiedUser && (
                <>
                  <Form.Group className='w-25 mb-0'>
                    <Form.Label>New Password</Form.Label>
                    <Form.Control
                      type='password'
                      name='new-password'
                      placeholder='Enter new Password'
                      autoComplete='new-password'
                      onChange={handleChange}
                    />
                  </Form.Group>

                  <Form.Group className='w-25 mb-0'>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                      type='password'
                      name='confirm-password'
                      placeholder='Confirm Password'
                      autoComplete='new-password'
                      onChange={handleChange}
                    />
                    {error.password && (
                      <small className='text-danger'>
                        <span> {error.password} </span>
                      </small>
                    )}
                  </Form.Group>
                </>
              )}
              <Button variant='primary' className='d-block my-2' type='button' onClick={handleSubmit}>
                {apiRequest ? 'Submitting...' : 'Submit'}
              </Button>
              {updated ? (
                <>
                  <FontAwesomeIcon icon={faCheckCircle} className='fa-1x text-success' />
                  <span className='text-success mt-2 ml-1'>Updated Successfully</span>
                </>
              ) : (
                ''
              )}
            </Form>
          </div>
        </Col>
      </Row>
    </div>
  );
}
