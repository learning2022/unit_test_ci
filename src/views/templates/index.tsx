/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment, useState } from 'react';
import { Container, Col, Row, Tabs, Tab, Form } from 'react-bootstrap';
import LeftSidenav from '../../components/dashboardSideNavbar';
import EllipsisFeature from '../project/ellipsis';
import ExportContent from '../project/export';
import { ReactComponent as SearchIcon } from '../../assets/Icons/search.svg';
import { ReactComponent as FilterIcon } from '../../assets/Icons/filter.svg';
import './templates.scss';

const Templates = () => {
  const [toggle, setToggle] = useState(false);
  const closeToggle = () => {
    setToggle(false);
  };
  const TemplateSearchFilter = () => {
    const tempSearchBarToggle = document.getElementById('templateSearch');
    tempSearchBarToggle?.classList.toggle('d-block');
    const tempSearchIconToggle = document.getElementById('tempSearchIcon');
    tempSearchIconToggle?.classList.toggle('search-bar-active');
  };
  return (
    <Fragment>
      <LeftSidenav visible={toggle} onClick={closeToggle} />
      <Container fluid={true} className='recent-section'>
        <Row>
          <Col>
            <div className='my-templates-wrapper'>
              <h2 className='mt-5 font-weight-bold'>My Templates</h2>
              <div className='templates-tab-section position-relative'>
                <Tabs defaultActiveKey='all' id='uncontrolled-tab-example'>
                  <Tab eventKey='all' title='All'>
                    <div className='my-templates-list d-flex flex-column'>
                      <div className='template-unit d-flex justify-content-between mb-3'>
                        <div className='d-flex align-items-center custom-form-elements'>
                          <div className='custom-checkbox-common'>
                            <input type='checkbox' id='cust-check-1' className='checkbox-unit'></input>
                            <label htmlFor='cust-check-1'></label>
                          </div>
                          <img src={require('../../assets/images/hero-bg.png')} className='img-fluid mx-4' alt='loree'></img>
                          <div className='my-template-name editor-tooltip'>
                            <p className='mb-0'>Testing</p>
                            <span className='tooltip'>Testing</span>
                          </div>
                        </div>
                        <div className='d-flex template-date-details align-items-center'>
                          <div className='d-flex flex-column'>
                            <label>Created Date</label>
                            <p className='mb-0'>24-May-2020</p>
                          </div>
                          <div className='d-flex flex-column ml-5'>
                            <label>Last Updated</label>
                            <p className='mb-0'>3-June-2020</p>
                          </div>
                        </div>
                        <div className='ellipsis-icon'>
                          <EllipsisFeature></EllipsisFeature>
                        </div>
                      </div>
                      <div className='template-unit d-flex justify-content-between mb-3'>
                        <div className='d-flex align-items-center custom-form-elements'>
                          <div className='custom-checkbox-common'>
                            <input type='checkbox' id='cust-check-2' className='checkbox-unit'></input>
                            <label htmlFor='cust-check-2'></label>
                          </div>
                          <img src={require('../../assets/images/hero-bg.png')} className='img-fluid mx-4' alt='loree'></img>
                          <div className='my-template-name editor-tooltip'>
                            <p className='mb-0'>This is a lengthy name for tooltip demonstration</p>
                            <span className='tooltip'>This is a lengthy name for tooltip demonstration</span>
                          </div>
                        </div>
                        <div className='d-flex template-date-details align-items-center'>
                          <div className='d-flex flex-column'>
                            <label>Created Date</label>
                            <p className='mb-0'>24-May-2020</p>
                          </div>
                          <div className='d-flex flex-column ml-5'>
                            <label>Last Updated</label>
                            <p className='mb-0'>3-June-2020</p>
                          </div>
                        </div>
                        <div className='ellipsis-icon'>
                          <EllipsisFeature></EllipsisFeature>
                        </div>
                      </div>
                    </div>
                  </Tab>
                  <Tab eventKey='pages' title='Pages'>
                    Pages
                  </Tab>
                  <Tab eventKey='custom rows' title='Custom Rows'>
                    Custom Rows
                  </Tab>
                  <Tab eventKey='custom elements' title='Custom Elements'>
                    Custom elements
                  </Tab>
                  <Tab eventKey='assets' title='Assets'>
                    assets
                  </Tab>
                </Tabs>
                <div className='position-absolute d-flex template-tools align-items-center'>
                  <div className='position-relative template-search-block d-flex align-items-center'>
                    <input
                      id='templateSearch'
                      name='template search'
                      className='d-none template-search-bar'
                      type='text'
                      autoComplete='off'
                      placeholder='Search'
                      // onChange={handleChange}
                      // ref={inputFocus}
                      autoFocus
                    />
                    <SearchIcon
                      id='tempSearchIcon'
                      className='template-search-icon position-absolute'
                      data-test='searchbtn'
                      onClick={TemplateSearchFilter}
                    />
                  </div>
                  <ExportContent data={'page'} type='page'></ExportContent>
                  <div>
                    <FilterIcon width={24} height={24} data-test='filterbtn' />
                  </div>
                  <Form.Group>
                    <Form.Control as='select'>
                      <option>Sort by</option>
                      <option>Name</option>
                      <option>Date</option>
                    </Form.Control>
                    <div className='arrow-down'></div>
                  </Form.Group>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default Templates;
