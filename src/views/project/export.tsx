/* eslint-disable */ // Remove this line when editing this file
import React, { Fragment, Component } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { getPage } from '../../graphql/queries';
import { Dropdown } from 'react-bootstrap';
import { ReactComponent as ExportIcon } from '../../assets/Icons/export.svg';

import { handleDownloadHTML, handleDownloadPDF, handleDownloadProjectHTML, handleDownloadProjectPDF } from '../../utils/export';
import './project.scss';

type info = {
  data: any;
  type: string;
};

class ExportContent extends Component<info> {
  state = {
    data: [],
    type: '',
  };

  async handleDownloadPageAsPDF(data: any) {
    if (data.match !== undefined) {
      return this.handleDownloadPdfWithContent(data);
    }
    handleDownloadPDF(data.id, data.title);
  }

  async handleDownloadPdfWithContent(data: any) {
    const { id } = data.match.params;
    const page: any = await API.graphql(graphqlOperation(getPage, { id: id }));
    const pageTitle = page.data.getPage.title;
    handleDownloadPDF(id, pageTitle);
  }

  async handleDownloadPageAsHTMLwithContent(data: any) {
    if (data.pageContent !== '') {
      const id = data.match.params.id;
      handleDownloadHTML(id);
    }
  }

  //export page as html
  async handleDownloadPageAsHTML(data: any) {
    if (data.match !== undefined) {
      return this.handleDownloadPageAsHTMLwithContent(data);
    } else {
      handleDownloadHTML(data.id);
    }
  }

  //Export Project as zip (type: HTML)
  async handleDownloadProjectAsHTML(data: any) {
    handleDownloadProjectHTML(data.id);
  }

  //Export Project as zip (type: PDF)
  async handleDownloadProjectAsPDF(data: any) {
    handleDownloadProjectPDF(data.id, data.title);
  }

  render() {
    return (
      <Fragment>
        <Dropdown drop='down' alignRight className='card-export-button' style={{ padding: '0px !important' }}>
          <Dropdown.Toggle id='dropdown-menu-align-right' style={{ background: 'white', borderColor: 'white' }}>
            <ExportIcon width={14} height={14} className='export-icon' />
          </Dropdown.Toggle>
          <Dropdown.Menu className='dropdown-content'>
            {this.props.data.project || this.props.type === 'page' ? (
              <>
                <Dropdown.Item onClick={() => this.handleDownloadPageAsPDF(this.props.data)}>PDF</Dropdown.Item>
                <Dropdown.Item onClick={() => this.handleDownloadPageAsHTML(this.props.data)}>HTML</Dropdown.Item>
              </>
            ) : (
              <>
                <Dropdown.Item onClick={() => this.handleDownloadProjectAsPDF(this.props.data)}>PDF</Dropdown.Item>
                <Dropdown.Item onClick={() => this.handleDownloadProjectAsHTML(this.props.data)}>HTML</Dropdown.Item>
              </>
            )}
          </Dropdown.Menu>
        </Dropdown>
      </Fragment>
    );
  }
}

export default ExportContent;
