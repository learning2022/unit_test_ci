/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import ReactDOM from 'react-dom';
import Project from './newProject';
import { shallow } from 'enzyme';

describe('renders home', () => {
  const historyMock = { push: jest.fn() };
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Project history={historyMock} />, div);
  });

  it('renders home without crashing', () => {
    shallow(<Project history={historyMock} />);
  });

  it('renders page title', () => {
    const wrapper = shallow(<Project history={historyMock} />);
    const welcome = <h1>Projects</h1>;
    expect(wrapper.contains(welcome)).toEqual(false);
  });

  it('renders props', () => {
    const wrapper = shallow(<Project history={historyMock} />);
    expect(wrapper.props().jsonpayload).toBeUndefined();
  });
});
