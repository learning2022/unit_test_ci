/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import ViewProject from './viewProject';

const match = {
  params: {
    id: 1,
  },
};

const viewProject = mount(
  <Router>
    <ViewProject match={match} />
  </Router>,
);

describe('view page component', () => {
  it('contain a p tag for a loading', () => {
    expect(viewProject.find('p')).toHaveLength(1);
  });
  it('contain a div tag for a loading', () => {
    expect(viewProject.find('div')).toHaveLength(12);
  });
  it('contain a anchor tag for a loading', () => {
    expect(viewProject.find('a')).toHaveLength(11);
  });
  it('contain a span tag for a loading', () => {
    expect(viewProject.find('span')).toHaveLength(8);
  });
  it('contain a div tag for a loading', () => {
    expect(viewProject.find('img')).toHaveLength(3);
  });
  it('contain a user-avatar-wrapper class for a loading', () => {
    expect(viewProject.find('.user-avatar-wrapper')).toHaveLength(1);
  });
  it('contain a nav tag for a loading', () => {
    expect(viewProject.find('nav')).toHaveLength(2);
  });
});
