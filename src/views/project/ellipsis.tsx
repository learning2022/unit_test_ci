import React, { Component } from 'react';
import { Dropdown } from 'react-bootstrap';
import { ReactComponent as EllipsisIcon } from '../../assets/Icons/ellipsis_normal.svg';
import './project.scss';

class EllipsisFeature extends Component {
  state = {
    contentId: '',
  };

  render() {
    return (
      <>
        <Dropdown
          drop='down'
          alignRight
          className='card-export-button'
          style={{ padding: '0px !important' }}
        >
          <Dropdown.Toggle id='dropdown-menu-align-left' className='noPadding'>
            <EllipsisIcon className='fal fa-ellipsis-v' width={3} height={14} />
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item>Rename</Dropdown.Item>
            <Dropdown.Item>Save As Template</Dropdown.Item>
            <Dropdown.Item>Delete</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </>
    );
  }
}

export default EllipsisFeature;
