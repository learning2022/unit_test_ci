/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import ReactDOM from 'react-dom';
import Project from './index';
import { shallow } from 'enzyme';

const testProp = {
  filterText: '',
  filterValue: 'page',
  typeDropdown: '',
};

describe('renders home', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Project {...testProp} />, div);
  });

  it('renders home without crashing', () => {
    shallow(<Project {...testProp} />);
  });

  it('renders page title', () => {
    const wrapper = shallow(<Project {...testProp} />);
    const welcome = <h1>Projects</h1>;
    expect(wrapper.contains(welcome)).toEqual(false);
  });

  it('renders props', () => {
    const wrapper = shallow(<Project {...testProp} />);
    expect(wrapper.props().jsonpayload).toBeUndefined();
  });
});
