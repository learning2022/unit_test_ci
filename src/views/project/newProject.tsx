/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Modal, Button, Nav, Form } from 'react-bootstrap';
import { ReactComponent as CreateProject } from '../../assets/Icons/create_project.svg';
import { API, graphqlOperation } from 'aws-amplify';
import * as queries from '../../graphql/queries';
import { createProject } from '../../graphql/mutations';
import './project.scss';
import Loading from '../../components/loader/loading';

type NewProjectState = {
  modalShow: boolean;
  value: string;
  categories: Array<string>;
  categoryValue: string;
  projectName: string;
  projectInput: Array<string>;
  creating: boolean;
  errors: any;
  loading: boolean;
  newProjectID: string;
  disableButton: boolean;
};

class NewProject extends React.Component<{ history: any }, NewProjectState> {
  constructor(props: any) {
    super(props);
    this.state = {
      modalShow: false,
      value: '',
      categoryValue: '',
      categories: [],
      projectName: '',
      projectInput: [],
      creating: false,
      errors: {},
      loading: true,
      newProjectID: '',
      disableButton: true,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCategory = this.handleCategory.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setModalShow(value: boolean) {
    this.setState({ modalShow: value, projectName: '', disableButton: true, categoryValue: '' });
  }

  async componentDidMount() {
    try {
      const categories: any = await API.graphql(graphqlOperation(queries.listCategorys));
      this.setState({
        categories: categories.data.listCategorys.items,
        loading: false,
      });
    } catch (err) {
      console.log('error fetching categories and clients...', err);
    }
  }

  handleChange(event: any) {
    this.setState({ projectName: event.target.value, errors: {} }, this.validateButton.bind(this));
  }

  handleCategory(event: any) {
    if (event.target.value !== '') {
      this.setState(
        {
          categoryValue: event.target.value,
          errors: {
            categoryError: '',
          },
        },
        this.validateButton.bind(this),
      );
    } else {
      this.setState(
        {
          categoryValue: '',
          errors: {
            categoryError: 'Please select Category',
          },
        },
        this.validateButton.bind(this),
      );
    }
  }

  async handleSubmit(event: any) {
    event.preventDefault();
    event.stopPropagation();
    this.setState({ creating: true });
    const projectInput = {
      title: this.state.projectName,
      categoryID: this.state.categoryValue,
    };
    try {
      const newProject: any = await API.graphql(graphqlOperation(createProject, { input: projectInput }));
      this.setState({ newProjectID: newProject.data.createProject.id });
      this.props.history.push(`/project/${this.state.newProjectID}`);
      // window.location.reload(false);
    } catch (err) {
      this.setState({
        errors: {
          categoryError: this.state.categoryValue === '' ? 'Please select Category' : '',
        },
        creating: false,
      });
      return;
    }
  }

  validateButton() {
    if (
      this.state.projectName.trim().length > 0 &&
      this.state.projectName.trim().length > 0 &&
      this.state.categoryValue.trim().length > 0
    ) {
      this.setState({ disableButton: false });
    } else {
      this.setState({ disableButton: true });
    }
  }

  render() {
    if (this.state.loading) return <Loading />;
    return (
      <React.Fragment>
        <Nav.Link
          onClick={() => this.setModalShow(true)}
          id='dashboard-create-project'
          className='mr-0 mr-md-2 mb-2 mb-md-0 py-1 px-3 py-sm-2 px-sm-5 text-nowrap'
        >
          <CreateProject width={30} height={24} className='iconActive mb-1 d-block mx-auto' />
          <span className='create-project-text'>Create Project</span>
        </Nav.Link>
        <Modal
          animation={false}
          aria-labelledby='new-project-model'
          backdrop={'static'}
          centered
          className='create-project-modal'
          onHide={() => this.setModalShow(false)}
          show={this.state.modalShow}
          size={'sm'}
        >
          <Modal.Header closeButton>
            <Modal.Title id='new-project-model'>
              <h5 className='text-primary'>New Project Details</h5>
            </Modal.Title>
          </Modal.Header>
          <Form onSubmit={this.handleSubmit}>
            <Modal.Body>
              <Form.Group controlId='formProjectName'>
                <Form.Label>Project Name</Form.Label>
                <Form.Control
                  required
                  name='projectName'
                  type='text'
                  placeholder='My Project'
                  value={this.state.projectName}
                  onChange={this.handleChange}
                  maxLength={100}
                />
              </Form.Group>
              <Form.Group controlId='formProjectCategory'>
                <Form.Label>Choose your category</Form.Label>
                <Form.Control as='select' onChange={this.handleCategory}>
                  <option value=''>----Select Category-------</option>
                  {this.state.categories.map((category: any) => (
                    <option value={category.id} key={category.id}>
                      {category.name}
                    </option>
                  ))}
                </Form.Control>
                {this.state.errors.categoryError && <small className='text-danger ml-2'>{this.state.errors.categoryError}</small>}
              </Form.Group>
            </Modal.Body>
            <Modal.Footer style={{ justifyContent: 'center' }}>
              <Button variant='primary' type='submit' disabled={this.state.disableButton}>
                {this.state.creating ? 'Creating...' : 'Create'}
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </React.Fragment>
    );
  }
}

export default NewProject;
