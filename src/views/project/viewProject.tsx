/* eslint-disable */ // Remove this line when editing this file
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { API, graphqlOperation } from 'aws-amplify';
import { getProject } from '../../graphql/queries';
import { deleteProject, deletePage } from '../../graphql/mutations';
import { Container, Button, Row, Col } from 'react-bootstrap';
import LeftSidenav from '../../components/dashboardSideNavbar/index';
import PagesList from '../page/listAllPage';
import { DashboardHeader } from '../../components/dashboardHeader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import Loading from '../../components/loader/loading';
import { updateProject } from '../../graphql/mutations';

const ViewProject = (props: any) => {
  const projectId = props.match.params.id;
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [btnText, setBtnText] = useState('Delete Project');
  const [project, setProject] = useState<any>('');
  const [toggle, setToggle] = useState(false);
  const [projectName, setProjectName] = useState<string>('');
  const [currentprojectName, setCurrentProjectName] = useState<string>('');
  const handleToggle = () => {
    setToggle(true);
  };
  const closeToggle = () => {
    setToggle(false);
  };

  async function apiData() {
    const data: any = await API.graphql(graphqlOperation(getProject, { id: projectId }));
    setProject([data.data.getProject]);
    setLoading(false);
    setProjectName(data.data.getProject.title);
    setCurrentProjectName(data.data.getProject.title);
  }

  useEffect(() => {
    apiData();
    // eslint-disable-next-line
  }, []);

  const handleProjectDelete = async (id: any) => {
    setBtnText('Deleting...');
    const data: any = await API.graphql(graphqlOperation(getProject, { id: projectId }));
    const PageData = data.data.getProject.pages.items;
    for (const PageId of PageData) {
      await API.graphql(graphqlOperation(deletePage, { input: { id: PageId.id } }));
    }
    await API.graphql(graphqlOperation(deleteProject, { input: { id: id } }));
    history.push('/dashboard');
  };

  const updateProjectTitle = async () => {
    if (projectName !== '') {
      setCurrentProjectName(projectName);
      const projectInput = {
        id: projectId,
        title: projectName,
      };
      await API.graphql(graphqlOperation(updateProject, { input: projectInput }));
    } else {
      setProjectName(currentprojectName);
    }
  };

  function handleNameChange(e: any) {
    setProjectName(e.target.value);
  }

  const _onBlur = () => {
    updateProjectTitle();
  };

  return (
    <>
      <LeftSidenav visible={toggle} onClick={closeToggle} />
      <Container fluid={true} className='recent-section'>
        <Row>
          <Col>
            <DashboardHeader onClick={handleToggle} />
          </Col>
        </Row>
        {loading && <Loading />}
        {!loading && (
          <div className='mt-3'>
            <div className='d-flex flex-row mb-4'>
              <div className='d-flex justify-content-center align-items-center flex-fill'>
                <input className='text-primary title-font' onBlur={_onBlur} value={projectName} onChange={handleNameChange} />
              </div>
              <div>
                <Button variant='danger' className='btn-sm float-right' onClick={() => handleProjectDelete(project[0].id)}>
                  <FontAwesomeIcon icon={faTrash} className='mr-2' />
                  {btnText}
                </Button>
              </div>
            </div>
            <Row>
              <Col className='mt-3'>
                <PagesList projectID={projectId} />
              </Col>
            </Row>
          </div>
        )}
      </Container>
    </>
  );
};

export default ViewProject;
