/* eslint-disable */ // Remove this line when editing this file
/* eslint-disable react/jsx-key */
import React, { Fragment, Component } from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import { Link } from 'react-router-dom';
import ExportContent from './export';
import EllipsisFeature from './ellipsis';
import { listPages } from '../../graphql/queries';
import { Card, Col, Row } from 'react-bootstrap';
import ProjectThumnail from '../../assets/images/Image_innerText.png';
import Loading from '../../components/loader/loading';
import './project.scss';

type ProjectState = {
  projects: any;
  loading: boolean;
  title: string;
  pageKey: string;
  emptyPage: boolean;
};

class Project extends Component<{ filterText: string; filterValue: any; typeDropdown: string }, ProjectState> {
  state = {
    title: '',
    pageKey: '',
    projects: [],
    loading: true,
    emptyPage: true,
  };

  async componentDidMount() {
    try {
      const projects: any = await API.graphql(graphqlOperation(listPages, { limit: 100 }));
      let items: any[] = [];
      if (projects.data.listPages.items.length > 0) {
        items = projects.data.listPages.items;
        items.sort((arg: any, arg2: any) => {
          const dateOfArg: any = new Date(arg.createdAt);
          const dateOfArg2: any = new Date(arg2.createdAt);
          return dateOfArg2 - dateOfArg;
        });
      }
      this.setState({
        projects: items,
        loading: false,
      });
    } catch (err) {
      console.log('error fetching categories...', err);
    }
  }
  render() {
    const filterContent = this.props.filterValue;
    const filterProj =
      this.props.filterText !== ''
        ? filterContent
          ? filterContent.filter((data: any) => {
              return data.title.toLowerCase().indexOf(this.props.filterText.toLowerCase().substr(0, 20)) !== -1;
            })
          : this.state.projects.filter((data: any) => {
              return data.title.toLowerCase().indexOf(this.props.filterText.toLowerCase().substr(0, 20)) !== -1;
            })
        : filterContent
        ? filterContent
        : this.state.projects;
    if (this.state.loading) return <Loading />;
    return (
      <Fragment>
        <Row>
          {filterProj.length > 0 ? (
            filterProj.map((data: any) => (
              <Col key={data.id} sm={6} md={4} lg={3} className='card-container'>
                <Card key={data.id} className='mt-3 px-5 px-sm-3 px-md-0 recent-card-list'>
                  <div className='d-flex flex-row justify-content-end'>
                    <div>
                      <ExportContent data={data} type='project'></ExportContent>
                    </div>
                    <div className='ellipsis-icon'>
                      <EllipsisFeature></EllipsisFeature>
                    </div>
                  </div>
                  <Link to={data.project ? `/loree-editor/${data.id}` : `/project/${data.id}`}>
                    <Card.Img className='recent-card-img' variant='top' src={ProjectThumnail} />
                  </Link>
                </Card>
                <div className='project-element'>
                  <Link to={data.project ? `/loree-editor/${data.id}` : `/project/${data.id}`}>
                    <div>
                      {data.title}
                      {data.project ? <> - {data.project.title} </> : ''}
                    </div>
                  </Link>
                </div>
              </Col>
            ))
          ) : (
            <h3 className='mx-auto mt-3'>No Results Found</h3>
          )}
        </Row>
      </Fragment>
    );
  }
}

export default Project;
