import React from 'react';
import ReactDOM from 'react-dom';
import Ellipsis from './ellipsis';
import { shallow } from 'enzyme';

const testProp = {
  type: '',
  data: 'project',
};

describe('renders ellipsis UI for project', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Ellipsis {...testProp} />, div);
  });

  it('renders ellipsis icon UI without crashing', () => {
    shallow(<Ellipsis {...testProp} />);
  });
});
