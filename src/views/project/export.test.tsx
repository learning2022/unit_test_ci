import React from 'react';
import ReactDOM from 'react-dom';
import Export from './export';
import { shallow } from 'enzyme';

const testProp = {
  type: '',
  data: 'project',
};

const testPropPage = {
  type: '',
  data: 'page',
};

describe('renders export UI for project', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Export {...testProp} />, div);
  });

  it('renders export UI without crashing', () => {
    shallow(<Export {...testProp} />);
  });
});

describe('renders export UI for page', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Export {...testPropPage} />, div);
  });

  it('renders export UI without crashing', () => {
    shallow(<Export {...testPropPage} />);
  });
});
