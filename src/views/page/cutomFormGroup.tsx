/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Form } from 'react-bootstrap';

interface FromGroupInput {
  id: string;
  section: string;
  placeHolder: string;
  required: string;
  options: Option[];
  selected: any;
}

interface Option {
  id: string;
  name: string;
}

const CustomFormGroup: React.FC<FromGroupInput> = (prop: FromGroupInput) => {
  return (
    <Form.Group controlId={prop.id}>
      <Form.Label>{prop.section}</Form.Label>
      {prop.required}
      <Form.Control as='select' onChange={prop.selected}>
        <option value={-1} key={-1}>
          {prop.placeHolder}
        </option>
        {prop.options.map((option: any, index: number) => (
          <option key={index} value={option.id}>
            {option.name}
          </option>
        ))}
      </Form.Control>
    </Form.Group>
  );
};

export default CustomFormGroup;
