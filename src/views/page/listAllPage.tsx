/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { ListGroup, Button, Container, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { API, graphqlOperation } from 'aws-amplify';
import { getProject } from '../../graphql/queries';
import { deletePage } from '../../graphql/mutations';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import Loading from '../../components/loader/loading';
import ExportContent from '../project/export';

type PROPS = {
  projectID: string;
};

type STATE = {
  pageList: any;
  id: any;
  emptyPage: boolean;
  pageContent: any;
};

class PagesList extends React.Component<PROPS, STATE> {
  state: STATE = {
    pageList: null,
    id: '',
    emptyPage: true,
    pageContent: [],
  };

  static propTypes = {
    projectID: PropTypes.string.isRequired,
  };

  componentDidMount() {
    this.fetchPages();
  }

  fetchPages = async () => {
    const projectId: string = this.props.projectID;
    const pageList: any = await API.graphql(graphqlOperation(getProject, { id: projectId }));
    this.setState({ pageList: pageList.data.getProject.pages.items });
  };

  deletePage = async (pageId: string, index: string) => {
    this.setState({ id: pageId });
    const deleteInput = {
      id: pageId,
    };
    await API.graphql(graphqlOperation(deletePage, { input: deleteInput }));
    this.setState({
      pageList: [...this.state.pageList.slice(0, index), ...this.state.pageList.slice(index + 1)],
    });
  };

  render() {
    if (!this.state.pageList) {
      return <Loading />;
    }

    if (this.state.pageList.length > 0) {
      return (
        <Container>
          <ListGroup className={'mt-3'}>
            {this.state.pageList.map((page: any, index: string) => (
              <ListGroup.Item key={index} className='d-flex align-items-center justify-content-between'>
                <Row>
                  <Link to={`/loree-editor/${page.id}`}>{page.title}</Link>
                </Row>
                <Row>
                  <Col>
                    <ExportContent data={page} type='page'></ExportContent>
                  </Col>
                  <Col>
                    <Button variant='danger' type='submit' onClick={() => this.deletePage(page.id, index)}>
                      <FontAwesomeIcon icon={faTrash} />
                    </Button>
                  </Col>
                </Row>
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Container>
      );
    }

    return (
      <ListGroup className={'mt-3'}>
        <ListGroup.Item>No pages added to this project</ListGroup.Item>
      </ListGroup>
    );
  }
}

export default PagesList;
