/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Modal, Nav, Image, Form, Container, Row, Button } from 'react-bootstrap';
import BlankPage from '../../assets/Icons/blank_page_highlighted.svg';
import templates from '../../assets/Icons/global_templates_highlighted.svg';
import { API, graphqlOperation } from 'aws-amplify';
import { listProjects, getProject } from '../../graphql/queries';
import { createPage } from '../../graphql/mutations';
import CustomFormGroup from './cutomFormGroup';
import { ReactComponent as CreatePageIcon } from '../../assets/Icons/create_page.svg';
import './buildPage.scss';

type BuildPageInput = {
  modalShow: boolean;
  pageModalShow: boolean;
  page: string;
  selectedProject: string;
  selectedCategory: string;
  projects: any;
  creating: boolean;
  errors: any;
  disableButton: boolean;
};

// eslint-disable-next-line @typescript-eslint/ban-types
class BuildPage extends React.Component<{}, BuildPageInput> {
  constructor(props: any) {
    super(props);
    this.state = {
      modalShow: false,
      pageModalShow: false,
      page: '',
      selectedProject: '',
      selectedCategory: '',
      projects: [],
      creating: false,
      errors: '',
      disableButton: true,
    };

    this.pageHandler = this.pageHandler.bind(this);
    this.projectHandler = this.projectHandler.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setModalShow(value: boolean) {
    this.setState({ modalShow: value, disableButton: true });
  }

  setPageModalShow(value: boolean) {
    this.setState({ pageModalShow: value });
  }

  changeModalVisibility() {
    this.setPageModalShow(true);
    this.setModalShow(false);
    this.setState({
      page: '',
      selectedCategory: '',
    });
  }

  async componentDidMount() {
    let projectList: any = [];
    projectList = await API.graphql(graphqlOperation(listProjects));
    const project: any = [];
    projectList.data.listProjects.items.map((item: any) => project.push({ id: item.id, name: item.title }));
    this.setState({ projects: project });
  }

  pageHandler(event: any) {
    this.setState({ page: event.target.value }, this.validateButton.bind(this));
  }

  projectHandler(event: any) {
    const projectValue = event.target.value;
    if (!this.selectPlaceholder(event)) {
      this.setState({ selectedProject: projectValue }, this.validateButton.bind(this));
      this.fetchProjectDetails(projectValue);
    } else {
      this.setState({
        selectedCategory: '',
      });
    }
  }

  fetchProjectDetails = async (id: any) => {
    const projectDetail: any = await API.graphql(graphqlOperation(getProject, { id: id }));
    this.setState({
      selectedCategory: projectDetail.data.getProject.category.name,
    });
  };

  selectPlaceholder(event: any) {
    if (event.target.value === '-1') {
      this.setState({ selectedProject: '' }, this.validateButton.bind(this));
      return true;
    }
    return false;
  }

  async handleSubmit(event: any) {
    this.setState({ disableButton: true });
    this.setState({ creating: true });
    this.setModalShow(false);
    event.preventDefault();
    event.stopPropagation();
    const pageInput = {
      title: this.state.page,
      projectID: this.state.selectedProject,
    };
    try {
      await API.graphql(graphqlOperation(createPage, { input: pageInput }));
    } catch (err) {
      this.setState({ errors: err });
    }
    window.location.href = `/project/${this.state.selectedProject}`;
  }

  validateButton() {
    if (this.state.selectedProject.trim().length > 0 && this.state.page.trim().length > 0) {
      this.setState({ disableButton: false });
    } else {
      this.setState({ disableButton: true });
    }
  }

  render() {
    return (
      <React.Fragment>
        <Nav.Link
          onClick={() => this.setModalShow(true)}
          id='dashboard-create-page'
          className='mr-0 mr-md-2 mb-2 mb-md-0 py-1 px-3 py-sm-2 px-sm-5 text-nowrap'
        >
          <CreatePageIcon width={30} height={24} className='iconActive mb-1 d-block mx-auto' />

          <span>Create Page</span>
        </Nav.Link>

        <Modal
          animation={false}
          show={this.state.modalShow}
          onHide={() => this.setModalShow(false)}
          aria-labelledby='page-type-model'
          centered
        >
          <Modal.Body className='margin40 noPadding'>
            <Container className='noPadding'>
              <Row className='show-grid'>
                <div className='col-md-6'>
                  <div className='col-md-12 outlineImage'>
                    <Image src={BlankPage} width={30} height={24} style={{ margin: '10%' }} className='mb-1 icon-hover' />
                    <div className='leftBlockHeader'>
                      <b>Build</b>
                      <br />
                      <b>From Scratch</b>
                    </div>
                    <div className='leftBlockDescription'>This opens new page from scratch and user will be creating components</div>

                    <Button
                      className='leftBlockButton'
                      variant='primary'
                      type='submit'
                      onClick={() => this.changeModalVisibility()}
                      data-test='createbtn'
                    >
                      Create
                    </Button>
                  </div>
                </div>
                <div className='col-md-6'>
                  <div className='col-md-12 rightBlock'>
                    <Image src={templates} style={{ margin: '10%' }} width={30} height={24} className='mb-1 icon-hover' />
                    <div className='rightBlockHeader'>
                      <b>Build</b>
                      <br />
                      <b>From Template</b>
                    </div>
                    <div className='rightBlockDescription'>
                      This opens a page with glocal templates and templates saved by user. Still you can edit and create components
                    </div>

                    <Button className='rightBlockButton' variant='primary' type='submit'>
                      Explore And Create
                    </Button>
                  </div>
                </div>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
        <Modal
          animation={false}
          aria-labelledby='new-page-model'
          backdrop={'static'}
          centered
          onHide={() => this.setPageModalShow(false)}
          show={this.state.pageModalShow}
          size={'sm'}
        >
          <Modal.Header closeButton>
            <Modal.Title id='new-page-model'>
              <h5 className='text-primary'>Page Details</h5>
            </Modal.Title>
          </Modal.Header>
          <Form>
            <Modal.Body className='create-page-form'>
              <Form.Group controlId='formPageName'>
                <CustomFormGroup
                  id='formPageProject'
                  section='Project'
                  required=' *'
                  placeHolder=' --- Choose your project --- '
                  options={this.state.projects}
                  selected={this.projectHandler}
                ></CustomFormGroup>
                <Form.Label>Page Name *</Form.Label>
                <Form.Control
                  required
                  data-test='pageName'
                  type='text'
                  placeholder=' Page name'
                  value={this.state.page}
                  onChange={this.pageHandler}
                  maxLength={100}
                />
              </Form.Group>
              <Form.Group controlId='formPageCategory'>
                <Form.Label>Category</Form.Label>
                <Form.Control required type='text' value={this.state.selectedCategory} maxLength={100} readOnly />
              </Form.Group>
            </Modal.Body>

            <Modal.Footer style={{ justifyContent: 'center' }}>
              <Button variant='primary' type='submit' onClick={this.handleSubmit} disabled={this.state.disableButton}>
                {this.state.creating ? 'Creating...' : 'Create'}
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </React.Fragment>
    );
  }
}

export default BuildPage;
