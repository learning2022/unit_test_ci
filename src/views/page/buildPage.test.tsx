/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import BuildPage from './buildPage';

const CreatePage = mount(
  <Router>
    <BuildPage />
  </Router>,
);

describe('renders BuildPage', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<BuildPage />, div);
  });

  it('whether the button is createPage', () => {
    expect(CreatePage.find('a')).toHaveLength(1);
    expect(CreatePage.find('a').html()).toContain('svg');
  });

  test('whether the button is createPage contain span tag', () => {
    expect(CreatePage.find('a')).toHaveLength(1);
    expect(CreatePage.find('a').html()).toContain('span');
    const CreatePageText = CreatePage.find('span').html();
    expect(CreatePageText).toEqual('<span>Create Page</span>');
  });

  it('modol pop up opened', () => {
    const handleShow = jest.fn();
    CreatePage.find('a').simulate('click', handleShow);
    expect(handleShow.mock.calls.length).toBeCalledWith;
  });

  it('bootstrap modal classes existance', () => {
    expect(CreatePage.find('.modal-content')).toHaveLength(1);
    expect(CreatePage.find('.modal-header')).toHaveLength(0);
    expect(CreatePage.find('.modal-body')).toHaveLength(1);
    expect(CreatePage.find('.modal-footer')).toHaveLength(0);
  });

  it('Checking tags in Modal', () => {
    expect(CreatePage.find('Image')).toHaveLength(2);
    expect(CreatePage.find('h4')).toHaveLength(0);
    expect(CreatePage.find('p')).toHaveLength(0);
    expect(CreatePage.find('a')).toHaveLength(1);
    expect(CreatePage.find('Button')).toHaveLength(2);
  });

  it('Checking h4 tag text Build', () => {
    const BuildPageText = CreatePage.find('b').at(0).html();
    expect(BuildPageText).toEqual('<b>Build</b>');
  });

  it('Checking h4 tag text From Scratch', () => {
    const BuildPageText = CreatePage.find('b').at(1).html();
    expect(BuildPageText).toEqual('<b>From Scratch</b>');
  });

  it('Checking h4 tag text Build', () => {
    const BuildPageText = CreatePage.find('b').at(2).html();
    expect(BuildPageText).toEqual('<b>Build</b>');
  });

  it('Checking Button Content', () => {
    const BuildFromScratch = CreatePage.find('Button').at(0).html();
    const BuildFromTemplate = CreatePage.find('Button').at(1).html();
    expect(BuildFromScratch).toEqual('<button data-test="createbtn" type="submit" class="leftBlockButton btn btn-primary">Create</button>');
    expect(BuildFromTemplate).toEqual('<button type="submit" class="rightBlockButton btn btn-primary">Explore And Create</button>');
  });

  it('Create button functionality', () => {
    const handleSubmit = jest.fn();
    CreatePage.find('Button').at(0).simulate('click', handleSubmit);
    expect(handleSubmit.mock).toBeCalled;
  });

  it('project handler function', () => {
    const projectHandler = jest.fn();
    CreatePage.find('#formPageProject').at(1).simulate('change', projectHandler);
    expect(projectHandler.mock).toBeCalled;
  });

  it('page name handler function', () => {
    const pageHandler = jest.fn();
    CreatePage.find({ 'data-test': 'pageName' }).at(0).simulate('change', pageHandler);
    expect(pageHandler.mock).toBeCalled;
  });

  it('create btn handler function', () => {
    const changeModalVisibility = jest.fn();
    CreatePage.find('Button').at(0).simulate('click', changeModalVisibility);
    expect(changeModalVisibility.mock).toBeCalled;
  });
});
