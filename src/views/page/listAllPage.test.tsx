/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import ReactDOM from 'react-dom';
import ListAllPage from './listAllPage';
import { BrowserRouter as Router } from 'react-router-dom';
import { shallow, mount } from 'enzyme';

const testProp = {
  projectID: '1',
};

const listAllPage = mount(
  <Router>
    <ListAllPage {...testProp} />
  </Router>,
);

describe('renders list all pages UI for project', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ListAllPage {...testProp} />, div);
  });
  it('renders list all pages UI without crashing', () => {
    shallow(<ListAllPage {...testProp} />);
  });
  it('renders list all pages UI svg tag', () => {
    expect(listAllPage.find('svg')).toHaveLength(1);
  });
  it('renders list all pages UI icon', () => {
    expect(listAllPage.find('.fa-spin')).toHaveLength(2);
  });
});
