import React, { useState, useEffect, useLayoutEffect, Suspense } from 'react';
import ReactDOM from 'react-dom';

import { useA11yContext, A11yChecker } from '../../a11y-service';
import CONSTANTS from '../../loree-editor/constant';

import s from './A11yCheckerContainer.module.scss';
import STYLE_CONSTANTS from '../../loree-editor/styleConstant';

export function A11yCheckerContainer() {
  const { setContext, activeStep, isProcessing, setHighlightsOffset } = useA11yContext();
  const [portalRoot, setPortalRoot] = useState<Element>();

  const showChecker = activeStep >= 0 || isProcessing;

  useEffect(() => {
    setContext({
      include: [[`#${CONSTANTS.LOREE_IFRAME}`, `#${CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER}`]],
    });
  }, [setContext]);

  useLayoutEffect(() => {
    const newPortalRoot = document.getElementById(CONSTANTS.LOREE_SPACE_A11Y_CHECKER);
    if (newPortalRoot) {
      setHighlightsOffset([0, STYLE_CONSTANTS.A11Y_CHECKER_OFFSET_Y]);
      setPortalRoot(newPortalRoot);
    }
  }, [portalRoot, setHighlightsOffset]);

  if (!portalRoot) {
    return null;
  }

  return ReactDOM.createPortal(
    <div className={s.container}>
      {showChecker && (
        <Suspense fallback='loading...'>
          <div id={CONSTANTS.LOREE_MAIN_A11YCHECKER} className={s.a11yChecker}>
            <A11yChecker />
          </div>
        </Suspense>
      )}
    </div>,
    portalRoot,
  );
}
