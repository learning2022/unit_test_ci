import 'bootstrap';
import Loree from '../../loree-editor/loree';
import '../../a11y-service/theme.scss';
import '../../loree-editor/stylesheets/loree.scss';
import './style.scss';
import '../../components/loader/loading.scss';
import { init } from '../../loree-editor/a11y-client/init';

init();

let loree: Loree;

export function getLoree(): Loree {
  return loree;
}

export function setLoree(l: Loree) {
  loree = l;
}
