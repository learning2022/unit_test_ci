import React from 'react';
import 'bootstrap';
import { Container } from 'react-bootstrap';
import '../../a11y-service/theme.scss';
import '../../loree-editor/stylesheets/loree.scss';
import './style.scss';
import { API, graphqlOperation } from 'aws-amplify';
import {
  courseDetails,
  getPage,
  listCustomStyles,
  listLoreeFeatures,
  viewD2lContents,
  viewBbPageContentsById,
  courseEnrollment,
} from '../../graphql/queries';
import Loading from '../../components/loader/loading';
import '../../components/loader/loading.scss';
import {
  viewCanvasPage,
  viewCanvasDiscussion,
  viewCanvasAssignment,
} from '../../graphql/mutations';
import { setPageDetails, setD2lProps } from '../../utils/saveContent';
import { LmsAccess } from '../../utils/lmsAccess';
import CONSTANTS from '../../loree-editor/constant';
import { ultraContentFetch } from '../../lti/blackBoard/landingPage/bbUltra';
import juice from 'juice';
import { A11yContextProvider } from '../../a11y-service';
import { quickFixer } from '../../loree-editor/a11y-client/quick-fixer';
import {
  GetPageQuery,
  ListLoreeFeaturesQuery,
  ListCustomStylesQuery,
  CourseDetailsQuery,
  CourseEnrollmentQuery,
  ViewCanvasPageMutation,
  ViewCanvasAssignmentMutation,
  ViewCanvasDiscussionMutation,
  ViewD2lContentsQuery,
  ViewBbPageContentsByIdQuery,
} from '../../API';
import { convertIntoDoubleDigit, getEditorElementById } from '../../loree-editor/utils';
import { a11yNotificationService, A11yEventType, A11yStatusEvent } from './observerService';
import { listLMSLoreeRoles } from '../../lti/admin/rolesAndFeature/rolesAndFeaturesActions';
import { translate } from '../../i18n/translate';
import LoreeEditor from './LoreeEditor';
import { getLoree } from '.';

interface EditorProps {
  match: {
    path: string;
    params: {
      id: string;
      type: string;
      title: string;
    };
  };
}

export class LoreeEditorInitiate extends React.Component<EditorProps> {
  state = {
    loading: true,
    pageTitle: '',
    projectTitle: '',
    customColor: [{ color: '' }],
    customFont: [],
    customHeader: '',
    customLink: '',
    featuresList: '',
    d2lModulesContent: '',
    headTag: '',
    bbContentType: {},
  };

  componentDidMount(): void {
    void this.getTitleAndContent();

    // Added _Any type because the message inside subscribe() throws error because of 'complete()' error.
    a11yNotificationService.createObserver()?.subscribe((event: _Any) => {
      const message = event as A11yStatusEvent;
      const a11yNotification = getEditorElementById(CONSTANTS.LOREE_A11Y_NOTIFICATION_COUNT);
      const a11yButton = getEditorElementById(CONSTANTS.LOREE_HEADER_ACCESSIBILITY_BUTTON);

      if (!a11yNotification || !message) {
        return;
      }
      a11yNotification.innerHTML = convertIntoDoubleDigit(message.a11yIssueCount);
      a11yButton?.removeAttribute('disabled');
      if (message.eventType === A11yEventType.close) {
        a11yNotification.style.display = 'block';
      } else {
        a11yNotification.style.display = 'none';
      }
    });
  }

  // fetching page details and retrieve its content
  getTitleAndContent = async () => {
    const id = this.props.match.params.id;
    const moduleType = this.props.match.params.type;
    const contentTitle = unescape(this.props.match.params.title);
    const editorCheck = this.props.match.path.split('/');
    if (editorCheck[1] === 'loree-editor') {
      const pageDetails = await API.graphql<GetPageQuery>(graphqlOperation(getPage, { id: id }));
      const pageContent = pageDetails.data?.getPage?.content;
      const listFeatures = await this.featureListForStandalone();
      await this.customStyleForStandalone();
      this.setState({
        pageTitle: pageDetails.data?.getPage?.title,
        projectTitle: pageDetails.data?.getPage?.project?.title,
        loading: false,
        featuresList: listFeatures,
      });
      setPageDetails('loree-editor', id);
      getLoree().setHtml(pageContent);
    } else {
      try {
        const listCustomStyle = await API.graphql<ListCustomStylesQuery>(
          graphqlOperation(listCustomStyles, {
            filter: {
              ltiPlatformID: {
                eq: sessionStorage.getItem('ltiPlatformId'),
              },
            },
          }),
        );
        const lms = new LmsAccess();
        this.setCustomStyleList(listCustomStyle);
        const featuresList =
          sessionStorage.getItem('domainName') === 'canvas' && lms.getAccess()
            ? await this.selectedFeaturesList()
            : sessionStorage.getItem('domainName') === 'D2l'
            ? await this.selectedFeaturesList()
            : await this.selectedFeaturesList();
        this.setState({
          featuresList: featuresList,
        });
      } catch (error) {
        this.setState({
          customColor: [{ color: '' }],
          customHeader: '',
          customFont: [],
          customLink: '',
        });
        console.log('error in fetching customStyle', error);
      }
      const pageContent = await this.retriveLmsContent(id, moduleType);
      this.setState({
        pageTitle: contentTitle,
        projectTitle: '',
        loading: false,
      });
      if (sessionStorage.getItem('domainName') === 'canvas') {
        setPageDetails('canvaslti', id);
        getLoree().setHtml(
          moduleType === 'page'
            ? pageContent.body
            : moduleType === 'assignment'
            ? pageContent.description
            : moduleType === 'discussion'
            ? pageContent.message
            : '',
        );
      } else if (sessionStorage.getItem('domainName') === 'D2l') {
        setPageDetails('D2L', id);
        if (moduleType === 'modules') {
          this.setState({ d2lModulesContent: pageContent });
        }
        getLoree().setHtml(
          moduleType === 'modules' ? pageContent.Description.Html : this.splitContent(pageContent),
        );
        setD2lProps(moduleType, this.state.d2lModulesContent, this.state.headTag);
      } else if (sessionStorage.getItem('domainName') === 'BB') {
        this.setState({ bbContentType: pageContent.contentHandler });
        setPageDetails('bbLti', id, contentTitle, pageContent.contentHandler);
        if (sessionStorage.getItem('BBCourseStatus') === 'Ultra') {
          const ultraContent = pageContent.body;
          const contentUrl = ultraContent.substring(
            ultraContent.lastIndexOf('href="') + 6,
            ultraContent.lastIndexOf('"'),
          );
          const htmlBody = await ultraContentFetch(contentUrl);
          getLoree().setHtml(htmlBody);
        } else {
          getLoree().setHtml(pageContent.body);
        }
      }
    }
  };

  appendLoreeCss = (headTag: HTMLHeadElement) => {
    const loreeCss = `<link rel="stylesheet" media="all" href=${CONSTANTS.LOREE_CSS_URL}>`;
    const textHtml = new DOMParser().parseFromString(loreeCss, 'text/html');
    const linkTags = textHtml.getElementsByTagName('link');
    Array.from(linkTags).forEach((link) => {
      headTag.appendChild(link);
    });
  };

  appendLoreeJs = (headTag: HTMLHeadElement) => {
    const loreeJs = `<script src=${CONSTANTS.LOREE_BRIGHTSPACE_JS_URL}></script>`;
    const textHtml = new DOMParser().parseFromString(loreeJs, 'text/html');
    const scriptTags = textHtml.getElementsByTagName('script');
    Array.from(scriptTags).forEach((script) => {
      headTag.appendChild(script);
    });
  };

  splitContent = (content: string) => {
    let styleProperties = '';
    let headContent = '';
    let loreeCssNotExists = true;
    let loreeJsNotExists = true;
    const stylePattern = /<style[^>]*>((.|[\n\r])*)<\/style>/im;
    const styleSheet = stylePattern.exec(content);
    if (styleSheet?.[1]) styleProperties = styleSheet[1];
    const textHtml = new DOMParser().parseFromString(content, 'text/html');
    const headTag = textHtml.getElementsByTagName('head')[0];
    const bodyTag = textHtml.getElementsByTagName('body')[0];
    const loreeHeadTag = document.getElementsByTagName('head')[0];
    const linkTags = textHtml.getElementsByTagName('link');
    Array.from(linkTags).forEach((link) => {
      if (link.href === CONSTANTS.LOREE_CSS_URL) loreeCssNotExists = false;
    });
    if (loreeCssNotExists) this.appendLoreeCss(headTag);
    const scriptTags = textHtml.getElementsByTagName('script');
    Array.from(scriptTags).forEach((script) => {
      if (script.src === CONSTANTS.LOREE_BRIGHTSPACE_JS_URL) loreeJsNotExists = false;
      const clonedScript = script.cloneNode(true);
      loreeHeadTag.appendChild(clonedScript);
    });
    if (loreeJsNotExists) this.appendLoreeJs(headTag);
    headContent = headTag.outerHTML;
    this.setState({ headTag: headContent });
    const result = juice(`<style>${styleProperties}</style>${bodyTag.innerHTML}`);
    const pageContent = result || content;
    return pageContent;
  };

  // further we will update the feature list management for standalone
  featureListForStandalone = async () => {
    const features = await API.graphql<ListLoreeFeaturesQuery>(
      graphqlOperation(listLoreeFeatures, {
        filter: {
          ltiPlatformID: {
            eq: 'STANDALONE',
          },
        },
      }),
    );
    const listAllFeatures: Record<string, boolean> = {};

    const featureListJson = features?.data?.listLoreeFeatures?.items?.[0]?.featureList ?? 'null';
    for (const category of JSON.parse(featureListJson)) {
      const allFeatures = category.featureList;
      for (const feature of allFeatures) {
        const featureName = feature.id;
        listAllFeatures[featureName] = true;
      }
    }
    return listAllFeatures;
  };

  customStyleForStandalone = async () => {
    const styles = await API.graphql<ListCustomStylesQuery>(
      graphqlOperation(listCustomStyles, {
        filter: {
          ltiPlatformID: {
            eq: 'STANDALONE',
          },
        },
      }),
    );
    this.setCustomStyleList(styles);
  };

  // fetching custom styles based on lms/standalone
  setCustomStyleList = (listCustomStyle: { data?: ListCustomStylesQuery }): void => {
    const styleItem = listCustomStyle.data?.listCustomStyles?.items?.[0];
    const customHeader = JSON.parse(styleItem?.customHeader ?? 'null');
    const customFont = JSON.parse(styleItem?.customFont ?? 'null');
    const customLink = JSON.parse(styleItem?.customLink ?? 'null');
    const customColor = JSON.parse(styleItem?.customColor ?? 'null');
    this.setState({
      customColor: customColor,
      customHeader: customHeader,
      customFont: customFont,
      customLink: customLink,
    });
  };

  // get features list
  selectedFeaturesList = async () => {
    try {
      let roleBaseFeatureList = false;
      let courseEnrollmentID = null;
      let courseContentLength = 0;
      // features list for specific user
      if (sessionStorage.getItem('domainName') === 'canvas') {
        roleBaseFeatureList = true;
        const courses = await API.graphql<CourseDetailsQuery>(
          graphqlOperation(courseDetails, {
            courseId: sessionStorage.getItem('course_id'),
          }),
        );
        const courseData = JSON.parse(courses.data?.courseDetails ?? 'null');
        courseEnrollmentID = courseData.body.enrollments[0]?.role_id;
        courseContentLength = courseData.body.enrollments.length;
      } else if (sessionStorage.getItem('domainName') === 'BB') {
        roleBaseFeatureList = true;
        const courseRoleDetails = await API.graphql<CourseEnrollmentQuery>(
          graphqlOperation(courseEnrollment, {
            courseId: sessionStorage.getItem('course_id'),
          }),
        );
        const courseRole = JSON.parse(courseRoleDetails.data?.courseEnrollment ?? 'null');
        courseEnrollmentID = courseRole.body.courseRoleId;
        courseContentLength = courseEnrollmentID ? 1 : 0;
      }
      // retrieves lms roles
      const lmsRoleRetrieve = await listLMSLoreeRoles();

      // retrieves all features
      const features = await API.graphql<ListLoreeFeaturesQuery>(
        graphqlOperation(listLoreeFeatures, {
          filter: {
            ltiPlatformID: {
              eq: sessionStorage.getItem('ltiPlatformId'),
            },
          },
        }),
      );
      const selectedFeatures: string[] = [];
      const listAllFeatures: Record<string, boolean> = {};
      if (roleBaseFeatureList) {
        if (courseContentLength === 0) {
          // for non-enrollment courses
          for (const featureCategory of features.data?.listLoreeFeatures?.items ?? []) {
            const allFeatureList = JSON.parse(featureCategory?.featureList ?? '[]');
            for (const category of allFeatureList) {
              const allFeatures = category.featureList;
              for (const feature of allFeatures) {
                const featureName = feature.id;
                listAllFeatures[featureName] = true;
              }
            }
          }
          return listAllFeatures;
        } else {
          const roleTypeId = courseEnrollmentID;
          // gets user specific role features
          const allRoles = lmsRoleRetrieve ?? [];
          allRoles.map((featureCategory) => {
            const loreeRoles = featureCategory?.loreeRoleID;
            const featuresList = loreeRoles?.featureList;
            const lmsRoleID =
              sessionStorage.getItem('domainName') === 'canvas'
                ? parseInt(featureCategory?.lmsRoleId ?? '0')
                : featureCategory?.lmsRoleId;
            if (lmsRoleID === roleTypeId) {
              const selectedFeaturesList: { id: string }[] = JSON.parse(featuresList ?? '[]');
              selectedFeaturesList.map((category) => {
                selectedFeatures.push(category.id);
                return null;
              });
            }
            return null;
          });

          // lists enabled/disabled features
          features.data?.listLoreeFeatures?.items?.map((featureCategory) => {
            const allFeatureList: { featureList: { id: string }[] }[] = JSON.parse(
              featureCategory?.featureList ?? '[]',
            );
            allFeatureList.map((category, idx) => {
              const allFeatures = category.featureList;
              allFeatures.map((feature) => {
                const featureName = feature.id;
                if (selectedFeatures.includes(featureName)) {
                  listAllFeatures[featureName] = true;
                } else {
                  listAllFeatures[featureName] = false;
                }
                return null;
              });
              return null;
            });
            return null;
          });
          return listAllFeatures;
        }
      } else if (sessionStorage.getItem('domainName') === 'D2l') {
        const allFeatureList = JSON.parse(
          features.data?.listLoreeFeatures?.items?.[0]?.featureList ?? '[]',
        );
        for (const category of allFeatureList) {
          const allFeatures = category.featureList;
          for (const feature of allFeatures) {
            const featureName = feature.id;
            listAllFeatures[featureName] = true;
          }
        }
        return listAllFeatures;
      }
    } catch (err) {
      console.log('error fetching features list...', err);
    }
  };

  async retriveLmsContent(id: string, type: string) {
    const courseId = sessionStorage.getItem('course_id');
    if (sessionStorage.getItem('domainName') === 'canvas') {
      if (type === 'page') {
        const pageResponse = await API.graphql<ViewCanvasPageMutation>(
          graphqlOperation(viewCanvasPage, { courseId: courseId, pageId: id }),
        );
        const viewCanvasPageParsed = JSON.parse(pageResponse.data?.viewCanvasPage ?? 'null');
        if (viewCanvasPageParsed?.statusCode === 200) {
          return viewCanvasPageParsed.body;
        } else {
          return false;
        }
      } else if (type === 'assignment') {
        const assignmentResponse = await API.graphql<ViewCanvasAssignmentMutation>(
          graphqlOperation(viewCanvasAssignment, {
            courseId: courseId,
            assignmentId: id,
          }),
        );
        const viewCanvasAssignmentParsed = JSON.parse(
          assignmentResponse.data?.viewCanvasAssignment ?? 'null',
        );
        if (viewCanvasAssignmentParsed?.statusCode === 200) {
          return viewCanvasAssignmentParsed.body;
        } else {
          return false;
        }
      } else if (type === 'discussion') {
        const discussionResponse = await API.graphql<ViewCanvasDiscussionMutation>(
          graphqlOperation(viewCanvasDiscussion, {
            courseId: courseId,
            discussionId: id,
          }),
        );
        const viewCanvasDiscussionParsed = JSON.parse(
          discussionResponse.data?.viewCanvasDiscussion ?? 'null',
        );
        if (viewCanvasDiscussionParsed?.statusCode === 200) {
          return viewCanvasDiscussionParsed.body;
        } else {
          return false;
        }
      }
    } else if (sessionStorage.getItem('domainName') === 'D2l') {
      const contentResponse = await API.graphql<ViewD2lContentsQuery>(
        graphqlOperation(viewD2lContents, {
          courseId: `${courseId}`,
          contentId: `${id}`,
          contentType: `${type}`,
        }),
      );
      const viewD2lContentsParsed = JSON.parse(contentResponse.data?.viewD2lContents ?? 'null');
      if (viewD2lContentsParsed?.statusCode === 200) {
        return viewD2lContentsParsed.body;
      } else {
        return false;
      }
    } else if (sessionStorage.getItem('domainName') === 'BB') {
      const contentResponse = await API.graphql<ViewBbPageContentsByIdQuery>(
        graphqlOperation(viewBbPageContentsById, {
          courseId: `${courseId}`,
          contentId: `${id}`,
        }),
      );
      const viewBbPageContentsByIdParsed = JSON.parse(
        contentResponse.data?.viewBbPageContentsById ?? 'null',
      );
      if (viewBbPageContentsByIdParsed?.statusCode === 200) {
        return viewBbPageContentsByIdParsed.body;
      } else {
        return false;
      }
    }
  }

  handleCheckerListenerEvent = async (display: string, showCount: number = 0) => {
    // view and update the notification count
    const a11yNotification = getEditorElementById(CONSTANTS.LOREE_A11Y_NOTIFICATION_COUNT);

    if (!a11yNotification) {
      return;
    }
    a11yNotification.innerHTML = convertIntoDoubleDigit(showCount).toString();
    a11yNotification.style.display = display;
  };

  render(): React.ReactNode {
    if (this.state.loading) {
      return (
        <>
          <Container>
            <Loading />
            <p className='loaderViewText' style={{ top: '50%' }}>
              {translate('landing.readytorolltheeditoriscomingrightup')}
            </p>
          </Container>
        </>
      );
    }
    return (
      <A11yContextProvider quickFixer={quickFixer}>
        <LoreeEditor
          checkLMS={`${this.props.match.path}`}
          pageId={`${this.props.match.params.id}`}
          type={`${this.props.match.params.type}`}
          title={
            this.state.projectTitle
              ? `${this.state.projectTitle}/${this.state.pageTitle}`
              : this.state.pageTitle
          }
          getTitleAndContent={this.getTitleAndContent}
          customColor={this.state.customColor}
          customFonts={this.state.customFont}
          customHeader={this.state.customHeader}
          customLink={this.state.customLink}
          d2lModulesContent={this.state.d2lModulesContent}
          d2lHeadContent={this.state.headTag}
          bbContentType={this.state.bbContentType}
          featuresList={this.state.featuresList}
        />
      </A11yContextProvider>
    );
  }
}
