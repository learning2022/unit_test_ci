/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import { ReactComponent as SaveIcon } from './icons/saveIcon.svg';
import { ReactComponent as DuplicateTemplateIcon } from './icons/duplicateIcon.svg';
import { ReactComponent as AddRowIcon } from './icons/addRowIcon.svg';
import { ReactComponent as SaveTemplateIcon } from './icons/saveTemplate.svg';
import { ReactComponent as UpdateTemplateIcon } from './icons/updateTemplate.svg';
import { ReactComponent as SaveToLmsIcon } from './icons/saveToLms.svg';
import { ReactComponent as ClearEditor } from './icons/clearEditor.svg';
import { Auth, API, graphqlOperation } from 'aws-amplify';
import { getIFrameElementById } from '../../../loree-editor/common/dom';
import {
  listProjects,
  getProject,
  listCustomTemplates,
  categoryByPlatform,
} from '../../../graphql/queries';
import {
  updatePage,
  createPage,
  createCustomTemplate,
  updateCustomTemplate,
  createCategory,
} from '../../../graphql/mutations';
import './saveIcon.scss';
import CONSTANTS from '../../../loree-editor/constant';
import {
  customElementAlert,
  customElementAlertRedirectDashboard,
} from '../../../loree-editor/alert';
import {
  customBlockEditStatus,
  customBlockId,
  customBlockTitle,
  customBlockCategoryName,
  customBlockSelectedCategoryId,
  changeCustomBlockTitle,
  changeCustomBlockCategoryId,
  changeCustomBlockCategoryName,
  removePrompt,
  customGlobalStatus,
  convertCourseImageAsS3Image,
} from '../../../utils/saveContent';
import {
  changeHeaderTitle,
  customBlockThumbnailImage,
  appendNewCategory,
} from '../../../loree-editor/modules/customBlocks/customBlockEvents';
import { refreshTemplateList } from '../../../loree-editor/modules/customBlocks/customTemplateEvents';
import { LmsAccess } from '../../../utils/lmsAccess';
import { translate } from '../../../i18n/translate';
import LmsLoader from './saveToLmsLoader';
import $ from 'jquery';
import 'bootstrap';
import { isTemplateClicked } from '../../../loree-editor/modules/sidebar/sidebar';
import { CreateNewPageModal, ClearEditorModal, SaveTemplateModal } from './editorPageModal';
import { lmsUrlRedirection } from '../../../lmsConfig';
import {
  CategoryByPlatformQuery,
  CreateCategoryMutation,
  CreateCustomTemplateMutation,
  CreatePageMutation,
  GetProjectQuery,
  ListProjectsQuery,
} from '../../../API';
import { handleCustomTemplateAPI } from '../../../lti/admin/customTemplate/customTemplateActions';
import { TemplatePayloadType } from './saveIcon.interface';
import { getEditorElementById } from '../../../loree-editor/utils';

interface SaveIconProps {
  getTitleAndContent: () => void;
  saveEditorContent: () => void;
  clearAnchorPoppers: () => void;
  saveContentManually: () => void;
  deleteContent: () => void;
  getContent: () => string;
  standalone: boolean;
  manualSaving: boolean;
  closeLoaderModal: () => void;
  featuresList: { saveastemplate: boolean };
}

let observer: _Any;
let timer: ReturnType<typeof setTimeout>;

class SaveIconComponent extends Component<SaveIconProps> {
  state = {
    showSaveIconOptions: false,
    pageModalShow: false,
    page: '',
    projects: [],
    selectedProject: '',
    selectedCategory: '',
    creating: false,
    disableButton: true,
    currentPageContent: '',
    errors: '',
    templateModalShow: false,
    deleteEditorContentModalShow: false,
    templateName: '',
    categoryName: '',
    disableTemplateButton: true,
    saving: false,
    categories: [],
    categoryValue: '',
    categoryError: '',
    action: '',
    editCategoryName: '',
    editCategoryId: '',
    templates: [],
    categoryNames: [],
    templateNameError: '',
    categoryNameError: '',
    saveTemplateType: '',
    featuresList: false,
  };

  componentDidMount() {
    const lms = new LmsAccess();
    if (lms.getAccess() && this.props.featuresList) {
      this.setState({
        featuresList: !!this.props.featuresList.saveastemplate,
      });
    }
    const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
    if (!ltiPlatformID) void this.getProjectList();
    timer = setTimeout(() => {
      this.initiateIconTooltips();
      this.saveiconPosition();
    }, 150);
    if (customBlockEditStatus) this.editTemplateDetails();
  }

  initiateIconTooltips = () => {
    ($('[data-toggle="tooltip"]') as _Any).tooltip();

    $('.editor-tooltip').on('click', function () {
      ($(this) as _Any).tooltip('hide');
    });
  };

  getProjectList = async () => {
    const projectList: _Any = await API.graphql<ListProjectsQuery>(graphqlOperation(listProjects));
    const project: { id: string; name: string }[] = [];

    const projectListData = projectList?.data?.listProjects?.items ?? [];
    projectListData.map((item: { id: string; title: string }) =>
      project.push({ id: item.id, name: item.title }),
    );
    this.setState({ projects: project });
  };

  saveiconPosition = (): void => {
    const loreeSidebar: HTMLElement | null = document.getElementById(CONSTANTS.LOREE_SIDEBAR);
    const saveIconPosition: HTMLElement | null = document.getElementById(
      CONSTANTS.LOREE_SAVE_ICON_ELEMENT,
    );
    if (loreeSidebar && saveIconPosition) {
      observer = new MutationObserver((mutations) => {
        mutations.forEach(() => {
          const loreeSidebarDisp = window
            .getComputedStyle(loreeSidebar)
            .getPropertyValue('display');
          if (loreeSidebarDisp !== 'none') {
            saveIconPosition.classList.add('save-add-margin');
          } else {
            saveIconPosition.classList.remove('save-add-margin');
          }
        });
      });
      observer.observe(loreeSidebar, { attributes: true, attributeFilter: ['style'] });
    }
  };

  openCreateNewPageModal = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent> | React.KeyboardEvent<HTMLDivElement>,
    action: string,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    if (action === 'create' || action === 'duplicate') {
      this.setState({
        pageModalShow: true,
        action: action,
      });
      this.props.saveEditorContent();
    }

    if (action === 'newTemplate') {
      this.setState({
        saveTemplateType: 'Save as New Template',
        disableTemplateButton: true,
      });
    } else if (action === 'updateTemplate') {
      this.setState({
        saveTemplateType: 'Update Template',
      });
    } else {
      this.setState({
        saveTemplateType: 'Save to My Template',
      });
    }

    if (action === 'template' || action === 'newTemplate' || action === 'updateTemplate') {
      void this.getCategories(action);
      this.setState({
        templateModalShow: true,
      });

      if (action === 'updateTemplate') {
        this.editTemplateDetails();
      }
      this.props.saveEditorContent();
    }
    if (action === 'deleteEditorContent') {
      this.props.clearAnchorPoppers();
      this.setState({
        deleteEditorContentModalShow: true,
      });
    }
  };

  editTemplateDetails = () => {
    this.setState({
      templateName: customBlockTitle,
      editCategoryName: customBlockCategoryName,
      categoryName: customBlockCategoryName,
      disableTemplateButton: false,
      categoryValue: customBlockSelectedCategoryId,
      editCategoryId: customBlockCategoryName,
    });
  };

  setModalShow = () => {
    this.setState({ disableButton: true });
  };

  setPageModalShow = (value: boolean) => {
    this.setState({
      pageModalShow: value,
      page: '',
      selectedProject: '',
      selectedCategory: '',
      disableButton: true,
    });
  };

  getCategories = async (action: string) => {
    const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
    let categoryLists: _Any = [];
    const categoryNames: Array<string> = [];
    if (ltiPlatformID)
      categoryLists = await API.graphql<CategoryByPlatformQuery>(
        graphqlOperation(categoryByPlatform, { ltiPlatformID: ltiPlatformID }),
      );
    else
      categoryLists = await API.graphql(
        graphqlOperation(categoryByPlatform, { ltiPlatformID: 'STANDALONE' }),
      );
    if (categoryLists?.data?.categoryByPlatform?.items) {
      const categoryItems = categoryLists.data.categoryByPlatform.items;
      categoryItems.sort((a: { createdAt: string }, b: { createdAt: string }) => {
        return b.createdAt.localeCompare(a.createdAt);
      });
      categoryItems.forEach((item: { id: string; name: string }) => {
        if (customBlockEditStatus && action !== 'newTemplate') {
          if (customBlockSelectedCategoryId !== item.id) {
            categoryNames.push(item.name);
          }
        } else categoryNames.push(item.name);
      });
      this.setState({
        categories: categoryItems,
        categoryNames: categoryNames,
      });
    }
  };

  getTemplateLists = async (action: string) => {
    const ltiPlatformID = sessionStorage.getItem('ltiPlatformId');
    let templates: _Any = [];
    const templatesArray: string[] = [];
    let templateResponse = [];
    if (ltiPlatformID) {
      templates = await handleCustomTemplateAPI();
      for (const activeTemplate of templates) {
        if (activeTemplate?.active) {
          templateResponse.push(activeTemplate);
        }
      }
    } else {
      templates = await API.graphql(
        graphqlOperation(listCustomTemplates, { filter: { ltiPlatformID: { eq: 'STANDALONE' } } }),
      );
      templateResponse = templates.data.listCustomTemplates.items.filter(
        (data: { active: boolean }) => {
          return data.active;
        },
      );
    }
    if (templateResponse) {
      templateResponse.forEach((item: { id: string; title: string }) => {
        if (customBlockEditStatus && action !== 'newTemplate') {
          if (customBlockId !== item.id) templatesArray.push(item.title);
        } else templatesArray.push(item.title);
      });
      this.setState({
        templates: templatesArray,
      });
    }
  };

  setTemplateModalShow = (value: boolean) => {
    this.setState({
      templateModalShow: value,
      templateName: '',
      disableTemplateButton: !customBlockEditStatus,
      saving: false,
      categoryError: '',
      categoryValue: '',
      categoryLists: [],
      categoryName: '',
      templateNameError: '',
      categoryNames: [],
      templates: [],
      categoryNameError: '',
    });
  };

  setDeleteEditorContentModalShow = (value: boolean) => {
    if (value) {
      this.props.deleteContent();
    }
    this.setState({
      deleteEditorContentModalShow: false,
    });
  };

  templateNameHandler = (
    event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent<Element>,
  ) => {
    this.setState(
      { templateName: (event.target as HTMLButtonElement)?.value },
      this.validateTempSaveButton.bind(this),
    );
  };

  categoryNameHandler = (
    event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent<Element>,
  ) => {
    this.setState(
      { categoryName: (event.target as HTMLButtonElement)?.value },
      this.validateTempSaveButton.bind(this),
    );
  };

  createNewCategory = async (ltiPlatformID: string | null) => {
    const categoryInput: { name: string; ltiPlatformID: string } = {
      name: this.state.categoryName,
      ltiPlatformID: ltiPlatformID || 'STANDALONE',
    };
    const createdCategory = await API.graphql<CreateCategoryMutation>(
      graphqlOperation(createCategory, { input: categoryInput }),
    );
    if (createdCategory?.data?.createCategory?.id)
      this.state.categoryValue = createdCategory.data.createCategory.id;
    if (isTemplateClicked)
      appendNewCategory(this.state.categoryValue, this.state.categoryName, 'template');
  };

  handleSaveTemplate = async (
    event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent<Element>,
  ) => {
    event.preventDefault();
    event.stopPropagation();
    const templateName: string = this.state.templateName.trim();
    const templateLists: string[] = this.state.templates;
    const categoryName: string = this.state.categoryName.trim();
    const categoryLists: string[] = this.state.categoryNames;
    const templateSaveType = this.state.saveTemplateType;
    removePrompt(false);

    if (!customGlobalStatus) {
      const templateNameExists = templateLists.includes(templateName);
      const categoryExists = categoryName !== '' && categoryLists.includes(categoryName);
      this.setState({
        templateNameError: templateNameExists
          ? translate('template.templatenamealreadyexists')
          : false,
        categoryNameError: categoryExists ? translate('global.categorynamealreadyexists') : false,
      });
      if (templateNameExists || categoryExists) {
        return;
      }
    }

    this.setState({ saving: true, disableTemplateButton: true });

    const categoryBlock = document.getElementById('category-name-block');
    try {
      const currentAuthenticatedUser = await Auth.currentAuthenticatedUser();
      const ltiPlatformID = currentAuthenticatedUser.attributes['custom:platform'];
      if (categoryBlock && !categoryBlock?.classList.contains('d-none')) {
        await this.createNewCategory(ltiPlatformID);
      }
      const editorContent = this.props.getContent();

      const currentUser = currentAuthenticatedUser.attributes.name;
      const currentUserId = currentAuthenticatedUser.attributes.sub;

      const iframeContent = getIFrameElementById(
        'loree-iframe-content-wrapper',
      ) as HTMLIFrameElement;
      const templatePayload: TemplatePayloadType = {
        title: this.state.templateName,
        content: await convertCourseImageAsS3Image(editorContent, currentUserId),
        thumbnail: await customBlockThumbnailImage(
          this.state.templateName,
          'template',
          iframeContent,
        ),
        status: true,
        isGlobal: false,
        active: true,
      };
      switch (templateSaveType) {
        case 'Save as New Template':
        case 'Save to My Template':
          templatePayload.categoryID = this.state.categoryValue;
          templatePayload.createdBy = currentUser;
          templatePayload.owner = currentUserId;
          templatePayload.ltiPlatformID = ltiPlatformID || 'STANDALONE';
          await API.graphql<CreateCustomTemplateMutation>(
            graphqlOperation(createCustomTemplate, { input: templatePayload }),
          );
          break;
        case 'Update Template':
          changeHeaderTitle(this.state.templateName);
          changeCustomBlockTitle(this.state.templateName);
          if (this.state.categoryValue) changeCustomBlockCategoryId(this.state.categoryValue);
          templatePayload.id = customBlockId;
          templatePayload.categoryID = this.state.categoryValue
            ? customBlockSelectedCategoryId
            : this.state.categoryValue;
          templatePayload.ltiPlatformID = this.props.standalone ? ' ' : ltiPlatformID;
          await API.graphql(graphqlOperation(updateCustomTemplate, { input: templatePayload }));
          break;
        default:
          return;
      }
      if (categoryBlock && !categoryBlock?.classList.contains('d-none')) {
        const templateButton = getEditorElementById(CONSTANTS.LOREE_HEADER_TEMPLATE_BUTTON);
        templateButton?.click();
      }
      await refreshTemplateList('myTemplates');
      this.setTemplateModalShow(false);
      this.showCustomAlertForTemplateType(templateSaveType);
    } catch (e) {
      this.setState({
        categoryError: this.state.categoryValue === '' ? 'Please select Category' : '',
        saving: false,
      });
    }
  };

  showCustomAlertForTemplateType = (templateSaveType: string) => {
    setTimeout(() => {
      customElementAlert('Template', templateSaveType === 'Update Template' ? 'updated' : 'saved');
      this.deleteCustomAlert();
    }, 1000);
    if (templateSaveType === 'Save as New Template' || templateSaveType === 'Update Template') {
      removePrompt(true);
      setTimeout(() => {
        customElementAlertRedirectDashboard();
        this.deleteCustomAlert();
      }, 4000);
      setTimeout(() => {
        window.location.replace(lmsUrlRedirection());
      }, 5000);
    }
  };

  deleteCustomAlert = () => {
    const alertPopup: NodeListOf<ChildNode> | undefined = document.getElementById(
      CONSTANTS.LOREE_WRAPPER,
    )?.childNodes;
    alertPopup?.forEach((ele: ChildNode) => {
      if ((ele as HTMLElement).classList.contains('custom-success-container')) {
        (ele as HTMLElement).classList.remove('d-none');
        setTimeout(() => {
          ele.remove();
        }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
      }
    });
  };

  handleCategory = (event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent<Element>) => {
    this.state.categories.forEach((data: { id: string; name: string }) => {
      if ((event.target as HTMLButtonElement).value === data.id)
        changeCustomBlockCategoryName(data.name);
    });
    if (
      (event.target as HTMLButtonElement).value !== '' &&
      (event.target as HTMLButtonElement).value !== '-1' &&
      (event.target as HTMLButtonElement).value !== 'newCategory'
    ) {
      this.hideNewCategoryBlock();
      this.setState(
        {
          categoryValue: (event.target as HTMLButtonElement).value,
          categoryError: '',
        },
        this.validateTempSaveButton.bind(this),
      );
    } else if ((event.target as HTMLButtonElement).value === 'newCategory') {
      this.showNewCategoryBlock();
      this.validateTempSaveButton();
      this.setState({
        categoryValue: (event.target as HTMLButtonElement).value,
        categoryError: '',
      });
    } else {
      this.hideNewCategoryBlock();
      this.setState(
        {
          categoryValue: '',
          categoryError: 'Please select Category',
        },
        this.validateTempSaveButton.bind(this),
      );
    }
  };

  showNewCategoryBlock = () => {
    const categoryBlock = document.getElementById('category-name-block');
    categoryBlock?.classList?.remove('d-none');
  };

  hideNewCategoryBlock = () => {
    const categoryBlock = document.getElementById('category-name-block');
    categoryBlock?.classList?.add('d-none');
    this.setState({ categoryName: '' });
  };

  validateTempSaveButton = () => {
    const categoryBlock = document.getElementById('category-name-block');
    if (
      (this.state.categoryValue.trim().length > 0 &&
        this.state.templateName.trim().length > 0 &&
        categoryBlock?.classList.contains('d-none')) ||
      (this.state.categoryValue.trim().length > 0 &&
        this.state.templateName.trim().length > 0 &&
        this.state.categoryName.trim().length > 0)
    ) {
      this.setState({ disableTemplateButton: false });
    } else {
      this.setState({ disableTemplateButton: true });
    }
  };

  projectHandler = (event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent<Element>) => {
    const projectValue = (event.target as HTMLButtonElement).value;
    if (!this.selectPlaceholder(event)) {
      this.setState({ selectedProject: projectValue }, this.validateButton.bind(this));
      void this.fetchProjectDetails(projectValue);
    } else {
      this.setState({
        selectedCategory: '',
      });
    }
  };

  pageHandler = (event: Event) => {
    this.setState(
      { page: (event.target as HTMLButtonElement).value },
      this.validateButton.bind(this),
    );
  };

  selectPlaceholder = (
    event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent<Element>,
  ) => {
    if ((event.target as HTMLButtonElement).value === '-1') {
      this.setState({ selectedProject: '' }, this.validateButton.bind(this));
      return true;
    }
    return false;
  };

  validateButton = () => {
    if (this.state.selectedProject.trim().length > 0 && this.state.page.trim().length > 0) {
      this.setState({ disableButton: false });
    } else {
      this.setState({ disableButton: true });
    }
  };

  fetchProjectDetails = async (id: string) => {
    const projectDetail = await API.graphql<GetProjectQuery>(
      graphqlOperation(getProject, { id: id }),
    );
    this.setState({
      selectedCategory: projectDetail?.data?.getProject?.category?.name,
    });
  };

  handleSubmit = async (event: React.MouseEvent<Element, MouseEvent>) => {
    this.setState({ disableButton: true, creating: true });
    this.setModalShow();
    event.preventDefault();
    event.stopPropagation();
    const pageInput = {
      title: this.state.page,
      projectID: this.state.selectedProject,
    };
    try {
      const getCreatedPageDetails = await API.graphql<CreatePageMutation>(
        graphqlOperation(createPage, { input: pageInput }),
      );
      if (this.state.action === 'duplicate') {
        const editorContent = this.props.getContent();
        const id = getCreatedPageDetails?.data?.createPage?.id;
        const pageInput = {
          id: id,
          content: editorContent,
        };
        await API.graphql(graphqlOperation(updatePage, { input: pageInput }));
      }
      if (getCreatedPageDetails) {
        window.location.href = `/loree-editor/${getCreatedPageDetails?.data?.createPage?.id}`;
        this.props.getTitleAndContent();
      }
    } catch (err) {
      this.setState({ errors: err });
    }
  };

  showSaveIconOptions = () => {
    this.setState({
      showSaveIconOptions: !this.state.showSaveIconOptions,
    });
  };

  componentWillUnmount() {
    clearTimeout(timer);
    observer.disconnect();
  }

  handleSaveButtonClick = () => {
    this.props.saveContentManually();
  };

  render() {
    return (
      <>
        <div
          className='save-icon-wrapper d-flex position-absolute'
          id={CONSTANTS.LOREE_SAVE_ICON_ELEMENT}
        >
          <div className='position-relative mx-auto' id='save-icon-position'>
            <div className='save-icon px-4 py-1 d-flex'>
              {!this.props.standalone && (
                <>
                  <div
                    className='editor-tooltip mr-3'
                    onClick={() => this.handleSaveButtonClick()}
                    data-toggle='tooltip'
                    title={translate('bottommenu.savetolms')}
                    id='saveToLmsBtn'
                  >
                    <SaveToLmsIcon />
                  </div>
                  {(() => {
                    if (this.state.featuresList) {
                      return (
                        <>
                          <div
                            className='editor-tooltip mr-3'
                            onClick={(e) => this.openCreateNewPageModal(e, 'template')}
                            onKeyPress={(e) => this.openCreateNewPageModal(e, 'template')}
                            data-toggle='tooltip'
                            title={translate('bottommenu.saveastemplate')}
                          >
                            <SaveTemplateIcon />
                          </div>
                        </>
                      );
                    }
                  })()}
                  <div
                    className='editor-tooltip mr-3 d-none'
                    onClick={(e) => this.openCreateNewPageModal(e, 'updateTemplate')}
                    onKeyPress={(e) => this.openCreateNewPageModal(e, 'updateTemplate')}
                    data-toggle='tooltip'
                    title={translate('global.updatetemplate')}
                  >
                    <UpdateTemplateIcon />
                  </div>
                  {(() => {
                    if (this.state.featuresList) {
                      return (
                        <>
                          <div
                            className='editor-tooltip mr-3 d-none'
                            onClick={(e) => this.openCreateNewPageModal(e, 'newTemplate')}
                            onKeyPress={(e) => this.openCreateNewPageModal(e, 'newTemplate')}
                            data-toggle='tooltip'
                            title={translate('bottommenu.saveasnewtemplate')}
                          >
                            <SaveTemplateIcon />
                          </div>
                        </>
                      );
                    }
                  })()}
                </>
              )}
              {this.props.standalone && (
                <>
                  <div
                    className='mr-3'
                    onClick={() => this.showSaveIconOptions()}
                    onKeyPress={() => this.showSaveIconOptions()}
                  >
                    <SaveIcon />
                  </div>
                  <div
                    className={`position-absolute save-options ${
                      this.state.showSaveIconOptions ? 'save-icon-show' : ''
                    }`}
                    id='saveIconOptionsWrapper'
                  >
                    <div
                      id='save-option-list-wrapper'
                      className='save-options-list d-flex justify-content-between p-2'
                    >
                      <div
                        className='editor-tooltip mr-2'
                        onClick={() => this.handleSaveButtonClick()}
                        onKeyPress={() => this.handleSaveButtonClick()}
                        data-toggle='tooltip'
                        title={translate('global.save')}
                      >
                        <SaveIcon />
                      </div>
                      <div
                        className='editor-tooltip mr-2'
                        onClick={(e) => this.openCreateNewPageModal(e, 'template')}
                        onKeyPress={(e) => this.openCreateNewPageModal(e, 'template')}
                        data-toggle='tooltip'
                        title={translate('bottommenu.saveastemplate')}
                      >
                        <SaveTemplateIcon />
                      </div>
                      <div
                        className='editor-tooltip mr-2 d-none'
                        onClick={(e) => this.openCreateNewPageModal(e, 'updateTemplate')}
                        onKeyPress={(e) => this.openCreateNewPageModal(e, 'updateTemplate')}
                        data-toggle='tooltip'
                        title={translate('global.updatetemplate')}
                      >
                        <SaveTemplateIcon />
                      </div>
                      <div
                        className='editor-tooltip mr-2 d-none'
                        onClick={(e) => this.openCreateNewPageModal(e, 'newTemplate')}
                        onKeyPress={(e) => this.openCreateNewPageModal(e, 'newTemplate')}
                        data-toggle='tooltip'
                        title={translate('bottommenu.saveasnewtemplate')}
                      >
                        <SaveTemplateIcon />
                      </div>
                      <div
                        className='editor-tooltip mr-2'
                        onClick={(e) => this.openCreateNewPageModal(e, 'duplicate')}
                        onKeyPress={(e) => this.openCreateNewPageModal(e, 'duplicate')}
                        data-toggle='tooltip'
                        title={translate('bottommenu.duplicatepage')}
                      >
                        <DuplicateTemplateIcon />
                      </div>
                      <div
                        className='editor-tooltip'
                        onClick={(e) => this.openCreateNewPageModal(e, 'create')}
                        onKeyPress={(e) => this.openCreateNewPageModal(e, 'create')}
                        data-toggle='tooltip'
                        title={translate('bottommenu.createnewpage')}
                      >
                        <AddRowIcon />
                      </div>
                    </div>
                  </div>
                </>
              )}
              <div
                className='editor-tooltip'
                onClick={(e) => this.openCreateNewPageModal(e, 'deleteEditorContent')}
                onKeyPress={(e) => this.openCreateNewPageModal(e, 'deleteEditorContent')}
                data-toggle='tooltip'
                title={translate('bottommenu.cleareditor')}
              >
                <ClearEditor />
              </div>
            </div>
          </div>
        </div>
        <div className='loree-space-poweredBy' id={CONSTANTS.LOREE_SPACE_POWEREDBY}>
          <div className='powered-by-footer-section'>
            <p>
              Powered by <span>Crystal Delta</span> &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{' '}
              {CONSTANTS.LOREE_APP_VERSION}
            </p>
          </div>
        </div>
        <LmsLoader
          manualSaving={this.props.manualSaving}
          closeLoaderModal={this.props.closeLoaderModal}
          type='pageSave'
          action='Saving to LMS'
        />
        <CreateNewPageModal
          pageModalProps={{
            handleSubmit: this.handleSubmit,
            projectHandler: this.projectHandler,
            setPageModalShow: this.setPageModalShow,
            state: this.state,
          }}
        />
        <SaveTemplateModal
          templateProps={{
            setTemplateModalShow: this.setTemplateModalShow,
            handleCategory: this.handleCategory,
            templateNameHandler: this.templateNameHandler,
            categoryNameHandler: this.categoryNameHandler,
            handleSaveTemplate: this.handleSaveTemplate,
            state: this.state,
          }}
        />
        <ClearEditorModal
          deleteProps={{
            setDeleteEditorContentModalShow: this.setDeleteEditorContentModalShow,
            state: { deleteEditorContentModalShow: this.state.deleteEditorContentModalShow },
          }}
        />
      </>
    );
  }
}

export default SaveIconComponent;
