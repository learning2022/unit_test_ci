export interface SaveIconProps {
  getTitleAndContent: () => void;
  saveEditorContent: () => void;
  clearAnchorPoppers: () => void;
  saveContentManually: () => void;
  deleteContent: () => void;
  getContent: () => string;
  standalone: boolean;
  manualSaving: boolean;
  closeLoaderModal: () => void;
  featuresList: { saveastemplate: boolean; imageFeatureList: boolean };
}
export interface TemplatePayloadType {
  title: string;
  content: string;
  thumbnail: ThumbnailObjectType;
  status: boolean;
  isGlobal: boolean;
  active: boolean;
  categoryID?: string | null;
  createdBy?: string;
  owner?: string;
  id?: string | null;
  ltiPlatformID?: string | null;
}

interface ThumbnailObjectType {
  bucket: string;
  key: string;
  region: string;
}
