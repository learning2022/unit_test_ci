import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import SaveIconComponent from './saveIcon';
import * as saveContentEvents from '../../../utils/saveContent';
import * as customBlockHandler from '../../../../src/loree-editor/modules/customBlocks/customBlockHandler';
import * as customBlockEvent from '../../../../src/loree-editor/modules/customBlocks/customBlockEvents';
import { API, Auth } from 'aws-amplify';
import {
  editorContentMockData,
  eventMockData,
  imageBlockThumbnailMockData,
  saveEventMockData,
  saveTemplateTestMockData,
  saveTemplateType,
} from './saveIconMockData';
import * as customTemplateEvents from '../../../loree-editor/modules/customBlocks/customTemplateEvents';
import { userMockData } from '../../../loree-editor/modules/customBlocks/customBlockMockData';
import CONSTANTS from '../../../loree-editor/constant';
import { createElement, createIframeElement } from '../../../loree-editor/common/dom';

jest.mock('../../../i18n/translate', () => ({
  translate: jest.fn().mockImplementation((v) => `${v}`),
}));

jest.useFakeTimers();

const testProp = {
  getTitleAndContent: jest.fn(),
  saveEditorContent: jest.fn(),
  clearAnchorPoppers: jest.fn(),
  saveContentManually: () => jest.fn(),
  deleteContent: jest.fn(),
  getContent: jest.fn().mockImplementation(() => {
    return editorContentMockData;
  }),
  standalone: false,
  manualSaving: false,
  closeLoaderModal: jest.fn(),
  featuresList: { saveastemplate: true },
};
let wrapper: ReactWrapper;

describe('when editor is loaded', () => {
  beforeEach(() => {
    wrapper = mount(<SaveIconComponent {...testProp} />);
  });
  it('renders save button component', () => {
    expect(wrapper.find('.save-icon-wrapper')).not.toBeNull();
  });

  it('save button shoud be present', () => {
    expect(wrapper.find('.save-icon')).toHaveLength(1);
  });

  it('save as template has onclick event', () => {
    const templateSave = wrapper.find('.editor-tooltip').at(0).props();
    expect(Object.prototype.hasOwnProperty.call(templateSave, 'onClick')).toBe(true);
  });

  it('save as duplicate has onclick event', () => {
    const saveAsDuplicate = wrapper.find('.editor-tooltip').at(1).props();
    expect(Object.prototype.hasOwnProperty.call(saveAsDuplicate, 'onClick')).toBe(true);
  });

  it('create new page has onclick event', () => {
    const createNew = wrapper.find('.editor-tooltip').at(0).props();
    expect(Object.prototype.hasOwnProperty.call(createNew, 'onClick')).toBe(true);
  });
  test('renders footer', () => {
    const footer = wrapper.find('#loree-space-poweredby');
    expect(footer.text().includes('Powered by Crystal Delta')).toBeTruthy();
  });
});

describe('When user before edit the template', () => {
  beforeEach(() => {
    wrapper = mount(<SaveIconComponent {...testProp} />);
  });
  test('the Editor is not in edit mode', () => {
    expect(saveContentEvents.customBlockEditStatus).toBeFalsy();
  });
  test('Check for the editor having 3 bottom options before Edit', () => {
    expect(wrapper.find('.editor-tooltip').length).toBe(3);
  });

  test('Check Update Template option is available as D-NONE before Edit', () => {
    expect(wrapper.find('.editor-tooltip').at(0).props().title).toEqual('bottommenu.savetolms');
    expect(wrapper.find('.editor-tooltip').at(1).props().title).toEqual('global.updatetemplate');
    expect(wrapper.find('.editor-tooltip').at(2).props().title).toEqual('bottommenu.cleareditor');
  });

  test('Verify Save To LMS option is visible before edit', () => {
    const isExists = wrapper.find('.editor-tooltip').at(0).props().className;
    expect(isExists?.includes('d-none')).toBeFalsy();
  });
  test('Verify Update Template is not visible before edit', () => {
    const isExists = wrapper.find('.editor-tooltip').at(1).props().className;
    expect(isExists?.includes('d-none')).toBeTruthy();
  });
  test('Verify Clear Editor option is visible before edit', () => {
    const isExists = wrapper.find('.editor-tooltip').at(2).props().className;
    expect(isExists?.includes('d-none')).toBeFalsy();
  });
});

describe('When user edit the template', () => {
  beforeAll(() => {
    wrapper = mount(<SaveIconComponent {...testProp} />, { attachTo: document.body });
    window.sessionStorage.setItem('lmsUrl', 'https://test.com');
    saveContentEvents.setEditMode(true); // SetEditMode
  });
  test('Verify edit Mode is on', () => {
    expect(saveContentEvents.customBlockEditStatus).toBeTruthy();
  });
  test('Verify Save To LMS option is not visible now on edit', () => {
    customBlockHandler.changeSaveOptionList('My Template');
    const saveToLms = document.getElementsByClassName('editor-tooltip')[0];
    expect(saveToLms.className?.includes('d-none')).toBeTruthy();
  });
  test('Verify Update Template option is visible now on edit', () => {
    const updateTemplate = document.getElementsByClassName('editor-tooltip')[1];
    expect(updateTemplate.className?.includes('d-none')).toBeFalsy();
  });
  test('Verify Clear Editor option is visible now on edit', () => {
    const clearEditor = document.getElementsByClassName('editor-tooltip')[2];
    expect(clearEditor.className?.includes('d-none')).toBeFalsy();
  });
  test('click on update template option opens modal', () => {
    (document.getElementsByClassName('editor-tooltip')[1] as HTMLElement).click();
    expect(document.getElementById('save-template-model')?.childNodes[0].textContent).toEqual(
      'Update Template',
    );
  });
  afterAll(() => {
    document.body.innerHTML = '';
  });
});

describe('#BrowserPromptListener', () => {
  beforeEach(() => {
    wrapper = mount(<SaveIconComponent {...testProp} />);
  });
  test('browser prompt to be enabled on template save action', () => {
    wrapper.find('.editor-tooltip').at(1).simulate('click');
    wrapper.find({ 'data-test': 'save-template-button' }).at(1).simulate('click');
    expect(saveContentEvents.removePromptListener).toBeFalsy();
  });
});

describe('#Clear-Editor', () => {
  beforeEach(() => {
    wrapper = mount(<SaveIconComponent {...testProp} />);
  });

  test('when user trigger the clear editor button', () => {
    wrapper.find('.editor-tooltip').at(4).simulate('click');
    expect(document.getElementsByClassName('modal-title')[1].textContent).toBe(
      'modal.clearcontent',
    );
  });
  test('click on clear button in modal', () => {
    (document.getElementsByClassName('editor-btn-primary')[0] as HTMLElement).click();
    expect(wrapper.state('deleteEditorContentModalShow')).toEqual(false);
  });
  test('click on cancel button in modal', () => {
    wrapper.find('.editor-tooltip').at(4).simulate('keypress');
    (document.getElementsByClassName('btn-outline-primary')[1] as HTMLElement).click();
    expect(wrapper.state('deleteEditorContentModalShow')).toEqual(false);
  });
  test('click on X icon closes modal', () => {
    wrapper.find('.editor-tooltip').at(4).simulate('keypress');
    wrapper.find('button').at(0).simulate('click');
    expect(wrapper.state('deleteEditorContentModalShow')).toEqual(false);
  });
});

describe('save as template in LMS editor', () => {
  beforeAll(() => {
    wrapper = mount(<SaveIconComponent {...testProp} />);
    API.graphql = jest.fn().mockImplementation(() => {
      return {
        data: {
          categoryByPlatform: {
            items: [
              {
                id: '1c76cc8a-9ed2-4a4f-8e89-f2272ea2ee79',
                ltiPlatformID: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a',
                name: 'byplatform-canvas21',
                projects: {
                  nextToken: null,
                },
                createdAt: '2021-05-12T05:59:55.434Z',
                updatedAt: '2021-05-12T05:59:55.434Z',
              },
            ],
            nextToken: null,
          },
        },
      };
    });
  });
  test('save as template click', () => {
    wrapper.find('.editor-tooltip').at(1).simulate('click');
    expect(document.getElementById('save-template-model')?.childNodes[0].textContent).toEqual(
      'Save to My Template',
    );
  });
  test('change template name input', () => {
    wrapper
      .find(`[data-test='templateName']`)
      .at(0)
      ?.simulate('change', { target: { value: 'Test Template' } });
    expect(wrapper.state('templateName')).toEqual('Test Template');
  });
  test('click on save button with empty catagory throws error', () => {
    wrapper
      .find('.template-category-dropdown')
      .at(0)
      .simulate('change', { target: { value: '' } });
    wrapper.find({ 'data-test': 'save-template-button' }).at(0).simulate('click');
    expect(wrapper.state('categoryError')).toEqual('Please select Category');
  });
  test('click on Add New Category', async () => {
    wrapper
      .find('.template-category-dropdown')
      .at(0)
      .simulate('change', { target: { value: 'newCategory' } });
    wrapper
      .find(`[data-test='categoryName']`)
      .at(0)
      .simulate('change', { target: { value: 'test category' } });
    expect(wrapper.state('categoryName')).toEqual('test category');
    expect(wrapper.state('disableTemplateButton')).toEqual(false);
  });
  test('select a category option', () => {
    wrapper
      .find('.template-category-dropdown')
      .at(0)
      .simulate('change', { target: { value: '1c76cc8a-9ed2-4a4f-8e89-f2272ea2ee79' } });
    expect(wrapper.state('categoryName')).toEqual('');
  });
  test('click on save template button changes button text', () => {
    wrapper.find({ 'data-test': 'save-template-button' }).at(0).simulate('click');
    expect(wrapper.state('saving')).toEqual(true);
    expect(wrapper.find({ 'data-test': 'save-template-button' }).at(0).text()).toEqual(
      'global.saving',
    );
  });
  test('click on cancel button closes modal', () => {
    wrapper.find('.editor-tooltip').at(1).simulate('keypress');
    wrapper.find('.modal-footer-cancel-btn').at(0).simulate('click');
    expect(wrapper.state('templateModalShow')).toEqual(false);
  });
  test('click on X icon closes modal', () => {
    wrapper.find('.editor-tooltip').at(1).simulate('keypress');
    wrapper.find('button').at(0).simulate('click');
    expect(wrapper.state('templateModalShow')).toEqual(false);
  });
  afterAll(() => {
    document.body.innerHTML = '';
  });
});

describe('#standalone editor', () => {
  beforeEach(() => {
    testProp.standalone = true;
    wrapper = mount(<SaveIconComponent {...testProp} />);
  });
  test('opens standalone editor', () => {
    expect(wrapper.find('#save-option-list-wrapper').children()).toHaveLength(6);
  });
  test('click on save icon shows save options', () => {
    wrapper.find('.save-icon').children().at(0).simulate('click');
    expect(wrapper.state('showSaveIconOptions')).toBeTruthy();
  });
  describe('create new page', () => {
    test('click on create new page icon opens modal', () => {
      wrapper.find('#save-option-list-wrapper').children().at(5).simulate('click');
      expect(document.getElementById('new-page-model')?.childNodes[0].textContent).toEqual(
        'modal.pagedetails',
      );
    });
    test('click on cancel closes create page modal', () => {
      (document.getElementsByClassName('modal-footer-cancel-btn')[0] as HTMLElement).click();
      expect(wrapper.state('pageModalShow')).toBeFalsy();
    });
    test('check keypress on new page icon', () => {
      wrapper.find('#save-option-list-wrapper').children().at(5).simulate('keypress');
      expect(wrapper.state('action')).toEqual('create');
    });
  });
  describe('duplicate page', () => {
    test('click on duplicate page icon opens modal', () => {
      wrapper.find('#save-option-list-wrapper').children().at(4).simulate('click');
      expect(wrapper.state('action')).toEqual('duplicate');
      expect(document.getElementById('new-page-model')?.childNodes[0].textContent).toEqual(
        'modal.pagedetails',
      );
    });
    test('check keypress on duplicate page icon', () => {
      wrapper.find('#save-option-list-wrapper').children().at(4).simulate('keypress');
      expect(wrapper.state('action')).toEqual('duplicate');
    });
    test('click on X icon closes modal', () => {
      wrapper.find('#save-option-list-wrapper').children().at(4).simulate('keypress');
      wrapper.find('button').at(0).simulate('click');
      expect(wrapper.state('pageModalShow')).toBeFalsy();
    });
  });
  describe('save as template', () => {
    test('click on save as template icon opens modal', () => {
      wrapper.find('#save-option-list-wrapper').children().at(1).simulate('click');
      expect(document.getElementById('save-template-model')?.childNodes[0].textContent).toEqual(
        'Save to My Template',
      );
    });
    test('keypress on save as template icon opens modal', () => {
      wrapper.find('#save-option-list-wrapper').children().at(1).simulate('keypress');
      expect(document.getElementById('save-template-model')?.childNodes[0].textContent).toEqual(
        'Save to My Template',
      );
    });
  });
});

describe('save/update template in LMS', () => {
  const saveIconInstance = new SaveIconComponent(testProp);
  const iframe = createIframeElement('iframe');
  function loreeWrapper() {
    document.body.innerHTML = `<div id='loree-wrapper'></div>`;
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createElement('div');
    wrapper.id = CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER;
    iframe.appendChild(wrapper);
    iframe.innerHTML += '';
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }
  beforeEach(async () => {
    loreeWrapper();
    sessionStorage.setItem('ltiPlatformId', 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a');
    Auth.currentAuthenticatedUser = jest.fn().mockImplementation(() => userMockData);
    jest
      .spyOn(customBlockEvent, 'customBlockThumbnailImage')
      .mockReturnValueOnce(imageBlockThumbnailMockData);
    jest.spyOn(customTemplateEvents, 'refreshTemplateList');
    saveIconInstance.getCategories = jest.fn();
    saveIconInstance.getTemplateLists = jest.fn();
  });

  test.each(saveTemplateType)('click on save template button should open the modal', ({ type }) => {
    saveIconInstance.openCreateNewPageModal(saveEventMockData, type);
    expect(saveIconInstance.getCategories).toBeCalledWith(type);
    expect(saveIconInstance.getTemplateLists).not.toBeCalledWith(type);
  });

  test.each(saveTemplateTestMockData)(
    'should create/Update tempalte when the feature toggle is off',
    async ({ templateType, savedTemplateContent }) => {
      saveIconInstance.state.saveTemplateType = templateType;
      sessionStorage.setItem('domainName', 'bb');
      API.graphql = jest.fn().mockImplementation(() => savedTemplateContent);
      saveIconInstance.handleSaveTemplate(eventMockData);
      expect(saveIconInstance.state.templateName).toBe('');
      expect(saveIconInstance.state.categoryName).toBe('');
      expect(saveIconInstance.state.saveTemplateType).toBe(templateType);
    },
  );

  test.each(saveTemplateTestMockData)(
    'should create/Update tempalte when the feature toggle is on',
    async ({ templateType, savedTemplateContent }) => {
      saveIconInstance.state.saveTemplateType = templateType;
      sessionStorage.setItem('domainName', 'canvas');
      API.graphql = jest.fn().mockImplementation(() => savedTemplateContent);
      saveIconInstance.handleSaveTemplate(eventMockData);
      expect(saveIconInstance.state.templateName).toBe('');
      expect(saveIconInstance.state.categoryName).toBe('');
      expect(saveIconInstance.state.saveTemplateType).toBe(templateType);
    },
  );
});
