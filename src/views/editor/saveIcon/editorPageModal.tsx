/* eslint-disable @typescript-eslint/no-explicit-any, @typescript-eslint/no-redeclare */

import React from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import CustomFormGroup from '../../page/cutomFormGroup';
import { translate } from '../../../i18n/translate';

interface CreateNewPageModalProps {
  pageModalProps: CreateNewPageModal;
}
interface CreateNewPageModal {
  handleSubmit: (e: React.MouseEvent) => void;
  projectHandler: (e: React.MouseEvent | React.ChangeEvent) => void;
  setPageModalShow: (status: boolean) => void;
  state: any;
}
interface SaveTemplateModalProps {
  templateProps: SaveTemplateModal;
}
interface SaveTemplateModal {
  setTemplateModalShow: (status: boolean) => void;
  handleCategory: (e: React.MouseEvent | React.ChangeEvent) => void;
  templateNameHandler: (e: React.MouseEvent | React.ChangeEvent) => void;
  categoryNameHandler: (e: React.MouseEvent | React.ChangeEvent) => void;
  handleSaveTemplate: (e: React.MouseEvent) => void;
  state: any;
}
interface ClearEditorModalProps {
  deleteProps: {
    setDeleteEditorContentModalShow: (status: boolean) => void;
    state: { deleteEditorContentModalShow: boolean };
  };
}

export const CreateNewPageModal = (props: CreateNewPageModalProps) => {
  return (
    <Modal
      animation={false}
      aria-labelledby='new-page-model'
      backdrop='static'
      centered
      onHide={() => props.pageModalProps.setPageModalShow(false)}
      show={props.pageModalProps.state.pageModalShow}
      size='sm'
    >
      <Modal.Header closeButton>
        <Modal.Title id='new-page-model'>
          <h5 className='text-primary'>{translate('modal.pagedetails')}</h5>
        </Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body className='create-page-form'>
          <Form.Group controlId='formPageName'>
            <CustomFormGroup
              id='formPageProject'
              section='Project'
              required=' *'
              placeHolder={` --- ${translate('modal.chooseyourproject')} --- `}
              options={props.pageModalProps.state.projects}
              selected={(e: React.MouseEvent | React.ChangeEvent) =>
                props.pageModalProps.projectHandler(e)
              }
            />
            <Form.Label>Page Name *</Form.Label>
            <Form.Control
              required
              data-test='pageName'
              type='text'
              placeholder={` ${translate('global.pagename')}`}
              autoComplete='off'
              value={props.pageModalProps.state.page}
              onChange={(e: React.MouseEvent | React.ChangeEvent) =>
                props.pageModalProps.projectHandler(e)
              }
              maxLength={100}
            />
          </Form.Group>
          <Form.Group controlId='formPageCategory'>
            <Form.Label>{translate('global.category')}</Form.Label>
            <Form.Control
              required
              type='text'
              value={props.pageModalProps.state.selectedCategory}
              maxLength={100}
              readOnly
            />
          </Form.Group>
        </Modal.Body>

        <Modal.Footer className='d-flex justify-content-center'>
          <Button
            variant='outline-primary'
            className='modal-footer-cancel-btn editor-custom-btn mx-1'
            onClick={() => props.pageModalProps.setPageModalShow(false)}
          >
            {translate('global.cancel')}
          </Button>
          <Button
            variant='primary'
            className='editor-custom-btn mx-1'
            type='submit'
            onClick={(e: React.MouseEvent) => props.pageModalProps.handleSubmit(e)}
            disabled={props.pageModalProps.state.disableButton}
          >
            {props.pageModalProps.state.creating
              ? translate('global.creatingdot')
              : translate('global.create')}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export const SaveTemplateModal = (props: SaveTemplateModalProps) => {
  return (
    <Modal
      animation={false}
      aria-labelledby='save-template-model'
      backdrop='static'
      centered
      onHide={() => props.templateProps.setTemplateModalShow(false)}
      show={props.templateProps.state.templateModalShow}
      size='sm'
    >
      <Modal.Header closeButton>
        <Modal.Title id='save-template-model'>
          <h5 className='text-primary'>{props.templateProps.state.saveTemplateType}</h5>
        </Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body className='create-page-form'>
          <Form.Group controlId='formPageName'>
            <Form.Label>{translate('template.templatename*')}</Form.Label>
            <Form.Control
              required
              data-test='templateName'
              type='text'
              placeholder={` ${translate('template.templatename')}`}
              autoComplete='off'
              value={props.templateProps.state.templateName}
              onChange={(e) => props.templateProps.templateNameHandler(e)}
              maxLength={100}
            />
            {props.templateProps.state.templateNameError && (
              <small className='text-danger ml-2'>
                {props.templateProps.state.templateNameError}
              </small>
            )}
          </Form.Group>
          <Form.Group controlId='formTemplateCategory'>
            <Form.Label>{translate('modal.chooseyourcategory')}</Form.Label>
            <Form.Control
              as='select'
              className='template-category-dropdown'
              onChange={(e) => props.templateProps.handleCategory(e)}
            >
              {(() => {
                if (
                  props.templateProps.state.editCategoryId !== '' &&
                  props.templateProps.state.saveTemplateType === translate('global.updatetemplate')
                ) {
                  return (
                    <>
                      <option value=''>{props.templateProps.state.editCategoryId}</option>
                      <option value='newCategory'>{translate('modal.addnewcategory')}</option>
                    </>
                  );
                } else {
                  return (
                    <>
                      <option value=''>{`----${translate('global.selectcategory')}-------`}</option>
                      <option value='newCategory'>{translate('modal.addnewcategory')}</option>
                    </>
                  );
                }
              })()}

              {props.templateProps.state.categories.map((category: any) => (
                <option value={category.id} key={category.id}>
                  {category.name}
                </option>
              ))}
            </Form.Control>

            {props.templateProps.state.categoryError && (
              <small className='text-danger ml-2'>{props.templateProps.state.categoryError}</small>
            )}
          </Form.Group>
          <Form.Group className='d-none' id='category-name-block' controlId='formCategoryName'>
            <Form.Label>{translate('modal.categoryname*')}</Form.Label>
            <Form.Control
              required
              data-test='categoryName'
              type='text'
              placeholder={` ${translate('modal.categoryname')}`}
              autoComplete='off'
              value={props.templateProps.state.categoryName}
              onChange={(e) => props.templateProps.categoryNameHandler(e)}
              maxLength={100}
            />
            {props.templateProps.state.categoryNameError && (
              <small className='text-danger ml-2'>
                {props.templateProps.state.categoryNameError}
              </small>
            )}
          </Form.Group>
          {(props.templateProps.state.saveTemplateType === 'Save as New Template' ||
            props.templateProps.state.saveTemplateType === 'Update Template') && <></>}
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-center'>
          <Button
            variant='outline-primary'
            className='modal-footer-cancel-btn editor-custom-btn mx-1'
            onClick={() => props.templateProps.setTemplateModalShow(false)}
          >
            {translate('global.cancel')}
          </Button>
          <Button
            variant='primary'
            className='editor-custom-btn mx-1'
            type='submit'
            data-test='save-template-button'
            onClick={(e) => props.templateProps.handleSaveTemplate(e)}
            disabled={props.templateProps.state.disableTemplateButton}
          >
            {props.templateProps.state.saving
              ? translate('global.saving')
              : translate('global.save')}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export const ClearEditorModal = (props: ClearEditorModalProps) => {
  return (
    <Modal
      animation={false}
      aria-labelledby='clear-editor-content'
      backdrop='static'
      centered
      onHide={() => props.deleteProps.setDeleteEditorContentModalShow(false)}
      show={props.deleteProps.state.deleteEditorContentModalShow}
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <h5 className='text-primary'>{translate('modal.clearcontent')}</h5>
        </Modal.Title>
      </Modal.Header>
      <Form>
        <Modal.Body className='create-page-form'>
          <p className='mb-0'>{translate('modal.areyousureyouwanttoclear')}</p>
        </Modal.Body>
        <Modal.Footer style={{ justifyContent: 'center' }}>
          <Button
            variant='outline-primary'
            className='mx-1'
            onClick={() => props.deleteProps.setDeleteEditorContentModalShow(false)}
            size='sm'
          >
            {translate('global.cancel')}
          </Button>
          <Button
            variant='primary'
            className='editor-btn-primary mx-1'
            onClick={() => props.deleteProps.setDeleteEditorContentModalShow(true)}
            size='sm'
          >
            {translate('global.clear')}
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};
