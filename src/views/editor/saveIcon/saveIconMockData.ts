import { ChangeEvent } from 'react';

export const editorContentMockData =
  '<div class="loree-iframe-content-row row" style="padding: 10px; position:relative; margin: 0px"><div class="col-12 loree-iframe-content-column" style="padding:10px;"><div style="width:100%;margin:0 0 10px;padding:5px;display: flex;" class="loree-iframe-content-image-wrapper "><img alt="GL" style="width:auto;height:auto;max-width:100%;max-height:100%;border-width:0;border-style:solid;border-color:#000000" class="loree-iframe-content-image" src="https://loreev2storage24cd6295054c4df4b5661676008c768e164310-production.s3.ap-southeast-2.amazonaws.com/public/3b5fcee4-44c5-4501-b681-c3fc8494c2b5/6d637873-4104-44db-a17a-9895af9239dc/GL-1646068582322?X-Amz-Algorithm=AWS4-HMAC-SHA256&amp;X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&amp;X-Amz-Credential=ASIAY6CSGGBFGKHRGP4W%2F20220321%2Fap-southeast-2%2Fs3%2Faws4_request&amp;X-Amz-Date=20220321T135523Z&amp;X-Amz-Expires=900&amp;X-Amz-Security-Token=IQoJb3JpZ2luX2VjEI7%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDmFwLXNvdXRoZWFzdC0yIkcwRQIhAPY7jvcpRaZr7sU4%2Biy9xob8wZgfAY0DCN7ehRWJvlBeAiAYZ2xDAQ4fE73vG6rMYlwam3Ijc34JcqEr9zFb%2BAJllSriBAgXEAMaDDYxNDM1MjY5NTM3MCIMPEi2TjYbWF6MWizBKr8Ej1pd0mGRJs6%2BUERSiIbDsbR55IdhvcWAAtZcaMviyU4g8H595PFOlwSpEUkxmRm3AgK8mlU%2Fl4re%2B%2FqN9cEQ0Ew5sd%2FjkPo1LZF0kNdanwjTQEoNHKRfc4KNta9SjpgvRHi0WbyVWAVfoIpUnt4u0rLhOePImkD9%2FDlXXUxAjLux90J7mlfOSofDjZx%2Fq6T5Ex1Wfq7cON44faFqb1EC0tOX7S58IDXF3Ya2QLtsxJBFSiho2KZgwycj1TDrg1RPrz4YE3Np%2B5UQqRJzK4MPlj0O5LDdt0CFqiRgv3VPllhvrGopEUM7ZKnaICHX4rG1P0xKk9pi226Qu%2FDRHs%2BflCJMuNjyPr2bMAECcfrcSGcyg2t7%2Bx5p%2BzJv65vihSUxkZwPMZG%2BX2dhH%2B4PvZXkFz64fzADab%2FafQwD8i0gjohuEt%2BZvUxCmik3Qq3G1hDURaSGjYhrrdDYunAKfsO%2FncvMJBKTRQhLZGXkN3KqYGblT3qfEiANhskHO5qOeKZPitTmQUDZD3gxkmf4DpmgrzOPy9d%2BfNS1YQ82S9kSZYGHu5ORjEAYDAFCwew4vvLyIjgGkLRy1be36K5LmYmyC302rZ7ZTQzlZ17dtimefeqRKa04GGoqz8ZWZE7RLFRQNC5UDMtqF1W%2BVmVbeGJORvz5thMhRfYxJd2HZMVXoz1vOkRMtzwIeRBQ8wfVlGWwRcDo2xByFhB8yqgwb6CYnxXe8EKX9l2umNowoKqRuQwKpqIGCcpIMYCpXmrwankww4fikQY6hQKTZPqyK7P5UcktxRyNmf5pS1%2BsltBxoj47YrwRdGM8ABv5baiH1C1rWdBpwWoRv50A57GELa5VrL7%2BehQYMsU5juiQmi7sTpoQWXtLVSE5ZAnPxUMrSVBI0r557hCnx5939cfPiFWR1xpXb6K0wI8PScOJ4b4J%2BMskkBrDE7Ci4w6iNCL5lnUGNjLwkGG5wBS4aq39z%2FWQ4kPt6eUuiA4TYe%2B5yvT48wCv9dpMNXZ3YcuLHbbpRcLmTk4uc7h1Wo3VbZARK5NqD5YZoQxzOGYRtU0tTGImFkctYONtgbUiPld3fJ6Weu2j3B9pymXO1K%2BEWuR%2FFwglk54XfOkukw2vF5O%2FJjE%3D&amp;X-Amz-Signature=86c96dfbac2090209b24af544a7a0a1a8106ec4cbc8de9bc7558dd0ad2f60ced&amp;X-Amz-SignedHeaders=host&amp;x-amz-user-agent=aws-sdk-js%2F3.6.1%20os%2FLinux%20lang%2Fjs%20md%2Fbrowser%2FChrome_98.0.4758.102%20api%2Fs3%2F3.6.1%20aws-amplify%2F3.8.23_js&amp;x-id=GetObject"></div></div></div> ';

export const saveTemplateMockData = {
  data: {
    saveCanvasPage: `{"body": {"body": "<div><div><h1>Bruce Wayne</h1></div><div><img src='bruce_wayne.jpg'/></div></div>"}}`,
  },
};

export const imageBlockThumbnailMockData = {
  bucket: 'loreev2storage24cd6295054c4df4b5661676008c768e105225-local',
  key: 'd66b1309-ac2b-48e5-b96f-c0098e8baf0a/Thumbnail/template/testTemplate',
  region: 'ap-southeast-2',
} as unknown as Promise<_Any>;

export const saveTemplateTestMockData = [
  { templateType: 'Save as New Template', savedTemplateContent: saveTemplateMockData },
  { templateType: 'Save to My Template', savedTemplateContent: saveTemplateMockData },
  { templateType: 'Update Template', savedTemplateContent: saveTemplateMockData },
];

export const userMockData = {
  attributes: {
    email: 'canvas_123670000000000245@example.com',
    name: 'xyzabc',
    sub: 'f668f1c0-b21e-495a-906a-c9000cb0e886',
  },
};

export const eventMockData = {
  preventDefault: jest.fn(),
  stopPropagation: jest.fn(),
} as unknown as ChangeEvent<Element>;

export const saveEventMockData = {
  preventDefault: jest.fn(),
  stopPropagation: jest.fn(),
} as unknown as React.KeyboardEvent<HTMLDivElement>;

export const saveTemplateType = [
  { type: 'template' },
  { type: 'newTemplate' },
  { type: 'updateTemplate' },
];
