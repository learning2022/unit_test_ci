import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { ReactComponent as FolderIcon } from '../../../assets/Icons/folder.svg';
import { ReactComponent as LoaderIcon } from '../../../assets/Icons/loader.svg';
import { translate } from '../../../i18n/translate';

interface LoaderProps {
  manualSaving: boolean;
  closeLoaderModal: () => void;
  type: string;
  action: string;
  bodyContent?: string;
}

const LmsLoader = (props: LoaderProps) => {
  return (
    <Modal
      animation={false}
      show={props.manualSaving}
      onHide={props.closeLoaderModal}
      backdrop='static'
      centered
    >
      <Modal.Header closeButton>
        <h5 className='text-primary'>{props.action}</h5>
      </Modal.Header>
      <Modal.Body className={`${props.bodyContent !== '' ? 'mx-auto' : ''}`}>
        <div className='text-center'>
          <LoaderIcon className={`icon rotating ${props.bodyContent === '' ? 'mt-n2' : ''}`} />
          <FolderIcon
            className={
              props.type === 'global' || process.env.REACT_APP_A11Y_CHECKER_STATE !== 'true'
                ? 'lms-loader-folder-icon-global'
                : 'lms-loader-folder-icon'
            }
          />{' '}
        </div>
        <p className='my-2 mb-0 text-center font-weight-bold'>{props.action}</p>
        {props.type !== 'global' &&
          process.env.REACT_APP_A11Y_CHECKER_STATE === 'true' &&
          props.bodyContent !== '' && (
            <p className='text-center a11yCheckerInfo mt-3'>
              Note: We're performing an Accessibility Check in the background, we'll alert you via a
              numbered notification if we spot any issues.
            </p>
          )}
      </Modal.Body>
      <Modal.Footer className='mx-auto'>
        <Button
          variant='outline-primary'
          className='modal-footer-button mx-1 '
          onClick={props.closeLoaderModal}
        >
          {translate('global.cancel')}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
export default LmsLoader;
