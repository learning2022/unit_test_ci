import React, { useState, useLayoutEffect } from 'react';
import ReactDOM from 'react-dom';

import { A11yElementHighlighter } from '../../a11y-service';
import CONSTANTS from '../../loree-editor/constant';

export function A11yCheckerHighlightContainer() {
  const [portalRoot, setPortalRoot] = useState<Element>();

  useLayoutEffect(() => {
    const iframe: HTMLIFrameElement | null = document.getElementById(
      CONSTANTS.LOREE_IFRAME,
    ) as HTMLIFrameElement;
    if (!iframe) return;
    const loreeIframeDocument = iframe.contentDocument;

    const newPortalRoot = loreeIframeDocument?.body;
    if (newPortalRoot) {
      setPortalRoot(newPortalRoot);
    }
  }, [portalRoot]);

  if (!portalRoot) {
    return null;
  }

  return ReactDOM.createPortal(
    <div className='a11yContainer'>
      <A11yElementHighlighter positionStrategy='absolute' />
    </div>,
    portalRoot,
  );
}
