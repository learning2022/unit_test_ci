import React from 'react';
import Loree from '../../loree-editor/loree';
import { apm } from '@elastic/apm-rum';
import SaveIconComponent from './saveIcon/saveIcon';
import { Prompt } from 'react-router-dom';
import {
  updateStandaloneEditorContent,
  updateCanvasEditorContent,
  customBlockEditStatus,
  setEditMode,
  removePromptListener,
  handleAutoSaveOnCustomBlockAppend,
  autoSaveOff,
  browserPromptStatus,
  updateD2lEditorContent,
  updateBBEditorContent,
} from '../../utils/saveContent';
import CONSTANTS from '../../loree-editor/constant';
import { savePageAlert, savePageAlertContainerDisapper } from '../../loree-editor/alert';
import { CustomColors, CustomFonts } from '../../loree-editor/interface';
import { A11yCheckerContainer } from './A11yCheckerContainer';
import { a11yGlobalContext } from '../../a11y-service/context/a11y-context';
import { getEditorElementById } from '../../loree-editor/utils';
import { A11yCheckerHighlightContainer } from './A11yCheckerHighlightContainer';
import { getLoree, setLoree } from '.';
import ToastComponent from '../../lti/components/ToastComponent';
import { translate } from '../../i18n/translate';

let interval: number;
interface LoreeEditorProps {
  pageId: string;
  type: string;
  title: string;
  checkLMS: string;
  getTitleAndContent: () => void;
  customColor: CustomColors[];
  customFonts: CustomFonts[];
  customHeader: _Any;
  customLink: _Any;
  featuresList: _Any;
  d2lModulesContent?: _Any;
  d2lHeadContent: string;
  bbContentType?: _Any;
}

type LoreeEditorState = {
  manualSaving: boolean;
  loreeInitiated: boolean;
  showToast: boolean;
  toastMessage: string;
};

export default class LoreeEditor extends React.Component<LoreeEditorProps, LoreeEditorState> {
  state: LoreeEditorState = {
    manualSaving: false,
    loreeInitiated: false,
    showToast: false,
    toastMessage: '',
  };

  closeToast = () => {
    this.setState({ showToast: false });
  };

  componentDidMount() {
    try {
      const config = {
        header: {
          title: this.props.title,
        },
        customColor: { colors: this.props.customColor },
        customFonts: { fonts: this.props.customFonts },
        customHeaderStyle: { customHeader: this.props.customHeader },
        customLinkStyle: { customLink: this.props.customLink },
        features: this.props.featuresList,
      };
      setLoree(new Loree('loreeEditorWrapper'));
      getLoree().initiate(config as {});
      this.setState({ loreeInitiated: true });
      const validateEditor = this.props.checkLMS.split('/');
      if (validateEditor[1] === 'loree-editor')
        interval = window.setInterval(
          () => void this.saveEditorContent(),
          CONSTANTS.LOREE_AUTO_SAVE_INTERVAL,
        );
      handleAutoSaveOnCustomBlockAppend(false);
      window.addEventListener('beforeunload', (e) => {
        if (!browserPromptStatus) window.onbeforeunload = null;
        else this.initiateBrowserListener(e);
        this.removeCustomBlockAppendBtnOnRefresh();
      });
      setEditMode(false);
    } catch (e) {
      console.log('error fetching features...', e);
    }
  }

  removeCustomBlockAppendBtnOnRefresh = () => {
    const iFrameDoc = document.getElementById('loree-iframe') as HTMLIFrameElement;
    if (iFrameDoc?.contentDocument) {
      const cancelAppendBtn = iFrameDoc.contentDocument.getElementById(
        CONSTANTS.LOREE_CUSTOM_BLOCK_ADD_BUTTON,
      );
      if (cancelAppendBtn) {
        cancelAppendBtn.parentElement?.remove();
      }
    }
  };

  async componentWillUnmount() {
    await this.saveEditorContent();
    this.destroyEditor();
    clearInterval(interval); // trigger for autosave is commented till release
    window.removeEventListener('beforeunload', this.initiateBrowserListener);
  }

  initiateBrowserListener = (e: BeforeUnloadEvent) => {
    if (!removePromptListener) {
      e.preventDefault();
      // this.saveEditorContent();
      const confirmationMessage = 'Save changes';
      e.returnValue = confirmationMessage;
      return confirmationMessage;
    }
  };

  getContent = (): string => {
    return getLoree().getHtml();
  };

  saveEditorContent = async () => {
    this.clearAnchorPoppers();
    if (!customBlockEditStatus && !autoSaveOff) {
      const type = this.props.type;
      const saveLoader = document.getElementById('saving-loader');
      if (saveLoader) {
        saveLoader.classList.remove('d-none');
        const validateEditor = this.props.checkLMS.split('/');
        if (validateEditor[1] === 'loree-editor') {
          await updateStandaloneEditorContent(this.props.pageId);
        } else {
          await updateCanvasEditorContent(this.props.pageId, type);
        }
        setTimeout(() => {
          saveLoader.classList.add('d-none');
        }, CONSTANTS.LOREE_ALERT_SET_TIME_OUT);
      }
    }
  };

  saveContentManually = async () => {
    this.clearAnchorPoppers();
    const saveLoader = document.getElementById('content-saving-loader');
    if (saveLoader) saveLoader.classList.remove('d-none');
    this.setState({ manualSaving: true });
    try {
      await this.a11yCheckerNotification();
    } catch (err) {
      const toastMsg = translate('error.accessibilitychecker');
      apm.captureError(err as Error);
      this.setState({ showToast: true, toastMessage: toastMsg });
    }
    const type = this.props.type;
    const validateEditor = this.props.checkLMS.split('/');
    let response = null;
    if (validateEditor[1] === 'loree-editor') {
      response = await updateStandaloneEditorContent(this.props.pageId);
    } else {
      if (sessionStorage.getItem('domainName') === 'canvas') {
        response = await updateCanvasEditorContent(this.props.pageId, type);
      } else if (sessionStorage.getItem('domainName') === 'D2l') {
        response = await updateD2lEditorContent(
          this.props.pageId,
          type,
          this.props.d2lModulesContent,
          this.props.d2lHeadContent,
        );
      } else if (sessionStorage.getItem('domainName') === 'BB') {
        response = await updateBBEditorContent(
          this.props.pageId,
          this.props.title,
          this.props.bbContentType,
        );
      }
    }
    if (response) {
      this.setState({ manualSaving: false });
      if (saveLoader) saveLoader.classList.add('d-none');
      savePageAlert(
        translate('alert.savesuccessfullydetail', {
          detail:
            type === 'assignment' ? 'Assignment' : type === 'discussion' ? 'Discussion' : 'Page',
        }),
      );
      savePageAlertContainerDisapper();
    }
  };

  clearAnchorPoppers = (): void => {
    getLoree().hideAnchorOption();
  };

  a11yCheckerNotification = async () => {
    if (process.env.REACT_APP_A11Y_CHECKER_STATE !== 'true') {
      return;
    }
    const { setNotificationFlag, check, clear } = a11yGlobalContext.value;
    if (getEditorElementById(CONSTANTS.LOREE_MAIN_A11YCHECKER)) {
      await check(false);
    } else {
      setNotificationFlag(true);
      await clear();
    }
  };

  deleteContent = async () => {
    getLoree().deleteContent();
    await this.a11yCheckerNotification();
  };

  handleBrowserBack = (): string => {
    // this.saveEditorContent();
    return 'Changes that you made may not be saved. Are you sure ?';
  };

  destroyEditor = (): void => {
    getLoree().destroy();
    const loreeEditorWrapper = document.getElementById('loreeEditorWrapper');
    if (loreeEditorWrapper) {
      loreeEditorWrapper.innerHTML = '';
    }
  };

  closeLoaderModal = () => {
    this.setState({ manualSaving: false });
  };

  render() {
    let standalone = false;
    const validateEditor = this.props.checkLMS.split('/');
    if (validateEditor[1] === 'loree-editor') {
      standalone = true;
    }
    return (
      <>
        <Prompt message={() => this.handleBrowserBack()} />
        <div className='loreeEditorWrapper h-100' id='loreeEditorWrapper' />
        <SaveIconComponent
          getTitleAndContent={this.props.getTitleAndContent}
          saveEditorContent={this.saveEditorContent}
          clearAnchorPoppers={this.clearAnchorPoppers}
          saveContentManually={this.saveContentManually}
          deleteContent={this.deleteContent}
          getContent={this.getContent}
          standalone={standalone}
          manualSaving={this.state.manualSaving}
          closeLoaderModal={this.closeLoaderModal}
          featuresList={this.props.featuresList}
        />
        {this.state.showToast && (
          <ToastComponent
            showToast={this.state.showToast}
            toastMessage={this.state.toastMessage}
            closeToast={this.closeToast}
          />
        )}
        {this.state.loreeInitiated && <A11yCheckerContainer />}
        {this.state.loreeInitiated && <A11yCheckerHighlightContainer />}
      </>
    );
  }
}
