import { Subject } from 'rxjs';

const subject = new Subject();

export enum A11yEventType {
  close = 'CANCELLED',
  open = 'rulesExecuted',
}
export interface A11yStatusEvent {
  a11yIssueCount: number;
  eventType: A11yEventType;
}

export const a11yNotificationService = {
  sendMessage: (a11yStatusEvent: A11yStatusEvent) => subject.next(a11yStatusEvent),
  clearMessages: () => subject.next(),
  createObserver: () => subject.asObservable(),
};

export const a11yUpdateHighlightsService = {
  updateHighlightsIdentity: () => subject.next(),
  clearMessages: () => subject.next(),
  createObserver: () => subject.asObservable(),
};
