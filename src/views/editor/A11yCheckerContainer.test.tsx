import React from 'react';
import CONSTANTS from '../../loree-editor/constant';
import { A11yCheckerContainer } from './A11yCheckerContainer';
import * as a11y from '../../a11y-service/context/a11y-context';
import * as a11yContainer from '../../a11y-service/components/A11yElementHighlighter';
import * as a11yChecker from '../../a11y-service/components/A11yChecker';
import {
  mockUseA11yContext,
  a11yCheckerContainerMockData,
} from '../../loree-editor/a11y-client/quick-fixer/mockData';
import { configure, fireEvent, render, RenderResult, screen } from '@testing-library/react';
configure({ testIdAttribute: 'class' });

describe('#AcessibilityCheckerHighlighterHandleChange', () => {
  const activeAcTray = [
    ['impactModerate', 1],
    ['impactSerious', 0],
    ['impactCritical', 0],
  ];
  const activeElement = [
    [0, true],
    [1, false],
    [2, false],
  ];
  let elementHighlighters: HTMLCollection;
  beforeEach(() => {
    jest.clearAllMocks();
    const mock = jest.spyOn(a11y, 'useA11yContext');
    mock.mockImplementation(() => a11yCheckerContainerMockData);
    const domDiv = document.createElement('div');
    domDiv.id = CONSTANTS.LOREE_SPACE_A11Y_CHECKER;
    document.body.append(domDiv);
    render(<A11yCheckerContainer />);
    render(<a11yContainer.A11yElementHighlighter positionStrategy='absolute' />);
  });

  test('should render with aceesibility highlighters to notify the errors', () => {
    elementHighlighters = screen.queryAllByTestId('a11yContainer')[0].children;
    expect(elementHighlighters.length).toBe(3);
  });

  test.each(activeElement)('should render with the default active element', (index, selected) => {
    const element = elementHighlighters[index as number] as HTMLElement;
    expect(element.classList.contains('active')).toBe(selected);
  });

  test.each(activeAcTray)(
    'the selected highlighter and the pannel opend in the ac tray should be matched',
    (highliter, activeAcTray) => {
      const elements = screen.queryAllByTestId(highliter as string);
      expect(elements.length).toBe(activeAcTray);
    },
  );

  test('should open the actray which is relevant to particular highlighter when clicking on it', () => {
    const element = screen.queryAllByTestId('button impactSeverityButton')[1];
    fireEvent.click(element);
    expect(a11yCheckerContainerMockData.setActiveStep).toHaveBeenCalledTimes(1);
  });
});

describe('#AcessibilityCheckerContainer', () => {
  beforeEach(() => {
    const mock = jest.spyOn(a11y, 'useA11yContext');
    const mockElementHighlighter = jest.spyOn(a11yContainer, 'A11yElementHighlighter');
    const mockA11ychecker = jest.spyOn(a11yChecker, 'A11yChecker');
    mockElementHighlighter.mockImplementation(() => <div className='a11yElementHighlighter' />);
    mockA11ychecker.mockImplementation(() => <div className='a11yChecker' />);
    mock.mockImplementation(() => mockUseA11yContext);
  });
  describe(`should open the accesibility checker when the container exits`, () => {
    let checkerContainer: RenderResult;
    let highlighterContainer: RenderResult;
    beforeEach(() => {
      document.body.innerHTML = '';
      const domDiv = document.createElement('div');
      domDiv.id = CONSTANTS.LOREE_SPACE_A11Y_CHECKER;
      document.body.append(domDiv);
      checkerContainer = render(<A11yCheckerContainer />);
      highlighterContainer = render(
        <a11yContainer.A11yElementHighlighter positionStrategy='absolute' />,
      );
    });

    test('should render the ac highlighters on the elements when it contains error', () => {
      expect(highlighterContainer.getByTestId('a11yElementHighlighter')).toBeDefined();
    });
    test('should render the ac tray for the active element', () => {
      expect(checkerContainer.queryAllByTestId('a11yChecker').length).toBe(2);
    });
  });
  describe(`should not open the accesibility checker when the container doesn't exist`, () => {
    let wrapper: RenderResult;
    beforeEach(() => {
      const mock = jest.spyOn(a11y, 'useA11yContext');
      mockUseA11yContext.activeStep = -1;
      mockUseA11yContext.isProcessing = true;
      mock.mockImplementation(() => mockUseA11yContext);
      document.body.innerHTML = '';
      wrapper = render(<A11yCheckerContainer />);
    });

    test('should not render the ac highlighters on the elements when the container does not exists', () => {
      expect(wrapper.queryByTestId('a11yElementHighlighter')).toBeNull();
    });
    test('should not render the ac tray when the container does not exists', () => {
      expect(wrapper.queryByTestId('a11yChecker')).toBeNull();
    });
  });
});
