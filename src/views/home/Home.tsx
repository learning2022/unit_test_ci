/* eslint-disable */ // Remove this line when editing this file
import React, { useState } from 'react';
import { Container, Form, Button, Card, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen, faFile, faWindowRestore, faPhotoVideo } from '@fortawesome/free-solid-svg-icons';
import Header from './header/header';
import './Home.scss';
import SignUp from '../../components/auth/signUp';
import SignIn from '../../components/auth/signIn';

const Home: React.FC = () => {
  const [signIn, setSignIn] = useState(false);

  const handleModal = (): void => {
    setSignIn(!signIn);
  };
  return (
    <React.Fragment>
      <section id='loree-welcome-section'>
        <Header handleModal={handleModal} signInState={signIn} />
        <div className='loree-auth-section'>
          <div className='loree-auth-modal'>{signIn ? <SignIn handleModal={handleModal} /> : <SignUp />}</div>
        </div>
      </section>

      <section id='features' className='services section-bg'>
        <Container>
          <div className='section-title'>
            <h2>It’s time to get features that work for you.</h2>
            <p>
              Loree helps you to manages learning design across thousands of course variations. You can quickly customise create and collect
              a library high-impact learning templates that engage with your learners every time.
            </p>
          </div>

          <div className='row'>
            <div className='col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0'>
              <div className='icon-box icon-box-pink'>
                <div className='icon'>
                  <FontAwesomeIcon icon={faPen} />
                </div>
                <h4 className='title'>W.Y.S.I.W.Y.G.</h4>
                <p className='description'>
                  An acronym for “What you see is what you get” which means any editing you do shows up in the final Canvas course format.
                </p>
              </div>
            </div>

            <div className='col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0'>
              <div className='icon-box icon-box-cyan'>
                <div className='icon'>
                  <FontAwesomeIcon icon={faFile} />
                </div>
                <h4 className='title'>Screen Templates</h4>
                <p className='description'>
                  Creating highly interactive screens is simplicity itself. Pick a pre-configured template and add all of the items, then
                  hit publish.
                </p>
              </div>
            </div>

            <div className='col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0'>
              <div className='icon-box icon-box-green'>
                <div className='icon'>
                  <FontAwesomeIcon icon={faWindowRestore} />
                </div>
                <h4 className='title'>Responsive Design</h4>
                <p className='description'>
                  Loree courses look great on all devices: desktops, tablets and smartphones.Don’t worry about cross-device compatibility.
                  We’ve got that covered.
                </p>
              </div>
            </div>

            <div className='col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0'>
              <div className='icon-box icon-box-blue'>
                <div className='icon'>
                  <FontAwesomeIcon icon={faPhotoVideo} />
                </div>
                <h4 className='title'>Media Galleries</h4>
                <p className='description'>
                  Easily upload and access your images, videos and audio files on the go. Loree also integrates with ARC Media giving you
                  more access where ever you are.
                </p>
              </div>
            </div>
          </div>
        </Container>
      </section>

      <section id='demo' className='testimonials'>
        <Container>
          <div className='section-title'>
            <h2>Want To See Loree In Action?</h2>
            <p>
              Sign up and get Loree sandbox access within single click with just your name, role email and the name of your organisation.
              It’s as simple as that — no payments or credit card information required.
            </p>
            <Button variant='primary' className='mt-4'>
              Try Now
            </Button>
          </div>
        </Container>
      </section>

      <section id='pricing' className='pricing section-bg'>
        <Container>
          <div className='section-title'>
            <h2>Intrigued About Pricing?</h2>
            <p>
              Users and organisations are all different and &apos;one size fits all&apos; pricing isn&#39;t always the right match. We like
              to hear your needs first and then, work together to find the right package for you. Let’s chat!
            </p>
          </div>

          <div className='row'>
            <div className='col-lg-3 col-md-6'>
              <div className='box'>
                <h3>Free</h3>
                <h4>
                  <sup>$</sup>0<span> / month</span>
                </h4>
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li className='na'>Pharetra massa</li>
                  <li className='na'>Massa ultricies mi</li>
                </ul>
                <div className='btn-wrap'>
                  <Button variant='primary'>Try Now</Button>
                </div>
              </div>
            </div>

            <div className='col-lg-3 col-md-6 mt-4 mt-md-0'>
              <div className='box featured'>
                <h3>Basic</h3>
                <h4>
                  <sup>$</sup>19<span> / month</span>
                </h4>
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li>Pharetra massa</li>
                  <li className='na'>Massa ultricies mi</li>
                </ul>
                <div className='btn-wrap'>
                  <Button variant='primary'>Buy Now</Button>
                </div>
              </div>
            </div>

            <div className='col-lg-3 col-md-6 mt-4 mt-lg-0'>
              <div className='box'>
                <h3>Optimal</h3>
                <h4>
                  <sup>$</sup>29<span> / month</span>
                </h4>
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li>Pharetra massa</li>
                  <li>Massa ultricies mi</li>
                </ul>
                <div className='btn-wrap'>
                  <Button variant='primary'>Buy Now</Button>
                </div>
              </div>
            </div>

            <div className='col-lg-3 col-md-6 mt-4 mt-lg-0'>
              <div className='box'>
                <h3>Premium</h3>
                <h4>
                  <sup>$</sup>$$<span> / month</span>
                </h4>
                <ul>
                  <li>Aida dere</li>
                  <li>Nec feugiat nisl</li>
                  <li>Nulla at volutpat dola</li>
                  <li>Pharetra massa</li>
                  <li>Massa ultricies mi</li>
                </ul>
                <div className='btn-wrap'>
                  <Button variant='primary'>Contact Us</Button>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </section>

      <section id='tour' className='guided-demo'>
        <Container>
          <div className='section-title'>
            <h2>Brilliant Design Awaits</h2>
            <p>what you can accomplish with the power of Loree at your fingertips.</p>
            <Button variant='primary' className='mt-4'>
              Start your Guided Tour
            </Button>
          </div>
        </Container>
      </section>

      <section id='contact' className='contact section-bg'>
        <Container>
          <div className='section-title'>
            <h2>The Best Course Design Starts With Loree.</h2>
          </div>
          <Card>
            <Form>
              <Card.Body>
                <Row>
                  <Col>
                    <Form.Group controlId='firstName'>
                      <Form.Label>First Name</Form.Label>
                      <Form.Control type='text' />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group controlId='lastName'>
                      <Form.Label>Last Name</Form.Label>
                      <Form.Control type='text' />
                    </Form.Group>
                  </Col>
                </Row>
                <Form.Group controlId='formBasicEmail'>
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type='email' />
                  <Form.Text className='text-muted'>We will never share your email with anyone else.</Form.Text>
                </Form.Group>
                <Form.Group controlId='formBasicPassword'>
                  <Form.Label>Comment</Form.Label>
                  <Form.Control as='textarea' rows={3} />
                </Form.Group>
              </Card.Body>
              <Card.Footer className='text-center border-0'>
                <Button variant='primary' type='submit'>
                  Submit
                </Button>
              </Card.Footer>
            </Form>
          </Card>
        </Container>
      </section>

      <footer>
        <Container>
          <div className='copyright'>
            &copy; 2020
            <strong>
              <span> Crystaldelta</span>
            </strong>
            . All Rights Reserved
          </div>
          <div className='credits'>
            <a href='https://crystaldelta.com/'>CrystaldeltaProduct</a>
          </div>
        </Container>
      </footer>
    </React.Fragment>
  );
};

export default Home;
