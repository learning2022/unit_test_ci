/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import logo from '../../../assets/images/loree-header-logo.png';
interface HeaderProps {
  handleModal?: () => void;
  signInState?: boolean;
}

const Header: React.FC<HeaderProps> = (props: HeaderProps) => {
  return (
    <Navbar collapseOnSelect expand='lg'>
      <Navbar.Brand href='/'>
        <img alt='Loree Logo' src={logo} width='60' height='60' className='d-inline-block align-top' />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='responsive-navbar-nav' />
      <Navbar.Collapse id='responsive-navbar-nav'>
        <Nav className='mr-auto'></Nav>
        <Nav variant='pills' activeKey='1'>
          <Nav.Item>
            {!props.signInState ? (
              <Nav.Link eventKey='1' className={`loree-secondary-btn`} onClick={props.handleModal} data-testid='authbutton'>
                Login
              </Nav.Link>
            ) : (
              ''
            )}
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header;
