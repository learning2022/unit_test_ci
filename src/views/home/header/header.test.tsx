/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header';
import { mount } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';
import SignIn from '../../../components/auth/signIn';

const HeaderComp: any = mount(
  <Router>
    <Header />
  </Router>,
);

describe('describe home', () => {
  it('renders fully without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(HeaderComp, div);
  });

  it('render with 3 divs', () => {
    expect(HeaderComp.find('div')).toHaveLength(4);
  });

  it('render with nav', () => {
    expect(HeaderComp.find('nav')).toHaveLength(1);
  });

  it('render with nav classes', () => {
    expect(HeaderComp.find('.navbar')).toHaveLength(1);
    expect(HeaderComp.find('.navbar-brand')).toHaveLength(1);
    expect(HeaderComp.find('.navbar-expand-lg')).toHaveLength(1);
  });

  it('render sign in with a tag', () => {
    const signIn = mount(
      <Router>
        <SignIn />
      </Router>,
    );
    expect(signIn.find('a')).toHaveLength(2);
  });
});
