/* eslint-disable */ // Remove this line when editing this file
import React from 'react';
import { render } from '@testing-library/react';
import Home from './Home';
import { shallow } from 'enzyme';

describe('renders home', () => {
  it('renders home without crashing', () => {
    shallow(<Home />);
  });

  it('renders features text', () => {
    const { getByText } = render(<Home />);
    const headerElement = getByText(/features/i);
    expect(headerElement).toBeInTheDocument();
  });
});
