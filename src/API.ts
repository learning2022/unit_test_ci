/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateCategoryInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  name: string,
};

export type ModelCategoryConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  and?: Array< ModelCategoryConditionInput | null > | null,
  or?: Array< ModelCategoryConditionInput | null > | null,
  not?: ModelCategoryConditionInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type Category = {
  __typename: "Category",
  id?: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  name?: string,
  projects?: ModelProjectConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type ModelProjectConnection = {
  __typename: "ModelProjectConnection",
  items?:  Array<Project >,
  nextToken?: string | null,
};

export type Project = {
  __typename: "Project",
  id?: string,
  title?: string,
  categoryID?: string,
  category?: Category,
  pages?: ModelPageConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type ModelPageConnection = {
  __typename: "ModelPageConnection",
  items?:  Array<Page >,
  nextToken?: string | null,
};

export type Page = {
  __typename: "Page",
  id?: string,
  projectID?: string,
  project?: Project,
  title?: string,
  content?: string | null,
  state?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateCategoryInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  name?: string | null,
};

export type DeleteCategoryInput = {
  id: string,
};

export type CreateProjectInput = {
  id?: string | null,
  title: string,
  categoryID: string,
};

export type ModelProjectConditionInput = {
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  and?: Array< ModelProjectConditionInput | null > | null,
  or?: Array< ModelProjectConditionInput | null > | null,
  not?: ModelProjectConditionInput | null,
};

export type UpdateProjectInput = {
  id: string,
  title?: string | null,
  categoryID?: string | null,
};

export type DeleteProjectInput = {
  id: string,
};

export type CreatePageInput = {
  id?: string | null,
  projectID: string,
  title: string,
  content?: string | null,
  state?: string | null,
};

export type ModelPageConditionInput = {
  projectID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  content?: ModelStringInput | null,
  state?: ModelStringInput | null,
  and?: Array< ModelPageConditionInput | null > | null,
  or?: Array< ModelPageConditionInput | null > | null,
  not?: ModelPageConditionInput | null,
};

export type UpdatePageInput = {
  id: string,
  projectID?: string | null,
  title?: string | null,
  content?: string | null,
  state?: string | null,
};

export type DeletePageInput = {
  id: string,
};

export type CreateCustomTemplateInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title: string,
  categoryID: string,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  isGlobal?: boolean | null,
  isShared?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  owner?: string | null,
};

export type S3ObjectInput = {
  bucket: string,
  key: string,
  region: string,
};

export type ModelCustomTemplateConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  isGlobal?: ModelBooleanInput | null,
  isShared?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelCustomTemplateConditionInput | null > | null,
  or?: Array< ModelCustomTemplateConditionInput | null > | null,
  not?: ModelCustomTemplateConditionInput | null,
};

export type ModelBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type CustomTemplate = {
  __typename: "CustomTemplate",
  id?: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title?: string,
  categoryID?: string,
  category?: Category,
  thumbnail?: S3Object,
  content?: string | null,
  status?: boolean | null,
  isGlobal?: boolean | null,
  isShared?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  owner?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type S3Object = {
  __typename: "S3Object",
  bucket?: string,
  key?: string,
  region?: string,
};

export type UpdateCustomTemplateInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title?: string | null,
  categoryID?: string | null,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  isGlobal?: boolean | null,
  isShared?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  owner?: string | null,
};

export type DeleteCustomTemplateInput = {
  id: string,
};

export type CreateGlobalTemplatesInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customTemplateID: string,
  title: string,
  categoryID: string,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type ModelGlobalTemplatesConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  customTemplateID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelGlobalTemplatesConditionInput | null > | null,
  or?: Array< ModelGlobalTemplatesConditionInput | null > | null,
  not?: ModelGlobalTemplatesConditionInput | null,
};

export type GlobalTemplates = {
  __typename: "GlobalTemplates",
  id?: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customTemplateID?: string,
  title?: string,
  categoryID?: string,
  category?: Category,
  thumbnail?: S3Object,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateGlobalTemplatesInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customTemplateID?: string | null,
  title?: string | null,
  categoryID?: string | null,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type DeleteGlobalTemplatesInput = {
  id: string,
};

export type CreateSharedTemplatesInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title: string,
  customTemplateID: string,
  sharedAccountId: string,
  categoryID: string,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type ModelSharedTemplatesConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  customTemplateID?: ModelStringInput | null,
  sharedAccountId?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelSharedTemplatesConditionInput | null > | null,
  or?: Array< ModelSharedTemplatesConditionInput | null > | null,
  not?: ModelSharedTemplatesConditionInput | null,
};

export type SharedTemplates = {
  __typename: "SharedTemplates",
  id?: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title?: string,
  customTemplateID?: string,
  sharedAccountId?: string,
  categoryID?: string,
  category?: Category,
  thumbnail?: S3Object,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateSharedTemplatesInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title?: string | null,
  customTemplateID?: string | null,
  sharedAccountId?: string | null,
  categoryID?: string | null,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type DeleteSharedTemplatesInput = {
  id: string,
};

export type CreateFileInput = {
  id?: string | null,
  name: string,
  size?: string | null,
  location?: S3ObjectInput | null,
  type?: string | null,
  thumbnail?: S3ObjectInput | null,
};

export type ModelFileConditionInput = {
  name?: ModelStringInput | null,
  size?: ModelStringInput | null,
  type?: ModelStringInput | null,
  and?: Array< ModelFileConditionInput | null > | null,
  or?: Array< ModelFileConditionInput | null > | null,
  not?: ModelFileConditionInput | null,
};

export type File = {
  __typename: "File",
  id?: string,
  name?: string,
  size?: string | null,
  location?: S3Object,
  type?: string | null,
  thumbnail?: S3Object,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateFileInput = {
  id: string,
  name?: string | null,
  size?: string | null,
  location?: S3ObjectInput | null,
  type?: string | null,
  thumbnail?: S3ObjectInput | null,
};

export type DeleteFileInput = {
  id: string,
};

export type CreateLtiPlatformInput = {
  id?: string | null,
  clientId: string,
  clientSecret?: string | null,
  platformName?: string | null,
  platformUrl?: string | null,
  authEndpoint?: string | null,
  accesstokenEndpoint?: string | null,
  publicKeyURL?: string | null,
  authConfig?: AuthDetailInput | null,
  googleAnalyticsTrackingId?: string | null,
  ltiDeploymentId?: string | null,
  loreeOrganisationID?: string | null,
};

export type AuthDetailInput = {
  method?: string | null,
  key?: string | null,
};

export type ModelLtiPlatformConditionInput = {
  clientId?: ModelStringInput | null,
  clientSecret?: ModelStringInput | null,
  platformName?: ModelStringInput | null,
  platformUrl?: ModelStringInput | null,
  authEndpoint?: ModelStringInput | null,
  accesstokenEndpoint?: ModelStringInput | null,
  publicKeyURL?: ModelStringInput | null,
  googleAnalyticsTrackingId?: ModelStringInput | null,
  ltiDeploymentId?: ModelStringInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  and?: Array< ModelLtiPlatformConditionInput | null > | null,
  or?: Array< ModelLtiPlatformConditionInput | null > | null,
  not?: ModelLtiPlatformConditionInput | null,
};

export type LtiPlatform = {
  __typename: "LtiPlatform",
  id?: string,
  clientId?: string,
  clientSecret?: string | null,
  platformName?: string | null,
  platformUrl?: string | null,
  authEndpoint?: string | null,
  accesstokenEndpoint?: string | null,
  publicKeyURL?: string | null,
  authConfig?: AuthDetail,
  googleAnalyticsTrackingId?: string | null,
  ltiDeploymentId?: string | null,
  loreeOrganisationID?: string | null,
  loreeOrganisation?: LoreeOrganisation,
  ltiApiKey?: ModelLtiApiKeyConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type AuthDetail = {
  __typename: "AuthDetail",
  method?: string | null,
  key?: string | null,
};

export type LoreeOrganisation = {
  __typename: "LoreeOrganisation",
  id?: string,
  name?: string | null,
  ltiPlatforms?: ModelLtiPlatformConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type ModelLtiPlatformConnection = {
  __typename: "ModelLtiPlatformConnection",
  items?:  Array<LtiPlatform >,
  nextToken?: string | null,
};

export type ModelLtiApiKeyConnection = {
  __typename: "ModelLtiApiKeyConnection",
  items?:  Array<LtiApiKey >,
  nextToken?: string | null,
};

export type LtiApiKey = {
  __typename: "LtiApiKey",
  id?: string,
  ltiPlatformID?: string,
  loreeOrganisationID?: string | null,
  ltiClientID?: string | null,
  oauthLoginUrl?: string | null,
  oauthTokenUrl?: string | null,
  apiClientId?: string | null,
  apiSecretKey?: string | null,
  lmsApiUrl?: string | null,
  ltiAccessToken?: ModelLtiAccessTokenConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type ModelLtiAccessTokenConnection = {
  __typename: "ModelLtiAccessTokenConnection",
  items?:  Array<LtiAccessToken >,
  nextToken?: string | null,
};

export type LtiAccessToken = {
  __typename: "LtiAccessToken",
  id?: string,
  ltiApiKeyID?: string,
  ltiClientID?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  loreeUsername?: string | null,
  loreePassword?: string | null,
  user?: string | null,
  userInfo?: string | null,
  roles?: string | null,
  accessToken?: string | null,
  refreshToken?: string | null,
  isAdmin?: boolean | null,
  generatedAt?: string | null,
  lmsApiUrl?: string | null,
  expiresAt?: string | null,
  lms_email?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLtiPlatformInput = {
  id: string,
  clientId?: string | null,
  clientSecret?: string | null,
  platformName?: string | null,
  platformUrl?: string | null,
  authEndpoint?: string | null,
  accesstokenEndpoint?: string | null,
  publicKeyURL?: string | null,
  authConfig?: AuthDetailInput | null,
  googleAnalyticsTrackingId?: string | null,
  ltiDeploymentId?: string | null,
  loreeOrganisationID?: string | null,
};

export type DeleteLtiPlatformInput = {
  id: string,
};

export type CreateLtiApiKeyInput = {
  id?: string | null,
  ltiPlatformID: string,
  loreeOrganisationID?: string | null,
  ltiClientID?: string | null,
  oauthLoginUrl?: string | null,
  oauthTokenUrl?: string | null,
  apiClientId?: string | null,
  apiSecretKey?: string | null,
  lmsApiUrl?: string | null,
};

export type ModelLtiApiKeyConditionInput = {
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  ltiClientID?: ModelIDInput | null,
  oauthLoginUrl?: ModelStringInput | null,
  oauthTokenUrl?: ModelStringInput | null,
  apiClientId?: ModelStringInput | null,
  apiSecretKey?: ModelStringInput | null,
  lmsApiUrl?: ModelStringInput | null,
  and?: Array< ModelLtiApiKeyConditionInput | null > | null,
  or?: Array< ModelLtiApiKeyConditionInput | null > | null,
  not?: ModelLtiApiKeyConditionInput | null,
};

export type UpdateLtiApiKeyInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  ltiClientID?: string | null,
  oauthLoginUrl?: string | null,
  oauthTokenUrl?: string | null,
  apiClientId?: string | null,
  apiSecretKey?: string | null,
  lmsApiUrl?: string | null,
};

export type DeleteLtiApiKeyInput = {
  id: string,
};

export type CreateLtiPlatformKeyInput = {
  id?: string | null,
  ltiPlatformID: string,
  kid?: string | null,
  privatekey?: string | null,
  publicKey?: string | null,
};

export type ModelLtiPlatformKeyConditionInput = {
  ltiPlatformID?: ModelIDInput | null,
  kid?: ModelStringInput | null,
  privatekey?: ModelStringInput | null,
  publicKey?: ModelStringInput | null,
  and?: Array< ModelLtiPlatformKeyConditionInput | null > | null,
  or?: Array< ModelLtiPlatformKeyConditionInput | null > | null,
  not?: ModelLtiPlatformKeyConditionInput | null,
};

export type LtiPlatformKey = {
  __typename: "LtiPlatformKey",
  id?: string,
  ltiPlatformID?: string,
  kid?: string | null,
  privatekey?: string | null,
  publicKey?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLtiPlatformKeyInput = {
  id: string,
  ltiPlatformID?: string | null,
  kid?: string | null,
  privatekey?: string | null,
  publicKey?: string | null,
};

export type DeleteLtiPlatformKeyInput = {
  id: string,
};

export type CreateLtiAccessTokenInput = {
  id?: string | null,
  ltiApiKeyID: string,
  ltiClientID?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  loreeUsername?: string | null,
  loreePassword?: string | null,
  user?: string | null,
  userInfo?: string | null,
  roles?: string | null,
  accessToken?: string | null,
  refreshToken?: string | null,
  isAdmin?: boolean | null,
  generatedAt?: string | null,
  lmsApiUrl?: string | null,
  expiresAt?: string | null,
  lms_email?: string | null,
};

export type ModelLtiAccessTokenConditionInput = {
  ltiApiKeyID?: ModelIDInput | null,
  ltiClientID?: ModelStringInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  loreeUsername?: ModelStringInput | null,
  loreePassword?: ModelStringInput | null,
  user?: ModelStringInput | null,
  userInfo?: ModelStringInput | null,
  roles?: ModelStringInput | null,
  accessToken?: ModelStringInput | null,
  refreshToken?: ModelStringInput | null,
  isAdmin?: ModelBooleanInput | null,
  generatedAt?: ModelStringInput | null,
  lmsApiUrl?: ModelStringInput | null,
  expiresAt?: ModelStringInput | null,
  lms_email?: ModelStringInput | null,
  and?: Array< ModelLtiAccessTokenConditionInput | null > | null,
  or?: Array< ModelLtiAccessTokenConditionInput | null > | null,
  not?: ModelLtiAccessTokenConditionInput | null,
};

export type UpdateLtiAccessTokenInput = {
  id: string,
  ltiApiKeyID?: string | null,
  ltiClientID?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  loreeUsername?: string | null,
  loreePassword?: string | null,
  user?: string | null,
  userInfo?: string | null,
  roles?: string | null,
  accessToken?: string | null,
  refreshToken?: string | null,
  isAdmin?: boolean | null,
  generatedAt?: string | null,
  lmsApiUrl?: string | null,
  expiresAt?: string | null,
  lms_email?: string | null,
};

export type DeleteLtiAccessTokenInput = {
  id: string,
};

export type CreateLtiLogInput = {
  id: string,
  ltiPlatformID: string,
  userInfo?: string | null,
  clientInfo?: string | null,
  userGeoLocation?: string | null,
  userAction?: string | null,
  generatedAt?: string | null,
};

export type ModelLtiLogConditionInput = {
  userInfo?: ModelStringInput | null,
  clientInfo?: ModelStringInput | null,
  userGeoLocation?: ModelStringInput | null,
  userAction?: ModelStringInput | null,
  generatedAt?: ModelStringInput | null,
  and?: Array< ModelLtiLogConditionInput | null > | null,
  or?: Array< ModelLtiLogConditionInput | null > | null,
  not?: ModelLtiLogConditionInput | null,
};

export type LtiLog = {
  __typename: "LtiLog",
  id?: string,
  ltiPlatformID?: string,
  userInfo?: string | null,
  clientInfo?: string | null,
  userGeoLocation?: string | null,
  userAction?: string | null,
  generatedAt?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLtiLogInput = {
  id: string,
  ltiPlatformID?: string | null,
  userInfo?: string | null,
  clientInfo?: string | null,
  userGeoLocation?: string | null,
  userAction?: string | null,
  generatedAt?: string | null,
};

export type DeleteLtiLogInput = {
  id: string,
};

export type CreateCustomBlockInput = {
  id?: string | null,
  title: string,
  categoryID: string,
  ltiPlatformID: string,
  loreeOrganisationID?: string | null,
  type: string,
  content?: string | null,
  thumbnail?: S3ObjectInput | null,
  status?: boolean | null,
  isGlobal?: boolean | null,
  isShared?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  owner?: string | null,
};

export type ModelCustomBlockConditionInput = {
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  type?: ModelStringInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  isGlobal?: ModelBooleanInput | null,
  isShared?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelCustomBlockConditionInput | null > | null,
  or?: Array< ModelCustomBlockConditionInput | null > | null,
  not?: ModelCustomBlockConditionInput | null,
};

export type CustomBlock = {
  __typename: "CustomBlock",
  id?: string,
  title?: string,
  categoryID?: string,
  ltiPlatformID?: string,
  loreeOrganisationID?: string | null,
  category?: Category,
  type?: string,
  content?: string | null,
  thumbnail?: S3Object,
  status?: boolean | null,
  isGlobal?: boolean | null,
  isShared?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  owner?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateCustomBlockInput = {
  id: string,
  title?: string | null,
  categoryID?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  type?: string | null,
  content?: string | null,
  thumbnail?: S3ObjectInput | null,
  status?: boolean | null,
  isGlobal?: boolean | null,
  isShared?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  owner?: string | null,
};

export type DeleteCustomBlockInput = {
  id: string,
};

export type CreateGlobalBlocksInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customBlockID: string,
  title: string,
  type: string,
  categoryID: string,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type ModelGlobalBlocksConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  customBlockID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  type?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelGlobalBlocksConditionInput | null > | null,
  or?: Array< ModelGlobalBlocksConditionInput | null > | null,
  not?: ModelGlobalBlocksConditionInput | null,
};

export type GlobalBlocks = {
  __typename: "GlobalBlocks",
  id?: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customBlockID?: string,
  title?: string,
  type?: string,
  categoryID?: string,
  category?: Category,
  thumbnail?: S3Object,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateGlobalBlocksInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customBlockID?: string | null,
  title?: string | null,
  type?: string | null,
  categoryID?: string | null,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type DeleteGlobalBlocksInput = {
  id: string,
};

export type CreateSharedBlocksInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title: string,
  type: string,
  customBlockID?: string | null,
  categoryID: string,
  sharedAccountId: string,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type ModelSharedBlocksConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  type?: ModelStringInput | null,
  customBlockID?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  sharedAccountId?: ModelStringInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelSharedBlocksConditionInput | null > | null,
  or?: Array< ModelSharedBlocksConditionInput | null > | null,
  not?: ModelSharedBlocksConditionInput | null,
};

export type SharedBlocks = {
  __typename: "SharedBlocks",
  id?: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title?: string,
  type?: string,
  customBlockID?: string | null,
  categoryID?: string,
  sharedAccountId?: string,
  category?: Category,
  thumbnail?: S3Object,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateSharedBlocksInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  title?: string | null,
  type?: string | null,
  customBlockID?: string | null,
  categoryID?: string | null,
  sharedAccountId?: string | null,
  thumbnail?: S3ObjectInput | null,
  content?: string | null,
  status?: boolean | null,
  createdBy?: string | null,
  active?: boolean | null,
};

export type DeleteSharedBlocksInput = {
  id: string,
};

export type CreateCustomStyleInput = {
  id?: string | null,
  ltiPlatformID: string,
  loreeOrganisationID?: string | null,
  customColor?: string | null,
  customHeader?: string | null,
  customFont?: string | null,
  customLink?: string | null,
};

export type ModelCustomStyleConditionInput = {
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  customColor?: ModelStringInput | null,
  customHeader?: ModelStringInput | null,
  customFont?: ModelStringInput | null,
  customLink?: ModelStringInput | null,
  and?: Array< ModelCustomStyleConditionInput | null > | null,
  or?: Array< ModelCustomStyleConditionInput | null > | null,
  not?: ModelCustomStyleConditionInput | null,
};

export type CustomStyle = {
  __typename: "CustomStyle",
  id?: string,
  ltiPlatformID?: string,
  loreeOrganisationID?: string | null,
  customColor?: string | null,
  customHeader?: string | null,
  customFont?: string | null,
  customLink?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateCustomStyleInput = {
  id: string,
  ltiPlatformID?: string | null,
  loreeOrganisationID?: string | null,
  customColor?: string | null,
  customHeader?: string | null,
  customFont?: string | null,
  customLink?: string | null,
};

export type DeleteCustomStyleInput = {
  id: string,
};

export type CreateKalturaConfigInput = {
  id?: string | null,
  ltiPlatformID: string,
  partnerID?: string | null,
  appToken?: string | null,
  tokenID?: string | null,
};

export type ModelKalturaConfigConditionInput = {
  partnerID?: ModelStringInput | null,
  appToken?: ModelStringInput | null,
  tokenID?: ModelStringInput | null,
  and?: Array< ModelKalturaConfigConditionInput | null > | null,
  or?: Array< ModelKalturaConfigConditionInput | null > | null,
  not?: ModelKalturaConfigConditionInput | null,
};

export type KalturaConfig = {
  __typename: "KalturaConfig",
  id?: string,
  ltiPlatformID?: string,
  partnerID?: string | null,
  appToken?: string | null,
  tokenID?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateKalturaConfigInput = {
  id: string,
  ltiPlatformID?: string | null,
  partnerID?: string | null,
  appToken?: string | null,
  tokenID?: string | null,
};

export type DeleteKalturaConfigInput = {
  id: string,
};

export type CreateLoreeFeatureInput = {
  id?: string | null,
  ltiPlatformID: string,
  featureList?: string | null,
};

export type ModelLoreeFeatureConditionInput = {
  featureList?: ModelStringInput | null,
  and?: Array< ModelLoreeFeatureConditionInput | null > | null,
  or?: Array< ModelLoreeFeatureConditionInput | null > | null,
  not?: ModelLoreeFeatureConditionInput | null,
};

export type LoreeFeature = {
  __typename: "LoreeFeature",
  id?: string,
  ltiPlatformID?: string,
  featureList?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLoreeFeatureInput = {
  id: string,
  ltiPlatformID?: string | null,
  featureList?: string | null,
};

export type DeleteLoreeFeatureInput = {
  id: string,
};

export type CreateLoreeRoleInput = {
  id?: string | null,
  ltiPlatformID: string,
  name?: string | null,
  description?: string | null,
  featureList?: string | null,
};

export type ModelLoreeRoleConditionInput = {
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  featureList?: ModelStringInput | null,
  and?: Array< ModelLoreeRoleConditionInput | null > | null,
  or?: Array< ModelLoreeRoleConditionInput | null > | null,
  not?: ModelLoreeRoleConditionInput | null,
};

export type LoreeRole = {
  __typename: "LoreeRole",
  id?: string,
  ltiPlatformID?: string,
  name?: string | null,
  description?: string | null,
  featureList?: string | null,
  lmsLoreeRoles?: ModelLmsLoreeRoleConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type ModelLmsLoreeRoleConnection = {
  __typename: "ModelLmsLoreeRoleConnection",
  items?:  Array<LmsLoreeRole >,
  nextToken?: string | null,
};

export type LmsLoreeRole = {
  __typename: "LmsLoreeRole",
  id?: string,
  ltiPlatformID?: string,
  loreeRoleID?: LoreeRole,
  lmsRole?: string | null,
  lmsBaseRoleType?: string | null,
  lmsRoleId?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLoreeRoleInput = {
  id: string,
  ltiPlatformID?: string | null,
  name?: string | null,
  description?: string | null,
  featureList?: string | null,
};

export type DeleteLoreeRoleInput = {
  id: string,
};

export type CreateLmsLoreeRoleInput = {
  id?: string | null,
  ltiPlatformID: string,
  lmsRole?: string | null,
  lmsBaseRoleType?: string | null,
  lmsRoleId?: string | null,
  lmsLoreeRoleLoreeRoleIDId?: string | null,
};

export type ModelLmsLoreeRoleConditionInput = {
  lmsRole?: ModelStringInput | null,
  lmsBaseRoleType?: ModelStringInput | null,
  lmsRoleId?: ModelStringInput | null,
  and?: Array< ModelLmsLoreeRoleConditionInput | null > | null,
  or?: Array< ModelLmsLoreeRoleConditionInput | null > | null,
  not?: ModelLmsLoreeRoleConditionInput | null,
};

export type UpdateLmsLoreeRoleInput = {
  id: string,
  ltiPlatformID?: string | null,
  lmsRole?: string | null,
  lmsBaseRoleType?: string | null,
  lmsRoleId?: string | null,
  lmsLoreeRoleLoreeRoleIDId?: string | null,
};

export type DeleteLmsLoreeRoleInput = {
  id: string,
};

export type CreateHFivePUsersInput = {
  id?: string | null,
  email: string,
  loreeUser: string,
  ltiPlatformID: string,
};

export type ModelHFivePUsersConditionInput = {
  email?: ModelStringInput | null,
  loreeUser?: ModelStringInput | null,
  and?: Array< ModelHFivePUsersConditionInput | null > | null,
  or?: Array< ModelHFivePUsersConditionInput | null > | null,
  not?: ModelHFivePUsersConditionInput | null,
};

export type HFivePUsers = {
  __typename: "HFivePUsers",
  id?: string,
  email?: string,
  loreeUser?: string,
  ltiPlatformID?: string,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateHFivePUsersInput = {
  id: string,
  email?: string | null,
  loreeUser?: string | null,
  ltiPlatformID?: string | null,
};

export type DeleteHFivePUsersInput = {
  id: string,
};

export type CreateInteractivesMetaDataInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  metaKey: string,
  metaValue?: string | null,
  isRetired?: boolean | null,
};

export type ModelInteractivesMetaDataConditionInput = {
  metaKey?: ModelStringInput | null,
  metaValue?: ModelStringInput | null,
  isRetired?: ModelBooleanInput | null,
  and?: Array< ModelInteractivesMetaDataConditionInput | null > | null,
  or?: Array< ModelInteractivesMetaDataConditionInput | null > | null,
  not?: ModelInteractivesMetaDataConditionInput | null,
};

export type InteractivesMetaData = {
  __typename: "InteractivesMetaData",
  id?: string,
  ltiPlatformID?: string | null,
  metaKey?: string,
  metaValue?: string | null,
  isRetired?: boolean | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateInteractivesMetaDataInput = {
  id: string,
  ltiPlatformID?: string | null,
  metaKey?: string | null,
  metaValue?: string | null,
  isRetired?: boolean | null,
};

export type DeleteInteractivesMetaDataInput = {
  id: string,
};

export type CreateLmsImageInput = {
  id?: string | null,
  lms: string,
  lmsImageId: string,
  lmsImageUUID?: string | null,
  lmsImageName: string,
  lmsImageDisplayName?: string | null,
  lmsImageURL: string,
  lmsImageType?: string | null,
  s3ObjectKey?: string | null,
  s3PublicObjectKey?: string | null,
  s3GlobalTemplateKey?: string | null,
  s3GlobalRowsKey?: string | null,
  s3GlobalElementsKey?: string | null,
  s3SharedTemplateKey?: string | null,
  s3SharedRowsKey?: string | null,
  s3SharedElementsKey?: string | null,
  lmsImageMetaData?: string | null,
  s3MetaData?: string | null,
};

export type ModelLmsImageConditionInput = {
  lms?: ModelStringInput | null,
  lmsImageId?: ModelStringInput | null,
  lmsImageUUID?: ModelStringInput | null,
  lmsImageName?: ModelStringInput | null,
  lmsImageDisplayName?: ModelStringInput | null,
  lmsImageURL?: ModelStringInput | null,
  lmsImageType?: ModelStringInput | null,
  s3ObjectKey?: ModelStringInput | null,
  s3PublicObjectKey?: ModelStringInput | null,
  s3GlobalTemplateKey?: ModelStringInput | null,
  s3GlobalRowsKey?: ModelStringInput | null,
  s3GlobalElementsKey?: ModelStringInput | null,
  s3SharedTemplateKey?: ModelStringInput | null,
  s3SharedRowsKey?: ModelStringInput | null,
  s3SharedElementsKey?: ModelStringInput | null,
  lmsImageMetaData?: ModelStringInput | null,
  s3MetaData?: ModelStringInput | null,
  and?: Array< ModelLmsImageConditionInput | null > | null,
  or?: Array< ModelLmsImageConditionInput | null > | null,
  not?: ModelLmsImageConditionInput | null,
};

export type LmsImage = {
  __typename: "LmsImage",
  id?: string,
  lms?: string,
  lmsImageId?: string,
  lmsImageUUID?: string | null,
  lmsImageName?: string,
  lmsImageDisplayName?: string | null,
  lmsImageURL?: string,
  lmsImageType?: string | null,
  s3ObjectKey?: string | null,
  s3PublicObjectKey?: string | null,
  s3GlobalTemplateKey?: string | null,
  s3GlobalRowsKey?: string | null,
  s3GlobalElementsKey?: string | null,
  s3SharedTemplateKey?: string | null,
  s3SharedRowsKey?: string | null,
  s3SharedElementsKey?: string | null,
  lmsImageMetaData?: string | null,
  s3MetaData?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLmsImageInput = {
  id: string,
  lms?: string | null,
  lmsImageId?: string | null,
  lmsImageUUID?: string | null,
  lmsImageName?: string | null,
  lmsImageDisplayName?: string | null,
  lmsImageURL?: string | null,
  lmsImageType?: string | null,
  s3ObjectKey?: string | null,
  s3PublicObjectKey?: string | null,
  s3GlobalTemplateKey?: string | null,
  s3GlobalRowsKey?: string | null,
  s3GlobalElementsKey?: string | null,
  s3SharedTemplateKey?: string | null,
  s3SharedRowsKey?: string | null,
  s3SharedElementsKey?: string | null,
  lmsImageMetaData?: string | null,
  s3MetaData?: string | null,
};

export type DeleteLmsImageInput = {
  id: string,
};

export type CreateLoreeOrganisationInput = {
  id?: string | null,
  name?: string | null,
};

export type ModelLoreeOrganisationConditionInput = {
  name?: ModelStringInput | null,
  and?: Array< ModelLoreeOrganisationConditionInput | null > | null,
  or?: Array< ModelLoreeOrganisationConditionInput | null > | null,
  not?: ModelLoreeOrganisationConditionInput | null,
};

export type UpdateLoreeOrganisationInput = {
  id: string,
  name?: string | null,
};

export type DeleteLoreeOrganisationInput = {
  id: string,
};

export type CreateLoreeUserInput = {
  id?: string | null,
  loreeOrganisationID?: string | null,
  name?: string | null,
  email?: string | null,
  username?: string | null,
  externalIds?: Array< ExternalIdInput | null > | null,
};

export type ExternalIdInput = {
  key?: string | null,
  value?: string | null,
};

export type ModelLoreeUserConditionInput = {
  loreeOrganisationID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  email?: ModelStringInput | null,
  username?: ModelStringInput | null,
  and?: Array< ModelLoreeUserConditionInput | null > | null,
  or?: Array< ModelLoreeUserConditionInput | null > | null,
  not?: ModelLoreeUserConditionInput | null,
};

export type LoreeUser = {
  __typename: "LoreeUser",
  id?: string,
  loreeOrganisationID?: string | null,
  name?: string | null,
  email?: string | null,
  username?: string | null,
  externalIds?:  Array<ExternalId | null > | null,
  createdAt?: string,
  updatedAt?: string,
};

export type ExternalId = {
  __typename: "ExternalId",
  key?: string | null,
  value?: string | null,
};

export type UpdateLoreeUserInput = {
  id: string,
  loreeOrganisationID?: string | null,
  name?: string | null,
  email?: string | null,
  username?: string | null,
  externalIds?: Array< ExternalIdInput | null > | null,
};

export type DeleteLoreeUserInput = {
  id: string,
};

export type CreateLtiToolInput = {
  id?: string | null,
  ltiPlatformID?: string | null,
  toolName: string,
  issuerUrl: string,
  oidcUrl: string,
  redirectURI: string,
  targetLinkURI: string,
  jwksUrl: string,
  domainName: string,
  clientId: string,
  clientSecret?: string | null,
};

export type ModelLtiToolConditionInput = {
  toolName?: ModelStringInput | null,
  issuerUrl?: ModelStringInput | null,
  oidcUrl?: ModelStringInput | null,
  redirectURI?: ModelStringInput | null,
  targetLinkURI?: ModelStringInput | null,
  jwksUrl?: ModelStringInput | null,
  domainName?: ModelStringInput | null,
  clientId?: ModelStringInput | null,
  clientSecret?: ModelStringInput | null,
  and?: Array< ModelLtiToolConditionInput | null > | null,
  or?: Array< ModelLtiToolConditionInput | null > | null,
  not?: ModelLtiToolConditionInput | null,
};

export type LtiTool = {
  __typename: "LtiTool",
  id?: string,
  ltiPlatformID?: string | null,
  toolName?: string,
  issuerUrl?: string,
  oidcUrl?: string,
  redirectURI?: string,
  targetLinkURI?: string,
  jwksUrl?: string,
  domainName?: string,
  clientId?: string,
  clientSecret?: string | null,
  toolDeployments?: ModelLtiToolDeploymentConnection,
  createdAt?: string,
  updatedAt?: string,
};

export type ModelLtiToolDeploymentConnection = {
  __typename: "ModelLtiToolDeploymentConnection",
  items?:  Array<LtiToolDeployment >,
  nextToken?: string | null,
};

export type LtiToolDeployment = {
  __typename: "LtiToolDeployment",
  id?: string,
  toolId?: LtiTool,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLtiToolInput = {
  id: string,
  ltiPlatformID?: string | null,
  toolName?: string | null,
  issuerUrl?: string | null,
  oidcUrl?: string | null,
  redirectURI?: string | null,
  targetLinkURI?: string | null,
  jwksUrl?: string | null,
  domainName?: string | null,
  clientId?: string | null,
  clientSecret?: string | null,
};

export type DeleteLtiToolInput = {
  id: string,
};

export type CreateLtiToolLoginHintInput = {
  id?: string | null,
  issuerUrl: string,
  clientId: string,
  loreeUserEmail: string,
  messageType?: string | null,
  resourceLink?: string | null,
};

export type ModelLtiToolLoginHintConditionInput = {
  issuerUrl?: ModelStringInput | null,
  clientId?: ModelStringInput | null,
  loreeUserEmail?: ModelStringInput | null,
  messageType?: ModelStringInput | null,
  resourceLink?: ModelStringInput | null,
  and?: Array< ModelLtiToolLoginHintConditionInput | null > | null,
  or?: Array< ModelLtiToolLoginHintConditionInput | null > | null,
  not?: ModelLtiToolLoginHintConditionInput | null,
};

export type LtiToolLoginHint = {
  __typename: "LtiToolLoginHint",
  id?: string,
  issuerUrl?: string,
  clientId?: string,
  loreeUserEmail?: string,
  messageType?: string | null,
  resourceLink?: string | null,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateLtiToolLoginHintInput = {
  id: string,
  issuerUrl?: string | null,
  clientId?: string | null,
  loreeUserEmail?: string | null,
  messageType?: string | null,
  resourceLink?: string | null,
};

export type DeleteLtiToolLoginHintInput = {
  id: string,
};

export type CreateLtiToolDeploymentInput = {
  id?: string | null,
  ltiToolDeploymentToolIdId?: string | null,
};

export type ModelLtiToolDeploymentConditionInput = {
  and?: Array< ModelLtiToolDeploymentConditionInput | null > | null,
  or?: Array< ModelLtiToolDeploymentConditionInput | null > | null,
  not?: ModelLtiToolDeploymentConditionInput | null,
};

export type UpdateLtiToolDeploymentInput = {
  id: string,
  ltiToolDeploymentToolIdId?: string | null,
};

export type DeleteLtiToolDeploymentInput = {
  id: string,
};

export type CreateCustomThemeInput = {
  id?: string | null,
  ltiPlatformID: string,
  isShared?: boolean | null,
  themeData: string,
};

export type ModelCustomThemeConditionInput = {
  isShared?: ModelBooleanInput | null,
  themeData?: ModelStringInput | null,
  and?: Array< ModelCustomThemeConditionInput | null > | null,
  or?: Array< ModelCustomThemeConditionInput | null > | null,
  not?: ModelCustomThemeConditionInput | null,
};

export type CustomTheme = {
  __typename: "CustomTheme",
  id?: string,
  ltiPlatformID?: string,
  isShared?: boolean | null,
  themeData?: string,
  createdAt?: string,
  updatedAt?: string,
};

export type UpdateCustomThemeInput = {
  id: string,
  ltiPlatformID?: string | null,
  isShared?: boolean | null,
  themeData?: string | null,
};

export type DeleteCustomThemeInput = {
  id: string,
};

export type ModelCategoryFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  and?: Array< ModelCategoryFilterInput | null > | null,
  or?: Array< ModelCategoryFilterInput | null > | null,
  not?: ModelCategoryFilterInput | null,
};

export type ModelCategoryConnection = {
  __typename: "ModelCategoryConnection",
  items?:  Array<Category >,
  nextToken?: string | null,
};

export type ModelProjectFilterInput = {
  id?: ModelIDInput | null,
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  and?: Array< ModelProjectFilterInput | null > | null,
  or?: Array< ModelProjectFilterInput | null > | null,
  not?: ModelProjectFilterInput | null,
};

export type ModelPageFilterInput = {
  id?: ModelIDInput | null,
  projectID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  content?: ModelStringInput | null,
  state?: ModelStringInput | null,
  and?: Array< ModelPageFilterInput | null > | null,
  or?: Array< ModelPageFilterInput | null > | null,
  not?: ModelPageFilterInput | null,
};

export type ModelCustomTemplateFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  isGlobal?: ModelBooleanInput | null,
  isShared?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  owner?: ModelStringInput | null,
  and?: Array< ModelCustomTemplateFilterInput | null > | null,
  or?: Array< ModelCustomTemplateFilterInput | null > | null,
  not?: ModelCustomTemplateFilterInput | null,
};

export type ModelCustomTemplateConnection = {
  __typename: "ModelCustomTemplateConnection",
  items?:  Array<CustomTemplate >,
  nextToken?: string | null,
};

export type ModelGlobalTemplatesFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  customTemplateID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelGlobalTemplatesFilterInput | null > | null,
  or?: Array< ModelGlobalTemplatesFilterInput | null > | null,
  not?: ModelGlobalTemplatesFilterInput | null,
};

export type ModelGlobalTemplatesConnection = {
  __typename: "ModelGlobalTemplatesConnection",
  items?:  Array<GlobalTemplates >,
  nextToken?: string | null,
};

export type ModelSharedTemplatesFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  customTemplateID?: ModelStringInput | null,
  sharedAccountId?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelSharedTemplatesFilterInput | null > | null,
  or?: Array< ModelSharedTemplatesFilterInput | null > | null,
  not?: ModelSharedTemplatesFilterInput | null,
};

export type ModelSharedTemplatesConnection = {
  __typename: "ModelSharedTemplatesConnection",
  items?:  Array<SharedTemplates >,
  nextToken?: string | null,
};

export type ModelFileFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  size?: ModelStringInput | null,
  type?: ModelStringInput | null,
  and?: Array< ModelFileFilterInput | null > | null,
  or?: Array< ModelFileFilterInput | null > | null,
  not?: ModelFileFilterInput | null,
};

export type ModelFileConnection = {
  __typename: "ModelFileConnection",
  items?:  Array<File >,
  nextToken?: string | null,
};

export type ModelLtiPlatformFilterInput = {
  id?: ModelIDInput | null,
  clientId?: ModelStringInput | null,
  clientSecret?: ModelStringInput | null,
  platformName?: ModelStringInput | null,
  platformUrl?: ModelStringInput | null,
  authEndpoint?: ModelStringInput | null,
  accesstokenEndpoint?: ModelStringInput | null,
  publicKeyURL?: ModelStringInput | null,
  googleAnalyticsTrackingId?: ModelStringInput | null,
  ltiDeploymentId?: ModelStringInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  and?: Array< ModelLtiPlatformFilterInput | null > | null,
  or?: Array< ModelLtiPlatformFilterInput | null > | null,
  not?: ModelLtiPlatformFilterInput | null,
};

export type ModelLtiApiKeyFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  ltiClientID?: ModelIDInput | null,
  oauthLoginUrl?: ModelStringInput | null,
  oauthTokenUrl?: ModelStringInput | null,
  apiClientId?: ModelStringInput | null,
  apiSecretKey?: ModelStringInput | null,
  lmsApiUrl?: ModelStringInput | null,
  and?: Array< ModelLtiApiKeyFilterInput | null > | null,
  or?: Array< ModelLtiApiKeyFilterInput | null > | null,
  not?: ModelLtiApiKeyFilterInput | null,
};

export type ModelLtiPlatformKeyFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  kid?: ModelStringInput | null,
  privatekey?: ModelStringInput | null,
  publicKey?: ModelStringInput | null,
  and?: Array< ModelLtiPlatformKeyFilterInput | null > | null,
  or?: Array< ModelLtiPlatformKeyFilterInput | null > | null,
  not?: ModelLtiPlatformKeyFilterInput | null,
};

export type ModelLtiPlatformKeyConnection = {
  __typename: "ModelLtiPlatformKeyConnection",
  items?:  Array<LtiPlatformKey >,
  nextToken?: string | null,
};

export type ModelLtiAccessTokenFilterInput = {
  id?: ModelIDInput | null,
  ltiApiKeyID?: ModelIDInput | null,
  ltiClientID?: ModelStringInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  loreeUsername?: ModelStringInput | null,
  loreePassword?: ModelStringInput | null,
  user?: ModelStringInput | null,
  userInfo?: ModelStringInput | null,
  roles?: ModelStringInput | null,
  accessToken?: ModelStringInput | null,
  refreshToken?: ModelStringInput | null,
  isAdmin?: ModelBooleanInput | null,
  generatedAt?: ModelStringInput | null,
  lmsApiUrl?: ModelStringInput | null,
  expiresAt?: ModelStringInput | null,
  lms_email?: ModelStringInput | null,
  and?: Array< ModelLtiAccessTokenFilterInput | null > | null,
  or?: Array< ModelLtiAccessTokenFilterInput | null > | null,
  not?: ModelLtiAccessTokenFilterInput | null,
};

export type ModelLtiLogFilterInput = {
  id?: ModelStringInput | null,
  ltiPlatformID?: ModelIDInput | null,
  userInfo?: ModelStringInput | null,
  clientInfo?: ModelStringInput | null,
  userGeoLocation?: ModelStringInput | null,
  userAction?: ModelStringInput | null,
  generatedAt?: ModelStringInput | null,
  and?: Array< ModelLtiLogFilterInput | null > | null,
  or?: Array< ModelLtiLogFilterInput | null > | null,
  not?: ModelLtiLogFilterInput | null,
};

export type ModelLtiLogConnection = {
  __typename: "ModelLtiLogConnection",
  items?:  Array<LtiLog >,
  nextToken?: string | null,
};

export type ModelCustomBlockFilterInput = {
  id?: ModelIDInput | null,
  title?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  type?: ModelStringInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  isGlobal?: ModelBooleanInput | null,
  isShared?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  owner?: ModelStringInput | null,
  and?: Array< ModelCustomBlockFilterInput | null > | null,
  or?: Array< ModelCustomBlockFilterInput | null > | null,
  not?: ModelCustomBlockFilterInput | null,
};

export type ModelCustomBlockConnection = {
  __typename: "ModelCustomBlockConnection",
  items?:  Array<CustomBlock >,
  nextToken?: string | null,
};

export type ModelGlobalBlocksFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  customBlockID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  type?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelGlobalBlocksFilterInput | null > | null,
  or?: Array< ModelGlobalBlocksFilterInput | null > | null,
  not?: ModelGlobalBlocksFilterInput | null,
};

export type ModelGlobalBlocksConnection = {
  __typename: "ModelGlobalBlocksConnection",
  items?:  Array<GlobalBlocks >,
  nextToken?: string | null,
};

export type ModelSharedBlocksFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  title?: ModelStringInput | null,
  type?: ModelStringInput | null,
  customBlockID?: ModelStringInput | null,
  categoryID?: ModelIDInput | null,
  sharedAccountId?: ModelStringInput | null,
  content?: ModelStringInput | null,
  status?: ModelBooleanInput | null,
  createdBy?: ModelStringInput | null,
  active?: ModelBooleanInput | null,
  and?: Array< ModelSharedBlocksFilterInput | null > | null,
  or?: Array< ModelSharedBlocksFilterInput | null > | null,
  not?: ModelSharedBlocksFilterInput | null,
};

export type ModelSharedBlocksConnection = {
  __typename: "ModelSharedBlocksConnection",
  items?:  Array<SharedBlocks >,
  nextToken?: string | null,
};

export type ModelCustomStyleFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  customColor?: ModelStringInput | null,
  customHeader?: ModelStringInput | null,
  customFont?: ModelStringInput | null,
  customLink?: ModelStringInput | null,
  and?: Array< ModelCustomStyleFilterInput | null > | null,
  or?: Array< ModelCustomStyleFilterInput | null > | null,
  not?: ModelCustomStyleFilterInput | null,
};

export type ModelCustomStyleConnection = {
  __typename: "ModelCustomStyleConnection",
  items?:  Array<CustomStyle >,
  nextToken?: string | null,
};

export type ModelKalturaConfigFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  partnerID?: ModelStringInput | null,
  appToken?: ModelStringInput | null,
  tokenID?: ModelStringInput | null,
  and?: Array< ModelKalturaConfigFilterInput | null > | null,
  or?: Array< ModelKalturaConfigFilterInput | null > | null,
  not?: ModelKalturaConfigFilterInput | null,
};

export type ModelKalturaConfigConnection = {
  __typename: "ModelKalturaConfigConnection",
  items?:  Array<KalturaConfig >,
  nextToken?: string | null,
};

export type ModelLoreeFeatureFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  featureList?: ModelStringInput | null,
  and?: Array< ModelLoreeFeatureFilterInput | null > | null,
  or?: Array< ModelLoreeFeatureFilterInput | null > | null,
  not?: ModelLoreeFeatureFilterInput | null,
};

export type ModelLoreeFeatureConnection = {
  __typename: "ModelLoreeFeatureConnection",
  items?:  Array<LoreeFeature >,
  nextToken?: string | null,
};

export type ModelLoreeRoleFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  featureList?: ModelStringInput | null,
  and?: Array< ModelLoreeRoleFilterInput | null > | null,
  or?: Array< ModelLoreeRoleFilterInput | null > | null,
  not?: ModelLoreeRoleFilterInput | null,
};

export type ModelLoreeRoleConnection = {
  __typename: "ModelLoreeRoleConnection",
  items?:  Array<LoreeRole >,
  nextToken?: string | null,
};

export type ModelLmsLoreeRoleFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  lmsRole?: ModelStringInput | null,
  lmsBaseRoleType?: ModelStringInput | null,
  lmsRoleId?: ModelStringInput | null,
  and?: Array< ModelLmsLoreeRoleFilterInput | null > | null,
  or?: Array< ModelLmsLoreeRoleFilterInput | null > | null,
  not?: ModelLmsLoreeRoleFilterInput | null,
};

export type ModelHFivePUsersFilterInput = {
  id?: ModelIDInput | null,
  email?: ModelStringInput | null,
  loreeUser?: ModelStringInput | null,
  ltiPlatformID?: ModelIDInput | null,
  and?: Array< ModelHFivePUsersFilterInput | null > | null,
  or?: Array< ModelHFivePUsersFilterInput | null > | null,
  not?: ModelHFivePUsersFilterInput | null,
};

export type ModelHFivePUsersConnection = {
  __typename: "ModelHFivePUsersConnection",
  items?:  Array<HFivePUsers >,
  nextToken?: string | null,
};

export type ModelInteractivesMetaDataFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  metaKey?: ModelStringInput | null,
  metaValue?: ModelStringInput | null,
  isRetired?: ModelBooleanInput | null,
  and?: Array< ModelInteractivesMetaDataFilterInput | null > | null,
  or?: Array< ModelInteractivesMetaDataFilterInput | null > | null,
  not?: ModelInteractivesMetaDataFilterInput | null,
};

export type ModelInteractivesMetaDataConnection = {
  __typename: "ModelInteractivesMetaDataConnection",
  items?:  Array<InteractivesMetaData >,
  nextToken?: string | null,
};

export type ModelLmsImageFilterInput = {
  id?: ModelIDInput | null,
  lms?: ModelStringInput | null,
  lmsImageId?: ModelStringInput | null,
  lmsImageUUID?: ModelStringInput | null,
  lmsImageName?: ModelStringInput | null,
  lmsImageDisplayName?: ModelStringInput | null,
  lmsImageURL?: ModelStringInput | null,
  lmsImageType?: ModelStringInput | null,
  s3ObjectKey?: ModelStringInput | null,
  s3PublicObjectKey?: ModelStringInput | null,
  s3GlobalTemplateKey?: ModelStringInput | null,
  s3GlobalRowsKey?: ModelStringInput | null,
  s3GlobalElementsKey?: ModelStringInput | null,
  s3SharedTemplateKey?: ModelStringInput | null,
  s3SharedRowsKey?: ModelStringInput | null,
  s3SharedElementsKey?: ModelStringInput | null,
  lmsImageMetaData?: ModelStringInput | null,
  s3MetaData?: ModelStringInput | null,
  and?: Array< ModelLmsImageFilterInput | null > | null,
  or?: Array< ModelLmsImageFilterInput | null > | null,
  not?: ModelLmsImageFilterInput | null,
};

export type ModelLmsImageConnection = {
  __typename: "ModelLmsImageConnection",
  items?:  Array<LmsImage >,
  nextToken?: string | null,
};

export type ModelLoreeOrganisationFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  and?: Array< ModelLoreeOrganisationFilterInput | null > | null,
  or?: Array< ModelLoreeOrganisationFilterInput | null > | null,
  not?: ModelLoreeOrganisationFilterInput | null,
};

export type ModelLoreeOrganisationConnection = {
  __typename: "ModelLoreeOrganisationConnection",
  items?:  Array<LoreeOrganisation >,
  nextToken?: string | null,
};

export type ModelLoreeUserFilterInput = {
  id?: ModelIDInput | null,
  loreeOrganisationID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  email?: ModelStringInput | null,
  username?: ModelStringInput | null,
  and?: Array< ModelLoreeUserFilterInput | null > | null,
  or?: Array< ModelLoreeUserFilterInput | null > | null,
  not?: ModelLoreeUserFilterInput | null,
};

export type ModelLoreeUserConnection = {
  __typename: "ModelLoreeUserConnection",
  items?:  Array<LoreeUser >,
  nextToken?: string | null,
};

export type ModelLtiToolFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  toolName?: ModelStringInput | null,
  issuerUrl?: ModelStringInput | null,
  oidcUrl?: ModelStringInput | null,
  redirectURI?: ModelStringInput | null,
  targetLinkURI?: ModelStringInput | null,
  jwksUrl?: ModelStringInput | null,
  domainName?: ModelStringInput | null,
  clientId?: ModelStringInput | null,
  clientSecret?: ModelStringInput | null,
  and?: Array< ModelLtiToolFilterInput | null > | null,
  or?: Array< ModelLtiToolFilterInput | null > | null,
  not?: ModelLtiToolFilterInput | null,
};

export type ModelLtiToolConnection = {
  __typename: "ModelLtiToolConnection",
  items?:  Array<LtiTool >,
  nextToken?: string | null,
};

export type ModelLtiToolLoginHintFilterInput = {
  id?: ModelIDInput | null,
  issuerUrl?: ModelStringInput | null,
  clientId?: ModelStringInput | null,
  loreeUserEmail?: ModelStringInput | null,
  messageType?: ModelStringInput | null,
  resourceLink?: ModelStringInput | null,
  and?: Array< ModelLtiToolLoginHintFilterInput | null > | null,
  or?: Array< ModelLtiToolLoginHintFilterInput | null > | null,
  not?: ModelLtiToolLoginHintFilterInput | null,
};

export type ModelLtiToolLoginHintConnection = {
  __typename: "ModelLtiToolLoginHintConnection",
  items?:  Array<LtiToolLoginHint >,
  nextToken?: string | null,
};

export type ModelLtiToolDeploymentFilterInput = {
  id?: ModelIDInput | null,
  and?: Array< ModelLtiToolDeploymentFilterInput | null > | null,
  or?: Array< ModelLtiToolDeploymentFilterInput | null > | null,
  not?: ModelLtiToolDeploymentFilterInput | null,
};

export type ModelCustomThemeFilterInput = {
  id?: ModelIDInput | null,
  ltiPlatformID?: ModelIDInput | null,
  isShared?: ModelBooleanInput | null,
  themeData?: ModelStringInput | null,
  and?: Array< ModelCustomThemeFilterInput | null > | null,
  or?: Array< ModelCustomThemeFilterInput | null > | null,
  not?: ModelCustomThemeFilterInput | null,
};

export type ModelCustomThemeConnection = {
  __typename: "ModelCustomThemeConnection",
  items?:  Array<CustomTheme >,
  nextToken?: string | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type ModelStringKeyConditionInput = {
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type UpdateCanvasModuleMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
  moduleName?: string | null,
};

export type UpdateCanvasModuleMutation = {
  updateCanvasModule?: string | null,
};

export type DeleteCanvasModuleMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
};

export type DeleteCanvasModuleMutation = {
  deleteCanvasModule?: string | null,
};

export type ViewCanvasPageMutationVariables = {
  courseId?: string | null,
  pageId?: string | null,
};

export type ViewCanvasPageMutation = {
  viewCanvasPage?: string | null,
};

export type UpdateCanvasPageMutationVariables = {
  courseId?: string | null,
  pageUrl?: string | null,
  pageName?: string | null,
};

export type UpdateCanvasPageMutation = {
  updateCanvasPage?: string | null,
};

export type DeleteCanvasPageMutationVariables = {
  courseId?: string | null,
  pageUrl?: string | null,
};

export type DeleteCanvasPageMutation = {
  deleteCanvasPage?: string | null,
};

export type DuplicateCanvasPageMutationVariables = {
  courseId?: string | null,
  pageUrl?: string | null,
};

export type DuplicateCanvasPageMutation = {
  duplicateCanvasPage?: string | null,
};

export type SaveCanvasPageMutationVariables = {
  courseId?: string | null,
  pageID?: string | null,
  editorContent?: string | null,
  uploadAndConvertFromBackendFeatureToggle?: boolean | null,
};

export type SaveCanvasPageMutation = {
  saveCanvasPage?: string | null,
};

export type ViewCanvasDiscussionMutationVariables = {
  courseId?: string | null,
  discussionId?: string | null,
};

export type ViewCanvasDiscussionMutation = {
  viewCanvasDiscussion?: string | null,
};

export type UpdateCanvasDiscussionMutationVariables = {
  courseId?: string | null,
  discussionId?: string | null,
  discussionName?: string | null,
};

export type UpdateCanvasDiscussionMutation = {
  updateCanvasDiscussion?: string | null,
};

export type SaveCanvasDiscussionMutationVariables = {
  courseId?: string | null,
  discussionID?: string | null,
  editorContent?: string | null,
  uploadAndConvertFromBackendFeatureToggle?: boolean | null,
};

export type SaveCanvasDiscussionMutation = {
  saveCanvasDiscussion?: string | null,
};

export type ViewCanvasAssignmentMutationVariables = {
  courseId?: string | null,
  assignmentId?: string | null,
};

export type ViewCanvasAssignmentMutation = {
  viewCanvasAssignment?: string | null,
};

export type UpdateCanvasAssignmentMutationVariables = {
  courseId?: string | null,
  assignmentId?: string | null,
  assignmentName?: string | null,
};

export type UpdateCanvasAssignmentMutation = {
  updateCanvasAssignment?: string | null,
};

export type SaveCanvasAssignmentMutationVariables = {
  courseId?: string | null,
  assignmentID?: string | null,
  editorContent?: string | null,
  uploadAndConvertFromBackendFeatureToggle?: boolean | null,
};

export type SaveCanvasAssignmentMutation = {
  saveCanvasAssignment?: string | null,
};

export type UpdateModuleItemMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
  itemId?: string | null,
  title?: string | null,
};

export type UpdateModuleItemMutation = {
  updateModuleItem?: string | null,
};

export type DeleteModuleItemMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
  itemId?: string | null,
};

export type DeleteModuleItemMutation = {
  deleteModuleItem?: string | null,
};

export type DuplicatePageFromModuleMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
  pageUrl?: string | null,
};

export type DuplicatePageFromModuleMutation = {
  duplicatePageFromModule?: string | null,
};

export type CreateToCanvasMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
  Name?: string | null,
  type?: string | null,
};

export type CreateToCanvasMutation = {
  createToCanvas?: string | null,
};

export type ViewCanvasAssignmentsMutationVariables = {
  courseId?: string | null,
  assignmentId?: string | null,
};

export type ViewCanvasAssignmentsMutation = {
  viewCanvasAssignments?: string | null,
};

export type PublishToCanvasMutationVariables = {
  courseId?: string | null,
  moduleId?: string | null,
  itemId?: string | null,
  pageId?: string | null,
  assignmentId?: string | null,
  discussionId?: string | null,
  quizId?: string | null,
  status?: boolean | null,
  type?: string | null,
};

export type PublishToCanvasMutation = {
  publishToCanvas?: string | null,
};

export type ImageUploadMutationVariables = {
  courseId?: string | null,
  name?: string | null,
  type?: string | null,
  path?: string | null,
};

export type ImageUploadMutation = {
  imageUpload?: string | null,
};

export type ImageS3UploadMutationVariables = {
  platformId?: string | null,
  path?: string | null,
  title?: string | null,
  type?: string | null,
};

export type ImageS3UploadMutation = {
  imageS3Upload?: string | null,
};

export type UploadImagesToS3MutationVariables = {
  globalHtmlContent?: string | null,
  type?: string | null,
};

export type UploadImagesToS3Mutation = {
  uploadImagesToS3?: string | null,
};

export type VideoUploadMutationVariables = {
  courseId?: string | null,
  name?: string | null,
  path?: string | null,
  platformId?: string | null,
};

export type VideoUploadMutation = {
  videoUpload?: string | null,
};

export type UploadD2LImagesToS3MutationVariables = {
  platformId?: string | null,
  file?: string | null,
  title?: string | null,
  newTitle?: string | null,
  lmsUrl?: string | null,
};

export type UploadD2LImagesToS3Mutation = {
  uploadD2LImagesToS3?: string | null,
};

export type InitiateInteractiveMutationVariables = {
  tokenId?: string | null,
  type?: string | null,
  email?: string | null,
  name?: string | null,
  organization?: string | null,
};

export type InitiateInteractiveMutation = {
  initiateInteractive?: string | null,
};

export type VerifyInteractiveMutationVariables = {
  user?: string | null,
  orgId?: string | null,
};

export type VerifyInteractiveMutation = {
  verifyInteractive?: string | null,
};

export type UpdateInteractiveContentMutationVariables = {
  contentId?: string | null,
  userId?: string | null,
  status?: boolean | null,
};

export type UpdateInteractiveContentMutation = {
  updateInteractiveContent?: string | null,
};

export type UpdateLoreeInteractiveContentMutationVariables = {
  userId?: string | null,
  title?: string | null,
  contentId?: string | null,
  childOrganization?: string | null,
};

export type UpdateLoreeInteractiveContentMutation = {
  updateLoreeInteractiveContent?: string | null,
};

export type ContentShareUpdatesMutationVariables = {
  childOrganization?: string | null,
  userId?: string | null,
  shareStatus?: boolean | null,
  contentId?: string | null,
};

export type ContentShareUpdatesMutation = {
  contentShareUpdates?: string | null,
};

export type UpdateOrganizationAccountMutationVariables = {
  email?: string | null,
  childOrganization?: string | null,
};

export type UpdateOrganizationAccountMutation = {
  updateOrganizationAccount?: string | null,
};

export type DeleteLoreeInteractiveContentMutationVariables = {
  userId?: string | null,
  contentId?: string | null,
};

export type DeleteLoreeInteractiveContentMutation = {
  deleteLoreeInteractiveContent?: string | null,
};

export type LoreeInteractiveContentStatusUpdateMutationVariables = {
  status?: boolean | null,
  organization?: string | null,
  childOrganization?: string | null,
};

export type LoreeInteractiveContentStatusUpdateMutation = {
  loreeInteractiveContentStatusUpdate?: string | null,
};

export type DuplicateLoreeInteractiveContentMutationVariables = {
  email?: string | null,
  title?: string | null,
  contentId?: string | null,
};

export type DuplicateLoreeInteractiveContentMutation = {
  duplicateLoreeInteractiveContent?: string | null,
};

export type H5pUserInitiateMutationVariables = {
  email?: string | null,
  firstName?: string | null,
  role?: string | null,
  platformId?: string | null,
  platformDomain?: string | null,
};

export type H5pUserInitiateMutation = {
  h5pUserInitiate?: string | null,
};

export type DeleteH5PContentMutationVariables = {
  contentId?: string | null,
  platformId?: string | null,
};

export type DeleteH5PContentMutation = {
  deleteH5PContent?: string | null,
};

export type UpdateH5pMetaDataMutationVariables = {
  contentId?: string | null,
  platformId?: string | null,
  metaData?: string | null,
};

export type UpdateH5pMetaDataMutation = {
  updateH5pMetaData?: string | null,
};

export type UpdateD2lContentsMutationVariables = {
  courseId?: string | null,
  contentId?: string | null,
  contentType?: string | null,
  formData?: string | null,
};

export type UpdateD2lContentsMutation = {
  updateD2lContents?: string | null,
};

export type CreateD2lModulesMutationVariables = {
  courseId?: string | null,
  formData?: string | null,
};

export type CreateD2lModulesMutation = {
  createD2lModules?: string | null,
};

export type UpdateBbContentsMutationVariables = {
  courseId?: string | null,
  contentId?: string | null,
  updateContentData?: string | null,
};

export type UpdateBbContentsMutation = {
  updateBbContents?: string | null,
};

export type UpdateUltraContentMutationVariables = {
  courseId?: string | null,
  contentId?: string | null,
  updateContentData?: string | null,
};

export type UpdateUltraContentMutation = {
  updateUltraContent?: string | null,
};

export type CreateD2LSubContentMutationVariables = {
  courseId?: string | null,
  folderId?: string | null,
  formData?: string | null,
};

export type CreateD2LSubContentMutation = {
  createD2LSubContent?: string | null,
};

export type CreateBbRootContentMutationVariables = {
  courseId?: string | null,
  contentData?: string | null,
};

export type CreateBbRootContentMutation = {
  createBbRootContent?: string | null,
};

export type CreateBbChildContentMutationVariables = {
  courseId?: string | null,
  contentId?: string | null,
  contentData?: string | null,
};

export type CreateBbChildContentMutation = {
  createBbChildContent?: string | null,
};

export type CreateBbContentMutationVariables = {
  courseId?: string | null,
  contentId?: string | null,
  contentData?: string | null,
};

export type CreateBbContentMutation = {
  createBbContent?: string | null,
};

export type BbCourseImageUploadMutationVariables = {
  courseId?: string | null,
  file?: string | null,
  fileName?: string | null,
  fileType?: string | null,
  parentId?: string | null,
};

export type BbCourseImageUploadMutation = {
  bbCourseImageUpload?: string | null,
};

export type UploadBbUltraFileToS3MutationVariables = {
  platformId?: string | null,
  file?: string | null,
  title?: string | null,
  newTitle?: string | null,
};

export type UploadBbUltraFileToS3Mutation = {
  uploadBBUltraFileToS3?: string | null,
};

export type CreateCategoryMutationVariables = {
  input?: CreateCategoryInput,
  condition?: ModelCategoryConditionInput | null,
};

export type CreateCategoryMutation = {
  createCategory?:  {
    __typename: "Category",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    name: string,
    projects?:  {
      __typename: "ModelProjectConnection",
      items:  Array< {
        __typename: "Project",
        id: string,
        title: string,
        categoryID: string,
        category?:  {
          __typename: "Category",
          id: string,
          ltiPlatformID?: string | null,
          loreeOrganisationID?: string | null,
          name: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        pages?:  {
          __typename: "ModelPageConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateCategoryMutationVariables = {
  input?: UpdateCategoryInput,
  condition?: ModelCategoryConditionInput | null,
};

export type UpdateCategoryMutation = {
  updateCategory?:  {
    __typename: "Category",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    name: string,
    projects?:  {
      __typename: "ModelProjectConnection",
      items:  Array< {
        __typename: "Project",
        id: string,
        title: string,
        categoryID: string,
        category?:  {
          __typename: "Category",
          id: string,
          ltiPlatformID?: string | null,
          loreeOrganisationID?: string | null,
          name: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        pages?:  {
          __typename: "ModelPageConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteCategoryMutationVariables = {
  input?: DeleteCategoryInput,
  condition?: ModelCategoryConditionInput | null,
};

export type DeleteCategoryMutation = {
  deleteCategory?:  {
    __typename: "Category",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    name: string,
    projects?:  {
      __typename: "ModelProjectConnection",
      items:  Array< {
        __typename: "Project",
        id: string,
        title: string,
        categoryID: string,
        category?:  {
          __typename: "Category",
          id: string,
          ltiPlatformID?: string | null,
          loreeOrganisationID?: string | null,
          name: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        pages?:  {
          __typename: "ModelPageConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateProjectMutationVariables = {
  input?: CreateProjectInput,
  condition?: ModelProjectConditionInput | null,
};

export type CreateProjectMutation = {
  createProject?:  {
    __typename: "Project",
    id: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    pages?:  {
      __typename: "ModelPageConnection",
      items:  Array< {
        __typename: "Page",
        id: string,
        projectID: string,
        project?:  {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        title: string,
        content?: string | null,
        state?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateProjectMutationVariables = {
  input?: UpdateProjectInput,
  condition?: ModelProjectConditionInput | null,
};

export type UpdateProjectMutation = {
  updateProject?:  {
    __typename: "Project",
    id: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    pages?:  {
      __typename: "ModelPageConnection",
      items:  Array< {
        __typename: "Page",
        id: string,
        projectID: string,
        project?:  {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        title: string,
        content?: string | null,
        state?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteProjectMutationVariables = {
  input?: DeleteProjectInput,
  condition?: ModelProjectConditionInput | null,
};

export type DeleteProjectMutation = {
  deleteProject?:  {
    __typename: "Project",
    id: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    pages?:  {
      __typename: "ModelPageConnection",
      items:  Array< {
        __typename: "Page",
        id: string,
        projectID: string,
        project?:  {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        title: string,
        content?: string | null,
        state?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreatePageMutationVariables = {
  input?: CreatePageInput,
  condition?: ModelPageConditionInput | null,
};

export type CreatePageMutation = {
  createPage?:  {
    __typename: "Page",
    id: string,
    projectID: string,
    project?:  {
      __typename: "Project",
      id: string,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      pages?:  {
        __typename: "ModelPageConnection",
        items:  Array< {
          __typename: "Page",
          id: string,
          projectID: string,
          title: string,
          content?: string | null,
          state?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    title: string,
    content?: string | null,
    state?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdatePageMutationVariables = {
  input?: UpdatePageInput,
  condition?: ModelPageConditionInput | null,
};

export type UpdatePageMutation = {
  updatePage?:  {
    __typename: "Page",
    id: string,
    projectID: string,
    project?:  {
      __typename: "Project",
      id: string,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      pages?:  {
        __typename: "ModelPageConnection",
        items:  Array< {
          __typename: "Page",
          id: string,
          projectID: string,
          title: string,
          content?: string | null,
          state?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    title: string,
    content?: string | null,
    state?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeletePageMutationVariables = {
  input?: DeletePageInput,
  condition?: ModelPageConditionInput | null,
};

export type DeletePageMutation = {
  deletePage?:  {
    __typename: "Page",
    id: string,
    projectID: string,
    project?:  {
      __typename: "Project",
      id: string,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      pages?:  {
        __typename: "ModelPageConnection",
        items:  Array< {
          __typename: "Page",
          id: string,
          projectID: string,
          title: string,
          content?: string | null,
          state?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    title: string,
    content?: string | null,
    state?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateCustomTemplateMutationVariables = {
  input?: CreateCustomTemplateInput,
  condition?: ModelCustomTemplateConditionInput | null,
};

export type CreateCustomTemplateMutation = {
  createCustomTemplate?:  {
    __typename: "CustomTemplate",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateCustomTemplateMutationVariables = {
  input?: UpdateCustomTemplateInput,
  condition?: ModelCustomTemplateConditionInput | null,
};

export type UpdateCustomTemplateMutation = {
  updateCustomTemplate?:  {
    __typename: "CustomTemplate",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteCustomTemplateMutationVariables = {
  input?: DeleteCustomTemplateInput,
  condition?: ModelCustomTemplateConditionInput | null,
};

export type DeleteCustomTemplateMutation = {
  deleteCustomTemplate?:  {
    __typename: "CustomTemplate",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateGlobalTemplatesMutationVariables = {
  input?: CreateGlobalTemplatesInput,
  condition?: ModelGlobalTemplatesConditionInput | null,
};

export type CreateGlobalTemplatesMutation = {
  createGlobalTemplates?:  {
    __typename: "GlobalTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customTemplateID: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateGlobalTemplatesMutationVariables = {
  input?: UpdateGlobalTemplatesInput,
  condition?: ModelGlobalTemplatesConditionInput | null,
};

export type UpdateGlobalTemplatesMutation = {
  updateGlobalTemplates?:  {
    __typename: "GlobalTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customTemplateID: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteGlobalTemplatesMutationVariables = {
  input?: DeleteGlobalTemplatesInput,
  condition?: ModelGlobalTemplatesConditionInput | null,
};

export type DeleteGlobalTemplatesMutation = {
  deleteGlobalTemplates?:  {
    __typename: "GlobalTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customTemplateID: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateSharedTemplatesMutationVariables = {
  input?: CreateSharedTemplatesInput,
  condition?: ModelSharedTemplatesConditionInput | null,
};

export type CreateSharedTemplatesMutation = {
  createSharedTemplates?:  {
    __typename: "SharedTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    customTemplateID: string,
    sharedAccountId: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateSharedTemplatesMutationVariables = {
  input?: UpdateSharedTemplatesInput,
  condition?: ModelSharedTemplatesConditionInput | null,
};

export type UpdateSharedTemplatesMutation = {
  updateSharedTemplates?:  {
    __typename: "SharedTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    customTemplateID: string,
    sharedAccountId: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteSharedTemplatesMutationVariables = {
  input?: DeleteSharedTemplatesInput,
  condition?: ModelSharedTemplatesConditionInput | null,
};

export type DeleteSharedTemplatesMutation = {
  deleteSharedTemplates?:  {
    __typename: "SharedTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    customTemplateID: string,
    sharedAccountId: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateFileMutationVariables = {
  input?: CreateFileInput,
  condition?: ModelFileConditionInput | null,
};

export type CreateFileMutation = {
  createFile?:  {
    __typename: "File",
    id: string,
    name: string,
    size?: string | null,
    location?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    type?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateFileMutationVariables = {
  input?: UpdateFileInput,
  condition?: ModelFileConditionInput | null,
};

export type UpdateFileMutation = {
  updateFile?:  {
    __typename: "File",
    id: string,
    name: string,
    size?: string | null,
    location?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    type?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteFileMutationVariables = {
  input?: DeleteFileInput,
  condition?: ModelFileConditionInput | null,
};

export type DeleteFileMutation = {
  deleteFile?:  {
    __typename: "File",
    id: string,
    name: string,
    size?: string | null,
    location?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    type?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiPlatformMutationVariables = {
  input?: CreateLtiPlatformInput,
  condition?: ModelLtiPlatformConditionInput | null,
};

export type CreateLtiPlatformMutation = {
  createLtiPlatform?:  {
    __typename: "LtiPlatform",
    id: string,
    clientId: string,
    clientSecret?: string | null,
    platformName?: string | null,
    platformUrl?: string | null,
    authEndpoint?: string | null,
    accesstokenEndpoint?: string | null,
    publicKeyURL?: string | null,
    authConfig?:  {
      __typename: "AuthDetail",
      method?: string | null,
      key?: string | null,
    } | null,
    googleAnalyticsTrackingId?: string | null,
    ltiDeploymentId?: string | null,
    loreeOrganisationID?: string | null,
    loreeOrganisation?:  {
      __typename: "LoreeOrganisation",
      id: string,
      name?: string | null,
      ltiPlatforms?:  {
        __typename: "ModelLtiPlatformConnection",
        items:  Array< {
          __typename: "LtiPlatform",
          id: string,
          clientId: string,
          clientSecret?: string | null,
          platformName?: string | null,
          platformUrl?: string | null,
          authEndpoint?: string | null,
          accesstokenEndpoint?: string | null,
          publicKeyURL?: string | null,
          googleAnalyticsTrackingId?: string | null,
          ltiDeploymentId?: string | null,
          loreeOrganisationID?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    ltiApiKey?:  {
      __typename: "ModelLtiApiKeyConnection",
      items:  Array< {
        __typename: "LtiApiKey",
        id: string,
        ltiPlatformID: string,
        loreeOrganisationID?: string | null,
        ltiClientID?: string | null,
        oauthLoginUrl?: string | null,
        oauthTokenUrl?: string | null,
        apiClientId?: string | null,
        apiSecretKey?: string | null,
        lmsApiUrl?: string | null,
        ltiAccessToken?:  {
          __typename: "ModelLtiAccessTokenConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiPlatformMutationVariables = {
  input?: UpdateLtiPlatformInput,
  condition?: ModelLtiPlatformConditionInput | null,
};

export type UpdateLtiPlatformMutation = {
  updateLtiPlatform?:  {
    __typename: "LtiPlatform",
    id: string,
    clientId: string,
    clientSecret?: string | null,
    platformName?: string | null,
    platformUrl?: string | null,
    authEndpoint?: string | null,
    accesstokenEndpoint?: string | null,
    publicKeyURL?: string | null,
    authConfig?:  {
      __typename: "AuthDetail",
      method?: string | null,
      key?: string | null,
    } | null,
    googleAnalyticsTrackingId?: string | null,
    ltiDeploymentId?: string | null,
    loreeOrganisationID?: string | null,
    loreeOrganisation?:  {
      __typename: "LoreeOrganisation",
      id: string,
      name?: string | null,
      ltiPlatforms?:  {
        __typename: "ModelLtiPlatformConnection",
        items:  Array< {
          __typename: "LtiPlatform",
          id: string,
          clientId: string,
          clientSecret?: string | null,
          platformName?: string | null,
          platformUrl?: string | null,
          authEndpoint?: string | null,
          accesstokenEndpoint?: string | null,
          publicKeyURL?: string | null,
          googleAnalyticsTrackingId?: string | null,
          ltiDeploymentId?: string | null,
          loreeOrganisationID?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    ltiApiKey?:  {
      __typename: "ModelLtiApiKeyConnection",
      items:  Array< {
        __typename: "LtiApiKey",
        id: string,
        ltiPlatformID: string,
        loreeOrganisationID?: string | null,
        ltiClientID?: string | null,
        oauthLoginUrl?: string | null,
        oauthTokenUrl?: string | null,
        apiClientId?: string | null,
        apiSecretKey?: string | null,
        lmsApiUrl?: string | null,
        ltiAccessToken?:  {
          __typename: "ModelLtiAccessTokenConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiPlatformMutationVariables = {
  input?: DeleteLtiPlatformInput,
  condition?: ModelLtiPlatformConditionInput | null,
};

export type DeleteLtiPlatformMutation = {
  deleteLtiPlatform?:  {
    __typename: "LtiPlatform",
    id: string,
    clientId: string,
    clientSecret?: string | null,
    platformName?: string | null,
    platformUrl?: string | null,
    authEndpoint?: string | null,
    accesstokenEndpoint?: string | null,
    publicKeyURL?: string | null,
    authConfig?:  {
      __typename: "AuthDetail",
      method?: string | null,
      key?: string | null,
    } | null,
    googleAnalyticsTrackingId?: string | null,
    ltiDeploymentId?: string | null,
    loreeOrganisationID?: string | null,
    loreeOrganisation?:  {
      __typename: "LoreeOrganisation",
      id: string,
      name?: string | null,
      ltiPlatforms?:  {
        __typename: "ModelLtiPlatformConnection",
        items:  Array< {
          __typename: "LtiPlatform",
          id: string,
          clientId: string,
          clientSecret?: string | null,
          platformName?: string | null,
          platformUrl?: string | null,
          authEndpoint?: string | null,
          accesstokenEndpoint?: string | null,
          publicKeyURL?: string | null,
          googleAnalyticsTrackingId?: string | null,
          ltiDeploymentId?: string | null,
          loreeOrganisationID?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    ltiApiKey?:  {
      __typename: "ModelLtiApiKeyConnection",
      items:  Array< {
        __typename: "LtiApiKey",
        id: string,
        ltiPlatformID: string,
        loreeOrganisationID?: string | null,
        ltiClientID?: string | null,
        oauthLoginUrl?: string | null,
        oauthTokenUrl?: string | null,
        apiClientId?: string | null,
        apiSecretKey?: string | null,
        lmsApiUrl?: string | null,
        ltiAccessToken?:  {
          __typename: "ModelLtiAccessTokenConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiApiKeyMutationVariables = {
  input?: CreateLtiApiKeyInput,
  condition?: ModelLtiApiKeyConditionInput | null,
};

export type CreateLtiApiKeyMutation = {
  createLtiApiKey?:  {
    __typename: "LtiApiKey",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    ltiClientID?: string | null,
    oauthLoginUrl?: string | null,
    oauthTokenUrl?: string | null,
    apiClientId?: string | null,
    apiSecretKey?: string | null,
    lmsApiUrl?: string | null,
    ltiAccessToken?:  {
      __typename: "ModelLtiAccessTokenConnection",
      items:  Array< {
        __typename: "LtiAccessToken",
        id: string,
        ltiApiKeyID: string,
        ltiClientID?: string | null,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        loreeUsername?: string | null,
        loreePassword?: string | null,
        user?: string | null,
        userInfo?: string | null,
        roles?: string | null,
        accessToken?: string | null,
        refreshToken?: string | null,
        isAdmin?: boolean | null,
        generatedAt?: string | null,
        lmsApiUrl?: string | null,
        expiresAt?: string | null,
        lms_email?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiApiKeyMutationVariables = {
  input?: UpdateLtiApiKeyInput,
  condition?: ModelLtiApiKeyConditionInput | null,
};

export type UpdateLtiApiKeyMutation = {
  updateLtiApiKey?:  {
    __typename: "LtiApiKey",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    ltiClientID?: string | null,
    oauthLoginUrl?: string | null,
    oauthTokenUrl?: string | null,
    apiClientId?: string | null,
    apiSecretKey?: string | null,
    lmsApiUrl?: string | null,
    ltiAccessToken?:  {
      __typename: "ModelLtiAccessTokenConnection",
      items:  Array< {
        __typename: "LtiAccessToken",
        id: string,
        ltiApiKeyID: string,
        ltiClientID?: string | null,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        loreeUsername?: string | null,
        loreePassword?: string | null,
        user?: string | null,
        userInfo?: string | null,
        roles?: string | null,
        accessToken?: string | null,
        refreshToken?: string | null,
        isAdmin?: boolean | null,
        generatedAt?: string | null,
        lmsApiUrl?: string | null,
        expiresAt?: string | null,
        lms_email?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiApiKeyMutationVariables = {
  input?: DeleteLtiApiKeyInput,
  condition?: ModelLtiApiKeyConditionInput | null,
};

export type DeleteLtiApiKeyMutation = {
  deleteLtiApiKey?:  {
    __typename: "LtiApiKey",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    ltiClientID?: string | null,
    oauthLoginUrl?: string | null,
    oauthTokenUrl?: string | null,
    apiClientId?: string | null,
    apiSecretKey?: string | null,
    lmsApiUrl?: string | null,
    ltiAccessToken?:  {
      __typename: "ModelLtiAccessTokenConnection",
      items:  Array< {
        __typename: "LtiAccessToken",
        id: string,
        ltiApiKeyID: string,
        ltiClientID?: string | null,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        loreeUsername?: string | null,
        loreePassword?: string | null,
        user?: string | null,
        userInfo?: string | null,
        roles?: string | null,
        accessToken?: string | null,
        refreshToken?: string | null,
        isAdmin?: boolean | null,
        generatedAt?: string | null,
        lmsApiUrl?: string | null,
        expiresAt?: string | null,
        lms_email?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiPlatformKeyMutationVariables = {
  input?: CreateLtiPlatformKeyInput,
  condition?: ModelLtiPlatformKeyConditionInput | null,
};

export type CreateLtiPlatformKeyMutation = {
  createLtiPlatformKey?:  {
    __typename: "LtiPlatformKey",
    id: string,
    ltiPlatformID: string,
    kid?: string | null,
    privatekey?: string | null,
    publicKey?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiPlatformKeyMutationVariables = {
  input?: UpdateLtiPlatformKeyInput,
  condition?: ModelLtiPlatformKeyConditionInput | null,
};

export type UpdateLtiPlatformKeyMutation = {
  updateLtiPlatformKey?:  {
    __typename: "LtiPlatformKey",
    id: string,
    ltiPlatformID: string,
    kid?: string | null,
    privatekey?: string | null,
    publicKey?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiPlatformKeyMutationVariables = {
  input?: DeleteLtiPlatformKeyInput,
  condition?: ModelLtiPlatformKeyConditionInput | null,
};

export type DeleteLtiPlatformKeyMutation = {
  deleteLtiPlatformKey?:  {
    __typename: "LtiPlatformKey",
    id: string,
    ltiPlatformID: string,
    kid?: string | null,
    privatekey?: string | null,
    publicKey?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiAccessTokenMutationVariables = {
  input?: CreateLtiAccessTokenInput,
  condition?: ModelLtiAccessTokenConditionInput | null,
};

export type CreateLtiAccessTokenMutation = {
  createLtiAccessToken?:  {
    __typename: "LtiAccessToken",
    id: string,
    ltiApiKeyID: string,
    ltiClientID?: string | null,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    loreeUsername?: string | null,
    loreePassword?: string | null,
    user?: string | null,
    userInfo?: string | null,
    roles?: string | null,
    accessToken?: string | null,
    refreshToken?: string | null,
    isAdmin?: boolean | null,
    generatedAt?: string | null,
    lmsApiUrl?: string | null,
    expiresAt?: string | null,
    lms_email?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiAccessTokenMutationVariables = {
  input?: UpdateLtiAccessTokenInput,
  condition?: ModelLtiAccessTokenConditionInput | null,
};

export type UpdateLtiAccessTokenMutation = {
  updateLtiAccessToken?:  {
    __typename: "LtiAccessToken",
    id: string,
    ltiApiKeyID: string,
    ltiClientID?: string | null,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    loreeUsername?: string | null,
    loreePassword?: string | null,
    user?: string | null,
    userInfo?: string | null,
    roles?: string | null,
    accessToken?: string | null,
    refreshToken?: string | null,
    isAdmin?: boolean | null,
    generatedAt?: string | null,
    lmsApiUrl?: string | null,
    expiresAt?: string | null,
    lms_email?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiAccessTokenMutationVariables = {
  input?: DeleteLtiAccessTokenInput,
  condition?: ModelLtiAccessTokenConditionInput | null,
};

export type DeleteLtiAccessTokenMutation = {
  deleteLtiAccessToken?:  {
    __typename: "LtiAccessToken",
    id: string,
    ltiApiKeyID: string,
    ltiClientID?: string | null,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    loreeUsername?: string | null,
    loreePassword?: string | null,
    user?: string | null,
    userInfo?: string | null,
    roles?: string | null,
    accessToken?: string | null,
    refreshToken?: string | null,
    isAdmin?: boolean | null,
    generatedAt?: string | null,
    lmsApiUrl?: string | null,
    expiresAt?: string | null,
    lms_email?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiLogMutationVariables = {
  input?: CreateLtiLogInput,
  condition?: ModelLtiLogConditionInput | null,
};

export type CreateLtiLogMutation = {
  createLtiLog?:  {
    __typename: "LtiLog",
    id: string,
    ltiPlatformID: string,
    userInfo?: string | null,
    clientInfo?: string | null,
    userGeoLocation?: string | null,
    userAction?: string | null,
    generatedAt?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiLogMutationVariables = {
  input?: UpdateLtiLogInput,
  condition?: ModelLtiLogConditionInput | null,
};

export type UpdateLtiLogMutation = {
  updateLtiLog?:  {
    __typename: "LtiLog",
    id: string,
    ltiPlatformID: string,
    userInfo?: string | null,
    clientInfo?: string | null,
    userGeoLocation?: string | null,
    userAction?: string | null,
    generatedAt?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiLogMutationVariables = {
  input?: DeleteLtiLogInput,
  condition?: ModelLtiLogConditionInput | null,
};

export type DeleteLtiLogMutation = {
  deleteLtiLog?:  {
    __typename: "LtiLog",
    id: string,
    ltiPlatformID: string,
    userInfo?: string | null,
    clientInfo?: string | null,
    userGeoLocation?: string | null,
    userAction?: string | null,
    generatedAt?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateCustomBlockMutationVariables = {
  input?: CreateCustomBlockInput,
  condition?: ModelCustomBlockConditionInput | null,
};

export type CreateCustomBlockMutation = {
  createCustomBlock?:  {
    __typename: "CustomBlock",
    id: string,
    title: string,
    categoryID: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    type: string,
    content?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateCustomBlockMutationVariables = {
  input?: UpdateCustomBlockInput,
  condition?: ModelCustomBlockConditionInput | null,
};

export type UpdateCustomBlockMutation = {
  updateCustomBlock?:  {
    __typename: "CustomBlock",
    id: string,
    title: string,
    categoryID: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    type: string,
    content?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteCustomBlockMutationVariables = {
  input?: DeleteCustomBlockInput,
  condition?: ModelCustomBlockConditionInput | null,
};

export type DeleteCustomBlockMutation = {
  deleteCustomBlock?:  {
    __typename: "CustomBlock",
    id: string,
    title: string,
    categoryID: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    type: string,
    content?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateGlobalBlocksMutationVariables = {
  input?: CreateGlobalBlocksInput,
  condition?: ModelGlobalBlocksConditionInput | null,
};

export type CreateGlobalBlocksMutation = {
  createGlobalBlocks?:  {
    __typename: "GlobalBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customBlockID: string,
    title: string,
    type: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateGlobalBlocksMutationVariables = {
  input?: UpdateGlobalBlocksInput,
  condition?: ModelGlobalBlocksConditionInput | null,
};

export type UpdateGlobalBlocksMutation = {
  updateGlobalBlocks?:  {
    __typename: "GlobalBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customBlockID: string,
    title: string,
    type: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteGlobalBlocksMutationVariables = {
  input?: DeleteGlobalBlocksInput,
  condition?: ModelGlobalBlocksConditionInput | null,
};

export type DeleteGlobalBlocksMutation = {
  deleteGlobalBlocks?:  {
    __typename: "GlobalBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customBlockID: string,
    title: string,
    type: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateSharedBlocksMutationVariables = {
  input?: CreateSharedBlocksInput,
  condition?: ModelSharedBlocksConditionInput | null,
};

export type CreateSharedBlocksMutation = {
  createSharedBlocks?:  {
    __typename: "SharedBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    type: string,
    customBlockID?: string | null,
    categoryID: string,
    sharedAccountId: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateSharedBlocksMutationVariables = {
  input?: UpdateSharedBlocksInput,
  condition?: ModelSharedBlocksConditionInput | null,
};

export type UpdateSharedBlocksMutation = {
  updateSharedBlocks?:  {
    __typename: "SharedBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    type: string,
    customBlockID?: string | null,
    categoryID: string,
    sharedAccountId: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteSharedBlocksMutationVariables = {
  input?: DeleteSharedBlocksInput,
  condition?: ModelSharedBlocksConditionInput | null,
};

export type DeleteSharedBlocksMutation = {
  deleteSharedBlocks?:  {
    __typename: "SharedBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    type: string,
    customBlockID?: string | null,
    categoryID: string,
    sharedAccountId: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateCustomStyleMutationVariables = {
  input?: CreateCustomStyleInput,
  condition?: ModelCustomStyleConditionInput | null,
};

export type CreateCustomStyleMutation = {
  createCustomStyle?:  {
    __typename: "CustomStyle",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    customColor?: string | null,
    customHeader?: string | null,
    customFont?: string | null,
    customLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateCustomStyleMutationVariables = {
  input?: UpdateCustomStyleInput,
  condition?: ModelCustomStyleConditionInput | null,
};

export type UpdateCustomStyleMutation = {
  updateCustomStyle?:  {
    __typename: "CustomStyle",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    customColor?: string | null,
    customHeader?: string | null,
    customFont?: string | null,
    customLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteCustomStyleMutationVariables = {
  input?: DeleteCustomStyleInput,
  condition?: ModelCustomStyleConditionInput | null,
};

export type DeleteCustomStyleMutation = {
  deleteCustomStyle?:  {
    __typename: "CustomStyle",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    customColor?: string | null,
    customHeader?: string | null,
    customFont?: string | null,
    customLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateKalturaConfigMutationVariables = {
  input?: CreateKalturaConfigInput,
  condition?: ModelKalturaConfigConditionInput | null,
};

export type CreateKalturaConfigMutation = {
  createKalturaConfig?:  {
    __typename: "KalturaConfig",
    id: string,
    ltiPlatformID: string,
    partnerID?: string | null,
    appToken?: string | null,
    tokenID?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateKalturaConfigMutationVariables = {
  input?: UpdateKalturaConfigInput,
  condition?: ModelKalturaConfigConditionInput | null,
};

export type UpdateKalturaConfigMutation = {
  updateKalturaConfig?:  {
    __typename: "KalturaConfig",
    id: string,
    ltiPlatformID: string,
    partnerID?: string | null,
    appToken?: string | null,
    tokenID?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteKalturaConfigMutationVariables = {
  input?: DeleteKalturaConfigInput,
  condition?: ModelKalturaConfigConditionInput | null,
};

export type DeleteKalturaConfigMutation = {
  deleteKalturaConfig?:  {
    __typename: "KalturaConfig",
    id: string,
    ltiPlatformID: string,
    partnerID?: string | null,
    appToken?: string | null,
    tokenID?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLoreeFeatureMutationVariables = {
  input?: CreateLoreeFeatureInput,
  condition?: ModelLoreeFeatureConditionInput | null,
};

export type CreateLoreeFeatureMutation = {
  createLoreeFeature?:  {
    __typename: "LoreeFeature",
    id: string,
    ltiPlatformID: string,
    featureList?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLoreeFeatureMutationVariables = {
  input?: UpdateLoreeFeatureInput,
  condition?: ModelLoreeFeatureConditionInput | null,
};

export type UpdateLoreeFeatureMutation = {
  updateLoreeFeature?:  {
    __typename: "LoreeFeature",
    id: string,
    ltiPlatformID: string,
    featureList?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLoreeFeatureMutationVariables = {
  input?: DeleteLoreeFeatureInput,
  condition?: ModelLoreeFeatureConditionInput | null,
};

export type DeleteLoreeFeatureMutation = {
  deleteLoreeFeature?:  {
    __typename: "LoreeFeature",
    id: string,
    ltiPlatformID: string,
    featureList?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLoreeRoleMutationVariables = {
  input?: CreateLoreeRoleInput,
  condition?: ModelLoreeRoleConditionInput | null,
};

export type CreateLoreeRoleMutation = {
  createLoreeRole?:  {
    __typename: "LoreeRole",
    id: string,
    ltiPlatformID: string,
    name?: string | null,
    description?: string | null,
    featureList?: string | null,
    lmsLoreeRoles?:  {
      __typename: "ModelLmsLoreeRoleConnection",
      items:  Array< {
        __typename: "LmsLoreeRole",
        id: string,
        ltiPlatformID: string,
        loreeRoleID?:  {
          __typename: "LoreeRole",
          id: string,
          ltiPlatformID: string,
          name?: string | null,
          description?: string | null,
          featureList?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        lmsRole?: string | null,
        lmsBaseRoleType?: string | null,
        lmsRoleId?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLoreeRoleMutationVariables = {
  input?: UpdateLoreeRoleInput,
  condition?: ModelLoreeRoleConditionInput | null,
};

export type UpdateLoreeRoleMutation = {
  updateLoreeRole?:  {
    __typename: "LoreeRole",
    id: string,
    ltiPlatformID: string,
    name?: string | null,
    description?: string | null,
    featureList?: string | null,
    lmsLoreeRoles?:  {
      __typename: "ModelLmsLoreeRoleConnection",
      items:  Array< {
        __typename: "LmsLoreeRole",
        id: string,
        ltiPlatformID: string,
        loreeRoleID?:  {
          __typename: "LoreeRole",
          id: string,
          ltiPlatformID: string,
          name?: string | null,
          description?: string | null,
          featureList?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        lmsRole?: string | null,
        lmsBaseRoleType?: string | null,
        lmsRoleId?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLoreeRoleMutationVariables = {
  input?: DeleteLoreeRoleInput,
  condition?: ModelLoreeRoleConditionInput | null,
};

export type DeleteLoreeRoleMutation = {
  deleteLoreeRole?:  {
    __typename: "LoreeRole",
    id: string,
    ltiPlatformID: string,
    name?: string | null,
    description?: string | null,
    featureList?: string | null,
    lmsLoreeRoles?:  {
      __typename: "ModelLmsLoreeRoleConnection",
      items:  Array< {
        __typename: "LmsLoreeRole",
        id: string,
        ltiPlatformID: string,
        loreeRoleID?:  {
          __typename: "LoreeRole",
          id: string,
          ltiPlatformID: string,
          name?: string | null,
          description?: string | null,
          featureList?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        lmsRole?: string | null,
        lmsBaseRoleType?: string | null,
        lmsRoleId?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLmsLoreeRoleMutationVariables = {
  input?: CreateLmsLoreeRoleInput,
  condition?: ModelLmsLoreeRoleConditionInput | null,
};

export type CreateLmsLoreeRoleMutation = {
  createLmsLoreeRole?:  {
    __typename: "LmsLoreeRole",
    id: string,
    ltiPlatformID: string,
    loreeRoleID?:  {
      __typename: "LoreeRole",
      id: string,
      ltiPlatformID: string,
      name?: string | null,
      description?: string | null,
      featureList?: string | null,
      lmsLoreeRoles?:  {
        __typename: "ModelLmsLoreeRoleConnection",
        items:  Array< {
          __typename: "LmsLoreeRole",
          id: string,
          ltiPlatformID: string,
          lmsRole?: string | null,
          lmsBaseRoleType?: string | null,
          lmsRoleId?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    lmsRole?: string | null,
    lmsBaseRoleType?: string | null,
    lmsRoleId?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLmsLoreeRoleMutationVariables = {
  input?: UpdateLmsLoreeRoleInput,
  condition?: ModelLmsLoreeRoleConditionInput | null,
};

export type UpdateLmsLoreeRoleMutation = {
  updateLmsLoreeRole?:  {
    __typename: "LmsLoreeRole",
    id: string,
    ltiPlatformID: string,
    loreeRoleID?:  {
      __typename: "LoreeRole",
      id: string,
      ltiPlatformID: string,
      name?: string | null,
      description?: string | null,
      featureList?: string | null,
      lmsLoreeRoles?:  {
        __typename: "ModelLmsLoreeRoleConnection",
        items:  Array< {
          __typename: "LmsLoreeRole",
          id: string,
          ltiPlatformID: string,
          lmsRole?: string | null,
          lmsBaseRoleType?: string | null,
          lmsRoleId?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    lmsRole?: string | null,
    lmsBaseRoleType?: string | null,
    lmsRoleId?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLmsLoreeRoleMutationVariables = {
  input?: DeleteLmsLoreeRoleInput,
  condition?: ModelLmsLoreeRoleConditionInput | null,
};

export type DeleteLmsLoreeRoleMutation = {
  deleteLmsLoreeRole?:  {
    __typename: "LmsLoreeRole",
    id: string,
    ltiPlatformID: string,
    loreeRoleID?:  {
      __typename: "LoreeRole",
      id: string,
      ltiPlatformID: string,
      name?: string | null,
      description?: string | null,
      featureList?: string | null,
      lmsLoreeRoles?:  {
        __typename: "ModelLmsLoreeRoleConnection",
        items:  Array< {
          __typename: "LmsLoreeRole",
          id: string,
          ltiPlatformID: string,
          lmsRole?: string | null,
          lmsBaseRoleType?: string | null,
          lmsRoleId?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    lmsRole?: string | null,
    lmsBaseRoleType?: string | null,
    lmsRoleId?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateHFivePUsersMutationVariables = {
  input?: CreateHFivePUsersInput,
  condition?: ModelHFivePUsersConditionInput | null,
};

export type CreateHFivePUsersMutation = {
  createHFivePUsers?:  {
    __typename: "HFivePUsers",
    id: string,
    email: string,
    loreeUser: string,
    ltiPlatformID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateHFivePUsersMutationVariables = {
  input?: UpdateHFivePUsersInput,
  condition?: ModelHFivePUsersConditionInput | null,
};

export type UpdateHFivePUsersMutation = {
  updateHFivePUsers?:  {
    __typename: "HFivePUsers",
    id: string,
    email: string,
    loreeUser: string,
    ltiPlatformID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteHFivePUsersMutationVariables = {
  input?: DeleteHFivePUsersInput,
  condition?: ModelHFivePUsersConditionInput | null,
};

export type DeleteHFivePUsersMutation = {
  deleteHFivePUsers?:  {
    __typename: "HFivePUsers",
    id: string,
    email: string,
    loreeUser: string,
    ltiPlatformID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateInteractivesMetaDataMutationVariables = {
  input?: CreateInteractivesMetaDataInput,
  condition?: ModelInteractivesMetaDataConditionInput | null,
};

export type CreateInteractivesMetaDataMutation = {
  createInteractivesMetaData?:  {
    __typename: "InteractivesMetaData",
    id: string,
    ltiPlatformID?: string | null,
    metaKey: string,
    metaValue?: string | null,
    isRetired?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateInteractivesMetaDataMutationVariables = {
  input?: UpdateInteractivesMetaDataInput,
  condition?: ModelInteractivesMetaDataConditionInput | null,
};

export type UpdateInteractivesMetaDataMutation = {
  updateInteractivesMetaData?:  {
    __typename: "InteractivesMetaData",
    id: string,
    ltiPlatformID?: string | null,
    metaKey: string,
    metaValue?: string | null,
    isRetired?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteInteractivesMetaDataMutationVariables = {
  input?: DeleteInteractivesMetaDataInput,
  condition?: ModelInteractivesMetaDataConditionInput | null,
};

export type DeleteInteractivesMetaDataMutation = {
  deleteInteractivesMetaData?:  {
    __typename: "InteractivesMetaData",
    id: string,
    ltiPlatformID?: string | null,
    metaKey: string,
    metaValue?: string | null,
    isRetired?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLmsImageMutationVariables = {
  input?: CreateLmsImageInput,
  condition?: ModelLmsImageConditionInput | null,
};

export type CreateLmsImageMutation = {
  createLmsImage?:  {
    __typename: "LmsImage",
    id: string,
    lms: string,
    lmsImageId: string,
    lmsImageUUID?: string | null,
    lmsImageName: string,
    lmsImageDisplayName?: string | null,
    lmsImageURL: string,
    lmsImageType?: string | null,
    s3ObjectKey?: string | null,
    s3PublicObjectKey?: string | null,
    s3GlobalTemplateKey?: string | null,
    s3GlobalRowsKey?: string | null,
    s3GlobalElementsKey?: string | null,
    s3SharedTemplateKey?: string | null,
    s3SharedRowsKey?: string | null,
    s3SharedElementsKey?: string | null,
    lmsImageMetaData?: string | null,
    s3MetaData?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLmsImageMutationVariables = {
  input?: UpdateLmsImageInput,
  condition?: ModelLmsImageConditionInput | null,
};

export type UpdateLmsImageMutation = {
  updateLmsImage?:  {
    __typename: "LmsImage",
    id: string,
    lms: string,
    lmsImageId: string,
    lmsImageUUID?: string | null,
    lmsImageName: string,
    lmsImageDisplayName?: string | null,
    lmsImageURL: string,
    lmsImageType?: string | null,
    s3ObjectKey?: string | null,
    s3PublicObjectKey?: string | null,
    s3GlobalTemplateKey?: string | null,
    s3GlobalRowsKey?: string | null,
    s3GlobalElementsKey?: string | null,
    s3SharedTemplateKey?: string | null,
    s3SharedRowsKey?: string | null,
    s3SharedElementsKey?: string | null,
    lmsImageMetaData?: string | null,
    s3MetaData?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLmsImageMutationVariables = {
  input?: DeleteLmsImageInput,
  condition?: ModelLmsImageConditionInput | null,
};

export type DeleteLmsImageMutation = {
  deleteLmsImage?:  {
    __typename: "LmsImage",
    id: string,
    lms: string,
    lmsImageId: string,
    lmsImageUUID?: string | null,
    lmsImageName: string,
    lmsImageDisplayName?: string | null,
    lmsImageURL: string,
    lmsImageType?: string | null,
    s3ObjectKey?: string | null,
    s3PublicObjectKey?: string | null,
    s3GlobalTemplateKey?: string | null,
    s3GlobalRowsKey?: string | null,
    s3GlobalElementsKey?: string | null,
    s3SharedTemplateKey?: string | null,
    s3SharedRowsKey?: string | null,
    s3SharedElementsKey?: string | null,
    lmsImageMetaData?: string | null,
    s3MetaData?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLoreeOrganisationMutationVariables = {
  input?: CreateLoreeOrganisationInput,
  condition?: ModelLoreeOrganisationConditionInput | null,
};

export type CreateLoreeOrganisationMutation = {
  createLoreeOrganisation?:  {
    __typename: "LoreeOrganisation",
    id: string,
    name?: string | null,
    ltiPlatforms?:  {
      __typename: "ModelLtiPlatformConnection",
      items:  Array< {
        __typename: "LtiPlatform",
        id: string,
        clientId: string,
        clientSecret?: string | null,
        platformName?: string | null,
        platformUrl?: string | null,
        authEndpoint?: string | null,
        accesstokenEndpoint?: string | null,
        publicKeyURL?: string | null,
        authConfig?:  {
          __typename: "AuthDetail",
          method?: string | null,
          key?: string | null,
        } | null,
        googleAnalyticsTrackingId?: string | null,
        ltiDeploymentId?: string | null,
        loreeOrganisationID?: string | null,
        loreeOrganisation?:  {
          __typename: "LoreeOrganisation",
          id: string,
          name?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        ltiApiKey?:  {
          __typename: "ModelLtiApiKeyConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLoreeOrganisationMutationVariables = {
  input?: UpdateLoreeOrganisationInput,
  condition?: ModelLoreeOrganisationConditionInput | null,
};

export type UpdateLoreeOrganisationMutation = {
  updateLoreeOrganisation?:  {
    __typename: "LoreeOrganisation",
    id: string,
    name?: string | null,
    ltiPlatforms?:  {
      __typename: "ModelLtiPlatformConnection",
      items:  Array< {
        __typename: "LtiPlatform",
        id: string,
        clientId: string,
        clientSecret?: string | null,
        platformName?: string | null,
        platformUrl?: string | null,
        authEndpoint?: string | null,
        accesstokenEndpoint?: string | null,
        publicKeyURL?: string | null,
        authConfig?:  {
          __typename: "AuthDetail",
          method?: string | null,
          key?: string | null,
        } | null,
        googleAnalyticsTrackingId?: string | null,
        ltiDeploymentId?: string | null,
        loreeOrganisationID?: string | null,
        loreeOrganisation?:  {
          __typename: "LoreeOrganisation",
          id: string,
          name?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        ltiApiKey?:  {
          __typename: "ModelLtiApiKeyConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLoreeOrganisationMutationVariables = {
  input?: DeleteLoreeOrganisationInput,
  condition?: ModelLoreeOrganisationConditionInput | null,
};

export type DeleteLoreeOrganisationMutation = {
  deleteLoreeOrganisation?:  {
    __typename: "LoreeOrganisation",
    id: string,
    name?: string | null,
    ltiPlatforms?:  {
      __typename: "ModelLtiPlatformConnection",
      items:  Array< {
        __typename: "LtiPlatform",
        id: string,
        clientId: string,
        clientSecret?: string | null,
        platformName?: string | null,
        platformUrl?: string | null,
        authEndpoint?: string | null,
        accesstokenEndpoint?: string | null,
        publicKeyURL?: string | null,
        authConfig?:  {
          __typename: "AuthDetail",
          method?: string | null,
          key?: string | null,
        } | null,
        googleAnalyticsTrackingId?: string | null,
        ltiDeploymentId?: string | null,
        loreeOrganisationID?: string | null,
        loreeOrganisation?:  {
          __typename: "LoreeOrganisation",
          id: string,
          name?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        ltiApiKey?:  {
          __typename: "ModelLtiApiKeyConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLoreeUserMutationVariables = {
  input?: CreateLoreeUserInput,
  condition?: ModelLoreeUserConditionInput | null,
};

export type CreateLoreeUserMutation = {
  createLoreeUser?:  {
    __typename: "LoreeUser",
    id: string,
    loreeOrganisationID?: string | null,
    name?: string | null,
    email?: string | null,
    username?: string | null,
    externalIds?:  Array< {
      __typename: "ExternalId",
      key?: string | null,
      value?: string | null,
    } | null > | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLoreeUserMutationVariables = {
  input?: UpdateLoreeUserInput,
  condition?: ModelLoreeUserConditionInput | null,
};

export type UpdateLoreeUserMutation = {
  updateLoreeUser?:  {
    __typename: "LoreeUser",
    id: string,
    loreeOrganisationID?: string | null,
    name?: string | null,
    email?: string | null,
    username?: string | null,
    externalIds?:  Array< {
      __typename: "ExternalId",
      key?: string | null,
      value?: string | null,
    } | null > | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLoreeUserMutationVariables = {
  input?: DeleteLoreeUserInput,
  condition?: ModelLoreeUserConditionInput | null,
};

export type DeleteLoreeUserMutation = {
  deleteLoreeUser?:  {
    __typename: "LoreeUser",
    id: string,
    loreeOrganisationID?: string | null,
    name?: string | null,
    email?: string | null,
    username?: string | null,
    externalIds?:  Array< {
      __typename: "ExternalId",
      key?: string | null,
      value?: string | null,
    } | null > | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiToolMutationVariables = {
  input?: CreateLtiToolInput,
  condition?: ModelLtiToolConditionInput | null,
};

export type CreateLtiToolMutation = {
  createLtiTool?:  {
    __typename: "LtiTool",
    id: string,
    ltiPlatformID?: string | null,
    toolName: string,
    issuerUrl: string,
    oidcUrl: string,
    redirectURI: string,
    targetLinkURI: string,
    jwksUrl: string,
    domainName: string,
    clientId: string,
    clientSecret?: string | null,
    toolDeployments?:  {
      __typename: "ModelLtiToolDeploymentConnection",
      items:  Array< {
        __typename: "LtiToolDeployment",
        id: string,
        toolId?:  {
          __typename: "LtiTool",
          id: string,
          ltiPlatformID?: string | null,
          toolName: string,
          issuerUrl: string,
          oidcUrl: string,
          redirectURI: string,
          targetLinkURI: string,
          jwksUrl: string,
          domainName: string,
          clientId: string,
          clientSecret?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiToolMutationVariables = {
  input?: UpdateLtiToolInput,
  condition?: ModelLtiToolConditionInput | null,
};

export type UpdateLtiToolMutation = {
  updateLtiTool?:  {
    __typename: "LtiTool",
    id: string,
    ltiPlatformID?: string | null,
    toolName: string,
    issuerUrl: string,
    oidcUrl: string,
    redirectURI: string,
    targetLinkURI: string,
    jwksUrl: string,
    domainName: string,
    clientId: string,
    clientSecret?: string | null,
    toolDeployments?:  {
      __typename: "ModelLtiToolDeploymentConnection",
      items:  Array< {
        __typename: "LtiToolDeployment",
        id: string,
        toolId?:  {
          __typename: "LtiTool",
          id: string,
          ltiPlatformID?: string | null,
          toolName: string,
          issuerUrl: string,
          oidcUrl: string,
          redirectURI: string,
          targetLinkURI: string,
          jwksUrl: string,
          domainName: string,
          clientId: string,
          clientSecret?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiToolMutationVariables = {
  input?: DeleteLtiToolInput,
  condition?: ModelLtiToolConditionInput | null,
};

export type DeleteLtiToolMutation = {
  deleteLtiTool?:  {
    __typename: "LtiTool",
    id: string,
    ltiPlatformID?: string | null,
    toolName: string,
    issuerUrl: string,
    oidcUrl: string,
    redirectURI: string,
    targetLinkURI: string,
    jwksUrl: string,
    domainName: string,
    clientId: string,
    clientSecret?: string | null,
    toolDeployments?:  {
      __typename: "ModelLtiToolDeploymentConnection",
      items:  Array< {
        __typename: "LtiToolDeployment",
        id: string,
        toolId?:  {
          __typename: "LtiTool",
          id: string,
          ltiPlatformID?: string | null,
          toolName: string,
          issuerUrl: string,
          oidcUrl: string,
          redirectURI: string,
          targetLinkURI: string,
          jwksUrl: string,
          domainName: string,
          clientId: string,
          clientSecret?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiToolLoginHintMutationVariables = {
  input?: CreateLtiToolLoginHintInput,
  condition?: ModelLtiToolLoginHintConditionInput | null,
};

export type CreateLtiToolLoginHintMutation = {
  createLtiToolLoginHint?:  {
    __typename: "LtiToolLoginHint",
    id: string,
    issuerUrl: string,
    clientId: string,
    loreeUserEmail: string,
    messageType?: string | null,
    resourceLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiToolLoginHintMutationVariables = {
  input?: UpdateLtiToolLoginHintInput,
  condition?: ModelLtiToolLoginHintConditionInput | null,
};

export type UpdateLtiToolLoginHintMutation = {
  updateLtiToolLoginHint?:  {
    __typename: "LtiToolLoginHint",
    id: string,
    issuerUrl: string,
    clientId: string,
    loreeUserEmail: string,
    messageType?: string | null,
    resourceLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiToolLoginHintMutationVariables = {
  input?: DeleteLtiToolLoginHintInput,
  condition?: ModelLtiToolLoginHintConditionInput | null,
};

export type DeleteLtiToolLoginHintMutation = {
  deleteLtiToolLoginHint?:  {
    __typename: "LtiToolLoginHint",
    id: string,
    issuerUrl: string,
    clientId: string,
    loreeUserEmail: string,
    messageType?: string | null,
    resourceLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateLtiToolDeploymentMutationVariables = {
  input?: CreateLtiToolDeploymentInput,
  condition?: ModelLtiToolDeploymentConditionInput | null,
};

export type CreateLtiToolDeploymentMutation = {
  createLtiToolDeployment?:  {
    __typename: "LtiToolDeployment",
    id: string,
    toolId?:  {
      __typename: "LtiTool",
      id: string,
      ltiPlatformID?: string | null,
      toolName: string,
      issuerUrl: string,
      oidcUrl: string,
      redirectURI: string,
      targetLinkURI: string,
      jwksUrl: string,
      domainName: string,
      clientId: string,
      clientSecret?: string | null,
      toolDeployments?:  {
        __typename: "ModelLtiToolDeploymentConnection",
        items:  Array< {
          __typename: "LtiToolDeployment",
          id: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateLtiToolDeploymentMutationVariables = {
  input?: UpdateLtiToolDeploymentInput,
  condition?: ModelLtiToolDeploymentConditionInput | null,
};

export type UpdateLtiToolDeploymentMutation = {
  updateLtiToolDeployment?:  {
    __typename: "LtiToolDeployment",
    id: string,
    toolId?:  {
      __typename: "LtiTool",
      id: string,
      ltiPlatformID?: string | null,
      toolName: string,
      issuerUrl: string,
      oidcUrl: string,
      redirectURI: string,
      targetLinkURI: string,
      jwksUrl: string,
      domainName: string,
      clientId: string,
      clientSecret?: string | null,
      toolDeployments?:  {
        __typename: "ModelLtiToolDeploymentConnection",
        items:  Array< {
          __typename: "LtiToolDeployment",
          id: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteLtiToolDeploymentMutationVariables = {
  input?: DeleteLtiToolDeploymentInput,
  condition?: ModelLtiToolDeploymentConditionInput | null,
};

export type DeleteLtiToolDeploymentMutation = {
  deleteLtiToolDeployment?:  {
    __typename: "LtiToolDeployment",
    id: string,
    toolId?:  {
      __typename: "LtiTool",
      id: string,
      ltiPlatformID?: string | null,
      toolName: string,
      issuerUrl: string,
      oidcUrl: string,
      redirectURI: string,
      targetLinkURI: string,
      jwksUrl: string,
      domainName: string,
      clientId: string,
      clientSecret?: string | null,
      toolDeployments?:  {
        __typename: "ModelLtiToolDeploymentConnection",
        items:  Array< {
          __typename: "LtiToolDeployment",
          id: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateCustomThemeMutationVariables = {
  input?: CreateCustomThemeInput,
  condition?: ModelCustomThemeConditionInput | null,
};

export type CreateCustomThemeMutation = {
  createCustomTheme?:  {
    __typename: "CustomTheme",
    id: string,
    ltiPlatformID: string,
    isShared?: boolean | null,
    themeData: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateCustomThemeMutationVariables = {
  input?: UpdateCustomThemeInput,
  condition?: ModelCustomThemeConditionInput | null,
};

export type UpdateCustomThemeMutation = {
  updateCustomTheme?:  {
    __typename: "CustomTheme",
    id: string,
    ltiPlatformID: string,
    isShared?: boolean | null,
    themeData: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteCustomThemeMutationVariables = {
  input?: DeleteCustomThemeInput,
  condition?: ModelCustomThemeConditionInput | null,
};

export type DeleteCustomThemeMutation = {
  deleteCustomTheme?:  {
    __typename: "CustomTheme",
    id: string,
    ltiPlatformID: string,
    isShared?: boolean | null,
    themeData: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CanvasModulesQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
};

export type CanvasModulesQuery = {
  canvasModules?: string | null,
};

export type CanvasModuleItemsQueryVariables = {
  courseId?: string | null,
  moduleId?: string | null,
};

export type CanvasModuleItemsQuery = {
  canvasModuleItems?: string | null,
};

export type CanvasPagesQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
  orderType?: string | null,
  sortingValue?: string | null,
};

export type CanvasPagesQuery = {
  canvasPages?: string | null,
};

export type CanvasAssignmentsQueryVariables = {
  courseId?: string | null,
};

export type CanvasAssignmentsQuery = {
  canvasAssignments?: string | null,
};

export type CanvasCourseNavigationQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
};

export type CanvasCourseNavigationQuery = {
  canvasCourseNavigation?: string | null,
};

export type CanvasAnnouncementsQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
};

export type CanvasAnnouncementsQuery = {
  canvasAnnouncements?: string | null,
};

export type CanvasQuizzesQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
};

export type CanvasQuizzesQuery = {
  canvasQuizzes?: string | null,
};

export type CanvasFilesQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
};

export type CanvasFilesQuery = {
  canvasFiles?: string | null,
};

export type CanvasDiscussionsQueryVariables = {
  courseId?: string | null,
  pageIndex?: string | null,
  pageItems?: string | null,
};

export type CanvasDiscussionsQuery = {
  canvasDiscussions?: string | null,
};

export type CanvasImagesQueryVariables = {
  courseId?: string | null,
};

export type CanvasImagesQuery = {
  canvasImages?: string | null,
};

export type KalturaVideosQueryVariables = {
  courseId?: string | null,
  platformId?: string | null,
};

export type KalturaVideosQuery = {
  kalturaVideos?: string | null,
};

export type GetKalturaVideoQueryVariables = {
  userId?: string | null,
  entryId?: string | null,
  platformId?: string | null,
};

export type GetKalturaVideoQuery = {
  getKalturaVideo?: string | null,
};

export type InterativeLibraryQueryVariables = {
  userId?: string | null,
};

export type InterativeLibraryQuery = {
  interativeLibrary?: string | null,
};

export type InteractiveContentQueryVariables = {
  userId?: string | null,
  orgId?: string | null,
};

export type InteractiveContentQuery = {
  interactiveContent?: string | null,
};

export type InteractiveContentPaginateQueryVariables = {
  userId?: string | null,
  orgId?: string | null,
  pageId?: string | null,
  pageLimit?: string | null,
};

export type InteractiveContentPaginateQuery = {
  interactiveContentPaginate?: string | null,
};

export type ListLoreeInteractiveQueryVariables = {
  organization?: string | null,
  email?: string | null,
};

export type ListLoreeInteractiveQuery = {
  listLoreeInteractive?: string | null,
};

export type AdminAccountsQuery = {
  adminAccounts?: string | null,
};

export type AdminSubAccountsQueryVariables = {
  accountId?: string | null,
};

export type AdminSubAccountsQuery = {
  adminSubAccounts?: string | null,
};

export type AdminRolesQueryVariables = {
  loreeUserEmail?: string | null,
  accountId?: string | null,
};

export type AdminRolesQuery = {
  adminRoles?: string | null,
};

export type CourseDetailsQueryVariables = {
  courseId?: string | null,
};

export type CourseDetailsQuery = {
  courseDetails?: string | null,
};

export type AccountByCourseQueryVariables = {
  accountId?: string | null,
};

export type AccountByCourseQuery = {
  accountByCourse?: string | null,
};

export type CanvasClientQueryVariables = {
  platformId?: string | null,
};

export type CanvasClientQuery = {
  canvasClient?: string | null,
};

export type RefreshPlatformKeysQueryVariables = {
  platformId?: string | null,
};

export type RefreshPlatformKeysQuery = {
  refreshPlatformKeys?: string | null,
};

export type GetH5PContentQueryVariables = {
  userName?: string | null,
  platformId?: string | null,
};

export type GetH5PContentQuery = {
  getH5PContent?: string | null,
};

export type GetAdminH5PContentQueryVariables = {
  platformId?: string | null,
};

export type GetAdminH5PContentQuery = {
  getAdminH5PContent?: string | null,
};

export type AdminDashboardInteractiveQueryVariables = {
  organization?: string | null,
  email?: string | null,
  platformId?: string | null,
};

export type AdminDashboardInteractiveQuery = {
  AdminDashboardInteractive?: string | null,
};

export type D2lModulesQueryVariables = {
  courseId?: string | null,
};

export type D2lModulesQuery = {
  d2lModules?: string | null,
};

export type ViewD2lContentsQueryVariables = {
  courseId?: string | null,
  contentId?: string | null,
  contentType?: string | null,
};

export type ViewD2lContentsQuery = {
  viewD2lContents?: string | null,
};

export type ViewBbCoursesQueryVariables = {
  courseId?: string | null,
};

export type ViewBbCoursesQuery = {
  viewBbCourses?: string | null,
};

export type ViewBbContentsQueryVariables = {
  courseId?: string | null,
};

export type ViewBbContentsQuery = {
  viewBbContents?: string | null,
};

export type ListBbCourseImagesQueryVariables = {
  courseId?: string | null,
};

export type ListBbCourseImagesQuery = {
  listBbCourseImages?: string | null,
};

export type ViewBbContentsByIdQueryVariables = {
  courseId?: string | null,
  contentId?: string | null,
};

export type ViewBbContentsByIdQuery = {
  viewBbContentsById?: string | null,
};

export type ViewBbPageContentsByIdQueryVariables = {
  courseId?: string | null,
  contentId?: string | null,
};

export type ViewBbPageContentsByIdQuery = {
  viewBbPageContentsById?: string | null,
};

export type D2lAdminRolesQuery = {
  d2lAdminRoles?: string | null,
};

export type DashboardStatisticsQuery = {
  dashboardStatistics?: string | null,
};

export type FetchS3ImagesQueryVariables = {
  platformId?: string | null,
  lmsUrl?: string | null,
};

export type FetchS3ImagesQuery = {
  fetchS3Images?: string | null,
};

export type GetOrganizationAccountsQueryVariables = {
  email?: string | null,
};

export type GetOrganizationAccountsQuery = {
  getOrganizationAccounts?: string | null,
};

export type FetchInstitutionRolesQuery = {
  fetchInstitutionRoles?: string | null,
};

export type CourseEnrollmentQueryVariables = {
  courseId?: string | null,
};

export type CourseEnrollmentQuery = {
  courseEnrollment?: string | null,
};

export type ListChildCourseImagesQueryVariables = {
  courseId?: string | null,
  resourceId?: string | null,
};

export type ListChildCourseImagesQuery = {
  listChildCourseImages?: string | null,
};

export type ListCourseImagesByIdQueryVariables = {
  courseId?: string | null,
  resourceId?: string | null,
};

export type ListCourseImagesByIdQuery = {
  listCourseImagesById?: string | null,
};

export type MigrateUserQueryVariables = {
  email?: string | null,
};

export type MigrateUserQuery = {
  migrateUser?: string | null,
};

export type GetCategoryQueryVariables = {
  id?: string,
};

export type GetCategoryQuery = {
  getCategory?:  {
    __typename: "Category",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    name: string,
    projects?:  {
      __typename: "ModelProjectConnection",
      items:  Array< {
        __typename: "Project",
        id: string,
        title: string,
        categoryID: string,
        category?:  {
          __typename: "Category",
          id: string,
          ltiPlatformID?: string | null,
          loreeOrganisationID?: string | null,
          name: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        pages?:  {
          __typename: "ModelPageConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListCategorysQueryVariables = {
  filter?: ModelCategoryFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCategorysQuery = {
  listCategorys?:  {
    __typename: "ModelCategoryConnection",
    items:  Array< {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetProjectQueryVariables = {
  id?: string,
};

export type GetProjectQuery = {
  getProject?:  {
    __typename: "Project",
    id: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    pages?:  {
      __typename: "ModelPageConnection",
      items:  Array< {
        __typename: "Page",
        id: string,
        projectID: string,
        project?:  {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        title: string,
        content?: string | null,
        state?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListProjectsQueryVariables = {
  filter?: ModelProjectFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListProjectsQuery = {
  listProjects?:  {
    __typename: "ModelProjectConnection",
    items:  Array< {
      __typename: "Project",
      id: string,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      pages?:  {
        __typename: "ModelPageConnection",
        items:  Array< {
          __typename: "Page",
          id: string,
          projectID: string,
          title: string,
          content?: string | null,
          state?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetPageQueryVariables = {
  id?: string,
};

export type GetPageQuery = {
  getPage?:  {
    __typename: "Page",
    id: string,
    projectID: string,
    project?:  {
      __typename: "Project",
      id: string,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      pages?:  {
        __typename: "ModelPageConnection",
        items:  Array< {
          __typename: "Page",
          id: string,
          projectID: string,
          title: string,
          content?: string | null,
          state?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    title: string,
    content?: string | null,
    state?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListPagesQueryVariables = {
  filter?: ModelPageFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListPagesQuery = {
  listPages?:  {
    __typename: "ModelPageConnection",
    items:  Array< {
      __typename: "Page",
      id: string,
      projectID: string,
      project?:  {
        __typename: "Project",
        id: string,
        title: string,
        categoryID: string,
        category?:  {
          __typename: "Category",
          id: string,
          ltiPlatformID?: string | null,
          loreeOrganisationID?: string | null,
          name: string,
          createdAt: string,
          updatedAt: string,
        } | null,
        pages?:  {
          __typename: "ModelPageConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      title: string,
      content?: string | null,
      state?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetCustomTemplateQueryVariables = {
  id?: string,
};

export type GetCustomTemplateQuery = {
  getCustomTemplate?:  {
    __typename: "CustomTemplate",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListCustomTemplatesQueryVariables = {
  filter?: ModelCustomTemplateFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCustomTemplatesQuery = {
  listCustomTemplates?:  {
    __typename: "ModelCustomTemplateConnection",
    items:  Array< {
      __typename: "CustomTemplate",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      content?: string | null,
      status?: boolean | null,
      isGlobal?: boolean | null,
      isShared?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      owner?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetGlobalTemplatesQueryVariables = {
  id?: string,
};

export type GetGlobalTemplatesQuery = {
  getGlobalTemplates?:  {
    __typename: "GlobalTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customTemplateID: string,
    title: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListGlobalTemplatessQueryVariables = {
  filter?: ModelGlobalTemplatesFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListGlobalTemplatessQuery = {
  listGlobalTemplatess?:  {
    __typename: "ModelGlobalTemplatesConnection",
    items:  Array< {
      __typename: "GlobalTemplates",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      customTemplateID: string,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      content?: string | null,
      status?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetSharedTemplatesQueryVariables = {
  id?: string,
};

export type GetSharedTemplatesQuery = {
  getSharedTemplates?:  {
    __typename: "SharedTemplates",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    customTemplateID: string,
    sharedAccountId: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListSharedTemplatessQueryVariables = {
  filter?: ModelSharedTemplatesFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListSharedTemplatessQuery = {
  listSharedTemplatess?:  {
    __typename: "ModelSharedTemplatesConnection",
    items:  Array< {
      __typename: "SharedTemplates",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      title: string,
      customTemplateID: string,
      sharedAccountId: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      content?: string | null,
      status?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetFileQueryVariables = {
  id?: string,
};

export type GetFileQuery = {
  getFile?:  {
    __typename: "File",
    id: string,
    name: string,
    size?: string | null,
    location?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    type?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListFilesQueryVariables = {
  filter?: ModelFileFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListFilesQuery = {
  listFiles?:  {
    __typename: "ModelFileConnection",
    items:  Array< {
      __typename: "File",
      id: string,
      name: string,
      size?: string | null,
      location?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      type?: string | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiPlatformQueryVariables = {
  id?: string,
};

export type GetLtiPlatformQuery = {
  getLtiPlatform?:  {
    __typename: "LtiPlatform",
    id: string,
    clientId: string,
    clientSecret?: string | null,
    platformName?: string | null,
    platformUrl?: string | null,
    authEndpoint?: string | null,
    accesstokenEndpoint?: string | null,
    publicKeyURL?: string | null,
    authConfig?:  {
      __typename: "AuthDetail",
      method?: string | null,
      key?: string | null,
    } | null,
    googleAnalyticsTrackingId?: string | null,
    ltiDeploymentId?: string | null,
    loreeOrganisationID?: string | null,
    loreeOrganisation?:  {
      __typename: "LoreeOrganisation",
      id: string,
      name?: string | null,
      ltiPlatforms?:  {
        __typename: "ModelLtiPlatformConnection",
        items:  Array< {
          __typename: "LtiPlatform",
          id: string,
          clientId: string,
          clientSecret?: string | null,
          platformName?: string | null,
          platformUrl?: string | null,
          authEndpoint?: string | null,
          accesstokenEndpoint?: string | null,
          publicKeyURL?: string | null,
          googleAnalyticsTrackingId?: string | null,
          ltiDeploymentId?: string | null,
          loreeOrganisationID?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    ltiApiKey?:  {
      __typename: "ModelLtiApiKeyConnection",
      items:  Array< {
        __typename: "LtiApiKey",
        id: string,
        ltiPlatformID: string,
        loreeOrganisationID?: string | null,
        ltiClientID?: string | null,
        oauthLoginUrl?: string | null,
        oauthTokenUrl?: string | null,
        apiClientId?: string | null,
        apiSecretKey?: string | null,
        lmsApiUrl?: string | null,
        ltiAccessToken?:  {
          __typename: "ModelLtiAccessTokenConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiPlatformsQueryVariables = {
  filter?: ModelLtiPlatformFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiPlatformsQuery = {
  listLtiPlatforms?:  {
    __typename: "ModelLtiPlatformConnection",
    items:  Array< {
      __typename: "LtiPlatform",
      id: string,
      clientId: string,
      clientSecret?: string | null,
      platformName?: string | null,
      platformUrl?: string | null,
      authEndpoint?: string | null,
      accesstokenEndpoint?: string | null,
      publicKeyURL?: string | null,
      authConfig?:  {
        __typename: "AuthDetail",
        method?: string | null,
        key?: string | null,
      } | null,
      googleAnalyticsTrackingId?: string | null,
      ltiDeploymentId?: string | null,
      loreeOrganisationID?: string | null,
      loreeOrganisation?:  {
        __typename: "LoreeOrganisation",
        id: string,
        name?: string | null,
        ltiPlatforms?:  {
          __typename: "ModelLtiPlatformConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      ltiApiKey?:  {
        __typename: "ModelLtiApiKeyConnection",
        items:  Array< {
          __typename: "LtiApiKey",
          id: string,
          ltiPlatformID: string,
          loreeOrganisationID?: string | null,
          ltiClientID?: string | null,
          oauthLoginUrl?: string | null,
          oauthTokenUrl?: string | null,
          apiClientId?: string | null,
          apiSecretKey?: string | null,
          lmsApiUrl?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiApiKeyQueryVariables = {
  id?: string,
};

export type GetLtiApiKeyQuery = {
  getLtiApiKey?:  {
    __typename: "LtiApiKey",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    ltiClientID?: string | null,
    oauthLoginUrl?: string | null,
    oauthTokenUrl?: string | null,
    apiClientId?: string | null,
    apiSecretKey?: string | null,
    lmsApiUrl?: string | null,
    ltiAccessToken?:  {
      __typename: "ModelLtiAccessTokenConnection",
      items:  Array< {
        __typename: "LtiAccessToken",
        id: string,
        ltiApiKeyID: string,
        ltiClientID?: string | null,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        loreeUsername?: string | null,
        loreePassword?: string | null,
        user?: string | null,
        userInfo?: string | null,
        roles?: string | null,
        accessToken?: string | null,
        refreshToken?: string | null,
        isAdmin?: boolean | null,
        generatedAt?: string | null,
        lmsApiUrl?: string | null,
        expiresAt?: string | null,
        lms_email?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiApiKeysQueryVariables = {
  filter?: ModelLtiApiKeyFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiApiKeysQuery = {
  listLtiApiKeys?:  {
    __typename: "ModelLtiApiKeyConnection",
    items:  Array< {
      __typename: "LtiApiKey",
      id: string,
      ltiPlatformID: string,
      loreeOrganisationID?: string | null,
      ltiClientID?: string | null,
      oauthLoginUrl?: string | null,
      oauthTokenUrl?: string | null,
      apiClientId?: string | null,
      apiSecretKey?: string | null,
      lmsApiUrl?: string | null,
      ltiAccessToken?:  {
        __typename: "ModelLtiAccessTokenConnection",
        items:  Array< {
          __typename: "LtiAccessToken",
          id: string,
          ltiApiKeyID: string,
          ltiClientID?: string | null,
          ltiPlatformID?: string | null,
          loreeOrganisationID?: string | null,
          loreeUsername?: string | null,
          loreePassword?: string | null,
          user?: string | null,
          userInfo?: string | null,
          roles?: string | null,
          accessToken?: string | null,
          refreshToken?: string | null,
          isAdmin?: boolean | null,
          generatedAt?: string | null,
          lmsApiUrl?: string | null,
          expiresAt?: string | null,
          lms_email?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiPlatformKeyQueryVariables = {
  id?: string,
};

export type GetLtiPlatformKeyQuery = {
  getLtiPlatformKey?:  {
    __typename: "LtiPlatformKey",
    id: string,
    ltiPlatformID: string,
    kid?: string | null,
    privatekey?: string | null,
    publicKey?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiPlatformKeysQueryVariables = {
  filter?: ModelLtiPlatformKeyFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiPlatformKeysQuery = {
  listLtiPlatformKeys?:  {
    __typename: "ModelLtiPlatformKeyConnection",
    items:  Array< {
      __typename: "LtiPlatformKey",
      id: string,
      ltiPlatformID: string,
      kid?: string | null,
      privatekey?: string | null,
      publicKey?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiAccessTokenQueryVariables = {
  id?: string,
};

export type GetLtiAccessTokenQuery = {
  getLtiAccessToken?:  {
    __typename: "LtiAccessToken",
    id: string,
    ltiApiKeyID: string,
    ltiClientID?: string | null,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    loreeUsername?: string | null,
    loreePassword?: string | null,
    user?: string | null,
    userInfo?: string | null,
    roles?: string | null,
    accessToken?: string | null,
    refreshToken?: string | null,
    isAdmin?: boolean | null,
    generatedAt?: string | null,
    lmsApiUrl?: string | null,
    expiresAt?: string | null,
    lms_email?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiAccessTokensQueryVariables = {
  filter?: ModelLtiAccessTokenFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiAccessTokensQuery = {
  listLtiAccessTokens?:  {
    __typename: "ModelLtiAccessTokenConnection",
    items:  Array< {
      __typename: "LtiAccessToken",
      id: string,
      ltiApiKeyID: string,
      ltiClientID?: string | null,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      loreeUsername?: string | null,
      loreePassword?: string | null,
      user?: string | null,
      userInfo?: string | null,
      roles?: string | null,
      accessToken?: string | null,
      refreshToken?: string | null,
      isAdmin?: boolean | null,
      generatedAt?: string | null,
      lmsApiUrl?: string | null,
      expiresAt?: string | null,
      lms_email?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiLogQueryVariables = {
  id?: string,
};

export type GetLtiLogQuery = {
  getLtiLog?:  {
    __typename: "LtiLog",
    id: string,
    ltiPlatformID: string,
    userInfo?: string | null,
    clientInfo?: string | null,
    userGeoLocation?: string | null,
    userAction?: string | null,
    generatedAt?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiLogsQueryVariables = {
  filter?: ModelLtiLogFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiLogsQuery = {
  listLtiLogs?:  {
    __typename: "ModelLtiLogConnection",
    items:  Array< {
      __typename: "LtiLog",
      id: string,
      ltiPlatformID: string,
      userInfo?: string | null,
      clientInfo?: string | null,
      userGeoLocation?: string | null,
      userAction?: string | null,
      generatedAt?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetCustomBlockQueryVariables = {
  id?: string,
};

export type GetCustomBlockQuery = {
  getCustomBlock?:  {
    __typename: "CustomBlock",
    id: string,
    title: string,
    categoryID: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    type: string,
    content?: string | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    status?: boolean | null,
    isGlobal?: boolean | null,
    isShared?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    owner?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListCustomBlocksQueryVariables = {
  filter?: ModelCustomBlockFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCustomBlocksQuery = {
  listCustomBlocks?:  {
    __typename: "ModelCustomBlockConnection",
    items:  Array< {
      __typename: "CustomBlock",
      id: string,
      title: string,
      categoryID: string,
      ltiPlatformID: string,
      loreeOrganisationID?: string | null,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      type: string,
      content?: string | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      status?: boolean | null,
      isGlobal?: boolean | null,
      isShared?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      owner?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetGlobalBlocksQueryVariables = {
  id?: string,
};

export type GetGlobalBlocksQuery = {
  getGlobalBlocks?:  {
    __typename: "GlobalBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    customBlockID: string,
    title: string,
    type: string,
    categoryID: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListGlobalBlockssQueryVariables = {
  filter?: ModelGlobalBlocksFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListGlobalBlockssQuery = {
  listGlobalBlockss?:  {
    __typename: "ModelGlobalBlocksConnection",
    items:  Array< {
      __typename: "GlobalBlocks",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      customBlockID: string,
      title: string,
      type: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      content?: string | null,
      status?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetSharedBlocksQueryVariables = {
  id?: string,
};

export type GetSharedBlocksQuery = {
  getSharedBlocks?:  {
    __typename: "SharedBlocks",
    id: string,
    ltiPlatformID?: string | null,
    loreeOrganisationID?: string | null,
    title: string,
    type: string,
    customBlockID?: string | null,
    categoryID: string,
    sharedAccountId: string,
    category?:  {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    thumbnail?:  {
      __typename: "S3Object",
      bucket: string,
      key: string,
      region: string,
    } | null,
    content?: string | null,
    status?: boolean | null,
    createdBy?: string | null,
    active?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListSharedBlockssQueryVariables = {
  filter?: ModelSharedBlocksFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListSharedBlockssQuery = {
  listSharedBlockss?:  {
    __typename: "ModelSharedBlocksConnection",
    items:  Array< {
      __typename: "SharedBlocks",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      title: string,
      type: string,
      customBlockID?: string | null,
      categoryID: string,
      sharedAccountId: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      content?: string | null,
      status?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetCustomStyleQueryVariables = {
  id?: string,
};

export type GetCustomStyleQuery = {
  getCustomStyle?:  {
    __typename: "CustomStyle",
    id: string,
    ltiPlatformID: string,
    loreeOrganisationID?: string | null,
    customColor?: string | null,
    customHeader?: string | null,
    customFont?: string | null,
    customLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListCustomStylesQueryVariables = {
  filter?: ModelCustomStyleFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCustomStylesQuery = {
  listCustomStyles?:  {
    __typename: "ModelCustomStyleConnection",
    items:  Array< {
      __typename: "CustomStyle",
      id: string,
      ltiPlatformID: string,
      loreeOrganisationID?: string | null,
      customColor?: string | null,
      customHeader?: string | null,
      customFont?: string | null,
      customLink?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetKalturaConfigQueryVariables = {
  id?: string,
};

export type GetKalturaConfigQuery = {
  getKalturaConfig?:  {
    __typename: "KalturaConfig",
    id: string,
    ltiPlatformID: string,
    partnerID?: string | null,
    appToken?: string | null,
    tokenID?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListKalturaConfigsQueryVariables = {
  filter?: ModelKalturaConfigFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListKalturaConfigsQuery = {
  listKalturaConfigs?:  {
    __typename: "ModelKalturaConfigConnection",
    items:  Array< {
      __typename: "KalturaConfig",
      id: string,
      ltiPlatformID: string,
      partnerID?: string | null,
      appToken?: string | null,
      tokenID?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLoreeFeatureQueryVariables = {
  id?: string,
};

export type GetLoreeFeatureQuery = {
  getLoreeFeature?:  {
    __typename: "LoreeFeature",
    id: string,
    ltiPlatformID: string,
    featureList?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLoreeFeaturesQueryVariables = {
  filter?: ModelLoreeFeatureFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLoreeFeaturesQuery = {
  listLoreeFeatures?:  {
    __typename: "ModelLoreeFeatureConnection",
    items:  Array< {
      __typename: "LoreeFeature",
      id: string,
      ltiPlatformID: string,
      featureList?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLoreeRoleQueryVariables = {
  id?: string,
};

export type GetLoreeRoleQuery = {
  getLoreeRole?:  {
    __typename: "LoreeRole",
    id: string,
    ltiPlatformID: string,
    name?: string | null,
    description?: string | null,
    featureList?: string | null,
    lmsLoreeRoles?:  {
      __typename: "ModelLmsLoreeRoleConnection",
      items:  Array< {
        __typename: "LmsLoreeRole",
        id: string,
        ltiPlatformID: string,
        loreeRoleID?:  {
          __typename: "LoreeRole",
          id: string,
          ltiPlatformID: string,
          name?: string | null,
          description?: string | null,
          featureList?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        lmsRole?: string | null,
        lmsBaseRoleType?: string | null,
        lmsRoleId?: string | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLoreeRolesQueryVariables = {
  filter?: ModelLoreeRoleFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLoreeRolesQuery = {
  listLoreeRoles?:  {
    __typename: "ModelLoreeRoleConnection",
    items:  Array< {
      __typename: "LoreeRole",
      id: string,
      ltiPlatformID: string,
      name?: string | null,
      description?: string | null,
      featureList?: string | null,
      lmsLoreeRoles?:  {
        __typename: "ModelLmsLoreeRoleConnection",
        items:  Array< {
          __typename: "LmsLoreeRole",
          id: string,
          ltiPlatformID: string,
          lmsRole?: string | null,
          lmsBaseRoleType?: string | null,
          lmsRoleId?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLmsLoreeRoleQueryVariables = {
  id?: string,
};

export type GetLmsLoreeRoleQuery = {
  getLmsLoreeRole?:  {
    __typename: "LmsLoreeRole",
    id: string,
    ltiPlatformID: string,
    loreeRoleID?:  {
      __typename: "LoreeRole",
      id: string,
      ltiPlatformID: string,
      name?: string | null,
      description?: string | null,
      featureList?: string | null,
      lmsLoreeRoles?:  {
        __typename: "ModelLmsLoreeRoleConnection",
        items:  Array< {
          __typename: "LmsLoreeRole",
          id: string,
          ltiPlatformID: string,
          lmsRole?: string | null,
          lmsBaseRoleType?: string | null,
          lmsRoleId?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    lmsRole?: string | null,
    lmsBaseRoleType?: string | null,
    lmsRoleId?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLmsLoreeRolesQueryVariables = {
  filter?: ModelLmsLoreeRoleFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLmsLoreeRolesQuery = {
  listLmsLoreeRoles?:  {
    __typename: "ModelLmsLoreeRoleConnection",
    items:  Array< {
      __typename: "LmsLoreeRole",
      id: string,
      ltiPlatformID: string,
      loreeRoleID?:  {
        __typename: "LoreeRole",
        id: string,
        ltiPlatformID: string,
        name?: string | null,
        description?: string | null,
        featureList?: string | null,
        lmsLoreeRoles?:  {
          __typename: "ModelLmsLoreeRoleConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      lmsRole?: string | null,
      lmsBaseRoleType?: string | null,
      lmsRoleId?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetHFivePUsersQueryVariables = {
  id?: string,
};

export type GetHFivePUsersQuery = {
  getHFivePUsers?:  {
    __typename: "HFivePUsers",
    id: string,
    email: string,
    loreeUser: string,
    ltiPlatformID: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListHFivePUserssQueryVariables = {
  filter?: ModelHFivePUsersFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListHFivePUserssQuery = {
  listHFivePUserss?:  {
    __typename: "ModelHFivePUsersConnection",
    items:  Array< {
      __typename: "HFivePUsers",
      id: string,
      email: string,
      loreeUser: string,
      ltiPlatformID: string,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetInteractivesMetaDataQueryVariables = {
  id?: string,
};

export type GetInteractivesMetaDataQuery = {
  getInteractivesMetaData?:  {
    __typename: "InteractivesMetaData",
    id: string,
    ltiPlatformID?: string | null,
    metaKey: string,
    metaValue?: string | null,
    isRetired?: boolean | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListInteractivesMetaDatasQueryVariables = {
  filter?: ModelInteractivesMetaDataFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListInteractivesMetaDatasQuery = {
  listInteractivesMetaDatas?:  {
    __typename: "ModelInteractivesMetaDataConnection",
    items:  Array< {
      __typename: "InteractivesMetaData",
      id: string,
      ltiPlatformID?: string | null,
      metaKey: string,
      metaValue?: string | null,
      isRetired?: boolean | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLmsImageQueryVariables = {
  id?: string,
};

export type GetLmsImageQuery = {
  getLmsImage?:  {
    __typename: "LmsImage",
    id: string,
    lms: string,
    lmsImageId: string,
    lmsImageUUID?: string | null,
    lmsImageName: string,
    lmsImageDisplayName?: string | null,
    lmsImageURL: string,
    lmsImageType?: string | null,
    s3ObjectKey?: string | null,
    s3PublicObjectKey?: string | null,
    s3GlobalTemplateKey?: string | null,
    s3GlobalRowsKey?: string | null,
    s3GlobalElementsKey?: string | null,
    s3SharedTemplateKey?: string | null,
    s3SharedRowsKey?: string | null,
    s3SharedElementsKey?: string | null,
    lmsImageMetaData?: string | null,
    s3MetaData?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLmsImagesQueryVariables = {
  filter?: ModelLmsImageFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLmsImagesQuery = {
  listLmsImages?:  {
    __typename: "ModelLmsImageConnection",
    items:  Array< {
      __typename: "LmsImage",
      id: string,
      lms: string,
      lmsImageId: string,
      lmsImageUUID?: string | null,
      lmsImageName: string,
      lmsImageDisplayName?: string | null,
      lmsImageURL: string,
      lmsImageType?: string | null,
      s3ObjectKey?: string | null,
      s3PublicObjectKey?: string | null,
      s3GlobalTemplateKey?: string | null,
      s3GlobalRowsKey?: string | null,
      s3GlobalElementsKey?: string | null,
      s3SharedTemplateKey?: string | null,
      s3SharedRowsKey?: string | null,
      s3SharedElementsKey?: string | null,
      lmsImageMetaData?: string | null,
      s3MetaData?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLoreeOrganisationQueryVariables = {
  id?: string,
};

export type GetLoreeOrganisationQuery = {
  getLoreeOrganisation?:  {
    __typename: "LoreeOrganisation",
    id: string,
    name?: string | null,
    ltiPlatforms?:  {
      __typename: "ModelLtiPlatformConnection",
      items:  Array< {
        __typename: "LtiPlatform",
        id: string,
        clientId: string,
        clientSecret?: string | null,
        platformName?: string | null,
        platformUrl?: string | null,
        authEndpoint?: string | null,
        accesstokenEndpoint?: string | null,
        publicKeyURL?: string | null,
        authConfig?:  {
          __typename: "AuthDetail",
          method?: string | null,
          key?: string | null,
        } | null,
        googleAnalyticsTrackingId?: string | null,
        ltiDeploymentId?: string | null,
        loreeOrganisationID?: string | null,
        loreeOrganisation?:  {
          __typename: "LoreeOrganisation",
          id: string,
          name?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        ltiApiKey?:  {
          __typename: "ModelLtiApiKeyConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLoreeOrganisationsQueryVariables = {
  filter?: ModelLoreeOrganisationFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLoreeOrganisationsQuery = {
  listLoreeOrganisations?:  {
    __typename: "ModelLoreeOrganisationConnection",
    items:  Array< {
      __typename: "LoreeOrganisation",
      id: string,
      name?: string | null,
      ltiPlatforms?:  {
        __typename: "ModelLtiPlatformConnection",
        items:  Array< {
          __typename: "LtiPlatform",
          id: string,
          clientId: string,
          clientSecret?: string | null,
          platformName?: string | null,
          platformUrl?: string | null,
          authEndpoint?: string | null,
          accesstokenEndpoint?: string | null,
          publicKeyURL?: string | null,
          googleAnalyticsTrackingId?: string | null,
          ltiDeploymentId?: string | null,
          loreeOrganisationID?: string | null,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLoreeUserQueryVariables = {
  id?: string,
};

export type GetLoreeUserQuery = {
  getLoreeUser?:  {
    __typename: "LoreeUser",
    id: string,
    loreeOrganisationID?: string | null,
    name?: string | null,
    email?: string | null,
    username?: string | null,
    externalIds?:  Array< {
      __typename: "ExternalId",
      key?: string | null,
      value?: string | null,
    } | null > | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLoreeUsersQueryVariables = {
  filter?: ModelLoreeUserFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLoreeUsersQuery = {
  listLoreeUsers?:  {
    __typename: "ModelLoreeUserConnection",
    items:  Array< {
      __typename: "LoreeUser",
      id: string,
      loreeOrganisationID?: string | null,
      name?: string | null,
      email?: string | null,
      username?: string | null,
      externalIds?:  Array< {
        __typename: "ExternalId",
        key?: string | null,
        value?: string | null,
      } | null > | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiToolQueryVariables = {
  id?: string,
};

export type GetLtiToolQuery = {
  getLtiTool?:  {
    __typename: "LtiTool",
    id: string,
    ltiPlatformID?: string | null,
    toolName: string,
    issuerUrl: string,
    oidcUrl: string,
    redirectURI: string,
    targetLinkURI: string,
    jwksUrl: string,
    domainName: string,
    clientId: string,
    clientSecret?: string | null,
    toolDeployments?:  {
      __typename: "ModelLtiToolDeploymentConnection",
      items:  Array< {
        __typename: "LtiToolDeployment",
        id: string,
        toolId?:  {
          __typename: "LtiTool",
          id: string,
          ltiPlatformID?: string | null,
          toolName: string,
          issuerUrl: string,
          oidcUrl: string,
          redirectURI: string,
          targetLinkURI: string,
          jwksUrl: string,
          domainName: string,
          clientId: string,
          clientSecret?: string | null,
          createdAt: string,
          updatedAt: string,
        } | null,
        createdAt: string,
        updatedAt: string,
      } >,
      nextToken?: string | null,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiToolsQueryVariables = {
  filter?: ModelLtiToolFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiToolsQuery = {
  listLtiTools?:  {
    __typename: "ModelLtiToolConnection",
    items:  Array< {
      __typename: "LtiTool",
      id: string,
      ltiPlatformID?: string | null,
      toolName: string,
      issuerUrl: string,
      oidcUrl: string,
      redirectURI: string,
      targetLinkURI: string,
      jwksUrl: string,
      domainName: string,
      clientId: string,
      clientSecret?: string | null,
      toolDeployments?:  {
        __typename: "ModelLtiToolDeploymentConnection",
        items:  Array< {
          __typename: "LtiToolDeployment",
          id: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiToolLoginHintQueryVariables = {
  id?: string,
};

export type GetLtiToolLoginHintQuery = {
  getLtiToolLoginHint?:  {
    __typename: "LtiToolLoginHint",
    id: string,
    issuerUrl: string,
    clientId: string,
    loreeUserEmail: string,
    messageType?: string | null,
    resourceLink?: string | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiToolLoginHintsQueryVariables = {
  filter?: ModelLtiToolLoginHintFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiToolLoginHintsQuery = {
  listLtiToolLoginHints?:  {
    __typename: "ModelLtiToolLoginHintConnection",
    items:  Array< {
      __typename: "LtiToolLoginHint",
      id: string,
      issuerUrl: string,
      clientId: string,
      loreeUserEmail: string,
      messageType?: string | null,
      resourceLink?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetLtiToolDeploymentQueryVariables = {
  id?: string,
};

export type GetLtiToolDeploymentQuery = {
  getLtiToolDeployment?:  {
    __typename: "LtiToolDeployment",
    id: string,
    toolId?:  {
      __typename: "LtiTool",
      id: string,
      ltiPlatformID?: string | null,
      toolName: string,
      issuerUrl: string,
      oidcUrl: string,
      redirectURI: string,
      targetLinkURI: string,
      jwksUrl: string,
      domainName: string,
      clientId: string,
      clientSecret?: string | null,
      toolDeployments?:  {
        __typename: "ModelLtiToolDeploymentConnection",
        items:  Array< {
          __typename: "LtiToolDeployment",
          id: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } | null,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListLtiToolDeploymentsQueryVariables = {
  filter?: ModelLtiToolDeploymentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListLtiToolDeploymentsQuery = {
  listLtiToolDeployments?:  {
    __typename: "ModelLtiToolDeploymentConnection",
    items:  Array< {
      __typename: "LtiToolDeployment",
      id: string,
      toolId?:  {
        __typename: "LtiTool",
        id: string,
        ltiPlatformID?: string | null,
        toolName: string,
        issuerUrl: string,
        oidcUrl: string,
        redirectURI: string,
        targetLinkURI: string,
        jwksUrl: string,
        domainName: string,
        clientId: string,
        clientSecret?: string | null,
        toolDeployments?:  {
          __typename: "ModelLtiToolDeploymentConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type GetCustomThemeQueryVariables = {
  id?: string,
};

export type GetCustomThemeQuery = {
  getCustomTheme?:  {
    __typename: "CustomTheme",
    id: string,
    ltiPlatformID: string,
    isShared?: boolean | null,
    themeData: string,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListCustomThemesQueryVariables = {
  filter?: ModelCustomThemeFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCustomThemesQuery = {
  listCustomThemes?:  {
    __typename: "ModelCustomThemeConnection",
    items:  Array< {
      __typename: "CustomTheme",
      id: string,
      ltiPlatformID: string,
      isShared?: boolean | null,
      themeData: string,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type CategoryByPlatformQueryVariables = {
  ltiPlatformID?: string | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelCategoryFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type CategoryByPlatformQuery = {
  categoryByPlatform?:  {
    __typename: "ModelCategoryConnection",
    items:  Array< {
      __typename: "Category",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      name: string,
      projects?:  {
        __typename: "ModelProjectConnection",
        items:  Array< {
          __typename: "Project",
          id: string,
          title: string,
          categoryID: string,
          createdAt: string,
          updatedAt: string,
        } >,
        nextToken?: string | null,
      } | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type TemplateByOwnerQueryVariables = {
  ltiPlatformID?: string | null,
  owner?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelCustomTemplateFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TemplateByOwnerQuery = {
  templateByOwner?:  {
    __typename: "ModelCustomTemplateConnection",
    items:  Array< {
      __typename: "CustomTemplate",
      id: string,
      ltiPlatformID?: string | null,
      loreeOrganisationID?: string | null,
      title: string,
      categoryID: string,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      content?: string | null,
      status?: boolean | null,
      isGlobal?: boolean | null,
      isShared?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      owner?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type ByLtiPlatformByGeneratedAtQueryVariables = {
  ltiPlatformID?: string | null,
  generatedAt?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelLtiLogFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ByLtiPlatformByGeneratedAtQuery = {
  byLtiPlatformByGeneratedAt?:  {
    __typename: "ModelLtiLogConnection",
    items:  Array< {
      __typename: "LtiLog",
      id: string,
      ltiPlatformID: string,
      userInfo?: string | null,
      clientInfo?: string | null,
      userGeoLocation?: string | null,
      userAction?: string | null,
      generatedAt?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};

export type BlockByOwnerQueryVariables = {
  ltiPlatformID?: string | null,
  owner?: ModelStringKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelCustomBlockFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type BlockByOwnerQuery = {
  blockByOwner?:  {
    __typename: "ModelCustomBlockConnection",
    items:  Array< {
      __typename: "CustomBlock",
      id: string,
      title: string,
      categoryID: string,
      ltiPlatformID: string,
      loreeOrganisationID?: string | null,
      category?:  {
        __typename: "Category",
        id: string,
        ltiPlatformID?: string | null,
        loreeOrganisationID?: string | null,
        name: string,
        projects?:  {
          __typename: "ModelProjectConnection",
          nextToken?: string | null,
        } | null,
        createdAt: string,
        updatedAt: string,
      } | null,
      type: string,
      content?: string | null,
      thumbnail?:  {
        __typename: "S3Object",
        bucket: string,
        key: string,
        region: string,
      } | null,
      status?: boolean | null,
      isGlobal?: boolean | null,
      isShared?: boolean | null,
      createdBy?: string | null,
      active?: boolean | null,
      owner?: string | null,
      createdAt: string,
      updatedAt: string,
    } >,
    nextToken?: string | null,
  } | null,
};
