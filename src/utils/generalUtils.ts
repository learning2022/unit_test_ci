/**
 * Will throw a type error if the arguement is undefined or null
 * @param argument
 * @param message
 * @returns
 */
export function ensure<T>(
  argument: T | undefined | null,
  message: string = 'This value was promised to be there.',
): T {
  if (argument === undefined || argument === null) {
    throw new TypeError(message);
  }
  return argument;
}

/**
 * Will always return an item from the array given a filter
 * @param array
 * @param filter
 * @returns
 */
export function getOneAlways<T>(array: T[], filter: (arg0: T) => boolean): T {
  const value: T | undefined = array.find(filter);
  return ensure(value);
}
