import { externalToolFieldListMock, ExternalToolFieldMock } from './generalUtilMock';
import { ensure, getOneAlways } from './generalUtils';

describe('generalUtils helper methods', () => {
  describe('ensure', () => {
    test('should throw a typeerror when not found or undefined', async () => {
      const t = () => {
        ensure(
          externalToolFieldListMock.find(
            (config: ExternalToolFieldMock) => config.key === 'unknownkey',
          ),
        );
      };
      expect(t).toThrow(TypeError);
      expect(t).toThrow('This value was promised to be there.');
    });

    test('should throw a typeerror when not found or null', async () => {
      const t = () => {
        ensure(null);
      };
      expect(t).toThrow(TypeError);
      expect(t).toThrow('This value was promised to be there.');
    });

    test('should throw a typeerror when not found', async () => {
      const t = () => {
        ensure(
          externalToolFieldListMock.find(
            (config: ExternalToolFieldMock) => config.key === 'unknownkey',
          ),
        );
      };
      expect(t).toThrow(TypeError);
      expect(t).toThrow('This value was promised to be there.');
    });
  });

  describe('getOneAlways', () => {
    test('should return an item in the array when found', async () => {
      const expectedResult = {
        key: 'toolName',
        label: 'Tool Name',
        isHidden: true,
        isMandatory: true,
      };
      const result = getOneAlways(
        externalToolFieldListMock,
        (config: ExternalToolFieldMock) => config.key === 'toolName',
      );
      expect(result).toEqual(expectedResult);
    });

    test('should throw a typeerror when not found', async () => {
      const t = () => {
        getOneAlways(
          externalToolFieldListMock,
          (config: ExternalToolFieldMock) => config.key === 'unknownKey',
        );
      };
      expect(t).toThrow(TypeError);
      expect(t).toThrow('This value was promised to be there.');
    });
  });
});
