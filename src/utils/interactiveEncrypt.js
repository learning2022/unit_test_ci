const crypto = require('crypto');
const interactiveEncrypt = (data) => {
  const iv = crypto.randomBytes(16);
  iv.fill(0);
  const key = 'HRX/LjRbjnNBbSTX55egDEkHi81MOa0f';
  let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
  let encrypted = cipher.update(data.body.user);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
  const orgIdToString = data.body.organization_id.toString();
  let encryptedId = cipher.update(orgIdToString);
  encryptedId = Buffer.concat([encryptedId, cipher.final()]);
  const encryptdata = {
    user_encrypt: encrypted.toString('hex'),
    org_id_encrypt: encryptedId.toString('hex'),
    url: data.body.url,
    userId: data.body.userId,
  };
  return encryptdata;
};

module.exports = {
  interactiveEncrypt,
};
