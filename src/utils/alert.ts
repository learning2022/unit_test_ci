import CONSTANTS from '../loree-editor/constant';
import { translate } from '../i18n/translate';

const closeIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="7.586" height="7.587" viewBox="0 0 7.586 7.587">
<path id="close" d="M-5641.125-5116.2l-2.672-2.673-2.673,2.673a.657.657,0,0,1-.929,0,.657.657,0,0,1,0-.929l2.673-2.673-2.673-2.673a.656.656,0,0,1,0-.928.659.659,0,0,1,.929,0l2.673,2.673,2.672-2.673a.658.658,0,0,1,.928,0,.655.655,0,0,1,0,.928l-2.673,2.673,2.673,2.673a.656.656,0,0,1,0,.929.654.654,0,0,1-.464.192A.656.656,0,0,1-5641.125-5116.2Z" transform="translate(5647.591 5123.591)"/>
</svg>`;
const alertIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="90.276" height="80.201" viewBox="0 0 90.276 80.201"><defs><style>.a{fill:#000d9c;}</style></defs><path class="a" d="M-2765.612-16968.8a13.365,13.365,0,0,1-11.6-6.7,12.953,12.953,0,0,1,.1-13.3l31.8-53.7a13.1,13.1,0,0,1,11.4-6.5,12.977,12.977,0,0,1,11.4,6.6l31.9,53.6a12.943,12.943,0,0,1,.1,13.3,13.153,13.153,0,0,1-11.6,6.7Zm27.9-69.1-31.9,53.7a4.2,4.2,0,0,0,0,4.4,4.658,4.658,0,0,0,3.9,2.2h63.6a4.289,4.289,0,0,0,3.9-2.2,4.81,4.81,0,0,0,.1-4.5l-31.8-53.6a4.216,4.216,0,0,0-3.9-2.2A4.678,4.678,0,0,0-2737.714-17037.9Zm-.6,51v-1.5a4.314,4.314,0,0,1,4.4-4.4,4.256,4.256,0,0,1,4.4,4.4v1.5a4.313,4.313,0,0,1-4.4,4.4A4.314,4.314,0,0,1-2738.313-16986.9Zm0-12.8v-23.7a4.314,4.314,0,0,1,4.4-4.4,4.4,4.4,0,0,1,4.4,4.5v23.6a4.313,4.313,0,0,1-4.4,4.4A4.314,4.314,0,0,1-2738.313-16999.7Z" transform="translate(2779 17049.002)"/></svg>`;

type FileUploadConfirmAlertCallBack = (upload: boolean) => void;

const hideFileUploadConfirmAlert = (): void => {
  const fileUploadAlertWrapper = document.getElementById(
    CONSTANTS.LOREE_FILE_UPLOADER_ALERT_WRAPPER,
  );
  if (fileUploadAlertWrapper) {
    fileUploadAlertWrapper.remove();
  }
};

export const showFileUploadConfirmAlert = (
  fileType: string,
  callBack: FileUploadConfirmAlertCallBack,
): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const fileUploadAlertWrapper = document.createElement('div');
    fileUploadAlertWrapper.className = 'file-upload-alert-wrapper';
    fileUploadAlertWrapper.style.display = 'flex';
    fileUploadAlertWrapper.id = CONSTANTS.LOREE_FILE_UPLOADER_ALERT_WRAPPER;
    const confirmAlert = document.createElement('div');
    confirmAlert.className = 'col-8 col-lg-5 col-xl-4 file-upload-alert';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'card-header';
    alertHeader.innerHTML = `<span className='font-weight-bold confirm-header-text'>${translate(
      'alert.alert!',
    )}</span>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'float-right btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideFileUploadConfirmAlert();
    alertHeader.appendChild(alertCloseButton);
    confirmAlert.appendChild(alertHeader); // header append
    // Content
    const alertMessageDiv = document.createElement('div');
    alertMessageDiv.className = 'card-body';
    const confirmAlertFooter = document.createElement('div');
    confirmAlertFooter.className = 'text-center';
    if (fileType === 'video') {
      alertMessageDiv.innerHTML = `<div class="row">
          <div class='col-3'>
            ${alertIcon}
          </div>
          <div class='col-9'>${translate('alert.pleaseuploadafilelesserthan500mb')}</div>
        </div>
        </div>
      </div>`;
      const confirmNoButton = document.createElement('button');
      confirmNoButton.className = 'alert-btn m-2';
      confirmNoButton.innerHTML = translate('global.ok');
      confirmNoButton.onclick = (): void => {
        hideFileUploadConfirmAlert();
        callBack(true);
      };
      confirmAlertFooter.appendChild(confirmNoButton);
    } else {
      alertMessageDiv.innerHTML = `<div class="row">
          <div class='col-3'>
            ${alertIcon}
          </div>
          <div class='col-9'>${translate('alert.uploadedimagesizeismorethan500kb')}</div>
        </div>
        </div>
      </div>`;
      const confirmYesButton = document.createElement('button');
      confirmYesButton.className = 'alert-btn m-2';
      confirmYesButton.innerText = translate('global.yes');
      confirmYesButton.onclick = (): void => {
        hideFileUploadConfirmAlert();
        callBack(true);
      };
      const confirmNoButton = document.createElement('button');
      confirmNoButton.className = 'alert-btn m-2';
      confirmNoButton.innerHTML = translate('global.no');
      confirmNoButton.onclick = (): void => {
        hideFileUploadConfirmAlert();
        callBack(false);
      };
      confirmAlertFooter.appendChild(confirmYesButton);
      confirmAlertFooter.appendChild(confirmNoButton);
    }

    confirmAlert.appendChild(alertMessageDiv);

    // Footer

    confirmAlert.appendChild(confirmAlertFooter);
    fileUploadAlertWrapper.appendChild(confirmAlert);
    loreeWrapper.appendChild(fileUploadAlertWrapper);
  }
};

export const showFileTypeAlert = (fileType: string): void => {
  const loreeWrapper = document.getElementById(CONSTANTS.LOREE_WRAPPER);
  if (loreeWrapper) {
    const fileUploadAlertWrapper = document.createElement('div');
    fileUploadAlertWrapper.className = 'file-upload-alert-wrapper';
    fileUploadAlertWrapper.style.display = 'flex';
    fileUploadAlertWrapper.id = CONSTANTS.LOREE_FILE_UPLOADER_ALERT_WRAPPER;
    const confirmAlert = document.createElement('div');
    confirmAlert.className = 'col-lg-4 file-upload-alert';
    // Header
    const alertHeader = document.createElement('div');
    alertHeader.className = 'card-header';
    alertHeader.innerHTML = `<span className='font-weight-bold confirm-header-text'>${translate(
      'alert.alert!',
    )}</span>`;
    const alertCloseButton = document.createElement('div');
    alertCloseButton.className = 'float-right btn-icon';
    alertCloseButton.innerHTML = closeIcon;
    alertCloseButton.onclick = (): void => hideFileUploadConfirmAlert();
    alertHeader.appendChild(alertCloseButton);
    confirmAlert.appendChild(alertHeader); // header append
    // Content
    const alertMessageDiv = document.createElement('div');
    alertMessageDiv.className = 'card-body';
    if (fileType === 'video') {
      alertMessageDiv.innerHTML = `<div class="row">
          <div class='col-3'>
            ${alertIcon}
          </div>
          <div class='col-9'>${translate('alert.itonlysupportsvideoformatslikemovmp4')}</div>
        </div>
        </div>
      </div>`;
    } else {
      alertMessageDiv.innerHTML = `<div class="row">
          <div class='col-3'>
            ${alertIcon}
          </div>
          <div class='col-9'>${translate('alert.itsupportsonlyanimageformatslikejpgjpegpng')}</div>
        </div>
        </div>
      </div>`;
    }
    confirmAlert.appendChild(alertMessageDiv);
    fileUploadAlertWrapper.appendChild(confirmAlert);
    loreeWrapper.appendChild(fileUploadAlertWrapper);
  }
};
