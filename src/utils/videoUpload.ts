/* eslint-disable @typescript-eslint/no-explicit-any */
import { Storage, API, graphqlOperation } from 'aws-amplify';
import CONSTANTS from '../loree-editor/constant';
import {
  uploadProgressBlock,
  uploadVal,
  hideUploader,
  appendProgressBarList,
} from '../loree-editor/alert';
import { videoUpload } from '../graphql/mutations';
import { kalturaVideos, getKalturaVideo } from '../graphql/queries';
import { generateRandomString } from '../loree-editor/utils';

export const handleUploadVideo = async (file: any, fileName: string) => {
  try {
    const randomString = generateRandomString(6);
    const progressBarPopper = document.getElementById('loree-upload-progress-block');
    typeof progressBarPopper !== 'undefined' && progressBarPopper != null
      ? appendProgressBarList(randomString)
      : uploadProgressBlock(randomString);
    const uploadingLabel = document.getElementById(
      `${CONSTANTS.LOREE_UPLOADING_ITEM_LABEL}_${randomString}`,
    );
    if (uploadingLabel) {
      uploadingLabel.innerHTML = fileName;
    }
    const uploadUrl: any = await Storage.put(`video-block/${fileName}`, file, {
      level: 'private',
      contentType: 'video',
      progressCallback(progress: any) {
        const percent = Math.floor((progress.loaded * 100) / progress.total);
        if (percent < 100) {
          uploadVal(percent, randomString);
        }
      },
    });
    const progressBar = document.getElementById(
      `${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${randomString}`,
    ) as any;
    if (progressBar === null) {
      if (uploadUrl) {
        await Storage.vault.remove(uploadUrl.key).catch((err) => console.log(err));
      }
      uploadUrl.abort();
    }
    if (uploadUrl) {
      const videoDetails = await Storage.vault.list(uploadUrl.key);
      const objectUrl = await Storage.vault.get(uploadUrl.key);
      const result = progressBar === null ? false : [videoDetails[0], objectUrl];
      hideUploader(randomString);
      return result;
    }
    return false;
  } catch (e) {
    return false;
  }
};

export const handleCanvasUploadVideo = async (file: any, fileName: string) => {
  const randomString = generateRandomString(6);
  const progressBarPopper = document.getElementById('loree-upload-progress-block');
  typeof progressBarPopper !== 'undefined' && progressBarPopper != null
    ? appendProgressBarList(randomString)
    : uploadProgressBlock(randomString);
  const uploadingLabel = document.getElementById(
    `${CONSTANTS.LOREE_UPLOADING_ITEM_LABEL}_${randomString}`,
  );
  if (uploadingLabel) {
    uploadingLabel.innerHTML = fileName;
  }
  const uploadUrl: any = await Storage.put(`tmp/${fileName}`, file, {
    contentType: 'video',
    progressCallback(progress: any) {
      const percent = Math.floor((progress.loaded * 100) / progress.total);
      if (percent < 100) {
        uploadVal(percent, randomString);
      }
    },
  });
  const courseId = sessionStorage.getItem('course_id');
  const getUrl = await Storage.get(`tmp/${fileName}`, { expires: 60000 });
  const progressBar = document.getElementById(
    `${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${randomString}`,
  ) as any;
  if (progressBar === null) {
    if (uploadUrl) {
      await Storage.remove(`tmp/${fileName}`);
    }
  }
  let response: any = await API.graphql(
    graphqlOperation(videoUpload, {
      courseId: courseId,
      name: fileName,
      path: getUrl,
      platformId: sessionStorage.getItem('ltiPlatformId'),
    }),
  );
  response = JSON.parse(response.data.videoUpload);
  const errorCodes = ['-2', '-1', 'virusScan.ScanFailure', 'virusScan.Infected', '3', '6', '7'];
  if (response.status === '2') {
    removeFile(uploadUrl, fileName);
    const result = progressBar === null ? false : [response, response.dataUrl];
    hideUploader(randomString);
    return result;
  } else if (errorCodes.includes(response.status)) {
    removeFile(uploadUrl, fileName);
    hideUploader(randomString);
    return false;
  } else {
    const count: any = 0;
    const videoResponse = await uploadStatus(
      response,
      errorCodes,
      fileName,
      uploadUrl,
      count,
      randomString,
    );
    const result = progressBar === null ? false : videoResponse;
    hideUploader(randomString);
    return result;
  }
};

const removeFile: any = async (uploadurl: any, fileName: any) => {
  if (uploadurl) {
    await Storage.remove(`tmp/${fileName}`);
  }
};

const uploadStatus: any = async (
  response: any,
  errorCodes: string,
  fileName: any,
  uploadUrl: any,
  count: any,
  id: string,
) => {
  const pendingCodes = ['0', '1', '4', '5'];
  if (response.status === '2') {
    removeFile(uploadUrl, fileName);
    hideUploader(id);
    return [response, response.dataUrl];
  } else if (pendingCodes.includes(response.status)) {
    count = count + 1;
    if (count < 4) {
      const res: any = await getCanvasVideos(response.id);
      await timeout(30000);
      const status = await uploadStatus(res, errorCodes, fileName, uploadUrl, count, id);
      return status;
    } else {
      removeFile(uploadUrl, fileName);
      hideUploader(id);
      return [response, response.dataUrl];
    }
  } else {
    removeFile(uploadUrl, fileName);
    hideUploader(id);
    return null;
  }
};

async function timeout(ms: number) {
  return await new Promise((resolve) => setTimeout(resolve, ms));
}

export const getCanvasVideos = async (entryId: any) => {
  const response: any = await API.graphql(
    graphqlOperation(getKalturaVideo, {
      entryId: entryId,
      userId: sessionStorage.getItem('course_id'),
      platformId: sessionStorage.getItem('ltiPlatformId'),
    }),
  );
  return JSON.parse(response.data.getKalturaVideo);
};

export const listCanvasVideos = async () => {
  const response: any = await API.graphql(
    graphqlOperation(kalturaVideos, {
      courseId: sessionStorage.getItem('course_id'),
      platformId: sessionStorage.getItem('ltiPlatformId'),
    }),
  );
  return JSON.parse(response.data.kalturaVideos);
};
