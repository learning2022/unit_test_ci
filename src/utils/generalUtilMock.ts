export interface ExternalToolFieldMock {
  key: string;
  label: string;
  isHidden: boolean;
  isMandatory: boolean;
}

export const externalToolFieldListMock = [
  {
    key: 'toolName',
    label: 'Tool Name',
    isHidden: true,
    isMandatory: true,
  },
  {
    key: 'oidcUrl',
    label: 'OIDC Url',
    isHidden: true,
    isMandatory: true,
  },
  {
    key: 'jwksUrl',
    label: 'JWKS Url',
    isHidden: true,
    isMandatory: true,
  },
  {
    key: 'redirectURI',
    label: 'Redirect Url',
    isHidden: true,
    isMandatory: true,
  },
  {
    key: 'clientId',
    label: 'Client ID',
    isHidden: true,
    isMandatory: true,
  },
  {
    key: 'issuerUrl',
    label: 'Issuer Url',
    isHidden: true,
    isMandatory: true,
  },

  {
    key: 'clientSecret',
    label: 'Client Secret',
    isHidden: true,
    isMandatory: true,
  },
  {
    key: 'targetLinkURI',
    label: 'Target Link Url',
    isHidden: true,
    isMandatory: true,
  },
];
