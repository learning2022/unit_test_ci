export class LmsAccess {
  getAccess() {
    const session = sessionStorage.getItem('lmsUrl');
    if (session === null) {
      return false;
    }
    return true;
  }

  getDetails() {
    const courseDetails = {
      courseId: sessionStorage.getItem('course_id'),
    };
    return courseDetails;
  }
}
