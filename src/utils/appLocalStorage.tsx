export class AppLocalStorage {
  getValue(key: string) {
    let language = 'EN';
    const value = String(localStorage.getItem(key));
    if (value.length !== 0 && value !== 'null') {
      language = value;
    }
    return language;
  }

  setValue(key: string, value: string) {
    localStorage.setItem(key, value);
  }
}
