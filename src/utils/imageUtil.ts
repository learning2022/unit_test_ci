export const getS3Key = (imageUrl: string) => {
  try {
    const imagePathname = new URL(imageUrl).pathname;
    const imageUrlParams = imagePathname.split('?');
    const imageS3KeyArray = imageUrlParams[0].split('/');
    return imageS3KeyArray;
  } catch (e) {
    console.log('error on  URL', e);
    return null;
  }
};
