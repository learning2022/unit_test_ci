/* eslint-disable */ // Remove this line when editing this file
import jsPDF from 'jspdf';
import JSZip from 'jszip';
import { API, graphqlOperation, Storage } from 'aws-amplify';
import { getPage, getProject } from '../graphql/queries';
import fileSaver from 'file-saver';

function downloadFile(url: any, name: string) {
  const link = document.createElement('a');
  link.href = url;
  link.setAttribute('download', name);
  document.body.appendChild(link);
  link.click();
}

//Export Page as HTML
export const handleDownloadHTML = async (id: any) => {
  const page: any = await API.graphql(graphqlOperation(getPage, { id: id }));
  const pageKey = page.data.getPage.content;
  if (pageKey !== null) {
    const s3Url = await Storage.get(pageKey);
    downloadFile(s3Url, `${page.data.getPage.title}.html`);
  }
};

// create pdf document
function pdfDocument(title: string, content: string) {
  const doc = new jsPDF();
  doc.setFontSize(20);
  doc.text(title, 100, 10, null, null, 'center');
  doc.setFontSize(40);
  doc.fromHTML(content, 15, 15, { width: 180 });
  return doc;
}

async function fetchHTMLContent(url: string) {
  return fetch(url)
    .then(res => {
      return res.text();
    })
    .then(function (html: any) {
      const parser = new DOMParser();
      const doc: any = parser.parseFromString(html, 'text/html');
      return doc.getElementById('page-text-block').innerHTML;
    })
    .catch(function (err) {
      console.log('Failed to fetch page: ', err);
    });
}

//Export Page as PDF
export const handleDownloadPDF = async (id: any, title: any) => {
  const page: any = await API.graphql(graphqlOperation(getPage, { id: id }));
  const pageKey = page.data.getPage.content;

  if (pageKey !== null) {
    const dataURL: any = await Storage.get(pageKey);
    const content = await fetchHTMLContent(dataURL);
    const doc = pdfDocument(title, content);
    doc.save(`${title}.pdf`);
  }
};

//Export Project as HTML
export const handleDownloadProjectHTML = async (id: any) => {
  const zip = new JSZip();
  const projectId = { id: id };
  const pages: any = await API.graphql(graphqlOperation(getProject, projectId));
  let count = 0;

  for (const pageName of pages.data.getProject.pages.items) {
    const pagesUrl: any = await Storage.get(pageName.content);
    const downloadUrl = pagesUrl.split('?');
    let filename = downloadUrl[0];
    filename = filename.replace(downloadUrl[0], `${pageName.title}.html`);
    const content = await fetchHTMLContent(pagesUrl);
    zip.file(filename, content);
    count++;
    if (count === pages.data.getProject.pages.items.length) {
      zip.generateAsync({ type: 'blob' }).then(function (compressedFile: any) {
        const downloadUrl = window.URL.createObjectURL(new Blob([compressedFile]));
        downloadFile(downloadUrl, `${pages.data.getProject.title}.zip`);
      });
    }
  }
};

//Export Project as PDF
export const handleDownloadProjectPDF = async (id: any, title: any) => {
  const zip = new JSZip();
  const projectId = { id: id };
  const pages: any = await API.graphql(graphqlOperation(getProject, projectId));
  const projectTitle = title;
  const pagelist = pages.data.getProject.pages.items;
  let count = 0;
  for (const pageName of pages.data.getProject.pages.items) {
    const pagesUrl: any = await Storage.get(pageName.content);
    if (pagesUrl !== null) {
      const content = await fetchHTMLContent(pagesUrl);
      const doc = pdfDocument(pageName.title, content);
      zip.file(`${pageName.title}.pdf`, doc.output('blob'));
      count++;
      if (count === pagelist.length) {
        const downloadFileZIP = await zip.generateAsync({ type: 'blob' });
        fileSaver.saveAs(downloadFileZIP, `${projectTitle}.zip`);
      }
    }
  }
};

const getHtmlCode = (innerHTML: string): string => {
  const code = `<!doctype html>
  <html lang="en">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&family=Crimson+Pro:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Be+Vietnam:wght@100&family=Dancing+Script:wght@400;500;600;700&family=Fira+Code:wght@300;400;500;600;700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"/>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="./css/style.css">
  </head>
  <body>
    ${innerHTML}
  </body>
  <html>`;
  return code;
};

//Export Page HTML and CSS in ZIP
export const handleDownloadPageExport = (innerHTML: string, cssStyle: string) => {
  let title = 'archive';
  const titleContainer = document.getElementById('loree-header-title');
  if (titleContainer && titleContainer.title !== '') {
    title = titleContainer.title;
  }
  const code = getHtmlCode(innerHTML);
  const zip = new JSZip();
  zip.file('index.html', code);
  const css = zip.folder('css');
  if (css) css.file('style.css', cssStyle);
  zip.generateAsync({ type: 'blob' }).then(function (content) {
    fileSaver.saveAs(content, `${title}.zip`);
  });
};
