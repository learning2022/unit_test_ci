import { Storage, API, graphqlOperation } from 'aws-amplify';
import { apm } from '@elastic/apm-rum';
import CONSTANTS from '../loree-editor/constant';
import {
  uploadProgressBlock,
  uploadVal,
  hideUploader,
  appendProgressBarList,
} from '../loree-editor/alert';
import { bbCourseImageUpload, imageUpload, uploadD2LImagesToS3 } from '../graphql/mutations';
import {
  canvasImages,
  fetchS3Images,
  listBbCourseImages,
  listChildCourseImages,
  listCourseImagesById,
  viewBbContents,
} from '../graphql/queries';
import { generateRandomString } from '../loree-editor/utils';
import { ImageUploadMutation } from '../API';

// canvas images handling
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const handleUploadImage = async (file: any, imageTitle: string, source: any) => {
  const randomString = generateRandomString(6);
  const progressBarPopper = document.getElementById('loree-upload-progress-block');
  typeof progressBarPopper !== 'undefined' && progressBarPopper != null
    ? appendProgressBarList(randomString)
    : uploadProgressBlock(randomString);
  if (source === 'CANVAS') {
    return await uploadToCanvas(file, imageTitle, randomString);
  } else if (source === 'LOREE_TO_CANVAS') {
    return await uploadToCanvasFromLoree(file, imageTitle, randomString);
  } else {
    return await uploadToS3(file, imageTitle, randomString);
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function uploadToS3(file: any, imageTitle: string, id: string) {
  try {
    const uploadingLabel = document.getElementById(`${CONSTANTS.LOREE_UPLOADING_ITEM_LABEL}_${id}`);
    if (uploadingLabel) {
      uploadingLabel.innerHTML = imageTitle;
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const uploadUrl: any = await Storage.put(`img-block/${imageTitle}`, file, {
      level: 'private',
      contentType: 'image',
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      progressCallback(progress: any) {
        const percent = Math.floor((progress.loaded * 100) / progress.total);
        if (percent < 100) {
          uploadVal(percent, id);
        }
      },
    });
    const progressBar = document.getElementById(
      `${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${id}`,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) as any;
    if (progressBar === null) {
      if (uploadUrl) {
        await Storage.vault.remove(uploadUrl.key).catch((err) => console.log(err));
      }
      uploadUrl.abort();
    }
    if (uploadUrl) {
      const imageDetails = await Storage.vault.list(uploadUrl.key);
      const getUrl = await Storage.vault.get(uploadUrl.key);
      hideUploader(id);
      return [imageDetails[0], getUrl];
    }
    return null;
  } catch (e) {
    return null;
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function uploadToCanvasFromLoree(file: any, imageTitle: string, id: string) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const progressBar = document.getElementById(`${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${id}`) as any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let response: any = await saveToCanvas(file.type, file.src, imageTitle);
  response = JSON.parse(response.data.imageUpload);
  if (response.url) {
    const result = progressBar === null ? false : [response.url];
    hideUploader(id);
    return result;
  } else {
    hideUploader(id);
    return '';
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function uploadToCanvas(file: any, imageTitle: string, id: string) {
  const uploadingLabel = document.getElementById(`${CONSTANTS.LOREE_UPLOADING_ITEM_LABEL}_${id}`);
  if (uploadingLabel) {
    uploadingLabel.innerHTML = imageTitle;
  }
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const uploadUrl: any = await Storage.put(`tmp/${imageTitle}`, file, {
    contentType: 'image',
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    progressCallback(progress: any) {
      const percent = Math.floor((progress.loaded * 100) / progress.total);
      if (percent < 100) {
        uploadVal(percent, id);
      }
    },
  });
  const getUrl = await Storage.get(`tmp/${imageTitle}`, { expires: 120 });
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const progressBar = document.getElementById(`${CONSTANTS.LOREE_MEDIA_PROGRESS_BAR}_${id}`) as any;
  if (progressBar === null) {
    if (uploadUrl) {
      await Storage.remove(`tmp/${imageTitle}`);
    }
  }
  const fileType = 'image/' + imageTitle.split('.')[1];
  const canvasResult = await saveToCanvas(fileType, getUrl, imageTitle);
  if (uploadUrl) {
    await Storage.remove(`tmp/${imageTitle}`);
  }
  const response = JSON.parse(canvasResult?.data?.imageUpload as string);
  if (response.url) {
    const result = progressBar === null ? false : ['', response.url, response.id];
    hideUploader(id);
    return result;
  } else {
    hideUploader(id);
    return false;
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function saveToCanvas(fileType: any, srcUrl: any, imageTitle: string) {
  const courseId = sessionStorage.getItem('course_id');
  const response = await API.graphql<ImageUploadMutation>(
    graphqlOperation(imageUpload, {
      courseId: courseId,
      name: imageTitle,
      type: fileType,
      path: srcUrl,
    }),
  );
  return response;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const listCanvasImages = async (courseID: any) => {
  const response = await getData(courseID);
  return response;
};

// standlone images handling
export const getStandaloneMyImages = async () => {
  const response = await Storage.list('', { level: 'private' });
  for (let i = 0; i < response.length; i++) {
    const imageName = response[i].key.split('/')[1];
    response[i].name = imageName;
  }
  return response;
};

export const getStandaloneLoreeImages = async () => {
  const filterDataArray = await Storage.list(`Standalone-Free-Tier/`, { level: 'public' });
  for (let i = 0; i < filterDataArray.length; i++) {
    if (filterDataArray[i].size === 0) filterDataArray.splice(i, 1);
    const imageName = filterDataArray[i].key.split('/')[1];
    filterDataArray[i].name = imageName;
  }
  return filterDataArray;
};

export const getS3ImageList = async () => {
  let filterDataArray: {
    key: string;
    name: string;
    lastModified: string;
    size: number;
    created_at: string;
    display_name: string;
  }[] = [];
  const imagePath = [
    'Global-Templates',
    'Global-Rows',
    'Global-Elements',
    'Shared-Template',
    'Shared-Row',
    'Shared-Element',
  ];
  for (const path of imagePath) {
    const urlKeyList: [] = await Storage.list(
      `${sessionStorage.getItem('ltiPlatformId')}/${path}/`,
      { level: 'public' },
    );
    filterDataArray = [...filterDataArray, ...urlKeyList];
  }
  for (let i = 0; i < filterDataArray.length; i++) {
    const imageName = filterDataArray[i].key.split('/')[2];
    filterDataArray[i].name = imageName;
  }
  return filterDataArray;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getData(courseID: any) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let response: any = await API.graphql(
    graphqlOperation(canvasImages, {
      courseId: courseID,
    }),
  );
  response = JSON.parse(response.data.canvasImages);
  if (response.statusCode === 200) {
    return response.body;
  } else {
    return false;
  }
}

export const handleUploadThumbanail = async (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  file: any,
  imageTitle: string,
  thumbanailType: string,
) => {
  try {
    const uploadingLabel = document.getElementById(CONSTANTS.LOREE_UPLOADING_ITEM_LABEL);
    if (uploadingLabel) {
      uploadingLabel.innerHTML = imageTitle;
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let platformId: any = 'STANDALONE';
    if (sessionStorage.getItem('ltiPlatformId')) {
      platformId = sessionStorage.getItem('ltiPlatformId');
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const uploadUrl: any = await Storage.put(
      `${platformId}/Thumbnail/${thumbanailType}/${imageTitle}`,
      file,
      {
        contentType: 'image',
      },
    );
    if (uploadUrl) {
      return uploadUrl.key;
    }
    return null;
  } catch (e) {
    return null;
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getImageUrl = async (key: any, level?: string) => {
  try {
    const getUrl = await Storage.get(key, { level: level });
    return getUrl;
  } catch (e) {
    return null;
  }
};

// d2l images handling
// temporary function for D2l images
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const base64Conversion = async (file: any) => {
  return await new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};

const lmsDomainCheck = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let lmsUrl: any = sessionStorage.getItem('lmsUrl')?.split('https://');
  lmsUrl = lmsUrl[1].split('.');
  switch (lmsUrl[0]) {
    case 'crystaldelta':
      return 'prod.crystaldelta.net';
    case 'onlinelearning':
      return 'acf-content.com';
    default:
      return `${lmsUrl[0]}-content.com`;
  }
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const uploadLmsLoreeImageToS3 = async (file: any, imageTitle: string) => {
  const id = generateRandomString(6);
  const separateName = imageTitle.split('.');
  const newImageName = `${separateName[0]}_${id}.${separateName[1]}`;
  let imageUrl = '';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  await base64Conversion(file).then(async (imageData: any) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const response: any = await API.graphql(
      graphqlOperation(uploadD2LImagesToS3, {
        platformId: sessionStorage.getItem('ltiPlatformId'),
        file: imageData,
        title: imageTitle,
        newTitle: newImageName,
        lmsUrl:
          sessionStorage.getItem('envName') === 'production' ||
          sessionStorage.getItem('envName') === 'productionus'
            ? await lmsDomainCheck()
            : '',
      }),
    );
    imageUrl = response.data.uploadD2LImagesToS3;
  });
  return imageUrl.replace(/['"]+/g, '');
};

export const getD2LS3Images = async () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const d2lLoreeImagesResponse: any = await API.graphql(
    graphqlOperation(fetchS3Images, {
      platformId: sessionStorage.getItem('ltiPlatformId'),
      lmsUrl:
        sessionStorage.getItem('envName') === 'production' ||
        sessionStorage.getItem('envName') === 'productionus'
          ? await lmsDomainCheck()
          : '',
    }),
  );
  return JSON.parse(d2lLoreeImagesResponse.data.fetchS3Images);
};

// BB images handling
export const gettingContentId = async () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const response: any = await API.graphql(
    graphqlOperation(viewBbContents, {
      courseId: sessionStorage.getItem('course_id'),
    }),
  );
  return JSON.parse(response.data.viewBbContents).body;
};

const urlToBase64 = async (url: string) =>
  await fetch(url, {
    cache: 'no-store',
  })
    .then(async (response) => await response.blob())
    .then(
      async (blob) =>
        await new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.onload = () => resolve(reader.result);
          reader.onerror = reject;
          reader.readAsDataURL(blob);
        }),
    );

// BB upload images - temporary function
export const uploadLmsCourseImageToBB = async (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  file: any,
  imageTitle: string,
  uploadedType: string,
) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let imageData: any;
  let fileType: string;
  const contentId = await gettingContentId();
  const parentId =
    contentId && sessionStorage.getItem('BBCourseStatus') === 'Ultra'
      ? contentId.results[0].parentId
      : contentId.results[0].id;
  switch (uploadedType) {
    case 'globalUpload':
      fileType = `image/${imageTitle.split('.')[1]}`;
      imageData = await urlToBase64(file);
      imageData = `data:image/${imageTitle.split('.')[1]};base64,${imageData.split('base64,')[1]}`;
      break;
    default:
      fileType = file.type;
      imageData = await base64Conversion(file);
      break;
  }
  const imageUrl = await uploadBBImages(imageData, imageTitle, fileType, parentId);
  return await gettingBBImageUrl(imageUrl.split('-')[1]);
};

export const uploadBBImages = async (
  imageData: string,
  imageTitle: string,
  fileType: string,
  parentId: string,
) => {
  try {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const response: any = await API.graphql(
      graphqlOperation(bbCourseImageUpload, {
        courseId: sessionStorage.getItem('course_id'),
        file: imageData,
        fileName: imageTitle,
        fileType: fileType,
        parentId: parentId,
      }),
    );
    return JSON.parse(response.data.bbCourseImageUpload).body.contentDetail[
      'resource/x-bb-file'
    ].file.permanentUrl.split('/')[3];
  } catch (error) {
    apm.captureError(error as Error);
    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    return 'Error while uploading - ' + error;
  }
};

export const gettingBBImageUrl = async (imageUrl: string) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const bbImageResponse: any = await API.graphql(
    graphqlOperation(listCourseImagesById, {
      courseId: sessionStorage.getItem('course_id'),
      resourceId: imageUrl,
    }),
  );
  return JSON.parse(bbImageResponse.data.listCourseImagesById).body.downloadUrl;
};

export const listBbCourseImage = async () => {
  let imageFiles: Array<{
    created_at: string;
    created: string;
    size: number;
    name: string;
    display_name: string;
    type: string;
    mimeType: string;
  }> = [];
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const response: any = await API.graphql(
    graphqlOperation(listBbCourseImages, {
      courseId: sessionStorage.getItem('course_id'),
    }),
  );
  for (const imageFile of JSON.parse(response.data.listBbCourseImages).body.results) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let bbImageList: Array<any> = [];
    if (
      imageFile.mimeType === 'image/jpeg' ||
      imageFile.mimeType === 'image/png' ||
      imageFile.mimeType === 'image/jpg' ||
      imageFile.mimeType === 'image/gif'
    ) {
      imageFiles.push(imageFile);
    } else if (imageFile.type === 'Folder' && imageFile.name === 'READ_ONLY') {
      let id = imageFile.id;
      for (let folderList = 0; folderList < 3; folderList++) {
        const listBBFolderImages = await listBBChildFolderImages(id);
        id = listBBFolderImages.results[0].id;
        bbImageList = listBBFolderImages.results;
      }
      imageFiles = [...imageFiles, ...bbImageList];
    }
  }
  return imageFiles;
};

export const listBBChildFolderImages = async (id: string) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const bbImageResponse: any = await API.graphql(
    graphqlOperation(listChildCourseImages, {
      courseId: sessionStorage.getItem('course_id'),
      resourceId: id,
    }),
  );
  return JSON.parse(bbImageResponse.data.listChildCourseImages).body;
};
