import { API, graphqlOperation } from 'aws-amplify';
import {
  updatePage,
  saveCanvasPage,
  saveCanvasDiscussion,
  saveCanvasAssignment,
  updateD2lContents,
  updateBbContents,
  updateCustomTemplate,
  uploadImagesToS3,
} from '../graphql/mutations';
import {
  SaveCanvasPageMutation,
  SaveCanvasDiscussionMutation,
  SaveCanvasAssignmentMutation,
  UploadImagesToS3Mutation,
} from '../../src/API';
import Base from '../loree-editor/base';
import {
  uploadToCanvas,
  getS3ImageSource,
  uploadToS3,
} from '../lti/admin/globalImagesUpload/uploadImageToS3';
import { ultraContentUpdate } from '../lti/blackBoard/landingPage/bbUltra';
import CONSTANTS from '../loree-editor/constant';
import { getCurrentPlatform, isCanvas } from '../lmsConfig';
import { TemplatePayloadType } from '../views/editor/saveIcon/saveIcon.interface';
import { customBlockThumbnailImage } from '../loree-editor/modules/customBlocks/customBlockEvents';
import { getIFrameElementById } from '../loree-editor/common/dom';
import { apm } from '@elastic/apm-rum';

let app = '';
let pageId = '';
let title = '' as string | undefined;
let contentType = '' as string | undefined;
export let customBlockEditStatus = false;
export let customBlockId: string | null = '';
export let customBlockTitle: string | null = '';
export let customBlockType: string | null = '';
export let customBlockCategoryName: string | null = '';
export let customBlockSelectedCategoryId: string | null = '';
export let removePromptListener = false;
export let autoSaveOff = false;
export let customGlobalStatus: boolean | null;
export let browserPromptStatus = true;

export const d2lProps = {
  moduleType: '',
  moduleContent: '',
  headTagContent: '',
};

export const currentEditBlock = {
  id: '',
};

export const isContainer = {
  active: false,
};

export const setCurrentEditBlock = (id: string) => {
  currentEditBlock.id = id;
};

export const getEditBlockDetails = (): _Any => {
  if (!customBlockEditStatus) return { isCustomBlock: false };
  return {
    isCustomBlock: true,
    customBlockType,
    customBlockTitle,
    customBlockSelectedCategoryId,
  };
};

const getContent = (): string => {
  const base = new Base();
  return base.getHtml();
};

export const updateStandaloneEditorContent = async (pageId: string) => {
  const editorContent = getContent();
  const pageInput = {
    id: pageId,
    content: editorContent,
  };
  const response = await API.graphql(graphqlOperation(updatePage, { input: pageInput }));
  return response;
};

export const updateCanvasEditorContent = async (
  pageId: string,
  type: string,
  modalType?: string,
) => {
  let editorContent = getContent();
  const courseId = sessionStorage.getItem('course_id');
  const s3ImageUrls = getS3ImageSource(editorContent);
  const isCanvasPlatform = isCanvas();

  let response = null;
  let canvasHtmlContent;

  if (!isCanvasPlatform) {
    const convertDataContent = await uploadToCanvas(editorContent);
    updateS3ImgaeURls(s3ImageUrls, convertDataContent, modalType);
    editorContent = convertDataContent;
  }

  try {
    switch (type) {
      case 'page':
        response = await API.graphql<SaveCanvasPageMutation>(
          graphqlOperation(saveCanvasPage, {
            courseId: courseId,
            pageID: pageId,
            editorContent: editorContent,
            uploadAndConvertFromBackendFeatureToggle: isCanvasPlatform,
          }),
        );
        canvasHtmlContent = JSON.parse(response?.data?.saveCanvasPage as string);
        break;
      case 'discussion':
        response = await API.graphql<SaveCanvasDiscussionMutation>(
          graphqlOperation(saveCanvasDiscussion, {
            courseId: courseId,
            discussionID: pageId,
            editorContent: editorContent,
            uploadAndConvertFromBackendFeatureToggle: isCanvasPlatform,
          }),
        );
        canvasHtmlContent = JSON.parse(response?.data?.saveCanvasDiscussion as string);
        break;
      case 'assignment':
        response = await API.graphql<SaveCanvasAssignmentMutation>(
          graphqlOperation(saveCanvasAssignment, {
            courseId: courseId,
            assignmentID: pageId,
            editorContent: editorContent,
            uploadAndConvertFromBackendFeatureToggle: isCanvasPlatform,
          }),
        );
        canvasHtmlContent = JSON.parse(response?.data?.saveCanvasAssignment as string);
    }
  } catch (error) {
    console.error('Error in saving the editor content' + error);
    apm.captureError(error as Error);
    throw new Error('Error in saving the editor content');
  }

  if (isCanvasPlatform) {
    updateS3ImgaeURls(s3ImageUrls, canvasHtmlContent.body.body, modalType);
  }

  return response;
};

export const updateD2lEditorContent = async (
  pageId: string,
  type: string,
  d2lModulesContent: string,
  d2lHeadContent: string,
) => {
  const editorContent = getContent();
  const courseId = sessionStorage.getItem('course_id');
  let data: _Any;
  if (type === 'topics') {
    data = {
      Raw: `<!DOCTYPE html>
      <html>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      ${d2lHeadContent}
      <body>${editorContent}</body>
      </html>`,
    };
  } else {
    data = d2lModulesContent;
    delete data.Description.Text;
    delete data.Description.Html;
    data.Description.Content = editorContent;
    data.Description.Type = 0;
  }
  const response = await API.graphql(
    graphqlOperation(updateD2lContents, {
      courseId: courseId,
      contentId: pageId,
      contentType: type,
      formData: JSON.stringify(data),
    }),
  );
  return response;
};

export const updateBBEditorContent = async (
  pageId: string,
  title: string,
  contentType: string | undefined,
  modalType?: string,
) => {
  const editorContent = getContent();
  const convertDataContent = await uploadToCanvas(editorContent);
  const s3ImageUrls = getS3ImageSource(editorContent);
  updateS3ImgaeURls(s3ImageUrls, convertDataContent, modalType);
  const loreeLinks = `<p><script src="${CONSTANTS.LOREE_JS_URL_BB}"></script></p><p><link rel="stylesheet" media="all" href="${CONSTANTS.LOREE_CSS_URL_BB}"></p>`;
  const bodyContent = loreeLinks.concat(convertDataContent);
  const courseId = sessionStorage.getItem('course_id');
  if (sessionStorage.getItem('BBCourseStatus') === 'Ultra') {
    return await ultraContentUpdate(pageId, title, convertDataContent);
  } else {
    const updateData = {
      title: title,
      body: bodyContent,
      contentHandler: contentType,
    };
    const response = await API.graphql(
      graphqlOperation(updateBbContents, {
        courseId: courseId,
        contentId: pageId,
        updateContentData: JSON.stringify(updateData),
      }),
    );
    return response;
  }
};

export const updateS3ImgaeURls = (
  s3ImageUrls: string[],
  convertDataContent: string,
  modalType?: string,
) => {
  if (s3ImageUrls.length > 0 && modalType !== 'edit') {
    const newBase = new Base();
    newBase.setHtml(convertDataContent);
  }
};

export const setPageDetails = (
  appType: string,
  pageID: string,
  pageTitle?: string,
  pageContentType?: string,
) => {
  app = appType;
  pageId = pageID;
  title = pageTitle;
  contentType = pageContentType;
};

export const setD2lProps = (moduleType: string, moduleContent: string, headTagContent: string) => {
  d2lProps.moduleType = moduleType;
  d2lProps.moduleContent = moduleContent;
  d2lProps.headTagContent = headTagContent;
};

export const fetchPageDetails = () => {
  return { app, pageId, title, contentType };
};

export const setEditMode = (status: boolean) => {
  customBlockEditStatus = status;
  if (!status) customBlockType = '';
};

export const changeCustomBlockTitle = (title: string | null) => {
  customBlockTitle = title;
};

export const changeCustomBlockCategoryId = (id: string | null) => {
  customBlockSelectedCategoryId = id;
};

export const changeCustomBlockCategoryName = (name: string | null) => {
  customBlockCategoryName = name;
};

export const removePrompt = (status: boolean) => {
  removePromptListener = status;
};

export const handleAutoSaveOnCustomBlockAppend = (status: boolean) => {
  autoSaveOff = status;
};

export const editContentDetails = (
  title: string | null,
  id: string | null,
  categoryId: string | null,
  category: string | null,
  type: string | null,
  globalStatus: boolean | null,
) => {
  customBlockTitle = title;
  customBlockId = id;
  customBlockType = type;
  customBlockCategoryName = category;
  customBlockSelectedCategoryId = categoryId;
  customGlobalStatus = globalStatus;
};

export const browserPromptHandler = (status: boolean) => {
  browserPromptStatus = status;
};

export const updateCurrentTemplate = async (templateTitle: string, categoryID: string) => {
  const { ltiPlatformID, currentUserId } = await getCurrentPlatform();
  let editorContent = getContent();
  const iframeContent = getIFrameElementById('loree-iframe-content-wrapper') as HTMLIFrameElement;
  if (!isCanvas()) {
    editorContent = await uploadToS3(editorContent, `${currentUserId}`);
  } else {
    const response = await API.graphql<UploadImagesToS3Mutation>(
      graphqlOperation(uploadImagesToS3, {
        globalHtmlContent: editorContent,
        type: `${currentUserId}`,
      }),
    );
    editorContent = JSON.parse(response?.data?.uploadImagesToS3 as string);
  }
  const templatePayload: TemplatePayloadType = {
    title: templateTitle,
    content: editorContent,
    thumbnail: await customBlockThumbnailImage(templateTitle, 'template', iframeContent),
    status: true,
    isGlobal: false,
    active: true,
  };
  templatePayload.id = customBlockId;
  templatePayload.categoryID = categoryID;
  templatePayload.ltiPlatformID = ltiPlatformID;
  await API.graphql(graphqlOperation(updateCustomTemplate, { input: templatePayload }));
};

export const convertCourseImageAsS3Image = async (editorContent: string, currentUserId: number) => {
  if (!isCanvas()) {
    return await uploadToS3(editorContent, `${currentUserId}`);
  } else {
    const response = await API.graphql<UploadImagesToS3Mutation>(
      graphqlOperation(uploadImagesToS3, {
        globalHtmlContent: editorContent,
        type: `${currentUserId}`,
      }),
    );
    return JSON.parse(response?.data?.uploadImagesToS3 as string);
  }
};
