import API from '@aws-amplify/api';
import * as imageUpload from './imageUpload';
import { handleUploadImage } from './imageUpload';
import { StorageMock } from './storageMock';
import { Storage } from 'aws-amplify';

describe('#bb course image list', () => {
  beforeEach(() => {
    global.sessionStorage = new StorageMock() as any;
  });
  describe('#get content by id', () => {
    const mockData = {
      statusCode: 200,
      body: {
        results: [
          {
            id: '_51090_1',
            parentId: '_1110_1',
            title: 'CD-Home-hero-backgrnd-1920z1000.jpg',
            created: '2021-05-24T08:05:39.646Z',
            modified: '2021-05-28T02:02:53.203Z',
            position: 0,
            launchInNewWindow: true,
            reviewable: false,
            availability: { available: 'No', allowGuests: true, adaptiveRelease: {} },
            contentHandler: {
              id: 'resource/x-bb-file',
              file: { fileName: 'CD-Home-hero-backgrnd-1920z1000.jpg' },
            },
            links: [
              {
                href: '/ultra/redirect?redirectType=nautilus&courseId=_22_1&contentId=_51090_1&parentId=_1110_1',
                rel: 'alternate',
                title: 'User Interface View',
                type: 'text/html',
              },
            ],
          },
          {
            id: '_51099_1',
            parentId: '_1110_1',
            title: 'test',
            created: '2021-05-24T09:10:19.875Z',
            modified: '2021-05-28T02:09:45.860Z',
            position: 5,
            hasChildren: true,
            launchInNewWindow: false,
            reviewable: false,
            availability: { available: 'Yes', allowGuests: true, adaptiveRelease: {} },
            contentHandler: { id: 'resource/x-bb-folder' },
            links: [
              {
                href: '/ultra/redirect?redirectType=nautilus&courseId=_22_1&contentId=_51099_1&parentId=_1110_1',
                rel: 'alternate',
                title: 'User Interface View',
                type: 'text/html',
              },
            ],
          },
        ],
      },
    };
    const contentData = {
      data: {
        viewBbContents: JSON.stringify(mockData),
      },
    };
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(() => contentData);
    });
    test('checks the parentId', async () => {
      expect((await imageUpload.gettingContentId()).results[0].parentId).toEqual('_1110_1');
    });
    test('checks the id', async () => {
      expect((await imageUpload.gettingContentId()).results[0].id).toEqual('_51090_1');
    });
  });

  describe('#list BB images', () => {
    describe('#file type', () => {
      const resourceData = {
        statusCode: '200',
        body: {
          results: [
            {
              created: '2021-05-03T05:05:58.000Z',
              creatorId: '_13_1',
              downloadUrl:
                'https://crystaldelta-eval.blackboard.com/bbcswebdav/xid-373934_1?VxJw3wfC56=1622270332&3cCnGYSz89=09IVFfnJ4xYodJUaZ41fy%2FLS8P9EBwNjd1Wign0ofWU%3D',
              id: '_373934_1',
              mimeType: 'image/jpeg',
              modified: '2021-05-03T05:05:58.000Z',
              name: '8a4b6a3b761f2f6154f44a69e34a4d75.jpg',
              parentId: '_8929_1',
              size: 32206,
              type: 'File',
            },
          ],
        },
      };
      const resourceMockData = {
        data: {
          listBbCourseImages: JSON.stringify(resourceData),
        },
      };
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(() => resourceMockData);
      });
      test('checks the file and mime type', async () => {
        expect((await imageUpload.listBbCourseImage())[0].type).toEqual('File');
        expect((await imageUpload.listBbCourseImage())[0].mimeType).toEqual('image/jpeg');
      });
    });
    describe('#folder type', () => {
      const resourceData = {
        statusCode: '200',
        body: {
          results: [
            {
              created: '2021-04-27T04:49:46.000Z',
              creatorId: '_13_1',
              id: '_317658_1',
              modified: '2021-04-27T04:49:46.000Z',
              name: 'READ_ONLY',
              parentId: '_8929_1',
              size: 8824426,
              type: 'Folder',
            },
          ],
        },
      };
      const resourceMockData = {
        data: {
          listChildCourseImages: JSON.stringify(resourceData),
        },
      };
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(() => resourceMockData);
      });
      test('checks type is folder', async () => {
        expect((await imageUpload.listBBChildFolderImages('_317658_1')).results[0].type).toEqual(
          'Folder',
        );
      });
      test('checks name is READ_ONLY folder', async () => {
        expect((await imageUpload.listBBChildFolderImages('_317658_1')).results[0].name).toEqual(
          'READ_ONLY',
        );
      });
    });
  });
  describe('#get resource', () => {
    const resourceData =
      '{"statusCode":200,"body":{"id":"_380408_1","name":"04-nature_721703848(4).jpg","type":"File","size":150748,"parentId":"_376237_1","creatorId":"_20_1","created":"2021-06-24T04:22:57.000Z","modified":"2021-06-24T04:22:57.000Z","mimeType":"image/jpeg","downloadUrl":"https://crystaldelta-eval.blackboard.com/bbcswebdav/xid-380408_1?VxJw3wfC56=1624509478&3cCnGYSz89=uIxRQSDtleQTyyurj%2ByRTwdvHkN2l8LoUXH7X7S%2B4pY%3D"}}';
    const resourceMockData = {
      data: {
        listCourseImagesById: resourceData,
      },
    };
    beforeEach(() => {
      API.graphql = jest.fn().mockImplementation(() => resourceMockData);
    });
    test('resource by id', async () => {
      expect(await imageUpload.gettingBBImageUrl('_380408_1')).toEqual(
        'https://crystaldelta-eval.blackboard.com/bbcswebdav/xid-380408_1?VxJw3wfC56=1624509478&3cCnGYSz89=uIxRQSDtleQTyyurj%2ByRTwdvHkN2l8LoUXH7X7S%2B4pY%3D',
      );
    });
  });
  describe('#upload bb image', () => {
    describe('image uploaded successfully', () => {
      const resourceData =
        '{"statusCode":201,"body":{"iconUrl":"/images/ci/sets/set12/file_non.svg","launchInNewWindow":false,"contentDetail":{"resource/x-bb-file":{"fileAssociationMode":"EMBED","file":{"mimeType":"image/jpeg","existingFileReference":"_3856_1","permanentUrl":"/bbcswebdav/pid-53689-dt-content-rid-380411_1/xid-380411_1","fileName":"wp3819619 (1).jpg","isMedia":true}}},"description":"","contentHandler":"resource/x-bb-file","position":7,"state":"None","inSequence":false,"renderType":"REG","inLesson":false,"titleColor":"#000000","isFromCartridge":false,"isTracked":false,"isReviewable":false,"isDescribed":false,"allowGuests":true,"allowObservers":true,"parentId":"_1082_1","isSampleContent":false,"isPartiallyVisible":false,"visibility":"VISIBLE","courseId":"_19_1","body":{"rawText":"","displayText":"","webLocation":"https://crystaldelta-eval.blackboard.com/courses/1/L01/content/_53689_1/embedded/","fileLocation":"BB%3FBB_RXEuqMBWvu0GPoCGOZO8u7gu2sZTjp0pQ89%2BEDuJUHdxNFKkSuM9pUxUMCvauDdGdMR7QLq9eYCz9TeF9yFlaeEE0yTG1YGtUbNmOvFzaY8%3D"},"isAvailable":false,"title":"wp3819619 (1).jpg","isGroupContent":false,"permissions":{"delete":true,"copy":true,"modifyAvailability":true,"dashboardView":true,"createLearningStandardsAlignment":true,"viewDesigner":true,"contentHandlerPermissionMap":{"createDiscussion":true},"adaptiveRelease":{"delete":true,"create":true,"criteria":{"performance":{"delete":true,"create":true,"edit":true},"acl":{"delete":false,"create":false,"edit":false},"dates":{"delete":true,"create":true,"edit":true}},"edit":true,"view":true},"edit":true},"id":"_53689_1"}}';
      const resourceMockData = {
        data: {
          bbCourseImageUpload: resourceData,
        },
      };
      let imageData: string;
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(() => resourceMockData);
        sessionStorage.setItem('course_id', '_19_1');
        imageData =
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAR8AAACwCAMAAAABtJrwAAABAlBMVEX///8zMzNon2Nch2OBv3gvLy9SUlJyq25nnGhup2xro2pzrW9pn2ksLCx2sHFspGpjl2UmJiZ7tnRamFRfmlp9unaEhIQnJydilGWgwJ20tLRwcHBUglytwLD2+fZfj2Tq8Om/1L2amppDQ0MICAgdHR0UFBRbW1vw8PBahGJ5eXnV1dX29vauyaza5tmQkJB6qXYAAAClpaWLs4c9PT3i4uKysrLHx8eWrpmrx6nK28nd6NxclV6WupOPtoy80rp2umxhpV5mZmZJfFJ7pXyLwIWlzqDA3LzX6dWcy5W117HAzsJWi1qWtZhboVmZw5dGg0xuk3SEoohvn3FGeVBOkkf9rVKvAAANJElEQVR4nO2d/UPaOhfHWxDFCiq2DlAGVQGtCrIxZRYYc96X3b3e3efu//9XniZt2qR5aVq6C9p+f9hQaZp+mpzkJOe0ivK0dXu2OVl1HdZZL2ql/ubtqmuxvnpRV9VS9/hi1fVYVwE+qlrt3qy6Imsql4+q1np3q67KWgrxUdXem9xQ0wr4qKX+eW6ow8L4ADN0eb3qCq2ZCD6qWu+frrpG66UQH8dQ13IzhInio6qvrlZdqTUSg0/1eNWVWiMx+JQ2V12pNVLOR6ycj1g5H7FyPmLlfMTK+YiV8xEr5yNWzkesnI9YOR+xcj5i5XzEyvmIlfMRK+cjVs5HrJyPWDkfsXI+YuV8xMr5iJXzESvnI1bOR6wV87la973+lfK56/X6ZykRsr/P7XRKUq7v7vx41SX5TDarl0mDziZveuBs/TSCZ6e7Dztbs9/M5UtSlNNur+bHqy7F53azX0oa+3rxwTkWqtpdNiar/f1hZ2dra3t/9rW9ZFHKpFQDlarVjuCPS/C5ftF1rxCVFUc33Wpwxlo9fgGYPj3s7kI+lf3ZbLFMSeCOo0r1YM9Pzud1Pzi0F9OKHPVq5Dl7m4nN0MeDgwOPz/b+fnlWHiYtCdxx7K7Bnr9ZSsbHNR7BMd1j+cjOq7MefdKEoaHm7+8xPhUHUHk2NpOURN5xt+f3ezQeGT6B8cDKkrQiF8fUsfEKwNT+4/3hIcYHNKByuWLcxy6JuuN8RfMhjIevmioT2XnKPNYtoBQzNPTP9xuHJB8NAirPZifxSmLd8aR8KOPhHxgdYH5U5xzrKlYq0ceN9xshPnt7ZRdQZTaaypfEu+NJ+LCMh69q94Xw2M2oJuwUIGmGzL8cOjQfpwG5gCrGO+mxnnvHY/PhGQ9fdX6ey/VlV6IJ1/uvZS7pE6TD4OM2oIqjgjGQoiO84/H4CIyHL95YT40O3AKi7difHh0Gn6ABOYQK0S5H5B2X53NUlWqGTI9hoso34Sg7Ntzw8TD4VAJAmmaMTDEemTsux+c20nj4ojyG2/N4N0nks7T/Cujw+ASANKMpMEMTuTsuwUfOePiqVbE+Qk5LJQvg2bFPOB0mn/K+b4IAoILBczli3HFcLD7SxsNXMFTHPxaKacc+bpB4WHwQoLIHaK+wx3I5Yt7xQDSfiZoANBqqY44OWD2oycLF7yE6fD5YD3MIGWOqkyW8ayrN5zqm8fBV7ztu+U2CHo7UJQey9uN+GA+fDwZoT9vbboTwJOtaUGE+alLQqvpqwnKApVUjrfTLlnUoxQdvQLAF7Wmtz2TrSY4nzGeyTFHnylnyo9U6yedtq9iR5NMge9h2p/WSKOo49oCBXRTJ52iJDqKepczH2pVuPzigipURPlQD4vFBDQgC2usUnyafaq8mMk8MPtaWZPvBe1jZWhkf8XqFmE+1ezk5Eg0kDD7Fonz/QoA056g4fGqq0BmKwafavzntiYY3AR/kZQm8OhYfS5NvPx6gfSsOH7iOKXLLpPl4jqjIe+DzwVYbuZVhth9Lkk+jgfyMYoz2g9bBBW69LB/fARB4n2fKJvvKiVUeXmXYfMqSfFADasTgg+2jcJeF5PgQDiTXAeHw6Yc3K67OWL2UyadITBIj2g8AVJTmE9rI4ywryvChFiDY7oxTFIsPyzdn1TjE52unA/k05Ph0IKByR5ZPiV5Uuekm41P1F7BstLxyfcn4ntOLGHzYCyh9+othPl86kJB1IMenA5tQUZJP6Q0jDuAiGR/V+5s50g0dLa+ch88JHXAGnzpzo+uNHB9AqCHLx/m/I8unesmqFQtkJB/HrQJqz/WCI8Nb5b0J9TDXfjP41Jh8zmgTTfFxAXWKu3J8HECN4sr42LpRcKWPwM/kVj6ydanyaTQ8QpJ8OgjPKvg0ER4HUDvEJ7DfKfNxCbW2JPl01pJPqf/Bt3Wp83EJSfIpriMfYgNrKT7kF7/O4LQGEPK9jKfHJ7QBmiqfmTvxa3R8L+Op8aHieFLkM5/NPEKBl/HE+PSoAIw0+RgzSGjWCbyMNeYTHt9P+6xAxPT5zPYxL2ON+Sjtpjc/9LbfXrMiL5bhUwvzMSAh96pdL2M9+Zx5fyP8C7bS5KMbkJB3yZ10+bDDDhi1kvC/go0pW7T9D/lEXnZMPga6ZGsnVT5qqUdZhzuJ+EymfyofKpg6H99lgEvRKfKhtvw5K2Ry62OyoYJp8vmGNx+nAW2nzIcIXeJGbEqur0pGLKfI5/u3B+MhuGTYgNLlg4Uu8SM2pdfnpUIF0+XzYFgYHquSOh8QunQELpq/axNjf0ciVDBlPkVC1mH6fMAkVxjdEWd/MDrkOV0+M4vks7/xC/ioJWHMSbz95aiI5XT5FEOyDn4FH7Hi7r/XeyIzlCqf/TAfZ5K49nzEj/Zn8KHnYUAf6CqH+PzxzaL4WLsr5yMRH4UvGIZ0TvGpV5lfvHpFfTPEZ/o3hcdpQHJ8/iXrd7pEUEqdDIu8Zu2RUeJGLIf5OCg59uqK2oQNd8TPjy26AW0fRvNptUIRrNfVxEGD1X6oKUzkIj05EcshPsK3bIQ3YSlD1X77SLegSD6tx7fUqZJG97KSA2Tju5kRywQfQfYK60wMQ27+CDchqxLB5/Efk3WuRNHhnOQSyfwAlhnC+ERkP3m1xs7EHOg+t8KEhHxaRW4+qmQ+SSDBS3gksw9pM+TzkX09C5bJwp4IKC9JM2SV+Xxajy+ZRXiKlZ0Skcj+WhgohjEmDQziE+P1Pr4Z4vBR2v8QZsja5fF5fBuxOiWf3RR9e2XN0CsGH8msN3Qmz3jy+DhDfRFvQo0DJp8vP8zoc0lmx0l4mpJmqNojjgF85LMm/TNB48nnQ5oha4vB50tH8nkcd9HZldK3N9IMUSb6vJTsvVAgNUvER1Gwsb5zQPH5W2h4SEVk58bKzb8TmiF6/Dt6JZX2zdBpty/mavpmyNrbJfnEfBCHsGPQe3pi8c1QrcTwrH7lG7OGvhki+Mz+Fyu5G4gbQQmXy+Lplr0yG88GpyRvrLcqAZ/4DweAYkZQJny2DMMMxbfB6Qi5HLuID//hEtPmeDzg9zva5Vji2UQho++Y5ZW9m8/88QgmiTuQj8F9OEl7DPbDDV3wbI6QyxH3qTSkcKO/XElLC4z1Fmg/7ExTqHvdCxYwDMG4jyU0L/1GT98MJTBhacsxQ42dbW6msmIbQbBJQRc9m8NzOVJ5I6xjhkqllZhlSu23//Iz3acjt/EYmodJn/PN0MVxt15P61Wed5tvbtb9lZdeGLLTsZxxrel9FkVVXN28yNA7GAdemJLehD+aqC1JPJsjA7ILHp1gXBuiX0U9m+P5K2gsxLi2IJtUZsU1NoRJyqpORIOV37K05I8DfNIaahFWJrBMSz9z8+nJcSaiRyl/ZEvyOMAnrXtkeMRPIGu/Q2YoU2M9cib06CfYTf1eiLkc5ng00pZ7jusaK3AmpBbJaCtuj8CnxejXVXGVuo89cvud0R3J2poy0LSmYj9Lq7TQ48/8TM+Y6yb46X6ozG3F+az9mhquVnrImZCTNxkYg8/OP82m7fSuZsxCnoJscJ1JpnwLcKAOPgE+88Ez5XNioG6S+MgB7F8/20oh1Zqth+BVikf19nRo28Np+Es24tMuKAtbmZuL5zjCM/gMBoN7f6S354YOszh0XR+f4F/0+ThzIvDf/fg/qO5/LppP+6cDwxvNHJcLW4Ym9zICPo53r41Gz3NGzeDjjGiGy6epF0IyNP+7dmLL9ZQk4jNAM0enb+moIfmTnMzzMb2p0cnQdDQ8cX1T/xnamedzD1qMYQZ/MUcFNOdRcj4KoBFax4DEvMlk5vkYBQoAmDSjDpbzofmYYOx/537OPB9obUbk3PFksVh4c8fM84H2uaCPBrbJOjLzfExvdgjmP9q7+5Mh2ZQyz0c5wafPwAUrNLHhLOdDBgF5kHwnPeejgEV34LwTiNAyfM7H/XG4aI4KeoDJ8NYxcj7Eb6f2/cjdPtXdAT7nQ6kNn4DlTaCzzuedbujUlh+cE83hx6zzAdNnamV6aKBdnczzmRuU++5CMfL2A/jADa7wls04tz+ITxvOnslgqSbm02edj/e4Rn20sKdwgdVuwilQPv9B47sX6uOuz/sTRMP7ds5HGVH7OwWj4Ef95HyUk4L/yE9vgzCIv8v5AA2bmo42mI05PtznfJDM6dCRGZorZoMPjP8hoqME/heuocaaXz87ubMcPLpOio8XYfdMQzJxuavwWHSmKcHHC1zQs5BqgALnK96+DZgW8vMugfwI3+cYEEUriA4HhGDwriFoGMwI8ectlKqs6958R+d+NZsZBig7BU0DuR0nsxkqKLuJ2KIIK9MZTrYG20YQfBhW5jPkpoPxaDRfmMw/5hmWQuUZuoTsMTF45xnehGzH0mBJuqZM7mV21P7pBqqi6bGf9C3OvcyO/FxdMFAJk76zKj/XezzKnDMhpwW+sJotZ0JOaL6TRWdCTp6jnklnQk72uKDN18zw/B8pfyGQTizTcAAAAABJRU5ErkJggg==';
      });
      test('upload', async () => {
        expect(
          await imageUpload.uploadBBImages(imageData, 'test-image', 'image/png', '_1081_1'),
        ).toEqual('xid-380411_1');
      });
    });
    describe('image not uploaded', () => {
      const resourceData = {
        data: { bbCourseImageUpload: null },
        errors: [
          {
            path: ['bbCourseImageUpload'],
            data: null,
            errorType: 'Lambda:Unhandled',
            errorInfo: null,
            locations: [{ line: 2, column: 3, sourceName: null }],
            message: '400 - "{\\"status\\":400,\\"message\\":\\"No files included in request\\"}"',
          },
        ],
      };
      let imageData: string;
      beforeEach(() => {
        API.graphql = jest.fn().mockImplementation(() => resourceData);
        sessionStorage.setItem('course_id', '_19_1');
      });
      test('upload', async () => {
        expect(await imageUpload.uploadBBImages(imageData, '', '', '_1081_1')).toEqual(
          `Error while uploading - TypeError: Cannot read property 'body' of null`,
        );
      });
    });
  });
});
describe('save to canvas', () => {
  const apiDummyResult = { data: { imageUpload: '{"result": "DummyResult"}' } };
  beforeEach(() => {
    Storage.put = jest.fn().mockImplementation(() => {});
    Storage.get = jest.fn().mockImplementation(() => 'http://dummy-file.org/');
    API.graphql = jest.fn().mockImplementation(() => apiDummyResult);
  });
  test('saving a file with correct name and type', async () => {
    await handleUploadImage('', 'dummyFileName', 'CANVAS');
    expect(API.graphql).toBeCalled();
  });
});
