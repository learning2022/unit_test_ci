import API from '@aws-amplify/api';
import * as saveContent from './saveContent';
import { StorageMock } from './storageMock';
import * as upload from '../lti/admin/globalImagesUpload/uploadImageToS3';
import CONSTANTS from '../loree-editor/constant';
import { LMSData, uploadedDataInCanvasPage, saveToLmsMockData } from './saveContentMock';
import { createDiv, createElement } from '../loree-editor/common/dom';
import Base from '../loree-editor/base';
import { appendElementToBody } from '../loree-editor/utils';

describe('save content to BB', () => {
  const correctResourceMockData = {
    data: {
      updateBbContents: LMSData.correctResourceData,
    },
  };
  const wrongResourceMockData = {
    data: {
      updateBbContents: LMSData.wrongResourceData,
    },
  };

  beforeEach(() => {
    upload.uploadToCanvas.bind = jest.fn().mockImplementation(() => '');
    upload.getS3ImageSource.bind = jest.fn().mockImplementation(() => []);
    global.sessionStorage = new StorageMock() as any;
    sessionStorage.setItem('course_id', '_19_1');
    window.sessionStorage.setItem('lmsUrl', 'https://test.com');
    sessionStorage.setItem('BBCourseStatus', 'original');
  });

  test('save content with BB js and css URL', async () => {
    API.graphql = jest.fn().mockImplementation(() => correctResourceMockData);
    let saveResponse: _Any = await saveContent.updateBBEditorContent(
      LMSData.id,
      LMSData.page,
      LMSData.contentType,
    );

    saveResponse = JSON.stringify(saveResponse.data.updateBbContents);
    expect(saveResponse.includes(CONSTANTS.LOREE_JS_URL_BB)).toEqual(true);
    expect(saveResponse.includes(CONSTANTS.LOREE_CSS_URL_BB)).toEqual(true);
    expect(saveResponse.includes(CONSTANTS.LOREE_CSS_URL)).toEqual(false);
  });
  test('save content with common js and css URL', async () => {
    API.graphql = jest.fn().mockImplementation(() => wrongResourceMockData);
    let saveResponse: _Any = await saveContent.updateBBEditorContent(
      LMSData.id,
      LMSData.page,
      LMSData.contentType,
    );

    saveResponse = JSON.stringify(saveResponse.data.updateBbContents);
    expect(saveResponse.includes(CONSTANTS.LOREE_JS_URL)).toEqual(true);
    expect(saveResponse.includes(CONSTANTS.LOREE_CSS_URL)).toEqual(true);
    expect(saveResponse.includes(CONSTANTS.LOREE_CSS_URL_BB)).toEqual(false);
  });
});

describe('Update s3 image URLs while saving', () => {
  let s3ImageUrls: string[];
  const baseInstance = new Base();
  document.body.innerHTML = '';
  const doc = document.body;
  const parentWrapper = createElement('div');
  parentWrapper.className = CONSTANTS.LOREE_IFRAME_CONTENT_COLUMN;
  doc.appendChild(parentWrapper);
  const anchorTag = createElement('a') as HTMLAnchorElement;
  anchorTag.href = '';
  parentWrapper.appendChild(anchorTag);
  const divTag = createElement('div');
  divTag.id = 'test-wrapper';
  divTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE_WRAPPER;
  anchorTag.appendChild(divTag);
  const imgTag = createElement('img') as HTMLImageElement;
  imgTag.src = '';
  imgTag.className = CONSTANTS.LOREE_IFRAME_CONTENT_IMAGE;
  divTag.appendChild(imgTag);

  const iframe = createElement('iframe') as HTMLIFrameElement;
  const iframeDocument = createDiv('loree-wrapper');

  function loreeWrapper() {
    iframeDocument.title = 'loree iframe';
    iframe.innerHTML = '';
    appendElementToBody(iframeDocument);
    iframe.id = CONSTANTS.LOREE_IFRAME;
    const wrapper = createDiv(CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER);
    iframe.appendChild(wrapper);
    document.body.appendChild(iframe);
    const iframeDoc = iframe.contentWindow?.document;
    iframeDoc?.open().write(iframe.innerHTML);
    iframeDoc?.close();
  }

  beforeEach(() => {
    loreeWrapper();
    baseInstance.getDocument = jest.fn().mockImplementation(() => document);
  });

  test('updates editor when s3 image url is uploaded', () => {
    s3ImageUrls = ['https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg'];
    saveContent.updateS3ImgaeURls(s3ImageUrls, doc.innerHTML, '');
    const editorContent = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
    ) as HTMLElement;
    expect(editorContent.innerHTML).not.toBe('');
  });

  test('does not update editor when no s3 image url is uploaded', () => {
    s3ImageUrls = [];
    saveContent.updateS3ImgaeURls(s3ImageUrls, doc.innerHTML, '');
    const editorContent = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
    ) as HTMLElement;
    expect(editorContent.innerHTML).toBe('');
  });

  test('does not update editor when switched to template edit option', () => {
    s3ImageUrls = ['https://loree-files.s3.ap-southeast-2.amazonaws.com/image_preset.svg'];
    saveContent.updateS3ImgaeURls(s3ImageUrls, doc.innerHTML, 'edit');
    const editorContent = iframe.contentDocument?.getElementById(
      CONSTANTS.LOREE_IFRAME_CONTENT_WRAPPER,
    ) as HTMLElement;
    expect(editorContent.innerHTML).toBe('');
  });
});
describe('save content to Canvas', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    window.sessionStorage.setItem('lmsUrl', 'https://test.com');
    sessionStorage.setItem('course_id', '875');
    sessionStorage.setItem('domainName', 'canvas');
    const htmlContentInTheDocument = `<div></div><h1>Bruce Wayne</h1></div><div><img src='bruce_wayne.jpg' /></div></div>`;
    const baseInstance = new Base();
    baseInstance.getHtml = jest.fn().mockImplementation(() => htmlContentInTheDocument);
  });

  test.each(saveToLmsMockData)(
    'save editor content as page/discussion/assignment',
    async ({ type, lmsSavedContent }) => {
      API.graphql = jest.fn().mockImplementation(() => lmsSavedContent);
      expect(await saveContent.updateCanvasEditorContent('20', type, 'edit')).toBe(lmsSavedContent);
    },
  );
  test('Save editor content as page with error', async () => {
    API.graphql = jest.fn().mockImplementation(() => uploadedDataInCanvasPage);
    await expect(async () => {
      await saveContent.updateCanvasEditorContent('20', 'assignment', 'edit');
    }).rejects.toThrow();
  });

  test.each(saveToLmsMockData)(
    'save editor content as page/discussion/assignment based on domain',
    async ({ type, lmsSavedContent }) => {
      sessionStorage.setItem('domainName', 'BB');
      const uploadToCanvasMock = jest.spyOn(upload, 'uploadToCanvas');
      API.graphql = jest.fn().mockImplementation(() => lmsSavedContent);
      const savedContent = await saveContent.updateCanvasEditorContent('20', type, 'edit');
      expect(uploadToCanvasMock).toBeCalledTimes(1);
      expect(savedContent).toEqual(lmsSavedContent);
    },
  );
});

describe('#Convert course image to s3', () => {
  beforeEach(() => {
    API.graphql = jest.fn();
  });
  test('upload course image to s3 when a custom block is saved in canvas', () => {
    sessionStorage.setItem('domainName', 'canvas');
    saveContent.convertCourseImageAsS3Image('', 1);
    expect(API.graphql).toHaveBeenCalled();
  });
  test('upload course image to s3 when a custom block is saved not in canvas', () => {
    sessionStorage.setItem('domainName', 'BB');
    const uploadToS3Mock = jest.spyOn(upload, 'uploadToS3');
    saveContent.convertCourseImageAsS3Image('', 1);
    expect(uploadToS3Mock).toHaveBeenCalledWith('', '1');
  });
});
