export const attatchHorizontalLine = (element: HTMLElement) => {
  element.appendChild(document.createElement('hr'));
};

// TODO relocate - this is doing s3 + image logic - so should be in a imageUtil.ts file or similar
export const getS3Key = (imageUrl: string) => {
  try {
    const key1 = new URL(imageUrl).pathname;
    const key2 = key1.split('?');
    const key3 = key2[0].split('/');
    return key3;
  } catch (e) {
    console.log('error on  URL', e);
    return null;
  }
};
