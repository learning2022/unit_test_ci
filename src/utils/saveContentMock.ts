export const LMSData = {
  correctResourceData:
    '{"statusCode":200,"body":{"id":"_61510_1","parentId":"_33077_1","title":"BB-play","body":"<p><script src=\\"https://alt-60189327d4180.blackboard.com/bbcswebdav/library/Library%20Content/js/Loree.js\\"></script></p> \\n<p> \\n <link rel=\\"stylesheet\\" media=\\"all\\" href=\\"https://alt-60189327d4180.blackboard.com/bbcswebdav/library/Library%20Content/css/Loree.css\\"></p> \\n<div class=\\"loree-iframe-content-row row\\" style=\\"padding: 10px; position:relative; margin: 0px\\"> \\n <div class=\\"col-12 loree-iframe-content-column\\" style=\\"padding:10px;\\"> \\n  <p class=\\"loree-iframe-content-element\\" style=\\"border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: &quot;Source Sans Pro&quot;; font-size: 20px;\\">Insert text here</p> \\n </div> \\n</div>","created":"2021-11-29T11:01:32.881Z","modified":"2021-11-29T12:24:45.819Z","position":1,"launchInNewWindow":false,"reviewable":false,"availability":{"available":"Yes","allowGuests":true,"adaptiveRelease":{}},"contentHandler":{"id":"resource/x-bb-document"},"links":[{"href":"/ultra/courses/_19_1/cl/outline?legacyUrl=%2Fwebapps%2Fblackboard%2Fexecute%2FdisplayIndividualContent%3Fcourse_id%3D_19_1%26content_id%3D_61510_1","rel":"alternate","title":"User Interface View","type":"text/html"}]}}"',
  wrongResourceData:
    '{"statusCode":200,"body":{"id":"_61510_1","parentId":"_33077_1","title":"BB-play","body":"<p><script src=\\"https://loree-files.s3-ap-southeast-2.amazonaws.com/Loree-2.0.js\\"></script></p> \\n<p> \\n <link rel=\\"stylesheet\\" media=\\"all\\" href=\\"https://wysiwyg-page-builder.s3.ap-southeast-2.amazonaws.com/Loree+CSS+and+JS/Loree_CSS.css\\"></p> \\n<div class=\\"loree-iframe-content-row row\\" style=\\"padding: 10px; position:relative; margin: 0px\\"> \\n <div class=\\"col-12 loree-iframe-content-column\\" style=\\"padding:10px;\\"> \\n  <p class=\\"loree-iframe-content-element\\" style=\\"border-width: 0px; border-style: solid; border-color: #000000; color: #000000; padding: 5px; margin: 0px 0px 10px; font-family: &quot;Source Sans Pro&quot;; font-size: 20px;\\">Insert text here</p> \\n </div> \\n</div>","created":"2021-11-29T11:01:32.881Z","modified":"2021-11-29T12:24:45.819Z","position":1,"launchInNewWindow":false,"reviewable":false,"availability":{"available":"Yes","allowGuests":true,"adaptiveRelease":{}},"contentHandler":{"id":"resource/x-bb-document"},"links":[{"href":"/ultra/courses/_19_1/cl/outline?legacyUrl=%2Fwebapps%2Fblackboard%2Fexecute%2FdisplayIndividualContent%3Fcourse_id%3D_19_1%26content_id%3D_61510_1","rel":"alternate","title":"User Interface View","type":"text/html"}]}}"',
  id: '61510_1',
  page: 'BB-play',
  contentType: "{ id: 'resource/x-bb-document' }",
};

export const uploadedDataInCanvasPage = {
  data: {
    saveCanvasPage: `{"body": {"body": "<div><div><h1>Bruce Wayne</h1></div><div><img src='bruce_wayne.jpg'/></div></div>"}}`,
  },
};

export const uploadedDataInCanvasDiscussion = {
  data: {
    saveCanvasDiscussion: `{"body": {"body": "<div><div><h1>Bruce Wayne</h1></div><div><img src='bruce_wayne.jpg'/></div></div>"}}`,
  },
};

export const uploadedDataInCanvasAssignment = {
  data: {
    saveCanvasAssignment: `{"body": {"body": "<div><div><h1>Bruce Wayne</h1></div><div><img src='bruce_wayne.jpg'/></div></div>"}}`,
  },
};

export const saveToLmsMockData = [
  { type: 'page', lmsSavedContent: uploadedDataInCanvasPage },
  { type: 'discussion', lmsSavedContent: uploadedDataInCanvasDiscussion },
  { type: 'assignment', lmsSavedContent: uploadedDataInCanvasAssignment },
];
