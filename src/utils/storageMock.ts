/* eslint-disable */ // Remove this line when editing this file
//to fake local and session storage
export class StorageMock {
  private store: any;

  constructor() {
    this.store = {};
  }

  clear() {
    this.store = {};
  }

  getItem(key: any) {
    return this.store[key] || null;
  }

  setItem(key: any, value: any) {
    this.store[key] = value;
  }

  removeItem(key: any) {
    delete this.store[key];
  }
}
