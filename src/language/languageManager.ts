/* eslint-disable unicorn/no-abusive-eslint-disable */
/* eslint-disable */ // Remove this line when editing this file
import en from '../language/en.json';
import hi from '../language/hi.json';
import tn from '../language/tn.json';
export class LanguageManager {
  messages: any = {
    EN: en,
    HI: hi,
    TN: tn,
  };
}
