# ATTENTION: Don't run this file directly. Run `$ yarn build-lms-assets` instead.

# === CSS ===
NODE_OPTIONS=--max_old_space_size=4096 node-sass lms-assets/src/themes.scss lms-assets/themes.css\
  --output-style expanded

# Prepend warning message
printf '%s\n' 0a '/*
ATTENTION: This file should be included in the LMS global CSS file.
Do not edit it directly. You should update `lms-assets/src/themes.scss`
and run `$ yarn build-lms-assets` to regenerate it.
*/
' . '$a' . x | ex 'lms-assets/themes.css'

# === JS ===
# TODO: Perform some kind of minification
cp lms-assets/src/themes.js lms-assets/themes.js

# Prepend warning message
printf '%s\n' 0a '/*
ATTENTION: This file should be included in the LMS global JS file.
Do not edit it directly. You should update `lms-assets/src/themes.js`
and run `$ yarn build-lms-assets` to regenerate it.
*/
' . '$a' . x | ex 'lms-assets/themes.js'
