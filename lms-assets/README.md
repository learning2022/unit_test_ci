# LMS Assets

This directory contains the assets that should be included at the global level for LMSs. This includes [themes.css](themes.css) and [themes.js](themes.js). Do not edit these files directly. Rather edit the source files in the [`src` directory](src) and run `$ yarn build-lms-assets` to generate the files. They will also automatically be generated as a pre-commit git hook.
