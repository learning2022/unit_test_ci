echo "Checking SAST report"

if [ -r gl-sast-report.json ]; then

  echo "Number of Medium vulnerabilities found: "

  SAST_Vulnerabilities_length=$(cat gl-sast-report.json | 
  jq '.vulnerabilities | 
  map(select(.severity == "Medium") | 
  select(.location.file | 
  startswith("src/"))) | length')

  echo "${SAST_Vulnerabilities_length}"
  
  echo "Detailed report of the Medium vulnerabilities: "
  
  SAST_Vulnerabilities=$(cat gl-sast-report.json | 
          jq '.vulnerabilities | 
          map(select(.severity == "Medium") | 
          select(.location.file | 
          startswith("src/")))')
 
  echo "${SAST_Vulnerabilities}" >> test_report.json

        if [ ${SAST_Vulnerabilities_length} -gt 0 ]; then
  
          echo "Outstanding SAST ${SAST_Vulnerabilities_length} Medium vulnerabilities found, these must be fixed in accordance with Engineering Standards before Pipeline will pass"

        fi
        else
  
        echo "No Sast Report found or not readable"
  
        fi