#!/usr/bin/env python3

import json 
# import json2html
from json2html import *


with open('./test_report.json') as f:
    d = json.load(f)
    scanOutput = json2html.convert(json=d)
    htmlReportFile = "./sast_report.html" 
    with open(htmlReportFile, 'w') as htmlfile:
        htmlfile.write(str(scanOutput))
        print("Json file is converted into html successfully...")
 