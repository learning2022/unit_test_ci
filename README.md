<a href="https://loree.io/" target="_blank">
    <img src="https://loree.io/wp-content/uploads/2019/06/Loree_Logo_Final.png" alt="Loree" width="250" >
</a>

# Loree 2.0 | CrystalDelta

## Setup Development Environment

## Access

You'll require access to:

- Gitlab (if you're reading this, you already have access) - _2FA required_
- AWS IAM (programatic access)- this need to connect with amplify and access to serverless

### Setup Amplify Cli

- Requires Node.js® version 14
  Strongly suggest installing nvm: https://github.com/nvm-sh/nvm#install--update-script

```bash
nvm install 14
nvm use 14
```

Install and configure the Amplify CLI as follows:

```bash
$ npm install -g @aws-amplify/cli
$ amplify configure
```

Open [Amplify Documentation](https://docs.amplify.aws/start/getting-started/installation/q/integration/react) to view it in detail.

## Developing

To set up your local dev environment, go to the `loree-v2` directory and then run the following:<br>
`yarn install`<br>
`amplify init` <br>
`yarn start` <br>

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Running tests

    yarn test

### Running tests with Coverage

    yarn test:coverage

### Linting

We are currently in the process of migrating to a new (more strict) ESLint standard. To minimize disruption and sweeping changes, the new rules will only be enforced on new/modified files. This is achieved by using 2 ESLint configurations:

- `.eslintrc.js` This is the main one, and will be used during CI. This will also be the file to use for your code editor's ESLint integration.
- `.eslintrc.pre-commit.js` Used during pre-commit git hooks to ensure that the newer lint rules are being enforced.

#### Editing Existing Files

You will see a `/* eslint-disable */ // Remove this line when editing this file` at the top of the file. We are using the `unicorn/no-abusive-eslint-disable` ESLint rule to ensure this line is removed when a file is modified. Once you remove the first line comment, the rest of the file will then be checked with the full set of ESLint rules. It is then your responsibility to fix any issues in the rest of the file.

#### Fixing common issues

- `@typescript-eslint/no-floating-promises` - Either handle the promise or explicitly mark it as ignored with the `void` operator.
- `prettier/prettier` - The formatting needs updating. Run `yarn lint-fix` or format the file with Prettier.
- `@typescript-eslint/no-explicit-any` - This rule is to encourage us to utilize and write extensive types. However, sometimes `any` is still a necessary evil. If this is the case, there are a number of ways to work around these lint errors:

  - Simply remove the explicit `any` declaration and allow TypeScript to infer the type as `any`.

    eg:

    ```ts
    const foo: any = JSON.parse(someJsonString);
    console.log(foo.bar); // <- No warning
    ```

    becomes:

    ```ts
    const foo = JSON.parse(someJsonString);
    console.log(foo.bar); // <- Now has a warning from @typescript-eslint/no-unsafe-member-access
    ```

  - Use the new `_Any` global type. This should be an absolute last resort.

### For ESLint's and Prettier lint check,

    yarn lint

### For ESLint's and Prettier lint fix,

    yarn lint-fix

### Non-js file formatting

If you edit a file that is not a js/ts/json file, then you need to make sure the file is formatted to the "Prettier" standard. The easiest way to do this is to use your code editor's built in [Prettier integration](https://prettier.io/docs/en/editors.html). Alternatively you can also run the following command:

    yarn prettier --write path/to/file.md

## Git Flow

We use a slim git flow approach - MRs are mandatory, and require filing of a MR template, all merged MRs to develop.

## Merge Requests

Merge requests should generally be opened against **develop**.

Only include **_src_** files in your PR. Don't include any build files i.e. dist/.

Before submitting MR make sure to run `yarn` on the root of monorepo to ensure that commit lint and husky are installed.

Mention Jira card number in MR description, it will automatically update the Jira card with MR details.

If you're still working on the changes, add `WIP` tag in gitlab while creating MR.

# Accessibility

Accessibility is extremely important for both educators and students. It is a core requirement for all of our customers. Please read through the [Accessibility Guidelines](./README-ACCESSIBILITY.md) before you develop any user-facing features.

## Tests

Please ensure that your change still passes unit tests, and ideally integration tests.
Wherever possible, merge requests should contain tests as appropriate.
Bugfixes should contain tests that exercise the corrected behavior (i.e., the test should fail without the bugfix and pass with it), and new features should be accompanied by tests exercising the feature.

## Code Style

Generally, match the style of the surrounding code. Please ensure your changes don't wildly deviate from those rules. You can run `yarn lint-fix` to identify and automatically fix most style issues.

## CI

### Deployments - Amplify Pipeline

Deployments to Dev on a `develop` merge are automatic. Staging deploys require a manual pipeline trigger(not yet created now). Take a look at [Amplify](https://ap-southeast-2.console.aws.amazon.com/amplify/home?region=ap-southeast-2#/d3k8jsltpcivb)
