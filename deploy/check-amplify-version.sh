#!/usr/bin/env bash

# taken from
# https://gist.github.com/bitmvr/9ed42e1cc2aac799b123de9fdc59b016

DESIRED_VERSION="$1"
echo "Amplify Desired Version: $DESIRED_VERSION"

VERSION="${2}"

function amplifyVersionFromPackageJson() {
    echo "$(which amplify)" | sed 's#bin/amplify#lib/node_modules/@aws-amplify/cli/package.json#g'
}

# for testing
#PACKAGE_JSON=`echo $(amplifyVersionFromPackageJson)` && mv "$PACKAGE_JSON" "$PACKAGE_JSON.temp"

if [ "$VERSION" == "" ]; then
    VERSION=$(cat $(amplifyVersionFromPackageJson) | jq .version -r)
fi

# for testing
#PACKAGE_JSON=`echo "$(amplifyVersionFromPackageJson)"` && mv "$PACKAGE_JSON.temp" "$PACKAGE_JSON"

if [ "$VERSION" == "" ]; then
    VERSION=$(amplify --version)
fi

# remove special colour coding characters - https://superuser.com/questions/380772/removing-ansi-color-codes-from-text-stream
VERSION=`echo $VERSION | sed 's/\x1b\[[0-9;]*m//g'`

VERSION="${VERSION#[vV]}"
VERSION_MAJOR="${VERSION%%\.*}"
VERSION_MINOR="${VERSION#*.}"
VERSION_MINOR="${VERSION_MINOR%.*}"
VERSION_PATCH="${VERSION##*.}"

echo "Amplify Actual Version: ${VERSION}"
# for testing
# echo "Amplify Version [major]: ${VERSION_MAJOR}"
# echo "Amplify Version [minor]: ${VERSION_MINOR}"
# echo "Amplify Version [patch]: ${VERSION_PATCH}"

# echo "Matching '$VERSION_MAJOR.$VERSION_MINOR' to '$DESIRED_VERSION'" | cat -v

if [ "$VERSION_MAJOR.$VERSION_MINOR" == "$DESIRED_VERSION" ] || [ "$VERSION" == "$DESIRED_VERSION" ]; then
    exit 0
else
    if [ "$MASTEDLY_AMPLIFY_VERSION_CHECK" == "warning" ]; then
        echo "Versions don't match - please fix..."
        echo "yarn install -g @aws-amplify/cli@$DESIRED_VERSION"
        echo " you may also need to add --force"
        exit 0
    fi
    exit 1
fi
