# Elasticsearch Pipelines

## Deploy Pipeline Changes

```
PUT _ingest/pipeline/loree_rename_transaction_name
{
    "description": "Removes many image URLs to be one URL",
    "processors": [
        {
          "set": {
               "field": "_source.transaction.name_old",
                "value": "{{_source.transaction.name}}",
                "ignore_failure": true
            }
        },
        {
            "gsub": {
               "field": "_source.transaction.name",
                "pattern": "(https://loreev2storage24cd6295054c4df4b5661676008c768e164310-production.s3.ap-southeast-2.amazonaws.com/public/tmp/)(?:.(?!\/))+$",
                "replacement": "https://loreev2storage24cd6295054c4df4b5661676008c768e164310-production.s3.ap-southeast-2.amazonaws.com/public/tmp/APM_GROUPED_IMAGES.png",
                "ignore_failure": true
            }
        }
    ]
}
```

Then add it to the APM pipelines

```
PUT _ingest/pipeline/apm
{
  "description": "Default enrichment for APM events",
  "processors": [
    {
      "pipeline": {
        "name": "apm_user_agent"
      }
    },
    {
      "pipeline": {
        "name": "apm_user_geo"
      }
    },
    {
      "pipeline": {
        "name": "loree_rename_transaction_name"
      }
    }
  ]
}
```

## Test Pipeline Changes Before applying

```
POST _ingest/pipeline/_simulate
{
  "pipeline": {
    "description": "describe pipeline",
    "processors": [
      {
        "set": {
          "field": "_source.transaction.name_old",
          "value": "{{_source.transaction.name}}"
        }
      },
      {
        "gsub": {
          "field": "_source.transaction.name",
          "pattern": "(https://loreev2storage24cd6295054c4df4b5661676008c768e164310-production.s3.ap-southeast-2.amazonaws.com/public/tmp/)(?:.(?!\/))+$",
          "replacement": "https://loreev2storage24cd6295054c4df4b5661676008c768e164310-production.s3.ap-southeast-2.amazonaws.com/public/tmp/APM_GROUPED_IMAGES.png"
        }
      }
    ]
  },
  "docs": [
    {
      "_source": {
        "transaction": {
          "name": "PUT https://loreev2storage24cd6295054c4df4b5661676008c768e164310-production.s3.ap-southeast-2.amazonaws.com/public/tmp/plan3.jpg"
        }
      }
    }
  ]
}
```
