#!/usr/bin/env bash
expect () {
    if [ $1 == $2 ]; then
        echo "PASSED - got $1 expected $2"
    else
        echo "FAILED - got $1 expected $2"
    fi
}

# no desired provided so it will fail
./check-amplify-version.sh; expect "$?" "1"
echo " ";

# on our desired version
./check-amplify-version.sh 7.3.6; expect "$?" "0"
echo " ";

# on our desired version to minor version
./check-amplify-version.sh 7.3; expect "$?" "0"
echo " ";

# not on our desired version
./check-amplify-version.sh 4.49; expect "$?" "1"
echo " ";

# not on our desired version
./check-amplify-version.sh 7.0 8.0.0; expect "$?" "1"
echo " ";

# on our desired version
./check-amplify-version.sh 4.50 4.50.1; expect "$?" "0"
echo " ";

# not on our desired version but we just want a warning
MASTEDLY_AMPLIFY_VERSION_CHECK=warning ./check-amplify-version.sh 7.0 8.0.0; expect "$?" "0"
echo " ";
