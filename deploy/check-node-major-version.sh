#!/usr/bin/env bash

# taken from
# https://gist.github.com/bitmvr/9ed42e1cc2aac799b123de9fdc59b016

DESIRED_VERSION="$1"
echo "Node Desired Version: $DESIRED_VERSION"
VERSION="${2:-$(node --version)}"
VERSION="${VERSION#[vV]}"

VERSION_MAJOR="${VERSION%%\.*}"
# VERSION_MINOR="${VERSION#*.}"
# VERSION_MINOR="${VERSION_MINOR%.*}"
# VERSION_PATCH="${VERSION##*.}"

echo "Node Actual Version: ${VERSION}"
# echo "Node Version [major]: ${VERSION_MAJOR}"
# echo "Version [minor]: ${VERSION_MINOR}"
# echo "Version [patch]: ${VERSION_PATCH}"

if [ "$VERSION_MAJOR" == "$DESIRED_VERSION" ]; then
    exit 0
else
    exit 1
fi
