import axios from 'axios';
import { parse } from 'node-html-parser';
import he from 'he'

const getAuthenticityToken = (body) => {
  const root = parse(body);
  const value = root
    .querySelector('#login_form')
    .getElementsByTagName('input')
    .filter((e) => e.getAttribute('name') === 'authenticity_token')[0]
    .getAttribute('value');
  return value;
};
async function loginCanvas(username, password) {
  try {
    console.log(">> Login")
    const loadLoginResponse = await axios({
      method: 'GET',
      url: `${process.env.LOREE_CANVAS_HOST}/login/canvas`,
      headers: {
        authority: 'crystaldelta.instructure.com',
        pragma: 'no-cache',
        'cache-control': 'no-cache',
        'upgrade-insecure-requests': '1',
        'user-agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-fetch-site': 'none',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'sec-fetch-dest': 'document',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"macOS"',
        Referer: 'https://crystaldelta.instructure.com/login/canvas',
        'accept-language': 'en-GB,en;q=0.9',
      },
      data: null,
    });

    console.log(">> Cookies")
    const authenticityToken = he.encode(getAuthenticityToken(loadLoginResponse.data));
    const loginCookies = loadLoginResponse.headers['set-cookie'];
    const cookieHeader = buildCookieFromSetCookies(loginCookies);
    // console.log(cookieHeader);

    const headers = {
      accept:
        'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
      authority: 'crystaldelta.instructure.com',
      pragma: 'no-cache',
      'cache-control': 'no-cache',
      'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
      'sec-ch-ua-mobile': '?0',
      'sec-ch-ua-platform': '"macOS"',
      'upgrade-insecure-requests': '1',
      origin: 'https://crystaldelta.instructure.com',
      'content-type': 'application/x-www-form-urlencoded',
      'user-agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
      'sec-fetch-site': 'same-origin',
      'sec-fetch-mode': 'navigate',
      'sec-fetch-user': '?1',
      'sec-fetch-dest': 'document',

      Referer: 'https://crystaldelta.instructure.com/login/canvas',
      'accept-language': 'en-GB,en;q=0.9',
      cookie: cookieHeader,
    };
    const data = `utf8=%E2%9C%93&authenticity_token=${encodeURIComponent(
      authenticityToken,
    )}&redirect_to_ssl=1&pseudonym_session%5Bunique_id%5D=${encodeURIComponent(username)}&pseudonym_session%5Bpassword%5D=${password}&pseudonym_session%5Bremember_me%5D=0`;

    console.log(">> Validate Cookies in Headers")
    const loginResponse = await axios({
      method: 'POST',
      url: `${process.env.LOREE_CANVAS_HOST}/login/canvas`,
      headers: headers,
      data: data,
      maxRedirects: 0,
      validateStatus: (status) => status >= 200 && status <= 302
    });

    return loginResponse;
  } catch (err) {
    console.error(err);
    console.log(">> Canvas login FAILED. Detailed errors written to stderr (use \"2> setup_stack_err.log\" and read that file)");
    return null;
  }
}

function buildCookieFromSetCookies(setCookies) {
  return setCookies
    .map((cookie) => {
      const cookieParts = cookie.split(';');
      return cookieParts[0];
    })
    .reduce((previous, current) => {
      return previous + '; ' + current;
    });
}

function buildCommonCookies(cookie, csrfToken) {
  return {
    'x-csrf-token': csrfToken,
    cookie: cookie,
    Referer: `${process.env.LOREE_CANVAS_HOST}/accounts/1/developer_keys`,
  };
}
function buildCommonPOSTCookies(cookie, csrfToken) {
  return {
    ...buildCommonCookies(cookie, csrfToken),
    'content-type': 'application/json;charset=UTF-8',
  };
}

const getDeveloperKeys = async (cookie, csrfToken, accountId) => {
  console.log(">>> getDeveloperKeys() canvasBrowserRequest")
  return await canvasBrowserRequest(
    'GET',
    `${process.env.LOREE_CANVAS_HOST}/api/v1/accounts/${accountId}/developer_keys`,
    buildCommonCookies(cookie, csrfToken),
  );
};

async function setDeveloperKeyStatus(cookie, csrfToken, account, developerKeyId, status) {
  return await canvasBrowserRequest(
    'POST',
    `${process.env.LOREE_CANVAS_HOST}/api/v1/accounts/${account}/developer_keys/${developerKeyId}/developer_key_account_bindings`,
    buildCommonPOSTCookies(cookie, csrfToken),
    JSON.stringify({ developer_key_account_binding: { workflow_state: status } }),
  );
}

async function createDeveloperAPIKey(cookie, csrfToken, body, account = 1) {
  return await canvasBrowserRequest(
    'POST',
    `${process.env.LOREE_CANVAS_HOST}/api/v1/accounts/${account}/developer_keys`,
    buildCommonPOSTCookies(cookie, csrfToken),
    JSON.stringify(body),
  );
}

async function createDeveloperKey(cookie, csrfToken, body, account) {
  return await canvasBrowserRequest(
    'POST',
    `${process.env.LOREE_CANVAS_HOST}/api/lti/accounts/${account}/developer_keys/tool_configuration`,
    buildCommonPOSTCookies(cookie, csrfToken),
    JSON.stringify(body),
  );
}
// https://canvas.instructure.com/doc/api/external_tools.html#method.external_tools.create
async function getCanvasExternalTools(bearerToken) {
  const url = `${process.env.LOREE_CANVAS_HOST}/api/v1/accounts/1/external_tools?per_page=1000`;
  const headers = {
    Authorization: `Bearer ${bearerToken}`,
  };
  const response = await axios.get(url, { headers });
  response.data.forEach((externalTool) => {
    console.log(externalTool);
    const data = {
      url: externalTool.url,
      name: externalTool.name,
      custom_fields: externalTool.custom_fields,
      course_navigation: externalTool.course_navigation,
      developer_key_id: externalTool.developer_key_id,
      deployment_id: externalTool.deployment_id,
      version: externalTool.version,
    };
    console.log(data);
  });
}

async function createCanvasExternalTool(bearerToken, clientId, account, envName) {
  const url = `${process.env.LOREE_CANVAS_HOST}/api/v1/accounts/${account}/external_tools`;
  const headers = {
    Authorization: `Bearer ${bearerToken}`,
  };
  const body = {
    name: `Loree v2 ${envName}`,
    client_id: clientId,
  };
  return await axios.post(url, body, { headers });
}

async function listCanvasExternalTool(bearerToken, account = 1, envName = null) {
  let url = `${process.env.LOREE_CANVAS_HOST}/api/v1/accounts/${account}/external_tools?per_page=100`;
  if (envName) url = `${url}&search_term=${encodeURIComponent(envName)}`;
  const headers = {
    Authorization: `Bearer ${bearerToken}`,
  };
  return await axios.get(url, { headers });
}

const canvasBrowserRequest = async (method, url, headers = {}, body = undefined) => {
  const allHeaders = {
    ...headers,

    accept: 'application/json+canvas-string-ids, application/json, text/plain, */*',
    'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8,tr;q=0.7',
    'cache-control': 'no-cache',
    pragma: 'no-cache',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'x-requested-with': 'XMLHttpRequest',
    'Referrer-Policy': 'no-referrer-when-downgrade',

    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
    'authority': 'crystaldelta.instructure.com',
    'origin': 'https://crystaldelta.instructure.com'
  };
  // console.log(`allHeaders for ${method} ${url}`, allHeaders);
  // console.log(body);
  // console.log()
  return await axios({
    method,
    url,
    headers:allHeaders,
    data: body,
  });
};

const getApiKeyConfig = (envName, baseUrl) => ({
  developer_key: {
    name: `Loree V2 ${envName} API Key`,
    redirect_uris: `${baseUrl}/lti/token`,
    redirect_uri: `${baseUrl}/lti/token`,
    require_scopes: false,
  },
});

const getLtiConfig = (envName, baseUrl) => ({
  tool_configuration: {
    settings: {
      title: `Loree 2.0 ${envName}`,
      scopes: [],
      extensions: [
        {
          platform: 'canvas.instructure.com',
          settings: {
            // TODO - dont think this platform is needed here - its above
            platform: 'canvas.instructure.com',
            placements: [
              {
                placement: 'course_navigation',
                message_type: 'LtiResourceLinkRequest',
              },
            ],
          },
          privacy_level: 'public',
        },
      ],
      public_jwk: {},
      description: `${envName} environment`,
      custom_fields: {
        roles: '$Canvas.membership.roles',
        user_id: '$Canvas.user.id',
        course_id: '$Canvas.course.id',
        is_root_account_admin: '$Canvas.user.isRootAccountAdmin',
      },
      public_jwk_url: 'https://canvas.instructure.com/api/lti/security/jwks',
      target_link_uri: `${baseUrl}/lti/auth`,
      oidc_initiation_url: `${baseUrl}/lti/launch`,
    },
  },
  developer_key: {
    redirect_uris: `${baseUrl}/lti/auth`,
    name: envName,
    scopes: [],
  },
});

async function findDeveloperKey(cookie, csrfToken, ltiToolUrl) {
  const developerKeys = await getDeveloperKeys(cookie, csrfToken, 1);
  // console.debug(developerKeys.data);

  const envs = developerKeys.data.filter(
    (developerKey) =>
      developerKey.tool_configuration &&
      developerKey.tool_configuration.target_link_uri.indexOf(ltiToolUrl) > -1,
  );
  // console.debug(envs);

  if (envs.length === 0) {
    return null;
  }
  if (envs.length === 1) {
    return envs[0];
  }
  if (envs.length !== 1 || envs.length !== 0) {
    throw new Error('env not uniquely identified');
  }
}

async function findApiKeyByName(cookie, csrfToken, name) {
  const developerKeys = await getDeveloperKeys(cookie, csrfToken, 1);
  // console.debug(developerKeys.data);

  const envs = developerKeys.data.filter((developerKey) => developerKey.name === name);
  // console.debug(envs);

  if (envs.length === 0) {
    return null;
  }
  if (envs.length === 1) {
    return envs[0];
  }
  if (envs.length !== 1 || envs.length !== 0) {
    throw new Error('env not uniquely identified');
  }
}

export {
  createDeveloperKey,
  setDeveloperKeyStatus,
  createCanvasExternalTool,
  findDeveloperKey,
  getLtiConfig,
  getApiKeyConfig,
  createDeveloperAPIKey,
  findApiKeyByName,
  listCanvasExternalTool,
  loginCanvas,
  buildCookieFromSetCookies
}
