import fs from 'fs';
import crypto from 'crypto';
import AWS from 'aws-sdk';

async function getChildResource(cloudformation, envStack, childLogicalResourceId) {
  const envStackResources = await cloudformation
    .listStackResources({ StackName: envStack.StackName || envStack.PhysicalResourceId })
    .promise();

  const childResource = envStackResources.StackResourceSummaries.filter(
    (resource) => resource.LogicalResourceId === childLogicalResourceId,
  );

  if (childResource.length !== 1) {
    throw new Error(`Found ${childResource.length} matches for ${childLogicalResourceId}`);
  }

  return childResource[0];
}

async function findEnvStack(cloudformation, envName) {
  const allStacks = await listAllStacks(cloudformation);
  const envStacks = allStacks.StackSummaries.filter(
    (stack) => stack.StackName.indexOf(envName) > -1 && !stack.RootId,
  );
  if (envStacks.length !== 1) {
    throw new Error('More than one stack found');
  }
  const envStack = envStacks[0];
  return envStack;
}

async function listAllTables(dynamodb) {
  const allTables = await dynamodb.listTables().promise();
  let next = allTables;
  while (next.LastEvaluatedTableName) {
    next = await dynamodb
      .listTables({ ExclusiveStartTableName: next.LastEvaluatedTableName })
      .promise();
    allTables.TableNames.push(...next.TableNames);
  }
  return allTables;
}

async function listAllStacks(cloudformation) {
  const stacks = await cloudformation.listStacks().promise();
  let nextStacks = stacks;
  while (nextStacks.NextToken) {
    nextStacks = await cloudformation.listStacks({ NextToken: nextStacks.NextToken }).promise();
    stacks.StackSummaries.push(...nextStacks.StackSummaries);
  }
  return stacks;
}

async function primeLtiPlatformTable(dynamodb, tableName, envName, clientId) {
  const record = JSON.parse(fs.readFileSync('./deploy/setup/setupStack-LtiPlatform.json', 'utf8'));
  record.clientId = { S: clientId };
  record.platformName = { S: `Loree 2.0 ${envName}` };
  const rowToPut = {
    TableName: tableName,
    Item: record,
  };
  // console.log(rowToPut);

  await dynamodb.putItem(rowToPut).promise();
  return record;
}

async function primeLoreeFeaturesTable(dynamodb, tableName, ltiPlatformID) {
  const record = JSON.parse(
    fs.readFileSync('./deploy/setup/setupStack-LoreeFeatures.json', 'utf8'),
  );
  record.ltiPlatformID = { S: ltiPlatformID };
  const rowToPut = {
    TableName: tableName,
    Item: record,
  };

  await dynamodb.putItem(rowToPut).promise();
  return record;
}

async function primeCustomStyleTable(dynamodb, tableName, ltiPlatformID) {
  const record = JSON.parse(fs.readFileSync('./deploy/setup/setupStack-CustomStyle.json', 'utf8'));
  record.ltiPlatformID = { S: ltiPlatformID };
  // file.owner = ''; // TODO - work out the impact
  const rowToPut = {
    TableName: tableName,
    Item: record,
  };

  await dynamodb.putItem(rowToPut).promise();
  return record;
}

async function primeLtiApiKeyTable(
  dynamodb,
  tableName,
  ltiPlatformID,
  clientId,
  apiClientId,
  apiSecretKey,
) {
  const record = JSON.parse(fs.readFileSync('./deploy/setup/setupStack-LtiApiKey.json', 'utf8'));
  record.ltiPlatformID = { S: ltiPlatformID };
  record.apiClientId = { S: apiClientId };
  record.apiSecretKey = { S: apiSecretKey };
  record.ltiClientID = { S: clientId };
  const rowToPut = {
    TableName: tableName,
    Item: record,
  };

  await dynamodb.putItem(rowToPut).promise();
  return record;
}

async function queryLtiPlatformKey(docClient2, tableName, platformId) {
  const docClient = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: tableName,
    IndexName: 'byLtiPlatform',
    KeyConditionExpression: '#name = :value',
    ExpressionAttributeValues: { ':value': platformId },
    ExpressionAttributeNames: { '#name': 'ltiPlatformID' },
  };

  try {
    const data = await docClient.query(params).promise();
    console.log('Query LtiPlatformKey. Data.items :', data);
    return data.Items[0];
  } catch (err) {
    console.error('Error in query LtiPlatformKey. Error:', err);
    return err;
  }
}

async function updateLtiPlatformKey(
  docClient2,
  tableName,
  ltiPlatformID,
  keyid,
  kid,
  privatekey,
  publicKey,
) {
  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: tableName,
    Key: {
      id: keyid,
    },
    UpdateExpression:
      'set #kid = :kid, #privatekey = :privatekey, #publicKey = :publicKey, #ltiPlatformID = :ltiPlatformID',
    ExpressionAttributeNames: {
      '#kid': 'kid',
      '#privatekey': 'privatekey',
      '#publicKey': 'publicKey',
      '#ltiPlatformID': 'ltiPlatformID',
    },
    ExpressionAttributeValues: {
      ':kid': kid,
      ':privatekey': privatekey,
      ':publicKey': publicKey,
      ':ltiPlatformID': ltiPlatformID,
    },
  };
  try {
    console.log('updated LtiPlatformKey. updateLtiPlatformKey:');
    return await docClient.update(params).promise();
  } catch (err) {
    console.error('Error in updateLtiPlatformKey. Error:', err);
    return err;
  }
}

async function primeLtiPlatformKeyTable(dynamodb, tableName, ltiPlatformID) {
  const keys = crypto.generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem',
    },
    privateKeyEncoding: {
      type: 'pkcs1',
      format: 'pem',
    },
  });
  const kid = crypto.randomBytes(16).toString('hex');

  const record = {
    id: { S: 'a028ccb1-e1be-4019-95b0-445e99f3738b' },
    kid: { S: kid },
    ltiPlatformID: { S: ltiPlatformID },
    privatekey: { S: keys.privateKey },
    publicKey: { S: keys.publicKey },
  };
  const rowToPut = {
    TableName: tableName,
    Item: record,
  };

  await dynamodb.putItem(rowToPut).promise();
  return record;
}

async function addEnvToLtiLambda(lambda, envName) {
  const loreeltifnLambdaFunction = await lambda
    .getFunctionConfiguration({
      FunctionName: `loreeltifn-${envName}`,
    })
    .promise();

  await lambda
    .updateFunctionConfiguration({
      FunctionName: `loreeltifn-${envName}`,
      Environment: {
        Variables: {
          ...loreeltifnLambdaFunction.Environment.Variables,
          LOREEV2_APP_URL: process.env.LOREE_FRONT_END_URL,
        },
      },
    })
    .promise();
}

export {
  getChildResource,
  findEnvStack,
  listAllTables,
  listAllStacks,
  primeLtiPlatformTable,
  primeLoreeFeaturesTable,
  primeCustomStyleTable,
  updateLtiPlatformKey,
  queryLtiPlatformKey,
  primeLtiPlatformKeyTable,
  primeLtiApiKeyTable,
  addEnvToLtiLambda
}
