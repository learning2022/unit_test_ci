#!/bin/bash
set -e
IFS='|'

AWSCLOUDFORMATIONCONFIG="{\
\"configLevel\":\"project\",\
\"useProfile\":true,\
\"profileName\":\"cd-sandbox-AdministratorAccess\"\
}"
NOTIFICATIONSCONFIG="{\
\"Pinpoint\":{
\"SMS\":{
\"Enabled\":true\
},\
\"Email\":{
\"Enabled\":true,\
\"FromAddress\":\"xxx@amzon.com\",\
\"Identity\":\"identityArn\",\
\"RoleArn\":\"roleArn\"\
},\
\"APNS\":{
\"Enabled\":true,\
\"DefaultAuthenticationMethod\":\"Certificate\",\
\"P12FilePath\":\"p12filePath\",\
\"Password\":\"p12FilePasswordIfAny\"\
},\
\"FCM\":{
\"Enabled\":true,\
\"ApiKey\":\"fcmapikey\"\
}\
}\
}"
AMPLIFY="{\
\"envName\":\"$LOREE_ENV_NAME\"\
}"
PROVIDERS="{\
\"awscloudformation\":$AWSCLOUDFORMATIONCONFIG\
}"
CATEGORIES="{\
\"notifications\":$NOTIFICATIONSCONFIG\
}"

amplify init \
--amplify $AMPLIFY \
--providers $PROVIDERS \
--categories $CATEGORIES \
--yes
