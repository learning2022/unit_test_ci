import {
  createDeveloperKey,
  setDeveloperKeyStatus,
  createCanvasExternalTool,
  findDeveloperKey,
  getLtiConfig,
  getApiKeyConfig,
  createDeveloperAPIKey,
  findApiKeyByName,
  listCanvasExternalTool,
  loginCanvas,
  buildCookieFromSetCookies
} from './canvasHelper.mjs';

import AWS from 'aws-sdk';
import {
  findEnvStack,
  getChildResource,
  listAllTables,
  primeLtiPlatformTable,
  primeLoreeFeaturesTable,
  primeCustomStyleTable,
  primeLtiApiKeyTable,
  primeLtiPlatformKeyTable,
  addEnvToLtiLambda,
} from './awsHelper.mjs';

// node deploy/setup/setupStack.mjs

const region = process.env.AWS_REGION || 'ap-southeast-2';

const envName = process.argv[2] || process.env.LOREE_ENV_NAME;
if (!envName) {
  console.log('Please set envName');
  process.exit(1);
}
const username = process.env.LOREE_USERNAME;
if (!process.env.LOREE_USERNAME) {
  console.log('Please set process.env.LOREE_USERNAME');
  process.exit(1);
}

const password = (""+process.env.LOREE_PASSWORD).replace(/[\\$'"]/g, "\\$&"); // escape for url
if (!process.env.LOREE_PASSWORD) {
  console.log('Please set process.env.LOREE_PASSWORD');
  process.exit(1);
}

const accessToken = process.env.MASTEDLY_TESTING_ACCESS_TOKEN;
if (!accessToken || accessToken.includes("_")) {
  console.log('Invalid -- process.env.MASTEDLY_TESTING_ACCESS_TOKEN: ' + accessToken);
  console.log('Please set process.env.MASTEDLY_TESTING_ACCESS_TOKEN - COPY FROM CANVAS PROFILE');
  process.exit(1);
}
if (!process.env.LOREE_CANVAS_HOST) {
  process.env.LOREE_CANVAS_HOST = 'https://crystaldelta.instructure.com';
}

if (!process.env.LOREE_FRONT_END_URL) {
  process.env.LOREE_FRONT_END_URL = 'http://localhost:3000';
}

setupEnv(envName)
  .then(() => {
    console.log('');
  })
  .catch((e) => {
    console.error(e);
    if (e.response && e.response.headers) {
      console.error(e.response.headers);
    }

    if (e.response && e.response.data) {
      console.error(e.response.data);
    }
  });

async function setupEnv(envName) {
  const cloudformation = new AWS.CloudFormation();
  const apigateway = new AWS.APIGateway();
  const dynamodb = new AWS.DynamoDB();
  const lambda = new AWS.Lambda();
  const cogintoUserPool = new AWS.CognitoIdentityServiceProvider();

  /// /////////////////////////////////////////////////////////////
  // Canvas Login
  /// /////////////////////////////////////////////////////////////

  console.log("> LoginCanvas");
  const loginResponse = await loginCanvas(username, password);
  const cookie = buildCookieFromSetCookies(loginResponse.headers['set-cookie']);
  const csrfToken = decodeURIComponent(loginResponse.headers['set-cookie']
    .find(cookie => cookie.indexOf('_csrf_token=') > -1)
    .split(';')[0].split('=')[1])

  const envStack = await findEnvStack(cloudformation, envName);

  /// /////////////////////////////////////////////////////////////
  // Setup Canvas
  /// /////////////////////////////////////////////////////////////

  try {
    console.log("> SetupCanvas")
    // get api resource url
    console.log(">> API GW")
    const url = await getApiGatewayUrl(cloudformation, envStack, apigateway);

    console.log(">> findDeveloperKey")
    let developerKey = await findDeveloperKey(cookie, csrfToken, url);

    if (developerKey === null) {
      const ltiConfig = getLtiConfig(envName, url);
      console.log('createDeveloperKey');
      developerKey = await createDeveloperKey(cookie, csrfToken, ltiConfig, 1);
    }
    // get resource || create response
    console.log(">> setDeveloperKeyStatus")
    const clientId = developerKey.id || developerKey.data.developer_key.id;
    await setDeveloperKeyStatus(cookie, csrfToken, '1', clientId, 'on');

    let apiKey = await findApiKeyByName(cookie, csrfToken, `Loree V2 ${envName} API Key`);
    if (apiKey === null) {
      const apiKeyConfig = getApiKeyConfig(envName, url);
      console.log('>> createDeveloperAPIKey');
      apiKey = await createDeveloperAPIKey(cookie, csrfToken, apiKeyConfig, 1);
    }

    const apiKeyId = apiKey.id || apiKey.data.id;
    const apiKeySecret = apiKey.api_key || apiKey.data.api_key;
    console.log(">> setDeveloperKeyStatus")
    await setDeveloperKeyStatus(cookie, csrfToken, '1', apiKeyId, 'on');

    let externalTools = await listCanvasExternalTool(accessToken, 1, `${envName}`);
    externalTools = externalTools.data.filter((et) => et.name === `Loree 2.0 ${envName}`);
    // console.log(externalTools);
    let externalTool;
    if (externalTools.length === 0) {
      console.log('>> createCanvasExternalTool');
      externalTool = (await createCanvasExternalTool(accessToken, clientId, 1, envName)).data;
    } else if (externalTools.length === 1) {
      externalTool = externalTools[0];
    } else if (externalTools.length !== 1) {
      throw new Error(
        `multiple ${externalTools.length} matches of external tool 'Loree v2 ${envName}'`,
      );
    }
    // console.log(externalTool);

    /// /////////////////////////////////////////////////////////////
    // Configure Lambda and Cognito
    /// /////////////////////////////////////////////////////////////

    await addEnvToLtiLambda(lambda, envName);
    await createCognitoUserPoolCustomAttributes(cloudformation, envStack, cogintoUserPool);

    /// /////////////////////////////////////////////////////////////
    // Prime Dynamodb Tables
    /// /////////////////////////////////////////////////////////////

    const allTables = await listAllTables(dynamodb);
    const envTables = allTables.TableNames.filter((tableName) => tableName.includes(envName));

    const customStyleTable = envTables.find((tableName) => tableName.includes('CustomStyle-'));
    const loreeFeaturesTable = envTables.find((tableName) => tableName.includes('LoreeFeature-'));
    const ltiPlatformTable = envTables.find((tableName) => tableName.includes('LtiPlatform-'));
    const ltiApiKeyTable = envTables.find((tableName) => tableName.includes('LtiApiKey-'));
    const ltiPlatformKeyTable = envTables.find((tableName) => tableName.includes('LtiPlatformKey-'));

    console.log(`>> primeLtiPlatformTable (upsert) - ${ltiPlatformTable}`);
    const ltiPlatform = await primeLtiPlatformTable(dynamodb, ltiPlatformTable, envName, clientId);
    console.log(`>> primeLoreeFeaturesTable (upsert) - ${loreeFeaturesTable}`);
    await primeLoreeFeaturesTable(dynamodb, loreeFeaturesTable, ltiPlatform.id.S);
    console.log(`>> primeCustomStyleTable (upsert) - ${customStyleTable}`);
    await primeCustomStyleTable(dynamodb, customStyleTable, ltiPlatform.id.S);
    console.log(`>> primeLtiApiKey (upsert) - ${ltiApiKeyTable}`);
    await primeLtiApiKeyTable(
      dynamodb,
      ltiApiKeyTable,
      ltiPlatform.id.S,
      clientId,
      apiKeyId,
      apiKeySecret,
    );

    console.log(`>> primeLtiPlatformKey (upsert - regenerate) - ${ltiPlatformKeyTable}`);
    await primeLtiPlatformKeyTable(dynamodb, ltiPlatformKeyTable, ltiPlatform.id.S);

    console.log('');
    console.log('Hi you can test me out here.....');
    console.log(`${process.env.LOREE_CANVAS_HOST}/courses/803/external_tools/${externalTool.id}`);
    console.log('');

  } catch (err) {
    console.error(err);
    console.log(">> SetupCanvas FAILED. Detailed errors written to stderr (use \"2> setup_stack_err.log\" and read that file)");
    return;
  }
}

async function getApiGatewayUrl(cloudformation, envStack, apigateway) {
  const apiloreeltiapiStack = await getChildResource(cloudformation, envStack, 'apiloreeltiapi');

  const loreeltiapi = await getChildResource(cloudformation, apiloreeltiapiStack, 'loreeltiapi');
  const apiStage = await apigateway
    .getStages({ restApiId: loreeltiapi.PhysicalResourceId })
    .promise();
  return `https://${loreeltiapi.PhysicalResourceId}.execute-api.${region}.amazonaws.com/${apiStage.item[0].stageName}`;
}

async function createCognitoUserPoolCustomAttributes(cloudformation, envStack, cogintoUserPool) {
  const cognitoStack = await getChildResource(cloudformation, envStack, 'authloreev275f92347');
  const loreeCognitoUserPool = await getChildResource(cloudformation, cognitoStack, 'UserPool');
  //console.log('UserPool>>>', loreeCognitoUserPool);
  const existingLoreeCognitoUserPoolCustomAttributes = await cogintoUserPool
    .describeUserPool({
      UserPoolId: loreeCognitoUserPool.PhysicalResourceId,
    })
    .promise();

  // console.log(existingLoreeCognitoUserPoolCustomAttributes.UserPool.SchemaAttributes);
  const loreeCognitoUserPoolCustomAttributesExist =
    existingLoreeCognitoUserPoolCustomAttributes.UserPool.SchemaAttributes.some(
      (attribute) => ['custom:lms_email', 'custom:platform'].indexOf(attribute.Name) > -1,
    );

  if (!loreeCognitoUserPoolCustomAttributesExist) {
    const loreeCognitoUserPoolCustomAttributes = {
      CustomAttributes: [
        {
          AttributeDataType: 'String',
          DeveloperOnlyAttribute: false,
          Mutable: true,
          Name: 'platform',
          Required: false,
          StringAttributeConstraints: {
            MaxLength: '256',
            MinLength: '1',
          },
        },
        {
          AttributeDataType: 'String',
          DeveloperOnlyAttribute: false,
          Mutable: true,
          Name: 'lms_email',
          Required: false,
          StringAttributeConstraints: {
            MaxLength: '256',
            MinLength: '1',
          },
        },
      ],
      UserPoolId: loreeCognitoUserPool.PhysicalResourceId,
    };
    await cogintoUserPool.addCustomAttributes(loreeCognitoUserPoolCustomAttributes).promise();
  }
}
