const fs = require('fs');
const process = require('process');

function getGitCommit(gitDir = './') {
  const rev = fs.readFileSync(`${gitDir}.git/HEAD`).toString().trim();
  if (rev.indexOf(':') === -1) {
    return rev;
  } else {
    return fs
      .readFileSync(`${gitDir}.git/` + rev.substring(5))
      .toString()
      .trim();
  }
}

console.log(getGitCommit(process.argv.slice(2)[0]));
