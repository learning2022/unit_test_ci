#!/usr/bin/env bash
expect () {
    if [ $1 == $2 ]; then
        echo "PASSED - got $1 expected $2"
    else
        echo "FAILED - got $1 expected $2"
    fi
}

./check-node-major-version.sh; expect "$?" "1"
./check-node-major-version.sh 14; expect "$?" "0"
./check-node-major-version.sh 12; expect "$?" "1"
./check-node-major-version.sh 12 16.12.1; expect "$?" "1"
./check-node-major-version.sh 12 v12.15.1; expect "$?" "0"
