#!/bin/bash

# deploy/apm-upload-sourcemaps.sh $APM_SECRET_KEY

echo REACT_APP_LOREE_ENV: $REACT_APP_LOREE_ENV

APM_SECRET_KEY=$1
PROJECT_BASE=.
JS_FILES=$PROJECT_BASE/build/static/js/*.js

 ./node_modules/typescript/bin/tsc ./src/loree-editor/constant.ts
SERVICE_VERSION=$(node -e "console.log(require('./src/loree-editor/constant').default.LOREE_APP_VERSION)")
rm ./src/loree-editor/constant.js

GIT_COMMIT=$(node $PROJECT_BASE/deploy/getGitCommit.js)
echo service_version: $GIT_COMMIT-$SERVICE_VERSION

case $REACT_APP_LOREE_ENV in
  develop)
    BASE_URL=https://dev-loree-v2.crystaldelta.net/static/js/
    ;;
  staging)
    BASE_URL=https://stg-loree-v2.crystaldelta.net/static/js/
    ;;
  production)
    BASE_URL=https://master.d1uzdukkodhm61.amplifyapp.com/static/js/
    ;;
  productionus)
    BASE_URL=https://master.d273ugfkhsuatz.amplifyapp.com/static/js/
    ;;
  *)
    echo "unsupported environment - $ENV"
    exit
    ;;
esac

echo BASE_URL: $BASE_URL

for f in $JS_FILES; do
  echo "Source File - $(basename $f)"

  curl https://5c460b35aaaa424491760e12614fccc2.apm.ap-southeast-2.aws.cloud.es.io:443/assets/v1/sourcemaps -X POST \
      -F sourcemap="@$f.map" \
      -F service_version="$GIT_COMMIT-$SERVICE_VERSION" \
      -F bundle_filepath="$BASE_URL/$(basename $f)" \
      -F service_name="Loree-V2" \
      -H "Authorization: Bearer $APM_SECRET_KEY"
done
